<?php

function has_child_class($class_id,  $type = "category"){
    $sql = "SELECT * FROM `class` c JOIN `class_type` ct WHERE (c.class_id = ct.class_id) ";
    $sql .= "AND  ct.type = '$type' AND ct.parent = $class_id";
    $en = DB::run()->setSql($sql)->run_sql();
    return ($en->get_count()) ? true:false;
}

function most_news_category(){
    $most_news_sql = "select count(*) as total_news, (select url_ref from class c where c.class_id = cr.class_id) as url_ref
    from content co join class_relationship cr where(cr.entity_id = co.content_id)
    group by cr.class_id order by total_news desc";
    $m = DB::run()->setSql($most_news_sql)->run_sql();
    if($m->get_count()){
        $most = $m->get_first();
        return $most['url_ref'];
    }
    return "";
}

//get class options
function get_class_options_func($row, $ac_id, $lavel){
    $lavel--;
    $selected = "";
    if(is_array($ac_id)){
        $selected = in_array($row['class_id'], $ac_id) ? " selected":" ";
    }else {
        $selected = ($ac_id == $row['class_id']) ? " selected":" ";
    }
    $res = "<option value=\"".$row['class_id']."\" ";
    $res .= $selected;
    $res .= " >";
    $res .= child_sym($lavel, "&nbsp;&nbsp;&nbsp;").$row['name'];
    $res .= "</option>";
    return $res; 
}

function get_class_options_func_by_ref($row, $ac_id, $level){
    $level--;
    $selected = "";
    
    if(is_array($ac_id)){
        $selected = in_array($row['url_ref'], $ac_id) ? " selected":" ";
    }else {
        $selected = ($ac_id == $row['url_ref']) ? " selected":" ";
    }
    
    $res = "<option value=\"".$row['url_ref']."\" ";
    $res .= $selected;
    $res .= " >";
    $res .= child_sym($level, "&nbsp;&nbsp;&nbsp;").$row['name'];
    $res .= "</option>";
    return $res; 
}

function news_id_to_cats($id){
    $c = _get("class_relationship", "entity_id", $id);
    
    $cats = array();
    foreach($c as $cat){
        $cats[] = $cat['class_id'];
    }
    return $cats;
}

function get_class_options($type = "category", $init_html = null, $selected = null){
    
    $tree = load()->lib('ClassList')->run($type);
    
    $tree->set_active_param($selected);
    
    $tree->init_html($init_html);
    $tree->set_is_tree(true);
    $tree->set_elemets_oper("get_class_options_func");
    
    return $tree;

    //return $tree->getData()->get(false);
}

function get_class_options_by_ref($type = "category", $init_html = null, $selected = null){
    $tree = load()->lib('ClassList')->run($type);    
    $tree->set_active_param($selected);
    $tree->init_html($init_html);
    $tree->set_is_tree(true);
    $tree->set_elemets_oper("get_class_options_func_by_ref");
    return $tree;
}

function get_class_options_backup($type = "category", $init_html = null, $selected = null){
    $sql = "select * from class ";
    $sql .= "natural join class_type ";
    $sql .= "where type='".$type."' order by class_id DESC";
    $tree = loadLib("GetClassData");
    
    $tree->setSql($sql);
    
    $tree->set_active($selected);
    $tree->init_html($init_html);
    $tree->set_row("get_class_options_func");
    return $tree->getData()->get(false);
}

function get_class_opt_func($row, $ac_id, $level){
    $selected = "";
    if(is_array($ac_id)){
        $selected = in_array($row['url_ref'], $ac_id) ? " selected":" ";
    }else {
        $selected = ($ac_id == $row['url_ref']) ? " selected":" ";
    }

    $res = "<option value=\"".$row['url_ref']."\" ";
    $res .= $selected;
    $res .= " >";
    $res .= child_sym($level, "&nbsp;&nbsp;&nbsp;").$row['name'];
    $res .= "</option>";
    return $res; 
}



function get_class_opt($type = "category", $init_html = null, $selected = null){
    $sql = "select * from class ";
    $sql .= "natural join class_type ";
    $sql .= "where type='".$type."' order by class_id DESC";
    $tree = loadLib("GetClassData");
    $tree->setSql($sql);
    $tree->set_active($selected);
    $tree->init_html($init_html);
    $tree->set_row("get_class_opt_func");
    return $tree->getData()->get(false);
}
//end of get class options

//get class delete 
function get_class_delete_func($row, $ac_id, $level){
    global $type;
    $res = "<tr >";
    $res .= "<td style='width:310px'>";
    $res .= child_sym($level,"&nbsp;&nbsp;&nbsp;").$row['name'];
    $res .= "</td>";
    $res .= "<td  style='text-align:center'>";
    $res .= "<a href='?type=$type&id=".$row['class_id']."'>";
    $res .= "Delete";
    $res .= "</a>";
    $res .= "</td>";
    $res .= "</tr>";
    return $res;
}

function get_class_delete($type = "category", $init_html = null, $selected = null){
    $sql = "select * from class ";
    $sql .= "natural join class_type ";
    $sql .= "where type='".$type."' order by class_id DESC";
    $tree = loadLib("GetClassData");
    $tree->setSql($sql);
    $tree->set_active($selected);
    $tree->init_html($init_html);
    $tree->setTag('table');
    $tree->set_row("get_class_delete_func");
    return $tree->getData()->get(false);
}
//end of get class delete function 


//get class list


function get_class_list($function, $type = 'category', $is_tree = true, $ac = ""){
    $list = load()->lib("ClassList")->run($type);
    if($type != "tag") $list->set_html("", $is_tree, "");
    $list->set_active_param($ac);
    $list->set_is_tree($is_tree);
    $list->_print_tag_in_tree = false;
    $list->set_elemets_oper($function);
    return $list;
}


function get_headlines($function, $top = false, $limit = 10){
    $list = load()->lib("Headlines")->run($top);
    $list->set_is_tree(false);
    $list->data_limit($limit);
    $list->set_html("");
    $list->set_elemets_oper($function);
    return $list;
}

//end of get class list

function get_author_options_backup(){
    $db = DB::run()->read('author')->run_sql();
    //$res = "<option value=\"0\" >(Select)</option>";
    $res = "";
    if($db->get_count()){
        foreach($db->get_array() as $auth){
            $res .= "<option value=\"".$auth['auth_id']."\" >";
            $res .= $auth['auth_name'];
            $res .= "</option>";
        }
    }
    return $res;
}

function child_sym($order, $symbol = "&mdash; "){
        
    $res = "";
    if($order == 1){
        $res = $symbol;  
    }else{
        for($i = 0; $i < $order; $i++){
            $res .= $symbol;
        }
    }
    return $res;
}

function has_class_exists($name,$type, $parent = 0){
    $sql = "select * from class ";
    $sql .= "natural join class_type ";
    $sql .= "where type ='$type'";
    $sql .= " and name = '$name'";
    $sql .= " and parent = '$parent'";
    $cats = DB::run()->setSql($sql)->run_sql();
    return $cats->get_count() ? true:false;
}

function make_unique_class($name, $type, $parent = 0, $order = 0, $level = 0){
    //echo "Run from level".$level+1 ."<br>";
    
    $name = strtolower($name);;
    $ref = "";
    if($level == 3000) exit("Fatal Error!");
    if($level == 0){
        $name = str_replace(
            array('#','%','&','*','(',')','!','@',' '),
            array('-hash-','-percent-','-and-','-star-','-','-','-','-at-','-') , 
            strtolower($name)
        );
        if($order > 0){
            $sql = "SELECT `url_ref` FROM class NATURAL JOIN class_type";
            $r = DB::run()->setSql($sql)->where(array('class_id'=> $parent, 'type'=>$type))->run_sql()->get_first();
            $r = $r['url_ref'];
            $ref = $r."-".$name;            
        }else{
            $ref = $name;
        }
    }else {
        $params = explode('-', $name);
        $last_elem = $params[count($params) - 1];
        if(is_numeric($last_elem)){
            if(count($params) > 1){
                array_pop($params);
                $last_elem++;
                $ref =  strtolower(implode('-', $params).'-'.$last_elem);
            }else {
                $ref = strtolower($ref."-".(2));
            }
        }else {
            $ref = strtolower($name."-".(2));
        }
    }
    
    if(is_unique_class($ref, $type)){
        return $ref;
    }else {
        $level++;
        return make_unique_class($ref,$type, $parent, $order, $level);
    }
    
}   

function is_unique_class($ref, $type){
    $sql = "SELECT `url_ref` FROM class NATURAL JOIN class_type";
    $cond = DB::run()->setSql($sql)->where(array('url_ref'=> $ref, 'type'=>$type))->limit(1)->run_sql();
    return ($cond->get_count()) ? false:true;
}


function make_unique_ref($ref, $table, $p=0){
    $p++;

    if($p == 2000) exit("Internal Probleme");
    
    $ref = str_replace(
        array('#','%','&','*','(',')','!','@',' ','?','>',":"),
        array('-hash-','-percent-','-and-','-star-','-','-','-','-at-','-','-','-','-') , 
        strtolower($ref)
    );
    
    if(is_unic_ref($ref, $table)){
        return $ref;
    }
    $refs  = '';
    $params = explode('-', $ref);
    if(is_numeric($params[(count($params)-1)])){
        if(count($params) > 1){
            $number = $params[(count($params)-1)];
            $number++;
            array_pop($params);
            if(count($params) == 1){
                $refs = strtolower($params[0].'-'.$number);
            }else {
                $refs = strtolower(implode('-', $params).'-'.$number);
            }
        }
    }else {
        $refs = strtolower($ref."-".(1));
    }
    if(is_unic_ref($refs, $table)){
        return $refs;
    }else {
        return make_unique_ref($refs, $table, $p);
    }
}



function is_unic_ref($ref,$table){
    $col = ($table == 'author') ? 'auth_url_ref':'url_ref';
    
    $cond = DB::run()->read($table)->where($col, $ref)->limit(1)->run_sql();
    
    return ($cond->get_count()) ? false:true;
}

function has_class($cname, $parent, $type = "category"){
    $cname = escape_all($cname);
    $type = escape_all($type);
    $parent = (int) $parent;
    $sql  = "SELECT class_id FROM class";
    $sql .= " NATURAL JOIN class_type";
    return DB::run()->setSql($sql)->where(array('name' => $cname, 'parent' => $parent, 'type' => $type))->run_sql()->get_count() ? true:false;
}

function get_image_thumb($filename, $date, $ext){
    $book_root = get_config("books/root-path");
    $split_filename = explode(".", $filename);
    $y = get_date_formate($date, "Y");
    $m = get_date_formate($date, "m");
    if(count($split_filename) > 1){
        $path = $book_root."/".$y."/".$m."/thumb/".$split_filename[0]."_thumb.".$ext;
        
        if(file_exists(ABSPATH.$path)){
            return base_url()."/".$path;
            
        }else{
            return admin_url()."/images/book_thumb.png";
        }
    }else {
        return admin_url()."/images/book_thumb.png";
    }
    return false;
}

function get_thumb_from_DB($book_id){ 
    $book_root = get_config("books/root-path");
    $t = DB::run()->read("book_list")->where("book_id", $book_id)->run_sql()->get_first_obj();
    
    $y = get_date_formate($t->upload_date, "Y");
    $m = get_date_formate($t->upload_date, "m");

    if($t->thumb == null){   
         return get_image_thumb($t->filename, $t->upload_date, $t->thumb_ext);
    }else {
        $path = $book_root."/".$y."/".$m."/thumb/".$t->thumb;
        if(file_exists(ABSPATH.$path)){
            return base_url()."/".$path;
        }else{
            return admin_url()."/images/book_thumb.png";
        }
    }
    return false;
}

function get_author_info($book_id){
    $sql = "SELECT * FROM author NATURAL JOIN book_auth_rel ";
    $auths_db = DB::run()->setSql($sql)->where(
                array(
                    'book_id'=> $book_id,
                    'author_priority'=> 1,
                )
            )->run_sql();
    $auth = $auths_db->get_first();
    return $auth;
}

function show_errors($errors, $class = ""){
   
    $e = "<ul class='".$class."'>";
    foreach($errors as $error){
        $e .= "<li>";
            $e .= $error;
        $e .= "</li>";
    }
    $e .= "</ul>";
    echo $e;
}
    
//Class List
function class_list_func($each, $active_key, $level){

    $url_ref = md5($each['url_ref']);
    $ac_class =  "";
    $checked =  "";
    if( is_array($active_key)){
        if(in_array($each['class_id'], $active_key)){                    
            $checked =  "checked";
            $ac_class =  "class='active'";
        }
    }else if( $each['class_id'] == $active_key){
            $checked =  "checked";
            $ac_class =  "class='active'";
    }

    $result = "<li $ac_class>";
    $result .= "<div class='chkBox'>";
    $result .= "<input type='checkBox' name='category[ ]' $checked value='".$each['class_id']."' id='".$url_ref."'>";
    $result .= "</div>";
    $result .= "<div class='label'>";
    $result .= "<label for='" . $url_ref . "'>".child_sym($level-1).$each['name']."</label>";
    $result .= "</div>";
    $result .= "</li>";
    return $result;
}

function class_list($set_ac_paprams = array(), $type = "category" ){
    $list = load()->lib("ClassList")->run($type);
    if($type != "tag") $list->set_html("", true);
    $list->_print_tag_in_tree = false;
    $list->set_active_param($set_ac_paprams);
    
    $list->set_is_tree(true);
    $list->set_elemets_oper("class_list_func");
    return $list->show_data();
}
//End of Class List


//Author List
function author_list_func($each, $active_key){
    $m = DB::run()->read('book_auth_rel', 'auth_id')->where(array('author_priority' => 1 ))->run_sql();
    $p =-1;
    if($m->get_count()){
        $prs = $m->get_array();   
        foreach($prs as $pr){
            if(in_array($each['auth_id'], $pr)){
                $p = $pr["auth_id"];
            }
        }
    }
    $url_ref = md5($each['auth_url_ref'].$each['auth_id']);
    $ac_class =  "";
    $checked =  "";
    $priority_checked =  "";

    if( is_array($active_key)){
        if(in_array($each['auth_id'], $active_key)){                    
            $checked =  "checked";
            $ac_class =  "class='active'";
            if($each['auth_id'] == $p){
                $priority_checked =  "checked";
            }
        }
    }else if( $each['auth_id'] == $active_key){
        $checked =  "checked";
        $ac_class =  "class='active'";
        $priority_checked =  "checked";
    }

    $result = "<li $ac_class>";
    $result .= "<div class='chkBox'>";
    $result .= "<input type='checkBox'  name='authors[ ]' $checked value='".$each['auth_id']."' id='".$url_ref."'>";
    $result .= "</div>";
    $result .= "<div class='label'>";
    $result .= "<label for='" . $url_ref . "'>".$each['auth_name']."</label>";
    $result .= "</div>";
    $result .= "<div class='main'>";
    $result .= "<input required type='radio' $priority_checked name='main_author' value='".$each['auth_id']."' id='". md5($each['auth_url_ref'])."'>";
    $result .= "<label for='" . md5($each['auth_url_ref']) . "'>Set Main</label>";
    $result .= "</div>";
    $result .= "</li>";
    return $result;
}

function author_list($set_ac_paprams = array()){
    $list = load()->lib("AuthorList")->run();
    
    $list->set_html("");
    
    $list->set_active_param($set_ac_paprams);
    
    $list->set_is_tree(false);
    $list->set_elemets_oper("author_list_func");

    return $list->show_data("auth_id", "DESC");
}

//end of Author List


//News Paper List Table
function news_paper_list_table_func($each){
    $r = "<tr>";
    $r .= "<td>". $each['paper_name'] ."</td>";
    $r .= "<td><a href='". $each['paper_url'] ."' target='_blank'>". $each['paper_url'] ."</a></td>";
    $r .= "<td><a href='".getAdminPath()."/delete/paper-name/". $each['paper_id'] ."?next=".  full_url() ."'>Delete</a></td>";
    $r .= "<td><a href='".getAdminPath()."/edit/paper-name/". $each['paper_id'] ."'>Edit</a></td>";
    $r .= "</tr>";
    return $r;
}

function news_paper_list_table(){
    $list = load()->lib("NewsPaperList")->run();
    $list->setAttrs(array("class"=> "contet-list", "id"=> "contet-list"));
    
    $init = "<tbody>";
    $init .= "<tr>";
    $init .= "<td>Paper Name</td>";
    $init .= "<td>Paper Link</td>";
    $init .= "<td colspan='2'>Action</td>";
    $init .= "</tr>";
    $init .= "</tbody>";
    $list->init_html($init);
    
    $list->set_html("table");
    
    
    $list->set_is_tree(false);
    
    $list->set_elemets_oper("news_paper_list_table_func");
            
    echo "<h1>".$list->list_title()."</h1>";
    $list->show_data("paper_id", "DESC");
    //return htmlentities($l);
}
//End of News Paper List Table

//News Paper List Option

function news_paper_list_options_func($each, $active){
    global $ac_para;
    $r = "<option value='".$each['paper_id']."' " . ( $active == $each['paper_id']? "selected":"" ) . ">". $each['paper_name'] ."</option>";
    return $r;
}

function news_paper_list_options($active_param = null, $init = null){
    echo $active_param;
    $list = load()->lib("NewsPaperList")->run();
    $ac_para = $active_param;
    
    $init = ($init == null) ? "<option value=''>(select)</option>":$init;
    
    $list->init_html($init);
    $list->set_html("");
    $list->set_active_param($active_param);
    $list->set_is_tree(false);
    
    $list->set_elemets_oper("news_paper_list_options_func");
    
    $list->show_data("paper_name", "ASC");
    //return htmlentities($l);
}
//End of News Paper List Option Function

//Home news list
function  news_function ($each, $ac){
        
    $news_page = get_config("news-details/reference");
    $res = '<div class="n-list-item">';
    $res .= '<div class="item-title">';
    $res .= '<a href="'.base_url()."/".$news_page."/".$each['content_id'].'" >'.$each["content_title"].'</a>';
    $res .= '    </div>';
    $res .= '<div class="item-content">';
    $res .= make_excerpt($each["body"]);
    $res .= '</div>';
    $res .= '<div class="item-link">';
    $res .= '<a href="'.base_url()."/".$news_page."/".$each['content_id'].'" >More</a>';
    $res .= '</div>';
    $res .= '</div>';
    return $res;
}

function make_excerpt($str, $limit = 500){
    if(strlen($str) < $limit) return $str .' [...]';
    $str = str_replace("&nbsp;", " ", $str);
    $str = substr(html_entity_decode(strip_tags($str)),0,$limit);
    
    //html_entity_decode($string, true, "utf");
    $arr = explode(' ', $str);
    array_pop($arr);
    array_pop($arr);
    return strip_tags(implode(' ', $arr) . '');
}

function home_news_list($class_url_ref = "", $limit = ""){
    //echo $class_url_ref;
    
    $config = array();
    if($class_url_ref){
        $config['class_url_ref'] =  $class_url_ref;
        $config['class-type'] =  "category";
    }
    $config['content-type'] = "news";
    $config['visibility'] = 1;
    $config['status'] = 'publish';
    
    $n = load()->lib("NewsList")->run($config, $limit);
    
    $n->set_html("");
    if($limit) $n->data_limit($limit);

    $n->set_elemets_oper("news_function");
    return $n;
}

function _get($table, $column, $search_query, $get = null){
    
    if($get){
        $db = DB::run()->read($table, $get)->where($column, $search_query)->run_sql();
    }else {
        $db = DB::run()->read($table)->where($column, $search_query)->run_sql();
    }
    
    echo $db->sql_error();
    
    if($db->get_count()){
        $data_array = (!$get) ? $db->get_array():false;
        $d = $db->get_first();
        return ($get) ? $d[$get]:$data_array;
    }
    return ($get) ? "":array();
}





function news_list($function, $config = array(), $limit = "", $type = "news"){
    //echo $class_url_ref;
    //$config['class_url_ref'] = "";
    
    $config['class-type'] =  "category";
    $config['status'] =  array("!=", "trash");
    
    if(count($config)){
        $n = load()->lib("NewsList")->run($config);
    }else {
        $n = load()->lib("NewsList")->run();
    }
    
    $n->set_html("");
    if($limit) $n->data_limit($limit);

    $n->set_elemets_oper($function);
    
    //echo $n->get_sql();
    
    return $n;
}

function news_trash_list($function, $config = array(), $limit = "", $type = "news"){
    //echo $class_url_ref;
    //$config['class_url_ref'] = "";
    $config['status'] = 'trash';
    
    if(count($config)){
        $n = load()->lib("NewsList")->run($config);
    }else {
        $n = load()->lib("NewsList")->run();
    }
    
    $n->set_html("");
    if($limit) $n->data_limit($limit);

    $n->set_elemets_oper($function);
    
    //echo $n->get_sql();
    
    return $n;
}


function cat_news_list($class_url_ref = "", $limit = ""){
    
    $config = array();

    $config['class_url_ref'] = $class_url_ref;
    $config['class-type'] =  "category";

    $config['content-type'] = "news";
    $config['visibility'] = 1;
    
    $n = load()->lib("NewsList")->run($config, $limit);
    
    $n->set_html("");
    if($limit) $n->data_limit($limit);

    $n->set_elemets_oper("news_function");
    return $n;
}

//End of Home news list

function update_class_counter($class_id = false){
    if(is_array($class_id)){
        foreach ($class_id as $class){
            
            $number = "SELECT count(*) FROM `class_relationship` cr JOIN `content` c WHERE (cr.entity_id = c.content_id) AND cr.class_id = ". $class;
            $number .= " AND c.visibility = 1 AND c.content_type = 'news'";
            
            $rel_sql = "update class_type set count = ($number) WHERE class_id = ". $class;
            $r = DB::run()->setSql($rel_sql)->run_sql();
        }
    }else if(is_numeric($class_id)) {
        
        $number = "SELECT count(*) as total FROM `class_relationship` cr JOIN `content` c WHERE (cr.entity_id = c.content_id) AND cr.class_id = ". $class_id;
        $number .= " AND c.visibility = 1 AND c.content_type = 'news'";
        

        $rel_sql = "update class_type set count = ($number) WHERE class_id = ". $class_id;
        $r = DB::run()->setSql($rel_sql)->run_sql();
    }
}

function id_to_username($id){
    $db = DB::run()->read("user","username")->where("user_id", $id)->limit(1)->run_sql();
    $u = $db->get_first();
    if(count($u)){
        return $u["username"];
    }
    return "";
}


function paper_id_to_name($id){
    $db = DB::run()->read("news_paper_list", "paper_name")->where("paper_id", $id)->limit(1)->run_sql();
    $u = $db->get_first();
    if(count($u)){
        return $u["paper_name"];
    }
    return "";
}

function get_cat_row($id){
    $db = DB::run()->read("class")->where("class_id", $id)->limit(1)->run_sql();
    $u = $db->get_first();
    if(count($u)){
        return $u;
    }
    return false;
}

function get_cat_value($filter_value, $ref, $key = "class_id"){
    
    $db = DB::run()->read("class", $key)->where($ref, $filter_value)->limit(1)->run_sql();
    $u = $db->get_first();
    if(count($u)){
        return $u[$key];
    }
    return "";
}

function has_entity($class_id, &$entities = null, $type = "category"){
    $sql = "SELECT * FROM `class_relationship` cr JOIN `class_type` c WHERE (cr.class_id = c.class_id) ";
    $sql .= "AND  type = '$type' AND cr.class_id = $class_id";
    $en = DB::run()->setSql($sql)->run_sql();

    if($en->get_count()){
        $entities = $en->get_array();
        return true;
    }else {
        $entities = array();
        return false;
    }
}
function has_visible_entity($class_id, &$entities = null ){
    $sql = "SELECT * FROM `class_relationship` cr JOIN content co WHERE (cr.entity_id = co.content_id) ";
    $sql .= "AND cr.class_id = $class_id AND co.visibility = 1";
    $en = DB::run()->setSql($sql)->run_sql();

    if($en->get_count()){
        $entities = $en->get_array();
        return true;
    }else {
        $entities = array();
        return false;
    }
}

function nav_list_func(){
    
}

function get_nav_list(){
    $n = load()->lib("NavList")->run();
}

function __alt($str){
    $codes = array();
    $counter = 0;
    $string = $str;
    while ((strpos($str, "{")+1) && strpos($str, "}")){
        $start = strpos($str, "{");
        $end = strpos($str, "}");
        $length = ($end - $start)-1;
        $code = substr($str, $start+1, $length);
        $codes[] = $code;
        $str = str_replace("{".$code."}", " ", $str);
        if($counter == 20){
            break;
        }
        $counter++;
    }

    foreach ($codes as $c){
        switch ($c){
            case "base_url":
                $string = str_replace("{".$c."}", base_url(), $string);
                break;
            case "cat":
                $string = str_replace("{".$c."}", get_config("category/reference"), $string);
                break;
            case "page":
                $string = str_replace("{".$c."}", get_config("page/reference"), $string);
                break;
            case "news":
                $string = str_replace("{".$c."}", get_config("news-details/reference"), $string);
                break;
            case "tag":
                $string = str_replace("{".$c."}", get_config("tag/reference"), $string);
                break;
            case "author":
                $string = str_replace("{".$c."}", get_config("author/reference"), $string);
                break;
            default :
                $string = str_replace("{".$c."}", "", $string);
        }
    }
    
    return $string;
}


function item_id_to_priority($item_id){
    $n = DB::run()->read("nav_item")->where("item_id", $item_id)->run_sql();
    if($n->get_count()){
        return $n->get_first_obj()->priority;
    }
    return false;
}

function rearange_item($item_id, $to, $parent){

    
    $from = item_id_to_priority($item_id);
    
    if($from < $to){
        $dec_sql = "UPDATE nav_item SET priority = priority-1 WHERE priority <= {$to} AND priority > {$from} AND `parent_id` = ".$parent;
    }else if($from > $to) {
        $dec_sql = "UPDATE nav_item SET priority = priority+1 WHERE priority >= {$to} AND priority < $from AND `parent_id` = ".$parent;
    }
    $upd_sql = "UPDATE nav_item SET priority = {$to} WHERE item_id = {$item_id} AND `parent_id` = ".$parent;
    
    
    if($from != $to){
        $a = DB::run()->setSql($dec_sql)->run_sql();

        if(!$a->error()){
            $b = DB::run()->setSql($upd_sql)->run_sql();
            if(!$b->error()){
                
            }else {
                echo $b->sql_error()."<br>";
                echo $b->get_sql()."<br>";
            }

        }else {
            echo $a->sql_error()."<br>";
            echo $a->get_sql()."<br>";
        }
    }
}

function has_child_nav($id){
    $sql = "SELECT * FROM `nav_item` WHERE `parent_id` = " . $id;
    $en = DB::run()->setSql($sql)->run_sql();
    return ($en->get_count()) ? true:false;
}

function _reposition_nav($parent_id, $class_id){
    $where = array(
        'parent_id'  => $parent_id,
        'class_id'  => $class_id,
    );
    $n = DB::run()->read('nav_item')->where($where)->order_by('priority','asc')->run_sql();
    
    if($n->get_count()){
        $nav = $n->get_array();
        $pr = 1;
        foreach ($nav as $nv){
            $where['item_id'] = $nv['item_id'];
            DB::run()->edit("nav_item")->values(array('priority'=>$pr))->where($where)->run_sql();
            $pr++;
        }
        
    }
}

/************************************************/
/****             Form Functions            *****/
/************************************************/
function view_inputs($key){
    global $inputs;
    if(isset($inputs)){
        if(array_key_exists($key, $inputs)){
            echo $inputs[$key];
        }else {
            echo "";
        }
    }else{
        echo "";
    }
}

function get_inputs($key){
    global $inputs;
    if(isset($inputs)){
        if(array_key_exists($key, $inputs)){
            return $inputs[$key];
        }else {
            return "";
        }
    }else{
        return "";
    }
}

function validate_values($array, $except = array()){
    global $errors;
    global $inputs;

    foreach ($array as $key => $val){
        $inputs[$key] = $val;

        $k = str_replace("_"," ",$key);
        $k = str_replace("-"," ",$k);
        $k = ucfirst($k);

        if(!in_array($key, $except)){
            if(strlen(trim(strip_tags($val))) == 0 ){
                $errors[] = "$k is empty!"; 
            }
        }
    }
}

function view_user_form($show_types = false, $action = "", $class = "") {?>
    <div class="create-user-form <?php echo $class; ?>">
        <div class="message">
        <?php
        $usr_types = get_config("meta_attr/usr/types");
        $public_user_key = get_config("meta_attr/usr/public_user_key");
        global $inputs;
        global $errors;
        global $message;
        if (count($errors)) {
            show_errors($errors);
        }
        if (isset($message)) {
            echo $message;
        }
        ?>
        </div>
        <form action="<?php echo $action; ?>" method="post"> 
            <h2 class="main-body-page-header">Create an User</h2>
            <div class="form-inputs">
                <label for="first_name">First Name</label>
                <input type="text" id="first_name" value="<?php view_inputs("first_name") ?>" required name="first_name">
            </div>
            <div class="form-inputs">
                <label for="last_name">Last name</label>
                <input type="text" id="last_name" value="<?php view_inputs("last_name") ?>" name="last_name">
            </div>
            <div class="form-inputs">
                <label for="username">Type a username*</label>
                <input type="text" id="username" value="<?php view_inputs("username") ?>" name="username" required>
            </div>

            <div class="form-inputs">
                <label for="email">Type a email address*</label>
                <input type="email" id="email"  value="<?php view_inputs("email") ?>"  name="email" required>
            </div>
            <div class="form-inputs">
                <label for="confirm-email">Confirm email address*</label>
                <input type="email" id="confirm-email"  value="<?php view_inputs("confirm-email") ?>" name="confirm-email" required>
            </div>
            <div class="form-inputs">
                <label for="password">Type a Password*</label>
                <input type="password" id="password"  value="<?php view_inputs("password") ?>"  name="password"  required>
            </div>
            <div class="form-inputs">
                <label for="confirm-password">Confirm password*</label>
                <input type="password" id="confirm-password" value="<?php view_inputs("confirm-password") ?>"  name="confirm-password" required>
            </div>

            <?php if ($show_types): ?>
            <div class="form-inputs">
                <label for="user-type">User Role*</label>
                <select name="user-type">
                    <option value="">select</option>
            <?php foreach ($usr_types as $type): ?>
                    <option value="<?php echo strtolower($type); ?>" <?php echo ($type == $inputs['user-type']) ? "selected" : "" ?>><?php echo ucfirst($type) ?></option>
            <?php endforeach; ?>
                </select>
            </div>
            <?php else: ?>
                <input type="hidden" value="<?php echo $public_user_key ?>" name="user-type" >
            <?php endif; ?>

            <div class="form-inputs">
                <input type="submit" name="submit-button" value="Registraton">
            </div>
        </form>
    </div>
<?php
}

function operate_user_form($success_redirect = ""){
    global $inputs;
    global $errors;
    global $message;

    $usr_types = get_config("meta_attr/usr/types");    
    $fn_key = get_config("meta_attr/usr/fname_key");
    $ln_key = get_config("meta_attr/usr/lname_key");
    $ut_key = get_config("meta_attr/usr/utype_key");

    if(isset($_POST) && !empty($_POST)){
        validate_values($_POST, array('first_name', 'last_name'));

        if(!count($errors)){
            if($inputs["password"] == $inputs["confirm-password"]){
                if(strip_tags(trim($inputs["email"])) == strip_tags(trim($inputs["confirm-email"])) ){

                    $values['username'] = strip_tags(trim($inputs['username']));
                    $values['email'] = strip_tags(trim($inputs['email']));
                    $values['password'] = $inputs['password'];
                    $first_name = strip_tags(trim($inputs['first_name']));
                    $last_name = strip_tags(trim($inputs['last_name']));
                    $values['display_name'] = $first_name." ".$last_name;
                    
                    $user = load()->sys("User")->run();


                    if( $user->create($values,$message, $id) ){
                    //if( true ){
                        
                        $user_meta = load()->sys("UserInfo")->run($id);
                        
                        $message = $inputs['first_name'] ." Successfully inserted!";

                        $user_meta->add($fn_key, $inputs['first_name'] );
                        $user_meta->add($ln_key, $inputs['last_name'] );
                        $user_meta->add($ut_key, $inputs['user-type'] );
                        
                        load()->sys('Capabilities')->open();
                        
                        $capabilities = new Capabilities(0);
                        $capabilities = new Capabilities($id);
                        
                        if(strtoupper($inputs['user-type']) === "ADMIN"){
                            if($capabilities->add(0)){
                                if($success_redirect !== ""){
                                    redirect($success_redirect);
                                }
                            }
                        }else {
                            if($user->isAdmin()){
                            //echo $inputs['user-type']
                            //$capabilities->add(0);
                                echo "Yes You are admin!<br>";
                                
                                _add_capabilities($inputs['user-type'], $id);
                                
                            } 
                        }
                    }
                }else {
                    $errors[] = "Email dosen't match!";
                }
            }else {
                $errors[] = "Password dosen't match!";
            }
        }   
    }
}

/*
$config['cap']['add_news'] = 'an';
$config['cap']['edit_news'] = 'en';
$config['cap']['add_category'] = 'ac';
$config['cap']['edit_category'] = 'ec';
$config['cap']['add_nav'] = 'anv';
$config['cap']['edit_nav'] = 'env';
$config['cap']['edit_section'] = 'es';
$config['cap']['edit_theme'] = 'et';
$config['cap']['edit_setting'] = 'es';

$config['cap']['add_page'] = 'ap';
$config['cap']['edit_page'] = 'ep';
$config['cap']['submit_news'] = 'sun';
$config['cap']['approve_news'] = 'an';
$config['cap']['edit_self_user_setting'] = 'esus';
*/

function _add_capabilities($user_type, $user_id){
    load()->sys('Capabilities')->open();
    
    $a = new Capabilities($user_id);
    global $user;
    if($user->isAdmin()){
        $caps = array();
        switch ($user_type){
            case "reporter":
                $caps = array('submit_news', 'edit_self_user_setting', 
                    'add_category', 'edit_category', 'add_page');
                break;
            case "editor":
                $caps = array('edit_news', 'add_news', 'add_category', 'edit_category', 'add_page', 'edit_self_user_setting');
                break;
            default :
                echo "Default!";
        }

        foreach ($caps as $c){
            if($a->add($c, "yes")){
                echo $c . " added!<br>";
            }else {
                echo $c . " not added!<br>";
            }
        }    
    }
}


/************************************************/
/****         End of Form Functions         *****/
/************************************************/



function login_form_operation($next = "", $class = "", $action = ""){
    $inputs = array();
    $errors = array();
    global $errors;
    global $inputs;
    validate_values($_POST);
    view_inputs('remember');
    $remember = false;
    if(isset($inputs) && !empty($inputs)){
        if(array_key_exists("remember", $inputs)){
            $chk = ($inputs['remember'] === "on") ? "checked":"";
            $remember = $inputs['remember'] == "on" ? true:false;
        }else {
            $chk = "";
        }
    }else {
        $chk = "";
    }

    if(isset($_POST) && !empty($_POST)){
        if(!count($errors)){
            $username = $inputs['username'];
            $password = $inputs['password'];

            $user = load()->sys("user")->run($username);

            if($user->exists()){
                $login = $user->login($username, $password, $remember);

                if($login){

                    echo "Logoed in successfully!";
                    redirect($next);

                }else {
                    echo "Log In Failed!";
                }
            }else {
                echo "User not found!";
            }

        }else {
        }
    }
}

function show_login_form($title, $action = "", $class = "", $u_label = "U", $p_label  = "P"){ 
    global $errors;
    global $inputs;

    if(isset($inputs) && !empty($inputs)){
        if(array_key_exists("remember", $inputs)){
            $chk = ($inputs['remember'] === "on") ? "checked":"";
        }else {
            $chk = "";
        }
    }else {
        $chk = "";                        
    }
    ?>

    <div class="login-in <?php echo $class; ?>">
        <form class="login" method="post" action="" autocomplete="off">
            <h1><?php echo $title ?></h1>
            <div class="inputs">
                <label for="username"><?php echo $u_label; ?></label>
                <input type="text" name="username"  value="<?php view_inputs('username')?>"  class="username" id="username" placeholder="Username">
            </div>
            <div class="inputs">
                <label for="password"><?php echo $p_label; ?></label>
                <input type="password" name="password" class="password" value="<?php view_inputs('password')?>" id="password" placeholder="Password">
            </div>
            <div class="bottom">
                <input type="checkbox" name="remember" class="remember" <?php echo $chk; ?> id="remember">
                <label for="remember">Remember Me</label>
                <input type="submit" class="login-submit" value="Login &raquo;" />
            </div>
        </form>
    </div>
    <?php }

/************************************************/
/****         End of Login Form Functions   *****/
/************************************************/