<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function Explain($exp, $type = 'print'){
    echo  "<tt><pre>";
    switch($type){
        case 'print':
            print_r($exp);
        break;
        case 'dump':
            var_dump($exp);
        break;
        case 'export':
            var_export($exp);
        break;
    }
    echo  "</pre></tt>";
}