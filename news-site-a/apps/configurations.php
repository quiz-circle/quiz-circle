<?php



$config = array();

$config['site']['admin'] = 'control-panel';
//$config['site']['default'] = 'test-html';
$config['site']['default'] = 'news-b';
$config['category']['reference'] = "subject";
$config['page']['reference'] = "p";
$config['tag']['reference'] = "tag";
$config['news-details']['reference'] = "single";
$config['author']['reference'] = "writer";


$config['db-config']['path'] = ABSPATH . "config.php";
$config['books']['root-path'] = "books";

$config['session']['session-name'] = '_user_session';
$config['remember']['cookie-name'] = '_login_cookie';
$config['remember']['cookie-value'] = '_cookie_value';
$config['remember']['cookie_expire'] = 3600*24*7;

$config['encryption']['array-spliter'] = '$%^';

$config["meta_attr"]["usr"]["fname_key"] = "first_name";
$config["meta_attr"]["usr"]["lname_key"] = "last_name";
$config["meta_attr"]["usr"]["utype_key"] = "user_type";
$config["meta_attr"]["usr"]["sess_name_key"] = "_session_token_value";
$config["meta_attr"]["usr"]["admin_user_key"] = "admin";
$config["meta_attr"]["usr"]["public_user_key"] = "reader";
$config["meta_attr"]["usr"]["cap"] = "_user_capabilities";
$config["meta_attr"]["usr"]["types"] = array($config["meta_attr"]["usr"]["admin_user_key"], 
    "editor",
    "uploader", 
    "reporter", 
    $config["meta_attr"]["usr"]["public_user_key"]);

$config["meta_attr"]["headlines_class_key"] = "headlines_class";
$config["meta_attr"]["spcial_news_class_key"] = "spcial_news_class";

$config["meta_attr"]["section_key"] = "news_sections";
$config["meta_attr"]["section_key_right"] = "news_sections_right";

$config["meta_attr"]["home-sidebar"]["educationist"] = "sidebar-educationist";
$config["meta_attr"]["home-sidebar"]["organigation"] = "sidebar-organigation";
$config["meta_attr"]["home-sidebar"]["journalist"] = "sidebar-journalist";
$config["meta_attr"]["home-sidebar"]["schoalrship"] = "sidebar-schoalrship";
$config["meta_attr"]["home-sidebar"]["university"] = "sidebar-university";
$config["meta_attr"]["home-sidebar"]["ministar-gallery"] = "minister-gellery";

$config['paper-logo']['path'] = "uploads/news-paper-logo";

$config['cap']['add_news'] = 'an';
$config['cap']['edit_news'] = 'en';
$config['cap']['add_category'] = 'ac';
$config['cap']['edit_category'] = 'ec';
$config['cap']['add_nav'] = 'anv';
$config['cap']['edit_nav'] = 'env';
$config['cap']['edit_section'] = 'es';
$config['cap']['edit_theme'] = 'et';
$config['cap']['edit_setting'] = 'es';

$config['cap']['add_page'] = 'ap';
$config['cap']['edit_page'] = 'ep';
$config['cap']['submit_news'] = 'sun';
$config['cap']['approve_news'] = 'an';
$config['cap']['edit_self_user_setting'] = 'esus';