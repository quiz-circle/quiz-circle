<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once ABSPATH .'root/core/system/ContentList.php';
/**
 * Description of AuthorList
 *
 * @author Saif
 */
class NewsList extends ContentList {
    private $_config = array();
    public function __construct($config = array('class-type' => "category"), $columns = "") {
        $classes = array();
        $this->_config = $config;
        $a = $config;
        /*
        unset($a["class-type"]);
        $has_config = count($config);
        $url_ref = $this->config("class_url_ref");
        $class_type = ($this->config("class-type")) ? $this->config("class-type"):"category";
        */
        
        $class_id = 0;
        
        $class_name = isset($confs["class_name"]) ? $confs["class_name"]:"";
        $class_url_ref = $this->config("class_url_ref");
        $class_type = isset($confs["category"])? $confs["category"]:isset($confs["class-type"]) ? $confs["class-type"]:isset($confs["tag"]) ?$confs["tag"]:"";
        
        if($class_url_ref){
            $class_id = (int) _get('class', 'url_ref', $class_url_ref, 'class_id');
        }
        $sql = "SELECT ".($columns ? $columns:"*")." FROM `content` co ";
        $sql .= " JOIN class c JOIN class_relationship cr  ";
        $sql .= " JOIN class_type ct ";
        $sql .= " WHERE (cr.entity_id = co.content_id ) AND (c.class_id = cr.class_id) AND (c.class_id = ct.class_id)";
        $sql .= ($class_id) ? " AND c.class_id = {$class_id} ":" ";
        $sql .= ($class_type) ? " AND ct.type = {$class_type} ":" ";
        $sql .= ($class_name) ? " AND c.name = '{$class_name}' ":" ";
        
        $sql .= $this->search_criteria();
        $sql .= " group by co.content_title ";
        
        parent::__construct($sql);
    }
    
    private function search_criteria(){
        $confs =  $this->_config;
        $sql_ext = "";
        
        unset($confs['class-type']);
        unset($confs['category']);
        unset($confs['tag']);
        unset($confs['class_url_ref']);

        foreach($confs as $k => $c) {
            $k = rtrim($k, "!@#$1");
            $k = rtrim($k, "!@#$2");
            $k = rtrim($k, "!@#$3");
            $k = rtrim($k, "!@#$4");
            $k = rtrim($k, "!@#$5");
            $k = rtrim($k, "!@#$6");
            $value = $c;
            $operator = "=";
            if(is_array($c)){
                if(count($c)  == 2){
                    $operator = $c[0];
                    $value = "'".$c[1]."'";
                }else {
                    continue;
                }
            }else {
                $value = !is_numeric($value) ? "'$value'":$value;
            }
            $sql_ext .=  " AND ";
            $sql_ext .= "".str_replace("-", "_", $k) ." ".$operator." " . $value;
        }
        return $sql_ext;
    }
    
    private function config($path){
        if ($path) {
            $config = $this->_config;
            $path = explode('/', $path);
            
            foreach ($path as $p) {
                if (isset($config[$p])) {
                    $config = $config[$p];
                }else {
                    return false;
                }
            }
            return $config;
        }
        return false;
    }

    private function make_class_range($class_list){
        $sql = "";
        $c = 0;
        if(count($class_list)){
            $sql  .= "AND co.content_id IN (";
                foreach ($class_list as $class){
                    $sql .= ($c != 0) ? ", ":"";
                    $sql .= $class['entity_id'];
                    $c++;
                }
            $sql .= ")";
        }
        return $sql;
    }
        
    /*
    public function show_data($order_by = "", $order_type = "") {
        parent::show_data($order_by, $order_type);
    }
     * 
     */
    
}
