<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once ABSPATH .'root/core/system/ContentList.php';
/**
 * Description of AuthorList
 *
 * @author Saif
 */
class NavList extends ContentList {
    //private $_type;
    public function __construct($class_id) {
        //$type = mysql_real_escape_string($class_id);
        $id = "";
        if(!is_numeric($class_id)){
            $c = DB::run()->read("class")->extra("NATURAL JOIN class_type")->where("name", $class_id)->run_sql();
            $id = $c->get_first_obj()->class_id;
        }else {
            $id = $class_id;
        }
        $sql = "SELECT * FROM `nav_item` WHERE class_id='$id'";
        parent::__construct( $sql, "item_id", "parent_id");
    }
    /*
    public function list_title(){
        return "Showing $this->_type";
    }
    
    public function list_sub_title(){
        return "Total ".$this->get_total()." $this->_type" . ($this->get_total() > 1 ? "es":"");
    }
    
    public function show_data($order_by = "", $order_type = "") {
        parent::show_data($order_by, $order_type);
    }
    */
}
