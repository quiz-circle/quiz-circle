<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once ABSPATH .'root/core/system/ContentList.php';
/**
 * Description of AuthorList
 *
 * @author Saif
 */
class BootstrapNav extends ContentList {
    public $boot_tag_start, $boot_tag_end;
    
    //private $_type;
    public function __construct($class_id) {
        $this->tag = "ul";
        $this->_print_tag_in_tree = true;
        $this->li_tag_inside = true;
        $this->tag_start = "";
        $this->tag_end  = "";
        
        $this->boot_tag_start = "<ul class='dropdown-menu'>";
        $this->boot_tag_end  = "</ul>";
        
        $this->in_tag_start = "<li>";
        $this->in_tag_end = "</li>";
       
        
        //$type = mysql_real_escape_string($class_id);
        $id = "";
        if(!is_numeric($class_id)){
            $c = DB::run()->read("class")->extra("NATURAL JOIN class_type")->where("name", $class_id)->run_sql();
            $id = $c->get_first_obj()->class_id;
        }else {
            $id = $class_id;
        }
        $sql = "SELECT * FROM `nav_item` WHERE class_id='$id'";
        parent::__construct( $sql, "item_id", "parent_id");
    }
    
    protected function set_active_li($arr, &$html_list) {
        
        if ($this->_print_tag_in_tree){

            $inside_tag = rtrim($this->in_tag_start, ">");
            if($this->hasChild($arr['item_id'])){
                $inside_tag .= " class='dropdown' ";   
            }
            if (array_key_exists($this->active_key, $arr)) {
                $inside_tag .= ($arr[$this->active_key] == $this->active_param ) ? " class='active'" : "";
            }
            
            $html_list = true;
            $inside_tag .= ">";
            return $inside_tag;
        }
        return "";
    }
    
    protected  function make_list_tree($root = 0, $n = 1) {
        
        if(is_array($this->attributes)){
            if(count($this->attributes)){
                foreach($this->attributes as $attr=> $value){
                    $attrs .= $attr ."=\"" .$value. "\" ";
                }
            }
        }
        
        
        $func = $this->_per_elem_operation;
        if ($this->_total_elem && $func) {
            $inside_tag = "";
            if ($this->_print_tag_in_tree && $root !== 0){
                $this->_result .= $this->boot_tag_start;
            }
            
            $html_list = false;
            
            //Explain($this->_data);

            foreach ($this->_data as $key => $val) {
                
                if (array_key_exists($this->_tree_params[0], $val)) {
                    if (count($this->_tree_params) == 2) {
                        if ($val[$this->_tree_params[1]] == $root) {
                            if($this->li_tag_inside){
                                $this->_result .= $this->set_active_li($val, $html_list);
                            }
                            $this->_result .= $func($val, $this->active_param, $this);
                            
                            if ($this->hasChild($val[$this->_tree_params[0]])) {
                                $this->make_list_tree($val[$this->_tree_params[0]], $n);
                            }
                            if($this->li_tag_inside){
                                $this->_result .= ($html_list) ? $this->in_tag_end : "";
                            }
                        }
                    }
                }

            }
           
            if ($this->_print_tag_in_tree && $root != 0){
                $this->_result .= $this->boot_tag_end;
            }
        }
    }
    
    /*
    public function list_title(){
        return "Showing $this->_type";
    }
    
    public function list_sub_title(){
        return "Total ".$this->get_total()." $this->_type" . ($this->get_total() > 1 ? "es":"");
    }
    
    public function show_data($order_by = "", $order_type = "") {
        parent::show_data($order_by, $order_type);
    }
    */
}
