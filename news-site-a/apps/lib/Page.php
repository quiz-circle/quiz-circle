<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

load()->lib("page_configuration")->open();

require_once ABSPATH .'root/core/system/page_configuration.php';

class Page extends page_configuration {
    private  $_content; 
    public function __construct($url_segment = 0) {
        parent::__construct($url_segment);
        $this->_content = parent::get_content();
    }
    
    public function print_content(){
        if($this->has_content_exists()){
            if($this->has_template){
                $include_path = "/pages";
            }else {
                $include_path = "/";
            }
            $page_content = $this->_content;
            $path = ABSPATH ."themes/".get_theme_name(). $include_path."/".$this->page_file_name;
            if(file_exists($path)){                
                include $path;
                return true;
            }else {
                return false;
            }
        }else {
            return false;
        }
    }
    
    public function page_filename() {
        return $this->page_file_name;
    }
}