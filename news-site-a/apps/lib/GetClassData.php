<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


require_once ABSPATH .'root/core/system/Tree.php';
/**
 * Description of TreeUL
 *
 * @author Saif
 */

class GetClassData extends Tree {
    
    private $result, $type = "", 
            $set_row, 
            $tag_start = "", 
            $tag_end = "", 
            $attributes, 
            $is_tree, 
            $init_html = null, 
            $active_id; 
    
   
    public function setAttrs($attr){
        $this->attributes = $attr;
        return $this;
    }
    public function setTag($type = "ul"){
        $attr = $this->attributes;
        $this->type = $type;
        switch($type){
            case "ul":
                $this->tag_start = "<ul ".$attr.">";
                $this->tag_end = "</ul>";
                break;
            case "ol":
                $this->tag_start = "<ol ".$attr.">";
                $this->tag_end = "</ol>";
                break;
            case "":
                $this->tag_start = "";
                $this->tag_end = "";
                break;
            default :                 
                $this->tag_start = "<".$type." ".$attr.">";
                $this->tag_end = "</".$type.">";

        }
        
        return $this;
    }
    
    public function set_row($func){
        $this->row_set = $func;
        return $this;
    }
    
    public function init_html($data = null){
        $this->init_html = $data;
        return $this;
    }
    
    public function set_active($id){
        $this->active_id = $id;
        return $this;
    } 
    
    protected function tree($root = 0){
        $tp = $this->type;
        $func = $this->row_set;
        if($tp == "ul" || $tp == "ol"){
            $this->result .= ($this->is_tree) ? $this->tag_start:"";
        }
        if($this->_total){
            foreach($this->_data as $key => $val ){
                if($val['parent'] == $root){
                    $this->result .= $func($val, $this->active_id);
                    if($this->hasChild($val['class_id'])){
                        $this->tree($val['class_id']);
                    }
                }
            }
        }
        if($tp == "ul" || $tp == "ol"){
            $this->result .= ($this->is_tree) ? $this->tag_end:"";
        }
    }
    
    public function get($is_tree = true){
        $this->is_tree = $is_tree;
        
        if(!$is_tree && ($this->type == "ul" || $this->type == "ol")){
            if($this->type == "ul" || $this->type == "ol"){
                $this->result .= $this->tag_start;
                    $this->tree();
                $this->result .= $this->tag_end;
            }
        }else {
            $func = $this->init_html;
            $this->result .= $this->tag_start;
                $this->result .= $func != null ? $func:"";
                $this->tree();
            $this->result .=  $this->tag_end;
        }
        return $this->result;
        
    }
}
