<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Validate
 *
 * @author Saif
 */
class Validatation {
    //put your code here
    
    private $_passed = false,
            $_errors = array(),
            $_db = null;
	
    public function __construct(){
        $this->_db = DB::run();
    }

    public function check($source, $items = array()){
        foreach($items as $item => $rules){
            foreach($rules as $rule => $rule_value){
                $value = trim($source[$item]);
                if($rule === 'requered' && empty($value)){
                    $this->addError($item . " is requered",$rule.'-'.$item);
                }else if(!empty($value)){
                    switch($rule){
                        case 'min':
                            if(strlen($value) <  $rule_value){
                                $this->addError("{$item} must be a minium of {$rule_value} characters.", $rule.'-'.$item);
                            }
                        break;
                        case 'max':
                            if(strlen($value) > $rule_value){
                                $this->addError("{$item} must be a maxium of {$rule_value} characters.", $rule.'-'.$item);
                            }
                        break;
                        case 'matches':
                            if($value != $source[$rule_value]){
                                $this->addError("{$item} must match {$rule_value}", $rule.'-'.$item);
                            }
                        break;

                        case 'unique':
                        $check = $this->_db->read($rule_value)->where($item , $value)->run_sql();
                        if($check->get_count()){
                            $this->addError("{$item} already exists", $rule.'-'.$item);
                        }
                        break;
                    }
                }
            }
        }

        if(empty($this->_errors)){
            $this->_passed = true;
        }
        return $this;
    }

    private function addError($error, $name){
        $this->_errors[$name] = $error;
    }

    public function errors(){
        return $this->_errors;
    }

    public function passed(){
        return $this->_passed;
    }
}
