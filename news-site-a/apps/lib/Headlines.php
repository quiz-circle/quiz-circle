<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once ABSPATH .'root/core/system/ContentList.php';


class Headlines extends ContentList {
    public function __construct($top = false) {
        $sql = "SELECT * FROM content NATURAL JOIN headlines WHERE `head_priority`";
        $sql .= (!$top) ? " != 0":" = 0";
        
        parent::__construct($sql);
    }
    
}