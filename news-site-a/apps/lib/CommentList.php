<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once ABSPATH .'root/core/system/ContentList.php';
/**
 * Description of AuthorList
 *
 * @author Saif
 */
class CommentList extends ContentList {
    //private $_type;
    public function __construct($content_id = 0, $config = "", $column = "*", $sql_extra = "") {
        if(is_array($config)){
            if($content_id !== 0){
                unset($config['content_id']);
            }
        }
        
        
        if($content_id !== 0){
            $config['content_id'] = $content_id;
        }
        
        $c = DB::run()->read("comment", $column)->extra($sql_extra)->where($config);
        
        $sql = $c->get_sql();

        //$sql = "SELECT * FROM `nav_item` WHERE class_id='$id'";
        parent::__construct( $sql, "comment_id", "parent");
    }
    /*
    public function list_title(){
        return "Showing $this->_type";
    }
    
    public function list_sub_title(){
        return "Total ".$this->get_total()." $this->_type" . ($this->get_total() > 1 ? "es":"");
    }
    
    public function show_data($order_by = "", $order_type = "") {
        parent::show_data($order_by, $order_type);
    }
    */
}
