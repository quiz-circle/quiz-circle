<?php

/*
 * Code Owner: Saif Uddin ( https://facebook.com/saifuddin24 )
 */

/*
 *  Description of AuthorList
 *  This is  Class for listing the Class
 *  @author Saif
 */

require_once ABSPATH .'root/core/system/ContentList.php';

class ClassList extends ContentList {
    //Declaration of class type e.g. category, tag
    private $_type;
    
    //Constructor class 
    public function __construct($type = "category") {
        $this->_type = $type;
        //$type = mysqli_real_escape_string($type);
        $sql = "SELECT * FROM `class` NATURAL JOIN `class_type` WHERE type='$type' order by class_id DESC";
        parent::__construct($sql, "class_id", "parent");
    }
    
    //
    public function list_title(){
        return "Showing $this->_type";
    }
    
    public function list_sub_title(){
        echo "Strpos of".$this->_type." ".strpos($this->_type, "s");
        
        return "Total ".$this->get_total()." $this->_type" . ($this->get_total() > 1 ? "es":"");
    }
    
    public function show_data($order_by = "", $order_type = "") {
        parent::show_data($order_by, $order_type);
    }
}
