<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HTML
 *
 * @author Saif
 */
class HTML {
    public  $element,
            $attribute = array(),
            $inside,
            $closing;
    
    public  function __construct($element){
        
        $element = isset($element) ? $element:'</div>';
        
        $element = ltrim($element,'<');
        $element = rtrim($element, '>');
        
        if($element[0] == '/'){
            $element =  ltrim($element, '/');
            $this->closing = true;
        }else {
            $this->closing = false;
        }
        $this->element = $element;
    }
    
    public function addAttr($attr, $value = null){
        $attribute = array($attr, $value);
        $this->attribute[] = $attribute;
    }
    
    public function inside($value){
        $this->inside = $value;
    }
    
    public function getInside(){
        return ($this->clossing()) ? $this->inside:'';
    }
    
    public function clossing(){
        return $this->closing;
    }
    
    public function getAttributes(){
        $result = '';
        if(count($this->attribute)){    
            foreach($this->attribute as  $attr){
                $result .= ' '.$attr[0]; 
                $result .=  (isset($attr[1])) ? '="'.$attr[1].'" ':'';
            }
            return $result;
            }
        return null;
    }
    
    
    public static function make($tag, $inside, $attrs = array()){
        
        $html = new HTML($tag);
        if(count($attrs)){
            foreach($attrs as $at){
                $atts = explode('=', $at);
                $arg1 = $atts[0]. ' ';
                $arg2 = isset($atts[1]) ? $atts[1]:null;
                
                if(count($atts) > 2){
                    $arg2 = null;
                    for($i=1; $i < (count($atts)); $i++){
                        $arg2 .= (($i != 1)?'=':'').$atts[$i]; 
                    }
                }
                $html->addAttr($arg1, $arg2);
            }
        }
        $html->inside($inside);
        
        $result = '';
        $result .= $html->start();
        $result .= $html->getInside();
        $result .= $html->close();
        return $result;
    }
    
    public function start(){
        $result     =   '<'  . $this->element. $this->getAttributes();  
        $result     .=      ($this->clossing()) ? '>':'/>'; 
        
        return $result;
    }
    
    public function close(){
        return ($this->clossing()) ? '</'.$this->element.'>':'';
    }
}
