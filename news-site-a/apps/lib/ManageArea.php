<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ManageArea
 *
 * @author Saif
 */
class ManageArea{
    private $_divisions,
            $_division_id,
            $_district_id,
            $_thana_id,
            $_base_url,
            $_dis_enabled,
            $_thana_disabled;
    public $div_selected, $dis_selected, $thana_selected, $dis_default, $thana_default;
    
    
    public function __construct($div_selected = false, $dis_selected = false, $thana_selected = false) {
        $this->_divisions = get_divisions();
        $this->_base_url = base_url();
        $this->div_selected = $div_selected;
        $this->dis_selected = $dis_selected;
        $this->thana_selected = $thana_selected;
    }
    
    private function _bindOthers($values){
        if(is_array($values) || $values === null){
            $result = "";
            foreach($values as $key => $value){
                $result .= (!is_numeric($key)) ?" ".$key ."=\"".$value."\" ":" ".$value;
            }
            return $result;
        }
        return "";
    }
    
    private function _setDivSelected($value){
        if($this->div_selected) {
           if(is_numeric($this->div_selected)){
               
               echo ($this->div_selected == $value['area_id']) ? "selected":"";
           }else {
               echo (!strcasecmp($this->div_selected, $value['name'])) ? "selected":"";
           }
        }
    }
    
    public function show_division_form($form_name, $id, $others = "", $detault = ""){
        
        $divs = $this->_divisions;
        
        $this->_division_id = $id;
        
        $detault = !empty($detault) ? $detault:"Select";
        
        ?>
        <select name="<?php echo $form_name  ?>" id="<?php echo $id?>" <?php echo $this->_bindOthers($others)?> >
            <option value=""><?php echo $detault; ?></option>
            <?php foreach ($divs as $div): ?>
            <option value="<?php echo $div['area_id'] ?>" <?php $this->_setDivSelected($div) ?> ><?php echo $div["name"] ?></option>
            <?php endforeach; ?>
        </select>
        <?php
    }
    
    public function show_district_form($form_name, $id, $others = "", $Disabled = false, $default = ""){
        
        $this->dis_default = $default = (!empty($default)) ? $default:"Select";
        
        $this->_district_id = $id;
        $this->_dis_disabled = $Disabled;
        ?>
        <select name="<?php echo $form_name  ?>" id="<?php echo $id?>" <?php echo $this->_bindOthers($others)?> >
            <option value=""><?php echo $default; ?></option>
        </select>
        <?php
    }
    
    public function show_thana_form($form_name, $id, $others = "", $default = ""){
        $this->thana_default = $default = (!empty($default)) ? $default:"Select";
        $this->_thana_id = $id;
        ?>
        <select name="<?php echo $form_name  ?>" id="<?php echo $id?>" <?php echo $this->_bindOthers($others)?> >
            <option value=""><?php echo $default; ?></option>
        </select>
        <?php
    }
    
    public function __destruct() {
        
        $ajax_url = $this->_base_url."/themes/".get_theme_name()."/ajax/getArea.php";
        
        ?>
        <script>
            var dis_selected = "<?php echo isset($this->dis_selected) ? $this->dis_selected:""; ?>";
            var thana_selected = "<?php echo isset($this->thana_selected) ? $this->thana_selected:"";?>";
            var division_id= "<?php echo  isset($this->_division_id) ? $this->_division_id:""; ?>";
            var district_id= "<?php echo  isset($this->_district_id) ? $this->_district_id:"" ?>";
            var thana_id= "<?php echo  isset($this->_thana_id) ? $this->_thana_id:"" ?>";
            var dis_enabled = "<?php echo isset($this->_dis_disabled) ? $this->_dis_disabled:"" ?>";
            var dis_default = "<?php echo $this->dis_default?>";
            var thana_default = "<?php echo $this->thana_default?>";
            
            var selector = $("#"+division_id+", #"+district_id);
            var district = $("#"+district_id);
            var thana = $("#"+thana_id);
            
            var default_name = "Select";
            
            selector.on("change", changeArea);            
            $(window).on("load", changeArea(division_id, function(){
                changeArea(district_id);
            }));
            
            function changeArea(di, callback){
                var ID, ar_id;
                
                if(typeof di.type === "undefined"){                    
                        ID  = di;
                        var ar_id = $("#"+ID).val();
                    var eventtype = false;
                }else {
                    var eventtype = di.type;
                    if(di.type == "change"){
                        dis_selected = "";
                        thana_selected = "";
                        ID = this.id;
                        var ar_id = $(this).val();
                    }
                }

                var change_id = "";
                var type  = "";
                switch(ID){
                    case division_id:
                        type = division_id;
                        change_id = district_id;
                        var d = ( thana_default.length !== 0) ? thana_default:default_name; 
                        var result = '<option value="">'+d+'</option>';
                        
                        $("#"+thana_id).html(result);
                    break;
                    case district_id:
                        type = district_id;
                        change_id = thana_id;
                    break;
                    default :
                }
                $("#"+change_id).prop("disabled", true);
               
                
                $.getJSON("<?php echo $ajax_url;  ?>",{"area_id":ar_id, "area_type": type},
                    function(res){
                        bindSelect(res, change_id);
                        
                        if(!eventtype)
                        $("#"+change_id).prop("disabled", dis_enabled);
                        else 
                        $("#"+change_id).prop("disabled", false);
                        
                        if(change_id === district_id && !eventtype) callback();
                       
                    }
                );
            }
            
            
            function setSelected(vals, type){                    
                switch(type){
                    case district_id :
                        var check = dis_selected;
                        break;
                    case thana_id :
                        var check = thana_selected;
                        break;
                    default :
                        var check = "";
                }
                if($.isNumeric(check)){
                    return (check == vals.area_id) ? "selected":"";
                }else {
                    return (check == vals.name) ? "selected":"";
                }
            }
            
            function bindSelect(values, selecter, selected){
                var selec = $("#"+selecter);
                
                
                if(selecter === district_id ){
                    default_name = ( dis_default.length !== 0) ? dis_default:default_name;
                }else if(selecter === thana_id) {
                    default_name = ( thana_default.length !== 0) ? thana_default:default_name;
                }else {
                    default_name = "Select";
                }
                console.log(dis_default);
                
                var result = "";
                    result += '<option value="">'+default_name+'</option>';
                    
                    
                if(values.length !== 0){
                    for (var prop in values){    
                        result += '<option value="'+values[prop].area_id +'" ';
                        result += setSelected(values[prop], selecter);
                        result += ' >'+values[prop].name+'</option>';

                    }
                }
                selec.html(result);
            }
            
        </script>
        <?php

    }
    
    public function run_script(){
    }
}