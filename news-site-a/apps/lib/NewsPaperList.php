<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once ABSPATH .'root/core/system/ContentList.php';
/**
 * Description of AuthorList
 *
 * @author Saif
 */
class NewsPaperList extends ContentList {

    public function __construct() {
        $sql = "SELECT * FROM `news_paper_list`";        
        parent::__construct($sql);
    }
    
    public function list_title(){
        return $this->get_total()." News Paper" . ($this->get_total() > 1 ? "s":""). " listed!";
    }
    
    public function list_sub_title(){
        return "Page ".$this->current_page()." of ".$this->total_pages();
    }
    
    public function show_data($order_by = "", $order_type = "") {
        parent::show_data($order_by, $order_type);
    }
}
