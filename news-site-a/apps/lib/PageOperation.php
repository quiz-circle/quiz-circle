<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PageOperation
 *
 * @author Saif
 */


require_once ABSPATH .'root/core/system/page_configuration.php';

class PageOperation extends page_configuration {
    //put your code here
    
    public function __construct($page_id = 0) {
        parent::__construct("", $page_id);
    }
    
    public function get_templates(){
        if($this->_template_list()){
            return $this->templates;
        }
    }
}


