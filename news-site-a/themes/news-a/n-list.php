<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function news_by_cat_function($each){
    return $each['content_title']." <strong>Priority:</strong> ".$each['priority']." Time: ". $each['added_date'] . "<br>";
}

function get_news_by_cat( $function, $class_url_ref = null, $limit = 8, $sort_by_piority = false, $top = false){
    $config = array(
        'class-type'        =>  "category",
        'content-type'      =>  "news",
        'visibility'        =>  1,
    );
    
    if($top){
        $config['priority'] = 0;
    }else {
        if(!$sort_by_piority){
            $config['priority'] = 99;
        }else {            
            $config['priority'] = array(">", 0);
            $config['priority!@#$1'] = array("<", 99);
        }
    }
    
    if( $class_url_ref ){
        $config['class_url_ref'] =  $class_url_ref;
    }

    $list = load()->lib("NewsList")->run($config);

    $list->set_html("");
    $list->set_elemets_oper($function);

    
    if($limit) $list->data_limit($limit);
    
    return $list;
}




function newslist($caturl_ref, $cover_news_function, $others_news_function, $limit){
    //$caturl_ref = "";
    //$cover_news_function = "news_by_cat_function";
    //$others_news_function = "news_by_cat_function";
    //$limit = 20;

    $cover_news = get_news_by_cat($cover_news_function, $caturl_ref, 1, false, true );
    $news_by_priority = get_news_by_cat($others_news_function, $caturl_ref, $limit, true, false);

    $priority_total = $news_by_priority->get_total();
    
    if($cover_news->get_total()) $cover_news->show_data();
    
    if($priority_total) $news_by_priority->show_data(array( "priority" => "ASC", "added_date"=>"ASC" ));
    if($priority_total < $limit){
        $date_limit =  $limit - $priority_total;
        $news_by_added_date = get_news_by_cat($others_news_function, $caturl_ref, $date_limit, false, false);
        if($news_by_added_date->get_total()){
            $news_by_added_date->show_data(array('added_date'=>"desc", "priority"),'desc');
        }
    }
}

newslist("সংস্কৃতি", "news_by_cat_function", "news_by_cat_function", 10);


class NList extends ContentList{
    
    public function __construct() {
        
        $news = DB::run()->read("content");
    }
}

