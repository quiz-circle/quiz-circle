<?php  print_header(); ?>
          
            <div class="row" id="main">
                
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div id="section-header" class="col-lg-12">
                        <h2> শীর্ষ খবর</h2>
                    </div>                    
                    <div id="section-body" class="col-lg-12">
                        <?php
                        function headline_function_home($each){
                                $res = '<div class="item">';
                                $res .= '<div class="news-thumb">';
                                $res .= '<div class="news-list-thumb-container">';
                                $images = get_image_links($each['body'],array('src','title','alt')); 
                                if(count($images)){
                                    $src = $images[0]['src'];
                                    $alt = $images[0]['alt'];
                                    $res .= '<img src="'.$src.'" alt="'.$alt.'" />';
                                }else {
                                    $res .= '<img src="'. themePaths() . '/images/news-thumb.png" alt="cover-image" />';
                                }
                                $res .= '</div>';
                                $res .= '</div>';
                                $res .= '<div class="title">';
                                $res .= '<h2> <a href="">';
                                $res .= $each['content_title'];
                                $res .= '</a></h2>';
                                $res .= '</div>';
                                $res .= '</div>';
                            return $res;
                        }

                        function top_headline_function_home($each){
                            $images = get_image_links($each['body'],array('src','title','alt'));                            
                            $re = '<div id="cover-image" >';
                            if(count($images)){
                                $src = $images[0]['src'];
                                $alt = $images[0]['alt'];
                                $re .= '<img src="'.$src.'" alt="'.$alt.'" />';
                            }else {
                                $re .= '<img src="'. themePaths() . '/images/news-thumb.png" alt="cover-image" />';
                            }
                            $re .= '</div>';
                            $re .= '<div id="lead-title">';
                            $re .= '<h2>'; 
                            $re .= '<a href="">';
                            $re .= $each['content_title'];
                            $re .= '</a>';                                
                            $re .= '</h2>';
                            $re .= '</div>';
                            $re .= '<div id="lead-body">';
                                $re .= make_excerpt($each['body'], 800);
                            $re .= '</div>';
                            
                            
                            return $re;
                        }
                        

                        $top_headline = get_headlines("top_headline_function_home", true);
                        $headline = get_headlines("headline_function_home",false, 6);
                        ?>
                        
                        
                        <div class="col-lg-12" id="lead-news">                           
                            <?php  if($top_headline->get_total()) $top_headline->show_data(); ?>
                        </div>
                        
                        <div class="col-lg-12" id="list-container">
                            <?php if($headline->get_total()): ?>
                            <div class="news-list with-thumb">
                                <?php $headline->show_data('head_priority', 'DESC'); ?>    
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>                    
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    
                    <div id="section-header" class="col-lg-12">
                        <h2>অন্যান্য শীর্ষ খবর</h2>
                    </div>                    
                    <div class="col-lg-12" id="list-container">
                        <div class="news-list without-thumb">
                            <div class="item">
                                <h2> <a href="">বিশ্বের দ্বিতীয় ক্রিকেটার হিসেবে ৫০ উইকেট নেওয়ার পাশাপাশি করেছেন ১ হাজার রান</a>।</h2>
                            </div>
                            <div class="item">
                                <h2> <a href="">ইডেন গার্ডেন্সের উইকেট সম্পর্কে সম্পূর্ণ ভুল ধারণা করেছিল বাংলাদেশ।</a></h2>
                            </div>
                            <div class="item">
                                <h2> <a href="">স্পিনারদের নৈপুণ্যে টি-টোয়েন্টি বিশ্বকাপের সুপার টেনের প্রথম ম্যাচে মহেন্দ্র সিং ধোনির দলকে হারিয়েছে কিউইরা।</a></h2>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div id="section-header" class="col-lg-12">
                        <h2>অন্যান্য শীর্ষ খবর</h2>
                    </div>                    
                    <div class="col-lg-12" id="list-container">
                        <div class="news-list with-thumb">
                            <div class="item">
                                <div class="news-thumb">
                                    <div class="news-list-thumb-container">
                                        <img src="<?php theme_url()?>/images/img8.jpg">
                                    </div>
                                </div>
                                <div class="title">
                                    <h2> <a href="">ম্যাককালাম ৯১টি ছক্কা মারতে ৭১টি ম্যাচ খেলেছিলেন। গেইল ৪৬ ম্যাচেই তাকে ছাড়িয়ে গেলেন।ম্যাচেই তাকে ছাড়িয়ে গেলেন</a></h2>
                                </div>
                            </div>
                            <div class="item">
                                <div class="news-thumb">
                                    <div class="news-list-thumb-container">
                                        <img src="<?php theme_url()?>/images/img6.jpg">
                                    </div>
                                </div>
                                <div class="title">
                                    <h2> <a href="">অধিনায়ক মাশরাফি বিন মুর্তজা জানিয়েছেন, উইকেট সবুজ ঘাস দেখে বিভ্রান্ত হয়েছিলেন তারা।</a></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="main">
                
                <?php 
                
                    print_section(array('class'=> 'col-lg-4 col-md-6 col-sm-6 col-xs-12'), 2);
                    print_section(array('class'=> 'col-lg-4 col-md-6 col-sm-6 col-xs-12'), 13);
                    print_section(array('class'=> 'col-lg-4 col-md-6 col-sm-6 col-xs-12'), 3);
                ?>

                
            </div>
            <div class="row" id="main">

                <?php 
                    print_section(array('class'=> 'col-lg-4 col-md-6 col-sm-6 col-xs-12'), 14);
                    print_section(array('class'=> 'col-lg-4 col-md-6 col-sm-6 col-xs-12'), 11);
                    print_section(array('class'=> 'col-lg-4 col-md-6 col-sm-6 col-xs-12'), 7);
                ?>
            </div>
            <div class="row" id="main">

                <?php 
                    print_section(array('class'=> 'col-lg-4 col-md-6 col-sm-6 col-xs-12'), 6);
                    print_section(array('class'=> 'col-lg-4 col-md-6 col-sm-6 col-xs-12'), 4);
                    print_section(array('class'=> 'col-lg-4 col-md-6 col-sm-6 col-xs-12'), 5);
                ?>
            </div>
        
            <div class="row" id="main">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <ul>
                        <li>
                            <a href="">
                                হোম
                            </a>
                        <li>
                            <a href="">
                                বাংলাদেশ
                            </a>
                        </li>
                        <li>
                            <a href="">
                                রাজনীতি
                            </a>
                        </li>
                        <li>
                            <a href="">
                                অর্থনীতি
                            </a>
                        </li>
                        <li>
                            <a href="">
                                বাণিজ্য
                            </a>
                        </li>
                        <li>
                            <a href="">
                                খেলাধুলা
                            </a>
                        </li>
                        <li>
                            <a href="">

                                শিক্ষা
                            </a>
                        </li>
                        <li>
                            <a href="">

                                বিশ্ব
                            </a>
                        </li>
                        <li>
                            <a href="">
                                কৃষি
                        </li>

                        <li>
                            </a>
                            <a href="">
                                পোশাক শিল্প

                            </a>
                        </li>
                        <li>
                            <a href="">

                                আর্থিক সংগঠন
                            </a>
                        </li>
                        <li>
                            <a href="">
                                শেয়ার বাজার

                            </a>
                        </li>
                        <li>
                            <a href="">

                                জীবন বীমা
                            </a>
                        </li>
                        <li>
                            <a href="">

                                আন্তর্জাতিক
                            </a>
                        </li>
                    </ul>
                </div>             
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <ul>
                        <li>
                            <a href="">
                                হোম
                            </a>
                        <li>
                            <a href="">
                                বাংলাদেশ
                            </a>
                        </li>
                        <li>
                            <a href="">
                                রাজনীতি
                            </a>
                        </li>
                        <li>
                            <a href="">
                                অর্থনীতি
                            </a>
                        </li>
                        <li>
                            <a href="">
                                বাণিজ্য
                            </a>
                        </li>
                        <li>
                            <a href="">
                                খেলাধুলা
                            </a>
                        </li>
                        <li>
                            <a href="">

                                শিক্ষা
                            </a>
                        </li>
                        <li>
                            <a href="">

                                বিশ্ব
                            </a>
                        </li>
                        <li>
                            <a href="">
                                কৃষি
                        </li>

                        <li>
                            </a>
                            <a href="">
                                পোশাক শিল্প

                            </a>
                        </li>
                        <li>
                            <a href="">

                                আর্থিক সংগঠন
                            </a>
                        </li>
                        <li>
                            <a href="">
                                শেয়ার বাজার

                            </a>
                        </li>
                        <li>
                            <a href="">

                                জীবন বীমা
                            </a>
                        </li>
                        <li>
                            <a href="">

                                আন্তর্জাতিক
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <ul>
                        <li>
                            <a href="">
                                হোম
                            </a>
                        <li>
                            <a href="">
                                বাংলাদেশ
                            </a>
                        </li>
                        <li>
                            <a href="">
                                রাজনীতি
                            </a>
                        </li>
                        <li>
                            <a href="">
                                অর্থনীতি
                            </a>
                        </li>
                        <li>
                            <a href="">
                                বাণিজ্য
                            </a>
                        </li>
                        <li>
                            <a href="">
                                খেলাধুলা
                            </a>
                        </li>
                        <li>
                            <a href="">

                                শিক্ষা
                            </a>
                        </li>
                        <li>
                            <a href="">

                                বিশ্ব
                            </a>
                        </li>
                        <li>
                            <a href="">
                                কৃষি
                        </li>

                        <li>
                            </a>
                            <a href="">
                                পোশাক শিল্প

                            </a>
                        </li>
                        <li>
                            <a href="">

                                আর্থিক সংগঠন
                            </a>
                        </li>
                        <li>
                            <a href="">
                                শেয়ার বাজার

                            </a>
                        </li>
                        <li>
                            <a href="">

                                জীবন বীমা
                            </a>
                        </li>
                        <li>
                            <a href="">

                                আন্তর্জাতিক
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            
<?php print_footer(); ?>
































