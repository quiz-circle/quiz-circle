<?php page_include("includes/function.php") ?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="stylesheet" href="<?php theme_url()?>/css/bootstrap.min.css">
        
        <link rel="stylesheet" href="<?php theme_url()?>/css/bootstrap-theme.min.css">
        
        
        <script src="<?php theme_url()?>/javascript/jQuery.js"> </script>
        
        <script src="<?php theme_url()?>/javascript/bootstrap.min.js"> </script>
        
    </head>
    <body>
        <div class="container">
            <div class="row" style="height: 120px">
                
            </div>
        </div>
        <nav class="navbar navbar-inverse" style="margin-bottom: 0 ">
            <div class="container" >
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>                        
                </button>
                <!--  
                <a class="navbar-brand" href="#">WebSiteName</a>
                -->
              </div>
              <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#" class="glyphicon glyphicon-home" title="Home"></a></li>
                  <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">খবর <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">রাজনীতি</a></li>
                      <li><a href="#">অর্থনীতি</a></li>
                      <li><a href="#">বিশ্ব</a></li>
                      <li><a href="#">তথ্য ও প্রযুক্তি</a></li>
                      <li><a href="#">পরিবেশ</a></li>
                      <li><a href="#">সাস্থ</a></li>
                      <li><a href="#">আন্তর্জাতিক</a></li>
                    </ul>
                  </li>
                  <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">বাণিজ্য<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">পোশাক শিল্প</a></li>
                      <li><a href="#">হাউজিং</a></li>
                      <li><a href="#">শেয়ার বাজার</a></li>
                    </ul>
                  </li>
                  <li>
                    <a href="#">খেলাধুলা</a >
                  </li>
                  <li>
                      <a href="#">শিক্ষা</a>
                  </li>
                  <li>
                      <a href="#">পর্যটন</a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        <style>
            #main {
                margin-top: 40px;
            }
            .navbar-inverse {
                background: none;
                border: none;
            }
            #myNavbar {
                background-color: #2a6496;
            }
            
            .navbar-header {
                background-color: #2a6496;
            }
            
            .navbar-inverse .navbar-nav>li>a {
                color: #FFF;
            }
            .navbar-inverse .navbar-nav>.open>a, .navbar-inverse .navbar-nav>.open>a:hover, .navbar-inverse .navbar-nav>.open>a:focus {
                color: #fff;
                background-color: #555;
            }
            #section-header {
                width: 100%;
                height: 35px;
                float: left;
                background-color: #2a6496;
            }
            
            #section-header h2 {
                width: 100%;
                height: 100%;
                float: left;
                margin: 0;
                padding: 0;
                line-height: 35px;
                font-size: 18px;
                font-weight: bold;
                color: #FFF;
                text-align: center;
            }
            
            #section-body {
                padding: 0;
                float: left;
                font-size: 18px;
                color: #555;
            }
            
            #section-body #lead-news {
                padding: 0;
            }
            
            #section-body #lead-news #cover-image{
                width: 100%;
                height: 200px;
                float: left;
                overflow: hidden;
                border-bottom: 1px solid #DDD;
            }
            #section-body #lead-news #cover-image img{
                width: 100%;
            }

            #section-body #lead-news #lead-title{
                float: left;
                width: 100%;
            }
            
            #section-body #lead-news #lead-title h2{
                font-size: 22px;
                font-weight: bold;
                margin: 10px 0 20px;
                color: #333;
            }
            
            #section-body #lead-news #lead-body{
                
            }

            #list-container {
                width: 100%;
                margin: 0;
                padding: 0;
                margin-top: 35px;
            }
            
            .news-list {
                width: 100%;
                float: left;
            }
            
            .news-list .item {
                width: 100%;
                float: left;
            }
            
            .with-thumb .item {
                width: 100%;
                float: left;
                margin-bottom: 18px;
                overflow: hidden;
            }
            .with-thumb .news-list-thumb-container {
                height: 100%;
                overflow: hidden;
            }
            
            .with-thumb .item .news-thumb {
                width: 65px;
                height: 50px;
                float: left;
                display: block;
                border: 1px solid #EEE;
                margin-right: 10px;
                margin-bottom:  2px;
                padding: 5px;
                overflow: hidden;
                position: relative;
            }
            
            .with-thumb .item .news-thumb img {
                width: 100%;
                border: 1px solid #DDD;
                position: relative;
                margin-bottom: 5px;
            }

            .with-thumb .item  h2,.without-thumb .item  h2 {
                font-size: 18px;
                margin: 0;
                display: inline;
                padding: 0;
                display: inline;
                clear: none;
                font-weight: bold;
                line-height: 25px;
                text-wrap: none;
                color: #333;
            }
            
            .without-thumb .item  h2 {
                float: left;
                margin-top: 20px;
            }
        </style>
        <div class="container" >  