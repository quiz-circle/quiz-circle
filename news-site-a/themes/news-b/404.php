<?php  print_header(); ?>
        <div class="container" >
            <div class="row " id="single-page-top">
                <div class="col-lg-8">
                    <?php
                    $SEG_VARS = segment_val();
                    
                    $meta = load()->sys("MetaInfo")->run("site_info", "info_key", "info_value");
                    $hline =  $meta->get(get_config('meta_attr/headlines_class_key'));
                    $recent_news = get_news_by_cat("recent_news_list",  $hline, array( 'limit'=> 10, 'start'=> 0));
                    $recent_news_total = $recent_news->get_total();
                    $order = array("priority"=>"ASC", "added_date"=> "DESC");
                    
                    ?>
                    
                    
                    <h2 style="text-align: center; margin-top: 150px; font-size: 55px; font-weight: bold; color: #005522">Ops.. Not Found!</h2>
                    <p class="news-single-body" style="text-align: center">
                        Your requested page our url does not match to any news or page, We are sorry!
                    </p>
                </div>
                <div class="col-lg-4 single-page-recent">
                    Recent News
                    <div class="recent-content-section">
                        <div class="recent-content-section-element" style="display: block">
                            <?php if($recent_news_total): ?>
                                <?php $recent_news->show_data($order); ?>
                            <?php endif; ?>                             
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php print_footer(); ?>
































