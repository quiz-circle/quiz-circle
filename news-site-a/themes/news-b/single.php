<?php  print_header(); ?>
        <div class="container" >
            <div class="row " id="single-page-top">
                <div class="col-lg-8">
                    <?php
                    $SEG_VARS = segment_val();
                    
                    $meta = load()->sys("MetaInfo")->run("site_info", "info_key", "info_value");
                    $hline =  $meta->get(get_config('meta_attr/headlines_class_key'));
                    $recent_news = get_news_by_cat("recent_news_list",  $hline, array( 'limit'=> 10, 'start'=> 0));
                    $recent_news_total = $recent_news->get_total();
                    $order = array("priority"=>"ASC", "added_date"=> "DESC");
                    
                    $url_sec = 0;
                    
                    
                    $content_id = isset($_SEG_VARS[0]) ? (int) $_SEG_VARS[0]:false;
                    
                    if($content_id === false){
                        redirect(base_url());
                    }
                    
                    $where_columns = array(
                        'visibility' => 1,
                        'status' => 'publish',
                        'content_type' => 'news',
                        'content_id' => $content_id,
                    );
                    
                    load()->sys("content")->open();
                    
                    
                    $news = new Content("content","content_id", $url_sec);
                    
                    $news = $news->read();
                    
                    $read_news = DB::run()->read('content')->where($where_columns)->run_sql();
                    
                    if($read_news->error()){
                        echo $read_news->sql_error();
                    }else {
                        if($read_news->get_count()){
                            $news = $read_news->get_first();
                        }else {
                            redirect(base_url());
                        }
                    }
                    
                    
                    $news_without_image = preg_replace('/<img[^>]+>/i', "", $news['body']);
                    $image_thumb = get_image_links( $news['body'], 'src');
                        
                    //Explain($image_thumb);
                    $str = "https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.prothom-alo.com%2Finternational%2Farticle%2F866185%2F%25E0%25A6%25AC%25E0%25A6%25BF%25E0%25A6%25AE%25E0%25A6%25BE%25E0%25A6%25A8-%25E0%25A6%25AC%25E0%25A6%25BF%25E0%25A6%25A7%25E0%25A7%258D%25E0%25A6%25AC%25E0%25A6%25B8%25E0%25A7%258D%25E0%25A6%25A4-%25E0%25A6%25B8%25E0%25A6%25AE%25E0%25A7%258D%25E0%25A6%25AD%25E0%25A6%25BE%25E0%25A6%25AC%25E0%25A7%258D%25E0%25A6%25AF-%25E0%25A6%25B8%25E0%25A6%25AC-%25E0%25A6%2595%25E0%25A6%25BE%25E0%25A6%25B0%25E0%25A6%25A3-%25E0%25A6%2596%25E0%25A6%25A4%25E0%25A6%25BF%25E0%25A7%259F%25E0%25A7%2587-%25E0%25A6%25A6%25E0%25A7%2587%25E0%25A6%2596%25E0%25A6%25BE-%25E0%25A6%25B9%25E0%25A6%259A%25E0%25A7%258D%25E0%25A6%259B%25E0%25A7%2587";
                    
                    //echo urldecode($str);
                    
                    ?>
                    
                    <iframe src="https://www.facebook.com/plugins/share_button.php?href=<?php echo full_url();?>&layout=button_count&mobile_iframe=true&width=95&height=20&appId" width="95" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true">
                    </iframe>
                    <div class="breadcrumb-container">
                       <?php show_bracecrumb($news['content_id']); ?>
                    </div>
                    <div id="social-options">
                    </div>
                    <div class="news-single-title">
                        <h2><?php echo $news['content_title']; ?></h2>
                    </div>
                    <div class="news-single-information">
                        <div class="writer"> <?php echo $news['author'] ?></div>
                        <div class="date"><?php echo convert_date($news['added_date'], "D, d M, Y")?> </div>
                    </div>
                    <p class="news-single-body">
                        <?php echo $news['body']; ?>
                    </p>
                    <div class="news-comments">
                        <a name="comment"></a>
                        <div class="news-comment-head">
                            <h2>Comment</h2>
                        </div>
                        <?php
                        
                            $message = "";
                            $session = load()->sys('session')->run();
                            $hash = load()->sys('hash')->run();
                            
                            
                            $post_hash = POST_DATA('hash');
                            $session_hash = $session->flush('input-hash');
                                                    
                            if(POST_DATA() && ($post_hash === $session_hash)){
                                
                                $comment_values = array();
                                
                                $comment_approved = 0;
                                $user_id = 0;

                                $comment_content_id  = POST_DATA('content_id');
                                $comment_name  = POST_DATA('name');
                                $comment_email  = POST_DATA('email');
                                $comment_body  = POST_DATA('comment');

                                $comment_parent  = (int) POST_DATA('parent');
                                $comment_parent_order  = (int) POST_DATA('parent_order');                                
                                $comment_values['content_id'] = $comment_content_id;
                                $comment_values['parent'] = $comment_parent;
                                $comment_values['order'] = $comment_parent_order;
                                $comment_values['name'] = $comment_name;
                                $comment_values['email'] = $comment_email;
                                $comment_values['comment_body'] = $comment_body;
                                $comment_values['approved'] = $comment_approved;
                                $comment_values['user_id'] = $user_id;
                                $comment_values['date'] = date("Y-m-d H:i:s");
                                $comment_values['host'] = $_SERVER['REMOTE_ADDR'];

                                Explain($comment_values);
                                
                                $comment_insert = DB::run()->write("comment")->values($comment_values)->run_sql();
                                
                                //echo $comment_insert->get_sql()."<br>";
                                //echo $comment_insert->sql_error()."<br>";
                                
                                if($comment_insert->error()){
                                    $message = "<h4>Internal Error!</h4>";
                                }else {
                                    $message = "<h4>Comment posted!</h4>";
                                    $message .= "<p>Thank You for your comment</p>";
                                }   
                            }
                            $comment = get_comments("commentsList", $news['content_id']);
                        ?>
                        <div class="comment-success-message">
                            <?php 
                                echo $message;
                            ?>
                        </div>
                        <?php
                            if($comment->get_total()){
                                $comment->show_data("comment_id","DESC");
                            }else {
                        ?>
                        <h5> কোন মন্তব্য নেই 
                        </h5>
                        <?php } ?>
                        <div class="comment-form">
                            <p style="margin: 30px 0 0 0; padding: 0;font-size: 15px;"><?php echo ($comment->get_total()) ? "আপনিও":""; ?> মন্তব্য লিখুন</p>
                            <form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>#comment" class="comment-form-in">
                                <div class="input-group">
                                    <input type="hidden" value="<?php echo $SEG_VARS[0]; ?>" name="content_id" class="form-control" >
                                    <input type="hidden" value="<?php echo $session->add("input-hash", $hash->unique()); ?>" name="hash" class="form-control" >
                                <div class="input-group">
                                    <input type="hidden" value="0" name="parent" >
                                    <input type="hidden" value="0" name="parent_order">
                                </div>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">আপনার নাম</span>
                                    <input type="text" name="name" class="form-control" required placeholder="Type your name (Required)">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">আপনার ইমেইল</span>
                                    <input type="email" name="email" class="form-control" placeholder="Type your name (Required)" >
                                </div>
                                <div class="input-group">
                                    <textarea class="form-control" name="comment" required placeholder="Type Your comment" ></textarea>
                                </div>
                                <div class="input-group">
                                    <input type="submit" value="Post Comment" class="btn btn-primary">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 single-page-recent">
                    Recent News
                    <div class="recent-content-section">
                        <div class="recent-content-section-element" style="display: block">
                            <?php if($recent_news_total): ?>
                                <?php $recent_news->show_data($order); ?>
                            <?php endif; ?>                             
                        </div>
                    </div>
                </div>
            </div>
        </div>

<script>
    var ajax_path = theme_path+"/ajax";
    var news_read_miliseconds = 5000;
    var news_id = <?php echo SEG_VARS($url_sec); ?>;
    
    
   setTimeout(function(){
        $.ajax({
            'type':'post',
            'data': {"news_id":news_id},
            'url':ajax_path + "/update_most_read.php",
            'success':most_read_success_function,
            'error':most_read_error_function
        });
   }, news_read_miliseconds)
    
    function most_read_success_function(r){
       console.log(r);
    }
    
    function most_read_error_function(a, e){
        console.log(e);
    }
    
</script>
<script>
    //$.jQuery(document).ready(
    $( document ).ready(function() {
    // Handler for .ready() called.
        $("#social-options").jsSocials({
            url: window.location,
            text: "<?php echo $news['content_title']?>",
            shareIn: "popup",
            shares: ["email", {"share":"facebook", "label":"share"}, "twitter",  "googleplus", "linkedin", "pinterest", "stumbleupon", "whatsapp"]
        });
    });
    //);
</script>
<?php print_footer(); ?>
































