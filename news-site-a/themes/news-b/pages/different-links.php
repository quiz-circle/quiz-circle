<?php  print_header(); ?>
<style>
.news-single-body #content-tab,.news-single-body #content-body {
    width: 100%;
    float: left;
}

.news-single-body #content-tab {
    margin-top: 10px;
    border-bottom: 1px solid #CCC;
    z-index: -2;
}

.news-single-body #content-body {
    padding: 10px 0;
}

.news-single-body #content-tab ul {
    list-style-type: none;
    padding: 0;
    margin: 0;
    margin-bottom: -2px;
    float: left;
    z-index: -1;
    overflow: hidden;
}

.news-single-body #content-tab ul li{
    display: inline;
    float: left;
    margin-right: 1px;
    z-index: 0;
    overflow: visible;
}

.news-single-body #content-tab ul li a{
    border: 1px solid #CCC;
    font-size: 12px;
    float: left;
    padding: 5px 8px;
    width: 100%;
    text-decoration: none;
    border-radius: 5px 5px 0 0;
    z-index: 0;
    border-bottom: none;
}

.news-single-body #content-tab ul li a:hover{
    background-color: #CCC;
}

.news-single-body #content-tab ul li.active a{
    background-color: #FFF;
    color: #FFF;
    color: #333;
}
.news-single-body #content-tab ul li.active a:hover{

}


</style>
        <div class="container" >
            <div class="row " id="single-page-top">
                <div class="col-lg-8">

                    <?php
                        $SEG_VARS = segment_val();
                        $meta = load()->sys("MetaInfo")->run("site_info", "info_key", "info_value");
                        $hline =  $meta->get(get_config('meta_attr/headlines_class_key'));
                        $recent_news = get_news_by_cat("recent_news_list",  $hline, array( 'limit'=> 10, 'start'=> 0));
                        $recent_news_total = $recent_news->get_total();
                        $order = array("priority"=>"ASC", "added_date"=> "DESC");
                        $category_id = _get("class_relationship", "entity_id", $this->_content['content_id'], "class_id");
                        
                        $content_ids = array();
                        if($category_id) {
                            $sql = "SELECT * FROM class_relationship cr JOIN content c WHERE  (cr.entity_id = c.content_id) ";
                            $sql .= "AND cr.class_id = " . $category_id ;
                            $sql .= " AND c.content_type = 'news'";
                            $content =DB::run()->setSql($sql)->run_sql();
                            
                            if($content->get_count()){
                                $content_ids = $content->get_array();
                                //$content_ids = _get("class_relationship", "class_id", $category_id);
                                
                                //Explain($content_ids);
                            }
                        }
                        $url_ref = "";
                        $title = null;
                        $body = null;
                        $li_active = "";
                        
                        $main_data = array();
                        $main_data['body'] = null;
                        $main_data['content_title'] = null;
                        $main_data['url_ref'] = null;
                        
                        if(GET_DATA("tab")){
                            $url_ref = GET_DATA("tab");
                            $body = DB::run()->read('content')->where('url_ref' ,  $url_ref)->run_sql();
                            if($body->get_count()){
                                $main_data = $body->get_first();
                                $title = $main_data['content_title'];
                                $body = $main_data['body'];
                            }
                        }else {
                            $title = isset($content_ids[0]) ? $content_ids[0]['content_title']:"";
                            $body = isset($content_ids[0]) ? $content_ids[0]['body']:"";
                        }
                        
                        
                        
                        
                    ?>

                    
                    <div class="breadcrumb-container">
                       <?php //show_bracecrumb($news['content_id']); ?>
                    </div>
                    <!--
                    <div id="social-options">
                    </div>
                    -->
                    <div class="news-single-title">
                        <h2><?php echo ($title !== null && $body !== null)  ? $title : "Not Found"; ?></h2>
                    </div>
                    <div class="news-single-body">
                        <?php if(count($content_ids)): ?>
                        <div id="content-tab">
                            <ul>
                                <?php foreach ($content_ids as $content): ?>
                                <li <?php echo ( $content['url_ref'] == $url_ref) ? " class='active' ":""; ?>>
                                    <a href="<?php echo base_url()."/". page_name(). "?tab=" .urlencode($content['url_ref'])?>" ><?php echo $content['content_title']?></a>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <div id="content-body">
                            <?php echo ($title !== null && $body !== null)  ? $body : "The " . str_replace(array("_", "-"), array(" ", " "), $url_ref) . "  has not found in database!"; ?>
                        </div>
                        <?php else: ?>
                        <p style="color: #888; margin-top: 10px; font-size: 14px;float: left;">No Content Added for this page!</p>
                        <?php endif; ?>
                    
                    </div>
                </div>
                <div class="col-lg-4 single-page-recent">
                    Recent News (<?php echo $this->_content['source_link']; ?>)
                    <div class="recent-content-section">
                        <div class="recent-content-section-element" style="display: block">
                            <?php if($recent_news_total): ?>
                                <?php $recent_news->show_data($order); ?>
                            <?php endif; ?>                             
                        </div>
                    </div>
                </div>
            </div>
        </div>

<script>
    //$.jQuery(document).ready(
    $( document ).ready(function() {
    // Handler for .ready() called.
        $("#social-options").jsSocials({
            url: window.location,
            text: "<?php echo $news['content_title']?>",
            shareIn: "popup",
            shares: ["email", {"share":"facebook", "label":"share"}, "twitter",  "googleplus", "linkedin", "pinterest", "stumbleupon", "whatsapp"]
        });
    });
    //);
</script>
<?php print_footer(); ?>
































