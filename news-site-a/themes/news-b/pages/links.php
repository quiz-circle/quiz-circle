<?php  print_header(); ?>
        <div class="container" >
            <div class="row " id="single-page-top">
                <div class="col-lg-8">

                    <?php
                        $SEG_VARS = segment_val();
                        $meta = load()->sys("MetaInfo")->run("site_info", "info_key", "info_value");
                        $hline =  $meta->get(get_config('meta_attr/headlines_class_key'));
                        $recent_news = get_news_by_cat("recent_news_list",  $hline, array( 'limit'=> 10, 'start'=> 17));
                        $recent_news_total = $recent_news->get_total();
                        $order = array("priority"=>"ASC", "added_date"=> "DESC");
                    ?>
                   
                    <div class="breadcrumb-container">
                       <?php //show_bracecrumb($news['content_id']); ?>
                    </div>
                    <!--
                    <div id="social-options">
                    </div>
                    -->
                    <div class="news-single-title">
                        <h2><?php echo $this->_content['content_title']; ?></h2>
                    </div>
                    <p class="news-single-body">
                        <?php echo $this->_content['body']; ?>
                    </p>
                </div>
                <div class="col-lg-4 single-page-recent">
                    Recent News (links)
                    <div class="recent-content-section">
                        <div class="recent-content-section-element" style="display: block">
                            <?php if($recent_news_total): ?>
                                <?php $recent_news->show_data($order); ?>
                            <?php endif; ?>                             
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php print_footer(); ?>
































