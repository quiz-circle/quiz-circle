<?php

page_include("function.php");

function headline_function_home($each){
        $res = '<div class="item">';
        $res .= '<div class="news-thumb">';
        $res .= '<div class="news-list-thumb-container">';
        $images = get_image_links($each['body'],array('src','title','alt')); 
        if(count($images)){
            $src = $images[0]['src'];
            $alt = $images[0]['alt'];
            $res .= '<img src="'.$src.'" alt="'.$alt.'" />';
        }else {
            $res .= '<img src="'. themePaths() . '/images/news-thumb.png" alt="cover-image" />';
        }
        $res .= '</div>';
        $res .= '</div>';
        $res .= '<div class="title">';
        $res .= '<h2> <a href="">';
        $res .= $each['content_title'];
        $res .= '</a></h2>';
        $res .= '</div>';
        $res .= '</div>';
    return $res;
}

function top_headline_function_home($each){
    $images = get_image_links($each['body'],array('src','title','alt'));                            
    $re = '<div id="cover-image" >';
    if(count($images)){
        $src = $images[0]['src'];
        $alt = $images[0]['alt'];
        $re .= '<img src="'.$src.'" alt="'.$alt.'" />';
    }else {
        $re .= '<img src="'. themePaths() . '/images/news-thumb.png" alt="cover-image" />';
    }
    $re .= '</div>';
    $re .= '<div id="lead-title">';
    $re .= '<h2>'; 
    $re .= '<a href="">';
    $re .= $each['content_title'];
    $re .= '</a>';                                
    $re .= '</h2>';
    $re .= '</div>';
    $re .= '<div id="lead-body">';
        $re .= make_excerpt($each['body'], 800);
    $re .= '</div>';


    return $re;
}

function get_headlines($function, $top = false, $limit = 10){
    $list = load()->lib("Headlines")->run($top);
    $list->set_is_tree(false);
    $list->data_limit($limit);
    $list->set_html("");
    $list->set_elemets_oper($function);
    return $list;
}

$top_headline = get_headlines("top_headline_function_home", true);
$headline = get_headlines("headline_function_home",false, 6);


