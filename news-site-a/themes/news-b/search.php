<?php  print_header(); ?>
<style>
    .search-highlight {
        color: #000011;
    }
    
</style>

        <?php 
            $search_query = GET_DATA("query");
            
            $meta = load()->sys("MetaInfo")->run("site_info", "info_key", "info_value");
            $hline =  $meta->get(get_config('meta_attr/headlines_class_key'));
            $recent_news = get_news_by_cat("recent_news_list",  $hline, array( 'limit'=> 10, 'start'=> 0));
            $recent_news_total = $recent_news->get_total();
            $order = array("priority"=>"ASC", "added_date"=> "DESC");
        ?>

        <div class="container" >
            <div class="row " id="cagetory-page-top">
                <div class="col-lg-8">
                    <?php if($search_query): ?>
                    <div class="breadcrumb-container">
                        <ol class="breadcrumb">
                            <li>হোম</li>
                            <li>Search</li>
                            <li class="active"><?php echo $search_query?></li>
                        </ol>
                    </div>
                    <div class="heading">
                        <h1>Search result for <?php echo $search_query; ?></h1>
                    </div>
                    <div class="list-by-category">
                        <?php
                        
                            function list_from_search($row){
                                $search_query = GET_DATA("query");
                                $res = "";
                                $res .= '';
                                $res .= '<div class="list-by-category-each">';
                                    $res .= '<div class="image-container-vh list-by-cat-image">';
                                        $res .= image_from_content($row['body']);
                                    $res .= '</div>';
                                    $res .= '<div class="list-by-cat-right">';
                                        $res .= '<h2 class="list-by-cat-title">';
                                            $res .= '<a href="' . news_single_url($row['content_id']) . '">' . str_replace($search_query, "<em class='search-highlight'>".$search_query . "</em>", $row['content_title']).'</a>';
                                        $res .= '</h2>';
                                        $res .= '<div class="list-by-cat-date">';
                                            $res .= $row['added_date']; 
                                        $res .= '</div>';
                                        $res .= '<p class="list-by-cat-body">';
                                            $res .= strip_tags(make_excerpt($row['body'], 600));
                                            $res .= '<a href="' . news_single_url($row['content_id']) . '" class="news-details">...বিস্তারিত</a>';
                                        $res .= '</p>';
                                    $res .= '</div>';
                                $res .= '</div>';
                                return $res;
                            }
                        
                            $news = search_news("list_from_search", $search_query, false);
                        ?>

                        <?php
                            if($news){
                                $total_page_limit = 4;
                                if($news->get_total()){
                                    $c['start'] = '<ul class="pagination"> ';

                                    $news->set_pagination_conf($c);
                                    $pagination =  $news->set_pagination($total_page_limit, "p");
                                    $news->show_data();
                                }else {
                                    ?>
                                    <h2 style="
                                        color:#BBB; 
                                        text-align: center; 
                                        margin-top: 80px; 
                                        width: 100%; 
                                        float: left; 
                                        display: inline-block"
                                    >Nothing Found For <?php echo $search_query; ?>!</h2>
                                <?php
                                }
                            }else {
                                ?>
                                <h2 style="
                                    color:#BBB; 
                                    text-align: center; 
                                    margin-top: 80px; 
                                    width: 100%; 
                                    float: left; 
                                    display: inline-block"
                                >Nothing given for searching!</h2>
                            <?php
                            }
                        ?>
                    </div>
                    <div class="pagination">
                        <?php
                            if($news){
                                if($news->get_total() > $total_page_limit){
                                    $pagination =  $news->get_pagination();
                                    echo $pagination;
                                }
                            }
                        ?>
                    </div>
                    <?php else: ?>
                        <h2 style="
                            color:#BBB; 
                            text-align: center; 
                            margin-top: 80px; 
                            margin-bottom: 80px; 
                            width: 100%; 
                            float: left; 
                            display: inline-block"
                            ><em>This category is not exists!</em></h2>
                    <?php endif; ?>
                </div>
                <div class="col-lg-4 single-page-recent">
                    Recent News
                    <div class="recent-content-section">
                        <div class="recent-content-section-element" style="display: block">
                            <?php if($recent_news_total): ?>
                                <?php $recent_news->show_data($order); ?>
                            <?php endif; ?>                             
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php print_footer(); ?>