<?php  print_header(); ?>
        <div class="container" >
            <div class="row " id="single-page-top">
                <div class="col-lg-8">
                    <style>
                        #single-page-top {
                            margin-top: 15px;
                        }
                        
                        .breadcrumb-container {
                            float: left;
                            margin-bottom: 8px;
                            width: 100%;
                        }
                        .breadcrumb-container .breadcrumb {
                           margin: 0;
                           padding:5px 0 ;
                           background: none;
                        }
                        .breadcrumb-container .breadcrumb li+li:before {
                            content: "\00BB";
                            color: #333;
                        }
                        .news-single-title {
                            width: 100%;
                            float: left;
                            border-top: 1px solid #DDD; 
                            border-bottom: 1px solid #DDD; 
                        }
                        
                        .news-single-title h2 {
                            width: 100%;
                            float: left;
                            margin: 0;
                            padding: 8px 0;
                            font-size: 22px;
                        }
                        
                        .news-single-information {
                            width: 100%;
                            float: left;
                            border-bottom: 1px solid #CCC;
                            margin-bottom: 25px;
                            padding:0 0 3px 0;
                        }
                        
                        .news-single-information div {
                            display: inline-block;
                            font-size: 14px;
                            color: #888;
                            padding-top: 5px
                        }
                        .news-single-information .writer {
                            
                        }
                        
                        .news-single-information .writer:after {
                            content: "|";
                            padding: 0px 5px 0 5px;
                            font-size: 15px;
                            color: #333;
                        }
                        
                        .news-single-body {
                            font-size: 20px;
                            line-height: 1.5;
                        }
                        
                    </style>
                    <?php
                    
                    $news = load()->sys("content")->run("content","content_id", 0);                    
                    $news = $news->read();
                    $news_without_image = preg_replace('/<img[^>]+>/i', "", $news['body']);                    
                    

                        
                    ?>
                    <div class="breadcrumb-container">
                        
                        
                        <?php show_bracecrumb($news['content_id']); ?>
                        <?php show_bracecrumb("নিহত-dsfg-sd-asdf-"); ?>
                        
                    </div>
                </div>
                <div class="col-lg-4">
                    Recent News
                </div>
            </div>
        </div>
<?php print_footer(); ?>
































