<?php

function site_title(){
    return "Newspaper";
}

function print_title(){
    $single = get_config("news-details/reference");
    $category = get_config("category/reference");
    $tag = get_config("tag/reference");
    $page = get_config("page/reference");
    $author = get_config("author/reference");
    
    switch (page_name()){
        case $single :
            if(SEG_VARS(0)){
                echo urldecode(_get('content', 'content_id', SEG_VARS(0), 'content_title'));
            }else {
                echo "Page ";
            } 
            echo " &raquo; ";
            break;
        case  $category:
            if(SEG_VARS(0)){
                echo urldecode(SEG_VARS(0));
            }else {
                echo "Category ";
            }
            echo " &raquo; ";
            break;
        case  $page:
            if(SEG_VARS(0)){
                echo urldecode(SEG_VARS(0));
            }else {
                echo "Page ";
            }
            echo " &raquo; ";
            break;
        case  $author:
            if(SEG_VARS(0)){
                echo urldecode(SEG_VARS(0));
            }else {
                echo "Author ";
            }
            echo " &raquo; ";
            break;
        case  $author:
            if(SEG_VARS(0)){
                echo urldecode(SEG_VARS(0));
            }else {
                echo "Tag ";
            }
            echo " &raquo; ";
            break;
        default :
            echo "Welcome to ";
    }
    echo site_title();
}



/*
function lead_news_function($each){
    $res = '<div class="col-lg-12" id="lead-news">';
    $res .= '<div id="cover-image">';
        $images = get_image_links($each['body'],array('src','title','alt'));
        if(count($images)){
            $res .= '<img src="'.$images[0]['src'].'" alt="'.$images[0]['src'].'" />';
        }else {
            $res .= '<img src="'. themePaths() . '/images/news-thumb.png" alt="cover-image" />';
        }    
        $res .= '</div>';
    $res .= '<div id="lead-title">';
        $res .= '<h2> <a href="">';
            $res .= $each['content_title']; 
            $res .= '</a>';                                
        $res .= '</h2>';
    $res .= '</div>';
    $res .= '<div id="lead-body">';
        $res .= strip_tags(make_excerpt($each['body'], 800));
    $res .= '</div>';
    $res .= '</div>';
    return $res;
}

*/

function get_nav_items($function, $class_id, $html, $attr = ""){
    $nav = load()->lib("NavList")->run($class_id);

    $nav->set_html($html, true);
    $nav->set_is_tree(true);

    if($attr){
        $nav->setAttrs($attr);
    }
    $nav->set_elemets_oper($function);
    $nav->show_data("priority");
}

function get_list_by_paper_id( $function, $paper_id, $limit){
    
    $config = array(
        'class-type'        =>  "category",
        'content-type'      =>  "news",
        'paper_id'        =>  $paper_id,
    );
    
    $list = load()->lib("NewsList")->run($config);

    $list->set_html("");
    $list->set_elemets_oper($function);
    
    //echo $list->get_sql()."<br>";
    
    if($limit) {
        if(is_array($limit)){
            if(isset($limit['limit']) && isset($limit['start']) ){
                $list->data_limit($limit['limit'], $limit['start']);
            }
        }else {
            $list->data_limit($limit);
        }
    }
    
    //echo "<br><strong>SQL:</strong> ".$list->get_sql();
    return $list;
}

function search_news( $function, $search_query = "", $limit = 8, $column = "*"){
    $config = array(
        'visibility'        =>  1,
        'added-date'        => array("LIKE", "%".date('Y')."%"),
        'status'            =>  "publish",
    );
    
    if( $search_query != ""){
        $search_query = strip_tags(trim($search_query));
        $config['content_title'] = array("LIKE", "%" . $search_query . "%");
        $list = load()->lib("NewsList")->run($config, $column);

        $list->set_html("");
        $list->set_elemets_oper($function);
        
        if($limit) {
            if(is_array($limit)){
                if(isset($limit['limit']) && isset($limit['start']) ){
                    $list->data_limit($limit['limit'], $limit['start']);
                }
            }else {
                $list->data_limit($limit);
            }
        }
    
        //echo "<br><strong>SQL:</strong> ".$list->get_sql();
        return $list;
    }
    return false;
}



function get_news_by_cat( $function, $class_url_ref = null, $limit = 8, $column = "*"){
    $config = array(
        'class-type'        =>  "category",
        'content-type'      =>  "news",
        'visibility'        =>  1,
        'added-date'        => array("LIKE", "%".date('Y')."%"),
        'status'            =>  "publish",
    );
    
    if( $class_url_ref ){
        $config['class_url_ref'] = strip_tags(trim($class_url_ref));
    }

    $list = load()->lib("NewsList")->run($config, $column);

    $list->set_html("");
    $list->set_elemets_oper($function);
    
    //echo $list->get_sql()."<br>";
    
    if($limit) {
        if(is_array($limit)){
            if(isset($limit['limit']) && isset($limit['start']) ){
                $list->data_limit($limit['limit'], $limit['start']);
            }
        }else {
            $list->data_limit($limit);
        }
    }
    
    //echo "<br><strong>SQL:</strong> ".$list->get_sql();
    return $list;
}




function newslist($caturl_ref, $cover_news_function, $others_news_function, $limit){
    $news = array(); 
    $news['top'] = "";
    $news['other'] = "";

    $cover_news = get_news_by_cat($cover_news_function, $caturl_ref, 1);
    if($cover_news->get_total()){
        $news['top'] = $cover_news->get_data(array( "priority" => "ASC", "added_date"=>"ASC" ));
    }
    $limit--;
    
    $other_news = get_news_by_cat($others_news_function, $caturl_ref, array('start'=> 1, 'limit'=> $limit));
    
    if($other_news->get_total()){
        $news['other'] .= $other_news->get_data(array("priority"=> "ASC", 'added_date'=>"DESC"));
    }
    
    return $news;
}

?>

<!--
<div class="col-lg-8 two-column-news-list">
    <div class="news-list-head">
        প্রাইভেট বিশ্ববিদ্যালয়
    </div>
    <div class="col-lg-6 two-colmn-left">
        <div class="section-top-news">
            <div class="image-container-vh section-top-news-image">
                <img src="<?php echo theme_url() ?>/images/image16.jpg">
            </div>
            <h2 class="section-top-news-title">
                <a href="#">
                    ব্যাংক খাতে শৃঙ্খলা ফেরানোর পরামর্শ
                </a>
            </h2>
            <p class="section-top-excerpt">
                ব্যাংক খাতে শৃঙ্খলা ফিরিয়ে আনার পরামর্শ দিয়েছেন দেশের প্রিন্ট ও ইলেকট্রনিক গণমাধ্যমের সম্পাদক, প্রকাশক ও প্রধান নির্বাহীরা। তাঁদের মতে, শৃঙ্খলা ফেরানোর কাজটি শুরু করতে হবে বাংলাদেশ ব্যাংক থেকেই। বাংলাদেশ ব্যাংককে তাদের কর্মকাণ্ডের জন্য জবাবদিহির আওতায় আনতে হবে।
            </p>
        </div>
    </div>
    <div class="col-lg-6 two-colmn-right">
        <ul class="list-item news-list-ul">
            <li><a href="#">হাবিপ্রবি আর্কিটেকচার ২য় ব্যাচের শিক্ষার্থীদের ভ্রমণ</a></li>
            <li><a href="#">প্রধানমন্ত্রীর শিক্ষা সহায়তা ট্রাস্ট এর পরিচিতি</a></li>
            <li><a href="#">শাবি শিক্ষার্থীদের স্বপ্নবাংলা গ্রুপের উদ্বোধন</a></li>
            <li><a href="#">‘মাদরাসা শিক্ষার প্রসারে কাজ করছে সরকার’</a></li>
            <li><a href="#">হাবিপ্রবি আর্কিটেকচার ২য় ব্যাচের শিক্ষার্থীদের ভ্রমণ</a></li>
            <li><a href="#">প্রধানমন্ত্রীর শিক্ষা সহায়তা ট্রাস্ট এর পরিচিতি</a></li>
            <li><a href="#">প্রধানমন্ত্রীর শিক্ষা সহায়তা ট্রাস্ট এর পরিচিতি</a></li>
            <li><a href="#">হাবিপ্রবি আর্কিটেকচার ২য় ব্যাচের শিক্ষার্থীদের ভ্রমণ</a></li>
            <li><a href="#">প্রধানমন্ত্রীর শিক্ষা সহায়তা ট্রাস্ট এর পরিচিতি</a></li>
        </ul>
    </div>
</div>
<div class="col-lg-4 one-column-news-list">
    
    <div class="news-list-head">
        পাবলিক বিশ্ববিদ্যালয়
    </div>
    <div class="section-top-news">
        <div class="image-container-vh section-top-news-image">
            <img src="<?php echo theme_url() ?>/images/image17.jpg">
        </div>
        <h2 class="section-top-news-title">
            <a href="#">
                ব্যাংক খাতে শৃঙ্খলা ফেরানোর পরামর্শ
            </a>
        </h2>
    </div>


    <ul class="list-item news-list-ul">
        <li><a href="#">হাবিপ্রবি আর্কিটেকচার ২য় ব্যাচের শিক্ষার্থীদের ভ্রমণ</a></li>
        <li><a href="#">প্রধানমন্ত্রীর শিক্ষা সহায়তা ট্রাস্ট এর পরিচিতি</a></li>
        <li><a href="#">শাবি শিক্ষার্থীদের স্বপ্নবাংলা গ্রুপের উদ্বোধন</a></li>
        <li><a href="#">‘মাদরাসা শিক্ষার প্রসারে কাজ করছে সরকার’</a></li>
    </ul>
</div>
-->
<?php

function top_news_function_tc($row){
    $res = '<div class="section-top-news">';
        $res .= '<div class="image-container-vh section-top-news-image">';
            $res .= image_from_content($row['body']);
        $res .= '</div>';
        $res .= '<h2 class="section-top-news-title">';
            $res .= '<a href="' . news_single_url($row['content_id']) . '">';
                $res .= $row['content_title'];
            $res .= '</a>';
        $res .= '</h2>';
        $res .= '<p class="section-top-excerpt">';
            $res .= strip_tags(make_excerpt($row['body'], 800));
        $res .= '</p>';
    $res .= '</div>';
    return $res;
}

function top_news_function_main($row){
    
    $res  = '<div class="section-top-news">';
        $res .= '<div class="image-container-vh section-top-news-image">';
            $res .= image_from_content($row['body']);
        $res .= '</div>';
        $res .= '<h2 class="section-top-news-title">';
            $res .= '<a href="' . news_single_url($row['content_id']) . '">';
                $res .= $row['content_title'];
            $res .= '</a>';
        $res .= '</h2>';
    $res .= '</div>';
    return $res;
    
}

function other_news_function_main($each){   
    $res = '<li>';
        $res .= '<a href="'.  news_single_url($each['content_id']).'">'.$each['content_title'].'</a>';
    $res .= '</li>';
    return $res;
}

function print_section($configs = array(), $class_id, $limit = 10, $col_size = 1){
    $heading_back = isset($configs['heading_back']) ? $configs['heading_back']: "#00aeed";
    $heading_color = isset($configs['heading_color']) ? $configs['heading_color']: "#333333";
    $class = isset($container_config['class']) ? $container_config['class']:""; 
    $style = isset($container_config['style']) ? $container_config['style']:""; 
    $id = isset($container_config['id']) ? $container_config['id']:""; 
?>
        <?php  
            //echo $class ? " class='".$class."' ":" "; 
            //echo $id ? " id='".$id."' ":" ";
            //echo $style ? " style='".$style."' ":" "; 
        ?>
        <?php
            $title = "(untitled)";
            $url_ref = "";
            $ne['top'] = "";
            $ne['other'] = "";
            if($cat = get_cat_row($class_id)){
                $title = $cat['name'];
                $url_ref = $cat['url_ref'];
                if($col_size == 2){
                    $ne = newslist($url_ref, "top_news_function_tc", "other_news_function_main", $limit);
                }else {
                    $ne = newslist($url_ref, "top_news_function_main", "other_news_function_main", $limit);
                }
            }
        ?>
        
        <!--One Col<div class="col-lg-4 one-column-news-list">-->
        
        <?php if($col_size == 2): ?>
        <div class="col-lg-8 two-column-news-list">
        <?php elseif($col_size == 1): ?>
        <div class="col-lg-4 one-column-news-list">
        <?php endif; ?>
            <div class="news-list-head" style="background-color: <?php echo $heading_back?>; color: <?php echo $heading_color?>">
                <?php echo $title; ?>
            </div>
            <?php if($col_size == 2): ?>
            <div class="col-lg-6 two-colmn-left">
            <?php endif; ?>
                <?php echo $ne['top']; ?>
            <?php if($col_size == 2): ?>
            </div>
            <?php endif; ?>

            <?php if($col_size == 2): ?>
            <div class="col-lg-6 two-colmn-right">
            <?php endif; ?>
                <ul class="list-item news-list-ul">
                    <?php echo $ne['other']; ?>
                </ul>
            <?php if($col_size == 2): ?>
            </div>
            <?php endif; ?>
            
        </div>
<?php } ?>       

<?php
function sidebar_news_list($row, $ac, $i){
    $i++;
    $syle = "style='";
    
    if($i%2 == 1) {
        $syle .= "padding-right: 5px";
    }else {
        $syle .= "padding-left: 5px";
    }
    
    $syle .= "'";
    
    $result = "<div class='sidebar-news-element' ".$syle.">";
        $result .= "<a href='" . news_single_url($row['content_id']) . "' title='".$row['content_title']."'>";
            $result .= "<div class='image-container-vh sidebar-news-image-container'>";
                $result .= image_from_content($row['body']);
            $result .= "</div>";
        $result .= "</a>";
    $result .= "</p>";
        $result .= "<a href='" . news_single_url($row['content_id']) . "' title='".$row['content_title']."'>";
        $result .= $row['content_title'];
        $result .= "</a>";
    $result .= "</p>";
    $result .= "</div>";
    
    return $result;
}

function print_sidebar_section($class_id, $limit = 10, $configs = array()){
    $heading_back = isset($configs['heading_back']) ? $configs['heading_back']: "#00aeed";
    $heading_color = isset($configs['heading_color']) ? $configs['heading_color']: "#333333";
  
    $title = "(untitled)";
    $url_ref = "";

    $ne = false;
    if($class_id){
        if($cat = get_cat_row($class_id)){
            $title = $cat['name'];
            $url_ref = $cat['url_ref'];
            $ne = get_news_by_cat("sidebar_news_list", $url_ref, $limit);
        }
    }
?>          
        <div class="other">
            <div class="section-header other-section-head" style="background-color: <?php echo $heading_back?>; color: <?php echo $heading_color?>">    
                <?php echo $title; ?>
            </div>
            <div class="other-body">
                <?php
                if($ne){
                    if($ne->get_total()){
                        echo $ne->show_data(); 
                    }
                }
                ?>
            </div>
        </div>
<?php
}  


function sidebar_versity_list($row){
    $result = "<div class='university-list-item' >";
        $result .= "<div class='image-container-vh university-image-container' >";
                $result .= "<a href='" . news_single_url($row['content_id']) . "' >";
                    $result .= image_from_content($row['body']);
                $result .= "</a>";
        $result .= "</div>";
        $result .= "<h2 class='university-title' >";
            $result .= "<a href='" . news_single_url($row['content_id']) . "' >";
                $result .= $row['content_title'];
            $result .= "</a>";
        $result .= "</h2>";
        $result .= "<div class='university-description' >";
            $result .= make_excerpt($row['body'], 200);
        $result .= "</div>";
    $result .= "</div>";
    return $result;
}

function print_sidebar_versity_info($class_id, $limit = 10, $configs = array() ){
    $heading_back = isset($configs['heading_back']) ? $configs['heading_back']: "#00aeed";
    $heading_color = isset($configs['heading_color']) ? $configs['heading_color']: "#333333";
    
    $title = "(untitled)";
    $url_ref = "";
    if($cat = get_cat_row($class_id)){
        $title = $cat['name'];
        $url_ref = $cat['url_ref'];
        $ne = get_news_by_cat("sidebar_versity_list", $url_ref, $limit);
    }
?>          
        <div class="other">
            <div class="section-header other-section-head" style="background-color: <?php echo $heading_back?>; color: <?php echo $heading_color?>">    
                <?php echo $title; ?>
            </div>
            <div class="other-body">
                <?php 
                if($ne->get_total()){
                    echo $ne->show_data(); 
                }
                ?>
            </div>
        </div>
<?php
}  
     

function news_headline_list($each){
    $images = get_image_links($each['body'],array('src','title','alt'));

    $res = '<div class="slider-element">';
    $res .= '<div class="slider-image-container" style="text-align: center">';
        /*$res .= '<img src="<?php echo theme_url() ?>/images/image1.jpg" >';*/
        if(count($images)){
            $src = $images[0]['src'];
            $alt = $images[0]['alt'];
            $res .= '<img src="'.$src.'" alt="'.$alt.'" />';
        }else {
            $res .= '<img src="'. themePaths() . '/images/news-thumb.png" alt="cover-image" />';
        }

    $res .= '</div>';
        $res .= '<div class="slider-text-container">';
            $res .= '<h1 class="slider-text-head">';
                $res .= $each['content_title'];
            $res .= '</h1>';
            $res .= '<p class="slider-text-body">';
                $res .= make_excerpt(strip_tags($each['body']), 400)."... ";
                $res .= "<a href='".news_single_url($each['content_id'])."'>বিস্তারিত</a>";
            $res .= '</p>';
        $res .= '</div>';
    $res .= '</div>';
    return $res;
}

function news_gallery_list($each){
    $images = get_image_links($each['body'],array('src','title','alt'));

    $res = '<div class="gallery-element">';
        $res .= '<div class="gallery-image-container">';
            /*$res .= '<img src="<?php echo theme_url() ?>/images/image1.jpg" >';*/
            if(count($images)){
                $src = $images[0]['src'];
                $alt = $images[0]['alt'];
                $res .= '<img src="'.$src.'" alt="'.$alt.'" />';
            }else {
                $res .= '<img src="'. themePaths() . '/images/news-thumb.png" alt="cover-image" />';
            }
        $res .= '</div>';

        $res .= '<div class="gallery-image-title">';
            $res .= '<h1 class="gallery-image-title-in">';
                $res .= $each['content_title'];
            $res .= '</h1>';
        $res .= '</div>';

    $res .= '</div>';
    return $res;
}

function theme_default_image(){
    return '<img src="'. themePaths() . '/images/news-thumb.png" alt="cover-image" />';
}

function image_from_content($content){
    $img = "";    
    $images = get_image_links($content, array('src','title','alt'));
    if(count($images)){
        $src = $images[0]['src'];
        $alt = $images[0]['alt'];
        $img = '<img src="'.$src.'" alt="'.$alt.'" />';
        return $img;
    }
    return '<img src="'. themePaths() . '/images/news-thumb.png" alt="cover-image" />';
}

function news_top_right($each){  
    $res = "";
    
    $res .= '<div class="top-right-element">';
        $res .= '<div class="image-container top-right-element-img-cont">';
            $res .= image_from_content( $each['body'] );
        $res .= '</div>';
        $res .= '<div class="top-right-element-text">';
            $res .= '<a href="'.news_single_url($each['content_id']).'"><h2>'.$each['content_title'].'</h2></a>';
        $res .= '</div>';
    $res .= '</div>';
    
    return $res;
}

function second_column_list($each){
    $res = "";
    $res .= '<div class="news-col-element">';
        $res .= '<div class="image-container-vh news-col-element-img">';
            $res .= '<a href=" ' . news_single_url($each['content_id']) .'">';
                $res .= image_from_content( $each['body'] );
            $res .= '</a>';
        $res .= '</div>';
        $res .= '<div class="news-col-element-content">';
            $res .= '<h2 class="news-col-element-head">';
                $res .= '<a href="' . news_single_url($each['content_id']) . '">'.$each['content_title']."</a>";
            $res .= '</h2>';
            $res .= '<p class="news-col-element-body">';
            $res .= make_excerpt($each['body'], 500);
            $res .= '</p>';
        $res .= '</div>';
    $res .= '</div>';
    return $res;
}

function recent_news_list($each){
    $res = "";
    $res .= '<div class="recent-news-element">';
        $res .= '<div class="image-container-vh recent-element-img">';
            $res .= image_from_content( $each['body'] );
        $res .= '</div>';
        $res .= '<div class="recent-news-element-content">';
            $res .= '<h2 class="recent-news-element-head">';
                $res .= '<a href="'.news_single_url($each['content_id']).'">'.$each['content_title'].'</a>';
            $res .= '</h2>';
            $res .= '<p class="recent-news-element-date">';
                $res .= $each['added_date'];
            $res .= '</p>';
        $res .= '</div>';
    $res .= '</div>';
    
    return $res;
}


function news_single_url($id){
    $news_single_page = "single";
    return base_url()."/".$news_single_page."/".$id;
}

function spcial_news_list($each){
    $res = "";
    $res .= '<div class="spcial-news-element">';
        $res .= '<div class="spcial-news-spcaial-line">';
            $res .= $each['content_subtitle'];
        $res .= '</div>';
        $res .= '<h1 class="spcial-news-element-head">';
            $res .= $each['content_title'];
        $res .= '</h1>';
        $res .= '<div class="image-container-vh spcial-news-image-cont">';
            $res .= image_from_content($each['body']);
        $res .= '</div>';
            $res .= '<p class="spcial-news-content">';
            $res .= make_excerpt($each['body'], 1500);
            $res .= ' <a href="' . news_single_url($each['content_id']) . '" class="news-details">বিস্তারিত</a>';
        $res .= '</p>';
        $res .= '<p class="spcial-news-date-comment">';
            $res .= '<span class="glyphicon glyphicon-time"></span> ' . $each['added_date'] . ' | ';
            $res .= '<span class="glyphicon glyphicon-comment"></span> ' . $each['total_comments'];
        $res .= '</p>';
    $res .= '</div>';
    
    return $res;
}

function show_bracecrumb($content_id){
    $title = _get('content', "content_id", $content_id, 'content_title');
    
    //if content id is the url ref of class table
    $idFromUrlRef = _get("class", "url_ref", $content_id, "class_id");
    
    $remove_id = array();
    $meta = load()->sys("MetaInfo")->run("site_info", "info_key", "info_value");

    $headline_class_url = $meta->get(get_config("meta_attr/headlines_class_key"));
    $spcial_news_class_url = $meta->get(get_config("meta_attr/spcial_news_class_key"));

    $remove_id[] = _get("class", "url_ref", $headline_class_url, "class_id");
    $remove_id[] = _get("class", "url_ref", $spcial_news_class_url, "class_id");

    $new_list = array();
    
    $expected_id = 0;
    $c = 0;
    $relation = DB::run()->read('class_relationship')->where('entity_id', $content_id)->run_sql( );
    
    if(!$relation->get_count()){
        $relation = DB::run()->read('class_relationship')->where('class_id', $idFromUrlRef )->run_sql( );
    }
    
    if($relation->get_count()){
        $relations = $relation->get_array();
        $pa = 0;
        $num = 0;
        
        foreach ($relations as $rel){
            if(!in_array($rel['class_id'], $remove_id )){
                $c_type = DB::run()->read('class_type')->where('class_id', $rel['class_id'])->run_sql();
                $ct_value = $c_type->get_first_obj();
                $parent = $ct_value->parent;
                if($pa < $parent){
                    $expected_id = $ct_value->class_id;
                }
                $pa = $parent;
                $new_list[]  = $rel['class_id'];
                //echo "Test Case: ".++$num."<br>";
                //echo "Class ID: ".$ct_value->class_id."<br>";
                //echo "Value of \$pa: ".$pa."<br><br>";
            }
        }
        if(count($new_list)){
            $expected_id = $new_list[0];
        }else {
            $expected_id = $remove_id[0];
        }
        
        $c = $expected_id;
    }else {
       $c  = _get("class", "url_ref", $content_id, "class_id");
    }
    

    $cl_array = array();

    $parent = 1000;
    $n=0;
    while($parent){
        $cDb = DB::run()->read('class')->extra("natural join class_type")->where('class_id', $c)->run_sql();
        if($cDb->get_count()){
            $class = $cDb->get_first();
            $cl_array []= $class;
            //echo "Test Case: ".++$n."<br>";
            $parent = (int) $class['parent'];
            $c = (int) $class['parent'];
        }  else {
            $parent = 0;
        }
    }


    
    $list = array();
    
    
    
    $class = _get("class", "class_id", $expected_id);
    $class_name  = (isset($class[0]['name'])) ? $class[0]['name']:""; 
    $class_id  = (isset($class[0]['class_id'])) ? $class[0]['class_id']:""; 
?>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">হোম</a></li>
        <?php for($i = (count($cl_array)-1); $i > 0; $i--): ?>
        <li><a href="<?php echo class_href($cl_array[$i]['class_id']); ?>"><?php echo $cl_array[$i]['name']; ?></a></li>
        <?php endfor; ?>
        <?php if(!$title):?>
        <li class="active"><?php echo isset($cl_array[0]['name']) ? $cl_array[0]['name']:""; ?></li>
        <?php else:?>
        <li><a href="<?php echo class_href($cl_array[0]['class_id']); ?>"><?php echo $cl_array[0]['name']; ?></a></li>
        <li class="active"><?php echo $title; ?></li>
        <?php endif;?>
    </ol>
<?php 
} 

function commentsList($row){
    return "<div class='name'>".$row['name']."</div>  ".$row['comment_body'];
}



function get_comments($function, $content_id = 0){
    $config = array('approved' => 1);

    $comments = load()->lib("CommentList")->run($content_id, $config);
    
    $comments->set_html("div", true, "div class='comment-item'");
    $comments->set_is_tree(true);
   
    $comments->set_elemets_oper($function);

    return $comments;
}

function class_href($id){
    $column = (is_numeric($id) ) ? "class_id":"url_ref"; 
    $url_ref = _get('class', $column, $id, 'url_ref');
    $category_ref = get_config("category/reference");
    return base_url()."/".$category_ref."/".$url_ref;
}