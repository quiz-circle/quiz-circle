
<?php page_include("includes/function.php") ?>
<!DOCTYPE html>  
<html>
    <head>
        <title><?php print_title(); ?> </title>        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?php theme_url()?>/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php theme_url()?>/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<?php theme_url()?>/css/style.css">
        <link rel="stylesheet" href="<?php plugin_url("jsshare")?>/dist/jssocials.css">
        <link rel="stylesheet" href="<?php plugin_url("jsshare")?>/dist/jssocials-theme-flat.css">
        
        <script src="<?php theme_url()?>/javascript/jQuery.js"> </script> 
        <script src="<?php plugin_url("jsshare") ?>/dist/jssocials.js"> </script> 
        <script src="<?php theme_url()?>/javascript/bootstrap.min.js"> </script>
        <script src="<?php theme_url()?>/javascript/common.js"> </script>
        
        <link rel="stylesheet" href="<?php theme_url()?>/css/content.css">
        <script>
            var ABSPATH = '<?php echo ABSPATH; ?>';
            var theme_path = '<?php echo theme_url(); ?>';
            var base_url = '<?php echo base_url(); ?>';
            var admin_url = '<?php echo themePaths(); ?>';
        </script>
    </head>

    <body>
    <?php    
    function navigation_lists($e, $ac, $th){
        $t = "";
        $c = "";
        if($e['link_target'] != "") {
            $t = "target='".$e['link_target']."'";
        }

        if($e['li_class'] != "") {
            $c =  "class='".$e['li_class']."'";
        }

        if($th->hasChild($e['item_id'])){
            $res = '<a class="dropdown-toggle" data-toggle="dropdown" href="' . __alt($e['ref_url']) . '">' . $e['item_title'] . ' <span class="caret"></span></a>';
            return $res;
        }

        return "<a href='" . __alt($e['ref_url']) . "' ".$c." ".$t." >" . $e['item_title'] . "</a>";
    }

    $n = load()->lib("BootstrapNav")->run('header-nav');
    $n->set_is_tree(true);
    $n->set_elemets_oper("navigation_lists");
    
    
    


    

    
    ?>
        <style>
            #header-top {
                padding: 0;
                width: 100%;
                margin: 20px 5px;    
            }

            .header-top-nav {
                width: 100%;
                float: left;
            }

            .header-top-nav ul {
                margin: 0;
                padding: 0;
                float: right;
                list-style: none;
                text-align: center;
                border-bottom: 3px solid #999;;
            }

            .header-top-nav ul li{
                display: inline;
                float: left;
                text-align: center;
                padding: 0 8px;
            }

            .header-top-nav ul li a {
                color: #999;
            }
            
            #header-top {
            }
            
            #search-and-social {
                margin: 0;
            }
            
            #header-top-nav {
                margin: 0;
            }
            
        </style>
        
        <header class="container">
            <div id="header-top" class="row">
                <div class="col-lg-5 col-sm-12" id="search-and-social">
                    <div class="col-lg-6 col-sm-12 search">
                        <form action="" method="get">
                            <input type="text" name="search"><input type="submit" value="Search" >
                        </form>
                    </div>
                    <div class="col-lg-6  col-sm-12 social-link">
                        <ul>
                            <li><a class="fb" href="">F</a></li>
                            <li><a href= ""  class="G-plus">G+</a></li>
                            <li><a href=""  class="in">LI</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-7 col-sm-12" id="header-top-nav">
                    <div class="header-top-nav">
                        <ul>
                            <li>
                                <a href="#">শিক্ষা বৃত্তি</a>
                            </li>
                            <li>
                                <a href="#">ভর্তি পরীক্ষা</a>
                            </li>
                            <li>
                                <a href="#">বিদেশে উচ্চ শিক্ষা</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div id="header-bottom" class="row">
                <div class="col-lg-2  col-md-2 col-sm-2 col-xs-12" style="">
                    <div class="logo">
                        <a href="#">
                            LOGO
                        </a>
                    </div>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-10  col-xs-12" style="border:1px solid white;">
                    <nav class="navbar" id="navbar-custom" style="margin-bottom: 0;  ">
                        <div class="container" style="margin: 0; padding: 0">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>                        
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="myNavbar">
                                <ul class="nav navbar-nav">
                                    <li class="active"> <a href="<?php echo base_url();?>" class="glyphicon glyphicon-home" title="Home"> </a></li>
                                    <?php
                                        if($n->get_total()){
                                            $n->show_data("priority");
                                        }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
        
        


