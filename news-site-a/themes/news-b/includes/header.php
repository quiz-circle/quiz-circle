
<?php page_include("includes/function.php") ?>
<!DOCTYPE html>  
<html>
    <head>
        <title><?php print_title(); ?> </title>        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?php theme_url()?>/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php theme_url()?>/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<?php theme_url()?>/css/style.css">
        <link rel="stylesheet" href="<?php plugin_url("jsshare")?>/dist/jssocials.css">
        <link rel="stylesheet" href="<?php plugin_url("jsshare")?>/dist/jssocials-theme-flat.css">
        
        <script src="<?php theme_url()?>/javascript/jQuery.js"> </script> 
        <script src="<?php plugin_url("jsshare") ?>/dist/jssocials.js"> </script> 
        <script src="<?php theme_url()?>/javascript/bootstrap.min.js"> </script>
        <script src="<?php theme_url()?>/javascript/common.js"> </script>
        
        <link rel="stylesheet" href="<?php theme_url()?>/css/content.css">
        <script>
            var ABSPATH = '<?php echo ABSPATH; ?>';
            var theme_path = '<?php echo theme_url(); ?>';
            var base_url = '<?php echo base_url(); ?>';
            var admin_url = '<?php echo themePaths(); ?>';
        </script>
    </head>

    <body>
        <style>
            #header-top {
                padding: 0;
                width: 100%;
                margin: 20px 5px;    
            }

            .header-top-nav {
                width: 100%;
                float: left;
                margin-top: 2px;
            }

            .header-top-nav ul {
                margin: 0;
                padding: 0;
                float: right;
                list-style: none;
                text-align: center;
                border-bottom: 3px solid #999;;
            }

            .header-top-nav ul li{
                display: inline;
                float: left;
                text-align: center;
                padding: 0 8px;
            }

            .header-top-nav ul li a {
                color: #999;
            }
            
            #header-top {
            }
            
            #search-and-social {
                margin: 0;
            }
            
            #header-top-nav {
                margin: 0;
            }
            
            
            .header-menu ul {
                width: 100%;
                list-style-type: none;
                border: 1px solid #333;
                float: left;
                padding: 0;
                margin: 0;
                position: relative;
                border-left:none;
                border-right:none;
                z-index: 1000;
            }

            .header-menu ul li {
                display: inline-block;
                float: left;
                border-right: 1px solid #555;
                padding-bottom: 2px;
                height: 33px;
                border-bottom: 1px solid #333;
            }

            .header-menu ul li a {
                //border: 1px solid red;
                padding: 5px 8px;
                color:#333;
                float: left;
                text-decoration: none;
                transition: 0.1s;
                font-size: 15px;
            }
            .header-menu ul li a span.caret{
                border-top-color: #00AAE8;
            }

            .header-menu ul li a:hover, .header-menu ul li a.active {
                background-color: #555;
                color: #FFF;
                border-top: 2px solid #00AAE8;
            }



            .header-menu ul li ul {
                display: none;
                position: absolute;
                width: 100%;
                top: inherit;
                left: 0;
                transition: 1s display;
                background-color: rgba(200, 255, 200, 0.9);
                border-bottom-color: #e74c4f; 
            }
            .header-menu ul li:hover > ul{
                display: block;
            }
            
            #search-and-social input[type='text'] {
                height: 30px;
                outline: none;
                margin-top: 0;
                vertical-align: top;
                margin-top: 1px;
                padding: 3px 4px;
                border-width: 1px 0 1px 1px ;
            }
            
            #search-and-social input[type='submit'] {
                color: #999;
                height: 30px;
                font-size: 15px;
                margin-top: 0;
                vertical-align: top;
            }
            
            #search-and-social .social-link ul{
                display: inline-block;
                float: left;
                padding: 0;
                margin: 0;
                margin-top: 1px;
            }
            
            #search-and-social .social-link ul li{
                display: inline-block;
                float: left; 
                margin-right: 2px;
            }
            
            #search-and-social .social-link ul li a{
                border: 1px solid green;
                height: 30px;
                width: 30px;
                text-align: center;
                padding-top: 5px;
                float: left;
            }
            
        </style>
        
        <header class="container">
            <div  class="row"  style=" padding-top: 15px; padding-bottom: 15px;">
                <div class="col-lg-7 col-sm-12" id="search-and-social" style="padding-left: 0;">
                    <div class="col-lg-4 col-sm-12 search">
                        <form action="<?php echo base_url()?>/search" method="get">
                            <input type="text" name="query" value="<?php echo GET_DATA("query")?>" placeholder="Search For a content"><input type="submit" value="&#xe003;" class="glyphicon" >
                        </form>
                    </div>
                    <div class="col-lg-8  col-sm-12 social-link">
                        <ul>
                            <li><a class="fb" href="">F</a></li>
                            <li><a href= ""  class="G-plus">G+</a></li>
                            <li><a href=""  class="in">LI</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-5 col-sm-12" id="header-top-nav">
                    <div class="header-top-nav">
                        <ul>
                            <li>
                                <a href="#">শিক্ষা বৃত্তি</a>
                            </li>
                            <li>
                                <a href="#">ভর্তি পরীক্ষা</a>
                            </li>
                            <li>
                                <a href="#">বিদেশে উচ্চ শিক্ষা</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div id="header-bottom" class="row" style="">
                <div class="col-lg-2  col-md-2 col-sm-2 col-xs-12">
                    <div class="logo">
                        <a href="<?php echo base_url(); ?>">
                            <img src="<?php theme_url() ?>/images/news.png">
                        </a>
                    </div>
                </div>
                <?php    
                    function navigation_lists($e, $ac, $l, $c, $th){
                        $t = "";
                        $c = "";
                        if($e['link_target'] != "") {
                            $t = "target='".$e['link_target']."'";
                        }

                        if($e['li_class'] != "") {
                            $c =  "class='".$e['li_class']."'";
                        }

                        $child_icon = '';
                        if($th->hasChild($e['item_id'])){
                            $child_icon = '<span class="caret"></span>';
                        }
                        return "<a href='" . __alt($e['ref_url']) . "' ".$c." ".$t." >" . $e['item_title'] . $child_icon . "</a>";
                    }
                    $n = load()->lib("NavList")->run('header-nav');
                    $n->set_is_tree(true);
                    $n->set_html("ul", true, "li");
                    $active = (page_name() == "home" || page_name() == "index.php") ? "class='active'":"";
                    $n->init_html("<li><a href='".  base_url()."' ".$active.">প্রচ্ছদ</a><li>");
                    $n->set_elemets_oper("navigation_lists");
                ?>
                <div class="col-lg-10 col-md-10 col-sm-10  col-xs-12"  style="">
                    <nav class="header-menu">
                        <?php
                            if($n->get_total()){
                                echo $n->get_data("priority");
                            }
                        ?>
                    </nav>
                </div>
                
                <script>
        
                    $(".header-menu ul li").each(function(i){
                        $(this).on("mouseover", function(){
                            var th = $(this);
                            var target  = th.find("ul:first");
                            var targetTop = th[0].offsetTop;
                            target.css("top", (targetTop+33) + "px");
                        });
                    });

                </script>
            </div>
        </header>
        
        


