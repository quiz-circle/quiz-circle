        <footer  class="container">
            
            <ul class="row footer-top" id="footer-top">
                <li class="col-lg-2" id="footer-top-column">
                    <div class="menu-title">
                        Educational Institute
                    </div>
                    <ul id="footer-top-list">
                        <li><a href="http://localhost/news-site-a/university">University</a></li>
                        <li><a href="#">National University</a></li>
                        <li><a href="#">Medical & Related</a></li>
                        <li><a href="#">Diploma Institute</a></li>
                        <li><a href="#">Intermediate College</a></li>
                        <li><a href="#">Madrasha</a></li>
                        <li><a href="#">Subject Comparison</a></li>
                        <li><a href="#">Subject Search</a></li>
                        <li><a href="#">Admission Center</a></li>                 
                    </ul>
                </li>
                <li class="col-lg-2" id="footer-top-column">
                    <div class="menu-title">
                        Scholarship
                    </div>
                    <ul id="footer-top-list">
                        <li><a href="#">District Council</a></li>
                        <li><a href="#">Foreign Govt.(Ministry)</a></li>
                        <li><a href="#">Foreign Govt.(Others)</a></li>
                        <li><a href="#">Foreign Organization</a></li>
                        <li><a href="#">Bangladeshi Organization</a></li>
                        <li><a href="#">Scholarship Archive</a></li>
                    </ul>
                </li>
                <li class="col-lg-2" id="footer-top-column">
                    
                    <div class="menu-title">
                        Study Abroad & Visa
                    </div>
                    <ul id="footer-top-list">
                        <li><a href="#">Study Abroad</a></li>
                        <li><a href="#">Student Visa</a></li>
                        <li><a href="#">Counseling Firm</a></li>
                        <li><a href="#">Interview</a></li>
                        <li><a href="#">Standard Test</a></li>
                        <li><a href="#">Online Fee Payment</a></li>
                    </ul>
                </li>
                <li class="col-lg-2" id="footer-top-column">
                    <div class="menu-title">
                        Circular Corner
                    </div>
                    <ul id="footer-top-list">
                        <li><a href="#">Govt. University Undergraduate</a></li>
                        <li><a href="#">Govt. University Masters</a></li>
                        <li><a href="#">Private University Undergraduate</a></li>
                        <li><a href="#">Private University Masters</a></li>
                        <li><a href="#">Institute Under Govt University</a></li>
                        <li><a href="#">Institute Under NU</a></li>
                        <li><a href="#">Nursing Institute</a></li>
                        <li><a href="#">Polytechnic Institute</a></li>
                        <li><a href="#">All Circular</a></li>
                    </ul>
                </li>
                <li class="col-lg-2" id="footer-top-column">
                    <div class="menu-title">Information</div>
                    <ul id="footer-top-list">
                        <li><a href="#">News</a></li>
                        <li><a href="#">Notice</a></li>
                        <li><a href="#">Feature</a></li>
                        <li><a href="#">Interview</a></li>
                        <li><a href="#">Forum</a></li>
                        <li><a href="#">Education Statistics</a></li>
                    </ul>
                </li>
                <li class="col-lg-2" id="footer-top-column">
                    <div class="menu-title">Information</div>
                    <ul id="footer-top-list">
                        <li><a href="#">News</a></li>
                        <li><a href="#">Notice</a></li>
                        <li><a href="#">Feature</a></li>
                        <li><a href="#">Interview</a></li>
                        <li><a href="#">Forum</a></li>
                        <li><a href="#">Education Statistics</a></li>
                    </ul>
                </li>
            </ul>
            
            <div class="row footer-in" id="footer-nav-list">
                <?php                
                    
                    $c = array(
                        array("class" => "footer-nav")
                    );

                    function footer_item($r, $ac, $l){
                        $result = $l == 1 ? "<a href='javascript:'>":"";
                        $result .= $r['item_title'];
                        $result .= $l == 1 ? "</a>":"";
                        return  $result;
                    }

                    $class_name = "footer-nav";
                    $class_id = _get('class', 'name', $class_name, 'class_id');
                    
                ?>

                <style>

                    #footer-nav-column ul {
                        -moz-column-count: 3;
                        -moz-column-gap: 5px;
                        -webkit-column-count: 3;
                        -webkit-column-gap: 5px;
                        column-count: 3;
                        column-gap: 5px;
                    }
                </style>
                

                <div class="col-lg-12" id="footer-nav-column">
                        <?php get_nav_items("footer_item", $class_id, "ul",$c); ?>
                </div>
                <!--
                <div class="col-lg-3" id="footer-nav-column">
                    <ul class="footer-nav">
                        <li><a href="#">বিদেশী সংস্থা</a></li>
                        <li><a href="#">বিদেশী সংস্থা</a></li>
                        <li><a href="#">ভর্তি তথ্য</a></li>
                        <li><a href="#">সরকারি বিশ্ববিদ্যালয়</a></li>
                        <li><a href="#">বেসরকারি বিশ্ববিদ্যালয়</a></li>
                        <li><a href="#">বেসরকারি বিশ্ববিদ্যালয়</a></li>
                        <li><a href="#">জাতীয় বিশ্ববিদ্যালয়</a></li>
                    </ul>
                </div>
                <div class="col-lg-3" id="footer-nav-column">
                    <ul class="footer-nav">
                        <li><a href="#">পলিটেকনিক ইন্সটিটিউট</a></li>
                        <li><a href="#">মেডিকেল কলেজ</a></li>
                        <li><a href="#">জাতীয় বিশ্ববিদ্যালয়</a></li>
                        <li><a href="#">বিদেশী সংস্থা</a></li>
                        <li><a href="#">সরকার বিদেশী</a></li>
                    </ul>
                </div>
                
                -->
            </div>
        </footer>
    </body>
</html>        