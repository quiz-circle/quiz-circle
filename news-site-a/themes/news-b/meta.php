<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

page_include("function.php");

$a = array(
        "r1" => array(
            "d1"    =>  array(
                    "col-size" => "1",
                    "c_id" => 2,
                    "head_back_color" => "124687",
                ),
            "d2"    =>  array(
                    "col-size" => "1",
                    "c_id" => 3,
                    "head_back_color" => "ec123a",
            ),
            "d3"    =>  array(
                    "col-size" => "1",
                    "c_id" => 4,
                    "head_back_color" => "ac587a",
            )
        ),
        "r2" => array(
            "d1"    =>  array(
                    "col-size" => "2",
                    "c_id" => 13,
                    "head_back_color" => "31ca87",

            ),
            "d2"    =>  array(
                    "col-size" => "1",
                    "c_id" => 14,
                    "head_back_color" => "13726a",
            ),
            "d3"    =>  array(
                    "col-size" => "1",
                    "c_id" => 6,
                    "head_back_color" => "13726a",
            )
        ),
        "r3" => array(
            "d1"    =>  array(
                    "col-size" => "2",
                    "c_id" => 7,
                    "head_back_color" => "31ca87",

            ),
            "d2"    =>  array(
                    "col-size" => "1",
                    "c_id" => 8,
                    "head_back_color" => "13726a",
            ),
            "d3"    =>  array(
                    "col-size" => "1",
                    "c_id" => 5,
                    "head_back_color" => "13726a",
            )
        ),
    
    );

$db = json_encode($a);

/*
echo $db;

echo "<br>";
echo strlen($db);
echo "<br>";
echo "<br>";

$informations = json_decode($db, true);

foreach ($informations as $row => $info) {
    echo $row .":&raquo; ";
    
    foreach ($info as $d_key => $data){
        echo " ".$d_key.": ( ";
        echo $data["c_id"]." ";
        echo ") ";
    }
    echo "<br>";
} 
*/

?>
<style>
    
    .test-sec {
        border:1px solid red;
        width: 100%;
        float: left;
    }
    
    div#lead-news {
        width: 100%;
        float: left;
    }
    
    div#lead-news img {
        width: 100%;
    }
    
    h2 {
        width: 100%;
        float: left;
        font-size: 14px;
        margin: 0;
        padding: 0;
    }
    
    #section-header h2 {
        width: 100%;
        float: left;
        text-align: center;
        height: 35px;
        font-size: 20px;
        line-height: 35px;
    }
    
    .section-row {
        width: 960px;
        float: left;
    }
    
    .sec-row-single {
        width: 300px;
        float: left;
        padding: 8px 10px;
    }
    
</style>
<?php


$meta_sql = "SELECT * FROM theme_info";

$meta_list = load()->sys('contentList')->run($meta_sql);


function meta_list($each){
    $meta = "";
    $meta .= $each['info_key'];
    $meta .= " <a href='./meta?action=edit&key=".$each['info_key']."'>Edit</a>";
    return $meta;
}


$meta_list->set_elemets_oper("meta_list");

if(!GET_DATA('key')){
    $meta_list->show_data();
}else {
    $key = GET_DATA('key');
        $meta = load()->sys('metaInfo')->run("theme_info", "info_key", "info_value");
        
        
        
        ?>  
        <form action="" method="post">
            <textarea name="meta_value"><?php echo POST_DATA("meta_value", $meta->get($key))?></textarea>
            <br><br>
            <input type="submit" value="Edit Meta">
        </form>
        <br>
        <a href="./meta">Meta List</a>
    <?php
}