<?php  print_header(); ?>
        <div class="container" >
            <div class="row section-top" id="section-top">
                <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12" id="slider">
                    
                    <?php 
                        $sec_key = get_config("meta_attr/section_key"); //section key
                        $sec_key_right = get_config("meta_attr/section_key_right"); //section key
                        $meta = load()->sys("MetaInfo")->run("theme_info","info_key", "info_value");
                        
                        $section_info = $meta->get($sec_key);                        
                        $informations = json_decode($section_info, true);
                        
                        $section_info_right = $meta->get($sec_key_right);                        
                        $informations_right = json_decode($section_info_right, true);
                        
                        //Explain($informations_right);
                        
                        //exit();
                        
                        //$db = json_encode($a);


                        //$hline = $GLOBALS['hline'];
                        $meta = load()->sys("MetaInfo")->run("site_info", "info_key", "info_value");
                        $hline =  $meta->get(get_config('meta_attr/headlines_class_key'));
                        $spcial_news_category =  $meta->get(get_config('meta_attr/spcial_news_class_key'));

                     
                        
                        
                        //Slide Attributes
                        $slide_limit = 5;

                        //Top Right Section attributes
                        $top_right_limit = 4;

                        //Second Column Attributes
                        $second_column_news_limit = 8;
                        $second_column_news_start = $slide_limit + $top_right_limit;

                        //recent news section attributes
                        $recent_news_start = $second_column_news_start+$second_column_news_limit;  

                        $recent_news_limit = 10; 

                        
                        $headlines = get_news_by_cat("news_headline_list",  $hline, $slide_limit);
                        
                        
                        $top_right = get_news_by_cat("news_top_right",  $hline, array('start'=> $slide_limit, 'limit'=> $top_right_limit) );
                        
                        $second_column_list_left = get_news_by_cat("second_column_list",  $hline,
                                array('start'=> $second_column_news_start, 'limit'=> (int) ($second_column_news_limit/2)
                            ) 
                        );
                        
                        $second_column_list_right = get_news_by_cat("second_column_list",  $hline,
                            array(
                                'start'=> (int) ($second_column_news_start + ( $second_column_news_limit / 2 )), 
                                'limit'=> (int) ( $second_column_news_limit / 2 )
                            ) 
                        );

                        $recent_news = get_news_by_cat("recent_news_list",  $hline, array( 'limit'=> $recent_news_limit, 'start'=> $recent_news_start));
                        
                        $columns = "*, (SELECT count(*) FROM comment where co.content_id = comment.content_id and approved = 1) as total_comments";
                        
                        $most_read_news = get_news_by_cat("recent_news_list",  "", 10, $columns);
                        $spcial_news = get_news_by_cat("spcial_news_list",  $spcial_news_category, 4, $columns);
                        $spcial_news_total = $spcial_news->get_total();
                        
                        //$recent_news = get_news_by_cat("recent_news_list",  $hline, 100);

                        $headline_total = $headlines->get_total();
                        $top_right_total = $top_right->get_total();
                        $second_column_left_total = $second_column_list_left->get_total();
                        $second_column_right_total = $second_column_list_right->get_total();
                        $recent_news_total = $recent_news->get_total();
                        $most_read_news_total = $most_read_news->get_total();
                        
                        $order = array("priority"=>"ASC", "added_date"=> "DESC");
                        
                        //Explain($headlines->show_data());
                        
                    ?>
                    <div id="slider-in">
                        <?php if($headline_total): ?>
                        <div class="slider-control">
                            <ul>
                                <?php for($i = 1; $i <= $slide_limit; $i++): ?>
                                <li><a  href="#"><?php echo $i; ?></a></li>
                                <?php endfor; ?>
                            </ul>
                        </div>
                        <a href="javascript:void(0)" class="prev-btn">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a href="javascript:void(0)" class="next-btn">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                        <?php $headlines->show_data($order); ?>
                        <script>
                            $(".slider-element").eq(0).fadeIn();
                        </script>
                        <?php endif;?>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12 " id="top-right">
                    <div id="top-right-in">
                        <?php if($top_right_total): ?>
                        <?php $top_right->show_data($order); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="row sesciton-second" style="">
                <div class="col-lg-4 " id="news-column-1">
                    <div class="news-column-1-in">
                        <?php if($second_column_left_total): ?>
                            <?php $second_column_list_left->show_data($order); ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-lg-4" id="news-column-2">
                    <div class="news-column-2-in">
                        
                        <?php if($second_column_right_total): ?>
                            <?php $second_column_list_right->show_data($order); ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-lg-4" id="recent-most-read">
                    <div class="recent-most-read-in">
                        <div class="recent-tab-section">
                            <ul class="tab-selector">
                                <li ><a href="javascript:void(0)">সর্বশেষ সংবাদ</a> </li>
                                <li ><a href="javascript:void(0)">সর্বাধিক পঠিত</a> </li>
                            </ul>
                        </div>
                        <div class="recent-content-section">
                            <div class="recent-content-section-element" style="display: block">
                                <?php if($recent_news_total): ?>
                                    <?php $recent_news->show_data($order); ?>
                                <?php endif; ?>                             
                            </div>
                            <div class="recent-content-section-element" style="display: none">
                                <?php if($most_read_news_total): ?>
                                    <?php $most_read_news->show_data(array('co.count'=>'DESC', 'total_comments'=> 'DESC', 'added_date'=>'DESC')); ?>
                                <?php endif; ?>                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
            
            <div class="row special-section">
                <div class="col-lg-8" id="scecial-section-left">
                    <div class="link-list">
                        <div class="link-list-tab-selector">
                            <ul class="list-item">
                                <li><a href="javascript:void(0)">কুইক লিংকস</a></li>
                                <li><a href="javascript:void(0)">জরুরি তথ্য</a></li>
                                <li><a href="javascript:void(0)">জরুরি সেবা</a></li>
                                <li><a href="javascript:void(0)">শিক্ষা ও ভর্তি</a></li>
                            </ul>
                        </div>
                        <div class="link-list-contents">
                            
                            <style>
                                
                                .parent {
                                    -moz-column-count: 4;
                                    -moz-column-gap: 20px;
                                    -webkit-column-count: 4;
                                    -webkit-column-gap: 20px;
                                    column-count: 4;
                                    column-gap: 20px;
                                    padding: 0;
                                    margin: 0;
                                }
                                .child-a {
                                    -moz-column-count: 0;
                                    -moz-column-gap: 20px;
                                    -webkit-column-count: 0;
                                    -webkit-column-gap: 0;
                                    column-count: 0;
                                    column-gap: 0;
                                } 
                                /*
                                .parent {
                                    margin: 0;
                                    padding: 0;
                                }
                                .parent li{
                                    width: 25%;
                                    float: left;
                                }
                                
                                .parent li .child-a li {
                                    width: 100%;
                                }
                                */
                            </style>
                            
                            <div class="link-list-element">
                                <?php
                                
                                    $attributes = array(
                                        array('class'=> 'parent'),
                                        array('class'=> 'child-a'),
                                        array('class'=> 'child-b'),
                                        array('class'=> 'child-c'),
                                        array('class'=> 'child-d'),
                                    );
                                    

                                    
                                    function links_item($r, $ac, $l){
                                        $result = $l == 1 ? "<a href='".__alt($r['ref_url'])."'>":"";
                                        $result .= $r['item_title'];
                                        $result .= $l == 1 ? "</a>":"";
                                        return  $result;
                                    }
                                    
                                    $class_name = "quick-link";
                                    $class_id = _get('class', 'name', $class_name, 'class_id');
                                    get_nav_items("links_item", $class_id, 'ul', $attributes);
                                ?>
                                
                                
                            </div>
                            <div class="link-list-element">
                                <?php
                                    $class_name = "emergency-information";
                                    $class_id = _get('class', 'name', $class_name, 'class_id');
                                    get_nav_items("links_item", $class_id, 'ul', $attributes);
                                ?>
                            </div>
                            <div class="link-list-element">
                                <?php
                                    $class_name = "emergency-service";
                                    $class_id = _get('class', 'name', $class_name, 'class_id');
                                    get_nav_items("links_item", $class_id, 'ul', $attributes);
                                ?>
                            </div>
                            <div class="link-list-element">
                                <?php
                                    $class_name = "education-and-admission";
                                    $class_id = _get('class', 'name', $class_name, 'class_id');
                                    get_nav_items("links_item", $class_id, 'ul', $attributes);
                                ?>
                            </div>
                            <script>
                                $(".link-list-contents .parent:first-child li .child-a").css("display", "none");
                                $(".link-list-contents .parent:first-child li").each(function(i){
                                    $(this).on("click", function(){
                                        $(this).find(".child-a").slideToggle();
                                    });
                                })

                            </script>
                        </div>
                    </div>
                    <div class="spcial-news">
                        <div class="spcial-news-head">
                            বিশেষ সংবাদ
                        </div>                        
                        <div class="spcial-news-contents">
         
                            <?php //if($spcial_news_total): ?>
                                <?php $spcial_news->show_data("added_date", "DESC"); ?>
                            <?php //endif; ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="news-paper-list">
                        <div class="section-header paper-list-head">
                            আজেকের জাতীয় দৈনিকের সংবাদ
                        </div>
                        <div class="news-paper-list-main">
                            
                            <?php
                            
                            function news_list_by_paper($row){
                                return "<li><a href='" .  urldecode($row['source_link']) . "' target='_blank'>".$row['content_title']."</li></a>";
                            }

                            
                            $paper = DB::run()->read('news_paper_list')->order_by('priority')->run_sql();
                            if($paper->get_count()){
                                $paper_list = $paper->get_array();
                                foreach ($paper_list as $row):
                                    $logo_path = get_config('paper-logo/path');
                                    $list = get_list_by_paper_id("news_list_by_paper", $row['paper_id'], 3);
                            ?>
                            <div class="news-paper-list-item">
                                <div class="image-container paper-image-item">
                                    <a href="<?php echo $row['paper_url']; ?>" target="_blank" >
                                        <img src="<?php echo base_url() ?>/<?php echo $logo_path."/".$row['paper_logo_img']?>">
                                    </a>
                                </div>
                                <ul class="paper-news">
                                    <?php if($list->get_total()){$list->show_data(array("priority"=>"ASC", "added_date"=> "DESC")); }?>
                                </ul>
                            </div>
                            <?php endforeach; ?>
                            <?php } ?>
                        </div>
                    </div>
                        <?php
                            $title = "";
                            $class_key = $meta->get(get_config('meta_attr/home-sidebar/ministar-gallery'));
                            $class_id = _get('class', 'url_ref', $class_key, 'class_id');
                            
                            print_sidebar_section($class_id, 4, $title);
                        ?>
                </div>
            </div>
            <div class="row main-news-section">
                <div class="col-lg-9">
                    <?php
                    foreach ($informations as $row => $info) { ?>
                        <div class="col-lg-12 news-in-row">
                        <?php
                        foreach ($info as $d_key => $data){
                            $configs = array("title"=> "", "heading_back" => $data['head_bg_color'], "heading_color" => $data['head_color']);
                            print_section($configs, $data["class_id"], 10, $data['column_size']);
                        }
                        ?>
                        </div>
                        <?php }
                        ?>
                    
                    <?php
                        
                    
                        $headlines = get_news_by_cat("news_gallery_list",  $hline, 5);
                    ?>
                    <div id="gallery" class="col-lg-12">
                        <?php if($headline_total): ?>
                        <a href="javascript:void(0)" class="prev-btn">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a href="javascript:void(0)" class="next-btn">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                        <?php $headlines->show_data($order); ?>
                        <script>
                            //$("#gallery .slider-element").eq(0).fadeIn();
                            
                        </script>
                        <?php endif;?>
                    </div>
                </div>
                <div class="col-lg-3 bottom-sidebar">
                    <?php foreach($informations_right as $d=> $row): ?>
                    <div class="bottom-sidebar-element">
                        <?php     
                        $title = "";
                        $configs = array("title"=> "", "heading_back" => $row['h_color'], "heading_color" => $row['cl_color']);
                        switch($row['view']){
                            case "icon":
                                print_sidebar_section($row['class_id'], 2, $configs);
                                break;
                            case "list":
                                print_section($configs, $row['class_id'], $limit = 10, $col_size = 1);
                                break;
                            case "un":
                                print_sidebar_versity_info($row['class_id'], 4, $configs);
                                break;
                            case "col":
                                print_sidebar_versity_info($row['class_id'], 4, $configs);
                                break;
                            default :
                                print_sidebar_section($row['class_id'], 2, $configs);
                        }
                        ?>
                    </div>
                    <?php endforeach; ?>
                </div>
                
            </div>
        </div>
<?php print_footer(); ?>





