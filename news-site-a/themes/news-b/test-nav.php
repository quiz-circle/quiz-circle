<?php  print_header(); ?>
    
<?php
    function navigation_lists_test($e, $ac, $l, $c, $th){
        $t = "";
        $c = "";
        if($e['link_target'] != "") {
            $t = "target='".$e['link_target']."'";
        }

        if($e['li_class'] != "") {
            $c =  "class='".$e['li_class']."'";
        }
        
        $child_icon = '';
        if($th->hasChild($e['item_id'])){
            $child_icon = '<span class="caret"></span>';
        }
        return "<a href='" . __alt($e['ref_url']) . "' ".$c." ".$t." >" . $e['item_title'] . $child_icon . "</a>";
    }

    $n = load()->lib("NavList")->run('header-nav');
    $n->set_is_tree(true);
    $n->set_html("ul", true, "li");
    $n->set_elemets_oper("navigation_lists_test");
    
    //Explain($n->get_data("priority"));
?>
    <style>
        .header-menu ul {
            width: 100%;
            list-style-type: none;
            border: 1px solid #333;
            float: left;
            padding: 0;
            margin: 0;
            position: relative;
            border-left:none;
            border-right:none;
        }
        
        .header-menu ul li {
            display: inline-block;
            float: left;
            border-right: 1px solid #555;
            padding-bottom: 2px;
            height: 33px;
            border-bottom: 1px solid #333;
        }
        
        .header-menu ul li a {
            //border: 1px solid red;
            padding: 5px 8px;
            color:#333;
            float: left;
            text-decoration: none;
            transition: 0.1s;
            font-size: 15px;
        }
        .header-menu ul li a span.caret{
            border-top-color: #00AAE8;
        }
        
        .header-menu ul li a:hover {
            background-color: #555;
            color: #FFF;
            border-top: 2px solid #00AAE8;
        }
        
        
         
        .header-menu ul li ul {
            display: none;
            position: absolute;
            width: 100%;
            top: inherit;
            left: 0;
            transition: 1s display;
            background-color: rgba(200, 255, 200, 0.9);
            border-bottom-color: #e74c4f; 
        }
        .header-menu ul li:hover > ul{
            display: block;
        }
        
        
    </style>

    <div class="container">
        <div class="row">
            <div class="header-menu">
                <?php
                    if($n->get_total()){
                        echo $n->get_data("priority");
                    }
                ?>
            </div>
        </div>
    </div>
    
    <script>
        
        $(".header-menu ul li").each(function(i){
            $(this).on("mouseover", function(){
                var th = $(this);
                var target  = th.find("ul:first");
                var targetTop = th[0].offsetTop;
                target.css("top", (targetTop+33) + "px");
            });
        });
        
    </script>

    

<?php print_footer(); ?>





