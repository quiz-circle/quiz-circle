<!DOCTYPE html>
<html>
    <head>
        <title>News </title>        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?php theme_url()?>/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php theme_url()?>/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<?php theme_url()?>/css/style.css">

        
        <script src="<?php theme_url()?>/javascript/jQuery.js"> </script> 
        <script src="<?php theme_url()?>/javascript/bootstrap.min.js"> </script>
        <script src="<?php theme_url()?>/javascript/common.js"> </script>
    </head>
    <body>
<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

page_include("includes/function.php");

$a = array(
        "r1" => array(
            "d1"    =>  array(
                    "col-size" => "2",
                    "c_id" => 2,
                    "head_back_color" => "124687",
                ),
            "d2"    =>  array(
                    "col-size" => "1",
                    "c_id" => 3,
                    "head_back_color" => "ec123a",
            ),
        ),
        "r2" => array(
            "d1"    =>  array(
                    "col-size" => "1",
                    "c_id" => 13,
                    "head_back_color" => "31ca87",

            ),
            "d2"    =>  array(
                    "col-size" => "1",
                    "c_id" => 14,
                    "head_back_color" => "13726a",
            ),
            "d3"    =>  array(
                    "col-size" => "1",
                    "c_id" => 4,
                    "head_back_color" => "13726a",
            ),
            
        ),
        "r3" => array(
            "d1"    =>  array(
                    "col-size" => "1",
                    "c_id" => 7,
                    "head_back_color" => "31ca87",

            ),
            "d2"    =>  array(
                    "col-size" => "2",
                    "c_id" => 8,
                    "head_back_color" => "13726a",
            ),
        ),
    
    );

$db = json_encode($a);

echo $db;

echo "<br>";
echo strlen($db);
echo "<br>";
echo "<br>";

$informations = json_decode($db, true);

foreach ($informations as $row => $info) {
    echo $row .":&raquo; ";
    
    foreach ($info as $d_key => $data){
        echo " ".$d_key.": ( ";
        echo $data["c_id"].", Column Size: ";
        echo $data['col-size']." ";
        echo ") ";
    }
    echo "<br>";
} 


?>
<style>
    /*
    .test-sec {
        border:1px solid red;
        width: 100%;
        float: left;
    }
    
    div#lead-news {
        width: 100%;
        float: left;
    }
    
    div#lead-news img {
        width: 100%;
    }
    
    h2 {
        width: 100%;
        float: left;
        font-size: 14px;
        margin: 0;
        padding: 0;
    }
    
    #section-header h2 {
        width: 100%;
        float: left;
        text-align: center;
        height: 35px;
        font-size: 20px;
        line-height: 35px;
    }
    
    .section-row {
        width: 960px;
        float: left;
    }
    
    .sec-row-single {
        width: 300px;
        float: left;
        padding: 8px 10px;
    }
    */
</style>

        <div class="container">
            <div class="row main-news-section">
                <div class="col-lg-9">
                
                <?php
                foreach ($informations as $row => $info) {?>
                    <div class="col-lg-12 news-in-row">
                    <?php
                    foreach ($info as $d_key => $data){
                        print_section(array(), $data["c_id"], 10, $data['col-size']);
                    }
                    ?>
                    </div>
                    <?php }  ?>
                </div>
                
                <div class="col-lg-3">
                    <div class="other">
                        <div class="section-header other-section-head">
                            শিক্ষা সংক্রান্ত সংগঠন পরিচিতি
                        </div>
                        <div class="other-body">
                            Others body 
                        </div>
                    </div>
                    <div class="other">
                        <div class="section-header other-section-head">
                            শিক্ষাবিদ পরিচিতি
                        </div>
                        <div class="other-body">
                            শিক্ষাবিদ পরিচিতি
                        </div>
                    </div>
                    <div class="other">
                        <div class="section-header other-section-head">
                            শিক্ষা সাংবাদিক পরিচিতি
                        </div>
                        <div class="other-body">
                            শিক্ষা সাংবাদিক পরিচিতি
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--
        <div class="col-lg-9">
                <?php
                /*
                foreach ($informations as $row => $info) {?>
                    <div class="col-lg-12 news-in-row">
                    <?php
                    foreach ($info as $d_key => $data){
                        print_section(array(), $data["c_id"], 10, $data['col-size']);
                    }
                    ?>
                    </div>
                    <?php
                } */
                ?>
        </div>
        -->
    </body>
</html>