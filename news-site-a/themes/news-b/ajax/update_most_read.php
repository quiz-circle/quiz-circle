<?php
require_once '../../../root/load-settings.php';
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$session = load()->sys('Session')->run();

$news_id = POST_DATA('news_id');
$session_key = md5($news_id);

if(!$session->check($session_key)){
    $session->add($session_key, "read");
    
    $update = DB::run()->edit('content')->increment('count')->where("content_id", $news_id)->run_sql();
    
    if(!$update->error()){
        echo "Updated!";
    }
}else {
    echo "Already Updated!";
}


