<?php

page_include("includes/function.php");
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$headline_class = load()->sys("MetaInfo")->run("site_info", "info_key", "info_value")->get(get_config('meta_attr/headlines_class_key'));


function test_recent($e){ 
    return "<tr>"
            . "<td>".$e['content_id']."</td>"
            . "<td>".$e['content_title']."</td><td>".$e['added_date']." </td><td> ".$e["priority"]
            ."</td>"
            . "<td>".$e['total_comments']."</td>"
            . "<td>".$e['count']."</td>"
            . "</tr>";
}


$col =  "*, (SELECT count(*) FROM comment where content.content_id = comment.content_id) as total_comments";

$recent_news = get_news_by_cat("test_recent",  $headline_class , 100 , $col);
/*
$meta = load()->sys("MetaInfo")->run("site_info", "info_key", "info_value");
$headline_class_key = get_config('meta_attr/headlines_class_key');
$hline =  $meta->get($headline_class_key); 
*/

echo  "<table border='10' style='border-collapse:collapse'> ";
?>
<tr>
    <td>Content ID</td>
    <td>Content Title</td>
    <td>Added Date</td>
    <td>Priority</td>
    <td>Total Comment</td>
    <td>Count</td>
</tr>
<?php

$recent_news->show_data(
    array(
        "count"=>"DESC",
        "total_comments"=>"DESC",
    ));

echo  "</table>";