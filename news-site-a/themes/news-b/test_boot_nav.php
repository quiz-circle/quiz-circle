<?php
print_header();


function nav_lists($e, $ac, $th){
    $t = "";
    $c = "";
    if($e['link_target'] != "") {
        $t = "target='".$e['link_target']."'";
    }
    
    if($e['li_class'] != "") {
        $c =  "class='".$e['li_class']."'";
    }
    
    if($th->hasChild($e['item_id'])){
        $res = '<a class="dropdown-toggle" data-toggle="dropdown" href="' . __alt($e['ref_url']) . '">' . $e['item_title'] . ' <span class="caret"></span></a>';
        return $res;
    }
    
    return "<a href='" . __alt($e['ref_url']) . "' ".$c." ".$t." >" . $e['item_title'] . "</a>";
}

$n = load()->lib("BootstrapNav")->run('header-nav');
$n->set_is_tree(true);
$n->set_elemets_oper("nav_lists");
    
?>

<div class="container">
    <div class="row">
    <div class="col-lg-10 col-md-10 col-sm-10  col-xs-12" style="border:1px solid white;">
        <nav class="navbar" id="navbar-custom" style="margin-bottom: 0;  ">
            <div class="container" style="margin: 0; padding: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                    <?php  
                        $n->init_html('<li class="active"><a href="#" class="glyphicon glyphicon-home" title="Home"></a></li>');
                        $n->show_data("priority"); 
                    ?>
                        <!--
                        <li class="active"><a href="#" class="glyphicon glyphicon-home" title="Home"></a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">খবর <span class="caret"></span></a>
                            <ul class="dropdown-menu" >
                                <li><a href="#">রাজনীতি</a></li>
                                <li><a href="#">অর্থনীতি</a></li>
                                <li><a href="#">বিশ্ব</a></li>
                                <li><a href="#">তথ্য ও প্রযুক্তি</a></li>
                                <li><a href="#">পরিবেশ</a></li>
                                <li><a href="#">সাস্থ</a></li>
                                <li><a href="#">আন্তর্জাতিক</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">খেলাধুলা</a>
                        </li>
                        -->
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    </div>
</div>
<?php print_footer(); ?>