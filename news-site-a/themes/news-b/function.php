<script>
        //alert("Hello...");
</script>
<?php


function lead_news_function($each){
    $res = '<div class="col-lg-12" id="lead-news">';
    $res .= '<div id="cover-image">';
        $images = get_image_links($each['body'],array('src','title','alt'));
        if(count($images)){
            $res .= '<img src="'.$images[0]['src'].'" alt="'.$images[0]['src'].'" />';
        }else {
            $res .= '<img src="'. themePaths() . '/images/news-thumb.png" alt="cover-image" />';
        }    
        $res .= '</div>';
    $res .= '<div id="lead-title">';
        $res .= '<h2> <a href="">';
            $res .= $each['content_title']; 
            $res .= '</a>';                                
        $res .= '</h2>';
    $res .= '</div>';
    $res .= '<div id="lead-body">';
        $res .= strip_tags(make_excerpt($each['body'], 400));
    $res .= '</div>';
    $res .= '</div>';
    return $res;
}

function other_news_function($each){
    
    $res = '<div class="item">';
        $res .= '<div class="news-thumb">';
            $res .= '<div class="news-list-thumb-container">';
            /*
            $images = get_image_links($each['body'],array('src','title','alt'));
            if(count($images)){
                $res .= '<img src="'.$images[0]['src'].'" alt="'.$images[0]['alt'].'" />';
            }else {
                $res .= '<img src="'. themePaths() . '/images/news-thumb.png" alt="cover-image" />';
            }
            */
            $res .= '</div>';
        $res .= '</div>';
        $res .= '<div class="title">';
            $res .= '<h2> <a href="">';
                $res .= $each['content_title'];
            $res .= '</a></h2>';
        $res .= '</div>';
    $res .= '</div>';
    return $res;
    //return $each['content_title']." <strong>Priority:</strong> ".$each['priority']." Time: ". $each['added_date'] . "<br>";
}

function get_news_by_cat( $function, $class_url_ref = null, $limit = 8, $top = false){
    $config = array(
        'class-type'        =>  "category",
        'content-type'      =>  "news",
        'visibility'        =>  1,
        
        
    );
    
    
    if( $class_url_ref ){
        $config['class_url_ref'] =  $class_url_ref;
    }

    $list = load()->lib("NewsList")->run($config);

    $list->set_html("");
    $list->set_elemets_oper($function);
    
    //echo $list->get_sql()."<br>";
    
    if($limit) $list->data_limit($limit);
    return $list;
}




function newslist($caturl_ref, $cover_news_function, $others_news_function, $limit, $divider = ""){
    $news = array(); 
    $news['top'] = "";
    $news['other'] = "";
    
    $cover_news = get_news_by_cat($cover_news_function, $caturl_ref, 1, false, true );
    $news_by_priority = get_news_by_cat($others_news_function, $caturl_ref, $limit, true, false);

    $priority_total = $news_by_priority->get_total();
    
    
    if($cover_news->get_total()) $news['top'] = $cover_news->get_data();
    
    echo $divider;
    
    //if($priority_total) $news_by_priority->show_data(array( "priority" => "ASC", "added_date"=>"ASC" ));
    if($priority_total) $news['other'] .= $news_by_priority->get_data(array( "priority" => "ASC", "added_date"=>"ASC" ));
    if($priority_total < $limit){
        $date_limit =  $limit - $priority_total;
        $news_by_added_date = get_news_by_cat($others_news_function, $caturl_ref, $date_limit, false, false);
        if($news_by_added_date->get_total()){
            $news['other'] .= $news_by_added_date->get_data(array('added_date'=>"desc", "priority"),'desc');
        }
    }
    return $news;
    //echo $news['other'];
}

function print_section($container_config, $class_id, $limit = 10){

    $class = isset($container_config['class']) ? $container_config['class']:""; 
    $id = isset($container_config['id']) ? $container_config['id']:""; 
    $style = isset($container_config['style']) ? $container_config['style']:""; 
?>
    <div <?php  echo $class ? " class='".$class."' ":" "; 
            echo $id ? " id='".$id."' ":" ";
            echo $style ? " style='".$style."' ":" "; ?> >
        <?php
            $title = "(untitled)";
            $url_ref = "";
            $ne['top'] = "";
            $ne['other'] = "";
            if($cat = get_cat_row($class_id)){
                $title = $cat['name'];
                $url_ref = $cat['url_ref'];
                $ne = newslist($url_ref, "lead_news_function", "other_news_function", $limit);
            }
        ?>

        <div id="section-header" class="col-lg-12">
            <h2><?php echo $title; ?> </h2>
        </div>                    
        <div id="test-section-body" class="col-lg-12">
            <?php echo $ne['top']; ?>
            <div class="col-lg-12" id="list-container">
                <div class="news-list with-thumb">
                    <?php echo $ne['other']; ?>
                </div>
            </div>
        </div>                    
    </div>
<?php } 