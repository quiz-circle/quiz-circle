<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function link_list_col1($r, $ac, $l, $i){
    if(($l == 1 && $i <= 5)){
        return $r['item_title']."---"."Level: ".$l.", Index: ".$i."<br>";
    }
}

function link_list_col2($r, $ac, $l, $i){
    if(($l == 1 && $i >= 4)){
        return $r['item_title']."---"."Level: ".$l.", Index: ".$i."<br>";
    }
}



function nav_list_split($function, $class_id, $column = ""){
    load()->lib("NavList")->open();
    $n = new NavList($class_id);
    
    $n->set_html("ol", true);
    $n->set_is_tree(true);
    $n->setAttrs(array(
        array('class'=> 'parent'),
        array('class'=> 'child-a'),
        array('class'=> 'child-b'),
        array('class'=> 'child-c'),
        array('class'=> 'child-d'),
    ));

    $nav = $n->set_elemets_oper($function);

    $data = $nav->get_data();
    return $data;

}


Explain(nav_list_split("link_list_col1", 43, 5));
Explain(nav_list_split("link_list_col2", 43, 5));