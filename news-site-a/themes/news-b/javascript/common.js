/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



(function(){
    function manageTabs(tabLocation, secLocation, find, event){
        event = (typeof event == "undefined") ? "click":event;
        
        var hashNamePrefix = "tab-";
        var Tabs = $(tabLocation);
        var sections = $(secLocation);
        var f = (find === undefined) ? "div":find;

        sections.find(f).css("display", "none");
        sections.find(f).eq(0).css("display", "block");

        Tabs.eq(0).addClass("active");
        Tabs.each(function(i, e){
            $(this).on(event, function(){
                var afterHash = __afterHash();
                var name = $(this).attr("id");
                if(name !== undefined){
                    window.location.href = "#"+hashNamePrefix+name;
                }

                Tabs.removeClass("active");
                Tabs.eq(i).addClass("active");
                sections.find(f).css("display", "none");
                sections.find(f).eq(i).css("display", "block"); 
            });
        });

        $(this).on('load', function(){
            var loc = __afterHash();
            loc = loc.replace(hashNamePrefix, "");

            if(sections.find("#"+loc).length !== 0){
                Tabs.removeClass("active");
                $(tabLocation+"#"+loc).addClass("active");
                sections.find(f).css("display", "none");
                sections.find("#"+loc).css("display","block");

            }     
        });

    }

    function __afterHash(){
        var split = window.location.href.split("#");
        if(split.length < 2){
            return "";
        }else {
            return split[1];
        }
    }
    
    
    
    
    
    
    $(document).ready(function(){
        
        /*****************************************/
        /*--------Start Coding For Slider--------*/
        /*****************************************/
        
        
        a = $("#slider-in .slider-element");
        
        var number_of_element = parseInt(a.length); //defining the number of the element
        
        console.log(number_of_element);
        
        var timeout = 5000; //miliseconds
        var global_index = -1;
        var timer = "";
        var slider_control = $(".slider-control li");
        $(".slider-element").eq(0).fadeIn();

        function slider_change(key){
            slider_control.removeClass("active");
            var slider_element = $(".slider-element");
            slider_element.css("display", "none");    
            slider_element.eq(key).fadeIn();
            slider_control.eq(key).addClass("active");
            global_index = key;
        }

        $(".prev-btn").on("click",function(){
            if(global_index == 0 ){
                global_index = number_of_element;
            }
            var index = global_index-1;
             slider_change(index);
            reset_timer();
        });

        $(".next-btn").on("click",function(){
            if(global_index == (number_of_element-1) ){
                global_index = -1;
            }
            var index = global_index+1;
            slider_change(global_index+1);
            reset_timer();
        });

        function reset_timer(){                                    
            clearTimeout(timer);
            timer = setTimeout(function(){
                auto_slider_change();
            },timeout);
        }

        $(".slider-control li").each(function(key){
            $(this).on("click", function(){
                slider_change(key);
                reset_timer();
                return false;
            });
        });

        function auto_slider_change(){
            if(global_index == (number_of_element-1) ){
                global_index = -1;
            }
            var index = global_index+1;
            slider_change(index);
            timer = setTimeout(auto_slider_change, timeout);   
        }

        auto_slider_change();
        
        /*--------End of Coding For Slider--------*/

        
        /**************************************************************************/
        /*--------Manageing Tab section For Recent News And Most Read News--------*/
        /*--------------------Manageing Tab section link lists--------------------*/
        /***************************************************************************/
        
        manageTabs(".tab-selector li", ".recent-content-section", ".recent-content-section-element", "mouseover");
        manageTabs(".link-list-tab-selector ul li", ".link-list-contents", ".link-list-element");

        /*--------End of Coding For Slider--------*/
    });
})();

//var slider = slider || {};
/*
var __s = function(selector){    
    //"use strict";
    return {
        slider_element : $(selector),
        change: null, 
        init_control:null,
        reset_timer:null, 
        start:null,

        //Properties
        options:null,
        timer:null,
        global_index:-1,
        number_of_element:null,
        timeout:5000, //miliseconds
        slider_control:null,


        //$(".slider-element").eq(0).fadeIn();


        setting : function(opt){
            this.number_of_element = $(this.sliderElement).length;
            if(this.slider_element !== undefined){
                this.number_of_element = this.slider_element.length;

                if(opt.control !== undefined){
                    this.init_control(opt.control);
                    this.start();
                    //console.log(this.slider_control);
                }
            }
            return this;
        },

        change : function(key){
            this.slider_element.css("display", "none");
            this.slider_control.removeClass("active");
            this.slider_control.eq(key).addClass("active");
            this.slider_element.eq(key).fadeIn();
            this.global_index = key;
        },

        init_control : function(setting){
            if(setting.length !== 0){
                var nextBTN = (setting.nextBtn !== undefined ) ? setting.nextBtn:"";
                var prevBTN = (setting.prevBtn !== undefined ) ? setting.prevBtn:"";
                var listCTRL = (setting.listCtrl !== undefined ) ? setting.listCtrl:"";
                this.slider_control = $(listCTRL);

                $(prevBTN).on("click",function(){
                    if(this.global_index == 0 ){
                        this.global_index = this.number_of_element;
                    }
                    var index = this.global_index-1;
                    this.change(index);
                    this.reset_timer();
                });

                $(nextBTN).on("click",function(){
                    if(this.global_index == (number_of_element-1) ){
                        this.global_index = -1;
                    }
                    var index = this.global_index+1;
                    this.change(this.global_index+1);
                    this.reset_timer();
                });

                this.slider_control.each(function(key){
                    $(this).on("click", function(e){
                        e.preventDefault();
                        console.log(key);
                        this.change(key);
                        this.reset_timer();
                    });
                });
            }
        }
        ,
        reset_timer : function(){                    
            clearTimeout(this.timer);
            this.timer = setTimeout(function(){
                this.start();
            }, this.timeout);
        },

        start :  function(){
            if(this.global_index == (this.number_of_element - 1) ){
                this.global_index = -1;
            }
            console.log(this);
            var index = this.global_index+1;
            //this.change(index);
            this.timer = setTimeout(function(){
                self  =  slider(selector)
                self = this;
                self.start;
            }, 2000);   
        } 
    } 
}



function slider(selector){
    return new __s(selector);
}
*/