<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function get_list_by_paper_id( $function, $paper_id, $limit){
    
    $config = array(
        'class-type'        =>  "category",
        'content-type'      =>  "news",
        'paper_id'        =>  $paper_id,
    );
    
    $list = load()->lib("NewsList")->run($config);

    $list->set_html("");
    $list->set_elemets_oper($function);
    
    //echo $list->get_sql()."<br>";
    
    if($limit) {
        if(is_array($limit)){
            if(isset($limit['limit']) && isset($limit['start']) ){
                $list->data_limit($limit['limit'], $limit['start']);
            }
        }else {
            $list->data_limit($limit);
        }
    }
    
    //echo "<br><strong>SQL:</strong> ".$list->get_sql();
    return $list;
}


function news_list_by_paper($row){
    return "<div style='width:100%'>".$row['content_title']."</div>";
}

$list = get_list_by_paper_id("news_list_by_paper", 35, 3);

if($list->get_total()){
    $list->show_data();
}

$paper = DB::run()->read('news_paper_list')->order_by('priority')->run_sql();

if($paper->get_count()){
    $paper_list = $paper->get_array();
}
