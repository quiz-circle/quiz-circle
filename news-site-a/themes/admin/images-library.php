<?php admin_page_include('includes/header.php');?>

<?php
    $link = GET_DATA('operation_link');
    if( !$link ){
        redirect(admin_link());
    }
    
    $link = urldecode($link);
?>
<style>
    body {
        overflow: hidden;
    }
    
    #opr-frame {
        width: 100%;
        height:500px; 
    }
    
</style>
<h1 class="main-body-page-header">
    Images Library
</h1>
<iframe src="<?php echo $link ?>" frameborder='0' id="opr-frame">


<?php admin_page_include('includes/footer.php');?>