<?php 
header('Content-Type: text/html; charset=utf-8');
mb_internal_encoding('utf-8');
admin_page_include('includes/header.php');

$errs = array();

echo $seg_vars = urldecode($_SEG_VARS[1]);

$year = date("Y");
$month = date("m");
$uplaod_path =  "books/".$year.'/'.$month;

$b = DB::run()->read('book_list')->where('url_ref', $seg_vars)->run_sql();
$book = $b->get_first();
//Explain();
$book_id = $book['book_id'];

$c = DB::run()->read("class_relationship")->where('entity_id', $book_id)->run_sql();
$a = DB::run()->read("book_auth_rel")->where('book_id', $book_id)->run_sql();


Explain($c->get_array());
Explain($a->get_array());

$old_category = array();
foreach($c->get_array() as $val){
    $old_category['rel_id'] = $val['class_id']; 
}

$old_auths = array();
foreach($a->get_array() as $val){
    $old_auths['rel_id'] = $val['auth_id']; 
}

Explain($old_category);
Explain($old_auths);


if(isset($_POST) && !empty($_POST)){

    require_once "themes/admin/functions.php";
    
    $today = date('Y-m-d');
    
    
    $book_file = load()->sys('inputFile')->run('book-file');

    if($book_file->isExists()){
        if(trim($book_file->getExtention()) == 'pdf'){
            if(($book_file->getSize()/ (1024*1024)) < 512){
                $n = $book_file->getName();
                $t = $book_file->getTempName();
            }
            $time = time();
            $filename = $time ."_".substr(md5($n), 0, 15).".pdf";
            echo "Ok";
            

            $uploaded = $book_file->upload($filename, $uplaod_path, $t);
        }
        
        if($uploaded){
            
        /*
        $book_thumb = loadSYS('inputFile','book-thumb');
        echo $book_thumb->getType()."<br>";
        echo $book_thumb->getExtention()."<br>";
        echo $book_thumb->getName()."<br>";
        echo $book_thumb->getSize()/ (1024*1024)." MB<br>";
        echo $book_thumb->getTempName()."<br>";
        echo $book_thumb->Error()."<br>";
        */


        //data for book list table
        $title = strip_tags(trim($_POST['book-name']));
        $url_ref = make_unique_ref($title, 'book_list');
        $description = strip_tags(trim($_POST['description']));
        $publisher = strip_tags(trim($_POST['publisher']));
        $isbn = strip_tags(trim($_POST['isbn']));

        $book_ins = DB::run()->write('book_list')->values(
                    array(
                        'book-title'    =>  $title,
                        'url_ref'       =>  $url_ref,
                        'description'   =>  $description,
                        'publisher'     =>  $publisher,
                        'isbn'          =>  $isbn,
                        'filename'      =>  $filename,
                        'upload_date'   =>  $today,
                    )
                )->run_sql();

        if(!$book_ins->error()){
            //data for category relations
            $cats = $_POST['category'];
            $book_id = $book_ins->last_insert_id();


            //data for authors relation
            $auths = $_POST['author'];


            foreach ($auths as $a){
                $barel = DB::run()->write('book_auth_rel')->values(
                    array(
                        'book_id' => $book_id,
                        'auth_id' => $a
                    )
                )->run_sql();
            }
            foreach ($cats as $c){
                $bcrel = DB::run()->write('class_relationship')->values(
                    array(
                        'entity_id' => $book_id,
                        'class_id' => $c
                    )
                )->run_sql();
            }
        }  else {
            echo "Book insertion failed: ".$book_ins->sql_error();
        }
    }
}else {
        $errs['001'] = "Book not uploaded!";
    }
}

Explain($errs);

$sql = "select * from class ";
$sql .= "natural join class_type ";
$sql .= "where type='category'";
$cats = DB::run()->setSql($sql)->run_sql();

$sql = "select * from author ";
$auth = DB::run()->setSql($sql)->run_sql();



?>
<style>

    #cat-form, #auth-form {
        display: none;
        border: 3px solid gray;
        box-shadow: 0px 0px 10px 6px #CCC;
        padding: 15px;
        margin-top: 14px;
        font-size: 14px;
    }

    #cat-form h1 ,
    #auth-form h1 {
        font-size: 20px;
        margin: 0;
        padding: 0;
        margin-bottom: 15px;
    }

    #cat-form input, #cat-form select,
    #auth-form input, #auth-form select {
        width: 80%;
        height: 20px;
        padding: 3px;
        margin-top: 5px;
    }
    #cat-form select,
    #auth-form select {
        height: 30px;
        width: 85%;
    }
    #cat-form input[type='submit'] ,
    #auth-form input[type='submit'] {
        width: 50%;
        height: 40px;
        cursor: pointer;
    }


</style>

<div style="width: 250px; margin: 0 auto">
    <form action="" method="post" enctype="multipart/form-data"  accept-charset="utf-8">
        <h1>Add a Book</h1>
        Upload a pdf book:<br>
        <input type="file" name="book-thumb" accept="image/jpeg, image/x-png, image/gif" required style="width:250px;"><br><br>
        Name:<br>
        <input type="text" name="book-name" required style="width:250px;"><br><br>
        Publisher:<br>
        <input type="text" name="publisher" style="width:250px;"><br><br>
        ISBN:<br>
        <input type="text" name="isbn" style="width:250px;"><br><br>
        Description:<br>
        <textarea id="description" name="description"  style="width:250px; height: 130px"></textarea><br>

        Category:<br>
        <select name="category[ ]" required multiple id="from-cat" style="width:250px; height:<?php echo $cats->get_count() * 18?>px">  
            <?php echo get_class_options("category","",array('a'=>222,'c'=>255,'d'=>224,'e'=>177) ); ?>
        </select><br>
        <a href="<?php echo getAdminPath()."/add-class?type=category"?>" onclick="return open_form('#cat-form')" target="_blank">Add a category</a>
        <div id="cat-form">
            <h1>Add a Category</h1>
            <div id="message" style="display: none"></div>
            Type a Category name:<br>
            <input type="text" name="name" id="name"><br><br>
            Parent:<br>                
            <select name="parent" required id="parent">
                <?php echo get_class_options("category", "<option value=\"0\">(no parent)</option>"); ?>
            </select><br><br>
            <input type="submit" id="cat-add" value="Add Category" >
        </div>
        <br>
        <br>
        Author:<br>
        <select name="author[ ]" required multiple id="author" style="width:250px; height:<?php echo $auth->get_count() * 22?>px"> 
            <?php echo get_author_options() ?>
        </select><br>

        <a href="<?php echo getAdminPath()."/add-author"?>" onclick="return open_form('#auth-form')" target="_blank">Add a author</a>
        <div id="auth-form">
            <h1>Add an Author</h1>
            <div id="message" style="display: none"></div>
            Type a Category name*:<br>
            <input type="text" name="auth-name" id="auth-name"><br><br>
            Type a Nick Name:<br>
            <input type="text" name="nick-name" id="nick-name"><br><br>
            <input type="submit" id="auth-add" value="Add Author" >
        </div>
        <br>
        <br>
        <input type="submit" value="Add" >

        <script>

            var type = "category";
            var is_parent = "true";; 
            var aj_path = "<?php echo admin_url(). "/ajax/"?>";

            var cname = $("#name");
            var parent = $("#parent");
            var form = $a("#cat-add", "json");

            form.setEvent("click");
            form.setUrl(aj_path +"add_class.php");
            form.setValues(function(){

                this.values = {
                    name:cname.val(),
                    parent_id:parent.val(),
                    type:type,
                    is_parent:is_parent
                }

                return false;
            });
            form.validation(function(){
                if(this.values.name.length == 0){
                    alert("You did'nt type anything!");
                    return false;
                }
                return true;
            });
            form.success(function(r){
                if(type != 'tag'){

                    if(r.status){
                        var p_id = r.parent_id;
                        var parent = $a("#parent", 'load');
                        parent.setValues(function(){
                            this.values = {
                                active_id : p_id,
                                init_html : "<option value=\"0\">(no parent)</option>",
                            }
                        });
                        parent.setUrl(aj_path +"get_class.php");
                        parent.run();  

                        change_form_cat(r.last_id);
                    }
                }
            });
            form.run();
            var aname = $("#auth-name");
            var nick_name = $("#nick-name");

            var auth_form = $a("#auth-add", "json");
            auth_form.setEvent("click");
            auth_form.setUrl(aj_path +"add_author.php");
            auth_form.setValues(function(){

                this.values = {
                    name:aname.val(),
                    nick_name:nick_name.val()
                }
                return false;
            });

            auth_form.validation(function(){
                if(this.values.name.length == 0){
                    alert("You did'nt type anything!");
                    return false;
                }
                return true;
            });

            auth_form.success(function(r){
                console.log(r);

                if(r.status){
                    //var p_id = r.parent_id;
                    var list = $a("#author", 'load');
                    list.setUrl(aj_path +"get_author.php");
                    list.success(function(){
                        set_active_author(r.last_id);
                    });
                    list.run();

                }
            });
            auth_form.run();



        </script>
        <script src="<?php echo admin_url()."/js/fsinheader.js"?>" type="text/javascript"></script>

    </form>
</div>

<?php admin_page_include('includes/footer.php')?>