<?php 
admin_page_include('includes/header.php');

admin_page_include('includes/functions.php');

//require_once "themes/admin/functions.php";

if(isset($_GET['book_id']) || !empty($_GET['book_id'])){
    $id = $_GET['book_id'];
    $image_link = get_thumb_from_DB($id);
} else {
    redirect(admin_link());
}

$errs = array();

$book = DB::run()->read("book_list")->where('book_id', $id)->run_sql();

if(!$book->get_count()){
    redirect(admin_link()."/book-list");
}

$book_list = $book->get_first();

$filename = $book_list['filename'];
$up_date = $book_list['upload_date'];
$ext = $book_list['thumb_ext'];


$image_link = get_image_thumb($filename, $up_date, $ext );
$book_filename = explode(".", $book_list['filename']);
$book_filename = $book_filename[0];
$thumb_path = "books/".get_date_formate($up_date, "Y")."/".get_date_formate($up_date, "m")."/thumb";

$title = $book_list['book-title'];
$description = $book_list['description'];
$publisher = $book_list['publisher'];
$isbn = $book_list['ISBN'];
$extention = null;
$uploaded = false;
if(isset($_POST) && !empty($_POST)){
    $today = date('Y-m-d');
    $book_thumb = load()->sys('inputFile')->run('book-thumb');
    $allowed_thumbs_ext = array('png','jpg','jpeg','gif','PNG','JPG','JPEG','GIF');
    if($book_thumb->isExists()){
        if(!$book_thumb->isEmpty()){
            echo "Image Is Exists!";
            if(in_array($book_thumb->getExtention(), $allowed_thumbs_ext)){
                if(($book_thumb->getSize()/ (1024*1024))    < 4){
                    $n = $book_thumb->getName();
                    $t = $book_thumb->getTempName();
                    $book_thumb_name = $book_filename."_thumb.".$book_thumb->getExtention();   
                    $uploaded = $book_thumb->upload($book_thumb_name, $thumb_path, $t);
                    $extention = $book_thumb->getExtention();
                    $thumb_name = $book_thumb_name;
                    $image_link = get_image_thumb($filename, $up_date, $extention);
                }else {
                    $errs[] = "Book Thumb not Uploaded Invalid book thumb filesize is greater than 3MB!";
                }
            }
        }
    //data for book list table
    }
    
    $title = strip_tags(trim($_POST['book-name']));
    $description = strip_tags(trim($_POST['description']));
    $publisher = strip_tags(trim($_POST['publisher']));
    $isbn = strip_tags(trim($_POST['isbn']));
    
    if($extention){
        $update_cols = array(
            'book-title'    =>  $title,
            'description'   =>  $description,
            'publisher'     =>  $publisher,
            'isbn'          =>  $isbn,
            'thumb_ext'     =>  $extention,
            'thumb'         =>  $thumb_name
        );
    }else {
        $update_cols = array(
            'book-title'    =>  $title,
            'description'   =>  $description,
            'publisher'     =>  $publisher,
            'isbn'          =>  $isbn,
        );
    }
    
    $book_update = DB::run()->edit('book_list')->values($update_cols)->where('book_id', $id)->run_sql();
    
    if($book_update->error()){
        $errs[] = "Updating Error!";
    }else {
        if($book_update->has_changed()){
            echo "Successfully Updated!";
        }else {
            echo "Already Updated!";
        }
    }
}

show_errors($errs);

$sql = "select * from class ";
$sql .= "natural join class_type ";
$sql .= "where type='category'";
$cats = DB::run()->setSql($sql)->run_sql();
$athors = DB::run()->read("author")->run_sql();
$total_authors = $athors->get_count();

function get_class_ids($id){
    $sql = "select * from class_relationship ";
    $sql .= "where `entity_id` =" . $id;
    $cats_rel = DB::run()->setSql($sql)->run_sql();
    $res = array();
    if($cats_rel->get_count()){
        foreach($cats_rel->get_array() as $id){
            $res[] = $id['class_id'];
        }
    }
    
    return $res;
}
function get_auth_ids($id){
    $sql = "select * from book_auth_rel ";
    $sql .= "where `book_id` =" . $id;
    $auth_rel = DB::run()->setSql($sql)->run_sql();
    $res = array();
    
    if($auth_rel->get_count()){
        foreach($auth_rel->get_array() as $id){
            $res[] = $id['auth_id'];
        }
    }
    return $res;
}

$class_ids = get_class_ids($book_list['book_id']);
$auth_ids = get_auth_ids($book_list['book_id']);

?>



<style>

    #cat-form, #auth-form {
        display: none;
        border: 3px solid gray;
        box-shadow: 0px 0px 10px 6px #CCC;
        padding: 15px;
        margin-top: 14px;
        font-size: 14px;
    }

    #cat-form h1 ,
    #auth-form h1 {
        font-size: 20px;
        margin: 0;
        padding: 0;
        margin-bottom: 15px;
    }

    #cat-form input, #cat-form select,
    #auth-form input, #auth-form select {
        width: 80%;
        height: 20px;
        padding: 3px;
        margin-top: 5px;
    }
    #cat-form select,
    #auth-form select {
        height: 30px;
        width: 85%;
    }
    #cat-form input[type='submit'] ,
    #auth-form input[type='submit'] {
        width: 50%;
        height: 40px;
        cursor: pointer;
    }

    .book_thumb_image {
        width: 180px;
    }
    
    .list {
        width: 400px;
        overflow-y:scroll;
        height: 500px;
        float: left;
    }
        .class_list {
        width: 400px;
        border:1px solid black;
        list-style-type: none;
        float: left;
        padding: 0;
        margin: 0;
        color: #444;
    }
    
    .class_list li{
        float: left;
        width: 100%;
        border-bottom:1px solid green;
        position: relative;
        margin-bottom: 1px;
        
    }
    
    .class_list li .chkBox{
        float: left;
        width: 4%; 
        padding: 4px 0;
    }
    
    .class_list li .label{
        float: left;
        width: 94%;
        padding-left: 5px;
        
    } 
    
    .class_list li .main{
        float: left;
        width: 25%;
        position: absolute;
        right: 0;
        top: 0;
        padding: 4px 0; 
        //padding-right: 5px;
        color: darkviolet;
    } 
    .class_list li .main label {
        width: 100%;
    }
    
    .class_list li .main:hover {
        background-color: rgba(200, 200, 200, 0.5)
    }
    
    .class_list li .label label {
        width: 100%;
        float: left;
        cursor: pointer;
        padding: 4px 0; 
        
    }    
    
    .active, .active:hover {
        background-color: #CCC;
        color: #000;
    }
</style>

<div style="width: 400px; margin: 0 auto">
    <form action="" method="post" enctype="multipart/form-data"  accept-charset="utf-8">
        <h1>Add a Book</h1>
        Edit Book Thumbnail:<br>
        <img class="book_thumb_image" src="<?php echo $image_link?>" alt="book_thumb" />        
        <input type="file" name="book-thumb" accept="image/jpeg, image/x-png, image/gif" style="width:400px;" /><br><br>
        Type a name:<br>
        <input type="text" name="book-name" value="<?php echo $title ?>" required style="width:400px;" /><br><br>
        Publisher:<br>
        <input type="text" name="publisher"  value="<?php echo $publisher ?>" style="width:400px;" /><br><br>
        ISBN:<br>
        <input type="text" name="isbn"   value="<?php echo $isbn ?>" style="width:400px;"><br><br>
        Description:<br>
        <textarea id="description" name="description" style="width:400px; height: 130px"><?php echo $description ?></textarea><br>
        Category:<br>
        
        <!--<select name="category[ ]" required multiple id="from-cat" style="width:250px; height:<?php //echo $cats->get_count() * 18?>px">  
            <?php //echo get_class_options("category"); ?>
        </select><br>
        -->
        <div class="list">
            <ul class="class_list">
                <?php echo class_list($class_ids); ?>
            </ul>
        </div>
        <br><br>
        <!--
        <a href="<?php //echo getAdminPath()."/add-class?type=category"?>" onclick="return open_form('#cat-form')" target="_blank">Add a category</a>
        <div id="cat-form">
            <h1>Add a Category</h1>
            <div id="message" style="display: none"></div>
            Type a Category name:<br>
            <input type="text" name="name" id="name"><br><br>
            Parent:<br>                
            <select name="parent" required id="parent">
                <?php echo get_class_options("category", "<option value=\"0\">(no parent)</option>"); ?>
            </select><br><br>
            <input type="submit" id="cat-add" value="Add Category" >
        </div>
        -->
        Author:
        
        
        <div class="widget">    
            <h3 class="widget-title" >Author:</h3>
            <div class="widget-body" >
                <div class="list" style="float: left">
                    <ul class="class_list"  style="float: left">
                    <?php echo author_list($auth_ids); ?>
                    </ul>
                </div> 
            </div>
        </div>
        
        <style>
            
            .widget {
                float: left;
                width: 100%;
                border: 1px solid #AAA;
                padding: 0;
                margin: 0;
                position: relative;
            }
            
            .widget .widget-title{
                float: left;
                width: 100%;
                height: 35px;
                background-color: #ddd;
                line-height: 35px;
                font-size: 16px;
                font-weight: 600;
                color: #333;
                border-bottom: 1px solid #AAA;
                padding: 0 2.5%;
                margin: 0;
                position: relative;
            }
            .widget .widget-body {
                float: left;
                width: 100%;
                background-color: #ddd;
                color: #333;
                padding: 2.5%;
                background-color: #FFF;
                position: relative;
            }

        </style>
        
        
        <!--<div id="author" style="width:400px; height:<?php echo $total_authors * 15?>px; overflow-y:scroll">
            <?php //echo get_author_options() ?>
        </div><br>
        -->
        
        
        
        <!--
        <a href="<?php //echo getAdminPath()."/add-author"?>" onclick="return open_form('#auth-form')" target="_blank">Add a author</a>
        <div id="auth-form">
            <h1>Add an Author</h1>
            <div id="message" style="display: none"></div>
            Type a Category name*:<br>
            <input type="text" name="auth-name" id="auth-name"><br><br>
            Type a Nick Name:<br>
            <input type="text" name="nick-name" id="nick-name"><br><br>
            <input type="submit" id="auth-add" value="Add Author" >
        </div>
        -->
        <br>
        <br>
        
        <input type="submit" value="Add" >


        <script src="<?php echo admin_url()."/js/fsinheader.js"?>" type="text/javascript"></script>

    </form>
    
    <script>

    var type = "category";
    var is_parent = "true";; 
    var aj_path = "<?php echo admin_url(). "/ajax/"?>";

    var cname = $("#name");
    var parent = $("#parent");
    var form = $a("#cat-add", "json");

    form.setEvent("click");
    form.setUrl(aj_path +"add_class.php");
    form.setValues(function(){

        this.values = {
            name:cname.val(),
            parent_id:parent.val(),
            type:type,
            is_parent:is_parent
        }

        return false;
    });
    form.validation(function(){
        if(this.values.name.length == 0){
            alert("You did'nt type anything!");
            return false;
        }
        return true;
    });
    form.success(function(r){
        if(type != 'tag'){

            if(r.status){
                var p_id = r.parent_id;
                var parent = $a("#parent", 'load');
                parent.setValues(function(){
                    this.values = {
                        active_id : p_id,
                        init_html : "<option value=\"0\">(no parent)</option>",
                    }
                });
                parent.setUrl(aj_path +"get_class.php");
                parent.run();  

                change_form_cat(r.last_id);
            }
        }
    });
    form.run();
    var aname = $("#auth-name");
    var nick_name = $("#nick-name");

    var auth_form = $a("#auth-add", "json");
    auth_form.setEvent("click");
    auth_form.setUrl(aj_path +"add_author.php");
    auth_form.setValues(function(){

        this.values = {
            name:aname.val(),
            nick_name:nick_name.val()
        }
        return false;
    });

    auth_form.validation(function(){
        if(this.values.name.length == 0){
            alert("You did'nt type anything!");
            return false;
        }
        return true;
    });

    auth_form.success(function(r){
        console.log(r);

        if(r.status){
            //var p_id = r.parent_id;
            var list = $a("#author", 'load');
            list.setUrl(aj_path +"get_author.php");
            list.success(function(){
                set_active_author(r.last_id);
            });
            list.run();
        }
    });
    auth_form.run();



    </script>
    
    <?php if($uploaded): ?>
        
        <script>
            var image_link = '<?php echo $image_link; ?>';
            /*
            setTimeout(function(){
                $(".book_thumb_image").prop("src", image_link);
            },2000);
            */
        </script>
    <?php endif;?>
</div>

<style>
    

    
</style>


<script type="text/javascript">
    $(".class_list li").each(function(key, a){
        var check = $(this).find(".chkBox input:checkbox");
        var exists_class = "active";
        var class_add_to = $(".class_list li").eq(key);
        check.on("change", function(){
            if(class_add_to.hasClass(exists_class)){
                class_add_to.removeClass(exists_class);
            }else {
                class_add_to.addClass(exists_class);
                console.log("class not exists");
            }
            //$(".class_list li").eq(key).css("background-color","#555");
        });
        
    });
</script>
<?php admin_page_include('includes/footer.php')?>
