<?php

$delete_table_reference = array(
    'paper-name'    =>  array(
        'table' => 'news_paper_list',
        'column' => 'paper_id',
        'url_param' =>  2
    )
);



$deleting_options = $delete_table_reference[SEG_VARS(1)];

$delete_from = $deleting_options['table'];
$delete_by = $deleting_options['column'];


$url_param = (isset($deleting_options['url_param']) && !empty($deleting_options['url_param'])) ? $deleting_options['url_param']:null;
if($url_param){
    $delete = load()->sys("Content")->run($delete_from, $delete_by, $url_param);
}else {
    $delete = load()->sys("Content")->run($delete_from, $delete_by);
}

admin_page_include("includes/delete/".SEG_VARS(1).".php");


