<?php 
header('Content-Type: text/html; charset=utf-8');
mb_internal_encoding('utf-8');
admin_page_include('includes/header.php');
?>
<span style="text-align: center; color: #A8116C; font-weight: bold; font-size: 25px; width: 100%; float: left">Invalid Request</span>
<span style="text-align: center; color: #A8116C; font-weight: bold; font-size: 15px; width: 100%; float: left" class="sub-message">Redirecting in <span class="time">5</span> seconds</span>

<?php 
$prev = load()->sys("session")->run()->get('_full_url');
$previous = $prev ? urldecode($prev):  admin_link();

$s = explode("/", $previous);
$ref = $s[(count($s)-1)];
?>
<script>
    var time = parseInt($(".time").html());
    
    function timer(){
        if(time == 0){
            window.location.href = "<?php echo $previous; ?>";
            exit(0);
        }else { 
            setTimeout(timer, 1000)
        };
        $(".time").html(time);
        time--;
    }
    
    <?php  if($ref != SEG_VARS(0)){ ?>
        timer();
    <?php }else { ?>
        $(".sub-message").html("");
    <?php } ?>
    
</script>
<?php  admin_page_include('includes/footer.php')?>