<?php admin_page_include('includes/header.php'); ?>

<?php
admin_page_include('includes/functions.php');
$row_identifier = unique_string();
?>
    <style>
        .main-wrapper {
            width: 800px;
            float: left;
            position:relative;
        }
        .main-wrapper-heading{
            width: 100%;
            border-bottom: 1px solid #999;
            margin: 0 0 30px 0;
            float: left;
            padding:0 0 8px 0;
        }
        .main-wrapper-heading h2 {
            display: inline-block;
            float: left;
        }
        .main-wrapper-heading .operation {
            float: right;
        }
        
        .main-wrapper-heading .status,
        .main-wrapper-heading .save-overall-right {
            display: inline;
            float: right;
        }
        
        .main-wrapper-heading .status {
            padding-top: 8px;
            padding-right: 15px;
        }
        
        .main-container {
            width: 100%;
            float: left;
            position:relative;
            display: block;
            overflow-y: auto;
            overflow-x: hidden;
            left: 0;
            top: 0;
        }
        
        .data-row {
            width: 100%;
            float: left;
            height: 150px;
            border: 2px dashed #999;
            margin-bottom: 15px;
            background-color: #E0E8E0;
            position: relative
        }
        
        .data {
            width: 696px;
            float: left;
            height: 100%;
        }
        
        .data-item {
            float: left;
            margin: .7% 5px;
            border: 1px solid rgba(59, 255, 153, 0.35);
            height: 92%;
            background-color: #FFF;
            box-shadow: #045d7d 0 0 5px;
        }
        
        .data .two-col {
            width: 440px;
        }
        
        .data .one-col {
            width: 220px;
        }
        
        .add-data {
            width: 100px;
            color: #151515;
            font-size: 20px;
            font-weight: bold;
            text-shadow: 0 1px  0px #777; 
            text-align: center;
            height: 100%;
            float: right;
            background-color: #555;
            padding-top: 5.5%;
        }
        .add-data-inactive-button {
            color: #595959;
            text-shadow: 0 1px  0px #CCC; 
            background-color: #999;
        }
        
        .add-data:hover {
            color: #000;
        }
        
        .add-data-inactive-button:hover {
            color: #595959;
            text-shadow: 0 1px  0px #CCC; 
            background-color: #999;            
        }
        
        .add-row {
            position: relative;
            width: 100%;
            color: #151515;
            font-size: 25px;
            font-weight: bold;
            text-shadow: 0 1px  0px #555; 
            text-align: center;
            height: 50px;
            float: right;
            background-color: #333;
            padding-top: 10px;
        }
        
        .add-row:hover {
            background-color:#005522;
            text-shadow: 0 -1px  0px #000011; 
            color: #DDD;
        }
        
        .data-item-in {
            text-align: center;
        }
        
        .data .head {
            font-size: 15px;
            width: 100%;
            height: 30px;
            background-color: #00AEED;
            text-align: center;
            padding: 5px;
            float: left;
            color: #000;
        }
        .data .column-type {
            float: left;
            width: 100%;
            margin: 10px 0;
        }
        
        .data .edit-section {
            width: 80%;
            margin: 10px 0;
        }
        
        .data-head {
            width: 100%;
            height: 30px;
            float: left;
            background-color: #00AEED;
            line-height: 30px;
            padding-left: 15px;
            cursor: move;
        }
        .data-body {
            width: 100%;
            float: left;
            background-color: #FFF;
            padding: 15px;
        }
        #cl_color {
            width: 15px;
            background-color: none;
            display: inline-block;
            float: left;
            cursor: pointer;
        }
        
        #full-popover {
            width: 80px;
            display: inline;
            float: none;
            height: 15px;
            font-size: 12px;
            box-shadow: none;
            cursor: pointer;
        }
    </style>
    
<?php
    $containers = array();
    $initial_row_class = _unique_id();
    $initial_data_class = _unique_id();
    $sec_key_right = get_config("meta_attr/section_key_right"); //section key
    $meta = load()->sys("MetaInfo")->run("theme_info","info_key", "info_value");
    $section_info_right = $meta->get($sec_key_right);
    $informations = json_decode($section_info_right, true);
?>

    <form action="" method="post" style="position: relative">
        <div class="main-wrapper"  style="position: relative">
            <div class="main-wrapper-heading" >
                <h2 class="main-body-page-header">Manage Right Section</h2>
                <div class="operation">
                    <button class="save-overall-right">Save</button>
                    <div class="status"></div>
                </div>
            </div>
            <input type="hidden" value="<?php echo $sec_key_right; ?>" name="reference_key" id="reference_key">
            <div class="main-container">
                
                <?php if(count($informations)): foreach ($informations as $information): $unique = _unique_id();?>
                    <?php
                        $containers[] = $unique;
                        $cl_name = _get("class", "class_id", $information['class_id'], "name");
                    ?>
                    <div class='data-row <?php echo $unique ?>'>
                            <input type='hidden' value='<?php echo $information['class_id'] ?>' id='class_id'>
                            <input type='hidden' value='<?php echo $information['view'] ?>' id='view'>
                            <input  type="hidden" value="<?php echo $information['cl_color']?>" id="cl_color" >
                        <div class='data-head' style="background-color: <?php echo $information['h_color']?>">
                            <span class='cl_name' style="float: left; color:<?php echo $information['cl_color']?>"><?php echo $cl_name ?></span>
                        </div>
                        <div class='data-body'>
                            <div class='view-type'>View: <span class='v_name'><?php echo $information['view'] ?></span></div>
                            <div class="color-picker">
                                Heading Color: <input type="text" class="form-control" id="full-popover" value="<?php echo $information['h_color']?>" data-color-format="hex">
                            </div>
                            <div class='edit-button'>
                                <a href='javascript:'  id='<?php echo $unique ?>'>Edit Section</a>
                            </div>
                            <div class='remove-button'>
                                <a href='javascript:' id='<?php echo $unique ?>'>Remove</a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; endif;?>
            </div>
            <a class="add-row" href="javascript:__add_right_section()">Add a Section</a>
        </div>
    </form>
    <script>

    </script>
    
    <script src="<?php echo admin_url()."/js/manage_section.js"?>"></script>
    
    <script>
        <?php foreach($containers as $cont_id): ?>
            _modify_right_section('<?php echo $cont_id ?>', true);
            
            $(".<?php echo $cont_id?>").find("input#full-popover").ColorPickerSliders({
                placement: 'auto',
                hsvpanel: true,
                previewformat: 'hex',
                order: {
                    rgb: 1
                  },
                swatches: ["#00AEED","red", "green", "blue","yellow", "violet", "skyblue", "orange"],
                customswatches: false,
                onchange: function(c, color){
                    if (color.cielch.l < 60) {
                        $(".<?php echo $cont_id?>").find(".cl_name").css("color", "#F1F1F1");
                        $(".<?php echo $cont_id?>").find("#cl_color").val("#F1F1F1");
                    }else {
                        $(".<?php echo $cont_id?>").find(".cl_name").css("color", "#333333");
                        $(".<?php echo $cont_id?>").find("#cl_color").val("#333333");
                    }
                    var color  = $(".<?php echo $cont_id?>").find("input#full-popover").val();
                    $(".<?php echo $cont_id?>").find(".data-head").css("background-color", color);
                    cons(color);
                }
            });
            
        <?php endforeach; ?>
    
    $(".main-container").sortable({ tolerance:"pointer", cursor:"move", 
        revert:true, opacity: 0.8,
        axis: "y",
        scroll : true, 
        handle: '.data-head',
    });
    
    </script>
<?php admin_page_include('includes/footer.php')?>
