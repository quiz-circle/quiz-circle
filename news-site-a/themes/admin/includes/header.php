<?php
function get_date_format(){ return  "d M y, h:ia"; }
global $user;
if(!$user->hasControlAccess()){
    echo "You have not the access to the control panel!<br>";
    echo "<h4><a href='" . admin_link() . "/login'>Login With Control Accessable Account</a></h4>";
    exit();    
}
?>
<script>
    var ABSPATH = '<?php echo ABSPATH; ?>';
    var admin_path = '<?php echo admin_url(); ?>';
    var theme_path = '<?php echo theme_url(); ?>';
    var base_url = '<?php echo base_url(); ?>';
    var admin_url = '<?php echo admin_link(); ?>';
</script>


<!DOCTYPE html>
<html>
    <head>
        <title>
        
        <?php
            $name = ucwords(str_replace(array("-","_"), array(" "," "), admin_page_name()));
            
            switch ($name){
                case "List":
                    echo "News List";
                    break;
                case "Class":
                    echo ucfirst(GET_DATA("type"));
                    break;
                default :
                    echo $name;
            }
            
            if(admin_page_name() == "") echo "Dashboard";
            
            echo " &raquo News Admin Panel";
            
        ?>
        </title>
        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <!--Color Picker css-->
        <link href="<?php plugin_url("color-picker")?>/src/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
        <link href="<?php plugin_url("color-picker")?>/src/bootstrap.colorpickersliders.css" rel="stylesheet" type="text/css" media="all">
        <!-------------------->
        
        <!-- Style sheet -->
        <link href="<?php echo admin_url()."/css/reset.css"?>" rel="stylesheet">
        <link href="<?php echo admin_url()."/css/style.css"?>" rel="stylesheet">
        <!--<link href="<?php plugin_url("color-picker")?>/" rel="stylesheet">-->
        
        <script src="<?php echo admin_url()."/js/jquery-1.10.2.js"?>" type="text/javascript"></script>
        <script src="<?php echo admin_url()."/js/jquery-ui.min.js"?>" type="text/javascript"></script>
        <script src="<?php echo admin_url()."/js/ajax-functions.js"?>" type="text/javascript"></script>
        <script src="<?php echo admin_url()."/js/fsinheader.js"?>" type="text/javascript"></script>
        <script src="<?php echo base_url()."/editor/ckeditor/ckeditor.js"?>" type="text/javascript"></script>
        
        <!--Color picker Script-->
        <script src="<?php plugin_url("color-picker")?>/src/bootstrap.min.js"></script>
        <script src="<?php plugin_url("color-picker")?>/src/tinycolor.min.js"></script>
        <script src="<?php plugin_url("color-picker")?>/src/bootstrap.colorpickersliders.js"></script>
        <!----------------------->
        
        <!--<?php plugin_url("color-picker")?>-->
        
        <?php
            $user_id = $user->getID();
            $user_display_name = _get('user', 'user_id', $user_id, 'display_name');
            
            $profile_image_link = admin_url()."/images/image12.png";
        ?>
        
        <!--<script src="//cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>-->
    </head>
    <body>
        <div id="main">
            <div id="main-in">
                <div id="top-header">
                    <h2 class="brand-header">
                        Control Panel
                    </h2>
                    <a target="_blank" href="<?php echo base_url()?>" class="view-site">View Site</a>
                    <ul class="header-menu-right">
                        <li>
                            <div class="profile-image">
                                <img src="<?php echo $profile_image_link; ?>" >
                            </div>
                            <a href="profile.php"><?php  echo trim(strip_tags($user_display_name)) !== "" ? trim(strip_tags($user_display_name)):"<span style='color:#999'>user</span>"; ?>
                                <span style=" color: inherit;border-width: 5px; border-top-width: 5px; border-bottom: none" class="caret"></span>
                            </a>
                            
                            <ul class="profile-drop-down">
                                <div class="profile-image-dropdown">
                                    <img src="<?php echo $profile_image_link; ?>">
                                </div>
                                <li><a href="<?php echo getAdminPath()."/profile"?>">Profile</a></li>
                                <li><a href="<?php echo getAdminPath()."/logout"?>" onclick="return confirm('Are you sure, want to logout?')" >Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div id="sidebar">
                    <?php
                        $cap = $GLOBALS['_get_cap'];
                    ?>
                    <div id="manu-cont">
                        <p style="text-align: center">
                            <ul class="ad-menu" style="float:left; padding: 0; margin: 0">
                                <li><a href="<?php echo getAdminPath()?>">Dashboard</a></li>
                                <li><a href="<?php echo getAdminPath()."/list"?>">News</a>
                                    <ul>
                                        <li><a href="<?php echo getAdminPath()."/list"?>">News List</a></li>
                                        <li><a href="<?php echo getAdminPath()."/add-news?action=add"?>">Add News</a></li>
                                        <li><a href="<?php echo getAdminPath().'/pin-news?category=' . most_news_category() ?>">Pin News</a></li>
                                        <li><a href="<?php echo getAdminPath().'/comments-list' ?>">Comment List</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo getAdminPath()."/class?type=category"?>">Category</a>
                                    <ul>
                                        <li><a href="<?php echo getAdminPath()."/class?type=category"?>">Category List</a></li>
                                        <li><a href="<?php echo getAdminPath()."/add-class?type=category"?>">Add Category</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo getAdminPath()."/class?type=tag"?>">Tag</a>
                                    <ul>
                                        <li><a href="<?php echo getAdminPath()."/class?type=tag"?>">Tag List</a></li>
                                        <li><a href="<?php echo getAdminPath()."/add-class?type=tag"?>">Add Tag</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo getAdminPath()."/paper-list"?>">News Paper</a>
                                    <ul>
                                        <li><a href="<?php echo getAdminPath()."/paper-list"?>">News Paper List</a></li>
                                        <li><a href="<?php echo getAdminPath()."/add-news-paper"?>">Add News Paper</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo getAdminPath()."/page-list"?>">Pages</a>
                                    <ul>
                                        <li><a href="<?php echo getAdminPath()."/page-list"?>">All Pages</a></li>
                                        <li><a href="<?php echo getAdminPath()."/add-page?action=add"?>">Add Page</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo getAdminPath()."/images-library?operation_link=".base_url()."/editor/ckeditor/plugins/imageuploader/imgbrowser.php?CKEditor=editor&CKEditorFuncNum=1&langCode=en"?>">Images Library</a></li>
                                <!--
                                <li><a href="<?php echo getAdminPath()."/paper-list"?>">Daily News Papers</a></li>
                                <li><a href="<?php echo getAdminPath()."/add-news-paper"?>">Add Newspaper</a></li>
                                <li><a href="<?php echo getAdminPath()."/manage_headlines"?>">Manage Headlines</a></li>
                                -->
                                <?php if($cap->edit_theme): ?>
                                <li><a href="javascript:">Manage Theme</a>
                                    <ul>
                                        <li><a href="<?php echo getAdminPath()."/manage-nav"?>">Manage nav</a></li>
                                        <li><a href="<?php echo getAdminPath()."/manage-section"?>">Manage Section</a></li>
                                        <li><a href="<?php echo getAdminPath()."/headlines-category?meta-attibutes=headlines_class_key"?>">Set Headline Class</a></li>
                                        <li><a href="<?php echo getAdminPath()."/headlines-category?meta-attibutes=spcial_news_class_key"?>">Spcial news Category</a></li>
                                        <li><a href="<?php echo getAdminPath()."/headlines-category?meta-attibutes=educationist"?>">Educatonist Category</a></li>
                                        <li><a href="<?php echo getAdminPath()."/headlines-category?meta-attibutes=organigation"?>">Organigation Category</a></li>
                                        <li><a href="<?php echo getAdminPath()."/headlines-category?meta-attibutes=journalist"?>">Journalist Category</a></li>
                                        <li><a href="<?php echo getAdminPath()."/headlines-category?meta-attibutes=schoalrship"?>">Schoalrship Category</a></li>
                                        <li><a href="<?php echo getAdminPath()."/headlines-category?meta-attibutes=university"?>">University Category</a></li>
                                        <li><a href="<?php echo getAdminPath()."/headlines-category?meta-attibutes=ministar-gallery"?>">Minister Gallery</a></li>
                                    </ul>
                                </li>
                                <?php endif; ?>
                                <li><a href="<?php echo getAdminPath()."/#setting"?>">Settings</a></li>
                                <li><a href="<?php echo getAdminPath()."/#setting"?>">User</a></li>
                            </ul>
                        </p>
                    </div>
                </div>
                <div id="main-body-wrapper">
                    <div id="main-body">
                        <style>
                            .main-message {
                                border: 1px solid green;
                                float: left;
                                background-color: #EED;
                                color: green;
                                padding:12px 24px;
                                width: 100%;
                                margin-bottom: 15px;
                                position:relative;
                                display: none;
                            }
                            .main-message .mm-body {
                                width: 100%;
                                float: left;
                                display: inline-block;
                            }
                            .main-message .mm-close-button {
                                width: 20px;
                                height: 20px;
                                float: left;
                                border: 1px solid #888;
                                text-align: center;
                                position: absolute;
                                right: 10px;
                                top: 10px;
                                cursor: pointer;
                                display: none;
                                background-color: #999;
                                box-shadow: inset 0 0 4px #DDD;
                                color:#FFF;
                            }
                            
                            .main-message:hover > .mm-close-button,
                            .main-message:focus > .mm-close-button {
                                display: block;
                            }
                            
                        </style>
                        
                        <div class="main-message" tabindex="1">
                            <div class="mm-body">Message</div>
                            <div class="mm-close-button" tabindex="2" >x</div>
                        </div>
                        
                        <script>
                            //showMainMessage("hello...");
                            //showMainMessage("hello...", "purple");
                        </script>
    
                        <?php //show_main_message("hi.....", "magenta"); ?>
                                    
                                    
                        
            <?php if($user->isAdmin()){ ?>
                <style>
                    #top-header, #sidebar {
                        background-color: #103005;
                    }
                </style>
            <?php } ?>