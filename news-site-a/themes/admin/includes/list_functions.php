<?php

/*--------- Functions for list.php page -----------*/

function make_news_config(){
    $conf = array();

    if(GET_DATA('category')){
        $conf['class_url_ref'] = urldecode(GET_DATA('category'));
        $conf['class-type'] =  "category";
    }

    if(GET_DATA('tag')){
        $conf['class_url_ref'] =  urldecode(GET_DATA('tag'));
        $conf['class-type'] =  "tag";
    }

    if(GET_DATA('year')){
        $d_query = GET_DATA('year'); 
        if(GET_DATA('month')){
            $d_query = $d_query ."-".GET_DATA('month');
            if(GET_DATA('day')){
                $d_query = GET_DATA('year') ."-".GET_DATA('month').'-'.GET_DATA('day');
            }
        }

        $conf['added-date'] =  array("LIKE", "%".$d_query."%");
    }

    if(GET_DATA('modified-date')){
        $conf['modified-date'] =  array("LIKE", GET_DATA('modified-date'));
    }

    if(isset($_GET['visibility'])){
        if(GET_DATA('visibility') !== ""){
            $conf['visibility'] =  GET_DATA('visibility');
        }
    }
    if(GET_DATA('content-type')){
        $conf['content-type'] =  urldecode(GET_DATA('content-type'));
    }

    if(GET_DATA('paper-id')){
        $conf['paper-id'] =  GET_DATA('paper-id');
    }

    if(GET_DATA('author')){
        $conf['author'] =  urldecode(GET_DATA('author'));
    }

    return $conf;
}

function edit_button($each){
    $u = $GLOBALS['user'];
    $cap = $GLOBALS['_get_cap'];        
    $usname = _get('user', 'user_id', $u->getID(), 'username');
    $e_query_name  = "";

    $tb = "<a href='". admin_link()."/add-news?action=edit&id=";
    $tb .= $each['content_id'];
    $tb .= "' class='details-edit'>Edit</a>";
    return $tb;
}

function delete_button(){

}

function make_trash_list_sql($ids){
    $info_key = "news-trash";
    load()->sys("MetaInfo")->open();
    $meta_opr = $meta = new MetaInfo("site_info", "info_key", "info_value");
    $value  = array();
    $value = json_encode($value);
    if(!$meta->is_exists($info_key)){
        $meta->add($info_key, $value);
    }
    $meta = $meta->get($info_key);
    $meta = json_decode($meta, true);

    if(count($ids)){
        $index = 0;
        while ($index < count($ids)){
                $sql = "";
                $old_status = _get('content', 'content_id', $ids[$index], 'status');
                if($old_status !== 'trash'){
                    $meta[] = array(md5($ids[$index]) => $old_status);
                }
            $index++;
        }
    }

    $meta_opr->edit($info_key, json_encode($meta));
    $meta_edit = $meta_opr->get_status(); 
    if(!$meta_edit->error()){
        //Explain(json_encode($meta));
        return "UPDATE content SET status = 'trash' ";
    }else {
        //Explain(json_encode($meta));
        return "";
    }
}

function make_approved_list_sql($ids){
    return "UPDATE content SET status ='publish', visibility = 1 ";
}

function GET_SELECTED($key, $value){
    if(isset($_GET[$key])){
        if(GET_DATA($key) !== ""){
            if(GET_DATA($key) == $value){
                echo "selected";
            }
        }
    }
}

function show_day_month_year($input = "post", $prefix = ""){
    
    $day = $prefix ? $prefix."_day":"day";
    $month = $prefix ? $prefix."_month":"month";
    $year = $prefix ? $prefix."_year":"year";
    ?>
    <label for="date">Date</label>
    <select name="<?php echo $day ?>" id="<?php echo $day ?>">
        <?php _option_range(1,31, "<option value=''>Day</option>", GET_DATA($day))?>
    </select>
    <select name="<?php echo $month ?>" id="<?php echo $month ?>">
        <?php _option_range(1,12, "<option value=''>Month</option>", GET_DATA($month))?>
    </select>
    <select name="<?php echo $year ?>" id="<?php echo $year ?>">
        <?php _option_range(2016,1980, "<option value=''>Year</option>", GET_DATA($year))?>
    </select>
    <?php
}


function _option_range($start, $end = "", $first_html = "", $selected = ""){
    echo $first_html;
    if(is_int($start)){
        if($start < $end){
            for($i = $start;  $i<= $end; $i++):
                $val  = $i < 10 ? "0".$i:$i;
            ?>
            <option value="<?php echo $val; ?>" <?php echo $i == $selected ? "selected":""  ?>> <?php echo $val?> </option>
            <?php 
            endfor;
        }else {

            for($i = $start;  $i >= $end; $i--):
            $val  = $i < 10 ? "0".$i:$i;
            ?>
            <option value="<?php echo $val; ?>" <?php echo $i == $selected ? "selected":""   ?>> <?php echo $val?> </option>
            <?php 
            endfor;
        }
    }else if(is_array($start)){
        foreach($start as $key => $value):
        ?>
        <option value="<?php echo $key; ?>"><?php echo $value?> </option>
        <?php 
        endforeach;
        
    }
}