<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function get_nav_items($function, $class_id, $html = "", $attr = ""){
    $nav = load()->lib("NavList")->run($class_id);

    $nav->first_level_tag("ul  class='sort'  id='nav-list'");
    $nav->unique_id = true;
    $nav->set_html($html, true);
    $nav->set_is_tree(true);

    if($attr){
        $nav->setAttrs($attr);
    }
    $nav->set_elemets_oper($function);
    return $nav;
}

function nav_menu_func($row, $ac, $l, $i, $th){
    $values = array(
        'id' => $row['item_id'],
        'title' => $row['item_title'],
        'url' => $row['ref_url'],
        'type' => $row['item_type'],
    );
    return _nav_menu_each($values, $th->hasChild($row['item_id']));
}

function _nav_menu_each($values, $hasChild = false){
    $id = isset($values['id']) ? $values['id']:"";
    $title = isset($values['title']) ? $values['title']:"";
    $url = isset($values['url']) ? $values['url']:"";
    $type = isset($values['type']) ? $values['type']:"";
    
    $res = "";
        $res .= '<div class="container">';

                $res .= '<div class="title" id="'. md5($id).'">';
                    $res .= '<input type="hidden" value="' . $id . '" id="item_id">';
                    $res .= '<div class="name">'  . $title . '</div>';
                    $res .= '<div class="item-type">'  . ucfirst($type) . '</div>';
                    $res .= '<div class="edit-button" id="'.md5($id).'"><a href="javascript:">Edit</a></div>';
                    //$res .= '<div class="handle"><span class="glyphicon glyphicon-move"></span></div>';
                $res .= '</div>';
                $res .= '<div class="item-editing '. md5($id).'">';
                    $res .= '<label for="item_name">Name</label>';
                    $res .= '<input type="text" value="'.$title.'" id="item_name">';
                    $res .= '<label for="item_url_ref">URL</label>';
                    $res .= '<input type="text" value="'.$url.'" id="item_url_ref">';
                    
                    $res .= '<a href="javascript:" class="delete-item" style="color:magenta;" id="'.md5($id).'" >Delere</a>';
                $res .= '</div>';

    $res .= '</div>';
    if(!$hasChild){
        $res .= '<ul class="sort" >';
        $res .= '</ul>';
    }
    return $res;
} 
?>
