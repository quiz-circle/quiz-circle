<style>
        .overall {
            width: 100%;
            height: 100%;
            float: left;
            background-color: rgba(5,5,5,0.5);
            position: absolute;
            z-index: 50;
            position: fixed;
        }
        
        .overall-center {
            width: 600px;
            margin: 5% auto;
        }
        
        .overall-in {
            width: 100%;
            background-color: #FFF;
            border: 10px solid #005522;
            position: relative;
        }
        
        .overall-in .header {
            border-bottom: 1px solid #999;;
        }
        
        .overall-in .header .close{
            border-radius: 50%;
            height: 35px;
            width: 35px;
            margin: 0;
            padding: 0;
            outline: none;
            border: 1px solid #0077CC;
            position: absolute;
            right: -20px;
            top: -20px;
            box-shadow: none;
            font-size: 17px;
            font-weight: bold;
            cursor: pointer;
            float: left;
        }
        
        .overall-in .header {
            padding: 10px;
            height: 50px;
        }
        
        .overall-in .header h1 {
            margin: 0;
            padding: 0;
            font-size: 25px;
        }
        .overall-in .body {
            padding: 10px;
        }
        
</style>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
