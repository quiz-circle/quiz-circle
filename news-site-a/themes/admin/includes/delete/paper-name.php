<?php 

/*
$next = urldecode(GET_DATA("next"));

$delete->set_redirect_path(getAdminPath().$next , "db");
$delete->set_redirect_path(getAdminPath(), "query");
$delete->set_redirect_path(getAdminPath().$next , "del_success");

function delete_failed($p){
    global $next;
    //$np = $p->get_first();
    
    echo $next;
    /*
    if(has_entity_by_paper($np['paper_id'])){
        $message = "Can't Be deleted! There has one or more entity on <strong><em>".$np['paper_name']."</em></strong>!";
        return false;
    }else {        
        return true;
    }
    load()->sys("session")->run()->add("_delete_class", $message);
    redirect($next);
     
}

function delete_success(){
    load()->sys("session")->run()->add("_delete_class", $message);
    redirect($next);
}

$delete->set_failed_message("News Exists!", "delete_cond", "delete_failed");
$delete->set_failed_message("Successfully deleted!", "del_success");
$delete->set_failed_message("Error in deleteing author!", "del_db_error");

function has_entity_by_paper($id){
    return count(_get("content", "paper_id", $id));
}


function before($p){
    global $next;
    $np = $p->get_first();
    if(has_entity_by_paper($np['paper_id'])){
        $message = "Can't Be deleted! There has one or more entity on <strong><em>".$np['paper_name']."</em></strong>!";
        return false;
    }else {        
        return true;
    }
    load()->sys("session")->run()->add("_delete_class", $message);
    redirect($next);
}


$delete->set_condition("before");

$delete->delete();
$delete->get_message();
*/


global $delete;
$next = urldecode(GET_DATA("next"));

function has_entity_by_paper($id){
    return count(_get("content", "paper_id", $id));
}


$delete->set_redirect_path("./", "db");
$delete->set_redirect_path("./", "query");

if($delete){
    $delete->set_redirect_path($next, "del_success");
}

function success_func(){
    global $type;
    load()->sys("session")->run()->add("_delete_paper", ucfirst($type)." Deleted successfully!");    
}

$delete->set_failed_message("Successfully deleted!", "del_success", 'success_func');
$delete->set_failed_message("Error in deleteing news!", "del_db_error");



function before($p){    
    $next = urldecode(GET_DATA("next"));
    Explain($p);
    $np = $p->get_first();
    if(has_entity_by_paper($np['paper_id'])){
        $message = "Can't Be deleted! There has one or more entity on <strong><em>".$np['paper_name']."</em></strong>!";
        load()->sys("session")->run()->add("_delete_paper", $message);
        redirect($next);
        return false;
    }else {        
        return true;
    }
}

$delete->set_condition("before");

$delete->delete();

$delete->get_message();
