<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function class_references($row, $ac, $level){
    $level--;
    $selected = ($ac == $row['class_id'] ) ? "selected":"";
    return  "<option value='" .$row['class_id'] . "'>" .child_sym($level, "--"). $row['name'] . "</option>";
}

function print_templates($templates, $editing_template, $config = array()){
    if( count($config) == 0 ){
        $config["id"] = "template";
        $config["name"] = "template";
        $config["class"] = "template";
    }
    
    echo "<select name='" . $config['name'] . "'  id='" . $config['id'] . "' class='" . $config['class'] . "'>";
    echo '<option value="">-Default-</option>';
    if(is_array($templates)){
        foreach ($templates as $template => $page_name){
            echo "<option value='".$template."'";
            echo ($editing_template == $template) ? " selected ":" ";
            echo ">" . $template . "</option>";
        }
    }
    echo "</select>";
}

function add_class_relation($classes, $entity_id, $type= "category"){
    $exists_sql = "SELECT * FROM class_relationship NATURAL JOIN class NATURAL JOIN class_type WHERE entity_id = $entity_id AND type='$type'";
    $ins_ids = array();
    $del_ids = array();
    
    $ex = DB::run()->setSql($exists_sql)->run_sql();
    
    $db_class = array();
    
    foreach($ex->get_array() as $dbc){
        $db_class[] = $dbc['class_id'];
    }

    $ins_sql = "INSERT INTO `class_relationship` ( `entity_id`, `class_id`) VALUES";
    
    $co = 0;
    
    foreach ($classes as $c){
        if(!in_array($c, $db_class)){
            $ins_ids[] = $c;
            $ins_sql .= ($co!=0) ? " , ":"";
            $ins_sql .= "($entity_id, $c) ";
            $co++;
        }
    }
    
    if($co){
        DB::run()->setSql($ins_sql)->run_sql();
    }
    
    $del_sql = "DELETE FROM `class_relationship` WHERE `entity_id` = $entity_id AND `class_id` IN(";
    $c = 0;
    
    foreach ($db_class as $db){
        if(!in_array($db, $classes)){
            $del_ids[] = $db;
            $del_sql .= ($c!=0) ? ", ":"";
            $del_sql .= "$db";
            $c++;
        }
    }
    $del_sql .= ")";
    
    if($c){
        DB::run()->setSql($del_sql)->run_sql();
    }
}