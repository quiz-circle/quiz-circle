<?php
require_once '../../../root/load-settings.php';

function _get_class($type = "category", $init_html = null, $selected = null, $limit = null){
    $tree = load()->lib('ClassList')->run($type);
    $tree->set_active_param($selected);
    $tree->init_html($init_html);
    
    if($limit !== null){
        $tree->data_limit($limit);
        $tree->set_is_tree(false);
    }else {
        $tree->set_is_tree(true);
    }
    
    $tree->set_elemets_oper("_fetch_list_element");
    return $tree;
}


function _fetch_list_element($row){
    $values = array();
    $values['id'] = $row['class_id'];
    $values['title'] = $row['name'];
    $values['type'] = 'category';
    return list_item($values);
}

$data = false;
$order_by = "";

$recent_limit = 5;
$result = '<ul class="fetched-content">';
$content = null;

if(POST_DATA()){
    $ref = (string) strtolower(POST_DATA('reference'));
    $range =  (string) strtolower(POST_DATA('range'));
    //echo $ref;
    //if($ref === "categories") 
    switch ($ref){
        case ".categories":
            $content = ($range === 'all') ?  _get_class("category", "", "", null):_get_class("category", "", "", $recent_limit);
            $order_by = 'class_id';
            break;
        case ".tags":
            $content = ($range ==='all') ?  _get_class("tag", "", "", null):_get_class("tag", "", "", $recent_limit);
            $order_by = 'class_id';
            break;
        case '.pages':
            $content = "page";
            $order_by = 'content_id';
            break;
        default :
            echo "Parsing Error!";
    }
    
    
    if($content !== "page"){
        if(is_object($content)){
            if($content->get_total()){
                $data = true;
                $result .= $content->get_data();
            }
        }
    }else {
        $p = DB::run()->read('content')->where(array('content_type' => 'page'))->run_sql();
        if($range !== "all"){
            $p->limit($recent_limit);
            $p->order_by($order_by, "DESC");
        }
        
        if($p->error()){
            $data = false;
            echo "Internal Error";
        }else {
            if($p->get_count()){
                $data = true;
                $con = $p->get_array();
                foreach ($con as $cont){
                    $values['id'] = $cont['content_id'];
                    $values['title'] = $cont['content_title'];
                    $values['type'] = "page";
                    $result .= list_item($values);
                }
            }
        }
    }
}

$result .= '</ul>';

if($data) echo $result;
    /*
    for($i=0; $i<= 10; $i++){
        $values['id'] = $i;
        $values['title'] = "Name - ".$i;
        $values['type'] = $ref;
        
        echo list_item($values);
    }
    */

function list_item($values){
    $id = isset($values['id']) ? $values['id'] : "";
    $title = isset($values['title']) ? $values['title'] : "";
    $type = isset($values['type']) ? $values['type'] : "";
    
    $unique = _unique_id();
    $result = '<li>';
        $result .= '<input type="hidden" value="'.$type.'" id="type">';
        $result .= '<input type="checkbox" id="'.$unique.'" name="ids"  class="select-content" value="'.$id.'">';
        $result .= '<label for="'.$unique.'" class="selection-level">'.$title.'</label>';
    $result .= '</li>';
    return $result;
}
?>