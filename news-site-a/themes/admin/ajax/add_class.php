<?php
header('Content-type: text/javascript');
//error_reporting(0);
require_once '../../../root/load-settings.php';
$result = array(
    "status"        =>  false,
    "message"       =>  "",
    "parent_id"     =>  "",
    "last_id"       =>  0
);

$cname = isset($_GET['name']) ? $_GET['name']:"";
$des = (isset($_GET['description']) && !empty($_GET['description'])) ? $_GET['description']:"";
$parent_id = isset($_GET['parent_id']) ? (int) $_GET['parent_id']:0;
$type = isset($_GET['type']) ? $_GET['type']:"";
$is_parent = isset($_GET['is_parent'])  ? $_GET['is_parent']:"";
$order = 0;
if($is_parent === "true" && $parent_id != 0){
    $or = DB::run()->read("class","order")->where( 'class_id', $parent_id )->limit(1);
    $or = $or->run_sql();
    $ord  = $or->get_first();
    $order = (int) $ord['order'];
    $order++;
}
$url_ref = ($type == "tag") ? make_unique_class($cname."-".$type, 'tag'):make_unique_class($cname, $type, $parent_id, $order);

if(has_class($cname, $parent_id, $type)){
    $result['message'] = ucfirst($type)." exists!";
}else {
    
    $result['message'] = ucfirst($type).", ".$url_ref;
    
    $cl_insert = DB::run()->write("class")->values(
        array(
            'name'      =>  $cname,
            'order'     =>  $order,
            'url_ref'   =>  $url_ref,
        )
    )->run_sql();
    
    
    if($cl_insert->error()){
        $result['message'] = ucfirst($type)." insertion failed!". $cl_insert->sql_error();
    }else {
        $class_id = $cl_insert->last_insert_id();
        $cl_type_ins = DB::run()->write("class_type")->values(
            array(
                'class_id'      =>  $class_id,
                'type'          =>  $type,
                'parent'        =>  $parent_id,
                'description'   =>  $des
            )
        )->run_sql();

        //echo "SQL ERROR: ".$cl_type_ins->sql_error()."<br>";
        
        
        if($cl_type_ins->error()){
            $result['message'] = ucfirst($type)." not successfully inserted!, ";
        }else {
            $result['status'] = true;
            $result['parent_id'] = $parent_id;
            $result['last_id'] = $class_id;
            $result['message'] = ucfirst($type)." successfully inserted! ";
        }
    }
     
}

echo json_encode($result);
exit(0);
