<?php
require_once '../../../root/load-settings.php';
admin_page_include("includes/manage_nav_functions.php");

function makeWhereRange($ids, $column){
    $wh = " WHERE {$column} IN(";
    if(is_array($ids)){
        $counter = 0;
        foreach ($ids as $id){
            $wh .= ($counter != 0 ) ? ", ":" ";
            $wh .= $id;
            $counter++;
        }
    }
    $wh .= ") ";
    return $wh;
}


if(POST_DATA()){
    $values = array();
    //Explain(POST_DATA());
    $reference = POST_DATA('reference');
    $ids = POST_DATA('ids');
    $sql = "SELECT ";
    $type = "";
    switch ($reference){
        case "categories":
        case "tags":
            $sql .= "class_id as id, ";
            $sql .= "name as title, ";
            $sql .= "url_ref as url_ref ";
            $sql .= " FROM class";
            $sql .= makeWhereRange($ids, "class_id");
            $type = ($reference === "categories") ? "category":"tag";
            break;
        case "pages":
            $sql .= "content_id as id, ";
            $sql .= "content_title as title, ";
            $sql .= "url_ref ";
            $sql .= " FROM content";
            $sql .= makeWhereRange($ids, "content_id");
            $type = "page";
            break;
        case "custom":
            $type = "custom";
            break;
        default :
            echo "_PE_";
    }
    
    $nav_reference = POST_DATA("nav_reference");
    $nav_refence_value = POST_DATA("nav_reference_value");
    
    $n_id = _get('class', 'url_ref', $nav_reference, 'class_id');
    $result = "No Result";
    if($nav_refence_value === md5($n_id)){
        if($type !== "custom"){
            $data = DB::run()->setSql($sql)->run_sql();
            if($data->get_count()){
                $result = "";
                $inDB = DB::run();
                foreach($data->get_array() as $item){
                    $inserted_values = _add_an_item($n_id, $item, $type, $item_id);
                    if(count($inserted_values)){
                        $v['id'] = $item_id;
                        $v['url'] = $inserted_values['ref_url'];
                        $v['title'] = $inserted_values['item_title'];
                        $v['type'] = $inserted_values['item_type'];

                        $result .= "<li id='".md5($item_id)."'>";
                            $result .= _nav_menu_each($v);
                        $result .= "</li>";
                    }
                }
            }
        }else {
            $result = "";
            $vals = POST_DATA('values');
            if($vals){
                $item['title'] = $vals['title'];
                $item['url_ref'] = $vals['url'];
                $inserted_values = _add_an_item($n_id, $item, "custom", $item_id);
                
                if(count($inserted_values)){
                    $v['id'] = $item_id;
                    $v['url'] = $inserted_values['ref_url'];
                    $v['title'] = $inserted_values['item_title'];
                    $v['type'] = $inserted_values['item_type'];

                    $result .= "<li>";
                        $result .= _nav_menu_each($v);
                    $result .= "</li>";
                }
            }
        }
    }else {
        echo "_RW_";
    }
}
echo $result;

function _add_an_item($n_id, $item, $type, &$item_id = 0){
    $max_sql = "SELECT max(priority) as max FROM nav_item WHERE class_id = ".$n_id." AND parent_id = 0";
    $mx = DB::run()->setSql($max_sql)->run_sql();
    if(!$mx->error()){
        if($mx->get_count()){
            $max = $mx->get_first();
            $max = (int) $max['max'];
        }else{
            $max = 0;
        }
    }else {
        $max = 0;
    }
    
    $max++;
    
    $item_id = 0;
    $inserting_value = array();
    $inserting_value['item_title'] = $item['title'];
    $inserting_value['ref_url'] = _make_url($item['url_ref'], $type);
    $inserting_value['item_type'] = $type;
    $inserting_value['class_id'] = $n_id;
    $inserting_value['priority'] = $max;
    
    $inDB = DB::run()->write('nav_item')->values($inserting_value)->run_sql();

    
    if(!$inDB->error()){
        $item_id = $inDB->last_insert_id();
        return $inserting_value;
    }
    return array();
}

function _make_url($url_ref, $type){
    if($type == "category"){
        return "{base_url}/{cat}/".$url_ref;
    }else if($type == "tag"){
        return "{base_url}/{tag}/".$url_ref;
    }else if($type == "page"){
        return "{base_url}/".$url_ref;
    }else if($type == "custom"){
        return $url_ref;
    }else {
        return "";
    }
}

function list_item($values){
    $id = isset($values['id']) ? $values['id'] : "";
    $title = isset($values['title']) ? $values['title'] : "";
    $type = isset($values['type']) ? $values['type'] : "";
    
    $unique = _unique_id();
    $result = '<li>';
        $result .= '<input type="hidden" value="'.$type.'" id="type">';
        $result .= '<input type="checkbox" id="'.$unique.'" name="ids"  class="select-content" value="'.$id.'">';
        $result .= '<label for="'.$unique.'" class="selection-level">'.$title.'</label>';
    $result .= '</li>';
    return $result;
}

?>