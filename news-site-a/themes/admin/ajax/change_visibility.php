<?php
header('Content-type: text/javascript');
error_reporting(0);
require_once '../../../root/load-settings.php';

$result = array(
    "status"            =>  false,
    "message"           =>  "",
    "change_vis"        =>  0,
    "sql_error"         =>  0
);


$id = GET_DATA('id');
if($id){
    $vis = (int) _get('content', 'content_id', $id, 'visibility');
    $vis = $vis == 1 ? 0:1;
    $vi = DB::run()->edit('content')->values('visibility', $vis)->where('content_id', $id)->run_sql();
    
    if($vi->error()){
        $result["message"]      =  "Error in changing Visibility!";
        $result['sql_error'] = $vi->sql_error();
    }else {
        $result["message"]      =  "Visibility status Changed Successfully!";
        $result['status']       = true;
        $result['change_vis']   = _get('content', 'content_id', $id, 'visibility');
        
        $category_id  = _get('class_relationship', 'entity_id', $id, 'class_id');
        
        
        update_class_counter($category_id);
    }
}

echo json_encode($result);
