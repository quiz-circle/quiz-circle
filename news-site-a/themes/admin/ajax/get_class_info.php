<?php
header('Content-type: text/javascript');
//error_reporting(0);
require_once '../../../root/load-settings.php';
$result = array(
    "status"        =>  false,
    "message"       =>  "",
    "class_name"     =>  "",
    "class_url_ref"       => "",
    "parent_id"       =>  "",
    "class_id"       =>  "",
);

$class_id = GET_DATA('class_id');
$type = GET_DATA('type');


$sql = "SELECT * FROM `class` NATURAL JOIN `class_type` where type = '$type' AND class_id = {$class_id}";

$class = DB::run()->read('class')->extra(" NATURAL JOIN `class_type`")->where(array('type' => $type, 'class_id' => $class_id))->run_sql();

if(!$class->error() && $class->get_count()){
    
    $class = $class->get_first_obj();
    $result['status'] = true;
    $result['message'] = "Successfully found!";
    $result['class_name'] = $class->name;;
    $result['class_url_ref'] = $class->url_ref; 
    $result['parent_id'] = $class->parent; 
    $result['class_id'] = $class_id; 
            
}else {
    $result['message'] = "This class not found!";
}

echo json_encode($result);
exit(0);
