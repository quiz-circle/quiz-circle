<?php
header('Content-type: text/javascript');
//error_reporting(0);
require_once '../../../root/load-settings.php';
$result = array(
    "status"        =>  false,
    "message"       =>  "",
    "parent_id"     =>  "",
    "last_id"       =>  0
);

$from = GET_DATA('from');
$to = GET_DATA('to');
$entity_id = GET_DATA('entity_id');
$class_id = GET_DATA('class_id');
$type = GET_DATA('type');

function rearange_pined_news($from, $to, $entity_id, $class_id, $type, &$status = false){
    if($from !== null && $to !== null && $entity_id && $class_id){
    
    if($to == 0 && $type == "unpin"){
        $upd_sql = "UPDATE class_relationship SET priority = 99 WHERE class_id = {$class_id} AND `entity_id` = " . $entity_id;
        
    }else if($from == 99 && $to == 0  && $type == "pin"){
        $pr = 1;
        $get_pr = DB::run()->read('class_relationship')->where(array('class_id'=>$class_id, 'priority' => array( 99, '!=')))->run_sql();
        $pr = (int) $get_pr->get_count();
        $pr++;
        $upd_sql = "UPDATE class_relationship SET priority = {$pr} WHERE class_id = {$class_id} AND `entity_id` = " . $entity_id;
    }else {
            if($from < $to){
                $change_sql = "UPDATE class_relationship SET priority = priority-1 WHERE priority <= {$to} AND priority > {$from} AND `class_id` = " . $class_id;
            }else if($from > $to) {
                $change_sql = "UPDATE class_relationship SET priority = priority+1 WHERE priority >= {$to} AND priority < $from AND `class_id` = " . $class_id;
            }
            $upd_sql = "UPDATE class_relationship SET priority = {$to} WHERE class_id = {$class_id} AND `entity_id` = " . $entity_id;

            if($from != $to){
                $a = DB::run()->setSql($change_sql)->run_sql();
                if(!$a->error()){
                    $b = DB::run()->setSql($upd_sql)->run_sql();
                    if(!$b->error()){
                        $status = true;
                    }
                }
            }

        }
    }
    $b = DB::run()->setSql($upd_sql)->run_sql();
    if(!$b->error()){
        $status = true;
        if($type == 'unpin'){
            
            $upd = "UPDATE class_relationship SET priority = priority-1 WHERE priority > $from AND class_id = {$class_id} AND priority != 99";

            $b = DB::run()->setSql($upd)->run_sql();
            if($b->error()){
                $status = false;
                $result['message'] = $b->sql_error();
            }else {
                $result['message'] = $b->get_sql();
            }
             
        }
    }else {

    }
}

rearange_pined_news($from, $to, $entity_id, $class_id, $type, $status);

if($status){
    $result['status'] = $status;
    //$result['message'] = "Status changed!";
}

echo json_encode($result);
exit(0);
