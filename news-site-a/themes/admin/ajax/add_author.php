<?php
header('Content-type: text/javascript');
error_reporting(0);
require_once '../../../root/load-settings.php';
$result = array(
    "status"        =>  false,
    "message"       =>  "",
    "last_id"       =>  0,
    "sql_error"     =>  0
);

$aname = isset($_GET['name']) ? $_GET['name']:"";
$nick_name = isset($_GET['nick_name']) ? $_GET['nick_name']:"";

$url_ref = str_replace(
    array('#','%','&','*','(',')','!','@',' '),
    array('-hash-','-percent-','-and-','-star-','-','-','-','-at-','-') , 
    strtolower($aname)
);

function auth_exists($auth){
    return $a = DB::run()->read("author")->where('auth_name', $auth)->run_sql()->get_count() ? true:false;
}

$url_reference = make_unique_ref($url_ref, "author");

if(auth_exists($aname)){
    $result['message'] = "This author exists!";
}else {
    $author = DB::run()->write("author")->values(
        array(
            'auth_name'         =>  $aname,
            'auth_url_ref'      =>  $url_reference,
            'nick_name'         =>  $nick_name,
        )
    )->run_sql();
    
    if($author->error()){    
        $result['message'] = "Author insertion failed!";
        $result['sql_error'] = $author->sql_error();
    }else {
        $result['last_id'] = $author->last_insert_id();
        $result['message'] = "Author successfully added!";
        $result['status'] = true;
    }

}

echo json_encode($result);
exit(0);
