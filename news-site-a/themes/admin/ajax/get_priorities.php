<?php
require_once '../../../root/load-settings.php';

$ac_pr = isset($_POST["active_pr"]) ? $_POST["active_pr"]:"-1";
$class_id = isset($_POST["class_id"]) ? $_POST["class_id"]:"-1";


$sql = "SELECT * FROM class_relationship WHERE priority != 99 AND class_id = {$class_id} order by priority";

$priority_db = DB::run()->setSql($sql)->run_sql();

echo $priority_db->sql_error()."<br>";

$priority = $priority_db->get_array();

$result = "";
if($priority_db->get_count()){
    foreach ($priority as $pr){
        $result .= "<option ";
        $result .= " value='".$pr['priority']."'" .($pr['priority'] == $ac_pr ? " SELECTED ":" ");
        $result .= ">";
        $result .= $pr['priority'];
        $result .= "</option>";
        
    }
}

echo $result;


