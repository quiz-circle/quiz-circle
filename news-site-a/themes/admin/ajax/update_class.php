<?php
header('Content-type: text/javascript');
error_reporting(0);
require_once '../../../root/load-settings.php';
$result = array(
    "status"        =>  false,
    "message"       =>  "",
);

$class_id = GET_DATA('class_id');
$url_ref = GET_DATA('url_ref');
$name = GET_DATA('name');
$parent = (int) GET_DATA('parent');


$values = array(
    'name' => $name,
    'url_ref' => $url_ref,
);

//echo json_encode($values);
//exit(0);

$type_values = array('parent' => $parent);



//update_class_information({"name":name, "url_ref":url_ref, "parent":parent, "class_id":class_id}, update_success, update_error );

$class = DB::run()->edit('class')->values($values)->where('class_id', $class_id)->run_sql();

if(!$class->error()){
    
    $type = DB::run()->edit('class_type')->values($type_values)->where('class_id', $class_id)->run_sql();   
    
    if(!$type->error()){
        $result['status'] = true;
        $result['message'] = "Successfully Updated";
    }else {
        $result['status'] = false;
        $result['sql'] = $class->get_sql().", ".$type->get_sql();
        $result['message'] = json_encode($type_values);
        $result['message'] = "Error in updating!";
    }
}else {
    $result['sql'] = $class->get_sql();
    $result['status'] = false;
    $result['message'] = "Updating Failed!";
}

echo json_encode($result);
exit(0);
