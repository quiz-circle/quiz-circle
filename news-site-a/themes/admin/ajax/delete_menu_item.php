<?php
require_once '../../../root/load-settings.php';
admin_page_include("includes/manage_nav_functions.php");

function hasChildNav($item_id){
    $ch = DB::run()->read('nav_item')->where('parent_id', $item_id)->run_sql();
    return (!$ch->error() && $ch->get_count());
}

$item_id = POST_DATA("item_id");

if(hasChildNav($item_id)){
    echo "<span style='color:#723'>This item has a child nav!</span>";
}else {
    $delete = DB::run()->delete('nav_item')->where("item_id", $item_id)->run_sql();
    
    if(!$delete->error() && $delete->has_changed()){
        echo "true";
    }else {
        echo "false";
    }
}
?>