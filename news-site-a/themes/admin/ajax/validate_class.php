<?php
header('Content-type: text/javascript');
error_reporting(0);
require_once '../../../root/load-settings.php';
$result = array(
    "status"        =>  false,
    "message"       =>  "",
);

$class_id = GET_DATA('class_id');
$type = GET_DATA('type');
$key = GET_DATA('key');
$value = GET_DATA('value');
$parent_id = GET_DATA('parent_id');

$where = array(
    $key => $value,
    "class_id" => array($class_id, "!=")
);

if($parent_id !== null){
    $where['parent'] =  $parent_id;
}

$class = DB::run()->read('class')->extra(" NATURAL JOIN `class_type`")->where($where)->run_sql();

if(!$class->error() && $class->get_count()){
    $result['status'] = true;
    $result['message'] = "This {$type} is exists!";
}else {
    $result['sql'] = $class->get_sql();
    $result['status'] = false;
    $result['message'] = "This {$type} is not exists!";
}
echo json_encode($result);
exit(0);
