<?php
header('Content-type: text/javascript');
//error_reporting(0);
require_once '../../../root/load-settings.php';
$result = array(
    "status"        =>  false,
    "message"       =>  ""
);

$data = isset($_GET['data']) ? $_GET['data']:"";
$ref_key = isset($_GET['reference_key']) ? $_GET['reference_key']:"";

if($data){
    $data = json_encode($data);
    
    $section_key = get_config("meta_attr/section_key");
    
    $section_key = $ref_key;

    $site_info = load()->sys("MetaInfo")->run("theme_info","info_key", "info_value");

    if(!$site_info->is_exists($section_key)){
        if($site_info->add($section_key, $data)){
            $result['status'] = true;
            $result['message'] = "Saved...";
        }else {
            $result['message'] = "Error in saving!";
        }
    }else {
        if($site_info->edit($section_key, $data)){
            $result['status'] = true;
            $result['message'] = "Saved!";
        }else {
            $result['message'] = "Error in saving!";
        }
    }
}else {
    $result['message'] = "Invalid!";
}

$result['test_reference'] = $ref_key;

echo json_encode($result);
exit(0);
