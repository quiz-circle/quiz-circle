<?php
require_once '../../../root/load-settings.php';
$ac_id = isset($_POST["active_id"]) ? $_POST["active_id"]:"-1";
$init_html = isset($_POST["init_html"]) ? $_POST["init_html"]:"<option value=\"0\">(no parent)</option>";
$class =  get_class_options("category", $init_html, $ac_id);
$class->set_is_tree(true);
echo ($class->get_total()) ? $class->show_data():$init;

