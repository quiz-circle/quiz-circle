<?php
header('Content-type: text/javascript');
error_reporting(0);
require_once '../../../root/load-settings.php';

$result = array(
    "status"            =>  false,
    "message"           =>  "",
    "change_vis"        =>  0,
    "sql_error"         =>  0
);


$id = GET_DATA('id');
if($id){
    $vis = (int) _get('comment', 'comment_id', $id, 'approved');
    $vis = $vis == 1 ? 0:1;
    $vi = DB::run()->edit('comment')->values('approved', $vis)->where('comment_id', $id)->run_sql();
    
    if($vi->error()){
        $result["message"]      =  "Error in changing Approval!";
        $result['sql_error'] = $vi->sql_error();
    }else {
        $result["message"]      =  "Approval status Changed Successfully!";
        $result['status']       = true;
        $result['change_approval']   = _get('comment', 'comment_id', $id, 'approved');
    }
}

echo json_encode($result);
