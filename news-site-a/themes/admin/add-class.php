<?php 
header('Content-Type: text/html; charset=utf-8');
mb_internal_encoding('utf-8');
admin_page_include('includes/header.php');
$tree_operation = array('category'=>true,'tag'=>false,'report'=>false);
?>
        <div>
            <?php
            

                if(!isset($_GET['type']) && empty($_GET['type'])){
                    redirect(getAdminPath());
                }else {
                    $type = $_GET['type'];
                }
                $init = "<option value=\"0\">(no parent)</option>";
                
                $class =  get_class_options($type, $init);
                $tree_opr = false;
                if(array_key_exists($type, $tree_operation)){
                    $tree_opr = true;
                    $class->set_is_tree($tree_operation[$type]);
                }else {
                    $tree_opr = false;
                    $class->set_is_tree(false);
                }
            ?>
            <form action="" id="form" method="post">
                <h2 class="main-body-page-header">Add a <?php echo $type?></h2>
                <div id="message" style="display: none"></div>
                Type a <?php echo $type?>:<br>
                <input type="text" name="name" id="name" style="width: 100%;"><br><br>
                <?php if($tree_opr):?>
                Parent:<br>                
                <select name="parent" required id="parent" style="width: 100%;">
                    <?php echo ($class->get_total()) ? $class->show_data():$init; ?>
                </select><br><br>
                <?php endif; ?>
                <input type="submit" value="Add <?php echo $type?>" >
            </form>   
        </div>
        <a href='./class?type=<?php echo $type?>'><h2>Show <?php echo $type?> List</h2></a>

        <script>
            function showMessage(message){                
                $("#message").html(message);
                $("#message").css("display","block");
            }
            
            var type = "<?php echo $type?>";
            var is_parent = "<?php echo $type == "tag" ? "false":"true"; ?>";; 
            var aj_path = "<?php echo admin_url(). "/ajax/"?>";

            var cname = $("#name");
            var parent = $("#parent");
            var form = $a("#form", "json");
            
            console.log(aj_path);
            
            form.setUrl(aj_path +"add_class.php");
            
            form.setValues(function(){
                showMessage("Adding...");
                this.values = {
                    name:cname.val(),
                    parent_id:parent.val(),
                    type:type,
                    is_parent:is_parent
                }
            });
            form.validation(function(){
                if(this.values.name.length == 0){
                    showMessage("You did'nt type anything!");
                    return false;
                }
                return true;
            });
            form.setEvent("submit");
            form.success(function(r){
                if(type != 'tag'){
                    if(r.status){
                        var p_id = r.parent_id;
                        var parent = $a("#parent", 'load');
                        parent.setValues(function(){
                            this.values = {
                                active_id : p_id
                            }
                        });
                        parent.success(function(){                

                        });
                        parent.setUrl(aj_path +"get_class.php");
                        parent.run();   
                    }
                }   
                showMessage(r.message);
                
            });
            form.run();
        </script>

<?php admin_page_include('includes/footer.php')?>