<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Home Book Library</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="<?php echo admin_url()."/js/jquery-1.10.2.js"?>" type="text/javascript"></script>
        <script src="<?php echo admin_url()."/js/ajax-functions.js"?>" type="text/javascript"></script>
    </head>
    <body>
        
        <div>
            <?php
                echo make_unique_class("Geography-tag",'tag', 0, 0);
            ?>
            <form action="" id="form" method="post">
                <div id="message" style="display: none"></div>
                Type a Category:<br>
                <input type="text" name="name" id="name"><br><br>
                Parent:<br>
                <select name="parent" required id="parent">
                    <?php echo get_class_options("category"); ?>
                </select><br><br>
                <input type="submit" value="Send" >
            </form>   
        </div>

        <script>
            function showMessage(message){                
                $("#message").html(message);
                $("#message").css("display","block");
            }
            
            var aj_path = "<?php echo admin_url(). "/ajax/"?>";

            var cname = $("#name");
            var parent = $("#parent");
            var form = $a("#form", "json");
            form.setUrl(aj_path +"add_class.php");
            form.setValues(function(){
                this.values = {
                    name:cname.val(),
                    parent_id:parent.val(),
                    type:"category",
                    is_parent:true
                }
            });
            form.validation(function(){
                if(this.values.name.length == 0){
                    showMessage("You did'nt type anything!");
                    return false;
                }
                return true;
            });
            form.setEvent("submit");
            form.success(function(r){
                console.log(r.message);   
                if(r.status){
                    var p_id = r.parent_id;
                    
                    var parent = $a("#parent", 'load');
                    parent.setValues(function(){
                        this.values = {
                            active_id : p_id
                        }
                    });
                    parent.setUrl(aj_path +"get_class.php");
                    parent.run();
                    showMessage(r.message);
                }else {
                    showMessage(r.message);
                }
            });
            form.run();
        </script>
    </body>
</html>
