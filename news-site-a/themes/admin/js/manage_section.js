/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var container = ".main-container";
        var dialogue_box_clossinng_idetifier;
        
        $(".save-overall").on("click", function(e){
            e.preventDefault();
            save_overall(process_of_section);
        });
        
        $(".save-overall-right").on("click", function(e){
            e.preventDefault();
            alert("ok");
            save_overall(process_of_right_section);
        });
        
        function process_of_right_section(){
            var section = {},
                c_ids = $(".data-row #class_id"),
                view = $(".data-row #view"),
                h_color = $(".data-row #full-popover"),
                cl_color = $(".data-row #cl_color"),
                section_data = {}

            for( var i = 0; i < c_ids.length; i++){
                section_data["d"+(i+1)] = {"class_id":c_ids.eq(i).val(), "view":view.eq(i).val(), "h_color": h_color.eq(i).val(), "cl_color":cl_color.eq(i).val() };
            }
            section['data'] = section_data;
            section['ref'] = $("#reference_key").val();
            
            cons(section);
            return section;
            alert("ok");
        }
        
        function process_of_section(){
            var section = {};
            show_status("Saving...");
            var section_data = {};
            var rows = $(".data-row .data");
            var ref_key = $("#reference_key").val();
            section['ref'] = ref_key;
            for( var i = 0; i < rows.length; i++){
                var row = "r"+(i+1);
                var data_item = {};
                var data = rows.eq(i).find(".data-item");
                for(var k = 0; k < data.length; k++){
                    var dn = "d"+(k+1);
                    var id = data.eq(k).find("#category-id").val();
                    var cn = data.eq(k).find("#number-of-column").val();
                    var hbc = data.eq(k).find("#heading_color").val();
                    var hc = data.eq(k).find("#head_color").val();
                    
                    data_item[dn] = {'class_id':id, 'column_size':cn, 'head_bg_color':hbc, 'head_color':hc};
                }
                section_data[row] = data_item;
            }
            section['data'] = section_data;
            return section;
        }
        
        function save_overall(process){
            if(typeof process === "function"){
                var input = process();
            }
            
            //return false;
            $.ajax({
                dataType: "json",
                url: aj_path + "add_section_data.php",
                data: {'data':input.data, 'reference_key':input.ref},
                success: data_success,
                error: data_error
            });
        }
        
        function data_success(res){
            if(res.status){
                show_status(res.message);
                //window.location = window.location;
            }
        }

        function data_error(e, a){
            console.log(a);
        }
        
        function dialogue_box_message(message){
            $(".dialogue-box-message").html(message);
        }
       
        function __add_row(){
            var row_identifier = guidGenerator();
            var row_html = "";
                row_html += '<div class="data-row '+row_identifier+' " >';
                    row_html += '<div class="data">';
                        
                    row_html += '</div>';
                    row_html += '<a href="javascript:__add_data(\''+row_identifier+'\')" class="add-data">+Add Section</a>';
                row_html += '</div>';
            var container = $(".main-container");
            container.append(row_html);
            return false;
        }
        
        function set_section_data(identifier, number_of_column, config, action, parent_class){
            var class_id = config.class_id;            
            var this_identifier = guidGenerator();
            var data_class = (number_of_column == 1) ? "one-col":"two-col"; 
            var col_label = (number_of_column == 1) ? "One Column":"Two Column";
            var head_bg_color = config.heading_bg_color;
            var head_color = config.heading_color;
            if(action == "edit"){
                data_class = config.data_class;
            }
            
            var html = '';
            
            var p_class = (action == "edit") ? parent_class : config.parent_class;
            var ident = (action == "edit") ? identifier : this_identifier;
            
            html += (action == "add") ? '<div class="data-item '+this_identifier+' '+data_class+'">':"";
            
                html += '<div class="data-item-in">';
                    html += '<input type="hidden" value="'+number_of_column+'" name="number-of-column" id="number-of-column">';
                    html += '<input type="hidden" value="' + p_class + '" name="parent_class" id="parent_class">';
                    html += '<input type="hidden" value="'+head_color+'" name="head_color" id="head_color" />';
                    html += '<div class="head" style="color:'+head_color+'; background-color: '+head_bg_color+'  " >';
                        html += config.name;
                        html += '<a href="javascript:_delete_data(\'' + p_class + '\', \''+ident+'\')">X</a>';
                        html += '<input type="hidden" value="'+class_id+'" name="category-id" id="category-id">';
                    html += '</div>';
                        html += '<div class="column-type">';
                            html += '<strong>Column Type &raquo;</strong> '+col_label;
                        html += '</div>';
                        html += '<div class="heading-bg-color">';
                            html += 'Heading Color &raquo; <input type="text" style="width:80px; float:none; display:inline"  class="form-control" id="heading_color" value="'+head_bg_color+'" data-color-format="hex">'
                        html += '</div>';
                    html += '<button class="edit-section" onclick="return __edit_data(\''+ident+'\', '+class_id+', '+number_of_column+', \'' + config.parent_class+'\')" >Change</button>';
                html += '</div>';
            html += (action == "add") ? '</div>': "";
            
            if(action === "add"){
                $("."+identifier+" .data").append(html);
                if(calculate_num_of_col(identifier) == 3){
                    $("."+identifier+" .add-data").addClass("add-data-inactive-button");
                }else {
                    $("."+identifier+" .add-data").removeClass("add-data-inactive-button");
                }
            }else if(action === "edit") {
                $("."+ident).html(html);
                if(number_of_column == 1){
                    if($("."+ident).hasClass('two-col')){
                        $("."+ident).removeClass('two-col');
                        $("."+ident).addClass('one-col');
                    }
                }else {
                    if($("."+ident).hasClass('one-col')){
                        $("."+ident).removeClass('one-col');
                        $("."+ident).addClass('two-col');
                    }
                }
            }
            var cl_color;
            color_picker( $("."+ident).find("#heading_color"), function(c, color){                
                var head_bg_color = $("."+ident).find("#heading_color").val();
                
                $("."+ident).find(".head").css("background-color", head_bg_color);
                
                
                if (color.cielch.l < 60) {
                    $("."+ident).find(".head").css("color", "#F1F1F1");
                    $("."+ident).find("#head_color").val("#F1F1F1");
                }else {
                    $("."+ident).find(".head").css("color", "#333333");
                    $("."+ident).find("#head_color").val("#333333");
                }
                
            });
        }

        function __add_data(id){
            var h_color = "#FF00FF";
            var cat_list = guidGenerator();
            var html = '';
            html += '<div style="text-align:center; width:100%; margin:50px 0;">';
            html += '<div class="dialogue-box-message"></div>';
            html += 'Column Size: <select name="column_size" id="column_size">';
                html += '<option value="1">One column</option>';
                if(calculate_num_of_col(id) < 2){
                    html += '<option value="2">Two Column</option>';
                }
            html += '</select> ';
            html += 'Select A Category: <select class="' + cat_list + ' category" name="category_id" id="category_id">';
                html += '<option value="a">(Select)</option>';
                html += '';
            html += '</select><br><br>';
            html += 'Heading Color: <input type="text" style="width:80px; float:none; display:inline"  class="form-control" id="h_color_in_db" value="'+h_color+'" data-color-format="hex">';
            html += '<br><br><input type="submit" value="Insert Into Row" class="data-insert-button" >';
            html += '</div>';
            if(calculate_num_of_col(id) < 3){
                dialogue_box("Add a Section", html);
                var cl_color;
                color_picker( $("#h_color_in_db"), function(c, color){    
                    if (color.cielch.l < 60) {
                        cl_color = "#F1F1F1";
                    }else {
                        cl_color = "#333333";
                    }
                });
                
                load_category_in("."+cat_list, 12);
                $(".data-insert-button").on("click", function(){
                    var column_size =$("#column_size").val();
                    var class_id = $("#category_id").val();
                    var head_color = $("#h_color_in_db").val();
                    var class_name = $("#category_id option[value='"+class_id+"']").html();
                    
                    var config = {
                        "class_id" : class_id, 
                        "name" : class_name,
                        "parent_class": id,
                        "heading_bg_color" : head_color,
                        "heading_color" : cl_color
                    };

                    if(column_size.length == 0 || column_size == 0){
                        dialogue_box_message("Select Column Size!");
                    }else if(class_id.length == 0 || class_id == 0){
                        dialogue_box_message("Select a category!");
                    } else {
                        set_section_data(id, column_size, config, 'add');
                        $(".data-row .data").sortable({ tolerance:"pointer", cursor:"move", 
                            revert:true, opacity: 0.8,
                            axis: "x", 
                            handle: '.head'
                        });
                        close_dialogue_box(dialogue_box_clossinng_idetifier);
                    }
                });
                show_status("");
            }else {
                show_status("<span style='color:#900'>Row is full! delete some data before adding.</span>", "fade");
            }
        }
        
        function __edit_data(identifier, active_class_id, number_of_column, parent_class ){
            var cat_list = guidGenerator();
            var heading_color = $("." + identifier).find("#heading_color").val();
            console.log(heading_color);
            
            var html = '';
            html += '<div style="text-align:center; width:100%; margin:50px 0;">';
            html += '<div class="dialogue-box-message"></div>';
            
            html += 'Column Size: <select  name="column_size" id="column_size">';
                if(calculate_num_of_col(parent_class) > 2){
                    if((number_of_column == 1)){
                        html += '<option value="1" '
                        html +=  (number_of_column == 1) ? 'SELECTED':'';
                        html += '>One column</option>';
                    }
                    if((number_of_column == 2)){
                        html += '<option value="1" ';
                        html +=  (number_of_column == 1) ? 'SELECTED':'';
                        html += '>One Column</option>';
                        html += '<option value="2" ';
                        html +=  (number_of_column == 2) ? 'SELECTED':'';
                        html += '>Two Column</option>';
                    }
                }else {
                    html += '<option value="1" ';
                    html +=  (number_of_column == 1) ? 'SELECTED':'';
                    html += '>One Column</option>';
                    html += '<option value="2" ';
                    html +=  (number_of_column == 2) ? 'SELECTED':'';
                    html += '>Two Column</option>';                    
                }
            html += '</select> ';
            html += 'Select A Category: <select class="'+cat_list+'" category" name="category_id" id="category_id">';
                html += '<option value="a">(Select)</option>';
                html += '';
            html += '</select><br><br>';
            html += 'Heading Color: <input type="text" style="width:80px; float:none; display:inline"  class="form-control" id="edit_heading_color" value="'+heading_color+'" data-color-format="hex">';
            html += '<br><br><input type="submit" value="Change" style="width: 30%" class="data-change-button">';
            html += '</div>';
            
            dialogue_box("Change Section Data", html);
            
            var cl_color;
            color_picker( $("#edit_heading_color"), function(c, color){    
                if (color.cielch.l < 60) {
                    cl_color = "#F1F1F1";
                }else {
                    cl_color = "#333333";
                }
            });
            load_category_in("."+cat_list, active_class_id);
                
            $(".data-change-button").on("click", function(){
                var column_size =$("#column_size").val();
                var class_id = $("#category_id").val();
                var head_color = $("#edit_heading_color").val();
                var class_name = $("#category_id option[value='"+class_id+"']").html();
                var data_class = (number_of_column == 1) ? "one-col":"two-col";
                
                var config = {
                    "class_id" : class_id, 
                    "name" : class_name,
                    "data_class" : data_class,
                    "parent_class" : parent_class,
                    "heading_bg_color" : head_color,
                    "heading_color" : cl_color
                };
                if(column_size.length == 0 || column_size == 0){
                    dialogue_box_message("Select Column Size!");
                }else if(class_id.length == 0 || class_id == 0){
                    dialogue_box_message("Select a category!");
                } else {
                    set_section_data(identifier, column_size, config , 'edit', parent_class);
                    var p_class = $("."+identifier).find("#parent_class").val();
                    //alert(calculate_num_of_col(parent_class));
                    if(calculate_num_of_col(parent_class) >= 3){
                        $("."+parent_class+" .add-data").addClass("add-data-inactive-button");
                    }else {
                        $("."+parent_class+" .add-data").removeClass("add-data-inactive-button");  
                        show_status("");
                    }
                    close_dialogue_box(dialogue_box_clossinng_idetifier);
                }
            });
            return false;
        }
        
        
        function __add_right_section(type, ident){
            var type = (typeof type == "undefined") ? "add":type;
            var ident = (typeof ident == "undefined") ? false:ident;
            var selectedView = "", 
                selectedId= "",
                button_name = "Add Section",
                h_color = "#00aeed";

            if(type == "edit"){
                selectedView = $("div." +ident).find("input#view").val();                
                selectedId = $("div." +ident).find("input#class_id").val();
                h_color = $("div." +ident).find("input#full-popover").val();
                button_name = "Change Section";
            }
            
            var  views = {'icon':"Icon", 'list' :"List", 'un':"University", 'col':"College"};
            
            var cat_list = guidGenerator();
            var view = guidGenerator();
            var html = '';
            html += '<div style="text-align:center; width:100%; margin:50px 0;">';
            html += '<div class="dialogue-box-message"></div>';
            html += 'Select A Category: <select class="' + cat_list + ' category" name="category_id" id="category_id">';
                html += '<option value="a">(Select)</option>';
                html += '';
            html += '</select><br><br>';
            html += 'Select A Category: <select class="view-list" name="view-list" id="view-list">';
                for (var key in views){
                    var sel = (selectedView == key) ? "selected":"";
                    html += '<option value="'+key+'" ' + sel + '>'+views[key]+'</option>';
                }                
            html += '</select><br><br>';
            html += 'Heading Color: <input type="text" style="width:80px; float:none; display:inline"  class="form-control" id="h_color_in_db" value="'+h_color+'" data-color-format="hex">';
            html += '<br><br>';
            html += '<input type="submit" value="' + button_name + '" class="data-change-button" >';
            html += '</div>';
            dialogue_box("Add a Section", html);
            load_category_in("."+cat_list, selectedId);
            
            var cl_color;
            color_picker( $("#h_color_in_db"), function(c, color){    
                if (color.cielch.l < 60) {
                    cl_color = "#F1F1F1";
                }else {
                    cl_color = "#333333";
                }
            });

            $(".data-change-button").on("click", function(){
                var class_id = $("#category_id").val();
                var icon = $("#view-list").val();
                var h_color = $("#h_color_in_db").val();
                var class_name = $("#category_id option[value='"+class_id+"']").html();
                var icon_name = $("#view-list option[value='"+icon+"']").html();
                
                var config = {
                    "ident":ident,
                    "class_id": class_id, 
                    "name":class_name, 
                    "parent_class":class_id,
                    "view":icon,
                    "view_name":icon_name,
                    "h_color":h_color,
                    "cl_color":cl_color
                };

                if(class_id.length == 0 || class_id == 0){
                    dialogue_box_message("Select a category!");
                } else {
                    if(set_right_section_data(class_id, config, type)){
                        close_dialogue_box(dialogue_box_clossinng_idetifier);
                    }
                }
            });
        }
        
        function color_picker( input_name, callback ){
            input_name.ColorPickerSliders({
                placement : 'auto',
                hsvpanel : true,
                previewformat : 'hex',
                order: {
                    rgb : 1
                  },
                swatches : ["#00AEED","red", "green", "blue","yellow", "violet", "skyblue", "orange"],
                customswatches : false,
                onchange :callback
            });
            
            /*
            if(typeof callback == "function"){
                input_name.ColorPickerSliders({
                    onchange:callback
                });
            }*/
        }
        
        function set_right_section_data(class_id, config, type){
            var name = config.name;
            var view = config.view;
            var view_name = config.view_name;
            var h_color = config.h_color;
            var cl_color = config.cl_color;
            var data_id = guidGenerator();
            
            if(type == "edit"){
                cons(config.ident);
                var section = $("."+config.ident);
                section.find("input#class_id").val(class_id);
                section.find("input#view").val(view);
                section.find(".data-body .color-picker #full-popover").val(h_color);
                section.find("input#cl_color").val(cl_color);
                
                section.find(".data-head .cl_name").html(name);
                section.find(".data-body .v_name").html(view_name);
                section.find(".data-head, .data-body .color-picker #full-popover").css("background-color", h_color);
                section.find(".cl_name").css("color", cl_color);
                return true;
            }else{
                var html = "";
                html += "<div class='data-row "+data_id+"'>";
                    html += "<input type='hidden' value='"+class_id+"' id='class_id'>";
                    html += "<input type='hidden' value='"+view+"' id='view'>";
                    html += "<input  type='hidden' value='"+cl_color+"' id='cl_color' >";
                    html += "<div class='data-head'  style='background-color: " + h_color + "'>";
                        html += "<span class='cl_name' style='color: "+cl_color+" ' id='cl_name'>" +name+"</span>";
                    html += "</div>";
                    html += "<div class='data-body'>";
                        html += "<div class='view-type'>View: <span class='v_name'>"+view_name+"</span></div>";
                        html += '<div class="color-picker">';
                            html += 'Heading Color: <input type="text" class="form-control" id="full-popover" value="'+h_color+'" data-color-format="hex">';
                        html += '</div>';
                        html += "<div class='edit-button'>";
                            html += "<a href='javascript:'  id='" + data_id +"'>Edit Section</a>";
                        html += "</div>";
                        html += "<div class='remove-button'>";
                            html += "<a href='javascript:' id='" + data_id +"'>Remove</a>";
                        html += "</div>";
                    html += "</div>";
                html += "</div>";
                
                $(".main-container").append(html);
                
                color_picker($("."+data_id).find("#full-popover"), function(c, color){
                    if (color.cielch.l < 60) {
                        $("." + data_id).find(".cl_name").css("color", "#F1F1F1");
                        $("." + data_id).find("#cl_color").val("#F1F1F1");
                    }else {
                        $("." + data_id).find(".cl_name").css("color", "#333333");
                        $("." + data_id).find("#cl_color").val("#333333");
                    }
                    var color  = $("."+data_id).find("input#full-popover").val();
                    $("."+data_id).find(".data-head").css("background-color", color);
                    cons(color);
                });
                
                _modify_right_section(data_id, true);
                
                return true;
            }
            return false;
        }
        
        function _modify_right_section(data_id, remove_confirmation){
            $("." + data_id + " .data-body .edit-button a").each(function(){
                $(this).on("click", function(){
                    __add_right_section("edit",  $(this).attr("id") );
                })
            });

            $("." + data_id + " .data-body .remove-button a").each(function(){
                $(this).on("click", function(){
                    if(remove_confirmation === true){
                        var con = window.confirm("Are you sure to romove this section");
                        if(con) $("."+$(this).attr("id")).remove();
                    }else {
                        $("."+$(this).attr("id")).remove();
                    }
                });
            });

        }
        
        function _delete_data(parent, id){
            var al = confirm("Are You Sure Want To delete the data section!");
            if(al){
                $("."+parent).find("."+id).remove();
                
                if(calculate_num_of_col(parent) < 3){
                    $("."+parent+" .add-data").removeClass("add-data-inactive-button");
                }
            }
        }
        
        function calculate_num_of_col(identifier){
            var num = 0;
            var data = $("."+identifier).find(".data-item");
            
            for(k =0; k < data.length; k++){
                num += parseInt(data.eq(k).find("#number-of-column").val());
            }
            return num;
        }
        
/*  

"d1":{"class_id":"40"}, 
"d2":{"class_id":"41"}
}

*/