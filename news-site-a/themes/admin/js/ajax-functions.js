var ajax = function(elem, type){
    this.element = elem;
    this.type = type;
    this.url = "";
    this.event = null;
    this.succ  =  null;
    this.setVals = function(){};
    this.validate = function(){};
    this.values = {};
    this.setEvent = function(event){
        this.event = typeof(event) !== undefined ? event:null;
        return this;
    };
    this.success = function(func){
        this.succ = typeof(func) !== undefined ? func:null;
        return this;
    };
    this.validation = function(v){
        this.validate = v;
    }
    this.setValues = function(vals){
        this.setVals = vals;
    };
    this.setUrl = function(url){
        this.url = url;
        return this;
    };
    var self = this;
    
    
    this.run = function(){ 
        switch(this.type){
            case "get":
                if(!this.event) this._get();
                else $(this.element).on(this.event, this._get);
                break;
            case "post" : 
                if(!this.event) this._post();
                else $(this.element).on(this.event, this._post);
                break;
            case "json" : 
                
                if(!this.event) this._get_json();
                else $(this.element).on(this.event, this._get_json);        
                break;
            case "load" :
                if(!this.event)  this._load(); 
                else $(this.element).on(this.event, this._load);        
        }
    };
    
    this._get_json = function(){
        
        if (typeof self.values !== undefined) self.setVals();
        if (typeof self.validate !== undefined)  var v = self.validate();
        if(v){
            if(typeof(self.succ) !== null) {
                $.getJSON(self.url, self.values, self.succ); 
            }else {
                $.getJSON(self.url, self.values); 
            }
        }
        return false;
    };
    this._get = function(){ 
        if(typeof(self.succ) !== null) {
            $.get(self.url, self.values, self.succ); 
        }else{
            $.get(self.url, self.values);
        }
        return false;
    },
    this._post = function(){       
        if(typeof(self.succ) !== null) {
            $.post(self.url, self.values, self.succ); 
        }else {

            $.post(self.url, self.values);
        }
        return false;
    };
    this._load = function(){
        if (typeof self.values !== undefined) self.setVals();
        if(this.values.length == 0){
            if(typeof(self.succ) !== null){
                $(self.element).load(self.url, self.succ);
            }else {
                $(self.element).load(self.url);
            }
        }else {
            if(typeof(self.succ) !== null){
                $(self.element).load(self.url, self.values, self.succ);
            }else {
                $(self.element).load(self.url, self.values);
            }
        }
        return false;
    }

}

function $a(elem, type){
    return new ajax(elem, type);
}