
function ajax(element, event, type, config, success){
    
    this.element = element;
    this.success = success;
    this.setConfig = config;
    this.values = {};
    this.url = "";
    
    var self = this;

    var init = function(){
        if(typeof(this.setConfig) !== undefined) self.setConfig();
    }

    var _get_json = function(){
        init();            
        if(typeof(this.success) !== undefined) {
            $.getJSON(self.url, self.values, self.success); 
        }
        return false;
    };
    
    var _get = function(){
        init();            
        if(typeof(this.success) !== undefined) {
            $.get(self.url, self.values, self.success); 
        }else{
            $.get(self.url, self.values);
        }
        return false;
    };
    var _post = function(){
        init();            
        if(typeof(this.success) !== undefined) {
            $.post(self.url, self.values, self.success); 
        }else {
            $.post(self.url, self.values);
        }
        return false;
    };
    var _load = function(){
        if(this.values.length == 0){
            $(self.element).load(self.url);
        }else {
            $(self.element).load(self.url, this.values);
        }
    };

    switch(type){
        case "get":
            $(this.element).on(event, _get);
            break;
        case "post" : 
            $(this.element).on(event, _post);
            break;
        case "json" :
            $(this.element).on(event, _get_json);        
            break;
        case "load" :
            _load();
    }
}
