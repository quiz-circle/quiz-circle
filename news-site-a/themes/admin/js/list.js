/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function _change_news_visibility(id, vis, ref){
    vis = parseInt(vis);

    var val = {'vis':vis, 'id':id};

    $.getJSON(admin_path+"/ajax/change_visibility.php", val, function(e,s ){

        if(e.status){
            //console.log(e.change_vis);
            if(e.change_vis == 0) {
                $("."+ref).removeClass('visible');
                $("."+ref).addClass('invisible-n');
                $("."+ref).html("Invisible");
            }else {
                $("."+ref).removeClass('invisible-n');
                $("."+ref).html("Visible");
                $("."+ref).addClass('visible');
            }
        }
    });
}