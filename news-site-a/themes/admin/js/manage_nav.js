/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    function status(html, image_link){
        
        var ref = $(".operation-status");
        if(image_link !== undefined){
            console.log(admin_path + "/images/"+image_link);
            ref.html("");
            ref.append("<img alt='"+html+"' src='" + admin_path + "/images/"+image_link+"'>");
        }else {
            ref.html(html);
        }
    }
    
    
    status("");
    
    var gc = 0;
    var listed = [];
    var listed2 = [];
    
    function make_sortable(){
        $(".sort").sortable({ 
            //tolerance:"pointer", 
            cursor:"move", 
//            /items:  '> li',
            revert:true, opacity: 0.8,
            scroll : true,
            connectWith: ".sort",
            handle: ".container .title",
            update : function(event,ui){ 
                status("<span style='color:#a71d54;'>Click save to change list order</span>");
                listed = [];
                listed2 = [];
                temp = {};
                gc = 0;
            }
        });
    }
    make_sortable();
    
    var temp = {};
    var x = {};
    
    function tree_list(li_q, le, prev){
        if(le == undefined) le = -1 ;
        le++;
        var c = 0;
        for(i=0; i<= (li_q.length-1); i++){
            gc++;
            var Nextid = $(li_q[i]).prop("id");
            var name = $(li_q[i]).find(".container .name").html();
            var item_id = $(li_q[i]).find(".container input#item_id").val();
            var elem = $("#"+Nextid).find("ul li");
            var ceLength = parseInt(elem.length);
            
            if($.inArray( Nextid , listed) > -1){
                continue;
            }else {
                listed.push(Nextid);    
                /*
                console.log("GC: "+gc+", Level: " + le + ", Index:" + i +" "+
                    $(li_q[i]).find(".container").html() +
                    " , NextLiLength: " + ceLength
                );
                */
                var pass;
                var values = {name: name, item_id:item_id, child:{}};

                if(le == 0){  
                    temp[c] = values;
                    pass = temp[c];                        
                }else if(le > 0) {
                    prev.child[c] = values;
                    pass = prev.child[c];
                }
                //listed2.push( $(li_q[c]).find(".container").html() );
                c++;
            }
            
            if(le > 0) {
                //temp = prev[i];
                //x[i] = prev[i];
            }
            
            if(gc > 500) return false;        
            if (le > 1000){ console.log(le); return false;}   
            if(ceLength > 0){
                //console.log("Level: "+le);
                //tree_list($("#"+Nextid+" ul li"), le, pass);
                tree_list(elem, le, pass);
            }
        }
        //x = prev;
    }
    
    $("#lists").on("click", function(){
        var list = $("#nav-list li");
        //console.log(list)
        tree_list(list);
        status("Saving...", "loading-small.gif");
        //console.log(temp);  
        
        $.ajax({
            url: admin_path + "/ajax/save-menu.php",
            type:"post",
            data: {list:temp},
            success: function(result){
                console.log(result);
                status("Done!");
            },
            error: function(){
                status("Internal Error!");
                console.log("Something Went wrong!");
            }
        })
    });
    
    
    
    
    //$(".containers .content-container").html("");
    
    $(".add-item-nav .fetch-content li").on("click load", "a",function(e){
        //alert(e.type);
        e.preventDefault();
        var containerName = e.target.className;
        var contREF = "."+containerName;
        var imageContainer = $(contREF).find(".containers .loading-overall");
        imageContainer.fadeIn(150);
        var range = e.target.id;

        $(contREF).find("ul.fetch-content li").removeClass("active");
        $(this).parent("li").removeClass("active");
        $(this).parent("li").addClass("active");
        $(contREF).find(".containers .content-container").css("display", "none");        
        var reference = $(contREF).find(".containers ." + range); 
        
        var fetchFrom = containerName;
        
        load_in(reference, {
            "reference": contREF,
            "range": range,
        }, function(r){
            reference.css("display", "block");        
            imageContainer.fadeOut(130);
        });
    });
    
    function on_page_load(containerName, range){
        
        var contREF = "."+containerName;
        var reference = $(contREF).find(".containers ." + range); 
        var imageContainer = $(contREF).find(".containers .loading-overall");
        load_in(reference, {
            "reference": contREF,
            "range": range,
        }, function(r){
            reference.css("display", "block");
            imageContainer.fadeOut(130);
        });
    }
    
    on_page_load("categories", "recent");
    on_page_load("pages", "recent");
    

setTimeout(function(){
    $(".categories .loading-overall").fadeOut(50);
}, 2000);


$(".containers .content-container .fetched-content li").each(function(i){
    $(this).on("click", function(e){
        //e.preventDefault();
        var checked = $(this).find("input.select-content").prop('checked'); 
        console.log(checked);
        if(checked === true){
            $(this).addClass("active");
        }else {
            $(this).removeClass("active");
        }
        
    });
})

$(".add-item-body .add-to-buton .status").css("display", "none");

$(".add-to-buton input[type='submit']").on("click", function(e){
    e.preventDefault();
    var reference = $(this).parent().prop("id"),
        oprContainer = $("#"+reference),
        container = $("."+reference),
        selectedIds = {},
        status = {};
        status['visible'] = container.find(".add-to-buton div.status");
        status['img'] = container.find(".add-to-buton div.status img");
        status['txt'] = container.find(".add-to-buton div.status span");
    
    switch(reference){
        case "custom":
            add_to_item(reference, status);
            break;
        default :
            selectedIds = get_selected_ids(container.find(".containers .content-container input.select-content"));  
            add_to_item(reference, status, selectedIds);
            
    }

});

function add_to_item(reference, status, ids){
    var nav_ref = $("._nav_ref");
    status.visible.css("display", "inline-block");
    status.txt.html("Adding... ");
    var data = {};
    data['ids'] = (typeof ids != "undefined") ?  ids:{};
    data['reference'] = reference;
    data['values'] = {};
    data['nav_reference'] = nav_ref.prop("id");
    data['nav_reference_value'] = nav_ref.val();
    
    //console.log(reference);
    
    if(reference == "custom"){
        data['values']['title'] = $("."+reference).find(".content-container #custom-item-name").val();
        data['values']['url'] = $("."+reference).find(".content-container #custom-item-url").val();

    }

    $.ajax({
        'url':aj_path +"add_menu_item.php",
        'type':'post',
        'data': data,
        success:function(r){
            $(".main-data ul#nav-list").append(r);
            make_sortable();
            enable_edit_content();
            console.log(r);
            status.img.css("display", "none");
            status.txt.html("<em style='color:darkgreen'>Success</em>");
        }
    });
    
}
function enable_edit_content(){
    $(".sort .container .edit-button").on("click", "a", function(e){
        e.preventDefault();

        var editID = $(this).parent().attr("id");
        var editingContainer = $(".sort .container").find("."+editID);

        if(editingContainer.css("display") == "none"){
            editingContainer.slideDown(60, function(){
                $(".sort .container #" +  editID+" a").html("Cancel");
            });
            $(".sort .container #" +  editID).css("display", "block");
        }else {
            $(".sort .container #" +  editID).css("display", "none");
            $(".sort .container #" +  editID).removeAttr("style");
            editingContainer.slideUp(60, function(){            
                $(".sort .container #" +  editID+" a").html("Edit");
            });
        }
        console.log(editingContainer.css("display"));
    });
    
    $(".sort .container .delete-item").each(function(){
        $(this).on("click", function(e){
            e.preventDefault();
            var item_id = $("div#" + e.target.id).find("input#item_id").val();
            var m = this;
            $.ajax({
                data: {item_id: item_id},
                type : "post",
                url:aj_path +"delete_menu_item.php",
                success: function(r){
                    if(r == "true"){
                        $(m).parents("li").remove();
                        status("Item Deleted!");
                        console.log(r);
                    }else {
                        status(r);
                    }
                }
            });
            
        });
    });
    
}

enable_edit_content();

function get_selected_ids(inputs){
    var ids = {};
    var ind = 0;
    inputs.each(function(i){
        var each =  $($(this)[0]);
        if(each.prop("checked")){
            ids[ind]= each.val();
            ind++;
        }
    })
    return ids;
}