var aj_path = admin_path + "/ajax/";

function showMainMessage(message, color){
    color = (typeof color === "undefined") ? "green":color;
    console.log(color);
    $(".main-message").slideDown(150);
    $(".main-message").css("border", "1px solid "+color);
    $(".main-message .mm-body").css("color", color);
    $(".main-message .mm-body").html(message);
    $(".mm-close-button").on("click", function(){
        $(".main-message").slideUp(130);
    })
}

function open_form(name){
        fid = $(name);
        status = fid.css("display");
        console.log(status);
        if(status == "block"){
            fid.slideUp();
        }else {
            fid.slideDown();
        }
    return false;
}

function select_value(selector, val){
    var s = $(selector + " option[value='" + val +"']");
    s.prop("selected",true);
}

function showMessage(message){                
    $("#message").html(message);
    $("#message").css("display","block");
}
var actives = [];

$("#from-cat").on("change", function(){
    var val = parseInt($(this).val());
    actives.push(val);
});

var author_active = [];

function set_active_author_back_up(a_id){
    author_active.push(a_id);
    for(var i = 0; i < author_active.length; i++){
        select_value("#author", author_active[i]);
    }
}
function set_active_author(a_id){
    author_active.push(a_id);
    
    for(var i = 0; i < author_active.length; i++){
        $("input:checkbox[value='"+author_active[i]+"']").prop("checked", true);
    }
}

function change_form_cat(p_id){
    var cats = $a("#from-cat", 'load');
    cats.success(function(){
        actives.push(p_id);
        for(var i =0; i < actives.length; i++){
            select_value("#from-cat", actives[i]);
        }
    });
    cats.setValues(function(){
        this.values = {
            init_html : "",
        }
    });
    cats.setUrl(aj_path +"get_class.php");
    cats.run(); 
}

var ck_editor_config = {
toolbar: [
        {name:document, items:[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', 'Undo', 'Redo' ]},
        ['Image','Table',"CreateDiv",'MediaEmbed','Smiley','Iframe'],
        ['Link','Unlink','Anchor'],
        ["Find","Replace"],
        ['Source','Save','-','Print','Maximize'],'/',
        ["SelectAll",'FontSize','Font'],
        ['Bold', 'Italic','Underline','RemoveFormat'],
        ['TextColor','BGColor'],
        ["NumberedList",
"BulletedList","Outdent","Indent","Blockquote",'-',"JustifyLeft","JustifyCenter","JustifyRight","JustifyBlock"]
    ],
    height: 500,
    extraPlugins: 'imageuploader'
}

        
function show_status(message, type){
    switch(type){
        case "fade":
            $(".status").hide();
            $(".status").html(message);
            $(".status").fadeIn();
            break;
        default:
            $(".status").html(message);
    }
}

function cons(data){
    console.log(data);
}

function guidGenerator() {
    var S4 = function() {
       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}

function load_category_in(identifier, active_id){
    var parent = $a(identifier, 'load');
    parent.setValues(function(){
        this.values = {
            "active_id" : active_id
        }
    });

    parent.success(function(){                

    });
    parent.setUrl(aj_path +"get_class.php");
    parent.run(); 
}

function dialogue_box(header, body, footer){
    var identifier = guidGenerator();
    var html = '<div class="overall '+identifier+'">';
        html += '<div class="overall-center">';
            html += '<div class="overall-in">';
                html += '<div class="header">';
                    html += '<h1>';
                        html += header;
                    html += '</h1>';
                    html += '<button class="close">X</button>';
                html += '</div>';
                html += '<div class="body">';
                    html += body;
                html += '</div>';
            html += '</div>';
        html += '</div>';
    html += '</div>';
    $("body").append(html);

    dialogue_box_clossinng_idetifier = identifier;

    $("."+identifier).on('click', function(e){
        if(e.target.className === 'close' || e.target.className === 'overall '+identifier){
            close_dialogue_box(identifier);
        }
    });
}

function close_dialogue_box(id){
    $("."+id).fadeOut();
    $("."+id).remove();
}

function load_in(identifier, config, callback){
    var parent = $a(identifier, 'load');
    
    parent.setValues(function(){
        this.values =  config;
    });

    if(typeof callback == "function"){
        parent.success(callback);
    }
    
    parent.setUrl(aj_path +"load_content.php");
    parent.run(); 
}

function _load_in(identifier, config, callback){
    var parent = $a(identifier, 'post');
    
    parent.setValues(function(){
        this.values =  config;
    });
    
    if(typeof callback == "function"){
        parent.success(callback);
    }
    
    parent.setUrl(aj_path +"load_content.php");
    parent.run(); 
}