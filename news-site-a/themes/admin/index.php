<?php admin_page_include('includes/header.php');?>
<style>
    .news-title {
        font-weight: bold;
    }
    .comment {
        
    }
</style>
    <?php 
        $date_format = get_date_format();
        load()->sys("Session")->open();
        load()->sys("Data")->open();
        
        $session = new Session;
        $cds = $session->flush('_coment_delete_dialogue_box');
        
        if(GET_DATA('approval_status') && GET_DATA('comment_id') && $cds === true){
            $comment_id = GET_DATA('comment_id');
            $session->add("_cdi_r", Data::encrypt($comment_id));
            $db_html = "";
            $db_html .= "The comment has been approved. It is now shown into website. do realy delete this comment!";
            $db_html .= "<br>";
            $db_html .= "<br>";
            $db_html .= "<a href='".  admin_link()."/delete-comment?comment_id={$comment_id}&force_comment_delete=1' class='yes-btn'>Yes</a> &nbsp;&nbsp;&nbsp;";
            $db_html .= "<a href='javascript:void(0)' class='no-btn'>No</a>";
            dialogue_box("Comment has been approved", $db_html);
        }
        
        load()->lib('CommentList')->open();
        $col = "natural join content";
        
        $comment = new CommentList(0, "", "*", $col);
        
        $comment->set_html("");
        $comment->data_limit(5);
        //$comment->setAttrs(array('class'=> ""));
        
        
        
        function dashboard_news_list($row){
            global $date_format;
            $result = "<tr>";
                $result .= "<td>";
                    $result .= $row['content_title'];
                $result .= "</td>";
                $result .= "<td>";
                    $result .= convert_date($row['added_date'], $date_format);
                $result .= "</td>";
            $result .= "</tr>";
            return $result;
        }
        
        function admin_comment_list($row){
            global $date_format;
            //Explain($row);
            $ref = md5($row['comment_id'] .  microtime().  uniqid());
            $result = '<tr>';
                $result .= '<td>';
                    $result .= '<p class="news-title">';
                        $result .= $row["content_title"];
                    $result .= '</p>';
                    $result .= '<p class="comment">';
                       $result .= make_excerpt($row["comment_body"], 80); 
                    $result .= '</p>';
                $result .= '</td>';
                $result .= '<td>'. convert_date($row['date'], $date_format) .'</td>';
                $result .= '<td>';
                $vis_back_pos = $row['approved'] == 0 ? "-25px":" 0";
                $result .= '<a class="'.$ref.'" href="javascript:" id="visibility-icon" style="background-position-x:'.$vis_back_pos.';"> ';
                $result .= '</a>';
                $result .= '</td>';
                $result .= '<td class="change-approval">';
                $result .= '<input type="checkBox" id="'.$ref.'" value="'.$row['comment_id'].'" '.($row['approved'] ? "checked":"").'>';
                $result .= '</td>';
            $result .= '</tr>';
            return $result;
        }
        
        
        
        $comment->set_elemets_oper("admin_comment_list");
        show_flashed_message("comment-delete-message");
        
        
        $news_list = news_list("dashboard_news_list", array(), 5);
        
    ?>
<script>
    $(".no-btn").on("click", function(){
        close_dialogue_box(dialogue_box_clossinng_idetifier);
    });
</script>
    <div class="col-6"> 
        <h1 class="main-body-page-header">
            Recent News
        </h1>
        <table class="contet-list comment-list">
            <thead>
                <tr>
                    <td>News</td>
                    <td>Date</td>
                </tr>
            </thead>
            <?php if($news_list->get_total()): ?>
                <?php $news_list->show_data("added_date", "DESC"); ?>
            <?php else: ?>
            <tr>
                <td colspan="4">No News Posted</td>
            </tr>
            <?php endif; ?>
        </table>
        <p>
            <a href="<?php echo admin_link()."/list"; ?>">Show All News</a>
        </p>
    </div>
<div class="col-5" style="width: 48.5%; margin-left: 1.5%">  
    <h1 class="main-body-page-header">
        Recent Comments
    </h1>
        <table class="contet-list comment-list">
            <thead>
                <tr>
                    <td>
                        Comment
                    </td>
                    <td>Writer</td>
                    <td colspan="3">Approved</td>
                </tr>
            </thead>
            <?php if($comment->get_total()) { $comment->show_data(array('comment_id'=>'DESC', 'content.content_id'=>'DESC')); } ?>
        </table>
        <p>
            <a href="<?php echo admin_link()."/comments-list"; ?>">Show All Comments</a>
        </p>
    </div>

<script>
    
    $(".comment-list tr td.change-approval input").each(function(e){
        $(this).on("click", function(){
            var id = $(this).val();
            var ref =  $(this).attr('id');
            console.log(ref);
            _change_comment_approved(id, ref);
        });
    });
    
    function _change_comment_approved(id, ref){
        
        var val = {'id':id};
        
        $("."+ref).css({
            "background-image": "url('"+admin_path+"/images/loading-small.gif')",
            "height": "8px",
        });
        $.getJSON("<?php echo admin_url() ?>/ajax/change_comment_approval.php", val, function(e,s ){
    
            if(e.status){
                console.log(e.change_approval);
                if(e.change_approval == 0) {
                    $("."+ref).removeClass('approved');
                    $("."+ref).addClass('not-approved');
                    $("."+ref).css({
                        "background-image": "url('"+admin_path+"/images/visibility.png')",
                        "height": "15px",
                    });
                    $("."+ref).css("background-position-x", "-25px");
                }else {
                    $("."+ref).removeClass('not-approved');
                    $("."+ref).css({
                        "background-image": "url('"+admin_path+"/images/visibility.png')",
                        "height": "15px",
                    });
                    $("."+ref).css("background-position-x", "0");
                    $("."+ref).addClass('approved');
                }
            }
        });
    }
    
</script>
<?php admin_page_include('includes/footer.php');?>