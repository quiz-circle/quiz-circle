<?php


?>
<?php 
header('Content-Type: text/html; charset=utf-8');
mb_internal_encoding('utf-8');
admin_page_include('includes/header.php');
?>
<style>
    #author-list .pagination {
        width: 100%;
        float: left;
    }
    
    #author-list .pagination {
        width: 100%;
        margin-bottom: 10px;
    }
    #author-list .pagination ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
    }
    #author-list .pagination ul li{
        width: 35px;
        height: 30px;
        float: left;
        text-align: center;
        line-height: 30px;
    }
    #author-list .pagination ul li a{
        height: 100%;
        width:  100%;
        float: left;
    }
    #author-list .pagination ul li.active {
        background-color:  #333;
    }
    #author-list .pagination ul li.active a {
        color:  #FFF;
    }
    
</style>
<div id="author-list">
        <?php
        
        
        if(isset($_POST) && !empty($_POST)){
            
            $auth_name = $_POST['auth-name'];
            $nick_name = $_POST['nick-name'];
            
            $author = load()->sys("Content")->run("author", "auth_id", "auth_id");
            $redirect_path = admin_link()."/authors";

            $author->set_redirect_path($redirect_path, "db");
            $author->set_redirect_path($redirect_path, "query");

            $author->set_failed_message("This Author Has one or many book!", "edit_cond");
            $author->set_failed_message("Successfully Edited!", "udpate_success");
            $author->set_failed_message("Already Updated!", "updated");
            $author->set_failed_message("Error in deleteing author!", "edit_db_error");

            $author->set_condition(true);

            $author->update(array("auth_name" => $auth_name, "nick_name" => $nick_name));
        
            echo $author->get_message();
        }
        
        $auth = load()->sys("Content")->run("author", "auth_id", "auth_id");
        
        $a = $auth->read();
        
        $auth_name = $a['auth_name'];
        $nick_name = $a['nick_name'];
        
        ?>
    
        <form action="" method="post">
            <h1>Add a Author</h1>
            Type a name:<br>
            <input type="text" name="auth-name" value="<?php echo $a["auth_name"]; ?>"required style="width: 100%"><br><br>
            Nick Name (optional):<br>
            <input type="text" name="nick-name" value="<?php echo $a["nick_name"]; ?>"  style="width: 100%"><br><br>
            <input type="submit" value="Add" >
        </form>
<?php admin_page_include('includes/footer.php')?>

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    