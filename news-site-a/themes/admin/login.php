<!DOCTYPE html>
<html>
    <head>
        <title>Login &raquo Book Library</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo admin_url()."/css/reset.css"?>" rel="stylesheet">
        <link href="<?php echo admin_url()."/css/style.css"?>" rel="stylesheet">
        <script src="<?php echo admin_url()."/js/jquery-1.10.2.js"?>" type="text/javascript"></script>
        <script src="<?php echo admin_url()."/js/ajax-functions.js"?>" type="text/javascript"></script>
        <style>
            .login-center {
                width:440px;
                margin: 15% auto;
            }

            .login-in {
                width:100%;
                float: left;
            }

            .login{
                width:100%;
                float:left;
                border:1px solid #CCC;
                position: relative;
                padding: 10%;
                border-top: 2px solid darkcyan;
            }

            .login h1 {
                width: 100%;
                text-align: center;
                float: left;
                margin-bottom: 30px;
                color: #444;
                font-size: 25px;
            }

            .login .inputs {
                width: 100%;
                height: 35px;
                margin-bottom: 15px;
                float: left;
            }
            .login .bottom {
                width: 100%;
                height: 35px;
                margin-top: 5px;
                float: left;
                position: relative;
            }


            .login .bottom input[type="submit"]:focus{
                box-shadow: none;
                border: none;
            }

            .login .bottom input[type="checkbox"],
            .login .bottom label {
                position: absolute;
                bottom: 10px;
            }

            .login .bottom input[type="checkbox"]{
                left: 0;
            }

            .login .bottom input[type="checkbox"]{
                bottom: 8px;
            }

            .login .bottom label {
                left: 20px;
                font-size: 14px;
                color: #888;
            }

            .login label {
                cursor: pointer;
            }

            .login .inputs label {
                width: 10%;
                height:  100%;
                float: left;
                border:1px solid #CCC;
                border-right: none;
                text-align: center;
                line-height: 35px;
                font-style: italic;
                background-color: #F1F1F1;
                color: #888;
                font-size: 12px;
            }
            .login .inputs input {
                width: 90%;
                height:  100%;
                padding: 5px 8px;
                border:1px solid #CCC;
                font-size: 14px;
                cursor: pointer;
            }

            .login .inputs input:hover {
                box-shadow: 0 0 4px #BBB;
            }

            .login .bottom .login-submit {
                float: right;
                padding: 8px 20px;
                background-color: darkcyan;
                border: 1px solid red;
                color: #FFF;
                border: none;
                font-size: 14px;
                cursor: pointer;
            }
        </style>
    </head>
    <body>
        
    <?php

    
        $next = isset($_GET["back_to"]) ? $_GET["back_to"]: admin_link();
        login_form_operation($next);
        ?>
        <div class="login-center">
        <?php show_login_form("Login To Control Panel"); ?>
        </div>
    </body>
</html>


































