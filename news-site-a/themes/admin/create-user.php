<?php 
header('Content-Type: text/html; charset=utf-8');
mb_internal_encoding('utf-8');
admin_page_include('includes/header.php');
?>


<style>
    .create-user {
        float: left;
        position: relative;
        display: block;
    }
    
    .create-user-form {
        width: 100%;
    }
    
    .create-user-form .form-inputs,
    .create-user-form .form-inputs label{
        float: left;
    }
    
    .create-user-form .form-inputs{
        width: 100%;
        margin-top: 10px;
    }
    
    .create-user-form .form-inputs label{
        width: 100%;
        margin-bottom: 5px;
    }
    
    .create-user-form .form-inputs input,
    .create-user-form .form-inputs select {
        
    }
    
    .create-user-form .message ul {
        list-style: none;
        float: left;
        width: 100%;
        padding: 0;
        margin: 0;
    }
    
    .create-user-form .message ul li {
        width: 100%;
        float: left;
        margin-bottom: 9px;
        color: brown;
    }
    
</style>

    <?php
    if($user->isAdmin()){
        if(isset($_GET['success']) && !empty($_GET['success'])){    
            echo "<h3>User Successfully created</h3>";
        }else {
            operate_user_form(admin_link()."/create-user/?success=true");
    ?>
    <div class="create-user">
        <?php view_user_form(true); 
    }
    ?>
    </div>
    <?php } else {
        echo  "Sorry, You are not admin you can't Create any user!";
        //redirect(admin_link());
    } 
    ?>
<?php admin_page_include('includes/footer.php'); ?>