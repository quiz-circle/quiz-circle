<?php 
header('Content-Type: text/html; charset=utf-8');
mb_internal_encoding('utf-8');
admin_page_include('includes/header.php');
?>        
        <div>
            <?php

            
                
                function delete_nav_item($id, &$message){
                    $l = DB::run()->read('nav_item')->where('item_id', $id)->run_sql();
                    $li = $l->get_first();
                    $nav_del = DB::run()->delete('nav_item')->where("item_id", $id)->run_sql();
                    
                    
                    if(!$nav_del->error()){
                        $message = "Successfully deleted! ";
                        Explain($li);
                        _reposition_nav($li['parent_id'], $li['class_id']);
                    }else {
                        $message = "Error in deleting item! ".$nav_del->sql_error();                                
                    } 
                }
                
                if(!GET_DATA('id')){
                    redirect(getAdminPath());
                }else {

                    $id = GET_DATA('id');
                    
                    if(has_child_nav($id)){
                        $message = "This item has the sub item";
                    }else {
                        delete_nav_item($id, $message);
                    }        
                    load()->sys("session")->run()->add("_delete_nav_mess", $message);

                    
                    //echo urldecode(load()->sys("session")->run()->get('_full_url'));
                    if(GET_DATA('next')){
                        redirect(GET_DATA('next'));
                    }
                }
                
                ?>
   
        </div>
 <?php admin_page_include('includes/footer.php')?>