<?php
header('Content-Type: text/html; charset=utf-8');
mb_internal_encoding('utf-8');
admin_page_include('includes/header.php');

admin_page_include("includes/add_page_functions.php");

$errs = array();

load()->lib("PageOperation")->open();

$page_operation = new PageOperation();

$templates = $page_operation->get_templates();

//geting the name of the content writer from database in refernce to the logged in user
$news_author_name = id_to_username($user->getID());
$cats = array();
$button_name = "Add News";



if(GET_DATA("action")){
    $button_name = "Add Page";
    
    if(POST_DATA()){ // If is the form  submitted!
        //Get news title
        $news_title = trim(strip_tags(POST_DATA("news_title")));
        //Get News Body
        //$news_body = str_replace('"', "'",POST_DATA("news_body"));
        $news_body = POST_DATA("news_body");
        //$news_body_editor = POST_DATA("news_body");
                
        //Get news subtitle
        $news_subtitle = trim(strip_tags(POST_DATA("spcial-line")));

        //Get news link
        $news_paper_link = trim(strip_tags(POST_DATA("source_link")));
        
        //Get news paper id
        $news_paper_id = (int) trim(strip_tags(POST_DATA("paper_id")));
        
        $reference = (int) POST_DATA("page_reference");
            
        $cats = (int) POST_DATA("category");
        
        $template =  trim(strip_tags(POST_DATA("template")));
        
        //Date and time vlues
        $D = (int) POST_DATA("day");
        $D = ($D < 10) ? "0".$D:$D;
        $M = (int) POST_DATA("month");
        $M = ($M < 10) ? "0".$M:$M;
        $Y = (int) POST_DATA("year");
        $h = (int) POST_DATA("hour");
        $h = ($h < 10) ? "0".$h:$h;
        $m = (int) POST_DATA("minute");
        $m = ($m < 10) ? "0".$m:$m;
        $diem = POST_DATA("diem");

        //get date and times
        $news_date = convert_date("{$Y}-{$M}-{$D} {$h}:{$m}{$diem}", "Y-m-d H:i");

        //get the new visibility
        $visibility = POST_DATA("visibility");

        
    }else {
        if(GET_DATA("action") == "add"){
            //run time values
            $news_body_editor = "";
            $news_title = "";
            $news_body = "";
            $news_subtitle = "";
            $news_paper_link = "";
            $news_paper_id = "";
            $news_date = date("Y-m-d H:i");
            $D = date("d");
            $M = date("m");
            $Y = date("Y");
            $h = date("h");
            $m = date("i");
            $diem = date("a");
            $visibility = "";
            $reference = "";
            $template = "";
            
        }else if(GET_DATA("action") == "edit"){
            if(GET_DATA("id")){
                if( GET_DATA("id") > 0 && is_numeric(GET_DATA("id"))){
                    $id = GET_DATA("id");
                    if(!POST_DATA()){ //If is the form not submitted, then fetch the value from database and show it into form 
                        $news_sql = "SELECT * FROM content WHERE content_id = $id";
                        $news_db = DB::run()->setSql($news_sql)->run_sql();
                        
                        if($news_db->get_count()){
                            $news = $news_db->get_first();  
                            $news_title = $news['content_title'];
                            $news_body = $news['body'];
                            $news_subtitle = $news['content_subtitle'];
                            $template = $news['source_link'];
                            $news_paper_id = $news['paper_id'];
                            $visibility = (int) $news['visibility'];
                            $date = $news["added_date"];
                            
                            $D = convert_date($date, "d");
                            $M = convert_date($date, "m");
                            $Y = convert_date($date, "Y");
                            $h = convert_date($date, "h");
                            $m = convert_date($date, "i");
                            $diem = convert_date($date, "a");;
                            
                            //$news_date = convert_date("{$Y}-{$M}-{$D} {$h}:{$m} {$d}", "Y-m-d H:i");
                            $rel_db = DB::run()->read("class_relationship")->where("entity_id", $id)->run_sql()->get_first();
                            $reference = $rel_db['class_id'];
                            
                            
                        }else {   
                            redirect(admin_link());
                        }
                    }
                }else {
                    redirect(admin_link());
                }
            }else {
                redirect(admin_link());
            }
        }
    }
    
    
    $operation_id = GET_DATA("id");
    $message = "";
    $status = false;
    
    if(POST_DATA()){ //If is the form submitted, then data will be added or edited
        $values = array();
        
        $values["content_title"] = $news_title;
        $values["content_subtitle"] = $news_subtitle;
        $values["body"] = str_replace("\\", "", $news_body);
        $values["visibility"] = $visibility;
        $values["modified_date"] = date("Y-m-d H:i"); 
        $values["added_date"] = $news_date;
        $values["paper_id"] = $news_paper_id; 
        $values["content_subtitle"] = $news_subtitle; 
        $values["source_link"] = $template == '$' ? "":$template;
        
        switch(GET_DATA("action")){ 
            case "add": // Adding the new data!
                $values["author"] = $news_author_name;      
                //$content_type =  GET_DATA('content-type') ? GET_DATA('content-type'):"news";
                $values["content_type"] = "page";
                $values["url_ref"] = make_unique_ref($news_title, "content");                
                $insert = DB::run()->write("content")->values($values)->run_sql();
                if($insert->error()){
                    $message = $insert->sql_error();
                }else {
                    $message = "News Added Successfully!";
                    $operation_id = $insert->last_insert_id();
                    $status = true;
                }
                break;
            case "edit": // Editing the existing data!
                $edit = DB::run()->edit("content")->values($values)->where("content_id", $operation_id)->limit(1)->run_sql();
               
                if($edit->error()){
                    $message = $edit->sql_error();
                }else {
                    $message = "News Edited Successfully!";
                    $status = true;
                }
                break;
            default :
                redirect(admin_link());
        }
    }
    
    if($status){ //If data has been added or edited
        
        if( $reference !== 0 || $reference !==  "" ){
            add_class_relation(array($reference), $operation_id);
        }
        
        if(GET_DATA("action") == "add"){
            $content_type = GET_DATA('content-type');
            redirect(admin_link().  "/".admin_page_name()."?action=edit&id=" . $operation_id);
        }
    }
    
    if(GET_DATA("action") == "edit"){
        //add_class_relation(array($reference), $operation_id);
        $button_name = "Update Page";
    }else if(GET_DATA("action") == "add") {
        $button_name = "Add Page";
    }else  {
        redirect(admin_link());
    }
}else {
    redirect(admin_link());
}

if(isset($message)) {
    if(strlen($message)){
        show_main_message($message);
    ?>
    <!--
    <div style="border: 1px solid greenyellow; width: 100%; float: left;padding: 20px 16px; font-size: 13px; margin-bottom: 30px">
        <?php echo $message; ?>
    </div>-->
    <?php
    }
}



?>

<style>
    .inputs {
        width: 100%;
        float:left;
        margin-bottom: 10px;
    }
    
    .inputs input {
        width: 100%;
    }
    
        #cat-form, #auth-form {
        display: none;
        border: 3px solid gray;
        box-shadow: 0px 0px 10px 6px #CCC;
        padding: 15px;
        margin-top: 14px;
        font-size: 14px;
    }

    #cat-form h1 ,
    #auth-form h1 {
        font-size: 20px;
        margin: 0;
        padding: 0;
        margin-bottom: 15px;
    }

    #cat-form input, #cat-form select,
    #auth-form input, #auth-form select {
        width: 80%;
        height: 20px;
        padding: 3px;
        margin-top: 5px;
    }
    #cat-form select,
    #auth-form select {
        height: 30px;
        width: 85%;
    }
    #cat-form input[type='submit'] ,
    #auth-form input[type='submit'] {
        width: 50%;
        height: 40px;
        cursor: pointer;
    }

    .book_thumb_image {
        width: 180px;
    }
    
    .list {
        overflow-y:scroll;
        height: 200px;
        float: left;
        width: 100%;
        border:1px solid #CCC;
        padding: 5px 5px;
        z-index: 5;
    }
    .class_list {
        width: 100%;
        border-top:1px solid #c4ed64;
        list-style-type: none;
        float: left;
        padding: 0;
        margin: 0;
        color: #444;
        z-index: 0;
    }
    
    .class_list li{
        float: left;
        width: 100%;
        border-bottom:1px solid #c4ed64;
        position: relative;
        margin-bottom: 1px;
        
    }
    
    .class_list li .chkBox{
        float: left;
        width: 4%; 
        padding: 4px 0;
    }
    
    .class_list li .label{
        float: left;
        width: 94%;
        padding-left: 8px;
        
    } 
    
    .class_list li .main{
        float: left;
        width: 25%;
        position: absolute;
        right: 0;
        top: 0;
        padding: 4px 0; 
        //padding-right: 5px;
        color: darkviolet;
    } 
    .class_list li .main label {
        width: 100%;
    }
    
    .class_list li .main:hover {
        background-color: rgba(200, 200, 200, 0.5)
    }
    
    .class_list li .label label {
        width: 100%;
        float: left;
        cursor: pointer;
        padding: 4px 0; 
        
    }    
    
    .active, .active:hover {
        background-color: #DDD;
        color: #000;
    }
    
    .custom-dropdown {
        
        
        position: relative;
        float: left;
        width: 100%;
        padding: 0;
        height: 30px;
    }
    
    .custom-dropdown .down_list{
        height: 100%;
        width: 100%;
        float: left;
        box-shadow: 0 0 5px #444;
    }
    .custom-dropdown .drop-down-box{
        height: 100%;
        float: left;
        width: 100%;
        border:1px solid #AAA;
    }
    
    .custom-dropdown .drop-down-box h2,
    .custom-dropdown .drop-down-box ul {
        margin: 0;
        padding: 0;
        float: left;
        width: 100%;
        //box-shadow: 0 0 5px #444;
    }
    .custom-dropdown .drop-down-box h2, .custom-dropdown .drop-down-box h2 a {
        float: left;
        position: relative;
        font-size: 14px;
        height: 100%;
        width: 100%;
    }
    .custom-dropdown .drop-down-box h2 a {
        //border: 1px solid green;
        line-height: 22px;
        height: 100%;
        float: left;
    }
    .custom-dropdown .drop-down-box ul a ,
    .custom-dropdown .drop-down-box h2 a {
        font-weight: normal;
        font-size: 13px;
        width: 100%;
        padding: 3px 12px;
        float: left;
    }
    
    .custom-dropdown .drop-down-box ul {
        list-style-type: none;
        position: absolute;
        top: 28px;
        background-color: #FFF;
        border: 1px solid #888;
        left: 0;
        padding: 0 0 6px;
        display: none;
    }
    
    .custom-dropdown select {
        display: block;
    }
    
    
    .inner-sidebar {
        float: left;
        width: 31.111%;
        margin-left: 2.22222222%;
    }
    
    .input-control-box {
        position: relative;
        height: 25px;
        width: 100px;
        float: left;
        border:1px solid #888;
        margin-right: 5px;

    }

    .input-control-box input {
        width: 100%;
        height: 100%;
        float: left;
        border: none;
        outline: none;
        color: #888;
    }

    .input-control-box input:focus {
        border: 0;
    }

    .input-control {
        width: 20px;
        height: 25px;
        float: left;
        position: absolute;
        right: 0;
        top: 0;
    }



    .up, .down {
        padding: 0;
        margin: 0;
        font-size: 10px;
        background: none;
        border: none;
        height: 12px; 
        width: 20px;
        box-shadow: none;
        cursor: pointer;
        float:left;
        color: #888;
        text-align: center;
    }

    .up {
        //margin-bottom: 2px;
    }

    .up:hover, .down:hover,.up:focus, .down:focus {
        background-color: #CCC;
        border: none;
        box-shadow: none;
    }
    .date {
        font-size: 11px; 
    }
    
    .date .day {
        font-size: 10px; 
        width: 50px;
        
    }
    .date .month {
        width: 48px;
    }
    .date .year {
        width: 60px;
    }
    .date .hour {
        width: 40px;
        margin-left: 8px;
    }
    .date .minute {
        width: 40px;
    }
    .date .diem {
        width: 60px;
        color: #888;
        float: left;
        height: 25px;
    }
    
    .date .diem select {
        width: 100%;
        border:1px solid #888;
        height: 100%;
        padding: 2px;
    }
    
</style>

<?php if(GET_DATA('action') == "add"):?>
<h2 class="main-body-page-header">Add Page</h2>
<?php else: ?>
<h2 class="main-body-page-header">Edit Page</h2>
<?php endif; ?>
<form action="" method="post" class="add-news">
    <div class="col-12">  
        <div class="inputs">
            <label for="news_title">Page Title</label>
            <input type="text" id="news_title" name="news_title" value="<?php echo $news_title; ?>" >
        </div>
    </div>
    <div class="col-8 col-mg-r-0">  

        <div class="widget">
            <textarea id="editor" name="news_body"><?php echo str_replace("\\", "",$news_body); ?></textarea>
        </div>
    </div>
    <div class="inner-sidebar" >   
        <div class="widget">
            <div class="widget-header">
                <h2>Template</h2>
                <span class="togole"></span>
            </div>
            <div class="widget-body">
                <p style="color: #999">Select page template so that the page will open with selected template</p>
                <?php print_templates($templates, $template); ?>
            </div>
        </div>
        <div class="widget">
            <div class="widget-header">
                <h2>Page Reference</h2>
                <span class="togole"></span>
            </div>
            <div class="widget-body">
                <p style="color: #999">Select page reference so that you can add page content by this page reference</p>
                <?php $s = get_class_list("class_references", 'category', true, $reference);
                if($s->get_total()){?>
                <select name="page_reference" id="page_reference">
                    <option value="">(select)</option>
                    <?php $s->show_data();?>
                </select>
                <?php } ?>
            </div>
        </div>
        <div class="widget">
            <div class="widget-header">
                <h2>Publish</h2>
            </div>
            <div class="widget-body">                
                
                <div class="inputs">
                    <label for="news-link">Visibility</label>
                    <div class="visibility-dropdown">
                        <select name="visibility">
                            <option value="1" <?php echo ($visibility === 1) ? "selected":"" ?> >Visible</option>
                            <option value="0" <?php echo ($visibility === 0) ? "selected":"" ?> >Invisible</option>
                        </select>
                    </div>
                </div>
                
                <script>
                    //custom_dropdown(".visibility-dropdown", 1);
                </script>
                <div class="button" style="width:100%; float: left; margin-top: 15px;">
                    <input type="submit" value="<?php echo $button_name; ?>" >
                </div>
            </div>
        </div>
    </div>

</form>
    <script>
        control_input("day","day-control", 31, 2);
        control_input("month","month-control", 12, 2);
        control_input("year","year-control", 2016, 1980);
        control_input("minute","minute-control", 59, 01);
        control_input("hour","hour-control", 12, 01);

        validate_date("day", 1);

        function is_valid_number(num) {
            var number = /[0-9]+/g;
            if(number.test(num)){
                return true;
            }
            return false;

            /*
            if(!$.isNumeric(num) ) {
                return false;
            }
            return true;
            */
        }


        function validate_date(){
            var D_name = "day";
            var M_name = "month";
            var Y_name = "year";
            
            var day = "."+D_name+" .input-box input";
            var month = "."+M_name+" .input-box input";
            var year = "."+Y_name+" .input-box input";
            
            var days_per_month = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30 , 31];

            var up_c = ".up";
            var down_c = ".down";

            $(day).on("keyup blur", validate);
            $(month).on("keyup blur", validate);
            $(year).on("keyup blur", validate);
            $(up_c).on("mouseup", validate);
            $(down_c).on("mouseup", validate);


            $(".add-news").on("submit", validate);

            function validate(e){
                return  true;
            }
        }

        function valid(value, box, min, max){
            var alert_color = "#B00";
            if(!is_valid_number(value)){                
                $(box).css("border","1px solid "+ alert_color);
                    return false;
            }else {
                if(value > max || value < min){   
                     $(box).css("border","1px solid "+alert_color);
                     return false;
                }else {
                    $(box).css("border","1px solid #888");
                    return true;
                }
            }
        }

        function foucs(box){
            $(box).css("border","1px solid red");
        }

        function de_foucs(box){
            $(box).css("border","1px solid #888");
        }


        var init = 0;

        function control_input(input_name, control_name, max, min){

            var up_c = "." + control_name + " .up";
            var down_c = "." + control_name + " .down";

            var key_c = "." + input_name + " .input-box input";

            $(key_c).attr("autocomplete", "off");

            $(key_c).on("keydown keyup", function(e){
                    var et = "";
                    switch(e.type){
                        case "keydown":
                            et = "down";
                            break;
                        case "keyup":
                            if(e.which == 38) et = "up";
                            break;
                    }


                    if(e.which == 38) {

                        if($(this).val() == "day"){
                           $(this).val(1); 
                        }else if($(this).val() < max){
                            increase_number(input_name, et);
                        }
                    }else if(e.which == 40){

                        //if($(this).val() < min){
                            decrease_number(input_name, et, min);
                        //}
                    }

            });


            $(up_c).on("mousedown mouseup", function(e){
                init = min;
                var et = "";
                switch(e.type){
                    case "mousedown":
                        et = "down";
                        break;
                    case "mouseup":
                        et = "up";
                        break;
                }
                function inc_num(){
                    if(et === "down"){
                        time = true;
                        if($(key_c).val() == "day"){
                            $(key_c).val(1);
                        }else if($(key_c).val() < max){
                            increase_number(input_name, et);
                            if(time){   
                                t = setTimeout(inc_num, 300);
                            }
                        }
                    }else if(et === "up"){
                        time = false;
                        clearTimeout(t);
                    }
                }
                inc_num();
            });

            $(down_c).on("mousedown mouseup", function(e){
                var et = "";
                switch(e.type){
                    case "mousedown":
                        et = "down";
                        break;
                    case "mouseup":
                        et = "up";
                        break;
                }

                function dec_num(){
                    if(et === "down"){
                        time = true;
                        if($(key_c).val() >= min){
                            decrease_number(input_name, et, min);
                            if(time){   
                                t = setTimeout(dec_num, 300);
                            }
                        }
                    }else if(et === "up"){
                        time = false;
                        clearTimeout(t);
                    }
                }
                dec_num(); 
            });
        }



        var time = true;
        function increase_number(input_name, e){

            if(e === "down"){

                var number_box = $("."+ input_name + " .input-box input");
                var number = number_box.val();
                if(number_box.val().length == 0 || number_box.val() == ""){
                    number_box.val(init);
                }else {
                    var num = parseInt($("."+ input_name + " .input-box input").val());
                    num++;
                    number_box.val(num);
                }
            }
        }

        function decrease_number(input_name, e, min){
            if(e === "down"){   
                var number_box = $("."+ input_name + " .input-box input");
                var number = number_box.val();
                if(number_box.val() !== "" || number_box.val().length !== 0){
                    if(min <= number_box.val()){
                        var num = parseInt($("."+ input_name + " .input-box input").val());
                        num--;
                        number_box.val(num);
                    }
                }
            }                
        }

        //CKEDITOR.replace( 'editor' , ck_editor_config);
        
        
        //console.log(ck_editor_config);
        CKEDITOR.replace( 'editor' , ck_editor_config);
        
        //CKEDITOR.replace( 'editor');

        var type = "category";
        var is_parent = "true";; 
        var aj_path = "<?php echo admin_url(). "/ajax/"?>";

        var cname = $("#name");
        var parent = $("#parent");
        var form = $a("#cat-add", "json");

        form.setEvent("click");
        form.setUrl(aj_path +"add_class.php");
        form.setValues(function(){

            this.values = {
                name:cname.val(),
                parent_id:parent.val(),
                type:type,
                is_parent:is_parent
            }

            return false;
        });
        form.validation(function(){
            if(this.values.name.length == 0){
                alert("You did'nt type anything!");
                return false;
            }
            return true;
        });
        form.success(function(r){
            if(type != 'tag'){

                if(r.status){
                    var p_id = r.parent_id;
                    var parent = $a("#parent", 'load');
                    parent.setValues(function(){
                        this.values = {
                            active_id : p_id,
                            init_html : "<option value=\"0\">(no parent)</option>",
                        }
                    });
                    parent.setUrl(aj_path +"get_class.php");
                    parent.run();  

                    change_form_cat(r.last_id);
                }
            }
        });
        form.run();
        var aname = $("#auth-name");
        var nick_name = $("#nick-name");

        var auth_form = $a("#auth-add", "json");
        auth_form.setEvent("click");
        auth_form.setUrl(aj_path +"add_author.php");
        auth_form.setValues(function(){

            this.values = {
                name:aname.val(),
                nick_name:nick_name.val()
            }
            return false;
        });

        auth_form.validation(function(){
            if(this.values.name.length == 0){
                alert("You did'nt type anything!");
                return false;
            }
            return true;
        });

        auth_form.success(function(r){

            if(r.status){
                //var p_id = r.parent_id;
                var list = $a("#author", 'load');
                list.setUrl(aj_path +"get_author.php");
                list.success(function(){
                    set_active_author(r.last_id);
                });
                list.run();
            }
        });
        auth_form.run();

        $(".class_list li").each(function(key, a){
            var check = $(this).find(".chkBox input:checkbox");
            var exists_class = "active";
            var class_add_to = $(".class_list li").eq(key);
            check.on("change", function(){
                if(class_add_to.hasClass(exists_class)){
                    class_add_to.removeClass(exists_class);
                }else {
                    class_add_to.addClass(exists_class);
                }
                //$(".class_list li").eq(key).css("background-color","#555");
            });

        });
        
        
        $( document ).ready(function() {
            function change_editor_theme(){
                 $("#cke_editor .cke_top, #cke_editor .cke_bottom").css("transition","2s;");
                 $("#cke_editor .cke_top, #cke_editor .cke_bottom").css("background-image","none");
                 $("#cke_editor .cke_top, #cke_editor .cke_bottom").css("background-color","#EEE");
                 //$("#cke_editor .cke_editable").css("height","500px");
                 //$("#cke_editor .cke_inner").css("height","500px");
                 //$("#cke_editor .cke_contents").css("height","399px");
            }
            
            setTimeout(change_editor_theme,600);
            setTimeout(change_editor_theme,1000);
            setTimeout(change_editor_theme,2000);
        });
    </script>
        <script src="<?php echo admin_url()."/js/fsinheader.js"?>" type="text/javascript"></script>
    <script>
        /*
        var paper_id = "<?php //echo POST_DATA("paper_id") ?>";
        paper_id = parseInt(paper_id);
        
        custom_dropdown("custom-dropdown", paper_id);
        */
    </script>
<?php admin_page_include('includes/footer.php')?>