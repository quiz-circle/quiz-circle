<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


load()->sys('Content')->open();
load()->sys('Session')->open();
load()->sys('Data')->open();

$com = new Content("comment","comment_id","comment_id");
//Explain($com);
$com->set_redirect_path(admin_link() . "/comments-list", "query");
$com->set_redirect_path(admin_link() . "/comments-list", "db");
$com->set_redirect_path(admin_link() . "/comments-list", "del_success");

function before_action_function($instanse){
    $session = new Session;
    $comment =  $instanse->get_first();
    
    if($comment['approved'] == 1){
        $session->add('_coment_delete_dialogue_box', true);
        
        if(GET_DATA('force_comment_delete') == 1){
            //cri_r  -> comment delete id reference
            $cdi_r = $session->flush('_cdi_r');
            if($cdi_r){
                $id = Data::decrypt($cdi_r);
                if($comment['comment_id'] == $id){
                    add_flash_message("comment-delete-message", "Comment successfully deleted!", "red");
                    return true;
                }
                return false;
            }
            return false;
        }else {
            redirect(admin_link()."/comments-list"."?approval_status=1&comment_id=" . $comment['comment_id'] );
        }
        return false;
        
    }else{
        add_flash_message("comment-delete-message", "Comment Successfully deleted!", "brown");
        return true;
    }    
}


$com->before_action("before_action_function");
$com->delete();

//add_flash_message("comment-delete-message", "comemnt successfullye deleted!");



//Explain($comment);
