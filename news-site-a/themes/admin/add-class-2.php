<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Home Book Library</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        
        <div>
            <?php
                require_once "themes/admin/functions.php";
                //echo "themes/admin/functions.php";
                //echo make_unique_ref($_GET['cat-name'], 'class');
            
                if(!isset($_GET['type']) && empty($_GET['type'])){
                    redirect(getAdminPath());
                }else {
                    $type = $_GET['type'];
                    if(isset($_POST) && !empty($_POST)){
                        
                        $name = trim($_POST['cat-name']);
                        $parent = $_POST['parent'];

                        if(!has_class_exists($name,$type, $parent)){

                            $url_ref = ($type == "tag") ? make_unique_ref($name."-".$type, 'class'):make_unique_ref($name, 'class');

                            $sql = "INSERT INTO class";
                            $sql .= "( name, url_ref)";
                            $sql .= " VALUES('$name', '$url_ref')";
                            $cl_insert = DB::run()->setSql($sql)->run_sql();


                            if(!$cl_insert->error()){
                                $insert_id = $cl_insert->last_insert_id();

                                $sql = "INSERT INTO class_type";
                                $sql .= "( class_id, type, parent)";
                                $sql .= " VALUES($insert_id, '$type', $parent)";
                                $ct_insert = DB::run()->setSql($sql)->run_sql();

                                if(!$ct_insert->error()){
                                    echo ucfirst($type)." successfully added!";
                                }else {
                                    echo "Not success!<br>";
                                    echo $cl_insert->sql_error();
                                }
                            }else {
                                echo "Failed to add category!<br>";
                                echo $cl_insert->sql_error();
                            }

                        }else {
                            echo "This ".ucfirst($type)." is exists";                        
                        }
                    }

                    $sql = "select * from class ";
                    $sql .= "natural join class_type ";
                    $sql .= "where type='$type'";
                    $cats = DB::run()->setSql($sql)->run_sql();
                }
            ?>

            <form action="" method="post">
                <h1>Add a <?php echo $_GET['type']?></h1>
                Type a name:<br>
                <input type="text" name="cat-name" required><br><br>
                <?php if($type != "tag"):?>
                Parent:<br>
                <select name="parent" required>
                    <option value="0">(no parent)</option>    
                    <?php if($cats->get_count()):?>
                    <?php foreach($cats->get_array() as $cat):?>
                    
                    <option value="<?php echo $cat['class_id']?>"><?php echo $cat['name']?></option>
                    
                    <?php endforeach;?>
                    <?php endif; ?>
                </select><br><br>
                <?php else: ?>
                <input type="hidden" name="parent" value="0">
                <?php endif ?>
                <input type="submit" value="Add" >
            </form>
        </div>
    </body>
</html>
