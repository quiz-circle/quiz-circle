<?php 
header('Content-Type: text/html; charset=utf-8');
mb_internal_encoding('utf-8');
admin_page_include('includes/header.php');


?>        
<style>
    #status-monitor {
        display: inline;
        margin-left: 10px;
    }
    
</style>
    
        <div>
            
            
            <?php
            echo load()->sys("session")->run()->flush("_delete_class");
            
            $cl = DB::run()->read("class")->run_sql();
            foreach ($cl->get_array() as $class){
                update_class_counter($class["class_id"]);
            }
            
            if(!isset($_GET['type']) && empty($_GET['type'])){
                redirect(getAdminPath());
            }else {
                $type = $_GET['type'];
                $tree = $_GET['type'] == "tag" ? false:true;       
                
                function get_class_list_func($row, $ac_id, $level){
                    $cell_reference = md5($row['class_id']);
                    $type = $GLOBALS['type'];
                    $tree = $GLOBALS['tree'];
                    
                    //global  $type;
                    $res = "<tr>";
                        $res .= "<td>";
                        $res .= "<a href='".admin_link()."/list?".$type."=".$row['url_ref']."'>";
                        $res .= child_sym(($level-1), "&nbsp;&nbsp;&nbsp;").$row['name'];
                        $res .= "</a>";
                        $res .= "</td>";
                        $res .= "<td>";
                        $res .= $row['url_ref'];
                        $res .= "</td>";
                        $res .= "<td>";
                        $res .= $row['count'];
                        $res .= "</td>";
                        $res .= "<td>";
                        $res .= "<a href='javascript:' ";
                        $res .= " onclick='_edit_class(".$row['class_id'].", \"".$type."\", \"".$tree."\" )' >";
                        $res .= "Edit";
                        $res .= "</a>";
                        $res .= "</td>";
                        $res .= "<td>";
                        $res .= "<a href='delete-class?id=";
                        $res .= $row['class_id'] ."&next=".  full_url()."&type=$type'>";
                        $res .= "Delete";
                        $res .= "</a>";
                        $res .= "</td>";
                    $res .= "</tr >";
                    return $res;
                }
                $cl = get_class_list("get_class_list_func", $type, $tree);
                $total = $cl->get_total();
                
                $type = strtolower($type);
                $type = str_replace(array('-','_'), array(' ',' '), $type);
                $head = "Total ".$total." ". _mpw($type, $total)." found";
                
                ?>
            
            <h2 class="main-body-page-header"><?php echo ucfirst($type)?> list</h2>
            <p><?php echo $head; ?></p>
                <?php if($total): ?>
                <table class="contet-list">
                    <thead>
                        <tr>
                            <td>Name </td>
                            <td>URL Reference</td>
                            <td>Count</td>
                            <td colspan="2">Action</td>
                        </tr>
                    </thead>
                <?php $cl->show_data(); ?>
                </table>
                <?php endif; } ?>
            
            <a href='add-class?type=<?php echo $type; ?>'><h2>Add <?php echo $type; ?></h2></a>
            <script>
                var tr;
                var reload = window.location; 
                function _edit_class(class_id, type, tree){
                    tr = tree;
                    var html = '';
                        html += '<div class="edit-class-input name" >';
                            html += '<label for="class-name">Name</label>';
                            html += '<input type="text" value="" id="class-name">';
                            html += '<div id="status-monitor" ></div>';
                        html += '</div>';
                        html += '<div class="edit-class-input" >';
                            html += '<label for="class-url-ref">Url reference</label>';
                            html += '<input type="text" value="a" id="class-url-ref">';
                            html += '<div id="status-monitor" ></div>';
                        html += '</div>';
                        if(tree){
                            html += '<div class="parent-class" >';
                                html += '<label for="class-url-ref">Parent</label>';
                                html += '<select id="parent-class">';
                                html += '</select>';
                            html += '</div>';
                        }
                        html += '<div class="edit-class-input" >';
                            html += '<input type="submit" value="Edit" id="class-edit-button" >';
                        html += '</div>';
                    html += "</div";
                    dialogue_box("Edit " + type, html);
                    disable_button();
                    
                    load_class_information(class_id, type, data_success, data_error);
                    
                    $("#class-edit-button").on("click", function(){
                        var name = $("#class-name").val();
                        var url_ref = $("#class-url-ref").val();
                        var parent = $("#parent-class").val();
                        update_class_information({"name":name, "url_ref":url_ref, "parent":parent, "class_id":class_id}, update_success, update_error );

                    });
                    
                    function update_success(r){
                        cons(r);
                        if(r.status){
                            window.location = reload;
                        }
                    }
                    
                    function update_error(r, a){
                        cons(a);
                    }
                    
                    function enable_button(){
                        $("#class-edit-button").removeAttr("disabled");
                    }
                    
                    function disable_button(){
                        $("#class-edit-button").attr("disabled", true);
                    }
                    
                    function update_class_information(class_data, callback, error){
                        $.ajax({
                            dataType: "json",
                            url: aj_path + "update_class.php",
                            data: class_data,
                            success: callback,
                            error: error
                        });
                    }
                    
                    $(".edit-class-input, .parent-class").each(function(){
                        var mes = $(this).find("div#status-monitor");
                        
                        $(this).find("input:text, select").on("keyup change", function(e){
                            var parent = (tree) ? parseInt($("#parent-class").val()):0;
                            var value = $(this).val();
                            var data = {};
                            var value_id = $(this).attr("id");
                            //cons(e);
                            
                            switch($(this).attr("id")){
                                case "class-name":
                                    data = {"class_id":class_id, "key": "name", "value": value, "parent_id":parent}
                                    break;
                                case "class-url-ref":
                                    data = {"class_id":class_id, "key": "url_ref", "value": value}
                                    break;
                                case "parent-class":
                                    data = {"class_id":class_id, "key": "name", "value": $("#class-name").val(), "parent_id":parent}
                                    mes = $(".name div#status-monitor");
                            }
                            
                            validate_class(data, data_exists, data_error)
                            //cons($(this).val());                            
                            function data_exists(res){
                                mes.html(res.message);
                                if(res.status){
                                    disable_button();
                                }else {
                                    enable_button();
                                }
                            }
                        });

                    });
                }

                function load_class_information(class_id, type, callback, error){
                    var class_data = {"class_id" :class_id, "type":type};
                    $.ajax({
                        dataType: "json",
                        url: aj_path + "get_class_info.php",
                        data: class_data,
                        success: callback,
                        error: error
                    });
                }
                
                function validate_class(class_data, callback, error){
                    $.ajax({
                        dataType: "json",
                        url: aj_path + "validate_class.php",
                        data: class_data,
                        success: callback,
                        error: error
                    });
                }
                
                
                function data_success(res){
                    if(res.status){
                        $("#class-name").val(res.class_name);
                        $("#class-url-ref").val(res.class_url_ref);
                        if(tr) load_category_in("#parent-class", res.parent_id);
                        cons(res);
                    }
                }

                function data_error(e, a){
                    console.log(a);
                }


            </script>
        </div>
 <?php admin_page_include('includes/footer.php')?>