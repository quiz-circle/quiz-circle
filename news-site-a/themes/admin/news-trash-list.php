<style>
    #news-visibility {
        padding: 4px 9px;
        font-size: 13px;
        letter-spacing: -0.1px;
        font-family: verdana;
        font-weight: bold;
    }
    
    .visible {
        color: #FFF !important;
        background-color: #2A970C;
        border: 1px solid green;
        text-decoration: none;
    }
    
    .invisible-n {
        text-decoration: none;
        border: 1px solid #CCC;
        background-color: #EEE;
        color: #888 !important;
    }
    
    .visible:hover ,.invisible-n:hover {
        text-decoration: none;
    }
    
    .contentCheckBox input[type='checkbox'] {
        width: 20px;
        height: 20px;
        border: none;
        outline:  none;
        transform: scale(1, 1);
    }
    
</style>
<?php
header('Content-Type: text/html; charset=utf-8');
mb_internal_encoding('utf-8');
admin_page_include('includes/header.php');

function show_day_month_year($input = "post", $prefix = ""){
    
    $day = $prefix ? $prefix."_day":"day";
    $month = $prefix ? $prefix."_month":"month";
    $year = $prefix ? $prefix."_year":"year";
    ?>
    <label for="date">Date</label>
    <select name="<?php echo $day ?>" id="<?php echo $day ?>">
        <?php _option_range(1,31, "<option value=''>Day</option>", GET_DATA($day))?>
    </select>
    <select name="<?php echo $month ?>" id="<?php echo $month ?>">
        <?php _option_range(1,12, "<option value=''>Month</option>", GET_DATA($month))?>
    </select>
    <select name="<?php echo $year ?>" id="<?php echo $year ?>">
        <?php _option_range(2016,1980, "<option value=''>Year</option>", GET_DATA($year))?>
    </select>
    <?php
}

function _option_range($start, $end = "", $first_html = "", $selected = ""){
    
    echo $first_html;
    if(is_int($start)){
        if($start < $end){
            for($i = $start;  $i<= $end; $i++):
                $val  = $i < 10 ? "0".$i:$i;
            ?>
            <option value="<?php echo $val; ?>" <?php echo $i == $selected ? "selected":""  ?>> <?php echo $val?> </option>
            <?php 
            endfor;
        }else {

            for($i = $start;  $i >= $end; $i--):
            $val  = $i < 10 ? "0".$i:$i;
            ?>
            <option value="<?php echo $val; ?>" <?php echo $i == $selected ? "selected":""   ?>> <?php echo $val?> </option>
            <?php 
            endfor;
        }
    }else if(is_array($start)){
        foreach($start as $key => $value):
        ?>
        <option value="<?php echo $key; ?>"><?php echo $value?> </option>
        <?php 
        endforeach;
        
    }
}

?>
    
<?php

    function make_news_config(){
        $conf = array();
        
        if(GET_DATA('category')){
            $conf['class_url_ref'] = urldecode(GET_DATA('category'));
            $conf['class-type'] =  "category";
        }
        
        if(GET_DATA('tag')){
            $conf['class_url_ref'] =  urldecode(GET_DATA('tag'));
            $conf['class-type'] =  "tag";
        }
        
        if(GET_DATA('year')){
            $d_query = GET_DATA('year'); 
            if(GET_DATA('month')){
                $d_query = $d_query ."-".GET_DATA('month');
                if(GET_DATA('day')){
                    $d_query = GET_DATA('year') ."-".GET_DATA('month').'-'.GET_DATA('day');
                }
            }
            
            $conf['added-date'] =  array("LIKE", "%".$d_query."%");
        }
        
        if(GET_DATA('modified-date')){
            $conf['modified-date'] =  array("LIKE", GET_DATA('modified-date'));
        }
        
        if(isset($_GET['visibility'])){
            if(GET_DATA('visibility') !== ""){
                $conf['visibility'] =  GET_DATA('visibility');
            }
        }
        if(GET_DATA('content-type')){
            $conf['content-type'] =  urldecode(GET_DATA('content-type'));
        }
        
        if(GET_DATA('paper-id')){
            $conf['paper-id'] =  GET_DATA('paper-id');
        }
        
        if(GET_DATA('author')){
            $conf['author'] =  urldecode(GET_DATA('author'));
        }
        
        return $conf;
    }
    
    function news_list_admin_home($each){
        $news_page = get_config("news-details/reference");;
        $tb = "<tr id='".md5($each['content_id'])."'>";
            $tb .= "<td width='40' class='contentCheckBox' style='text-align:center'>";
                $tb .= "<input type='checkBox' name='CheckedContent[ ]' value='".$each['content_id']."' >";
            $tb .= "</td>";
            $tb .= "<td width='650' class='news-content'>";
                $tb .= "<a href='". admin_link()."/restore-news?id="; 
                $tb .= $each['content_id']."&";
                $tb .= "next=".  full_url()."' onclick='return confirm(\"Are You Sure Want restore!\")' > ";
                $tb .= $each['content_title'];
                $tb .= "</a>";
            $tb .= "</td>";
            $tb .= "<td>";
                $tb .= get_old_status(md5($each['content_id']));
            $tb .= "</td>";
            $tb .= "<td>";
                $tb .= "<a href='". admin_link()."/news-trash-list?restore_id="; 
                $tb .= $each['content_id']."'";
                $tb .= " onclick='return confirm(\"Are You Sure Want restore!\")' > ";
                    $tb .= "Restore";
                $tb .= "</a>";
            $tb .= "<td>";
                $tb .= "<a href='". admin_link()."/delete-news?id="; 
                $tb .= $each['content_id']."&";
                $tb .= "next=".  full_url()."' onclick='return confirm(\"Are You Sure Want To delete this parmanently!\")' > ";
                    $tb .= "Delete Parmanently";
                $tb .= "</a>";
            $tb .= "</td>";
        $tb .= "</tr>";
        
        return $tb;
    }

    function get_old_status($hash){
        $info_key = "news-trash";
        load()->sys("MetaInfo")->open();
        $meta = new MetaInfo("site_info", "info_key", "info_value");
        
        $trash_meta = $meta->get($info_key);
        
        $trash_meta_data = json_decode($trash_meta, true);
        $index = 0;
        if(count($trash_meta_data)){
            while ($index < count($trash_meta_data)){
                if(array_key_exists($hash, $trash_meta_data[$index])){
                    return $trash_meta_data[$index][$hash];
                }
                $index++;
            }
            return "";
        }
        return "";
    }
    
    function edit_button($each){
        $u = $GLOBALS['user'];
        $cap = $GLOBALS['_get_cap'];        
        $usname = _get('user', 'user_id', $u->getID(), 'username');
        $e_query_name  = "";
        
        $tb = "<a href='". admin_link()."/add-news?action=edit&id=";
        $tb .= $each['content_id'];
        $tb .= "'>Edit</a>";
        return $tb;
    }
    
    function delete_button(){
        
    }
    
    $c = make_news_config();
    
    $c['content-type'] = !isset($c['content-type']) ? "news":$c['content-type'];
    $c['status'] = 'trash';
    
    $n = news_trash_list("news_list_admin_home", $c);
    
    $per_page = 10;
    
    $p_n = (int)($n->get_total() / $per_page);
    
    $n->set_pagination($per_page, "page");
    
    echo "<h2 class='main-body-page-header'>News Trash List</h2>";
    
    if($n->get_total() > $per_page){
        echo "<div class='pagination'>";
        echo $n->get_pagination();
        echo "</div>";
    }
    $pn = ($n->get_total()) ? GET_DATA("page", "1"):"0";
    echo "<p>Page " . $pn  . " of ".$n->total_pages()."</p>";
    
    function GET_SELECTED($key, $value){
        if(isset($_GET[$key])){
            if(GET_DATA($key) !== ""){
                if(GET_DATA($key) == $value){
                    echo "selected";
                }
            }
        }
    }
    
    
    if(POST_DATA()){
        $action = POST_DATA('action');
        $ids = POST_DATA('CheckedContent');
        if(!empty($action)){
            if($index = count($ids)){
                $whereClause = " content_id IN(";
                while ($index > 0){
                    $index--;
                    $whereClause .= $ids[$index]. ' ';
                    $whereClause .= $index !== 0 ? ',' : '';
                }
                
                $whereClause .= ')';
                $sql = "";
                switch ($action){
                    case "approve":
                        $sql .= make_approved_list_sql($ids);
                        break;
                    default :
                        $sql .= "UPDATE content ";
                        $sql .= " SET status = '" . strtolower($action) . "'";
                }
                
                if($sql !== ""){
                    $sql .= "WHERE ".$whereClause;
                    $status_update = DB::run()->setSql($sql)->run_sql();
                    if(!$status_update->error()){
                        show_main_message("News  status Changed!");
                    }else {
                        show_main_message($status_update->get_sql()."<br>". $status_update->sql_error());
                    }
                }
            }
        }
    }else if(GET_DATA('restore_id')) {
        $restore_id = GET_DATA('restore_id');
        $sql = "UPDATE content SET status ='".  get_old_status(md5($restore_id))."' WHERE content_id = " . $restore_id;
        
        $status_update = DB::run()->setSql($sql)->run_sql();
        if(!$status_update->error()){
            show_main_message("News  status Changed!");
        }else {
            show_main_message($status_update->get_sql()."<br>". $status_update->sql_error());
        }

    }    
            
    
    function make_approved_list_sql($ids){
        return "UPDATE content SET status ='publish', visibility = 1 ";
    }
    
    ?>
    <?php
    if($n->get_total()) {
        ?>
        <form action="" method="post">
            <div class="action-bar" style="margin-bottom:8px; margin-top: 15px; width: 100%; float: left; ">
                <label for="action">Action</label>
                <select id="action" name="action">
                    <option value="">(No Action)</option>
                    <option value="approve">Approve</option>
                    <option value="publish">Publish</option>
                    <option value="draft">Draft</option>
                    <option value="trash">Trash</option>
                </select>
                <input type="submit" value="Done" style="padding:3px 10px;">
            </div>
            <table class="contet-list">
                <thead>
                    <tr>
                        <td class="contentCheckBox">
                            <input type="checkbox" name="AllSelect" class="allSelect" >
                        </td>
                        <td>News Title</td>
                        <td>Status</td>
                        <td colspan="2">Action</td>
                    </tr>
                </thead>
            <?php
            $n->show_data(array('priority'=> "ASC", 'added_date'=>"DESC"));
            ?>
            </table>
        </form>
        <?php
    }else {
?>
<h1 style="text-align: center; width: 100%; color: #BBB; margin: 200px 0 ">No News Trash Found</h1>
<?php
    }
?>
<script>
    $(".news-content .short-edit").each(function(){
        $(this).on("click", function(e){
            e.preventDefault();
            var targetID = e.target.id;
            var target = $("tr#"+targetID); 
            //target.find('.classes').find('.author').find('visible-status').css('background-color', 'red');
            target.find('.classes').css('display', 'none');
            target.find('.author').css('display', 'none');
            //target.find('.visible-status').css('display', 'none');
            target.find('.news-content').attr('colspan', '3');
            
            
            
            $("tr#"+targetID).css("height", "200px");
            
            
            
            
            //$("#".targetID).css("height","400px");
            
        });
    });
    
    function _change_news_visibility(id, vis, ref){
        vis = parseInt(vis);
        
        var val = {'vis':vis, 'id':id};
        
        

        
        $.getJSON("<?php echo admin_url() ?>/ajax/change_visibility.php", val, function(e,s ){
    
            if(e.status){
                
                console.log(e.change_vis);
                if(e.change_vis == 0) {
                    $("."+ref).removeClass('visible');
                    $("."+ref).addClass('invisible-n');
                    $("."+ref).html("Invisible");
                }else {
                    $("."+ref).removeClass('invisible-n');
                    $("."+ref).html("Visible");
                    $("."+ref).addClass('visible');
                }
            }
        });
    }
    
</script>

<?php admin_page_include('includes/footer.php')?>