<style>
    #news-visibility {
        padding: 4px 9px;
        font-size: 13px;
        letter-spacing: -0.1px;
        font-family: verdana;
        font-weight: bold;
    }
    
    .visible {
        color: #FFF;
        background-color: #2A970C;
        border: 1px solid green;
    }
    .invisible {
        border: 1px solid #CCC;
        background-color: #EEE;
        color: #888;
    }
    
</style>
<?php
header('Content-Type: text/html; charset=utf-8');
mb_internal_encoding('utf-8');
admin_page_include('includes/header.php');

function show_day_month_year($input = "post", $prefix = ""){
    
    $day = $prefix ? $prefix."_day":"day";
    $month = $prefix ? $prefix."_month":"month";
    $year = $prefix ? $prefix."_year":"year";
    ?>
    <label for="date">Date</label>
    <select name="<?php echo $day ?>" id="<?php echo $day ?>">
        <?php _option_range(1,31, "<option value=''>Day</option>", GET_DATA($day))?>
    </select>
    <select name="<?php echo $month ?>" id="<?php echo $month ?>">
        <?php _option_range(1,12, "<option value=''>Month</option>", GET_DATA($month))?>
    </select>
    <select name="<?php echo $year ?>" id="<?php echo $year ?>">
        <?php _option_range(2016,1980, "<option value=''>Year</option>", GET_DATA($year))?>
    </select>
    <?php
}

function _option_range($start, $end = "", $first_html = "", $selected = ""){
    
    echo $first_html;
    if(is_int($start)){
        if($start < $end){
            for($i = $start;  $i<= $end; $i++):
                $val  = $i < 10 ? "0".$i:$i;
            ?>
            <option value="<?php echo $val; ?>" <?php echo $i == $selected ? "selected":""  ?>> <?php echo $val?> </option>
            <?php 
            endfor;
        }else {

            for($i = $start;  $i >= $end; $i--):
            $val  = $i < 10 ? "0".$i:$i;
            ?>
            <option value="<?php echo $val; ?>" <?php echo $i == $selected ? "selected":""   ?>> <?php echo $val?> </option>
            <?php 
            endfor;
        }
    }else if(is_array($start)){
        foreach($start as $key => $value):
        ?>
        <option value="<?php echo $key; ?>"><?php echo $value?> </option>
        <?php 
        endforeach;
        
    }
}

?>
    
<?php

    function make_news_config(){
        $conf = array();
        
        if(GET_DATA('category')){
            $conf['class_url_ref'] = urldecode(GET_DATA('category'));
            $conf['class-type'] =  "category";
        }
        
        if(GET_DATA('tag')){
            $conf['class_url_ref'] =  urldecode(GET_DATA('tag'));
            $conf['class-type'] =  "tag";
        }
        
        if(GET_DATA('year')){
            $d_query = GET_DATA('year'); 
            if(GET_DATA('month')){
                $d_query = $d_query ."-".GET_DATA('month');
                if(GET_DATA('day')){
                    $d_query = GET_DATA('year') ."-".GET_DATA('month').'-'.GET_DATA('day');
                }
            }
            
            $conf['added-date'] =  array("LIKE", "%".$d_query."%");
        }
        
        if(GET_DATA('modified-date')){
            $conf['modified-date'] =  array("LIKE", GET_DATA('modified-date'));
        }
        
        if(isset($_GET['visibility'])){
            if(GET_DATA('visibility') !== ""){
                $conf['visibility'] =  GET_DATA('visibility');
            }
        }
        if(GET_DATA('content-type')){
            $conf['content-type'] =  urldecode(GET_DATA('content-type'));
        }
        
        if(GET_DATA('paper-id')){
            $conf['paper-id'] =  GET_DATA('paper-id');
        }
        
        if(GET_DATA('author')){
            $conf['author'] =  urldecode(GET_DATA('author'));
        }
        
        return $conf;
    }
    
    $category_id = 0;
    if(GET_DATA('category')){
        $category_id = _get('class', 'url_ref', GET_DATA('category'), 'class_id');
    }
    
    
    function news_list_admin_home($each){
        $category_id = $GLOBALS['category_id'];
        $news_page = get_config("news-details/reference");;
        $tb = "<tr>";
            $tb .= "<td width='650'>";
            $tb .= "<a href='". base_url()."/".$news_page."/".$each['content_id']."' target='_blank'>";
            $tb .= $each['content_title'];
            $tb .= "</a>";
            $tb .= "</td>";
            
            $tb .= "<td style='text-align:center; width:180px'>";
            $identifier = md5($each["content_id"]);
            $pin_label = '';
            if((int)$each["priority"] != 99){
                $tb .= '<div ';
                $tb .=  'class="news-pined">Pined ('.$each["priority"].')';
                $tb .= "</div>";
                $pin_label = 'Unpin/Change Priority';
                $pin_label .= 'Unpin/Change Priority';
                $pin_label = '<a href="javascript:" class="pin-button"'; 
                $pin_label .= " onclick='_unpin_this(\"".$each["content_id"]."\", \"".$each["priority"]."\", \"".  $identifier."\", ".$category_id.")'"; 
                $pin_label .= '>Unpin</a> ';
                $pin_label .= '<a href="javascript:" ';
                $pin_label .= " onclick='_rearrange_priority(\"".$each["content_id"]."\", \"".$each["priority"]."\", \"".  $identifier."\", ".$category_id.")' >";
                $pin_label .= 'Change Priority</a>';
            }else {
                $tb .=  '<div class="news-unpined">Unpined';
                $tb .= "</div>";
                $pin_label = '<a href="javascript:" class="pin-button"'; 
                $pin_label .= " onclick='_pin_this(\"".$each["content_id"]."\", \"".$each["priority"]."\", \"".  $identifier."\", ".$category_id.")'"; 
                $pin_label .= '> Pin</a>';
            }
            
            $tb .= "</td>";
            $tb .= "<td>";
            $tb .= $pin_label;
            $tb .= "</a>";
            $tb .= "</td>";
        $tb .= "</tr>";
        
        return $tb;
    }
    
    $c = make_news_config();
    
    $c['content-type'] = !isset($c['content-type']) ? "news":$c['content-type'];
    
    $n = news_list("news_list_admin_home", $c);
    
    $per_page = 10;
    
    $p_n = (int)($n->get_total() / $per_page);
    
    
    echo "<h2 class='main-body-page-header'>Pin a News <a href='". admin_link()."/add-news?action=add'>Add News</a></h2>";
    if($n->get_total() > $per_page){
    $n->set_pagination($per_page, "page");
    
        echo "<div class='pagination'>";
        echo $n->get_pagination();
        echo "</div>";
    }
    $pn = ($n->get_total()) ? GET_DATA("page", "1"):"0";
    echo "<p>Page " . $pn  . " of ".$n->total_pages()."</p>";
    
    function GET_SELECTED($key, $value){
        if(isset($_GET[$key])){
            if(GET_DATA($key) !== ""){
                if(GET_DATA($key) == $value){
                    echo "selected";
                }
            }
        }
    }
    ?>
    <form action="" method="get" class="news-filter-form">
        <label for="category">Category</label>
        <select name="category">
            <!--<option value="">All</option>-->
            <?php echo get_class_opt("category","<option value=''>All</option>",  GET_DATA("category")); ?>
        </select>
        
        <label for="visisibility">Visible</label>
        <select name="visibility">
            <option value="">All</option>
            <option value="0"  <?php GET_SELECTED("visibility", 0)?> >0</option>
            <option value="1"  <?php GET_SELECTED("visibility", 1)?> >1</option>
        </select>
        <label for="visisibility">News Paper</label>
        <select name="paper-id">
            <?php news_paper_list_options(GET_DATA('paper-id'), "<option value=''>All</option>") ?>
        </select>
        <?php show_day_month_year(); ?>
        <input type="submit" value="Filter" >
    </form>
        
    <?php
    
    //$n->show_data();
    
    if($n->get_total()) {

        ?>
        
        <table class="contet-list">
            <thead>
                <tr>
                    <td>News Title</td>
                    <td colspan="2">Pin</td>
                </tr>
            </thead>
        <?php
            $n->show_data(array('priority'=> "ASC", 'added_date'=>"DESC"));
        ?>
        </table>
        <?php
    }else {
?>
<h1 style="text-align: center; width: 100%; color: #BBB; margin: 200px 0 ">No News Found</h1>
<?php
    }
?>
<script>
    
    function _unpin_this(content_id, priority, identifier, category_id){
        var input = {
            'from' : parseInt(priority),
            'to': 0,
            'entity_id': parseInt(content_id),
            'class_id': category_id,
            'type':'unpin'
        }
        set_priority(input);
    }
    
    function _pin_this(content_id, priority, identifier, category_id){
        var input = {
            'from' : parseInt(priority),
            'to': 0,
            'entity_id': parseInt(content_id),
            'class_id': category_id,
            'type':'pin'
        }
        set_priority(input);
    }
    
    function _rearrange_priority(content_id, priority, identifier, category_id){
        var html = ""; 
        html += "<div style='text-align:center;display:inline-block;margin-bottom: 50px;width: 100%;margin-top: 10px;'>";
            html += "<select id='priority-list'>";
            html += "</select> ";
            html += "<input type='submit' value='Set' id='priority-operation-button'><br><br>";
        html += "</div>";
        dialogue_box("Set Priority", html);
        load_priority_in(category_id, "#priority-list", priority);
        
        $("#priority-operation-button").on('click', function(){
            var pr_value = parseInt($("#priority-list").val());;            
            var input = {
                'from' : parseInt(priority),
                'to': pr_value,
                'entity_id': parseInt(content_id),
                'class_id': category_id,
                'type':'rearrange'
            }    
            cons(input);

            set_priority(input);
        });
    }
    
    
    function load_priority_in(class_id, identifier, active_id){
        var parent = $a(identifier, 'load');
        parent.setValues(function(){
            this.values = {
                "active_pr" : active_id,
                "class_id" : class_id
            }
        });

        parent.success(function(){                
            
        });
        parent.setUrl(aj_path +"get_priorities.php");
        parent.run(); 
    }
    
    //cons(aj_path + "set_priority.php");
    
    function set_priority(input){
        $.ajax({
            dataType: "json",
            url: aj_path + "set_priority.php",
            data: input,
            success: data_success,
            error: data_error
        });
    }
    
    function data_success(res){
        if(res.status){
            cons(res);
            window.location = window.location;
        }
    }

    function data_error(e, a){
        console.log(a);
    }
    
    
</script>

<?php admin_page_include('includes/footer.php')?>