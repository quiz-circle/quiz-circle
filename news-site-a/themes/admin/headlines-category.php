<?php 
header('Content-Type: text/html; charset=utf-8');
mb_internal_encoding('utf-8');
admin_page_include('includes/header.php');

function page_heading($key){
    switch ($key) {
        case "spcial_news_class":
            echo "Set Category For Spcial News";
            break;
        case "headlines_class":
            echo "Set Category For headlines";
            break;
        default :
            echo "Set Category For " . GET_DATA('meta-attibutes') . " column";
            break;
    }
}
?>
        <div>    
            <?php
                $init = "<option value=\"\">(select)</option>";
                $meta = load()->sys("MetaInfo")->run("site_info","info_key", "info_value");
                $sidebar_meta = "home-sidebar";
                $seted = '';
                $class_key = "";
                if(GET_DATA("meta-attibutes")){
                    $meta_attr = GET_DATA("meta-attibutes");
                    if(!is_array(get_config('meta_attr/'.$meta_attr))){
                        $seted = $meta->get(get_config('meta_attr/'.GET_DATA("meta-attibutes")));
                        $class_key  = get_config('meta_attr/' . $meta_attr);
                    }else {
                        if( is_array(get_config('meta_attr/'.$sidebar_meta)) ){
                            $meta_list = get_config('meta_attr/'.$sidebar_meta);
                            if(array_key_exists( GET_DATA('meta-attibutes'), $meta_list)){
                                $seted = $meta->get(get_config('meta_attr/'.$sidebar_meta.'/'.GET_DATA("meta-attibutes")));
                                $class_key = get_config('meta_attr/'.$sidebar_meta.'/'.GET_DATA("meta-attibutes"));
                            }else {
                                echo "d";
                                redirect(admin_link());
                            }   
                        }
                    }
                }else {
                    redirect(admin_link());
                }

                if(POST_DATA()){
                    if(POST_DATA("meta-key")){
                        $key = POST_DATA("meta-key");
                        if($meta->is_exists($key)){
                            $meta->edit($key, POST_DATA("category_url_ref"));
                        }else {
                            $meta->add($key, POST_DATA("category_url_ref"));
                        }
                    }
                }                
                
                $class =  get_class_options_by_ref("category", $init, POST_DATA("category_url_ref", $seted));
                ?>
            
            <h2 class="main-body-page-header"><?php echo page_heading($class_key); ?></h2>
            <form action="" method="post" style="width: 300px">
                <input type="hidden" value="<?php echo $class_key;?>" name="meta-key">
                Category<br>                
                <select name="category_url_ref" required id="parent" style="width: 100%;">
                    <?php echo ($class->get_total()) ? $class->show_data():$init; ?>
                </select><br><br>
                <input type="submit" value="Change" >
            </form>
        </div>
 <?php admin_page_include('includes/footer.php')?>