<style>
    #news-visibility {
        padding: 4px 9px;
        font-size: 13px;
        letter-spacing: -0.1px;
        font-family: verdana;
        font-weight: bold;
    }
    
    .visible {
        color: #FFF;
        background-color: #2A970C;
        border: 1px solid green;
    }
    .invisible {
        border: 1px solid #CCC;
        background-color: #EEE;
        color: #888;
    }
    
</style>
<?php
header('Content-Type: text/html; charset=utf-8');
mb_internal_encoding('utf-8');
admin_page_include('includes/header.php');

function show_day_month_year($input = "post", $prefix = ""){
    
    $day = $prefix ? $prefix."_day":"day";
    $month = $prefix ? $prefix."_month":"month";
    $year = $prefix ? $prefix."_year":"year";
    ?>
    <label for="date">Date</label>
    <select name="<?php echo $day ?>" id="<?php echo $day ?>">
        <?php _option_range(1,31, "<option value=''>Day</option>", GET_DATA($day))?>
    </select>
    <select name="<?php echo $month ?>" id="<?php echo $month ?>">
        <?php _option_range(1,12, "<option value=''>Month</option>", GET_DATA($month))?>
    </select>
    <select name="<?php echo $year ?>" id="<?php echo $year ?>">
        <?php _option_range(2016,1980, "<option value=''>Year</option>", GET_DATA($year))?>
    </select>
    <?php
}

function _option_range($start, $end = "", $first_html = "", $selected = ""){
    
    echo $first_html;
    if(is_int($start)){
        if($start < $end){
            for($i = $start;  $i<= $end; $i++):
                $val  = $i < 10 ? "0".$i:$i;
            ?>
            <option value="<?php echo $val; ?>" <?php echo $i == $selected ? "selected":""  ?>> <?php echo $val?> </option>
            <?php 
            endfor;
        }else {

            for($i = $start;  $i >= $end; $i--):
            $val  = $i < 10 ? "0".$i:$i;
            ?>
            <option value="<?php echo $val; ?>" <?php echo $i == $selected ? "selected":""   ?>> <?php echo $val?> </option>
            <?php 
            endfor;
        }
    }else if(is_array($start)){
        foreach($start as $key => $value):
        ?>
        <option value="<?php echo $key; ?>"><?php echo $value?> </option>
        <?php 
        endforeach;
        
    }
}

?>
    
<?php

    function make_news_config(){
        $conf = array();
        
        if(GET_DATA('category')){
            $conf['class_url_ref'] = urldecode(GET_DATA('category'));
            $conf['class-type'] =  "category";
        }
        
        if(GET_DATA('tag')){
            $conf['class_url_ref'] =  urldecode(GET_DATA('tag'));
            $conf['class-type'] =  "tag";
        }
        
        if(GET_DATA('year')){
            $d_query = GET_DATA('year'); 
            if(GET_DATA('month')){
                $d_query = $d_query ."-".GET_DATA('month');
                if(GET_DATA('day')){
                    $d_query = GET_DATA('year') ."-".GET_DATA('month').'-'.GET_DATA('day');
                }
            }
            
            $conf['added-date'] =  array("LIKE", "%".$d_query."%");
        }
        
        if(GET_DATA('modified-date')){
            $conf['modified-date'] =  array("LIKE", GET_DATA('modified-date'));
        }
        
        if(isset($_GET['visibility'])){
            if(GET_DATA('visibility') !== ""){
                $conf['visibility'] =  GET_DATA('visibility');
            }
        }
        if(GET_DATA('content-type')){
            $conf['content-type'] =  urldecode(GET_DATA('content-type'));
        }
        
        if(GET_DATA('paper-id')){
            $conf['paper-id'] =  GET_DATA('paper-id');
        }
        
        if(GET_DATA('author')){
            $conf['author'] =  urldecode(GET_DATA('author'));
        }
        
        return $conf;
    }
    
    function news_list_admin_home($each){
        $news_page = get_config("news-details/reference");;
        $tb = "<tr>";
            $tb .= "<td width='650'>";
            $tb .= "<a href='". base_url()."/".$news_page."/".$each['content_id']."' target='_blank'>";
            $tb .= $each['content_title'];
            $tb .= "</a>";
            $tb .= "</td>";
            $tb .= "<td>";
            $tb .= "<a href='". admin_link()."/add-page?action=edit&id=";
            $tb .= $each['content_id'];
            $tb .= "'>Edit</a>";
            $tb .= "</td>";
            $tb .= "<td>";
            $tb .= "<a href='". admin_link()."/delete-news?id=";
            $tb .= $each['content_id']."&";
            $tb .= "next=".  full_url()."' >";
            $tb .= "Delete";
            $tb .= "</a>";
            $tb .= "</td>";
            $tb .= "<td style='text-align:center; width:180px'>";
            $identifier = md5($each["content_id"]);
            $tb .= "<a id='news-visibility' class='". $identifier ." ".((int)$each["visibility"] ? 'visible':'invisible')."' ";
            $tb .= " href='javascript:' onclick='_change_news_visibility(\"".$each["content_id"]."\", \"".$each["visibility"]."\", \"".  $identifier."\")' >";
            $tb .= (int)$each["visibility"] ? 'Visible':'Invisible';
            $tb .= "</a>";
            $tb .= "</td>";
        $tb .= "</tr>";
        
        return $tb;
    }
    
    $c = make_news_config();
    
    $c['content-type'] = !isset($c['content-type']) ? "page":$c['content-type'];
    
    $n = news_list("news_list_admin_home", $c);
    
    $per_page = 10;
    
    $p_n = (int)($n->get_total() / $per_page);
    
    $n->set_pagination($per_page, "page");
    
    echo "<h2 class='main-body-page-header'>Showing News <a href='". admin_link()."/add-news?action=add'>Add News</a></h2>";
    
    if($n->get_total() > $per_page){
        echo "<div class='pagination'>";
        echo $n->get_pagination();
        echo "</div>";
    }
    $pn = ($n->get_total()) ? GET_DATA("page", "1"):"0";
    echo "<p>Page " . $pn  . " of ".$n->total_pages()."</p>";
    
    function GET_SELECTED($key, $value){
        if(isset($_GET[$key])){
            if(GET_DATA($key) !== ""){
                if(GET_DATA($key) == $value){
                    echo "selected";
                }
            }
        }
    }
    ?>
    <form action="" method="get" class="news-filter-form">
        <label for="category">Category</label>
        <select name="category">
            <!--<option value="">All</option>-->
            <?php echo get_class_opt("category","<option value=''>All</option>",  GET_DATA("category")); ?>
        </select>
        
        <label for="visisibility">Visible</label>
        <select name="visibility">
            <option value="">All</option>
            <option value="0"  <?php GET_SELECTED("visibility", 0)?> >0</option>
            <option value="1"  <?php GET_SELECTED("visibility", 1)?> >1</option>
        </select>
        <label for="visisibility">News Paper</label>
        <select name="paper-id">
            <?php news_paper_list_options(GET_DATA('paper-id'), "<option value=''>All</option>") ?>
        </select>
        <?php show_day_month_year(); ?>
        <input type="submit" value="Filter" >
    </form>
        
    <?php
    if($n->get_total()) {

        ?>
        
        <table class="contet-list">
            <thead>
                <tr>
                    <td>News Title</td>
                    <td colspan="2">Action</td>
                    <td>Visibility</td>
                </tr>
            </thead>
        <?php
        $n->show_data(array('priority'=> "ASC", 'added_date'=>"DESC"));
        ?>
        </table>
        <?php
    }else {
?>
<h1 style="text-align: center; width: 100%; color: #BBB; margin: 200px 0 ">No News Found</h1>
<?php
    }
?>
<script>
    
    function _change_news_visibility(id, vis, ref){
        vis = parseInt(vis);
        
        var val = {'vis':vis, 'id':id};
        
        

        
        $.getJSON("<?php echo admin_url() ?>/ajax/change_visibility.php", val, function(e,s ){
    
            if(e.status){
                
                console.log(e.change_vis);
                if(e.change_vis == 0) {
                    $("."+ref).removeClass('visible');
                    $("."+ref).addClass('invisible');
                    $("."+ref).html("Invisible");
                }else {
                    $("."+ref).removeClass('invisible');
                    $("."+ref).html("Visible");
                    $("."+ref).addClass('visible');
                }
            }
        });
    }
    
</script>

<?php admin_page_include('includes/footer.php')?>