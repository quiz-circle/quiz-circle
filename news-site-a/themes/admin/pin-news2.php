<?php
header('Content-Type: text/html; charset=utf-8');
mb_internal_encoding('utf-8');
admin_page_include('includes/header.php');

function page_heading($key){
    switch ($key) {
        case "spcial_news_class":
            echo "Set Category For Spcial News";
            break;
        case "headlines_class":
            echo "Set Category For headlines";
            break;
        default :
            echo "Set Category For " . GET_DATA('meta-attibutes') . " column";
            break;
    }
}

function show_day_month_year($input = "post", $prefix = ""){
    $day = $prefix ? $prefix."_day":"day";
    $month = $prefix ? $prefix."_month":"month";
    $year = $prefix ? $prefix."_year":"year";
    ?>
    <label for="date">Date</label>
    <select name="<?php echo $day ?>" id="<?php echo $day ?>">
        <?php _option_range(1,31, "<option value=''>Day</option>", GET_DATA($day))?>
    </select>
    <select name="<?php echo $month ?>" id="<?php echo $month ?>">
        <?php _option_range(1,12, "<option value=''>Month</option>", GET_DATA($month))?>
    </select>
    <select name="<?php echo $year ?>" id="<?php echo $year ?>">
        <?php _option_range(2016,1980, "<option value=''>Year</option>", GET_DATA($year))?>
    </select>
    <?php
}

?>
    
<?php

    $init = "<option value=\"\">(select)</option>";
    $meta = load()->sys("MetaInfo")->run("site_info", "info_key", "info_value");
    $sidebar_meta = "home-sidebar";
    $seted = '';
    $class_key = "";
    
    if(GET_DATA("meta-attibutes")){
        $meta_attr = GET_DATA("meta-attibutes");
        if(!is_array(get_config('meta_attr/'.$meta_attr))){
            $seted = $meta->get(get_config('meta_attr/'.GET_DATA("meta-attibutes")));
            $class_key  = get_config('meta_attr/' . $meta_attr);
        }else {
            if( is_array(get_config('meta_attr/'.$sidebar_meta)) ){
                $meta_list = get_config('meta_attr/'.$sidebar_meta);
                if(array_key_exists( GET_DATA('meta-attibutes'), $meta_list)){
                    $seted = $meta->get(get_config('meta_attr/'.$sidebar_meta.'/'.GET_DATA("meta-attibutes")));
                    $class_key = get_config('meta_attr/'.$sidebar_meta.'/'.GET_DATA("meta-attibutes"));
                }else {
                    echo "d";
                    //redirect(admin_link());
                }   
            }
        }
    }else {
        echo "ok";
        //redirect(admin_link());
    }

    if(POST_DATA()){
        if(POST_DATA("meta-key")){
            $key = POST_DATA("meta-key");
            if($meta->is_exists($key)){
                $meta->edit($key, POST_DATA("category_url_ref"));
            }else {
                $meta->add($key, POST_DATA("category_url_ref"));
            }
        }
    }
    
    $class =  get_class_options_by_ref("category", $init, POST_DATA("category_url_ref", $seted));
    
    echo $seted;
    
?>
    
    <h2 class="main-body-page-header"><?php echo page_heading($class_key); ?></h2>
    
    <form action="" method="post" style="width: 300px">
        <input type="hidden" value="<?php echo $class_key;?>" name="meta-key">
        Category<br>                
        <select name="category_url_ref" required id="parent" style="width: 100%;">
            <?php echo ($class->get_total()) ? $class->show_data():$init; ?>
        </select><br><br>
        <input type="submit" value="Change" >
    </form>
    
<?php
    function make_news_config(){
        $conf = array();
        
        if(GET_DATA('category')){
            $conf['class_url_ref'] = urldecode(GET_DATA('category'));
            $conf['class-type'] =  "category";
        }

        $conf['visibility'] =  1;    
        $conf['content-type'] =  'news';
        
        return $conf;
    }
    
    $category_id = 0;
    
    if(GET_DATA('category')){
        $category_id = _get('class', 'url_ref', GET_DATA('category'), 'class_id');
    }
    
    function news_list_admin_home($each){
        $category_id = $GLOBALS['category_id'];
        $news_page = get_config("news-details/reference");;
        $tb = "<tr>";
            $tb .= "<td width='650'>";
            $tb .= "<a href='". base_url()."/".$news_page."/".$each['content_id']."' target='_blank'>";
            $tb .= $each['content_title'];
            $tb .= "</a>";
            $tb .= "</td>";
            
            $tb .= "<td style='text-align:center; width:180px'>";
            $identifier = md5($each["content_id"]);
            $pin_label = '';
            if((int)$each["priority"] != 99){
                $tb .= '<div ';
                $tb .=  'class="news-pined">Pined ('.$each["priority"].')';
                $tb .= "</div>";
                $pin_label = 'Unpin/Change Priority';
                $pin_label .= 'Unpin/Change Priority';
                $pin_label = '<a href="javascript:" class="pin-button"'; 
                $pin_label .= " onclick='_unpin_this(\"".$each["content_id"]."\", \"".$each["priority"]."\", \"".  $identifier."\", ".$category_id.")'"; 
                $pin_label .= '>Unpin</a> ';
                $pin_label .= '<a href="javascript:" ';
                $pin_label .= " onclick='_rearrange_priority(\"".$each["content_id"]."\", \"".$each["priority"]."\", \"".  $identifier."\", ".$category_id.")' >";
                $pin_label .= 'Change Priority</a>';
            }else {
                $tb .=  '<div class="news-unpined">Unpined';
                $tb .= "</div>";
                $pin_label = '<a href="javascript:" class="pin-button"'; 
                $pin_label .= " onclick='_pin_this(\"".$each["content_id"]."\", \"".$each["priority"]."\", \"".  $identifier."\", ".$category_id.")'"; 
                $pin_label .= '> Pin</a>';
            }
            
            $tb .= "</td>";
            $tb .= "<td>";
            $tb .= $pin_label;
            $tb .= "</a>";
            $tb .= "</td>";
        $tb .= "</tr>";
        
        return $tb;
    }
    
    $c = make_news_config();
    
    $c['content-type'] = !isset($c['content-type']) ? "news" : $c['content-type'];    
    $c['class_url_ref'] = $seted;
    $c['class-type'] =  "category";
    
    $n = news_list("news_list_admin_home", $c);
    
    $per_page = 10;
    $p_n = (int)($n->get_total() / $per_page);

    echo "<h2 class='main-body-page-header'>Pin a News <a href='". admin_link()."/add-news?action=add'>Add News</a></h2>";
    
    if($n->get_total() > $per_page){
        $n->set_pagination($per_page, "page");
        echo "<div class='pagination'>";
        echo $n->get_pagination();
        echo "</div>";
    }
    
    $pn = ($n->get_total()) ? GET_DATA("page", "1"):"0";
    echo "<p>Page " . $pn  . " of ".$n->total_pages()."</p>";
    
    ?>

    <?php
    
    //$n->show_data();
    
    if($n->get_total()) {

        ?>
        
        <table class="contet-list">
            <thead>
                <tr>
                    <td>News Title</td>
                    <td colspan="2">Pin</td>
                </tr>
            </thead>
        <?php
            $n->show_data(array('priority'=> "ASC", 'added_date'=>"DESC"));
        ?>
        </table>
        <?php
    }else {
?>
<h1 style="text-align: center; width: 100%; color: #BBB; margin: 200px 0 ">No News Found</h1>
<?php } ?>
<script>
    
    function _unpin_this(content_id, priority, identifier, category_id){
        var input = {
            'from' : parseInt(priority),
            'to': 0,
            'entity_id': parseInt(content_id),
            'class_id': category_id,
            'type':'unpin'
        }
        set_priority(input);
    }
    
    function _pin_this(content_id, priority, identifier, category_id){
        var input = {
            'from' : parseInt(priority),
            'to': 0,
            'entity_id': parseInt(content_id),
            'class_id': category_id,
            'type':'pin'
        }
        set_priority(input);
    }
    
    function _rearrange_priority(content_id, priority, identifier, category_id){
        var html = ""; 
        html += "<div style='text-align:center;display:inline-block;margin-bottom: 50px;width: 100%;margin-top: 10px;'>";
            html += "<select id='priority-list'>";
            html += "</select> ";
            html += "<input type='submit' value='Set' id='priority-operation-button'><br><br>";
        html += "</div>";
        dialogue_box("Set Priority", html);
        load_priority_in(category_id, "#priority-list", priority);
        
        $("#priority-operation-button").on('click', function(){
            var pr_value = parseInt($("#priority-list").val());;            
            var input = {
                'from' : parseInt(priority),
                'to': pr_value,
                'entity_id': parseInt(content_id),
                'class_id': category_id,
                'type':'rearrange'
            }    
            cons(input);

            set_priority(input);
        });
    }
    
    
    function load_priority_in(class_id, identifier, active_id){
        var parent = $a(identifier, 'load');
        parent.setValues(function(){
            this.values = {
                "active_pr" : active_id,
                "class_id" : class_id
            }
        });

        parent.success(function(){                
            
        });
        parent.setUrl(aj_path +"get_priorities.php");
        parent.run(); 
    }
    
    //cons(aj_path + "set_priority.php");
    
    function set_priority(input){
        $.ajax({
            dataType: "json",
            url: aj_path + "set_priority.php",
            data: input,
            success: data_success,
            error: data_error
        });
    }
    
    function data_success(res){
        if(res.status){
            cons(res);
            window.location = window.location;
        }
    }

    function data_error(e, a){
        console.log(a);
    }
    
    
</script>

<?php admin_page_include('includes/footer.php')?>