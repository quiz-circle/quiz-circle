<?php 
header('Content-Type: text/html; charset=utf-8');
mb_internal_encoding('utf-8');
admin_page_include('includes/header.php');
admin_page_include('includes/manage_nav_functions.php');


$cap = $GLOBALS['_get_cap'];

if($cap->edit_nav === false){
    redirect(admin_link());
}


$nav_name = "";

function nav_menu_list($row, $ac){
    $result =  "<li ".($ac == $row['class_id'] ? ' class=\'active\' ':' ').">";
    $result .= "<a href='".admin_link() ."/". admin_page_name()."?nav-id=" .$row['class_id']."'>" .
            ucwords(str_replace(array('-', '_'), array(' ', ' '), $row['name'])) . "</a>";
    $result .= "</li>";
    return $result;
}


$nav_menus = get_class_list("nav_menu_list",  "nav-menu", false, GET_DATA('nav-id'));

$n = get_nav_items("nav_menu_func", GET_DATA('nav-id'), "ul", array("class"=> "sort"));


$nav_name = _get('class', "class_id", GET_DATA('nav-id'), 'name');
$nav_reference = _get('class', "class_id", GET_DATA('nav-id'), 'url_ref');
$nav_name = ucwords(str_replace(array('-', '_'), array(' ', ' '), $nav_name));

?>

<style>
    .sort {
        padding-left: 15px;
        width: 350px;
        float: left;
        //position: relative;
        min-height: 8px;
        padding-top: 8px;
    }
    
    .sort li {
        width: 100%;
        float: left;
        padding: 0;
        margin: 0;
    }

    .sort li .container {
        padding:6px 5px;
        border: 1px solid #CCC;
        background-color: #E9E9E9;
        width: 100%;
        float: left;
        position: relative;
        min-height: 8px;
        cursor: move
    }
    
    .sort li .container .title {
        position: relative;
        width: 100%;
        float: left;
    }
    
    .sort li .container .title .name, 
    .sort li .container .title .item-type,
    .sort li .container .title .edit-button,
    .sort li .container .title .handle {
        display: inline-block;
        float: left;
        
    }
    
    .sort li .container .title .name{
        
    }
    
    .sort li .container .title .item-type{
        position: absolute;
        right: 5px;
        font-style: normal;
        color: #999;
        font-size: 14px;
    }
    
    .sort li .container .title .edit-button {
        position: absolute;
        right: 90px;
        display: none;
    }
    .sort li .container:hover > .title .edit-button {
        display: inline-block;
    }
    
    .sort li .container .title .handle {
        position: absolute;
        right: 5px;
        color: #555;
    }
    .sort li .container .item-editing {
        display: none;
        width: 100%;
        background-color: #F2F2F2;
        float: left;
        border: 1px solid #ccc;
        padding: 3px;
        margin: 5px 0;
    }
    
    .sort li .container .item-editing label {
        width: 100%;
        float: left;
        display: block;
        margin-bottom: 0;
        color: #444;
        font-weight: normal;
        font-size: 13px;
    }
    .sort li .container .item-editing input {
        width: 80%;
        float: left;
        display: block;
        padding: 2px 4px;
        margin-bottom: 6px;
    }
    
    #nav-list {
        //position: relative;
        float: left;
        padding-left: 8px;
    }
    
    .highlight {
         border: 2px dashed #999;
         background-color: #FFF;
         float: left;
         min-height: 35px;
         bottom: 8px;
         top: 8px;
         margin-top: 0;
         position: relative;
    }
     
    .highlight:after {
        height: 5px;
        float: left;
        width: 100%;
        content: "";
    }
    
    .mnav-header {
        float: left;
        height: 40px;
        width: 100%;
    }
    
    .mnav-header .inputs {
        float: left;
        display: inline;
        margin-right: 10px;
    }
    .mnav-header .inputs input[type='submit'] {
        padding: 4px 15px;
    }
    
    .mnav-left-section, .mnav-right-section {
        float: left;
        border: 1px solid #CCC;
        border-top: none;
        margin-top: 5px;
        min-height: 600px;
    }
    
    .mnav-left-section {
        width: 29.5%;
        margin-right: 0.5%;
        border:none;
    }

    .mnav-right-section {
        width: 70%;
    }
    
    .nav-list {
        width: 100%;
        float: left;
        border-top: 1px solid #ccc;
        border-bottom: 1px solid #ccc;
        margin: 0;
        padding: 0;
        overflow: hidden;
    }
    
    .nav-list ul {
        list-style: none;
        padding: 0;
        margin: 0 auto;
        height: 30px;
        text-align: center;
    }
    
    .nav-list ul li{
        display: inline;
        margin: 1px auto;
        //float: left;
        text-align: center;
        overflow: hidden;
    }
    
    .nav-list ul li a{
        padding: 5px 8px;
        text-decoration: none;
        color: #726666;
        background-color: #c4c4c4;
        float: left;
        border-right: 1px solid #FFF;
    }
    
    .nav-list ul li a:hover {
        background-color: #7bbc57;
        color: #fff;
    }
    
    .nav-list ul li.active a {
        color: #0b0a0a;
        background-color: #00e86e;
        border-bottom: none;
    }
    
    .nav-list ul li.active a:hover{
        
    }
    
    .section-head {
    width: 100%;
    background-color: #7bbc57;
    float: left;
    height: 35px;
    padding: 8px 9px;
    color: #1f0000;
        //line-height: 40px;
    }
    
    .section-head .right{
        display: inline;
        float: left;
    }
    
    .section-head .operation-status,.section-head .save-button {
        float: right;
        display: inline;
    }
    
    .section-head .operation-status  {
        margin-right: 10px;
        color: #103005;
    }
    .section-head .operation-status img {
        margin-top: -2px;
    }
    
    .section-head .save-button {
        margin-top: -5px;
    }
    .section-head .save-button input {
        padding: 2px 12px;
        color: #103005;
        background-color: #f2f2f2;
        border: 1px solid #888;
        border-radius: 2px;
        box-shadow: 1px 0px 0px 1px #103005;
    }
    
    #operation-box {
        border: 1px solid #CCC;
        float: left;
        width: 100%;
        margin-top: 10px;
    }
    
    .add-item-head {
        border-bottom: 1px solid #CCC;
        background-color: #EEE;
        float: left;
        width: 100%;
        padding: 5px 5px 3px;
    }
    
    .add-item-body {
        padding: 5px;
        position: relative;
        float: left;
        width: 100%;
    }
    .add-item-body .loading-overall{
        text-align: center;
        width: 100%;
        float: left;
        position: absolute;
        top: 0px;
        left: 0;
        min-height: 100%;
        vertical-align: middle;
        background-color: rgba(230, 230, 230, 0.6);
        display: block;
        z-index: 1000;
    }
    
    .add-item-body .loading-overall img {
        margin-top:  15%;
        vertical-align: middle;
        opacity: 0.3;
    }
    
    .add-item-body .add-to-buton {
        width: 100%;
        float: left;
        padding-bottom:0px;
        padding-top: 5px;
    }
    
    .add-item-body .add-to-buton input{
        padding: 3px 8px;
        display: inline-block;
    }
    .add-item-body .add-to-buton .status{
        padding: 3px 8px;
        display: inline-block;
    }
    
    .add-item-head .title {
        display: inline-block;
        float: left;
        padding-top: 2px;
        color: #444;
        font-weight: bold;
    }
    
    .add-item-head .search-label {
        display: inline-block;
        float: right;
        padding-top: 2px;
        margin-right: 5px;
        color: #444;
        font-weight: bold;
    }
    
    .add-item-head .search-content {
        display: inline-block;
        float: right;
        width: 130px;
        height: 25px;
        padding: 2px 3px;
        font-size: 12px;
        border-radius: 3px;
    }
    
    .add-item-body {
        //z-index: -5;
    }
    
    .add-item-body .containers {
        border: 1px solid #D5D5D5;
        min-height: 140px;
        max-height: 180px;
        float: left;
        width: 100%;
        position: relative;
        display: block;
        margin-top: -2px;
        z-index: 1;
        overflow-y: scroll;
    }
    
    
    .add-item-nav .fetch-content {
        padding: 0;
        margin: 0;
        float: left;
    }
    
    .add-item-nav {
        z-index: 2;
    }
    
    .add-item-nav .fetch-content li{
        display: inline-block;
        float: left;
        margin-right: 2px;
        border: 1px solid #D5D5D5;
        line-height: 1.2;
    }
    .add-item-nav .fetch-content  li a{
        background-color: #7bbc57;
        padding: 0 6px;
        color: #FFF;
    }
    .add-item-nav .fetch-content  li.active {
        border-bottom: none;
    }
    .add-item-nav .fetch-content  li.active a{
        background-color: #FFF;
        color: #000;
    }
    
    .add-item-body .label {
        width: 100%;
        float: left;
        margin-bottom: 0 !important;
    }
    .add-item-body input[type='text'] {
        width: 90%;
        font-size: 13px;
        padding: 3px 4px;
        margin: 0 0 10px 0
    }
    .content-container {
        padding: 5px;
        display: none;
    }
    
    .custom .content-container {
        display: block;
    }
    
    .content-container .fetched-content li {
        width: 100%;
        padding: 4px 6px 0; 
        width: 100%;
        float: left;
        position: relative;
        margin-bottom: 2px;
        border-top: #D5D5D5 1px solid ;
        border-bottom: 1px solid darkcyan;
        background-color: #F8F8F8;
    }
    
    .content-container .fetched-content li.active {
        background-color: #D6DFD2;
        border-bottom: 1px solid darkcyan;
    }
    
    .content-container .fetched-content li label {
        margin: 0;
        float: left;
        width: 100%;
        padding: 0px 4px 4px; 
        padding-left: 25px;
        font-weight: normal;
        margin-top: 0;
    }
    
    .content-container {
        
    }
    
    .content-container .fetched-content li input[type='checkbox'] {
        height: 20px;
        width: 20px;
        float: left;
        position: absolute;
        margin-top: 0;
    }
</style>

<div class="mnav-header">
    <form class="add-nav" action="" method="post">
        <div class="inputs">
            <label for="nav-name">Type A Name For The Nav</label>
            <input type="text" name="nav-name" id="nav-name">
        </div>
        <div class="inputs">
            <input type="submit" value="Add Nav">
        </div>
    </form>
</div>
<div class="nav-list">
    <?php if($nav_menus->get_total()){?>
    <ul>
        <?php
            
            echo $nav_menus->get_data();
        ?>
    </ul>
    <?php } ?>
</div>
<div class="nav-list" style="float: left; margin: 5px 0;border-top: none;">
    <em style="color: #888;"><span style="font-weight: bold">Navigation Reference &raquo; </span> <?php echo $nav_reference;?></em>
</div>
<div class="mnav-left-section">
    <div class="section-head">
        <input type="hidden" value="<?php echo md5(GET_DATA('nav-id')); ?>" id="<?php echo $nav_reference?>" class="_nav_ref">
        Add Item to <em><?php echo $nav_name; ?></em>
    </div>
    
    <div class="categories" id="operation-box">
        <div class="add-item-head">
            <span class="title">Categories</span>
            <input type="text" value="" name="search-content" class="search-content" id="categories">
            <label for="categories" class="search-label" >Search</label>
        </div>
        <div class="add-item-body">
            <div class="add-item-nav">
                <ul class="fetch-content">
                    <li class="active"><a href="javascript:" class="categories" id="all">All</a></li>
                    <li><a href="javascript:" class="categories" id="recent">Recent</a></li>
                </ul>    
            </div>
            <div class="containers">
                <div class="loading-overall">
                    <img src="<?php echo admin_url() ?>/images/rolling_big.gif">
                </div>
                <div class="content-container all"></div>
                <div class="content-container recent"></div>
            </div>
            <div class="add-to-buton" id="categories">
                <input type="submit" value="Add To This Menu">
                <div class="status"><img src="<?php echo admin_url() ?>/images/rolling.gif" > <span></span></div>
            </div>
        </div>
    </div>
    
    <div class="pages" id="operation-box">
        <div class="add-item-head">
            <span class="title">Pages</span>
            <input type="text" value="" name="search-content" class="search-content" id="pages">
            <label for="pages"  class="search-label">Search</label>
        </div>
        <div class="add-item-body">
            <div class="add-item-nav">
                <ul class="fetch-content">
                    <li><a href="javascript:" class="pages" id="all">All</a></li>
                    <li class="active"><a href="javascript:" class="pages" id="recent">Recent</a></li>
                </ul>    
            </div>
            <div class="containers">
                <div class="loading-overall">
                    <img src="<?php echo admin_url() ?>/images/rolling_big.gif">
                </div>
                <div class="content-container all">

                </div>
                <div class="content-container recent">

                </div>
            </div>
            <div class="add-to-buton" id="pages">                
                <input type="submit" value="Add To This Menu">
                <div class="status"><img src="<?php echo admin_url() ?>/images/rolling.gif" > <span>Adding..</span> </div>
            </div>
        </div>
    </div>
    
    <div class="custom"  id="operation-box">
        <div class="add-item-head">
            Custom
        </div>
        <div class="add-item-body">
            <div class="content-container custom">
                <label for="">Name</label>
                <input type="text" value="" class="custom-item" name="custom-item-name" id="custom-item-name" placeholder="Type a name for a item">
                <label for="">URL</label>
                <input type="text" value=""  class="custom-item" name="custom-item-link" id="custom-item-url" placeholder="type your link e.g. http://www.example.com">
            </div>
            <div class="add-to-buton" id="custom">
                <input type="submit" value="Add To This Menu">
                <div class="status"><img src="<?php echo admin_url() ?>/images/rolling.gif" ><span></span></div>
            </div>
        </div>
    </div>
</div>

<div class="mnav-right-section">
    <div class="section-head">
        <div class="right"><em><?php echo $nav_name; ?>'s</em> Items</div>
        <?php if($n->get_total()){?>
        <div class="save-button">
            <input type="button" id="lists" value="Save">
        </div>
        <div class="operation-status">
        </div>
        <?php } ?>
    </div>
    <div class="main-data">
    <?php
        if($n->get_total()){
            $n->show_data("priority");
        }
    ?>
    </div>
</div>





<script src="<?php echo admin_url()."/js/manage_nav.js"?>"></script>
<?php  admin_page_include('includes/footer.php')?>