<?php admin_page_include('includes/header.php'); ?>

<?php    
admin_page_include('includes/functions.php');
$row_identifier = unique_string();
?>
    <style>
        .main-wrapper {
            width: 800px;
            float: left;
        }
        .main-wrapper-heading{
            width: 100%;
            border-bottom: 1px solid #999;
            margin: 0 0 30px 0;
            float: left;
            padding:0 0 8px 0;
        }
        .main-wrapper-heading h2 {
            display: inline-block;
            float: left;
        }
        .main-wrapper-heading .operation {
            float: right;
        }
        
        .main-wrapper-heading .status,
        .main-wrapper-heading .save-overall {
            display: inline;
            float: right;
        }
        
        .main-wrapper-heading .status {
            padding-top: 8px;
            padding-right: 15px;
        }
        
        .main-container {
            width: 100%;
            float: left;
            position: relative;
            left: 0;
            top: 0;
            overflow-y: auto;
            overflow-x: hidden;
        }
        
        .data-row {
            width: 100%;
            float: left;
            height: 150px;
            border: 2px dashed #999;
            margin-bottom: 15px;
            background-color: #E0E8E0;
        }
        
        .data {
            width: 696px;
            float: left;
            height: 100%;
        }
        
        .data-item {
            float: left;
            margin: .7% 5px;
            border: 1px solid rgba(59, 255, 153, 0.35);
            height: 92%;
            background-color: #FFF;
            box-shadow: #045d7d 0 0 5px;
        }
        
        .data .two-col {
            width: 440px;
        }
        
        .data .one-col {
            width: 220px;
        }
        
        .add-data {
            width: 100px;
            color: #151515;
            font-size: 20px;
            font-weight: bold;
            text-shadow: 0 1px  0px #777; 
            text-align: center;
            height: 100%;
            float: right;
            background-color: #555;
            padding-top: 5.5%;
        }
        .add-data-inactive-button {
            color: #595959;
            text-shadow: 0 1px  0px #CCC; 
            background-color: #999;
        }
        
        .add-data:hover {
            color: #000;
        }
        
        .add-data-inactive-button:hover {
            color: #595959;
            text-shadow: 0 1px  0px #CCC; 
            background-color: #999;            
        }
        
        .add-row {
            width: 100%;
            color: #151515;
            font-size: 25px;
            font-weight: bold;
            text-shadow: 0 1px  0px #555; 
            text-align: center;
            height: 50px;
            float: right;
            background-color: #333;
            padding-top: 10px;
        }
        
        .add-row:hover {
            background-color:#005522;
            text-shadow: 0 -1px  0px #000011; 
            color: #DDD;
        }
        
        .data-item-in {
            text-align: center;
        }
        
        .data .head {
            font-size: 15px;
            width: 100%;
            height: 30px;
            background-color: #00AEED;
            text-align: center;
            padding: 5px;
            float: left;
            color: #000;
        }
        .data .column-type {
            float: left;
            width: 100%;
            margin: 10px 0;
        }
        
        .data .edit-section {
            width: 30%;
            margin: 25px 0;
            padding: 2px 3px;
            font-size: 14px;
        }
        
        .heading-bg-color {
            height: 15px;
            float: none;
        }
        input#heading_color {
            height: 15px;
            width: 50px;
            float: left;
            font-size: 12px;
        }
    </style>
    
<?php
    $initial_row_class = _unique_id();
    $initial_data_class = _unique_id();
    
    $sec_key = get_config("meta_attr/section_key"); //section key
    $meta = load()->sys("MetaInfo")->run("theme_info","info_key", "info_value");
    $section_info = $meta->get($sec_key);
    $informations = json_decode($section_info, true);
    $container_ids = array();
    $r_ids = array();
?>

    <form action="" method="post">
        <div class="main-wrapper">
            <input type="hidden" value="<?php echo $sec_key; ?>" name="reference_key" id="reference_key">
            <div class="main-wrapper-heading">
                <h2 class="main-body-page-header">Manage News Section</h2>
                <div class="operation">
                    <button class="save-overall" onclick="//return save_overall()">Save</button>
                    <div class="status"></div>
                </div>
            </div>
            <div class="main-container">
                <?php if(count($informations)): foreach ($informations as $information):$row_id = _unique_id(); $r_ids[] = $row_id;?>   
                    
                <div class="data-row <?php echo $row_id ?>" >
                    <div class="data">
                        <?php 
                        $button_class = "";
                        $col_count = 0;
                        if(count($information)):
                            
                        foreach ($information as $data):$data_id = _unique_id();
                        $container_ids[$row_id][] = $data_id;
                        
                        $head_bg_color = $data['head_bg_color'];
                        $head_color = $data['head_color'];
                        $column_size = $data['column_size'];
                        $col_count += (int) $column_size;
                        $class_id = $data['class_id'];
                        $name = _get('class', 'class_id', $class_id, 'name');
                        //Explain($data);
                        $col_class = "";
                        $col_label = "";
                        if($column_size == 1) {
                            $col_class = "one-col";
                            $col_label = "One Column";
                        }else if($column_size == 2){
                            $col_class = "two-col";
                            $col_label = "Two Column";
                        }
                        
                        ?>
                            
                        <div class="data-item <?php echo $data_id.' '.$col_class?>">
                            <input type="hidden" value="<?php echo $column_size; ?>"  name="number-of-column" id="number-of-column" />
                            <input type="hidden" value="<?php echo $row_id; ?>" name="parent_class" id="parent_class" />
                            <input type="hidden" value="<?php echo $head_color; ?>" name="head_color" id="head_color" />
                            <div class="data-item-in">
                                <div class='head' style="color: <?php echo $head_color; ?>; background-color: <?php echo $head_bg_color ?>">
                                    <?php echo $name?>
                                    <a href="javascript:_delete_data('<?php echo $row_id?>', '<?php echo $data_id?>')">X</a>
                                    <input type="hidden" value="<?php echo $class_id; ?>" name="category-id" id="category-id">
                                </div>
                                <div class="column-type">
                                    <strong>Column Type &raquo;</strong> <?php echo $col_label; ?>
                                </div>
                                <div class="heading-bg-color">
                                    Heading Color &raquo; <input type="text" style="width:80px; float:none; display:inline"  class="form-control" id="heading_color" value="<?php echo $head_bg_color ?>" data-color-format="hex">
                                </div>
                                <button class="edit-section" onclick="return __edit_data('<?php echo $data_id; ?>', <?php echo $class_id?>, <?php echo $column_size?>, '<?php echo $row_id; ?>')" >Change</button>
                            </div>
                        </div>
                        <?php 
                        endforeach;
                        endif;
                            if($col_count > 2) {
                                $button_class = "add-data-inactive-button";
                            }
                        ?>
                    </div>
                    <a href="javascript:__add_data('<?php echo $row_id ?>')" class="add-data <?php echo $button_class; ?>">+Add Section</a>
                </div>
                <?php endforeach; endif;?>
            </div>
            <a class="add-row" href="javascript:__add_row()">Add Row</a>
        </div> 
    </form>
    
    <script src="<?php echo admin_url()."/js/manage_section.js"?>"></script>
    
    <script>
        $(".main-container").sortable({ tolerance:"pointer", cursor:"move", 
            revert:true, opacity: 0.8,
            axis: "y",
            scroll : true, 
            handle: '.add-data',
        });
        var c_ids = ".<?php echo implode(" .data , .", $r_ids);?> .data ";
        $(".data-row .data").sortable({ tolerance:"pointer", cursor:"move", 
            revert:true, opacity: 0.8,
            axis: "x", 
            handle: '.head'
        });
        <?php foreach ($container_ids as $c_ids):?>
            <?php foreach ($c_ids as $c_id):?>
                color_picker($(".<?php echo $c_id?>").find("#heading_color"), function(c, color){

                    var head_bg_color = $(".<?php echo $c_id?>").find("#heading_color").val();
                    
                    console.log("<?php echo $c_id?>");

                    $(".<?php echo $c_id?>").find(".head").css("background-color", head_bg_color);
                    
                    if (color.cielch.l < 60) {
                        $(".<?php echo $c_id?>").find(".head").css("color", "#F1F1F1");
                        $(".<?php echo $c_id?>").find("#head_color").val("#F1F1F1");
                    }else {
                        $(".<?php echo $c_id?>").find(".head").css("color", "#333333");
                        $(".<?php echo $c_id?>").find("#head_color").val("#333333");
                    }
                });
            <?php endforeach;?>
        <?php endforeach;?>
        
        
    </script>
    
    
<?php admin_page_include('includes/footer.php')?>
