<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

load()->sys("ContentList4")->open();



$content = new ContentList("SELECT * FROM class NATURAL JOIN class_type WHERE type ='category' ORDER BY class_id", "class_id", "parent" );

//Explain($content);
$content->is_tree(true);

$content->each(function($row){
    if(!is_array($row)){
        $outer = array();
        $outer['html'] = "ul";
        $outer['attrs'] = array("class" => 'parent', "id"=> "parent-".$row);
        return $outer;
    }
    $re = "<li>";
    $re .= "<b>Element: </b>";
    $re .= $row['name'];
    $re .= "</li>";
    
    return $re;
});

$content->show_data();