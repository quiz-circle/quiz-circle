<?php
$next = urldecode(GET_DATA("next"));

$id = SEG_VARS(1);

if(!$id){
    $id = GET_DATA('id');
}


$ca = news_id_to_cats($id);


$news = load()->sys("Content")->run("content", "content_id", "id");

$news->set_redirect_path("./", "db");
$news->set_redirect_path("./list", "query");
$news->set_redirect_path($next, "del_success");

function success_func($content_id){
    global $ca;
    update_class_counter($ca);
}

//$news->set_failed_message("This Author Has one or many book!", "delete_cond");
$news->set_failed_message("Successfully deleted!", "del_success","success_func");
$news->set_failed_message("Error in deleteing news!", "del_db_error");

function before($con){
    $content = $con->get_first();

    $rel_delete = DB::run()->delete("class_relationship")->where('entity_id', $content['content_id'])->run_sql();    
    if($rel_delete->error()){
        return false;
    }else {
        return true;
    }
}

$news->set_condition("before");

$news->delete();
echo $news->get_message();

?>