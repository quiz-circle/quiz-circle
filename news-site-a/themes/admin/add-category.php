<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Home Book Library</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        
        <div>
            <?php
                require_once "themes/admin/functions.php";
                //echo "themes/admin/functions.php";
                //echo make_unique_ref($_GET['cat-name'], 'class');
            
                if(!isset($_GET['type'])){
                    //redirect($path);
                }
                if(isset($_GET) && !empty($_GET)){
                    $name = trim($_GET['cat-name']);
                    $parent = $_GET['parent'];
                    
                    if(!has_cat_exists($name, $parent)){
                        
                        $url_ref = make_unique_ref($name, 'class');
                        
                        $sql = "INSERT INTO class";
                        $sql .= "( name, url_ref)";
                        $sql .= " VALUES('$name', '$url_ref')";
                        $cl_insert = DB::run()->setSql($sql)->run_sql();
                        
                        
                        if(!$cl_insert->error()){
                            $insert_id = $cl_insert->last_insert_id();

                            $sql = "INSERT INTO class_type";
                            $sql .= "( class_id, type, parent)";
                            $sql .= " VALUES($insert_id, 'category', $parent)";
                            $ct_insert = DB::run()->setSql($sql)->run_sql();
                            
                            if(!$ct_insert->error()){
                                echo "Category successfully added!";
                            }else {
                                echo "Not success!<br>";
                                echo $cl_insert->sql_error();
                            }
                        }else {
                            echo "Failed to add category!<br>";
                            echo $cl_insert->sql_error();
                        }
                        
                    }else {
                        echo "This category is exists with parent ". $parent;                        
                    }
                    
                }
                
                $sql = "select * from class ";
                $sql .= "natural join class_type ";
                $sql .= "where type='category'";
                $cats = DB::run()->setSql($sql)->run_sql();
            ?>
            
            <form action="" method="get">
                <h2 class="main-body-page-header">Add a Category</h2>
                Type a name:<br>
                <input type="text" name="cat-name" required><br><br>
                Parent:<br>
                <select name="parent" required>
                    <option value="0">(no parent)</option>    
                    <?php if($cats->get_count()):?>
                    <?php foreach($cats->get_array() as $cat):?>
                    
                    <option value="<?php echo $cat['class_id']?>"><?php echo $cat['name']?></option>
                    
                    <?php endforeach;?>
                    <?php endif; ?>
                </select><br><br>
                <input type="submit" value="Add" >
            </form>
        </div>
    </body>
</html>
