<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
$config['cap']['add_news'] = 'an';
$config['cap']['edit_news'] = 'en';
$config['cap']['add_category'] = 'ac';
$config['cap']['edit_category'] = 'ec';
$config['cap']['add_nav'] = 'anv';
$config['cap']['edit_nav'] = 'env';
$config['cap']['edit_section'] = 'es';
$config['cap']['edit_theme'] = 'et';
$config['cap']['edit_setting'] = 'es';
$config['cap']['add_page'] = 'ap';
$config['cap']['edit_page'] = 'ep';
$config['cap']['submit_news'] = 'sun';
$config['cap']['approve_news'] = 'an';
$config['cap']['edit_self_user_setting'] = 'esus';
 */

global $user;

$cap_conf = get_config('cap');

$cap = new Capabilities($user->getID());

if($cap->has('add_news')){
    echo _green_signal("Yes");
}else {
    echo _red_alert("No");
}




/*
function _alert($string){
    echo  "<script>alert('" . $string . "')</script>";
}

function _red_alert($string){
    echo "<span style='color:red'>" . $string . "</span>";
}
function _green_signal($string){
    echo "<span style='color:green'>" . $string . "</span>";
}
*/


/*
class Capabilities extends User{
    private $_cap_data, $_user_id;
    public  $user_capabilities_key, 
            $cap_conf;
    public function __construct($user_id = ""){
        parent::__construct();
        if($this->isLoggedIn()){
            $this->_user_id = $user_id === "" ? $this->getID() : $user_id;
           
            _red_alert($this->_user_id);
            
            load()->sys("UserInfo")->open();
            
            $this->cap_conf = get_config("cap");
            $this->_user_info = new UserInfo($this->_user_id);
            $this->user_capabilities_key = get_config("meta_attr/usr/cap");
            $this->_cap_data = json_decode( $this->_user_info->get($this->user_capabilities_key), true );
            
            Explain($this->_cap_data);
        }
    }
    
    public function has($key){
        echo $key;
        _br();
        Explain($this->cap_conf[$key]);
        if(!parent::isAdmin()){
            if(count($this->_cap_data)){
                if(array_key_exists(md5($this->cap_conf[$key]), $this->_cap_data)){
                    $u_type = $this->_user_info->get('user_type');
                    $nca = $this->_cap_data[md5($this->cap_conf[$key])]['a'];
                    $un = $this->_cap_data[md5($this->cap_conf[$key])]['a']['u'];
                    $ut = isset($this->_cap_data[md5($this->cap_conf[$key])]['a']['e']) ? $this->_cap_data[md5($this->cap_conf[$key])]['a']['e']:'';
                    $ncb = $this->_cap_data[md5($this->cap_conf[$key])]['b'];
                    return (md5(count($nca)) == $ncb && md5($this->_user_id) == $un && md5("yes+no".$u_type) == $ut);
                }
                return false;
            }
            return false;
        }
        return true;
    }
    
    public function add($key, $cap = ""){
        if(parent::isAdmin()){    
            if(!$this->isAdmin()){
                if(array_key_exists($key, $this->cap_conf)){
                    $this->_cap_data[md5($this->cap_conf[$key])] = array('a'=> $this->_cap_values($cap, $c), 'b'=> md5($c));
                    if( !$this->_user_info->exists($this->user_capabilities_key) ){
                        $this->_user_info->add($this->user_capabilities_key, json_encode($this->_cap_data));
                    }else {
                        $this->_user_info->edit($this->user_capabilities_key, json_encode($this->_cap_data));
                    }
                }
            }else {
                if($cap === "") $this->_cap_data[md5($this->cap_conf[$key])] = array('a'=> $this->_cap_values(0, $c), 'b'=> md5($c));
                
                if( !$this->_user_info->exists($this->user_capabilities_key) ){
                    $this->_user_info->add($this->user_capabilities_key, json_encode($this->_cap_data));
                }else {
                    $this->_user_info->edit($this->user_capabilities_key, json_encode($this->_cap_data));
                }
                _alert("Success!");
            }
        }else {
            echo  "<script>alert('Sorry you are not admin. Only admin can change capabilities!')</script>";
        }
        
    }
    
    public function isAdmin() {
        return (strtoupper($this->_user_info->get('user_type')) === "ADMIN");
    }
    
    public function edit($key, $cap ){
        if(parent::isAdmin()){
            if(!$this->isAdmin()){
                if(count($this->_cap_data)){
                    if(array_key_exists(md5($this->cap_conf[$key]), $this->_cap_data)){
                        $this->_cap_data[md5($this->cap_conf[$key])] = array('a'=> $this->_cap_values($cap, $c), 'b'=> md5($c));
                        $this->_user_info->edit($this->user_capabilities_key, json_encode($this->_cap_data));
                    }
                }
            }else {
                _alert("Admin capable in everything!");
            }
        }else {
            echo  "<script>alert('Sorry you are not admin. Only admin can change capabilities!')</script>";
        }
        
    } 
    
    private function _cap_values($cap, &$c){
        $array = array();
        $u_type = $this->_user_info->get('user_type');
        $c = rand(3,5);
        $id = rand(10, 99);
        $array['c'] = $c;
        
        if(is_int($cap)){
            if($cap === 0){
                if ($cap === "yes") $array['e'] = md5("yes+no" . $u_type);
            }
        }elseif ($cap === "yes") $array['e'] = md5("yes+no" . $u_type);
        
        if($c > 3){
            switch($c){
                case 4:
                    $array['m'] = rand(100, 120);
                    break;
                case 5:
                    $array['m'] = rand(100, 120);
                    $array['n'] = rand(121, 140);
                    break;
            }
        }
        $array['u'] = md5($this->_user_id);
        return $array;
    }
    
    public function make_empty(){
        
    }
}
*/

