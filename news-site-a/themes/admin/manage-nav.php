<?php
header('Content-Type: text/html; charset=utf-8');
mb_internal_encoding('utf-8');
admin_page_include('includes/header.php');
$cap = $GLOBALS['_get_cap'];
$nav_menus = get_class_options("nav-menu",  "<option value=''>(select)</option>", GET_DATA('menu-id'));
if($cap->edit_nav === false){
    redirect(admin_link());
}

?>

<style>
    .manage-nav ul{
        width: 100%;
        float: left;
        list-style-type: none;
        
        //padding-left: 2px;
    }
    .manage-nav ul li {
        display: block;
        float: left;
        margin-top: 15px;
        width: 100%;border-left: 1px solid #EEE;
        position: relative;
    }
    .manage-nav ul li .nav-item {
        position: relative;
        border: 1px solid #CCC;
        float: left;
        background-color: #EEE;
        padding: 10px 12px;
        width: 580px;
        font-size: 13px;
    }
    
    .manage-nav ul li .nav-item input,
    .manage-nav ul li .nav-item select{
        height: 25px;
        margin-right:5px;
        float: left;
    }
    .manage-nav ul li .nav-item label {
        float: left;
        line-height: 25px;
    }
    
    .manage-nav ul li .nav-item input[type="submit"] {
        padding: 4px 15px;
        float: left;
        display: block;
    }
    
    .show-opt-btn {
        float: left;
        display: block;
        line-height: 23px;
        margin-right: 5px;
    }
    
    .nav-item .edit-item {
        margin-bottom: 5px;
        padding-bottom: 5px;
    }
    .nav-item .add-sub-item {
        border-top: #CCC 1px solid;
        display: none;
        margin-top: 23px;    
    }
    
    .opt-sec {
        display: none;
        border: 1px solid #FFF;
        //background-color: cornflowerblue;
        width: 100%;
        float: left;
        padding: 12px 15px;
    }
    .opt-sec label {
        width: 100%;
        margin-top: 5px;
        font-size: 13px;
        line-height: 25px;
    }
    
    
    .add-child-btn {
        width: 100%;
        float: left;
        font-size: 13px;
        margin-top: 10px;
    }
    
    .main-sec {
        width: 100%;
        float: left;
        border: 1px solid #FFF;
    }
    
    .del-btn {
        position: absolute; 
        right: 15px;
        top: 11px;
        float: left;
        height: 25px;
        width: 25px;
        background-image:url("../themes/admin/images/remove.png");
        font-size: 0;
        text-indent: -5000px;
    }
    
    .del-btn:hover {
        background-position: 25px;
    }
    .link-url {
        width: 25px;
        height: 25px;
    }
    .show-opt-btn {
        width: 25px;
        height: 25px;
        text-indent: -5000px;
        font-size: 0;
        background-image:url("../themes/admin/images/show-advenced.png");
    }
    
</style>

<div class="edit-menu">
    <div class="select-menu">
        <form action="" method="get">
            <select name="menu-id">
                <?php echo $nav_menus->show_data() ?>
            </select>
            <input type="submit" value="Open Menu">
        </form>
    </div>
    <div class="manage-nav">
        <?php
        
        echo load()->sys("session")->run()->flush("_delete_nav_mess");
        
        function total_nav($class_id, $parent_id){
            
             $where = array(
                "class_id" => $class_id, 
                "parent_id" => $parent_id, 
            );
            $pr = DB::run()->read("nav_item")->where($where)->run_sql();
            return (int) $pr->get_count();
        }
        
        function priorities_options($class_id, $parent_id, $selected = null){
            $where = array(
                "class_id" => $class_id, 
                "parent_id" => $parent_id, 
            );

            $pr = DB::run()->read("nav_item","item_id, priority")->where($where)->order_by("priority")->run_sql();

            if($pr->get_count()){
                $pr_list = $pr->get_array();

                $res = "";
                foreach($pr_list as $pr){
                    $sel = ($pr['priority'] == $selected) ? 'selected':'';
                    $res .= "<option value='".$pr['priority']."' ".$sel. ">".$pr['priority']."</option>";
                }

                if(!$selected){
                    $last = (int) $pr_list[count($pr_list)-1]['priority'] + 1;
                    $res .= "<option value='".$last."' selected>".$last."</option>";
                }
                return  $res;
            }else {
                return "<option value='1'>1</option>";
            }
        }
        
        $action_types = array("add-item", "edit-item", "add-sub-item");
        function get_type($type, $id){
            global $action_types;            
            foreach ($action_types as $t){
                if(md5($t.trim($id)) == $type){
                    return $t;
                }
            }
            return false;
        }
        
        function get_nav_items($function, $class_id, $html){
            $nav = load()->lib("NavList")->run($class_id);
            $nav->set_html($html, true);
            $nav->set_is_tree(true);
            $nav->set_elemets_oper($function);
            $nav->show_data("priority");
        }
        
        function delete_item_url(){
            $delete_key = "delete_item";
            
            $split = explode("&", full_url());
            
            
            
            urldecode(full_url()) .'&delete_item='.$each['item_id'];
        }
        
        function manage_nav($each){ 
            $ord = (int) $each['order'];
            $ord++;
            $res = "";
            $res .= "<div class='nav-item'>";
            
            $res .= "<div class='edit-item'>";
            
            $res .= "<form action='' method='post'>";
            $res .= "<div class='main-sec'>";
            $res .= "<input type='hidden' name='item_id' value='".$each['item_id']."' required>";
            $res .= "<input type='hidden' name='parent_id' value='".$each['parent_id']."' required>";
            $res .= "<input type='hidden' name='type' value='".  md5("edit-item".$each['item_id'])."' required>";
            $res .= "<input type='hidden' name='order' value='".  $ord."' required >";
            $res .= "<input type='text' name='item_title' value='".$each['item_title']."' required>";
            $res .= "<label class='link-url'>URL </label> <input type='text'  name='ref_url' value='".$each['ref_url']."'>";
            $res .= "<a href='javascript:' class='show-opt-btn' title='Show More Options' id='" . md5($each['item_id'].'opt') . "'>More</a>";
            $res .= "<input type='submit' value='Save' name='".$each['item_id']."'>";
            
            
            $res .= (!has_child_nav($each['item_id'])) ? "<a class='del-btn' title='Delete This item' onclick='return confirm(\"Are You Sure Want To delete this item?\")' href='delete-nav-item?id=".$each['item_id']."&next=".  full_url()."'>X</a>":"";
            
            
            $res .= "</div>";
            
            $res .= "<div class='opt-sec ".md5($each['item_id'].'opt')."'>";
            $res .= '<label>Pos:</label><select name= "priority">';
            $res .= priorities_options(GET_DATA("menu-id"), $each['parent_id'], $each['priority']);
            
            $res .= '</select><br>';
            $res .= '<label>Target </label><select name= "link_target">';
            $res .= '<option value=""> &lt;not set&gt;</option>';
            $res .= '<option value="frame" ' . ($each['link_target'] =='frame' ? 'selected':'' ) . ' > &lt;frame&gt;</option>';
            $res .= '<option value="popup" ' . ($each['link_target'] =='popup' ? 'selected':'' ) . '> &lt;popup window&gt;</option>';
            $res .= '<option value="_blank" ' . ($each['link_target'] =='_blank' ? 'selected':'' ) . '> New Window (_blank)</option>';
            $res .= '<option value="_top" ' . ($each['link_target'] =='_top' ? 'selected':'' ) . '> Topmost Window (_top)</option>';
            $res .= '<option value="_self" ' . ($each['link_target'] =='_self' ? 'selected':'' ) . '> Same Window (_self)</option>';
            $res .= '<option value="_parent" ' . ($each['link_target'] =='_parent' ? 'selected':'' ) . '> Parent Window (_parent)</option>';
            $res .= '</select>';
            $res .= "<label>Stylesheet Class </label><input type='text' value='".$each['li_class']."'  name='class_name' value=''>";
            $res .= "</div>";
            $res .= "<a href='javascript:' class='add-child-btn' id='".  md5($each['item_id']) . "'>Add Child Element</a>";
            //End of opt-sec
            $res .= "</form>";
            //End of Form
            $res .= "</div>";
            //End of Edit item
            $res .= "<div class='add-sub-item ".  md5($each['item_id']) . "'>";
            
            $res .= "<form action='' method='post'>";
            $res .= "<input type='hidden' name='item_id' value='".$each['item_id']."' required>";
            $res .= "<input type='hidden' name='parent_id' value='".$each['parent_id']."' required>";
            $res .= "<input type='hidden' name='type' value='".  md5("add-sub-item".$each['item_id'])."' required>";
            $res .= "<input type='hidden' name='order' value='".  $ord."' required>";
            $res .= "Name: <input type='text' name='item_title' value='' required>";
            $res .= "Url: <input type='text'  name='ref_url' value='' >";
            $res .= 'Pos:<select name= "priority">';
            $res .= priorities_options(GET_DATA("menu-id"), $each['item_id']);
            $res .= '</select>';
            $res .= "<input type='submit' value='Add Sub Item' name='".$each['item_id']."'>";
            $res .= "</form>";
            
            $res .= "</div>";
            $res .= "</div>";
            return $res;
        }
        if(GET_DATA("menu-id")){
            $id = GET_DATA("menu-id");
            if(POST_DATA()){

                if(POST_DATA("item_id")){
                    $adding_id = (int) POST_DATA('item_id');
                }else {
                    $adding_id = (int) POST_DATA('class_id');
                }
                $type = get_type(POST_DATA('type'), $adding_id);
                $item_id = (int) POST_DATA("item_id");
                $parent_id = (int) POST_DATA("parent_id");
                $priority = (int) POST_DATA("priority");
                
                $values = array();
                $values['parent_id'] = $parent_id;
                
                $values['class_id'] = $id;
                $values['item_title'] = array(POST_DATA("item_title"), true);
                
                $values['ref_url'] = POST_DATA("ref_url");
                
                switch($type){
                    case $action_types[0]:
                    case $action_types[2]:
                        
                        //$ord = (int) POST_DATA("order");
                        $values['order'] = (int) POST_DATA("order");
                        $values['parent_id'] = (int) POST_DATA("item_id");
                        $values['priority'] = total_nav($id, $values['parent_id']) + 1;

                        $db = DB::run()->write("nav_item")->values($values)->run_sql();
                        echo $db->sql_error()."<br>";
                        $item_id = $db->last_insert_id();
                        
                        echo  "ITEM ID: ".POST_DATA("item_id");
                        
                        rearange_item($item_id, $priority, $values['parent_id']);
                        break;
                    case $action_types[1]:
                        $values['link_target'] = POST_DATA('link_target');
                        $values['li_class'] = POST_DATA('class_name');
                        $db = DB::run()->edit("nav_item")->values($values)->where( array('class_id'=> $id, 'item_id' => $item_id) )->run_sql();
                        rearange_item($item_id, $priority, $parent_id);
                        break;
                }
                if($db->error()){
                    echo $db->sql_error();
                }else {
                    echo $db->message();
                }
                echo $type;
            }
            get_nav_items("manage_nav", $id, "ul");
        
            
        ?>
        <form action='' method='post'>
            <h3>Add an Item</h3>
            <input type='hidden' name='parent_id' value='0' required >
            <input type='hidden' name='item_id' value='0' required >
            <input type='hidden' name='class_id' value='<?php echo $id?>' required >
            <input type='hidden' name='type' value='<?php echo md5("add-item".$id)?>' required>
            <input type='hidden' name='order' value='0' required>
            Name: <input type='text' name='item_title' value='' required>
            Url: <input type='text'  name='ref_url' value='' >
            Pos: 
            <select name="priority">
                <?php echo priorities_options(GET_DATA('menu-id'), 0);?>
            </select>
            <input type='submit' value='Add' name=''>
            </form>
        <?php } ?>
    </div>
    
    <script>
        /*
         *
         *.nav-item .add-sub-item {
        border-top: #A8116C 1px solid;
        display: none;
        }
        .add-child-btn
        */
       
        function show_hide_box(ref){
            $("." + ref).each(function(){
                $(this).on("click",function(){
                    var cl =  $(this).attr("id");
                    var status = $("."+cl).css("display");
                    console.log(status);

                    if(status == "none"){
                        $('.'+cl).css("display", "block");
                    }else {
                        $('.'+cl).css("display", "none");
                    }
                });
            });
        }
        
        show_hide_box("add-child-btn");
        show_hide_box("show-opt-btn");
        
    </script>
</div>


<?php admin_page_include('includes/footer.php')?>
