<?php
$type = urldecode(GET_DATA("type"));
$next = urldecode(GET_DATA("next"));

$news = load()->sys("Content")->run("class", "class_id", "id");

$news->set_redirect_path("./", "db");
$news->set_redirect_path("./", "query");

if($news){
    $news->set_redirect_path($next, "del_success");
}

function success_func(){
    global $type;
    load()->sys("session")->run()->add("_delete_class", ucfirst($type)." Deleted successfully!");    
}
$news->set_failed_message("Successfully deleted!", "del_success", 'success_func');

$news->set_failed_message("Error in deleteing news!", "del_db_error");





function before($con){
    $class = $con->get_first();
    global $next;
    if(has_entity($class['class_id'])){
        $message = "Can't Be deleted! There is some entities on <strong><em>".$class['name']."</em></strong>!";
    }else {
        if(has_child_class($class['class_id'])){
            $message = "Can't Be deleted! There is some childs on <strong><em>".$class['name']."</em></strong> Category!";
        }else {
            $ct = DB::run()->delete("class_type")->where("class_id", $class["class_id"])->run_sql();
            if($ct->error()){
                load()->sys("session")->run()->add("_delete_class", "Unexpected error!");
                redirect($next);
            }else {
                return true;
            }
        }
    }
    load()->sys("session")->run()->add("_delete_class", $message);
    redirect($next);
}

$news->set_condition("before");

$news->delete();
echo $news->get_message();

?>