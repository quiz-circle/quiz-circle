<style>
    #news-visibility {
        padding: 4px 9px;
        font-size: 13px;
        letter-spacing: -0.1px;
        font-family: verdana;
        font-weight: bold;
    }
    
    .visible {
        color: #FFF !important;
        background-color: #2A970C;
        border: 1px solid green;
        text-decoration: none;
    }
    
    .invisible-n {
        text-decoration: none;
        border: 1px solid #CCC;
        background-color: #EEE;
        color: #888 !important;
    }
    
    .visible:hover ,.invisible-n:hover {
        text-decoration: none;
    }
    
    .contentCheckBox input[type='checkbox'] {
        width: 20px;
        height: 20px;
        border: none;
        outline:  none;
        transform: scale(1, 1);
    }
   
    
</style>
<?php
header('Content-Type: text/html; charset=utf-8');
mb_internal_encoding('utf-8');
admin_page_include('includes/header.php');
admin_page_include('includes/list_functions.php');


?>
    
<?php
    function show_classes($each, $type = 'category'){
        $sql = "SELECT * FROM class c natural join class_relationship cr join class_type ct WHERE cr.entity_id = " . $each['content_id']; 
        $sql .= " AND ct.type = '" . $type . "' GROUP BY c.name";
        
        $cl = DB::run()->setSql($sql)->run_sql();
        
        if($index = (int) $cl->get_count()){
            $result = "";
            $class = $cl->get_array();
            while($index > 0){
                $index--;
                $result .= $class[$index]['name'];
                $result .= $index > 0 ? ", ":"";
            }
            return $result;
        }else {
            //return $cl->sql_error();
            return "<em>No ". ucfirst($type) ."</em>";
        }
    }
    
    $c = make_news_config();
    
    $c['content-type'] = !isset($c['content-type']) ? "news":$c['content-type'];
    
    $n = news_list("news_list_admin_home", $c);
    
    $per_page = 10;
    
    $p_n = (int)($n->get_total() / $per_page);
    
    $n->set_pagination($per_page, "page");
    
    echo "<h2 class='main-body-page-header'>Showing News <a href='". admin_link()."/add-news?action=add'>Add News</a></h2>";
    
    if($n->get_total() > $per_page){
        echo "<div class='pagination'>";
        echo $n->get_pagination();
        echo "</div>";
    }
    $pn = ($n->get_total()) ? GET_DATA("page", "1"):"0";
    echo "<p>Page " . $pn  . " of ".$n->total_pages()."</p>";
    

    
    
    if(POST_DATA()){
        $action = POST_DATA('action');
        $ids = POST_DATA('CheckedContent');
        if(!empty($action)){
            if($index = count($ids)){
                $whereClause = " content_id IN(";
                while ($index > 0){
                    $index--;
                    $whereClause .= $ids[$index]. ' ';
                    $whereClause .= $index !== 0 ? ',' : '';
                }
                
                $whereClause .= ')';
                $sql = "";
                switch ($action){
                    case "approve":
                        $sql .= make_approved_list_sql($ids);
                        break;
                    case "trash":
                        $sql .= make_trash_list_sql($ids);
                        break;
                    default :
                        $sql .= "UPDATE content ";
                        $sql .= " SET status = '" . strtolower($action) . "'";
                }
                
                
                if($sql !== ""){
                    $sql .= "WHERE ".$whereClause;
                    $status_update = DB::run()->setSql($sql)->run_sql();
                    if(!$status_update->error()){
                        show_main_message("News  status Changed!");
                    }else {
                        show_main_message($status_update->get_sql()."<br>". $status_update->sql_error());
                    }
                }
            }
        }
    }

    
    
    ?>
    <form action="" method="get" class="news-filter-form">
        <label for="category">Category</label>
        <select name="category">
            <!--<option value="">All</option>-->
            <?php echo get_class_opt("category","<option value=''>All</option>",  GET_DATA("category")); ?>
        </select>
        
        <label for="visisibility">Visible</label>
        <select name="visibility">
            <option value="">All</option>
            <option value="0"  <?php GET_SELECTED("visibility", 0)?> >0</option>
            <option value="1"  <?php GET_SELECTED("visibility", 1)?> >1</option>
        </select>
        <label for="visisibility">News Paper</label>
        <select name="paper-id">
            <?php news_paper_list_options(GET_DATA('paper-id'), "<option value=''>All</option>") ?>
        </select>
        <?php show_day_month_year(); ?>
        <input type="submit" value="Filter" >
    </form>
        
    <?php
    if($n->get_total()) {
        ?>
        <form action="" method="post">
            <div class="action-bar" style="margin-bottom:8px; margin-top: 15px; width: 100%; float: left; ">
                <label for="action">Action</label>
                <select id="action" name="action">
                    <option value="">(No Action)</option>
                    <option value="approve">Approve</option>
                    <option value="publish">Publish</option>
                    <option value="draft">Draft</option>
                    <option value="trash">Trash</option>
                </select>
                <input type="submit" value="Done" style="padding:3px 10px;">
                <a href="<?php echo admin_link()."/news-trash-list"; ?>">Trash</a>
            </div>
            <table class="contet-list">
                <thead>
                    <tr>
                        <td class="contentCheckBox">
                            <input type="checkbox" name="AllSelect" class="allSelect" >
                        </td>
                        <td>News Title</td>
                        <td>Categories</td>
                        <td>News Author</td>
                        <td>Visibility</td>
                    </tr>
                </thead>
            <?php
            $n->show_data(array('priority'=> "ASC", 'added_date'=>"ASC"));
            ?>
            </table>
        </form>

<style>
    .news-row .news-content {
        display: table-cell;
        position: relative;
        border: none;
    }
    
    .news-row:hover {
        background-color: #dfffdf;
    }
    .news-row:hover > .news-content .details-edit,
    .news-row:hover > .news-content .short-edit{
        display: block;
    }
    
    .news-row .news-content td {
        padding: 16px;
    }
    
    .news-row .news-content .details-edit {
        display: none;
        position: absolute;
        bottom: 0px;
        left: 80px;
        font-size: 12px;
        color: #00AAE8;
        font-family: cursive, verdana, sans-serif;
        //text-transform: uppercase;
    }
    
    .news-row .news-content .short-edit {
        display: none;
        position: absolute;
        left: 8px;
        bottom: 0px;
        font-size: 12px;
        color: #00AAE8;
        font-family: cursive, verdana, sans-serif;
        //text-transform: uppercase;
    }
    
</style>
        <?php
    }else {
?>
<h1 style="text-align: center; width: 100%; color: #BBB; margin: 200px 0 ">No News Found</h1>
<?php
    }
    
    function news_list_admin_home($each){
        $news_page = get_config("news-details/reference");;
        $tb = "<tr id='".md5($each['content_id'])."' class='news-row'>";
            $tb .= "<input type='hidden' value='".$each['content_title']."' style='display:none' class='title' >";
            $tb .= "<input type='hidden' value='".$each['content_subtitle']."' style='display:none' class='spcial_line' >";
            $tb .= "<input type='hidden' value='".$each['url_ref']."' style='display:none' class='url_ref' >";
            $tb .= "<input type='hidden' value='".$each['password']."' style='display:none' class='password' >";
            $tb .= "<input type='hidden' value='".$each['status']."' style='display:none' class='status' >";
            $tb .= "<input type='hidden' value='".$each['added_date']."' style='display:none' class='date' >";
            $tb .= "<td width='40' class='contentCheckBox' style='text-align:center'>";
                $tb .= "<input type='checkBox' name='CheckedContent[ ]' value='".$each['content_id']."' >";
            $tb .= "</td>";
            $tb .= "<td width='500' class='news-content'>";
            $tb .= "<a href='". base_url()."/".$news_page."/".$each['content_id']."' target='_blank' class='content-title'>";
            $tb .= $each['content_title'];
            $tb .= "</a>";
                $tb .= "<a href='' class='short-edit' id='".md5($each['content_id'])."'>Fast Edit</a>";
                $tb .= edit_button($each);
            $tb .= "</td>";
            $tb .= "<td class='classes'>";
            $tb .= show_classes($each);
            $tb .= "</td>";
            $tb .= "<td class='author' style='text-align:center'>";
            $tb .= _get('user', 'username', $each['author'], 'display_name');
            $tb .= "</td>";
            $tb .= "<td class='visible-status' style='text-align:center; width:100px'>";
            $identifier = md5($each["content_id"]);
            $tb .= "<a id='news-visibility' class='". $identifier ." ".((int)$each["visibility"] ? 'visible':'invisible-n')."' ";
            $tb .= " href='javascript:' onclick='_change_news_visibility(\"".$each["content_id"]."\", \"".$each["visibility"]."\", \"".  $identifier."\")' >";
            $tb .= (int)$each["visibility"] ? 'Visible':'Invisible';
            $tb .= "</a>";
            $tb .= "</td>";
        $tb .= "</tr>";
        
        return $tb;
    }
    
?>
<script>
    $(".news-content .short-edit").each(function(){
        $(this).on("click", function(e){
            e.preventDefault();
            var targetID = e.target.id;
            var target = $("tr#"+targetID); 
            //target.find('.classes').find('.author').find('visible-status').css('background-color', 'red');
            target.find('.classes').css('display', 'none');
            //target.find('.author').css('display', 'none');
            //target.find('.visible-status').css('display', 'none');
            target.find('.news-content').attr('colspan', '2');
            $("tr#"+targetID).css("height", "200px");
            //$("#".targetID).css("height","400px");
            
            
            html = "";
            html += "<div class='editing-panel'>";
            html += "<input type='text' value='abcd' name='content_title'>";
            html += "<div>";
            html += "</div>";
            
            //$("tr#"+targetID).find(".content-title").html();
            $("tr#"+targetID).find(".news-content .content-title").remove();
            $("tr#"+targetID).find(".news-content").prepend(html);
            target.find('.news-content').attr('valign', 'top');
            
            //$("tr#"+targetID).prepend(html);
            
            
        });
    });
    
    
</script>
<script src="<?php echo admin_url() ?>/js/list.js"></script>

<?php admin_page_include('includes/footer.php')?>