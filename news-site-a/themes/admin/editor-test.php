<?php
    $_SESSION['KCFINDER'] = array(
        'disabled' => false,
        'uploadURL' => base_url()."/uploads",
        'uploadDir' => ABSPATH."uploads",
    );
?>

<script>
    var ABSPATH = '<?php echo ABSPATH; ?>';
    var admin_path = '<?php echo admin_url(); ?>';
    var theme_path = '<?php echo theme_url(); ?>';
    var base_url = '<?php echo base_url(); ?>';
    var admin_url = '<?php echo admin_link(); ?>';
</script>


<!DOCTYPE HTML>
<html>
    <head>
        
        <?php $l = admin_url(); ?>
        <title>Editor Test</title>
        <script src="<?php echo $l?>/js/jquery-1.10.2.js" type="text/javascript" ></script >
        <script src="<?php echo $l?>/editor-panel/ckeditor/ckeditor.js" type="text/javascript" ></script>
        <!--<script src="<?php echo $l?>/editor-panel/ckeditor/config.js" type="text/javascript" ></script>-->
    </head>
    <body>
        <textarea id="editor1"> </textarea>
        
        <script>
        
            CKEDITOR.replace( 'editor1' );
        
        </script>
        
    </body>
</html>
