<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="stylesheet" href="css/bootstrap.min.css">
        
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        
        
        <script src="javascript/jQuery.js"> </script>
        
        <script src="javascript/bootstrap.min.js"> </script>
        
    </head>
    <body>
        <div class="container">
            <div class="row" style="height: 120px">
                
            </div>
        </div>
        <nav class="navbar navbar-inverse" style="margin-bottom: 0 ">
            <div class="container" >
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>                        
                </button>
                <!--  
                <a class="navbar-brand" href="#">WebSiteName</a>
                -->
              </div>
              <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#" class="glyphicon glyphicon-home" title="Home"></a></li>
                  <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">খবর <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">রাজনীতি</a></li>
                      <li><a href="#">অর্থনীতি</a></li>
                      <li><a href="#">বিশ্ব</a></li>
                      <li><a href="#">তথ্য ও প্রযুক্তি</a></li>
                      <li><a href="#">পরিবেশ</a></li>
                      <li><a href="#">সাস্থ</a></li>
                      <li><a href="#">আন্তর্জাতিক</a></li>
                    </ul>
                  </li>
                  <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">বাণিজ্য<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">পোশাক শিল্প</a></li>
                      <li><a href="#">হাউজিং</a></li>
                      <li><a href="#">শেয়ার বাজার</a></li>
                    </ul>
                  </li>
                  <li>
                    <a href="#">খেলাধুলা</a >
                  </li>
                  <li>
                      <a href="#">শিক্ষা</a>
                  </li>
                  <li>
                      <a href="#">পর্যটন</a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        <style>
            #main {
                margin-top: 40px;
            }
            .navbar-inverse {
                background: none;
                border: none;
            }
            #myNavbar {
                background-color: #2a6496;
            }
            
            .navbar-header {
                background-color: #2a6496;
            }
            
            .navbar-inverse .navbar-nav>li>a {
                color: #FFF;
            }
            .navbar-inverse .navbar-nav>.open>a, .navbar-inverse .navbar-nav>.open>a:hover, .navbar-inverse .navbar-nav>.open>a:focus {
                color: #fff;
                background-color: #555;
            }
            #section-header {
                width: 100%;
                height: 35px;
                float: left;
                background-color: #2a6496;
            }
            
            #section-header h2 {
                width: 100%;
                height: 100%;
                float: left;
                margin: 0;
                padding: 0;
                line-height: 35px;
                font-size: 18px;
                font-weight: bold;
                color: #FFF;
                text-align: center;
            }
            
            #section-body {
                padding: 0;
                float: left;
                font-size: 18px;
                color: #555;
            }
            
            #section-body #lead-news {
                padding: 0;
            }
            
            #section-body #lead-news #cover-image{
                width: 100%;
                height: 200px;
                float: left;
                overflow: hidden;
                border-bottom: 1px solid #DDD;
            }
            #section-body #lead-news #cover-image img{
                width: 100%;
            }

            #section-body #lead-news #lead-title{
                float: left;
                width: 100%;
            }
            
            #section-body #lead-news #lead-title h2{
                font-size: 22px;
                font-weight: bold;
                margin: 10px 0 20px;
                color: #333;
            }
            
            #section-body #lead-news #lead-body{
                
            }

            #list-container {
                width: 100%;
                margin: 0;
                padding: 0;
                margin-top: 35px;
            }
            
            .news-list {
                width: 100%;
                float: left;
            }
            
            .news-list .item {
                width: 100%;
                float: left;
            }
            
            .with-thumb .item {
                width: 100%;
                float: left;
                margin-bottom: 18px;
            }
            
            .with-thumb .item .news-thumb {
                width: 65px;
                height: 65px;
                float: left;
                display: block;
                border: 1px solid #EEE;
                margin-right: 10px;
                margin-bottom:  2px;
                padding: 5px;
                overflow: hidden;
            }
            
            .with-thumb .item .news-thumb img {
                width: 100%;
                border: 1px solid #DDD;
            }

            .with-thumb .item  h2,.without-thumb .item  h2 {
                font-size: 18px;
                margin: 0;
                display: inline;
                padding: 0;
                display: inline;
                clear: none;
                font-weight: bold;
                line-height: 25px;
                text-wrap: none;
                color: #333;
            }
            
            .without-thumb .item  h2 {
                float: left;
                margin-top: 20px;
            }
        </style>
        <div class="container" >                
            <div class="row" id="main">
                
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div id="section-header" class="col-lg-12">
                        <h2> শীর্ষ খবর</h2>
                    </div>                    
                    <div id="section-body" class="col-lg-12">
                        <div class="col-lg-12" id="lead-news">
                            <div id="cover-image">
                                <img src="images/img1.jpg" alt="cover-image" />
                            </div>
                            <div id="lead-title">
                                <h2> <a href="">
                                    সুপ্রিম কোর্টের বিচারপতি মনোনয়ন দিলেন ওবামা 
                                    </a>                                
                                </h2>
                            </div>
                            <div id="lead-body">
                                বুধবারই তিনি এ মনোনয়ন দেন। এতে করে বিচারপতি নিয়োগের ঘোর বিরোধিতা করে আসা সিনেট রিপাবলিকানদের সঙ্গে ওবামার তীব্র রাজনৈতিক সংঘাতের পট প্রস্তু হল। ৬৩ বছর বয়সী গারল্যান্ড ওয়াশিংটন আপিল কোর্টের প্রধান বিচারক এবং সাবেক কৌঁসুলি। যুক্তরাষ্ট্রের সুপ্রিম কোর্টের প্রয়াত বিচারপতি অ্যান্তনিন স্কালিয়ার উত্তরসূরি হিসাবেই গারল্যান্ডকে মনোনীত করলেন ওবামা। 
                            </div>
                        </div>
                        <div class="col-lg-12" id="list-container">
                            <div class="news-list with-thumb">
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img3.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">ম্যাককালাম ৯১টি ছক্কা মারতে ৭১টি ম্যাচ খেলেছিলেন। গেইল ৪৬ ম্যাচেই তাকে ছাড়িয়ে গেলেন।ম্যাচেই তাকে ছাড়িয়ে গেলেন</a></h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img6.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">অধিনায়ক মাশরাফি বিন মুর্তজা জানিয়েছেন, উইকেট সবুজ ঘাস দেখে বিভ্রান্ত হয়েছিলেন তারা।</a></h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img4.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">
                                                আফ্রিদির পর দ্বিতীয় খেলোয়াড় হিসেবে টি-টোয়েন্টিতে ১ হাজার রান ও ৫০ উইকেট নেওয়ার মাইলফলকে সাকিব
                                            </a>
                                        </h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img8.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> 
                                            <a href="">
                                                স্পিনারদের নৈপুণ্যে টি-টোয়েন্টি বিশ্বকাপের সুপার টেনের প্রথম ম্যাচে মহেন্দ্র সিং ধোনির দলকে হারিয়েছে কিউইরা।
                                            </a>
                                        </h2>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div id="section-header" class="col-lg-12">
                        <h2>অন্যান্য শীর্ষ খবর</h2>
                    </div>                    
                    <div class="col-lg-12" id="list-container">
                        <div class="news-list without-thumb">
                            <div class="item">
                                <h2> <a href="">বিশ্বের দ্বিতীয় ক্রিকেটার হিসেবে ৫০ উইকেট নেওয়ার পাশাপাশি করেছেন ১ হাজার রান</a>।</h2>
                            </div>
                            <div class="item">
                                <h2> <a href="">ইডেন গার্ডেন্সের উইকেট সম্পর্কে সম্পূর্ণ ভুল ধারণা করেছিল বাংলাদেশ।</a></h2>
                            </div>
                            <div class="item">
                                <h2> <a href="">স্পিনারদের নৈপুণ্যে টি-টোয়েন্টি বিশ্বকাপের সুপার টেনের প্রথম ম্যাচে মহেন্দ্র সিং ধোনির দলকে হারিয়েছে কিউইরা।</a></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div id="section-header" class="col-lg-12">
                        <h2>অন্যান্য শীর্ষ খবর</h2>
                    </div>                    
                    <div class="col-lg-12" id="list-container">
                        <div class="news-list with-thumb">
                            <div class="item">
                                <div class="news-thumb">
                                    <div class="news-list-thumb-container">
                                        <img src="images/img8.jpg">
                                    </div>
                                </div>
                                <div class="title">
                                    <h2> <a href="">ম্যাককালাম ৯১টি ছক্কা মারতে ৭১টি ম্যাচ খেলেছিলেন। গেইল ৪৬ ম্যাচেই তাকে ছাড়িয়ে গেলেন।ম্যাচেই তাকে ছাড়িয়ে গেলেন</a></h2>
                                </div>
                            </div>
                            <div class="item">
                                <div class="news-thumb">
                                    <div class="news-list-thumb-container">
                                        <img src="images/img6.jpg">
                                    </div>
                                </div>
                                <div class="title">
                                    <h2> <a href="">অধিনায়ক মাশরাফি বিন মুর্তজা জানিয়েছেন, উইকেট সবুজ ঘাস দেখে বিভ্রান্ত হয়েছিলেন তারা।</a></h2>
                                </div>
                            </div>
                            <div class="item">
                                <div class="news-thumb">
                                    <div class="news-list-thumb-container">
                                        <img src="images/img5.jpg">
                                    </div>
                                </div>
                                <div class="title">
                                    <h2> <a href="">
                                            আফ্রিদির পর দ্বিতীয় খেলোয়াড় হিসেবে টি-টোয়েন্টিতে ১ হাজার রান ও ৫০ উইকেট নেওয়ার মাইলফলকে সাকিব
                                        </a>
                                    </h2>
                                </div>
                            </div>
                            <div class="item">
                                <div class="news-thumb">
                                    <div class="news-list-thumb-container">
                                        <img src="images/img8.jpg">
                                    </div>
                                </div>
                                <div class="title">
                                    <h2> 
                                        <a href="">
                                            স্পিনারদের নৈপুণ্যে টি-টোয়েন্টি বিশ্বকাপের সুপার টেনের প্রথম ম্যাচে মহেন্দ্র সিং ধোনির দলকে হারিয়েছে কিউইরা।
                                        </a>
                                    </h2>
                                </div>
                            </div>  
                        </div>

                    </div>
                </div>
            </div>
            <div class="row" id="main">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div id="section-header" class="col-lg-12">
                        <h2> শীর্ষ খবর</h2>
                    </div>                    
                    <div id="section-body" class="col-lg-12">
                        <div class="col-lg-12" id="lead-news">
                            <div id="cover-image">
                                <img src="images/img1.jpg" alt="cover-image" />
                            </div>
                            <div id="lead-title">
                                <h2> <a href="">
                                    সুপ্রিম কোর্টের বিচারপতি মনোনয়ন দিলেন ওবামা 
                                    </a>                                
                                </h2>
                            </div>
                            <div id="lead-body">
                                বুধবারই তিনি এ মনোনয়ন দেন। এতে করে বিচারপতি নিয়োগের ঘোর বিরোধিতা করে আসা সিনেট রিপাবলিকানদের সঙ্গে ওবামার তীব্র রাজনৈতিক সংঘাতের পট প্রস্তু হল। ৬৩ বছর বয়সী গারল্যান্ড ওয়াশিংটন আপিল কোর্টের প্রধান বিচারক এবং সাবেক কৌঁসুলি। যুক্তরাষ্ট্রের সুপ্রিম কোর্টের প্রয়াত বিচারপতি অ্যান্তনিন স্কালিয়ার উত্তরসূরি হিসাবেই গারল্যান্ডকে মনোনীত করলেন ওবামা। 
                            </div>
                        </div>
                        <div class="col-lg-12" id="list-container">
                            <div class="news-list with-thumb">
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img3.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">ম্যাককালাম ৯১টি ছক্কা মারতে ৭১টি ম্যাচ খেলেছিলেন। গেইল ৪৬ ম্যাচেই তাকে ছাড়িয়ে গেলেন।ম্যাচেই তাকে ছাড়িয়ে গেলেন</a></h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img6.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">অধিনায়ক মাশরাফি বিন মুর্তজা জানিয়েছেন, উইকেট সবুজ ঘাস দেখে বিভ্রান্ত হয়েছিলেন তারা।</a></h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img4.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">
                                                আফ্রিদির পর দ্বিতীয় খেলোয়াড় হিসেবে টি-টোয়েন্টিতে ১ হাজার রান ও ৫০ উইকেট নেওয়ার মাইলফলকে সাকিব
                                            </a>
                                        </h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img8.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> 
                                            <a href="">
                                                স্পিনারদের নৈপুণ্যে টি-টোয়েন্টি বিশ্বকাপের সুপার টেনের প্রথম ম্যাচে মহেন্দ্র সিং ধোনির দলকে হারিয়েছে কিউইরা।
                                            </a>
                                        </h2>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div id="section-header" class="col-lg-12">
                        <h2> শীর্ষ খবর</h2>
                    </div>                    
                    <div id="section-body" class="col-lg-12">
                        <div class="col-lg-12" id="lead-news">
                            <div id="cover-image">
                                <img src="images/img1.jpg" alt="cover-image" />
                            </div>
                            <div id="lead-title">
                                <h2> <a href="">
                                    সুপ্রিম কোর্টের বিচারপতি মনোনয়ন দিলেন ওবামা 
                                    </a>                                
                                </h2>
                            </div>
                            <div id="lead-body">
                                বুধবারই তিনি এ মনোনয়ন দেন। এতে করে বিচারপতি নিয়োগের ঘোর বিরোধিতা করে আসা সিনেট রিপাবলিকানদের সঙ্গে ওবামার তীব্র রাজনৈতিক সংঘাতের পট প্রস্তু হল। ৬৩ বছর বয়সী গারল্যান্ড ওয়াশিংটন আপিল কোর্টের প্রধান বিচারক এবং সাবেক কৌঁসুলি। যুক্তরাষ্ট্রের সুপ্রিম কোর্টের প্রয়াত বিচারপতি অ্যান্তনিন স্কালিয়ার উত্তরসূরি হিসাবেই গারল্যান্ডকে মনোনীত করলেন ওবামা। 
                            </div>
                        </div>
                        <div class="col-lg-12" id="list-container">
                            <div class="news-list with-thumb">
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img3.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">ম্যাককালাম ৯১টি ছক্কা মারতে ৭১টি ম্যাচ খেলেছিলেন। গেইল ৪৬ ম্যাচেই তাকে ছাড়িয়ে গেলেন।ম্যাচেই তাকে ছাড়িয়ে গেলেন</a></h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img6.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">অধিনায়ক মাশরাফি বিন মুর্তজা জানিয়েছেন, উইকেট সবুজ ঘাস দেখে বিভ্রান্ত হয়েছিলেন তারা।</a></h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img4.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">
                                                আফ্রিদির পর দ্বিতীয় খেলোয়াড় হিসেবে টি-টোয়েন্টিতে ১ হাজার রান ও ৫০ উইকেট নেওয়ার মাইলফলকে সাকিব
                                            </a>
                                        </h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img8.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> 
                                            <a href="">
                                                স্পিনারদের নৈপুণ্যে টি-টোয়েন্টি বিশ্বকাপের সুপার টেনের প্রথম ম্যাচে মহেন্দ্র সিং ধোনির দলকে হারিয়েছে কিউইরা।
                                            </a>
                                        </h2>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div id="section-header" class="col-lg-12">
                        <h2> শীর্ষ খবর</h2>
                    </div>                    
                    <div id="section-body" class="col-lg-12">
                        <div class="col-lg-12" id="lead-news">
                            <div id="cover-image">
                                <img src="images/img1.jpg" alt="cover-image" />
                            </div>
                            <div id="lead-title">
                                <h2> <a href="">
                                    সুপ্রিম কোর্টের বিচারপতি মনোনয়ন দিলেন ওবামা 
                                    </a>                                
                                </h2>
                            </div>
                            <div id="lead-body">
                                বুধবারই তিনি এ মনোনয়ন দেন। এতে করে বিচারপতি নিয়োগের ঘোর বিরোধিতা করে আসা সিনেট রিপাবলিকানদের সঙ্গে ওবামার তীব্র রাজনৈতিক সংঘাতের পট প্রস্তু হল। ৬৩ বছর বয়সী গারল্যান্ড ওয়াশিংটন আপিল কোর্টের প্রধান বিচারক এবং সাবেক কৌঁসুলি। যুক্তরাষ্ট্রের সুপ্রিম কোর্টের প্রয়াত বিচারপতি অ্যান্তনিন স্কালিয়ার উত্তরসূরি হিসাবেই গারল্যান্ডকে মনোনীত করলেন ওবামা। 
                            </div>
                        </div>
                        <div class="col-lg-12" id="list-container">
                            <div class="news-list with-thumb">
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img3.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">ম্যাককালাম ৯১টি ছক্কা মারতে ৭১টি ম্যাচ খেলেছিলেন। গেইল ৪৬ ম্যাচেই তাকে ছাড়িয়ে গেলেন।ম্যাচেই তাকে ছাড়িয়ে গেলেন</a></h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img6.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">অধিনায়ক মাশরাফি বিন মুর্তজা জানিয়েছেন, উইকেট সবুজ ঘাস দেখে বিভ্রান্ত হয়েছিলেন তারা।</a></h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img4.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">
                                                আফ্রিদির পর দ্বিতীয় খেলোয়াড় হিসেবে টি-টোয়েন্টিতে ১ হাজার রান ও ৫০ উইকেট নেওয়ার মাইলফলকে সাকিব
                                            </a>
                                        </h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img8.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> 
                                            <a href="">
                                                স্পিনারদের নৈপুণ্যে টি-টোয়েন্টি বিশ্বকাপের সুপার টেনের প্রথম ম্যাচে মহেন্দ্র সিং ধোনির দলকে হারিয়েছে কিউইরা।
                                            </a>
                                        </h2>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="row" id="main">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div id="section-header" class="col-lg-12">
                        <h2> শীর্ষ খবর</h2>
                    </div>                    
                    <div id="section-body" class="col-lg-12">
                        <div class="col-lg-12" id="lead-news">
                            <div id="cover-image">
                                <img src="images/img1.jpg" alt="cover-image" />
                            </div>
                            <div id="lead-title">
                                <h2> <a href="">
                                    সুপ্রিম কোর্টের বিচারপতি মনোনয়ন দিলেন ওবামা 
                                    </a>                                
                                </h2>
                            </div>
                            <div id="lead-body">
                                বুধবারই তিনি এ মনোনয়ন দেন। এতে করে বিচারপতি নিয়োগের ঘোর বিরোধিতা করে আসা সিনেট রিপাবলিকানদের সঙ্গে ওবামার তীব্র রাজনৈতিক সংঘাতের পট প্রস্তু হল। ৬৩ বছর বয়সী গারল্যান্ড ওয়াশিংটন আপিল কোর্টের প্রধান বিচারক এবং সাবেক কৌঁসুলি। যুক্তরাষ্ট্রের সুপ্রিম কোর্টের প্রয়াত বিচারপতি অ্যান্তনিন স্কালিয়ার উত্তরসূরি হিসাবেই গারল্যান্ডকে মনোনীত করলেন ওবামা। 
                            </div>
                        </div>
                        <div class="col-lg-12" id="list-container">
                            <div class="news-list with-thumb">
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img3.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">ম্যাককালাম ৯১টি ছক্কা মারতে ৭১টি ম্যাচ খেলেছিলেন। গেইল ৪৬ ম্যাচেই তাকে ছাড়িয়ে গেলেন।ম্যাচেই তাকে ছাড়িয়ে গেলেন</a></h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img6.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">অধিনায়ক মাশরাফি বিন মুর্তজা জানিয়েছেন, উইকেট সবুজ ঘাস দেখে বিভ্রান্ত হয়েছিলেন তারা।</a></h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img4.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">
                                                আফ্রিদির পর দ্বিতীয় খেলোয়াড় হিসেবে টি-টোয়েন্টিতে ১ হাজার রান ও ৫০ উইকেট নেওয়ার মাইলফলকে সাকিব
                                            </a>
                                        </h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img8.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> 
                                            <a href="">
                                                স্পিনারদের নৈপুণ্যে টি-টোয়েন্টি বিশ্বকাপের সুপার টেনের প্রথম ম্যাচে মহেন্দ্র সিং ধোনির দলকে হারিয়েছে কিউইরা।
                                            </a>
                                        </h2>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div id="section-header" class="col-lg-12">
                        <h2> শীর্ষ খবর</h2>
                    </div>                    
                    <div id="section-body" class="col-lg-12">
                        <div class="col-lg-12" id="lead-news">
                            <div id="cover-image">
                                <img src="images/img1.jpg" alt="cover-image" />
                            </div>
                            <div id="lead-title">
                                <h2> <a href="">
                                    সুপ্রিম কোর্টের বিচারপতি মনোনয়ন দিলেন ওবামা 
                                    </a>                                
                                </h2>
                            </div>
                            <div id="lead-body">
                                বুধবারই তিনি এ মনোনয়ন দেন। এতে করে বিচারপতি নিয়োগের ঘোর বিরোধিতা করে আসা সিনেট রিপাবলিকানদের সঙ্গে ওবামার তীব্র রাজনৈতিক সংঘাতের পট প্রস্তু হল। ৬৩ বছর বয়সী গারল্যান্ড ওয়াশিংটন আপিল কোর্টের প্রধান বিচারক এবং সাবেক কৌঁসুলি। যুক্তরাষ্ট্রের সুপ্রিম কোর্টের প্রয়াত বিচারপতি অ্যান্তনিন স্কালিয়ার উত্তরসূরি হিসাবেই গারল্যান্ডকে মনোনীত করলেন ওবামা। 
                            </div>
                        </div>
                        <div class="col-lg-12" id="list-container">
                            <div class="news-list with-thumb">
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img3.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">ম্যাককালাম ৯১টি ছক্কা মারতে ৭১টি ম্যাচ খেলেছিলেন। গেইল ৪৬ ম্যাচেই তাকে ছাড়িয়ে গেলেন।ম্যাচেই তাকে ছাড়িয়ে গেলেন</a></h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img6.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">অধিনায়ক মাশরাফি বিন মুর্তজা জানিয়েছেন, উইকেট সবুজ ঘাস দেখে বিভ্রান্ত হয়েছিলেন তারা।</a></h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img4.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">
                                                আফ্রিদির পর দ্বিতীয় খেলোয়াড় হিসেবে টি-টোয়েন্টিতে ১ হাজার রান ও ৫০ উইকেট নেওয়ার মাইলফলকে সাকিব
                                            </a>
                                        </h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img8.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> 
                                            <a href="">
                                                স্পিনারদের নৈপুণ্যে টি-টোয়েন্টি বিশ্বকাপের সুপার টেনের প্রথম ম্যাচে মহেন্দ্র সিং ধোনির দলকে হারিয়েছে কিউইরা।
                                            </a>
                                        </h2>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div id="section-header" class="col-lg-12">
                        <h2> শীর্ষ খবর</h2>
                    </div>                    
                    <div id="section-body" class="col-lg-12">
                        <div class="col-lg-12" id="lead-news">
                            <div id="cover-image">
                                <img src="images/img1.jpg" alt="cover-image" />
                            </div>
                            <div id="lead-title">
                                <h2> <a href="">
                                    সুপ্রিম কোর্টের বিচারপতি মনোনয়ন দিলেন ওবামা 
                                    </a>                                
                                </h2>
                            </div>
                            <div id="lead-body">
                                বুধবারই তিনি এ মনোনয়ন দেন। এতে করে বিচারপতি নিয়োগের ঘোর বিরোধিতা করে আসা সিনেট রিপাবলিকানদের সঙ্গে ওবামার তীব্র রাজনৈতিক সংঘাতের পট প্রস্তু হল। ৬৩ বছর বয়সী গারল্যান্ড ওয়াশিংটন আপিল কোর্টের প্রধান বিচারক এবং সাবেক কৌঁসুলি। যুক্তরাষ্ট্রের সুপ্রিম কোর্টের প্রয়াত বিচারপতি অ্যান্তনিন স্কালিয়ার উত্তরসূরি হিসাবেই গারল্যান্ডকে মনোনীত করলেন ওবামা। 
                            </div>
                        </div>
                        <div class="col-lg-12" id="list-container">
                            <div class="news-list with-thumb">
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img3.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">ম্যাককালাম ৯১টি ছক্কা মারতে ৭১টি ম্যাচ খেলেছিলেন। গেইল ৪৬ ম্যাচেই তাকে ছাড়িয়ে গেলেন।ম্যাচেই তাকে ছাড়িয়ে গেলেন</a></h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img6.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">অধিনায়ক মাশরাফি বিন মুর্তজা জানিয়েছেন, উইকেট সবুজ ঘাস দেখে বিভ্রান্ত হয়েছিলেন তারা।</a></h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img4.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> <a href="">
                                                আফ্রিদির পর দ্বিতীয় খেলোয়াড় হিসেবে টি-টোয়েন্টিতে ১ হাজার রান ও ৫০ উইকেট নেওয়ার মাইলফলকে সাকিব
                                            </a>
                                        </h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="news-thumb">
                                        <div class="news-list-thumb-container">
                                            <img src="images/img8.jpg">
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2> 
                                            <a href="">
                                                স্পিনারদের নৈপুণ্যে টি-টোয়েন্টি বিশ্বকাপের সুপার টেনের প্রথম ম্যাচে মহেন্দ্র সিং ধোনির দলকে হারিয়েছে কিউইরা।
                                            </a>
                                        </h2>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        
            <div class="row" id="main">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <ul>
                        <li>
                            <a href="">
                                হোম
                            </a>
                        <li>
                            <a href="">
                                বাংলাদেশ
                            </a>
                        </li>
                        <li>
                            <a href="">
                                রাজনীতি
                            </a>
                        </li>
                        <li>
                            <a href="">
                                অর্থনীতি
                            </a>
                        </li>
                        <li>
                            <a href="">
                                বাণিজ্য
                            </a>
                        </li>
                        <li>
                            <a href="">
                                খেলাধুলা
                            </a>
                        </li>
                        <li>
                            <a href="">

                                শিক্ষা
                            </a>
                        </li>
                        <li>
                            <a href="">

                                বিশ্ব
                            </a>
                        </li>
                        <li>
                            <a href="">
                                কৃষি
                        </li>

                        <li>
                            </a>
                            <a href="">
                                পোশাক শিল্প

                            </a>
                        </li>
                        <li>
                            <a href="">

                                আর্থিক সংগঠন
                            </a>
                        </li>
                        <li>
                            <a href="">
                                শেয়ার বাজার

                            </a>
                        </li>
                        <li>
                            <a href="">

                                জীবন বীমা
                            </a>
                        </li>
                        <li>
                            <a href="">

                                আন্তর্জাতিক
                            </a>
                        </li>
                    </ul>
                </div>             
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <ul>
                        <li>
                            <a href="">
                                হোম
                            </a>
                        <li>
                            <a href="">
                                বাংলাদেশ
                            </a>
                        </li>
                        <li>
                            <a href="">
                                রাজনীতি
                            </a>
                        </li>
                        <li>
                            <a href="">
                                অর্থনীতি
                            </a>
                        </li>
                        <li>
                            <a href="">
                                বাণিজ্য
                            </a>
                        </li>
                        <li>
                            <a href="">
                                খেলাধুলা
                            </a>
                        </li>
                        <li>
                            <a href="">

                                শিক্ষা
                            </a>
                        </li>
                        <li>
                            <a href="">

                                বিশ্ব
                            </a>
                        </li>
                        <li>
                            <a href="">
                                কৃষি
                        </li>

                        <li>
                            </a>
                            <a href="">
                                পোশাক শিল্প

                            </a>
                        </li>
                        <li>
                            <a href="">

                                আর্থিক সংগঠন
                            </a>
                        </li>
                        <li>
                            <a href="">
                                শেয়ার বাজার

                            </a>
                        </li>
                        <li>
                            <a href="">

                                জীবন বীমা
                            </a>
                        </li>
                        <li>
                            <a href="">

                                আন্তর্জাতিক
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <ul>
                        <li>
                            <a href="">
                                হোম
                            </a>
                        <li>
                            <a href="">
                                বাংলাদেশ
                            </a>
                        </li>
                        <li>
                            <a href="">
                                রাজনীতি
                            </a>
                        </li>
                        <li>
                            <a href="">
                                অর্থনীতি
                            </a>
                        </li>
                        <li>
                            <a href="">
                                বাণিজ্য
                            </a>
                        </li>
                        <li>
                            <a href="">
                                খেলাধুলা
                            </a>
                        </li>
                        <li>
                            <a href="">

                                শিক্ষা
                            </a>
                        </li>
                        <li>
                            <a href="">

                                বিশ্ব
                            </a>
                        </li>
                        <li>
                            <a href="">
                                কৃষি
                        </li>

                        <li>
                            </a>
                            <a href="">
                                পোশাক শিল্প

                            </a>
                        </li>
                        <li>
                            <a href="">

                                আর্থিক সংগঠন
                            </a>
                        </li>
                        <li>
                            <a href="">
                                শেয়ার বাজার

                            </a>
                        </li>
                        <li>
                            <a href="">

                                জীবন বীমা
                            </a>
                        </li>
                        <li>
                            <a href="">

                                আন্তর্জাতিক
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </body>
</html>




































