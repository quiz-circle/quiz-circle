<?php

define("HOST", "mysql11.000webhost.com");

define("USER_NAME", "a1789574_plan");

define("PASSWORD", "asd012563");

define("DB_NAME", "a1789574_plan");

function GET_DATA($key = false){
    if(isset($_GET) && !empty($_GET)){
        if($key){
           if(isset($_GET[$key]) && !empty($_GET[$key])){
               return $_GET[$key];
           }
           return false;
        }
        return $_GET;
    }
    return false;
}

function SESSION_DATA($key = false){
    if(isset($_SESSION) && !empty($_SESSION)){
        if($key){
           if(isset($_SESSION[$key]) && !empty($_SESSION[$key])){
               return $_SESSION[$key];
           }
           return false;
        }
        return $_SESSION;
    }
    return false;
}

function POST_DATA($key = false){
    if(isset($_POST) && !empty($_POST)){
        if($key){
           if(isset($_POST[$key]) && !empty($_POST[$key])){
               return $_POST[$key];
           }
           return false;
        }
        return $_POST;
    }
    return false;
}

function Explain($exp, $type = ""){
    echo "<pre><tt>";
    switch($type){
        case "dump":
            var_dump($exp);
            break;
        case "export":
            var_export($exp);
            break;
        default :
            print_r($exp);
    };
    echo "</tt></pre>";
}

$db = "";

function db_connect(){
    global $db;
    $host = "";
    //connect to the host
    $db = mysql_connect(HOST, USER_NAME, PASSWORD ) or exit("Couldn't Connect To mysql Database!");
    
    //Establishing the database connetion
    mysql_select_db(DB_NAME) or exit("Database <em>".DB_NAME."</em> not found!");
    return $host;
}

db_connect();

function perform_sql($sql){
    global  $db;
    mysql_query("SET NAMES utf8", $db);
    return mysql_query($sql);
}

function get_query_type ($sql){
    $q = explode(" ", $sql);
    return strtoupper($q[0]);
}

function get_list($sql, &$total_rows = 0, &$sql_error = ""){
    if(get_query_type($sql) == "SELECT"){
        global  $db;
        $query = perform_sql($sql);
        $sql_error = mysql_error($db);

        $list_funal = array();
        if(!$sql_error){
            while($result = mysql_fetch_assoc($query)){
                $list_funal[] = $result;
            }
            $total_rows = mysql_num_rows($query);
        }
        return $list_funal;
    }
}

if(POST_DATA()){
    $sql = POST_DATA("sql");
    $query_type = "";
    if(strlen($sql)){
        $query_type = get_query_type($sql);
        
        $total = 0;
        $error = "";

        if($query_type === "SELECT"){
            $list = get_list($sql, $total, $error);
        }else {
            perform_sql($sql);
            $error = mysql_error($db);   
        }
        
        if($error){
            $message = $error;
        }else {
            $message = "Query Successfully executed!";
        }
        
    }else {
        $message = "";
        $list = array();
        $sql = "";
    }
}else {
    $message = "";
    $list = array();
    $query_type = "";
}

?>
<!doctype html>
<html>
    <head>
        <title>Sql Operation</title>
        <style>
            .main {
                width: 1200px;
                margin: 0 auto;
            }
            
            .main-in {
                width: 100%;
                float: left;
            }
            
            .sql_container, .run_button, .operation-container, .result-container, .message-container, .sql-container {
                width: 100%;
                float: left;
            }
            .sql_container #sql {
                width: 100%;
                height: 300px;
                font-size: verdana;
                color:azure;
                background-color: #000;
            }
            .run_button input {
                padding: 15px 30px;
                background-color: #333;
                color: #FFF;
                border: 1px #000 solid;
                outline: none;
                cursor: pointer;
            }
            .result-container {
                border: 1px solid #444;
            }
            .sql-container {
                background-color: #2A970C;
                color: #FFF;
            }
            .sql-container p {
                width: 100%;
                font-size: 12px;
                font-family: verdana;
            }
            
        </style>
    </head>
    <body>
        <div class="main">
            <div class="main-in">
                <div class="operation-container">
                    <form action="" method="post">
                        <div class="input-group sql_container">
                            <textarea id="sql" name="sql"><?php echo POST_DATA("sql")?></textarea>
                        </div>
                        <div class="input-group run_button">
                            <input type="submit" value="Run SQL">
                        </div>      
                    </form>
                </div>
                <div class="sql-container">
                    <p><?php echo $sql; ?></p>
                </div>
                <div class="message-container">
                    <p><?php echo $message; ?></p>
                </div>
                <?php if($query_type === "SELECT"):?>
                <div class="result-container">
                    <?php
                    echo "dd";
                        if(!$error){
                            Explain($list);
                        }
                    ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
        
    </body>
</html>
