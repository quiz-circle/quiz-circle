<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * @Description of useSSL
 * @params none
 * @return boolean
 */
function _unique_id(){
    return md5(microtime()." " .rand(0,500).rand(500,1000).uniqid());
}


function dialogue_box($header, $body, $footer = ''){
    ?>
    <script>
        dialogue_box("<?php echo $header; ?>","<?php echo $body; ?>", "<?php echo $footer; ?>");
    </script>
    <?php
}

function show_main_message($message,  $color = "green"){
    ?>
        <script>
            showMainMessage("<?php echo $message ?>","<?php echo $color?>");
        </script>
    <?php
}

function add_flash_message($name, $message, $color = "green"){
    load()->sys("Session")->open();
    $session = new Session();
    $session->add($name, $message);
    $color_key = $name . "_color";
    $session->add( $color_key , $color);
}

function show_flashed_message($name){
    load()->sys("Session")->open();
    $session = new Session();
    if($session->check($name)){
        $message = $session->flush($name);
        $color = ($session->check($name . "_color")) ? $session->flush($name . "_color") : "green";
        ?>
            <script>
                showMainMessage("<?php echo $message ?>","<?php echo $color?>");
            </script>
        <?php
    }
}

function useSSL(){
    return false;
}

/*
 * 
 * 
 * @Description of Base url
 * @params none
 * 
 * @return @String
 * 
 * 
 */
function _alert($string){
    echo  "<script>alert('" . $string . "')</script>";
}

function _red_alert($string){
    echo "<span style='color:red'>" . $string . "</span>";
}
function _green_signal($string){
    echo "<span style='color:green'>" . $string . "</span>";
}


function isAssoc($arr){
    if(is_array($arr)){
        return array_keys($arr) !== range(0, count($arr) - 1);
    }
}

function print_header(){
    page_include("includes/header.php");
}

function print_footer(){
    page_include("includes/footer.php");
}

function convert_date($date, $to){
    
    $d = date_create($date);
    return date_format($d, $to );
}

function _br(){
    echo "<br>";
}

function GET_DATA($key = false, $default = null){
    if(isset($_GET) && !empty($_GET)){
        if($key){
            
            
           if(isset($_GET[$key]) && !empty($_GET[$key])){
               return $_GET[$key];
           }else if (isset($_GET[$key])){
               if($_GET[$key] == 0){
                    return $_GET[$key];   
               }
           }
           return $default;
        }
        return $_GET;
    }
    return $default;
}

function SESSION_DATA($key = false, $default = null){
    if(isset($_SESSION) && !empty($_SESSION)){
        if($key){
           if(isset($_SESSION[$key]) && !empty($_SESSION[$key])){
               return $_SESSION[$key];
           }
           return $default;
        }
        return $_SESSION;
    }
    return $default;
}

function POST_DATA($key = false, $default = null){
    if(isset($_POST) && !empty($_POST)){
        if($key){
           if(isset($_POST[$key]) && !empty($_POST[$key])){
               return $_POST[$key];
           }
           return $default;
        }
        return $_POST;
    }
    return $default;
}

function SEG_VARS($index){
    global $_SEG_VARS;
    if(count($_SEG_VARS)){
        return isset($_SEG_VARS[$index]) ? $_SEG_VARS[$index]:"";
    }
}

function ADMIN_SEG_VARS($index){
    global $_SEG_VARS;
    
    array_shift($_SEG_VARS);   
    if(count($_SEG_VARS)){
        return isset($_SEG_VARS[$index]) ? $_SEG_VARS[$index]:"";
    }
}

function admin_page_name(){
    return SEG_VARS(0);
}


function get_date_formate($date, $formate = "Y-m-d"){
    $da = date_create($date);
    return date_format($da, $formate);
}

function base_url($useSSL = false){
    $host = $_SERVER['HTTP_HOST'];
    $link_prefix = ($useSSL) ? "https://":"http://";
    $split_abspath = str_ireplace("index.php", "", $_SERVER['PHP_SELF']);
    return rtrim($link_prefix.$host.$split_abspath,"/");
}
/*
function base_url($useSSL = false){
    $abspath = rtrim(ABSPATH, '/');    
    $host = $_SERVER['HTTP_HOST'];
    $link_prefix = ($useSSL) ? "https://":"http://";
    $split_abspath = explode('/', $abspath);
    $base_path = $split_abspath[count($split_abspath)-1];
    
    //Explain($_SERVER);
    
    return $link_prefix.$host.'/'.$base_path;
    
} 
*/

function make_directory($dir, $mode = "0755"){
    if(!is_dir(ABSPATH.$dir)){
        mkdir(ABSPATH.$dir, $mode);
    }
}

function theme_url($return = false){
    global $main;
    $url = base_url() . "/themes/" . $main->getThemeName();
    if($return) return $url;
    else echo $url;
}

function plugin_url($plugin_name){
    global $main;
    echo base_url() . "/plugins/" . $plugin_name;
}

function admin_url(){
    global $main;
    return  base_url() . "/themes/admin";
}

function admin_link(){
    return  base_url() . "/".get_config("site/admin");
}

function rootUri(){
    return rtrim(str_ireplace($_SERVER['DOCUMENT_ROOT'], '', ABSPATH),'/');
    
}


function create_table($table_name, $cols, $extra = null){
    //echo '<tt><pre>';
    //print_r($cols);
    //echo '</tt></pre>';
    
    $tb_sql = "CREATE TABLE IF NOT EXISTS `".$table_name."`(";
    $counter = 0;
    foreach($cols as $col => $attr) {
        
        $tb_sql .=  ($counter > 0)?  ", ":" ";
        
        $tb_sql .=(!is_numeric($col)) ? "`".$col . "` " . $attr:" ".$attr;
        
        $counter++;
    }
    //$tb_sql .= ($extra) ? ", ".$extra:"";
    $tb_sql .= ")";       
    return $tb_sql;
}

function _run_($action){
    $func = "__".$action; 
    $func();
}

function page_include($path){
    $file = $path;
    $path = ABSPATH ."themes/".  get_theme_name()."/". $path;   
    if(file_exists($path)) {
        include_once $path;
    }else {
        echo "<em><strong>".$file. "</strong></em> is not found on <em>".$main->getThemeName(). "</em> theme!";
    }
}
function admin_page_include($path){
    $file = $path;
    global $main;
    $path = ABSPATH ."themes/admin/". $path;   
    if(file_exists($path)) {
        include_once $path;
    }else {
        echo "<em><strong>".$file. "</strong></em> is not found on <em>".$main->getThemeName(). "</em> theme!";
    }
}

function get_theme_name(){
    global $main;
    return $main->getThemeName();
}

/*function get_admin_name(){
    global $main;
    return $main->getThemeName();
}*/


function get_config($path = null) {
    if ($path) {
        $config = $GLOBALS['config'];

        $path = explode('/', $path);

        foreach ($path as $p) {
            if (isset($config[$p])) {
                $config = $config[$p];
            }
        }
        return $config;
    }
    return false;
}

function themePaths(){
    global $main;
    return  base_url()."/themes/".$main->getThemeName();
}

function Explain($exp, $type = 'print'){
    echo  "<tt><pre>";
    switch($type){
        case 'print':
            print_r($exp);
        break;
        case 'dump':
            var_dump($exp);
        break;
        case 'export':
            var_export($exp);
        break;
    }
    echo  "</pre></tt>";
}

function escape_all($string){
    global  $db;
    return mysqli_real_escape_string($db->getLink(), strip_tags(trim($string)));
}

function loadLib($classes, $dir = "apps/lib"){
    
    $path = ABSPATH . $dir . "/".$classes.".php";
    if(file_exists($path)){
        require_once $path;
        if(class_exists($classes)){
           return new $classes();
        }
    }
    return false;
}


class load{
    private  $_path, $_filename;
    
    public function sys($fname){
        $this->set_path_and_fname('root/core/system', $fname);
        return $this;
    }
    public function db($fname){
        $this->set_path_and_fname('root/db', $fname);
        return $this;
    }
    
    public function lib($fname){
        $this->set_path_and_fname('apps/lib', $fname);
        return $this;
    }
    
    public function custom($path, $fname){
        $this->set_path_and_fname($path, $fname);
    }
    
    public function set_path_and_fname($path, $fname){
        $this->_path = ABSPATH . $path;
        $this->_filename = $fname;
    }
    
    public  function open(){
        $this->_filename = ucfirst($this->_filename);
        $path = $this->_path . "/".$this->_filename.".php";
        if(file_exists($path)){
           require_once $path;
        }
    }
    
    public  function run(){
        $cp = func_get_args();
        
        $this->_filename = ucfirst($this->_filename);
        $path = $this->_path . "/".$this->_filename.".php";
        
        if(file_exists($path)){
            $cn = $this->_filename;
            require_once $path;
            if(class_exists($this->_filename)){

                switch (count($cp)){
                    case 0:
                        return new $cn();
                        break;
                    case 1:
                        return new $cn($cp[0]);
                        break;
                    case 2:
                        return new $cn($cp[0],$cp[1]);
                        break;
                    case 3:
                        return new $cn($cp[0],$cp[1],$cp[2]);
                        break;
                    case 4:
                        return new $cn($cp[0],$cp[1],$cp[2],$cp[3]);
                        break;
                    case 5:
                        return new $cn($cp[0],$cp[1],$cp[2],$cp[3],$cp[4]);
                        break;
                    case 6:
                        return new $cn($cp[0],$cp[1],$cp[2],$cp[3],$cp[4],$cp[5]);
                        break;
                    case 7:
                        return new $cn($cp[0],$cp[1],$cp[2],$cp[3],$cp[4],$cp[5], $cp[6]);
                        break;
                    case 7:
                        return new $cn($cp[0],$cp[1],$cp[2],$cp[3],$cp[4],$cp[5], $cp[6], $cp[7]);
                        break;
                    case 7:
                        return new $cn($cp[0],$cp[1],$cp[2],$cp[3],$cp[4],$cp[5], $cp[6], $cp[7],$cp[8]);
                        break;
                    case 7:
                        return new $cn($cp[0],$cp[1],$cp[2],$cp[3],$cp[4],$cp[5], $cp[6], $cp[7], $cp[8], $cp[9]);
                        break;
                }

            }
            exit("Class ".$this->_filename." Not Found!");
        }
        exit("The ".$path ." is Not Exists!");
    }
    
    
    public static function init(){
        $l = new load;
        return $l;
    }
}

function load(){
  return load::init();   
}

function loadSYS($class_name){
        $cp = func_get_args();
        array_shift($cp);

        $class_name = ucfirst($class_name);
        $path = ABSPATH . "root/core/system/".$class_name.".php";
        if(file_exists($path)){
            $cn = $class_name;
            require_once $path;
            if(class_exists($class_name)){

                switch (count($cp)){
                    case 0:
                        return new $cn();
                        break;
                    case 1:
                        return new $cn($cp[0]);
                        break;
                    case 2:
                        return new $cn($cp[0],$cp[1]);
                        break;
                    case 3:
                        return new $cn($cp[0],$cp[1],$cp[2]);
                        break;
                    case 4:
                        return new $cn($cp[0],$cp[1],$cp[2],$cp[3]);
                        break;
                    case 5:
                        return new $cn($cp[0],$cp[1],$cp[2],$cp[3],$cp[4]);
                        break;
                    case 6:
                        return new $cn($cp[0],$cp[1],$cp[2],$cp[3],$cp[4],$cp[5]);
                        break;
                    case 7:
                        return new $cn($cp[0],$cp[1],$cp[2],$cp[3],$cp[4],$cp[5], $cp[6]);
                        break;
                }

            }
            return false;
        }
        return false;
    }

function loadCore($classes, $directory = "system"){
    switch($directory){
        case "system":
            $dir = "root/core/system";
        break;
        case "db" :
            $dir = "root/db";
        break;
    }
    
    if(is_array($classes)){
        $result = array();
        foreach($classes as $class){
            $class = ucfirst($class);
            $path = ABSPATH . $dir . "/".$class.".php";
            if(file_exists($path)){
                require_once $path;
                if(class_exists($path)){
                    $cl = new $class();
                    $result[$class] = $cl;
                }else return false;
            } else return false;   
        }
        return $result;
    }else {
        $classes = ucfirst($classes);
        $path = ABSPATH . $dir . "/".$classes.".php";
        if(file_exists($path)){
            require_once $path;
            if(class_exists($classes)){
               return new $classes();
            }
        }
        return false;        
    }
    return false;
    
}

function path_segments($index = false){
    if(!isset($_SERVER['PATH_INFO'])){
        
        $uri_split = explode('?',$_SERVER['REQUEST_URI']);       
        $rootURI = rtrim(str_ireplace($_SERVER['DOCUMENT_ROOT'], '', ABSPATH),'/');
        $paths = str_ireplace($rootURI, '',$uri_split[0]);
        
        $paths = rtrim(ltrim($paths,'/'),'/');
        if(strlen($paths) == 0){
            return array();
        }
        $path_split = explode('/',$paths);
        return $path_split;
        
    }else {
        $pathInfo = ltrim($_SERVER['PATH_INFO'],'/');
        if(strlen($pathInfo) == 0){
            return array();
        }else {
            $segments = explode('/', $pathInfo);
            return $segments;
        }
    }
}

function load_page($paths){
    if(is_array($paths)){
        foreach($paths as $path){
            require_once ABSPATH.$path;
        }
    } else require_once ABSPATH.$paths;
}

function change_db_config($path =null){
    global $config;
    $config['db-config']['path'] = ($path) ? ABSPATH.$path:$config['db-config']['path'];
}

function redirect($path){
    header("Location: ".$path);
    exit(0);
}

function page_name(){
    $path_seg = path_segments();
    
    return  isset($path_seg[0]) ? $path_seg[0]:"home";
}

function getAdminPath(){
    global $main;
    return  base_url()."/".$main->getAdminName();
}



function debug($var){
    echo is_array($var) ? "<h4>This is Array</h4>":  is_object($var) ? "<h4>This is Object</h4>":"";
    if(is_array($var) || is_object($var)){
        Explain($var);
    }else if(is_bool($var)){
        echo "<h4>This is Bollean</h4>";
        if($var){
            echo "True";
        }else {
            echo "False";
        }
    }else {
        echo Explain($var, "dump");
    }
    exit(0);
}

function compare($value1 , $value2){
    if($value1 == $value2){
        echo "This is True!";
    }else {
        echo "This is False";
    }
}


function get_image_links($html, $get = "src"){
    preg_match_all('/<img[^>]+>/i',$html, $result);
    array_walk($result[0], "find_attributes", $get);
    return $result[0];
}

function find_attributes(&$arr, $key, $get){   
    $res = array();
    if(!is_array($get)){
        $sr = preg_match_all('/('.$get.')=("[^"]*")/i', $arr, $att);
        if(!$sr){
            $sr = preg_match_all('/('.$get.')=(\'[^\']*\')/i', $arr, $att);         
        }
        $attr = ($sr) ?  $att[2][0]:"";
        $attr_value = rtrim(ltrim($attr, "\""), "\"");
        $attr_value = rtrim(ltrim($attr_value, "'"), "'");
        $res[$key] = $attr_value;
    }else if(is_array($get)) {
        foreach($get as $key){
            $sr = preg_match_all('/('.$key.')=("[^"]*")/i', $arr, $att);
            if(!$sr){
                $sr = preg_match_all('/('.$key.')=(\'[^\']*\')/i', $arr, $att);
            }
            $attr = ($sr) ?  $att[2][0]:"";
            $attr_value = rtrim(ltrim($attr, "\""), "\"");
            $attr_value = rtrim(ltrim($attr_value, "'"), "'");
            
            $res[$key] = $attr_value;            
        }
    }
    $arr = $res;
}

function unique_string(){
    return md5(uniqid().microtime());
}

function _mpw($word, $num){ //mpw = make plural word
    if($num > 1){ //If the word is realy a plural number e.g. 2 cats, 5 boxes etc are the plural number 1 cat or 1 box is singular number
        switch($word[strlen($word) -1]){
            case "y":
                $w = rtrim($word,"y");
                return $w."ies";
                break;
            case "s":
            case"x":
                return $word."es";
                break;
            default :
                return $word."s";
        }
    }
    return $word;
}