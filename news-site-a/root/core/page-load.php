<?php
    $seg  = path_segments();
    $admim = get_config("site/admin");
    $theme_name = $main->getThemeName();
    $path = ""; 
    load()->lib("Page")->open();
    $page = false;
    
    if(count($seg) == 0 || $seg[0] === "index.php"){
         $path = ABSPATH . "themes/".$theme_name."/index.php";
        
    }else if($seg[0] === $admim){
        if(empty($seg[1]) && !isset($seg[1])){
             $path = ABSPATH . "themes/admin/index.php";
        }else {
            $php_path = ABSPATH . "themes/admin/".$seg[1].".php";
            
            $html_path = ABSPATH . "themes/admin/".$seg[1].".html";
            $path = (file_exists($php_path)) ? $php_path : ((file_exists($html_path)) ? $html_path : null);
            
            if(!$path){
                 $path =  ABSPATH . "themes/admin/404.php";
            }
        }
    }else  if(!empty($seg[0]) && isset($seg[0])){        
        $php_path = ABSPATH . "themes/".$theme_name."/".$seg[0].".php";
        $html_path = ABSPATH . "themes/".$theme_name."/".$seg[0].".html";
        $path = (file_exists($php_path)) ? $php_path:((file_exists($html_path)) ? $html_path : null);
        
        $page_ref = get_config("page/reference");
        
        if(!strcasecmp(page_name(), $page_ref)) {
            $path = false;
            if(isset($seg[1])){
                $page = new Page(1);
            }else {
                redirect(base_url());
            }
        }else {
            $page = new Page;
        }
        
        if(!$path){ 

            $cat_ref = get_config("category/reference");
            $tag_ref = get_config("tag/reference");
            $auth_ref = get_config("author/reference");
            
            if(!strcasecmp(page_name(), $cat_ref)){
                $path = ABSPATH . "themes/".$theme_name."/category.php";
                $path = (!file_exists($path)) ? ABSPATH . "themes/".$theme_name."/404.php":$path;
            }else if(!strcasecmp(page_name(), $tag_ref)){
                $path = ABSPATH . "themes/".$theme_name."/tag.php";
                $path = (!!file_exists($path)) ? ABSPATH . "themes/".$theme_name."/404.php":$path;
            }else if(!strcasecmp(page_name(), $auth_ref)){
                $path = ABSPATH . "themes/".$theme_name."/author.php";
                $path = (!!file_exists($path)) ? ABSPATH . "themes/".$theme_name."/404.php":$path;                
            }else{
                $path = ABSPATH . "themes/".$theme_name."/404.php";
            }
        }
    }
    if(file_exists($path)){
        if($page){
            if(!$page->print_content()){
                include $path;
            }
        }else {            
            include $path;
        }
    }else {
        exit("Error In Your Theme!<br>Check Your Theme");
    }
    
    
