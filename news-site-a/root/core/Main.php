<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'apps/configurations.php';
require_once 'apps/temp_function.php';

class Main{
    protected  $controller = "home";
    protected  $method = "index";
    protected  $pamams = array();

    
    public function __construct() {
        $path = $this->path_segments();
        if(isset($path[0])){
            if(file_exists(ABSPATH . "root/controllers/".$path[0].".php")){
                $this->controller = $path[0];
                unset($path[0]);
            }
        }
        require_once ABSPATH . "root/controllers/".$this->controller.".php";
        $this->controller = new $this->controller;
        if(isset($path[1])){
            if(method_exists($this->controller, $path[1])){
                $this->method = $path[1];
                unset($path[1]);
            }
        }
        $this->pamams = $path ? array_values($path):array();
        call_user_func_array(array($this->controller, $this->method), $this->pamams );
    }   
    

    
    protected function path_segments($index = false){
        if(!isset($_SERVER['PATH_INFO'])){

            $uri_split = explode('?',$_SERVER['REQUEST_URI']);       
            $rootURI = rtrim(str_ireplace($_SERVER['DOCUMENT_ROOT'], '', ABSPATH),'/');
            $paths = str_ireplace($rootURI, '',$uri_split[0]);

            $paths = rtrim(ltrim($paths,'/'),'/');
            if(strlen($paths) == 0){
                return array();
            }
            $path_split = explode('/',$paths);
            return $path_split;

        }else {
            $pathInfo = filter_var(ltrim($_SERVER['PATH_INFO'],'/'), FILTER_SANITIZE_URL);
            if(strlen($pathInfo) == 0){
                return array();
            }else {
                $segments = explode('/', $pathInfo);
                return $segments;
            }
        }
    }

}