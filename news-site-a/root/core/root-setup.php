<?php
require_once ABSPATH.'root/functions.php';
require_once ABSPATH.'root/db/db-tables.php';
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function connectionCheck($host, $user, $pass, $db){
    if(mysql_connect($host, $user, $pass, $db)){
        if(mysql_select_db($db)){
            return true;
        }
        return false;
    }
    return false;
}



function __rootDatabaseSetup(){
    
    $host = $_SESSION['host'];
    $user = $_SESSION['user'];
    $pass = $_SESSION['pass'];
    $db = $_SESSION['db'];
    
    global $tables;
    $status = array();
    $keys = array_keys($tables);
    
    //echo "Keys: <pre>".print_r($keys, true)."</pre>";
    
    $counter = 0;
    if(connectionCheck($host, $user, $pass, $db)){
        foreach($tables as $tb_name => $tb_attr){
            $sql = create_table($tb_name, $tb_attr);
            //echo $tb_name.": ".$sql.'<br>';       
            mysql_query("SET NAMES 'utf8'");

            if(mysql_query($sql)){
                $status[$keys[$counter]] = 'success';
            } else {
                $status[$keys[$counter]] = array($sql, mysql_error());
            }
            $counter++;
        }
        //$tb_sql = create_table("_user", $cols);
        
    }else return false;
    
    echo "<tt><pre>";
    print_r($status);
    echo "</pre></tt>";
    
    $host = $_SESSION['host'] = "";
    $user = $_SESSION['user'] = "";
    $pass = $_SESSION['pass'] = "";
    $db = $_SESSION['db'] = "";
}