<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MetaInfo
 *
 * @author Saif
 */
class MetaInfo {
    //put your code here
    
    private $_db,
            $_sql,
            $_meta_table,
            $_key_column,
            $_value_column,
            $_rel_column,
            $_db_status;
    
    public function __construct($table, $key_column, $value_column, $rel_column = false) {
        $this->_db = DB::run();
        $this->_meta_table = $table;
        $this->_key_column = $key_column;
        $this->_value_column = $value_column;
        $this->_rel_column = $rel_column;
    }
    
    public function get_status(){
        return $this->_db_status;
    }
    
    public function get(  $key, $info_rel = false ){
        $wh_value = array(
            $this->_key_column=> $key,
        );
        
        if($info_rel) $wh_value[$this->_rel_column] = $info_rel;
        
        $val =  $this->_db->read($this->_meta_table)->where($wh_value)->run_sql();
        
        if($val->get_count()){
            $m = $val->get_first();
            return $m[$this->_value_column];
        }return false;
    }
    
   
    public function add( $key, $value, $info_rel = false){
        if(!$this->is_exists($key, $info_rel)){
            $values= array(
                $this->_key_column => $key,
                $this->_value_column => $value,
            );
            
            if($info_rel !== false){
                $values[$this->_rel_column] = $info_rel;
            }
            $meta_insert = $this->_db->write($this->_meta_table)->values($values);
            $meta_insert->run_sql();
            return $this->status($meta_insert);
        }else {
            trigger_error($key ." Already Added. meta information can be added only first time with same name, you can edit this now insted!");
        }
    }
    
    private function status($db){
        $this->_db_status = $db;
        if($db->error()){
            return false;
        }else {
            return true; 
        }
    }

    private function where($key, $rel = false){
        
        if($rel !== false){
            return  array(
                $this->_rel_column => $rel,
                $this->_key_column => $key
            );
        }else {
            return  array(
                $this->_key_column => $key
            );
        }
    }
    
    public function edit( $key, $value, $info_rel = false){
        if($this->is_exists($key, $info_rel)){
            $values = array(
                $this->_value_column => $value,
            );
            
            if($info_rel !== false) {
                //$values[$this->_rel_column] = $info_rel;
                $meta_edit = $this->_db->edit($this->_meta_table)->values($values)->where($this->where($key,  $info_rel));
            }else {
                $meta_edit = $this->_db->edit($this->_meta_table)->values($values)->where($this->where( $key));
            }
            $meta_edit->run_sql();
            return $this->status($meta_edit);
        }
         
    }
    
    public function delete($key, $info_rel = false){
        if($this->is_exists( $key, $info_rel)){
            $meta_delete = $this->_db->delete($this->_meta_table)->where($this->where( $info_rel , $key));
            $meta_delete->run_sql();
            return $this->status($meta_delete);
        }else {
            exit("System Error!");
        }
    }
    
    public function remove( $key, $info_rel = false){
        if($this->is_exists($key, $info_rel)){
            $meta_remove = $this->_db->edit($this->_meta_table)->values($this->_value_column, null)->where($this->where( $info_rel , $key));
            $meta_remove->run_sql();
            return $this->status($meta_remove);
        }else {
            exit("System Error!");
        }
    }

    
    public function is_exists( $key, $rel = false){
        $ex_wher = array(
            $this->_key_column=> $key
        );
        
        
        if($rel)  $ex_wher[$this->_rel_column] = $rel;
        
        $val =  $this->_db->read($this->_meta_table)->where($ex_wher)->run_sql();
        return ($val->get_count()) ? true:false;
        
    }
    
    public function is_null( $key, $rel = false){
        
        $val =  $this->_db->read($this->_meta_table)->where(
                    array(
                        $this->_rel_column=> $rel,
                        $this->_key_column=> $key,
                    )
                )->run_sql();
       $res = $val->get_first();
       return (trim($res[$this->_value_column]) == "") ? true:false;
        //return ($val->get_count()) ? true:false;
    }
}
