<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cookie
 *
 * @author Saif
 */

class Cookie {
    public function check($name){
        return isset($_COOKIE[$name]) ? true : false;
    }

    public function get($name){
        return isset($_COOKIE[$name]) ? $_COOKIE[$name]:NULL;
    }

    public function add($name, $value, $expiry){
        if(setcookie($name, $value, time() + $expiry, '/' )){
            return true;
        }
        return false;
    }
    
    public function delete($name){
        self::add($name, '' , time() - 1); 
    }
}