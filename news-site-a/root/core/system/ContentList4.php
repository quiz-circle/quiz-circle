<?php

define("CONTENT_DEBUG", false);

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of ContentList
 *
 * @author Saif
 */


class ContentList {
    private $_sql, $_db,  $_load, $_pagination,
            $_per_page, $_pagination_start, $_is_tree,
            $_total_pages = 0, $_current_page = 1,
            $_id_comumn_name, $_parent_column_name;
    
    public $tag, $unique_id = false,
            $tag_start, $tag_end, $in_tag_start, $in_tag_end,
            $attributes,
            $first_level_tag = "",
            $limit, $start,
            $li_tag_inside = true, $list_order_by,
            $order_type, $active_elements,
            $active_column, $init_html,
            $pagination_config = array(),
            $_print_tag_in_tree,
            $active_param;
    
    protected  $_data, $_total_elem = 0, $_elem_per_page,
            $_tree_params = array(), $_result = null,
            $_each = false,
            $_each_config = false;

    public function __construct($sql, $id_column_name = '', $parent_column_name = '') {
        $this->_db = DB::run();
        $this->_load = load();
        $this->_sql = $sql;
        $this->_id_comumn_name = $id_column_name;
        $this->_parent_column_name = $parent_column_name;
        $this->_total_elem = $this->_db->setSql($this->_sql)->run_sql()->get_count();
    }

    
    public  function data_limit($number, $start = false){
        $this->limit = $number;
        $this->start = $start ? $start:"0";
    }
    
    public function init_html($html){
        $this->init_html = $html;
    }
    
    public function set_($attr) {
        $this->attributes = $attr;
        return $this;
    }

    public function hasChild($id) {
        $array = $this->_data;
        $result = 0;
        foreach ($array as $key => $val) {
            if ($val[$this->_parent_column_name] == $id) {
                $result++;
            }
        }
        return $result !== 0;
    }

    public function set_pagination_conf($config) {
        $this->pagination_config = $config;
    }

    public function set_pagination($per_page, $query_name = "page") {
        $pagenate = $this->_load->sys("Pagination")->run();
        $this->_per_page = $per_page;
        $pagenate->set_config($this->pagination_config);
        $pagenate->number_of_elements($this->_total_elem);
        $pagenate->per_page($per_page);
        $pagenate->variable_name($query_name);

        $this->_current_page = isset($_GET[$query_name]) ? $_GET[$query_name] : 1;
        $this->_total_pages = $pagenate->total_pages();
        $this->_pagination_start = $pagenate->start();
        $this->_pagination = $pagenate->show();
        return $this;
    }

    public function total_pages() {
        return $this->_total_pages;
    }

    public function current_page() {
        return $this->_current_page;
    }

    public function each($element, $config = false) {
        if( is_object($element)) $this->_each = $element;
        //if(function_exists($config)) $this->_each_config = $config;
        return $this;
    }

    public function active_elements($elements) {
        $this->active_elements = $elements;
    }

    protected function set_active_li($arr, &$html_list, $unique_class) {    
        
    }

    
    protected  function make_list_tree($root = 0, $level = 0) {
        $level++;
        $index = 0;
        $func = $this->_each;
        
        if ($this->_total_elem && $func) {
            $this->_result .= $this->_outside_result($func($level, $this, null, null, null), $outer_tag, $wrap);
            $this->_result .= ($level == 2) ? "<li>abcd":"";
            $this->_result .= ($level == 2) ? "</li>":"";

            foreach ($this->_data as $key => $val) {
                $unique_class = _unique_id();
                
                if ($this->_id_comumn_name !== "" && $this->_parent_column_name !== "") {
                    if ($this->_parent_column_name == $root) {
                        $this->_result .= "<li>";
                        $this->_result .= $this->_inside_result($func($val, $this->active_param, $level, $index, $this), $wrap, $each_tag_end);
                        if ($this->hasChild($val[$this->_id_comumn_name])) {
                            $this->make_list_tree($val[$this->_id_comumn_name], $level);
                        }
                        $this->_result .= "</li>";
                        //$this->_result .= $each_tag_end;
                    }
                }
                $index++;
            }
            $this->_result .= "End: "."</".$outer_tag.">";
        }
    }
    
    private function _inside_result($each, $wrap, &$each_tag_end = ""){
        if($wrap === true){
            $user = strstr($each, '>', true);
            $tag_using = ltrim($user, "<");
            $r = rtrim(trim($each), ">");
            $r = rtrim(trim($r), $tag_using);
            $r = rtrim(trim($r), "/");
            $r = rtrim(trim($r), "<");
            $each_tag_end = "</".$tag_using.">";
            return $each;
            return $r;
        }
        $each_tag_end = "";
        return $each;
    }
    
    private function _outside_result($outer, &$outer_html, &$wrap){
        if(is_array($outer) && count($outer) >= 1){
            if(isset($outer['html']) && !empty($outer['html'])) {
                $attrs = "";
                $attributes = isset($outer['attrs']) && !empty($outer['attrs']) ? $outer['attrs'] : "";
                
                if(is_array($attributes)){
                    foreach ($attributes as $attribute => $value){
                        $attrs .= " " . $attribute . "='" . $value . "' ";
                    }
                }else {
                    $attrs = $attributes;
                }
                $wrap = true;
                $outer_html = $outer['html'];
                return "<" . $outer['html'] ." ". $attrs . ">";
            }
            $outer_html = "";
            $wrap = false;
            return "";
        }
        $outer_html = "";
        $wrap = false;
        return "";
    } 
    
    public function is_tree($condition) {
        $this->_is_tree = $condition;
    }

    public function get_data($order_by = "", $order_type = "", $split = ""){
        return $this->_data($order_by, $order_type);
    }   
    
    public function show_data($order_by = "", $order_type = ""){
        echo $this->_data($order_by, $order_type);
    }

    private function _data($order_by, $order_type) {
        $this->_data = $this->_db->setSql($this->_sql);
        
        $pn_start = $this->_pagination_start;
        $pn_per_page = $this->_per_page;

        if ($order_by && $order_type) {
            $this->_data->order_by($order_by, $order_type);
        } else if ($order_by && $order_type == '') {
            $this->_data->order_by($order_by);
        }

        if ($pn_per_page) {
            $this->_data->limit($pn_per_page, $pn_start);
        }
            
        if($this->limit){
            if($this->start){
                $this->_data->limit($this->limit, $this->start);
            }else {
                $this->_data->limit($this->limit);
            }
        }
        
        $this->_sql = $this->_data->get_sql();
        
        $this->_data = $this->_data->run_sql();
        
        if(CONTENT_DEBUG) echo $this->_data->get_sql()."<br>".$this->_data->sql_error();
        
        
        $this->_data = $this->_data->get_array();
        
        $this->prepare_result();
        return $this->_result;
    }
    
    public function get_sql(){
        return $this->_sql;
    }

    private function prepare_result() {
        
        if (!$this->_is_tree) {
            $index = 0;
            $func = $this->_each;
            if ($this->_total_elem && $func) {
                
                $this->_result .= ($this->tag_start != "") ? $this->tag_start." >":"";
                
                $this->_result .= $this->init_html;
                foreach ($this->_data as $key => $val) {
                    $this->_result .= $func($val, $this->active_param, $index);
                    $index++;
                }
                $this->_result .= $this->tag_end;
            }
        } else {
            $tag_end = "";
            
            if($this->tag_start != ""){
                if($this->first_level_tag){        
                    $this->_result .= "<".$this->first_level_tag. ">";
                    $tag_end = explode(" ", $this->first_level_tag);
                    $tag_end = isset($tag_end[0]) ? "</".$tag_end[0].">":"";
                }else {
                    $this->_result .= $this->tag_start." ".$attrs.">";
                    $tag_end = $this->tag_end;
                }
            }
            
            $this->_result .= $this->init_html;
                $this->make_list_tree();
            $this->_result .= $tag_end;
        }
    }

    public function get_pagination() {
        return $this->_pagination;
    }

    public function get_total() {
        return $this->_total_elem;
    }
}
