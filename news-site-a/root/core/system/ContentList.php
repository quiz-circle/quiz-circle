<?php

define("CONTENT_DEBUG", false);

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of ContentList
 *
 * @author Saif
 */

class ContentList {
    private $_sql, $_db,  $_load, $_pagination,
            $_per_page, $_pagination_start, $_is_tree,
            $_total_pages = 0, $_current_page = 1;
    
    public $tag, $unique_id = false,
            $tag_start, $tag_end, $in_tag_start, $in_tag_end,
            $attributes,
            $first_level_tag = "",
            $limit, $start,
            $li_tag_inside = true, $list_order_by,
            $order_type, $active_param,
            $active_key, $init_html,
            $pagination_config = array(),
            $_print_tag_in_tree;
    
    protected  $_data, $_total_elem = 0, $_elem_per_page,
            $_tree_params = array(), $_result = null,
            $_per_elem_operation = false;

    public function __construct($sql, $list_id_key = '', $parent_key = '') {
        $this->_db = DB::run();
        $this->_load = load();
        $this->_sql = $sql;
        $this->_tree_params[0] = $list_id_key;
        $this->_tree_params[1] = $parent_key;
        $this->_total_elem = $this->_db->setSql($this->_sql)->run_sql()->get_count();
    }
    
    public function first_level_tag($tag){
        $this->first_level_tag = $tag;
    }
    
    public  function data_limit($number, $start = false){
        $this->limit = $number;
        $this->start = $start ? $start:"0";
    }


    public function init_html($html){
        $this->init_html = $html;
    }
    
    public function setAttrs($attr) {
        $this->attributes = $attr;
        return $this;
    }

    public function hasChild($id) {
        $array = $this->_data;
        $result = 0;
        foreach ($array as $key => $val) {
            if ($val[$this->_tree_params[1]] == $id) {
                $result++;
            }
        }
        return ($result == 0) ? false : true;
    }

    public function set_html($type, $is_tree = false, $inside_tag = "") {
        //$this->_is_tree = $is_tree;

        $this->tag = $type;
        switch (strtolower($type)) {
            case "ul":
                $this->tag_start = "<ul";
                $this->tag_end = "</ul>";
                $this->_print_tag_in_tree = $is_tree;
                if(($is_tree)){
                    $this->in_tag_start = "<li>";
                    $this->in_tag_end = "</li>";
                }
                break;
            case "ol":
                $this->tag_start = "<ol ";
                $this->tag_end = "</ol>";
                $this->_print_tag_in_tree = $is_tree;
                if(($is_tree)){
                    $this->in_tag_start = "<li>";
                    $this->in_tag_end = "</li>";
                }
                break;
            case "":
                $this->_print_tag_in_tree = $is_tree;
                $this->tag_start = "";
                $this->tag_end = "";
                if(($is_tree)){
                    $this->in_tag_start = "<".$inside_tag." ";
                    $this->in_tag_end = "</".$inside_tag.">";
                }
                break;
            default :
                $this->_print_tag_in_tree = $is_tree;
                $this->tag_start = "<" . $type . " ";
                $this->tag_end = "</" . $type . ">";
                
                if(($is_tree)){
                    $this->in_tag_start = "<".$inside_tag." ";
                    $this->in_tag_end = "</".$inside_tag.">";
                }
        }
        return $this;
    }

    public function set_pagination_conf($config) {
        $this->pagination_config = $config;
    }

    public function set_pagination($per_page, $query_name = "page") {
        $pagenate = $this->_load->sys("Pagination")->run();
        $this->_per_page = $per_page;
        $pagenate->set_config($this->pagination_config);
        $pagenate->number_of_elements($this->_total_elem);
        $pagenate->per_page($per_page);
        $pagenate->variable_name($query_name);

        $this->_current_page = isset($_GET[$query_name]) ? $_GET[$query_name] : 1;
        $this->_total_pages = $pagenate->total_pages();
        $this->_pagination_start = $pagenate->start();
        $this->_pagination = $pagenate->show();
        return $this;
    }

    public function total_pages() {
        return $this->_total_pages;
    }

    public function current_page() {
        return $this->_current_page;
    }

    public function set_elemets_oper($function) {
        $func = (!is_string($function) &&
                !is_int($function) &&
                !is_float($function) &&
                !is_double($function) &&
                !is_bool($function) &&
                !is_array($function)
                ) ? $function : null;

        $this->_per_elem_operation = $function;
        return $this;
    }

    public function set_active_param($value, $parameter = "") {
        $this->active_param = $value;
        $this->active_key = $parameter;
    }

    protected function set_active_li($arr, &$html_list, $unique_class) {
        if ($this->_print_tag_in_tree){
            //if(strcasecmp($this->tag, "ul") == 0 || strcasecmp($this->tag, "ol") == 0) {
                $inside_tag = rtrim($this->in_tag_start, ">");
                
                $inside_tag .= ($this->unique_id) ?  " id='".$unique_class."' ":"";
                
                if (array_key_exists($this->active_key, $arr)) {
                    $inside_tag .= ($arr[$this->active_key] == $this->active_param ) ? " class='active'" : "";
                }
            //}else {
                
            //}
            $html_list = true;
            $inside_tag .= ">";
            return $inside_tag;
        }
        return "";
    }
    
    function li_tag_inside($cond){
        $this->li_tag_inside = $cond;
    }
    
    
    private  function _get_attributes($level = ""){        
        $attrs = "";
        if(is_array($this->attributes) && isAssoc($this->attributes)){
            if(count($this->attributes)){
                foreach($this->attributes as $attr=> $value){
                    $attrs .= $attr ."=\"" .$value. "\" ";
                }
            }
        }elseif (!isAssoc($this->attributes)) {
            $attribures = isset($this->attributes[$level]) ? $this->attributes[$level]:array();
            if(count($attribures)){
                foreach($attribures as $attr=> $value){
                    $attrs .= $attr ."=\"" .$value. "\" ";
                }
            }
        }else {
            $attrs .= $this->attributes;
        }
        return $attrs;
    } 


    protected  function make_list_tree($root = 0, $level = 0) {
        $attrs = $this->_get_attributes($level);
        
        $level++;
        $index = 0;
        $func = $this->_per_elem_operation;
        if ($this->_total_elem && $func) {
            
            $inside_tag = "";
            if ($this->_print_tag_in_tree && $root != 0){
                $this->_result .= $this->tag_start !== ""  ?   $this->tag_start." " . $attrs . " >":"";
            }
            
            $html_list = false;
            
            //Explain($this->_data, "export");
            foreach ($this->_data as $key => $val) {
                $unique_class = _unique_id();
                if (array_key_exists($this->_tree_params[0], $val)) {
                    if (count($this->_tree_params) == 2) {
                        if ($val[$this->_tree_params[1]] == $root) {
                            if($this->li_tag_inside){
                                $this->_result .= $this->set_active_li($val, $html_list, $unique_class);
                            }
                            $this->_result .= $func($val, $this->active_param, $level, $index, $this);
                            if ($this->hasChild($val[$this->_tree_params[0]])) {
                                $this->make_list_tree($val[$this->_tree_params[0]], $level);
                            }
                            if($this->li_tag_inside){
                                $this->_result .= ($html_list) ? $this->in_tag_end : "";
                            }
                        }
                    }
                }
                $index++;
            }
            if ($this->_print_tag_in_tree && $root != 0)
                $this->_result .= $this->tag_end;
        }
    }

    public function set_is_tree($condition) {
        $this->_is_tree = $condition;
        return $this;
    }

    public function get_data($order_by = "", $order_type = "", $split = ""){
        return $this->_data($order_by, $order_type);
    }   
    
    public function show_data($order_by = "", $order_type = ""){
        echo $this->_data($order_by, $order_type);
    }

    private function _data($order_by, $order_type) {
        
        $this->_data = $this->_db->setSql($this->_sql);
        
        $pn_start = $this->_pagination_start;
        $pn_per_page = $this->_per_page;

        if ($order_by && $order_type) {
            $this->_data->order_by($order_by, $order_type);
        } else if ($order_by && $order_type == '') {
            $this->_data->order_by($order_by);
        }

        if ($pn_per_page) {
            $this->_data->limit($pn_per_page, $pn_start);
        }
            
        if($this->limit){
            if($this->start){
                $this->_data->limit($this->limit, $this->start);
            }else {
                $this->_data->limit($this->limit);
            }
        }
        
        $this->_sql = $this->_data->get_sql();
        
        $this->_data = $this->_data->run_sql();
        
        if(CONTENT_DEBUG) echo $this->_data->get_sql()."<br>".$this->_data->sql_error();
        
        
        $this->_data = $this->_data->get_array();
        
        $this->prepare_result();
        return $this->_result;
    }
    
    public function get_sql(){
        return $this->_sql;
    }

    private function prepare_result() {
        $attrs = $this->_get_attributes(0);
        
        if (!$this->_is_tree) {
            $index = 0;
            $func = $this->_per_elem_operation;
            if ($this->_total_elem && $func) {
                
                $this->_result .= ($this->tag_start != "") ? $this->tag_start." ".$attrs.">":"";
                
                $this->_result .= $this->init_html;
                foreach ($this->_data as $key => $val) {
                    $this->_result .= $func($val, $this->active_param, $index);
                    $index++;
                }
                $this->_result .= $this->tag_end;
            }
        } else {
            $tag_end = "";
            
            if($this->tag_start != ""){
                if($this->first_level_tag){        
                    $this->_result .= "<".$this->first_level_tag. ">";
                    $tag_end = explode(" ", $this->first_level_tag);
                    $tag_end = isset($tag_end[0]) ? "</".$tag_end[0].">":"";
                }else {
                    $this->_result .= $this->tag_start." ".$attrs.">";
                    $tag_end = $this->tag_end;
                }
            }
            
            $this->_result .= $this->init_html;
                $this->make_list_tree();
            $this->_result .= $tag_end;
        }
    }

    public function get_pagination() {
        return $this->_pagination;
    }

    public function get_total() {
        return $this->_total_elem;
    }
}
