<?php
define("USER_DEBUG", false);

class User {
    
    protected   $_db,
                $_data,   
                $_sessionName,
                $_cookieName,
                $_sess,
                $_cookie,
                $_user_info,
                $_usr_key,
                $_isLoggedIn = false;
    
    public $message;
    
    
    public function __construct($email = false) {
        global $db;
        $this->_db = $db;
        $this->_sess = loadCore('session');
        $this->_cookie = loadCore('cookie');
        $this->_usr_key = get_config("meta_attr/usr");
        $this->_sessionName = get_config('session/session-name');
        $this->_cookieName= get_config('remember/cookie-name');
        $this->_user_info = load()->sys("userInfo")->run(0);
        
        $sess = $this->getSession();
        
        if(!$email){
            if($this->_sess->check($this->_sessionName)){
                $email = $sess[0];
                if($this->find($email)){
                        $this->_isLoggedIn = true;
                }else {
                    $this->logout();
                }
            }
        }else {
            $this->find($email);
        }
    }
    
    public function find($email){
        
        $id = 0;
        $field = (is_numeric($email)) ? 'user_id':'email';
        $users = $this->_db->read('user')->where($field, $email)->run_sql();
        
        
        if($users->get_count()){            
            $this->_data = $users->get_first();
            $id = $this->_data['user_id'];
            $this->_user_info = load()->sys("userInfo")->run($id);
            if(USER_DEBUG) echo "Test Case: E &raquo; ".$id;
            return true;
        }else {
            if($field === 'email'){
                $users = $this->_db->read('user')->where('username', $email)->run_sql();
                
                if($users->get_count()){
                    $this->_data = $users->get_first();
                    $id = $this->_data['user_id'];
                    
                    $this->_user_info = load()->sys("userInfo")->run($id);
                    if(USER_DEBUG) echo "Test Case: D &raquo; ".$id;
                    return true;
                }
                
                if(USER_DEBUG) echo "Test Case: A &raquo; ".$id;
                return false;
            }else {
                $this->_remove_anonymous_user_session($email);

                $page_name = $GLOBALS['login_page_name'];
                
                $login_link = admin_link()."/$page_name?back_to=" . full_url();
                redirect($login_link);
                
                if(USER_DEBUG) echo "Test Case: B &raquo; ".$id;
                return false;
            }
        }  
        if(USER_DEBUG) echo "Test Case: C &raquo; ".$id;
        return false;
    }
    
    private function _remove_anonymous_user_session(){
        $this->_sess->delete($this->_sessionName);
        $this->_cookie->delete($this->_cookieName);
        $this->_cookie->delete(get_config('remember/cookie-value'));
    }

    public function exists(){
        return $this->_data ? true : false;
    }

    public function isLoggedIn(){
        return $this->_isLoggedIn;
    }
    
    
    public function logout(){
        if($this->isLoggedIn()){
            $this->_sess->delete($this->_sessionName);
            $this->_cookie->delete($this->_cookieName);
            $this->_cookie->delete(get_config('remember/cookie-value'));

            if($this->_user_info->exists($this->_usr_key['sess_name_key'])){
                
                $this->_user_info->remove($this->_usr_key['sess_name_key']);
            }
        }
    }
    
    public  function getType(){
        if(count($this->_data)){
            //return $this->_user_info->get('user_type');
            $sess = $this->getSession();
             return (isset($sess[1])) ? $sess[1]:"";
        }
        return false;
    }
    
    public function isAdmin(){
        $cd = json_decode($this->_user_info->get(get_config("meta_attr/usr/cap")), true);
        
        //Explain($cd);
        
        //echo "<em>".md5("type") ."</em>";
        
        $admin = $this->_user_info->get($this->_usr_key['utype_key']);
        if(is_array($cd)){
            if( array_key_exists(md5("type"), $cd) ) {
                return (($cd[md5("type")]['a']['u'])) === md5($this->getID()) 
                && (md5(count($cd[md5("type")]['a'])) === $cd[md5("type")]['b'])
                && strtolower($this->getType()) === strtolower($admin);
            }
            return false;
        }
        return false;
        
        //return (strtolower($this->getType()) === strtolower($admin));   
    }
    
    
    public function hasControlAccess(){
        $public_key = get_config("meta_attr/usr/public_user_key");
        if(strcasecmp($public_key, $this->getType()) == 0){
            return false;
        }
        return true;
    }
    
    public  function getID(){
        if(count($this->_data)){
            $sess = $this->getSession();
            return $sess[0];
        }
        return false;
    }

    public function login($email = false, $password = false, $remember = false){
        $user_type_key = $this->_usr_key['utype_key'];
        
        if(!$email && !$password){
            
            $sess_data_array = array($this->_data['user_id']);
            if(is_object($this->_user_info)){
                if( $this->_user_info->is_exists($user_type_key)){
                    $sess_data_array[] = $this->_user_info->get($user_type_key);
                }
                $session_datas  = $this->_encrypt_values($sess_data_array, $values);
                $this->_sess->add($this->_sessionName, $values);
                Explain($this->getSession());
            }else {
                $this->_sess->delete($this->_sessionName);
            }
            echo $this->_sess->get($this->_sessionName);

        }else {
            $hash = load()->sys('hash')->run();
            $data = load()->sys('data')->run();
            $email = (is_numeric($email)) ? false:$this->find($email);

            if($email){
                if($this->_data['password'] === $hash->make($password, $this->_data['salt'])){
                    $session_datas  = $this->_encrypt_values(
                        array(
                            $this->_data['user_id'],
                            $this->_user_info->get('user_type')
                        )
                    , $values);
                    $this->_sess->add($this->_sessionName, $values);

                    if($remember){
   
                        $cookie_expiry = get_config('remember/cookie_expire');
                        $cookie_val_name = get_config('remember/cookie-value');
                        $hash_val = $hash->unique();
                        
                        if($this->_user_info->is_empty($this->_usr_key['sess_name_key'])){
                            
                            if( !$this->_user_info->exists($this->_usr_key['sess_name_key']) ){
                                $this->_user_info->add($this->_usr_key['sess_name_key'], $hash_val);
                            }else {
                                $this->_user_info->edit($this->_usr_key['sess_name_key'], $hash_val);
                            }
                        }
                        $cookie_values = $this->_hash_check();
                        if(!$cookie_values){   
                            $this->_encrypt_values(
                                array(
                                    'user_id'=> $this->_data['user_id'], 
                                    'hash'=> $hash_val
                            ), $values);
                            $this->_cookie->add($cookie_val_name, $values, $cookie_expiry);
                            
                        }else {
                            $hash_val = $cookie_values[1];
                        }
                        
                        $this->_cookie->add($this->_cookieName, $hash_val, $cookie_expiry);
                    }
                    return true;
                }
            }
            return false;
        }
    }
    
    public function create($array, &$message, &$user_id){
        
        $this->_makePassword($array['password'], $salt);
        $create_array = array(
            'username'          => $array['username'],
            'display_name'      => $array['display_name'],
            'email'             => $array['email'],
            'password'          => $array['password'],
            'created_date'      => date("Y-m-d H:i:s"),
            'modified_date'     => date("Y-m-d H:i:s"),
            'salt'              => $salt,
        );
        
        $create = $this->_db->write('user')->values($create_array)->run_sql();
        
        if($create->error()){
            $message = $create->sql_error();
            return false;
        }else {
            $message = $create->message();
            $user_id = $create->last_insert_id();
            return true;
        }
    }
    
    private function _makePassword(&$pass, &$salt){
        $hash = loadCore("Hash");
        $salt = $hash->salt(32);
        $pass = $hash->make($pass, $salt);
    }
    
    private function _encrypt_values($uservalues, &$values){
        $data = loadCore('data');
        $values = $data->encrypt(implode(get_config('encryption/array-spliter'), $uservalues));
    }
    
    private function _hash_check(){
        $user_id = $this->_data['user_id'];
        $cookie_values = $this->getCookie();
        return ($cookie_values[0] == $user_id) ? $cookie_values : null;
    }
    
    public function getCookie(){
        $cookie_val_name = get_config('remember/cookie-value');
        if($this->_cookie->check($cookie_val_name )){
            $data = loadCore('data');
            return explode(get_config('encryption/array-spliter'),$data->decrypt($this->_cookie->get($cookie_val_name)));
        }
        return null;
    }
    public function getSession(){
        if($this->_sess->check($this->_sessionName)){
            $data = loadCore('data');
            return explode(get_config('encryption/array-spliter'),$data->decrypt($this->_sess->get($this->_sessionName)));
        }
        return null;
    }
}
