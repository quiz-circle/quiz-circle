<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Hash
 *
 * @author Saif
 */
class Hash{
    public function make($string, $salt = ""){
        return hash('sha256', $string.$salt);
    }

    public function salt($length){
        return str_replace(array('+','/'), array('p','s'), substr(base64_encode(mcrypt_create_iv($length)),0,$length));
    }

    public function unique(){
        return $this->make(uniqid() . microtime());
    }
}