<?php



class Content{
    private $_db,
            $_db_instance,
            $_table,
            $_result,
            $_redirect_pages = array(),
            $_failed_messages = array(),
            $_find_params,
            $_param_value,
            $_db_param,
            $_param_value_found = false,
            $_after_failed_funcs = array(),
            $_cond = false;
    
    public  $message;
    
    public function __construct($table, $column = null, $find_parameter = null) {
        $this->_db = DB::run();
        $this->_db->setSql("");
        $this->_table = $table;
        $this->_db_param = $column;
        $this->_find_params = $find_parameter;
    }
    
    public function set_redirect_path($path, $not_found_key){
        $this->_redirect_pages[$not_found_key] = $path;
        return $this;
    }

    public function set_failed_message($message, $failed_key, $callback = null){
        $this->_failed_messages[$failed_key] = $message;
        $this->_after_failed_funcs[$failed_key] = $callback;
        return $this;
    }
    
    private function check_find_params( $action = false, $id = 0){
        
        if ($this->_find_params !== null) {
            if(SEG_VARS($this->_find_params)){                
                $id = SEG_VARS($this->_find_params);
            }else if (isset($_GET[$this->_find_params]) && !empty($_GET[$this->_find_params])) {
                $id = $_GET[$this->_find_params];
            } else {
                if ($action) {
                    $this->message = $this->message("query");
                    $this->redirect_to("query");
                    return false;
                } else {
                    return false;
                }
            }
        }

        $this->_param_value = $id;
        $this->_db_instance = $this->_db->read($this->_table)->where($this->_db_param, $id)->run_sql();
        if($this->_db_instance->get_count()){
            $this->_param_value_found = true;
            return true;
        }else {
            if($action){
                $this->message = $this->message("db");
                $this->redirect_to("db");
                return false;
            }else {
                return false;
            }
        }
		
    }
    
    protected function redirect_to($key){
        if(count($this->_redirect_pages)){
            if(array_key_exists($key, $this->_redirect_pages)){
                echo $key;
                redirect($this->_redirect_pages[$key]);
            }
        }
    }

    protected function message($key){
        if(count($this->_failed_messages)){
            if(array_key_exists($key, $this->_failed_messages))
                $func = $this->_after_failed_funcs[$key];
                
                if($func) $func($this->_param_value, $this);
                return $this->_failed_messages[$key];
        }
    }

    public function read(){
        $this->check_find_params(true);
        if($this->_param_value_found){
            $this->_result = $this->_db_instance->get_first();
            return $this->_result;
        }
    }
    
    public function update($values){
        $this->check_find_params(true);
        
        if($this->_param_value_found){
            if($this->_cond){
                $this->_db_instance = $this->_db->edit($this->_table)->values($values)->where($this->_db_param, $this->_param_value);
               
                $edit = $this->_db_instance->run_sql();
                
                if($edit->error()){
                    $this->message = $this->message("edit_db_error");
                }else {
                    if($edit->has_changed()){
                        $this->message = $this->message("udpate_success");
                    }else {
                        $this->message = $this->message("updated");
                    }
                }	
            }else {
                $this->message = $this->message("edit_cond");
            }
        }
    }
    
    public function delete(){
        $this->check_find_params(true);
        
        if($this->_param_value_found){
            if($this->_cond){
                $this->_db_instance = $this->_db->delete($this->_table)->where($this->_db_param, $this->_param_value);
               
                $delete = $this->_db_instance->run_sql();
                echo $this->_db_instance->get_sql();
                echo "<br>";
                echo $this->_db_instance->sql_error();
                
                if($delete->error()){
                    $this->message = $this->message("del_db_error");

                }else {
                    $this->message = $this->message("del_success");
                    $this->redirect_to("del_success");
                    
                }		
				
            }else {
                $this->message = $this->message("delete_cond");
            }
        }
    }
    
    public function exists($id){
        $this->check_find_params(false, $id);    
            return $this->_param_value_found;
    }
    
    public function get_message(){
        echo $this->message;
    }
    
    
    
    public function before_action($cond){
        if(is_bool($cond)){
            $this->_cond = $cond;
        }else{
            $this->check_find_params(false);
            //$this->_cond = true;
            $this->_cond = $cond($this->_db_instance);
        }
    }
    
    public function set_condition($cond){
        if(is_bool($cond)){
            $this->_cond = $cond;
        }else{
        $this->check_find_params(false);
            //$this->_cond = true;
            echo "Sec Condition Called!";
            Explain($this->_db_instance);
            $this->_cond = $cond($this->_db_instance);
        }
    }
}
