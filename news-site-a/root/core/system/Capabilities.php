<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once ABSPATH . 'root/core/system/User.php';

class Capabilities extends User{
    private $_cap_data, $_user_id;
    protected $_u_info;
    public  $user_capabilities_key, 
            $cap_conf;
    public function __construct($user_id = ""){
        parent::__construct();
        if($this->isLoggedIn()){
            $this->_user_id = $user_id === "" ? $this->getID() : $user_id;
            load()->sys("UserInfo")->open();
            
            $this->cap_conf = get_config("cap");
            
            $this->_u_info = new UserInfo($this->_user_id);
            
            $this->user_capabilities_key = get_config("meta_attr/usr/cap");
            
            $this->_cap_data = json_decode($this->_u_info->get($this->user_capabilities_key), true);
            //Explain($this->_cap_data);
        }
    }
    
    public function has($key){
        if(!parent::isAdmin()){
            if(count($this->_cap_data)){
                if(array_key_exists(md5($this->cap_conf[$key]), $this->_cap_data)){
                    $u_type = $this->_u_info->get('user_type');
                    $nca = $this->_cap_data[md5($this->cap_conf[$key])]['a'];
                    $un = $this->_cap_data[md5($this->cap_conf[$key])]['a']['u'];
                    $ut = isset($this->_cap_data[md5($this->cap_conf[$key])]['a']['e']) ? $this->_cap_data[md5($this->cap_conf[$key])]['a']['e']:'';
                    $ncb = $this->_cap_data[md5($this->cap_conf[$key])]['b'];
                    return (md5(count($nca)) == $ncb && md5($this->_user_id) == $un && md5("yes+no".$u_type) == $ut);
                }
                return false;
            }
            return false;
        }
        return true;
    }
    
    public function add($key, $cap = "") {
        $sql_d = "";
        if(parent::isAdmin()){
            if(!$this->isAdmin()){
                if(array_key_exists($key, $this->cap_conf)){
                    $this->_cap_data[md5($this->cap_conf[$key])] = array('a'=> $this->_cap_values($cap, $c), 'b'=> md5($c));
                    if( !$this->_u_info->exists($this->user_capabilities_key) ){
                        $this->_u_info->add($this->user_capabilities_key, json_encode($this->_cap_data));
                    }else {
                        $this->_u_info->edit($this->user_capabilities_key, json_encode($this->_cap_data));
                    }
                }
            }else {
                if($cap === "" && is_numeric($key)) $this->_cap_data[md5("type")] = array('a'=> $this->_cap_values(0, $c), 'b'=> md5($c));
                
                if( !$this->_u_info->exists($this->user_capabilities_key) ){
                    $this->_u_info->add($this->user_capabilities_key, json_encode($this->_cap_data));
                }else {
                    $this->_u_info->edit($this->user_capabilities_key, json_encode($this->_cap_data));
                }
                /*
                $sql_d = $this->_u_info->get_status();
                echo "SQL: ".$sql_d->get_sql();
                */
                return true;
                _alert("Success!");
            }
            
        }else {
            echo  "<script>alert('Sorry you are not admin. Only admin can change capabilities!')</script>";
            return false;
        }
    }
    
    public function isAdmin() {
        return (strtoupper($this->_u_info->get('user_type')) === "ADMIN");
    }
    
    public function edit($key, $cap ) {
        if(parent::isAdmin()){
            if(!$this->isAdmin()){
                if(count($this->_cap_data)){
                    if(array_key_exists(md5($this->cap_conf[$key]), $this->_cap_data)){
                        $this->_cap_data[md5($this->cap_conf[$key])] = array('a'=> $this->_cap_values($cap, $c), 'b'=> md5($c));
                        $this->_u_info->edit($this->user_capabilities_key, json_encode($this->_cap_data));
                        return true;
                    }
                    return false;
                }
                return false;
            }else {
                if($cap === "" && is_numeric($key)) $this->_cap_data[md5("type")] = array('a'=> $this->_cap_values(0, $c), 'b'=> md5($c));
                
                if( !$this->_u_info->exists($this->user_capabilities_key) ){
                    $this->_u_info->add($this->user_capabilities_key, json_encode($this->_cap_data));
                }else {
                    $this->_u_info->edit($this->user_capabilities_key, json_encode($this->_cap_data));
                }
                return true;
                _alert("Success!");
            }
            return false;
        }else {
            echo  "<script>alert('Sorry you are not admin. Only admin can change capabilities!')</script>";
            return false;
        }
        
    } 
    
    private function _cap_values($cap, &$c) {
        $array = array();
        $u_type = $this->_u_info->get('user_type');
        $c = rand(3,5);
        $id = rand(10, 99);
        $array['c'] = $c;
        
        if(is_numeric($cap)){
            if($cap === 0){
                $array['e'] = md5("yes+no" . $u_type);
            }
        }elseif ($cap === "yes") $array['e'] = md5("yes+no" . $u_type);
        
        if($c > 3){
            switch($c){
                case 4:
                    $array['m'] = rand(100, 120);
                    break;
                case 5:
                    $array['m'] = rand(100, 120);
                    $array['n'] = rand(121, 140);
                    break;
            }
        }
        $array['u'] = md5($this->_user_id);
        return $array;
    }
    
    public function make_empty(){
        
    }
}