<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Session
 *
 * @author Saif
 */
class Session {
    
    public function check($name){
        if(isset($_SESSION[$name])){
            return true;
        }
        return false;
    }
    
    public function add($name, $value){
        return $_SESSION[$name] = $value;
    }
    
    public function delete($name){
        if($this->check($name)){
            $_SESSION[$name] = null;
        }
    }
    
    public function get($name){
        if($this->check($name)){
            return $_SESSION[$name];
        }
        return false;
    }
    
    public function flush($name){
        if(Session::check($name)){
            $temp = $_SESSION[$name];
            Session::delete($name);
            return $temp;
        }
        return false;
    }
}
