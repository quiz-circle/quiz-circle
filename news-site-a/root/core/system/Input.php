<?php
/**
 * Description of Input
 *
 * @author Saif
 */
class Input {
    //put your code here
    public static function exists($type = 'post'){
        switch($type){
            case 'post':
                return (!empty($_POST)) ? true:false;
                break;
            case 'get':
                return (!empty($_GET)) ? true:false;
                break;
                
            default :
                return false;
                break;
        }
    }
    
    public static function get($item = null){
       
        if(isset($_POST[$item])){
            return $_POST[$item];
        }else if(isset($_GET[$item])) {
            return $_GET[$item];
        }else {
            return false;
        }
    }
    public static function server($item){
        return (isset($_SERVER[$item])) ? $_SERVER[$item]: false;
    }
}
