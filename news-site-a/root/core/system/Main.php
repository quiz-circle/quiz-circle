<?php
$db = null;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Main{
    private $_load,
            $_db_result;
    
    private $_db;
    
    
    public function __construct() {
        $this->getDbInstance();
    }
    
    
    public function getDB(){
        return $this->_db;
    }
    
    function getThemeName(){
        
        return get_config('site/default');
    }
    
    private function getDbInstance(){
        require_once ABSPATH.'root/db/DB.php';
        $this->_db = DB::run();
    }
    
    public function getUserHash(){
        
    }
    
    public function checkUserHash(){
        $cookie = loadCore('cookie');
        return $cookie->check(get_config('remember/cookie-name')) ? true : false;
    }
    public function getAdminName(){
        global $config;
        return $config['site']['admin'];
    }
}