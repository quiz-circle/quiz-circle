<?php
/**
 * Description of Pagination
 *
 * @author Saif
 */
class Pagination {
    public $config = array(),  $per_page, $variable_name, $elements, $active_page = false;
    public function __construct(){
        $this->variable_name = 'page';
        
        $this->config['start'] = '<ul>';
        $this->config['end'] = '</ul>';
        
        $this->config['previous_button_start'] = '<li>';
        $this->config['previous_button_end'] = '</li>';
        
        $this->config['next_button_start'] = '<li>';
        $this->config['next_button_end'] = '</li>';
        
        $this->config['previous_button_symbol'] = '&laquo;';
        $this->config['next_button_symbol'] = '&raquo;';
        $this->config['active_page_class'] = 'active';
        
        
        $this->config['page_number_start'] = '<li>';
        
        $this->config['page_number_end'] = '</li>';
    }
    
    public function set_config($index, $value = null){
        if(is_array($index)){
            foreach($index as $key => $val){
                $this->config[$key] = $val;
            }
        }else {
            $this->config[$index] = $value;
        }
        return $this;
    }
    
    public function variable_name($name){
        $this->variable_name = $name;
        return $this;
    }
    
    public function variable_value(){
        if(isset($_GET[$this->variable_name])){
            return $_GET[$this->variable_name];
        }
        return false;
    }
    
    public function number_of_elements($number){
        $this->elements = $number;
        return $this;
    }

    public function per_page($number_of_page){
        $this->per_page = $number_of_page;
        return $this;
    }
    
    public function  start(){
        if(isset($_GET[$this->variable_name])){
            return ($this->per_page * $this->variable_value()) - $this->per_page ;
        }else {
            return 0;
        }
    }
            
    function query_string(){
        if(isset($_GET[$this->variable_name])){
            if(count($_GET) > 1){
                $temp = array();
                foreach($_GET as $key => $val){
                    if($key != $this->variable_name){
                        $temp[] = $key.'='.$val.'&';
                     }
                }
                return implode('',$temp);
            }
            return null;
        }
        return $_SERVER['QUERY_STRING'].'&';
    }
    
    public function total_pages(){
        if($this->per_page == 0){
            return 0;
        }else {
            $p =  $this->elements/$this->per_page;
            $ext = (($this->elements % $this->per_page) > 0) ? 1:0;
            return (int) $p + $ext;
        }
    }
    
    function active_page($elem){
        $elem = rtrim($elem,'>');
        $elem = $elem.' class="'.$this->config['active_page_class'].'" >';
        return $elem;
    }
    
    public function show(){
        $total_pages = $this->total_pages();
        $prev = (int) $this->variable_value() - 1;
        $next = (int) $this->variable_value() + 1;

        $result = "";
        
        if( $total_pages > 0 ){
            $result .= $this->config['start'];

            $result .= $this->config['previous_button_start'];
            $result .= '<a href="'
                    .(($this->variable_value() == 1) ? '#': '?'.$this->query_string().$this->variable_name.'='.$prev)
                    . '">';
            $result .= $this->config['previous_button_symbol'];
            $result .= '</a>';
            $result .= $this->config['previous_button_end'];

                for($i = 0; $i<$total_pages; $i++ ){
                    $result .= ((($this->variable_value() == ($i+1) ) || (!$this->variable_value()&& $i==0)) ? 
                            $this->active_page($this->config['page_number_start']): 
                            $this->config['page_number_start']);
                        $result .= '<a href="?'.$this->query_string().$this->variable_name.'='.($i+1).'">'.($i+1).'</a>';
                    $result .= $this->config['page_number_end'];
                }

            $result .= $this->config['next_button_start'];
            $result .= '<a href="'
                    .(($this->variable_value() == $total_pages) ? '#': '?'.$this->query_string().$this->variable_name.'='.$next)
                    . '">';
            $result .= $this->config['next_button_symbol'];
            $result .= '</a>';
            $result .= $this->config['next_button_end'];


            $result .= $this->config['end'];
        }
        return  $result;
    }
    
}
?>
