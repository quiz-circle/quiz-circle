<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Token
 *
 * @author Saif
 */
class Token {
    public static function create(){
            return Session::add(get_config('session', 'token_name'),  md5(uniqid()));
    }

    public static function check($token){
            $tokenName = get_config('session', 'token_name');

            if(Session::check($tokenName) &&  $token === Session::get($tokenName) ){
                    Session::delete($tokenName);
                    return true;
            }
            return false;
    }
}
