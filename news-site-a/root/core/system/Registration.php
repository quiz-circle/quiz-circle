<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Registration
 *
 * @author Saif
 */
class Registration {
    
    private $_session;
    
    public function __construct(){
        $this->_session = loadCore("session");
    }
    
    public function checkHash($hash, $hash_name){
        
        if($this->_session->check($hash_name)){
            return ($this->_session->get($hash_name) === $hash) ? true:false;
        }
        return false;
    }
    
    public function hashDelete($hash_name){
        if($this->_session->check($hash_name)){
            $this->_session->delete($hash_name);
        }
    }
    
    function createHash($hash_name, &$hash){
        $hash = loadCore("hash");
        $hash =  ($this->_session->check($hash_name)) ? 
                $this->_session->get($hash_name): $this->_session->add($hash_name, urldecode($hash->salt(32)));
    }
}
