<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of News
 *
 * @author Saif
 */
class Contents {
    private $_instance, $_news_instance, $_total;
    public $type, $config, $where, $variable_value, $pagination = null, $pagination_config;
    
    private function __construct() {
        $this->_instance = DB::run();
    }
    
    public function fetchList(){
        if(count($this->where)){
            $this->_news_instance = $this->_instance->read('contents')->where($this->where);
        }else {
            $this->_news_instance = $this->_instance->read('contents');
        }
        $this->_total = $this->_news_instance->run_sql()->get_count();
        return $this;
    }
    
    public function config($refs){
        $refers = array();
        $names = array();
        $this->where = array();
        foreach($refs as $ref => $name){
            $refers[]     =   $ref;
            $names[]     =   $name;
        }
        $this->where = $this->manage_config($refers, $names);
        return $this;
    }
  
    private  function manage_config($refers, $names){
        $array = array();
        for($i= 0; $i < count($names); $i++){
            if(Input::exists('get')){
                if(is_array($names[$i])){
                    if(Input::get($names[$i][0])){
                        $array[$refers[$i]] = array('%'.Input::get($names[$i][0]).'%', $names[$i][1]);
                    }
                }else {
                    if(Input::get($names[$i])){
                        $array[$refers[$i]] = Input::get($names[$i]);   
                    }
                }
            }
        }
        return $array;
    }
    
    public static function init($variable_name = false){
        $news = new Contents($variable_name);
        return $news;
    }
    public function sql(){
        return $this->_news_instance->get_sql();
    }
    
    public function get_total(){
        return $this->_total;
    }

    public function pagination($status = true, $per_page = '5', $config = array()){
        
        if($status){
            $pagination = new Pagination();
            foreach($config as $index => $value){
                $pagination->set_config($index, $value);
            }
            $pagination->number_of_elements($this->_total);
            
            $pagination->per_page($per_page);
            
            $start = $pagination->start();
            
            $this->_news_instance->limit($per_page, $start);
            
            $this->pagination = $pagination->show();
        }
        return $this;
    }
    
    public function get_pagination(){
        return ($this->pagination) ? $this->pagination:null;
    }
    
    public function get(){
        return $this->_news_instance->run_sql()->get_array();
    }
    
}
