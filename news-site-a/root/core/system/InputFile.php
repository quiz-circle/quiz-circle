<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FileUplaod
 *
 * @author Saif
 */
class InputFile {
    //put your code here
    private $_files, $_input_name, $_empty;
    public $isMultiple = false;
    public function __construct($input_name) {
        $this->_input_name = $input_name;
        $this->_files = (isset($_FILES[$input_name])) ? $_FILES:null;
        $this->_empty = (empty($_FILES[$input_name]));
        
        if($this->isExists()){
            if($this->_files){
                if(is_array($this->_files[$this->_input_name]['name'])){
                    $this->isMultiple = true;
                }
            }
        }
    }
    
    public function isExists(){
        return ($this->_files) ? true:false;
    }
    
    public function isEmpty(){
        return ($this->_empty) ? true:false;
    }
    
    protected function split_array($ref = null){
        $array = array();
        $counter = 0;
        if($this->_files){
            foreach($this->_files[$this->_input_name]['name'] as  $name){
                if($ref){
                    $array[$name] = $this->_files[$this->_input_name][$ref][$counter];
                }else {
                    $array[] = $this->_files[$this->_input_name]['name'][$counter];
                }
               $counter++;
            }
           return $array; 
        }
        return false;
    }
    public function getExtention(){
        if($this->_files){
            if($this->isMultiple){
                $array = array();
                $counter = 0;
                foreach($this->_files[$this->_input_name]['name'] as  $name){
                    $param = explode('.', $this->_files[$this->_input_name]['name'][$counter]);

                    $array[$name] = (count($param) > 1) ? $param[count($param)-1]:null;
                    $counter++;
                }
                return $array;
            }
            $params = explode('.', $this->_files[$this->_input_name]['name']);
            return (count($params) > 1) ? $params[count($params)-1]:null;
        }
        return false;
    }
    
    public function getName(){
        if($this->isMultiple){
            return $this->split_array();
        }
        return ($this->_files) ? $this->_files[$this->_input_name]['name']:false;
    }
    
    public function  getType(){
        if($this->isMultiple){
             return $this->split_array('type');
        }
        return ($this->_files) ? $this->_files[$this->_input_name]['type']:false;
    }
    public function  getSize(){
        if($this->isMultiple){
             return $this->split_array('size');
        }
        return ($this->_files) ? $this->_files[$this->_input_name]['size']:false;
    }
    public function  getTempName(){
        if($this->isMultiple){
             return $this->split_array('tmp_name');
        }
        return ($this->_files) ? $this->_files[$this->_input_name]['tmp_name']:false;
    }
    
    public function  Error(){
        if($this->isMultiple){
             return $this->split_array('error');
        }
        return ($this->_files) ? $this->_files[$this->_input_name]['error']:false;
    }
    
    public function upload($name, $path, $temp = null){
        if($this->_files){
            $temp = (!$this->isMultiple) ? $this->getTempName(): $temp;
            if(is_dir(ABSPATH.$path)){
                return (move_uploaded_file($temp, ABSPATH.$path.'/'.$name)) ? true:false;
            }
        return false;
        }
        return false;
    }
}
