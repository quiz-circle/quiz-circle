<?php

/**
 * Description of Tree
 *
 * @author Saif
 */
class Tree {
    protected   $_db_instanse,
                $_total,
                $_sql ;
    protected   $_data, $_data_tree = array();
 
    
    public function __construct() {
        $this->_db_instanse = DB::run("new");
        return $this;
    }

    public function total() {
        return $this->_total;
    }
    
    public function setSql($sql){
        $this->_sql = $sql;
        return $this;
    }
    
    public function getData(){
        $sql = $this->_sql;
        $this->_db_instanse->setSql($sql);
        $db_ins = $this->_db_instanse->run_sql();
        $this->_total = $this->_db_instanse->get_count();
        $this->_data = $this->_db_instanse->get_array();
        
        
        return $this;
    }
    
    protected function make_value($value, $val){
        foreach (array_keys($this->_data[0]) as $v){
            $value = str_replace("@@$v", $val[$v], $value);
        }
        return $value;
    }
    
    protected function hasChild($id){
        $array = $this->_data;
        $result = 0; 
        foreach($array as $key => $val){
            if($val['parent'] == $id){
                $result++;
            }
        }
        return ($result == 0)? false:true;
    }
    
    protected function tree($root = 0){
        if($this->_total){
            foreach($this->_data as $key => $val ){
                if($val['parent'] == $root){
                    $this->_data_tree[] = $val;
                    if($this->hasChild($val['class_id'])){
                        $this->tree($val['class_id']);
                    }
                }
            }
        }
    }
    
    public function getTreeArray(){
        $this->tree();
        return $this->_data_tree;
    }

    public static function init(){
        $tree = new Tree();
        return  $tree;
    }
    
}
