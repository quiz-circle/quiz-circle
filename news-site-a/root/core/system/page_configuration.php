<?php

define("PAGE_TEMPLATE_LIST_FILE", "pages.config");
define("DEFAULT_PAGE", "page.php");
define("DEBUG_PAGES",  true);

abstract class page_configuration{
    public $template_list_ref_file, $templates, $page_file_name, $has_template = false;
    private $_content = array(), $content_where;
    
    protected function __construct($uri_positon = 0, $content_id = false) {
        if($content_id !== false){
            $this->content_where =(array('content_id'=>$content_id));
        }else {
            $uri = path_segments();
            $this->content_where =(array('url_ref'=> $uri[$uri_positon]));
        }        
    }

    
    protected function get_content(){
        if($this->fetch_page_content()){
            $this->has_template = true;
        } else {
            $this->has_template = false;  
        }
        return $this->_content;
    }
    
    protected function get_page_filename(){
        return $this->page_file_name;
    }

    public function has_template_exists($template_name){
        if($this->_template_list()){
            return array_key_exists($template_name, $this->templates);
        }
        return false;
    }
    
    public function has_content_exists(){
        return !empty($this->_content);
    }
    
    protected function add($values, $after_add, &$error = false){
        $add = DB::run()->write('content', $values)->run_sql();
        if(!$add->error()){
            if(function_exists($after_add)){
                $after_add($add->last_insert_id());
            }
            $error = false;
        }else {
            $error['msg'] = $add->sql_error();
            $error['sql'] = $add->get_sql();
        }
    }
    
    protected function edit($values, $id, $after_edit, &$error = false){
        $edit = DB::run()->write('content', $values)->where('content_id', $id)->run_sql();
        if(!$edit->error()){
            if(function_exists($after_edit)){
                $content = DB::run()->read('content')->where('content_id', $id)->run_sql();
                $after_edit($content->get_first());
            }
            $error = false;
        }else {
            $error['msg'] = $edit->sql_error();
            $error['sql'] = $edit->get_sql();
        }
    }
    
    public function has_template(){
        return $this->has_template;
    }


    public function fetch_page_content(){
        $this->content_where['content_type'] = 'page';
        $page = DB::run()->read("content")->where($this->content_where)->run_sql();
        if($page->get_count()){
            $this->_content = $page->get_first();
            
            if($this->has_template_exists($this->_content['source_link'])){
                $this->page_file_name = $this->templates[$this->_content['source_link']];
                return true;
            }else {
                $this->page_file_name = DEFAULT_PAGE;
                return false;
            }
            return false;
        }
        return false;
    }
    
    private function _has_exists_template_list_file(){
        $this->template_list_ref_file = ABSPATH."themes/".get_theme_name()."/pages/" . PAGE_TEMPLATE_LIST_FILE;
        return file_exists($this->template_list_ref_file);
    }
    
    protected function _template_list(){
        if($this->_has_exists_template_list_file()){
            $file_list  = array();
            $filecontent = file_get_contents($this->template_list_ref_file);
            preg_match_all('/<[^>]+>/i',$filecontent, $files_list);
            $fl = isset($files_list[0]) ? $files_list[0]:null;
            if(is_array($fl)){
                foreach($fl as $file) {
                   $ex = explode(" ", $file);
                    if(count($ex) == 2){
                        $file_list[ltrim($ex[0], "<")] = rtrim($ex[1], ">");
                    }else {
                        if(DEBUG_PAGES) trigger_error("Page reference file error!", E_USER_NOTICE); 
                    }
                }
            } else {
                return false;
                if(DEBUG_PAGES) trigger_error("Page reference file error!", E_USER_NOTICE);
            }
            $this->templates = $file_list;
            return true;
        }else {
            return false;
        }
    }    
}