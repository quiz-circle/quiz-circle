<?php

require_once ABSPATH .'root/core/system/MetaInfo.php';

/**
 * Description of UserInfo
 *
 * @author Saif
 */
class UserInfo extends MetaInfo{
    private $_meta_attrs,
            $_user_id;
    
    public function __construct($user_id) {
        $this->_user_id = $user_id;
        $this->_meta_attrs = get_config("meta_attr/usr");
        $table = "user_info";
        $key_column = "info_key";
        $value_column = "info_value";
        $rel_column = "user_id";
        parent::__construct($table, $key_column, $value_column, $rel_column);
    }
    
    
    public function add($key, $value) {
        if($this->key_is_valid($key)){
            return parent::add($key, $value, $this->_user_id);
        }else {
            trigger_error("The key <strong><em>".$key."</em></strong> for user information is not valid");
        }
    }
    
    public function edit($key, $value) {
        if($this->key_is_valid($key)){
            return parent::edit( $key, $value, $this->_user_id);
        }else {
            trigger_error("The key <strong><em>".$key."</em></strong> for user information is not valid");
        }
    }
    
    public function remove($key) {
        if($this->key_is_valid($key)){
            return parent::remove($key, $this->_user_id);
        }else {
            trigger_error("The key <strong><em>".$key."</em></strong> for user information is not valid");
        }
    }
    
    public function get($key ) {
        if($this->key_is_valid($key )){
            return parent::get($key, $this->_user_id);
        }else {
            trigger_error("The key <strong><em>".$key."</em></strong> for user information is not valid");
        }
    }
    
    public function exists($key ) {
        if($this->key_is_valid($key )){
            return parent::is_exists($key , $this->_user_id);
        }else {
            trigger_error("The key <strong><em>".$key."</em></strong> for user information is not valid");
        }
    }
    
    public function is_empty($key ) {
        if($this->key_is_valid($key )){
            return parent::is_null($this->_user_id, $key );
        }else {
            trigger_error("The key <strong><em>".$key."</em></strong> for user information is not valid");
        }
    }
    
    private function key_is_valid($val){
        return (in_array($val, $this->_meta_attrs)) ? true:false; 
    }
    
}
