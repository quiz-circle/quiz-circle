<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of controllers
 *
 * @author Saif
 */


class Controller {
    //put your code here
    public function __construct() {
        
    }
    
    protected function model($model = ""){
        $current_theme =  $this->get_config('site/default');
        require_once ABSPATH . "apps/models/".$model.".php";
        return new $model();
    }
    
    protected function view($page_name = "", $data = ""){
        $current_theme =  $this->get_config('site/default');
        require_once ABSPATH . "themes/".$current_theme."/".$page_name.".php";
    }

    
    protected function  get_config($path){
        if ($path) {
            $config = $GLOBALS['config'];

            $path = explode('/', $path);

            foreach ($path as $p) {
                if (isset($config[$p])) {
                    $config = $config[$p];
                }
            }
            return $config;
        }
        return false;
    }
    

}
