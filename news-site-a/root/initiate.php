<?php
global $db;
global $main;
$login_page_name =  "login";

$user = load()->sys("user")->run();


function _get_cap(&$user_type){
    $caps = array();
    global $user;
    load()->sys('Capabilities')->open();
    
    $cap = new Capabilities($user->getID());
    
    $cap_conf = get_config('cap');
    
    foreach ($cap_conf as $key => $v){
        $caps[$key] = $cap->has($key);
    }
    //Explain($caps);
    
    //echo "<script>alert('".$user->getType()."');</script>";
    //echo $cap->has("add_news") ? "<script>alert('ok...');</script>":"<script>alert('No...');</script>";
    
    return $caps;
}



function segment_val(){
    $seg_var = array();
    $ps = path_segments();
    for($i=1; $i <= count($ps)-1; $i++){
        $seg_var[] = $ps[$i];
    }
    return $seg_var;
}

$_SEG_VARS = segment_val();

function full_url(){
    return urlencode("http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]);
}



if( page_name() == get_config("site/admin")){
    if(!$user->isLoggedIn()){   
        if($main->checkUserHash()){
            
            $userCookie = $user->getCookie();
            if($userCookie){
                $user = new User($userCookie[0]);
                $user->login();
                $re_link = "http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
                redirect($re_link);
            }else {
                $user->logout();
            }
        }
    }else {

    }



    
    $s = path_segments();
    
    if(!$user->isLoggedIn()){
        if(!isset($s[1])){
            $s[1] = 'main-page';
        }
        $login_page_name =  "login";
        if($s[1] != $login_page_name){
            $login_link = admin_link()."/$login_page_name?back_to=" . full_url();
            redirect($login_link);
        }
    }
    
}

if(!isset($s[1])){
    $s[1] = 'main-page';
}

$_get_cap = null;

if($s[1] != $login_page_name){
    $_get_cap = _get_cap($user_type);
    $_get_cap = (object) $_get_cap; 
}


ob_start();
require ABSPATH . 'root/core/page-load.php';
load()->sys("session")->run()->add("_full_url", full_url());
ob_end_flush();