<?php

/* 
 * This page will initiated by default
 * 
 * 
 */
session_start();
ob_start();
date_default_timezone_set("Asia/Dhaka");

function root(){
    $path = array();
    $path = explode("\\", dirname(__FILE__));
    
    $probleme =  (count($path) == 1) ? true:false;
    if($probleme){
        $path = explode("/", dirname(__FILE__));
    }    
    array_pop($path);
    return implode("/" , $path)."/";
}
define('ABSPATH', root());

require_once ABSPATH . 'apps/configurations.php';
require_once ABSPATH . 'root/functions.php';
require_once ABSPATH . 'root/core/system/Main.php';

change_db_config();
$main = new Main();
$db = $main->getDB();




require_once ABSPATH . 'apps/app-functions.php';
