<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function bind_where($array, $value = null, $type = null){
    
    
    if(is_array($array)){
        $where = ' WHERE ';
        $c = 0;
        foreach($array as $key => $val){
            
            if(is_array($val)){
                $value = $val[0];
                $type = $val[1];
            }else {
                $value = $val;
                $type = '=';
            }
            
            
            $where .= ($c != 0) ? ' AND ':' ';
            $where .= '`'.$key . '` '.$type.' ' . ((is_numeric($val)) ? (int) $value:"'".escape($value)."'" ).' ';
            $c++;
        }
        return $where;
    }else {
        if($value == null && !is_array($array) && is_string($array)){
            return ' '.$array.' ';
        }
        return ' WHERE `'. $array . '` '.$type.' ' .  ((is_numeric($value)) ? (int) $value:"'".escape($value)."'" ).' ';
    }
}
function bind_update_values($array, $value){
    if(is_array($array)){
        $update = ' SET ';
        $c = 0;
        
        foreach($array as $key => $val){
            $update .= ($c != 0) ? ' , ':' ';
            $update .= '`'.$key . '` = ' . ((is_numeric($val)) ? (int) $val:"'".escape($val)."'" ).' ';
            $c++;
        }
        
        return $update;
    }else {
        return 'SET `'. $array . '` = ' .  ((is_numeric($value)) ? (int) $value:"'".escape($value)."'" ).' ';
    }
}

function  bind_insert_values($columns){
    if(is_array($columns)){
        $values = "  ";
        $kyes = array_keys($columns);
        
        $values .= '(`'.implode('`, `', $kyes).'`)';
        
        $values .= " VALUES (";
        
        $c = 0;
        foreach($columns as $value){
            if($c != 0 ) $values .= ', ';
            $values .= (is_numeric($value)) ? (int) $value :  "'" . escape($value) ."'";
            $c++;
        }
        
        $values .= ')';
        
        return $values;
        
    }
    return false;
}


function escape($string){
    return mysql_real_escape_string($string);
}

