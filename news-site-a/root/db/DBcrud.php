<?php
/**
 * Description of DBcrud 
 * This is core class for Creating, Reading, Updating or  Deleting any query from database. 
 * ----------------------------------------------------------------------------------
 * Run method
 * Core satatic method is run. This calls the main DBcrud class.
 * by calling db class mysqql connection and database connection is being esteblished 
 * **********************************************************************************
 * create read update and delete these are four main methot for doing operation in database
 * @author Saif
 */

class DBcrud {
    //put your code here
     private static  $_instance = null,
                    $_link = null,
                    $_query;
    private $_count = 0,
            $_error = false,
            $_result,
            $_insert_values = null,
            $_update_values = null;
    
    public  $cols, $where_type = '=',  $where, $limit, $sort, $message = null;

    private function  __construct(){
        $mysql = mysql_connect(HOST,USER,PASSWORD);     
        self::$_link = $mysql;
        if(!$mysql) die('Server connection error');   
        $databse = mysql_select_db(DB);	
	if(!$databse) die('Database error!');
    }   

    public static function run(){
        if(!isset(self::$_instance)){
            self::$_instance = new DBcrud();
        }
        return self::$_instance;
    }
    public function run_sql($sql){
        mysql_query("SET NAMES '".CHARSET."'",$this->get_link());
        self::$_query = mysql_query($sql, $this->get_link());
        
        if($this->confirm_query()){
            return  $this;
        }else {
            $this->_error = true;
        }
    }
    

    
    private function confirm_query(){
        return self::$_query ? self::$_query:false;
    }
    
    public function bind_insert_values($values){
        if(is_array($values) && count($values)){
            $col ='';
            $val ='';
            $counter = 1;
            foreach ($values as $key => $value) {
                $col .= ' `'.$key.'` ';
                $col .= ($counter == count($values))? null:',';
                $val .= (is_numeric($value))? $value:" '".$value."' ";
                $val .= ($counter == count($values))? null:',';
                $counter++;
            }
            $this->_insert_values = ' ('.$col .') VALUES ( '. $val.') ';

        }else {
            $this->_insert_values = null;
        }
    }
    
    private function bind_update_value($values){
        if(is_array($values) && count($values)){
            $binded ='SET ';
            $counter = 1;
            foreach ($values as $key => $value) {
                $binded .= ' `'.$key.'` = ';
                $binded .= (is_numeric($value))? " " . $value . " ":" '".$value."' ";
                $binded .= ($counter == count($values))? null:',';
                $counter++;
            }
            $this->_update_values = $binded;

        }else {
            $this->_update_values = null;
        }
    }
    
    public function has_error(){
        return $this->_error;
    }
    
    public function count(){
        return $this->_count;
    }
    
    public function create($table = null, $values){
        $this->bind_insert_values($values);
        $sql = $this->make_sql('create', $table);
        $this->run_sql($sql);
        if(!$this->has_error()){
             
        }else {
            echo 'Internal Error';
        }
        //echo $sql;
    }
    
    private function split_column($columns){
        if($columns){
            $column = explode(',', $columns);
            if(count($column) > 1){
                $this->cols = " `" . implode("`, `", $column) . "` "; 
            }else{
                if(trim($columns) === '*'){
                    $this->cols = '*';
                }else{
                    $this->cols = "`".$columns."`";
                }
            }
        }else {
            $this->cols = null;
        }
    }
    
    public function read($table = null,$where = null, $limit = null, $column = ' * ', $sort_method = null){
        if(isset($table)){
            $this->make_where($where);
            $this->set_limit($limit);
            $this->sorting($sort_method);
            $this->split_column($column);
            
            $sql = $this->make_sql('read', $table);
            $result = array();
            $this->run_sql($sql);
            
            $this->_count = (!$this->has_error()) ? mysql_num_rows(self::$_query):0;
                    
            if(!$this->has_error()){
                if($this->count()){
                    while($qr = mysql_fetch_assoc(self::$_query)){
                        $result[] = $qr;
                    }
                    $this->_result = $result;
                }else{
                    $this->message = 'Query Empty';
                    $this->_result = null;
                }
            }else {
                $this->message = "Internal Error!";
                $this->_result = null;
            }
            
        }else {
            $this->_result = null;
        }
    }
    
    public function first(){
        $result = $this->result();
        return $result[0];
    }
    
    public  function message(){
        return ($this->message) ? $this->message:null;
    }
    
    public function update($table = null,$values = null, $where = null, $limit = null){
        $this->bind_update_value($values);
        $this->make_where($where);
        //$this->set_limit($limit);
        
        $sql = $this->make_sql('update', $table);

        $this->run_sql($sql);
        
        if(!$this->has_error()){
            if(mysql_affected_rows($this->get_link()) > 0){
                $this->message = 'Updated';
            }else {
                $this->message = 'Already Updated';
            } 
        }else {
            $this->message = 'Internal Error';
        }
        return $this;
    }
    
    public function delete($table = null,$where = null, $limit = null){
        $this->make_where($where);
        $this->set_limit($limit);
        
        $sql = $this->make_sql('delete', $table);
        $this->run_sql($sql);
        if(!$this->has_error()){
            if(mysql_affected_rows($this->get_link()) > 0){
                $this->message = 'Deleted';
            }else {
                $this->message =  'Already deleted';
            } 
        }else 
            $this->message = 'Internal Error';
    }
    
    public function result(){
        
        return $this->_result;
    }
    
    public function sorting($sort_method){
        if(isset($sort_method)){    
            $m = explode(',', $sort_method);
            if(count($m) == 2){
                $this->sort = " ORDER BY `{$m[0]}` {$m[1]} ";
            }else if(count($m) == 1){
                $this->sort = " ORDER BY {$sort_method}";
            }else {
                $this->sort = null;
            }
        }else {
            $this->sort = null;
        }
    }
    
    
    public function make_sql($type = null, $table, $column = false){
        if($type){
            $where = $this->where;
            $limit = $this->limit;
            $sorting = $this->sort;
            $col = $this->cols;
            switch($type){
                case 'create':
                    $operation = 'INSERT ';
                    $col = null;
                    $prep = " INTO ";
                    break;
                case 'read':
                    $operation = 'SELECT ';
                    $col = $this->cols;
                    $prep = " FROM ";
                    break;
                case 'update':
                    $operation = 'UPDATE ';
                    $col = null;
                    $prep = " ";
                    break;
                case 'delete':
                    $operation = 'DELETE ';
                    $col = null;
                    $prep = " FROM ";
                    break;
                default :
                    return false;
            }
            
            return "{$operation} {$col} {$prep} `{$table}` {$this->_update_values} {$where} {$sorting} {$limit} {$this->_insert_values}";
        }
        return false;
    }
    public function where_type($type){
        $this->where_type = (isset($type)) ? $type:' = ';
    }
    
    public function make_where($where){
        if(is_array($where) && count($where)){
            $wh = " WHERE ";
            $counter =1;
            foreach($where as $col => $val){
               $wh .= " `{$col}` ";
               $wh .= (isset($this->where_type)) ? $this->where_type:" = ";
               
               $wh .= is_numeric($val) ? " ".$val." ":" '".$val."' ";
               $wh .= ($counter == count($where)) ? " ":" AND ";
               $counter++;
            }
        $this->where = $wh; 
        }else {
            $this->where = null;
        }
    }
    public function set_limit($limit){
        if(!is_int($limit)){ 
            $m = explode(',', $limit);
            if(count($m) == 2){
                $start =  $m[0];
                $limit =  $m[1];
                if(is_numeric($start) && is_numeric($limit)){
                    $limit = " LIMIT  ".(int) $start.",  ".(int) $limit;
                }else {
                    $limit = null;
                }
            }else if(count($m) == 1){
                if(is_numeric($limit)){
                    $limit = " LIMIT ".(int)$limit;
                }else {
                    $limit = null;
                }
            }else {
                $limit = null;
            }
            $this->limit = $limit;
        }else {
            $this->limit = null;
        }
    }
    public function get_link(){
        if(isset(self::$_link)){
         return self::$_link;
        }
        return false;
    }
    public static function stop(){
        self::$_instance = null;
        mysql_close(self::$_link);
    }
}
