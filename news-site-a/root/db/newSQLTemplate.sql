CREATE TABLE IF NOT EXISTS `author`( 
    `auth_id` int(11) not null auto_increment primary_key, 
    `name` varchar(300) not null, 
    `url_ref` varchar(150) not null UNIQUE, 
    `nick_name` varchar(200) not null
)

CREATE TABLE IF NOT EXISTS `review`( 
    `rev_id` int(11) not null auto_increment PRIMARY KEY, 
    `book_id` int(11) not null, 
    `name` varchar(150) not null, 
    `date` datetime not null, 
    `rev_content` longtext not null, 
    `aproved` tinyint(1), 
    `user_id` int(11), 
    `rev_ip` varchar(20),  
    FOREIGN KEY (book_id) REFERENCES book_list(book_id)
)

CREATE TABLE IF NOT EXISTS `book_info`( 
    `info_id` longint(20) not null auto_increment PRIMARY KEY, 
    `book_id` int(11) not null, 
    `info_key` varchar(30) not null, 
    `info_value` longtext not null,  
    FOREIGN KEY (book_id) REFERENCES book_list(book_id)
)