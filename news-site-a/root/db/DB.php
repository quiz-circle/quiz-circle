<?php
/*
 * Description of DBcrud 
 * This is core class for Creating, Reading, Updating or  Deleting any query from database. 
 * ----------------------------------------------------------------------------------
 * Run method
 * Core satatic method is run. This calls the main DBcrud class.
 * by calling db class mysqql connection and database connection is being esteblished 
 * **********************************************************************************
 * create read update and delete these are four main methot for doing operation in database
 * @author Saif
 */

define("DD_DEBUG", false);

require_once  get_config('db-config/path');

class DB {
    private $_sql, $_query, $_result, $_count;
    public static $_insatance, $_link;
    private $_type;
    public static $_counter = 0;
    public $db_run_time;
    
    private function __construct(){
       self::$_link = mysqli_connect(HOST, USER, PASSWORD, DB);
        //$this->db_run_time = $time;
    }
    
    public static function run($new = ""){
        if(DD_DEBUG) echo   ++ self::$_counter." time(s) Loades<br><br>";
        if(!isset(self::$_link)){
            self::$_insatance = new DB();
        }
        return self::$_insatance;
    }
    
    public function get_time(){
        return $this->db_run_time;
    }
    
    public function setSql($sql){
        $chk_sql = strtolower($sql);
        $types = array("select","insert","update","delete");
        $type_arr = explode(" ", $chk_sql);
        if(in_array( $type_arr[0], $types )){
            $this->_type = strtoupper($type_arr[0]);
        }
        $this->_sql = $sql;
        return $this;
    }
    
    public function read($table, $column = ' * '){
        $this->_type = 'SELECT';
        if(trim($column) != '*'){
            $colu = explode(',', trim($column));
            $column = array();
            foreach($colu as $col){
                $column[] = trim($col);
            }
            $column = (count($column) > 1) ? ''.implode(', ', $column).' ': ' '.$column[0].' ';
        }
        $this->_sql = 'SELECT ' .$column .' FROM `'.$table.'`';
        return $this;
    }
    
    public function write($table){
        $this->_type = 'INSERT';
        $this->_sql = 'INSERT INTO `'.$table.'`';
        return $this;
    }
    
    public function last_insert_id($link = false){
        $link = ($link === false) ? self::$_link:null;
        return mysqli_insert_id($link);
    }
    
    public function values($columns, $values = false){
        if($this->_type == 'INSERT'){
            $this->_sql .= $this->bind_insert_values($columns);
        }else if($this->_type == 'UPDATE'){
            $this->_sql .= $this->bind_update_values($columns, $values);
        }
        return $this;
    }

    public function edit($table){
        $this->_type = 'UPDATE';
        $this->_sql = 'UPDATE `'.$table .'` ';
        return $this;
    }
    
    public  function increment($column){
        if($this->_type == 'UPDATE'){
            $this->_sql .= 'SET '.$column.' = '.$column.'+1 ';
        }
        return $this;
    }
    
    public  function decrement($column){
        if($this->_type == 'UPDATE'){
            $this->_sql .= 'SET '.$column.' = '.$column.'-1 ';
        }
        return $this;
    }
    
    public function delete($table){
        $this->_type = 'DELETE';
        $this->_sql = 'DELETE FROM `'.$table.'`';
        return $this;
    }
    
    public function where($column, $value = null, $type = '=' ){

        $this->_sql .= ($this->_type != "INSERT") ? $this->bind_where($column, $value, $type):'';
        return $this;
    }
    
    public function order_by($column, $sortig_method = ' ASC ', $escape = true){
        $sort = " ORDER BY ";
        $c = 0;
        if(is_array($column)){
            foreach ($column as $col => $method){
               $sort .= ($c > 0) ? ", ":"";
               
               if(is_numeric($col)){    
                   $sort .= " " . ($escape ? $this->escape($method):$method) . " ";
               }else {
                   $sort .= " " . ($escape ? $this->escape($col):$col) . " $method ";
               }
               $c++;
            }
        }else {
            $sort .= " `". ($escape ? $this->escape($column):$column). "` $sortig_method ";
        }
        $this->_sql .= $sort;        
        return $this;
    }
    
    public function extra( $extra_sql){
        $this->_sql .= " ".$extra_sql." ";
        return $this;
    }
    
    public function limit($num, $start = false){
        if($this->_type != "INSERT"){
            $this->_sql .= ' LIMIT '. (($start !== false) ? $start . ', ' :'') . $num;
        }
        return $this;
    }
    
    public function get_sql(){
        return $this->_sql;
    }
    
    public function getLink(){
        return self::$_link;
    }
    
    public function  run_sql(){
        if(DD_DEBUG){
            echo "<br>";
            echo $this->_sql."<br><br>";;
        }
        mysqli_query(self::$_link, "SET NAMES '".CHARSET."'");
        $this->_query = mysqli_query(self::$_link, $this->get_sql());
        return $this; 
    }
    
    public function mysql_result($row = 0, $field=0){
        return mysqli_result($this->_query, $row, $field);
    }
    
    public function error(){
        return ($this->_query) ? false:true;
    }

    public function get_count(){
        return ($this->_type == "SELECT") ? (($this->error()) ? false: mysqli_num_rows($this->_query)):false;
    }
    

    public function result(){
        
        if($this->error()){
            return 'error';
        }else {
            return $this->_result;
        }
    }
    
    public function get_first(){
        if($this->_type == "SELECT"){
            $results = array();
            if(!$this->error()){ 
                $results = mysqli_fetch_assoc($this->_query);
            }else {
                $results = 'Intarnal error!';
            }
            return $results;
        }
    }
    
    public function get_array(){
        if($this->_type == "SELECT"){
        $results = array();
        if(!$this->error()){ 
             while($result = mysqli_fetch_assoc($this->_query)){
                $results[] = $result;
            }
        }else {
            $results = 'Intarnal error!';
        }
        return $results;
        }
    }

    public function get_first_obj(){
        return (object) $this->get_first();
    }
    public function get_object(){
        return (object) $this->get_array();
    }
    
    public  function has_changed(){
        return (mysqli_affected_rows(self::$_link) > 0) ? true:false;
    }

    public function sql_error(){
        return mysqli_error($this->getLink());
    }
    
    public function message(){
        if(!$this->error()){
            $type = ($this->_type == "UPDATE")? "Updated":"Deleted";
            if($this->_type == "UPDATE" || $this->_type == "DELETE"){
                return ($this->has_changed()) ? "Succeccfully {$type}" : "Already {$type}";
            }else if($this->_type == "INSERT"){
                return "Query Inserted";
            }    
        }else {
            return "Intarnal error!";
        }
    }
    
    
    
    private function bind_where($array, $value = null, $type = null){

        if(is_array($array)){
            $where = ' WHERE ';
            $c = 0;
            foreach($array as $key => $val){

                if(is_array($val)){
                    $value = $val[0];
                    $type = $val[1];
                }else {
                    $value = $val;
                    $type = '=';
                }
                $where .= ($c != 0) ? ' AND ':' ';
                $where .= '`'.$key . '` '.$type.' ' . ((is_numeric($val)) ? (int) $value:"'".$this->escape($value)."'" ).' ';
                $c++;
            }
            return $where;
        }else {
            if($value === null && is_string($array)){
                return ' '.$array.' ';
            }
            return ' WHERE `'. $array . '` '.$type.' ' .  ((is_numeric($value)) ? (int) $value:"'".$this->escape($value)."'" ).' ';
        }
    }

    private function bind_update_values($array, $value){
        if(is_array($array)){
            $update = ' SET ';
            $c = 0;

            foreach($array as $key => $val){
                $update .= ($c != 0) ? ' , ':' ';
                if(is_array($val)){
                    $v = isset($val[0]) ? $val[0]:"";
                    $quote = isset($val[1]) ? $val[1]:"";
                    echo $v;
                    $update .= '`'.$key . '` = ' . ( !$quote ? (int) $v:"'" . $this->escape($v)."'" ).' ';
                    
                }else {
                    $update .= '`'.$key . '` = ' . ((is_numeric($val)) ? (int) $val:"'" . $this->escape($val)."'" ).' ';
                }
                $c++;
            }

            return $update;
        }else {
            return 'SET `'. $array . '` = ' .  ((is_numeric($value)) ? (int) $value:"'".$this->escape($value)."'" ).' ';
        }
    }

    private function  bind_insert_values($columns){
        if(is_array($columns)){
            $values = "  ";
            $kyes = array_keys($columns);

            $values .= '(`'.implode('`, `', $kyes).'`)';

            $values .= " VALUES (";

            $c = 0;
            foreach($columns as $value){
                if($c != 0 ) $values .= ', ';
                if(is_array($value)){
                    
                    $val = isset($value[0]) ? $value[0]:"";
                    $quote = isset($value[1]) ? $value[1]:false;
                    
                    $values .= (!$quote) ? (int) $val :  "'" . $this->escape($val) ."'";
                }else {
                    $values .= (is_numeric($value)) ? (int) $value :  "'" . $this->escape($value) ."'";
                }
                $c++;
            }

            $values .= ')';

            return $values;

        }
        return false;
    }
    
    


    public function escape($string){
        return mysqli_real_escape_string(self::$_link, $string);
    }
}