<?php
/**
 * Description of DBcrud 
 * This is core class for Creating, Reading, Updating or  Deleting any query from database. 
 * ----------------------------------------------------------------------------------
 * Run method
 * Core satatic method is run. This calls the main DBcrud class.
 * by calling db class mysqql connection and database connection is being esteblished 
 * **********************************************************************************
 * create read update and delete these are four main methot for doing operation in database
 * @author Saif
 */

class DB {
    private  $_sql, $_query, $_result, $_link, $_count, $_db_instance;
    public $type = 'SELECT';
    
    private function __construct(){
        load_page(array(
            "root/db/db_funcions.php", 
            "root/db/Connection.php"
            )
        );
        
        
        $con = Connection::run();
        $this->_link = $con->getLink();
        
        $this->_db_instance = $con->getLink();
    }
    
    public static function run(){
        return new DB;
    }
    
    public function setSql($sql){
        $this->_sql = $sql;
        return $this;
    }
    
    public function read($table, $column = ' * '){
        if(trim($column) != '*'){
            $colu = explode(',', trim($column));
            $column = array();
            foreach($colu as $col){
                $column[] = trim($col);
            }
            $column = (count($column) > 1) ? '`'.implode('`,`', $column).'`': '`'.$column[0].'`';
        }
        $this->_sql = 'SELECT ' .$column .' FROM `'.$table.'`';
        return $this;
    }
    public function write($table){
        $this->type = 'INSERT';
        $this->_sql = 'INSERT INTO `'.$table.'`';
        return $this;
    }
    
    public function last_insert_id($link = false){
        $link = ($link === false) ? $this->_link:null;
        return mysql_insert_id($link);
    }
    
    public function values($columns, $values = false){
        if($this->type == 'INSERT'){
            $this->_sql .= bind_insert_values($columns);
        }else if($this->type == 'UPDATE'){
            $this->_sql .= bind_update_values($columns, $values);
        }
        return $this;
    }

    public function edit($table){
        $this->type = 'UPDATE';
        $this->_sql = 'UPDATE `'.$table .'` ';
        return $this; 
    }
    
    public  function increment($column){
        if($this->type == 'UPDATE'){
            $this->_sql .= 'SET '.$column.' = '.$column.'+1 ';
        }
        return $this;
    }
    
    public  function decrement($column){
        if($this->type == 'UPDATE'){
            $this->_sql .= 'SET '.$column.' = '.$column.'-1 ';
        }
        return $this;
    }
    
    public function delete($table){
        $this->type = 'DELETE';
        $this->_sql = 'DELETE FROM `'.$table.'`';
        return $this;
    }
    
    public function where($column, $value = null, $type = '=' ){
        $this->_sql .= ($this->type != "INSERT") ? bind_where($column, $value, $type):'';
        return $this;
    }
    
    public function order_by($column, $sortig_method = ' ASC '){
        $this->_sql .= ($this->type == "SELECT") ? ' ORDER BY `'. escape($column) . '` ' . $sortig_method:'';
        return $this;
    }
    
    public function extra( $extra_sql){
        $this->_sql .= " ".$extra_sql." ";
        return $this;
    }
    
    public function limit($num, $start = false){
        if($this->type != "INSERT"){
            $this->_sql .= ' LIMIT '. (($start !== false) ? $start . ', ' :'') . $num;
        }
        return $this;
    }
    
    public function get_sql(){
        return $this->_sql;
    }
    
    public function getLink(){
        return $this->_link;
    }
    
    public function  run_sql(){
        mysql_query("SET NAMES '".CHARSET."'", $this->_link);
        $this->_query = mysql_query($this->get_sql());
        return $this; 
    }
    public function mysql_result($row = 0, $field=0){
        return mysql_result($this->_query, $row, $field);
    }
    
    public function error(){
        return ($this->_query) ? false:true;
    }

    public function get_count(){
        return ($this->type == "SELECT") ? (($this->error()) ? false: mysql_num_rows($this->_query)):false;
    }
    

    public function result(){
        
        if($this->error()){
            return 'error';
        }else {
            return $this->_result;
        }
    }
    
    public function get_first(){
        if($this->type == "SELECT"){
            $results = array();
            if(!$this->error()){ 
                $results = mysql_fetch_assoc($this->_query);
            }else {
                $results = 'Intarnal error!';
            }
            return $results;
        }
    }
    public function get_array(){
        if($this->type == "SELECT"){
        $results = array();
        if(!$this->error()){ 
             while($result = mysql_fetch_assoc($this->_query)){
                $results[] = $result;
            }
        }else {
            $results = 'Intarnal error!';
        }
        return $results;
        }
    }
    
    public  function has_changed(){
        return (mysql_affected_rows($this->_link) > 0) ? true:false;
    }

    public function sql_error(){
        return mysql_error($this->getLink());
    }
    
    public function message(){
        if(!$this->error()){
            $type = ($this->type == "UPDATE")? "Updated":"Deleted";
            if($this->type == "UPDATE" || $this->type == "DELETE"){
                return ($this->has_changed()) ? "Succeccfully {$type}" : "Already {$type}";
            }else if($this->type == "INSERT"){
                return "Query Inserted";
            }    
        }else {
            return "Intarnal error!";
        }
    }
    
}