<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Connection
 *
 * @author Saif
 */
require_once  get_config('db-config/path');

class Connection {
    private $_db = DB, 
            $_host = HOST, 
            $_user = USER, 
            $_pass = PASSWORD,
            $_link = null,
            $_db_instance = null,
            $_db_time = null;
    
    private function __construct(){
        $this->_link = mysqli_connect($this->_host, $this->_user, $this->_pass, $this->_db);  
        if(!$this->_link ) {
            die("<h2>Couldnt Connect to Database!</h2>");
        }
        
        $this->_db_time = microtime(true);
    }
    
    public function get_time(){
        return $this->_db_time;
    }
    
    private function establish_db(){
        $this->_db_instance = mysql_select_db($this->_db, $this->_link) or  die("Couldnt Connect to DB!");
        return $this;
    }
    
    private function getLink(){
        return $this->_link;
    }
    
    public function getDbInstance(){
        return $this->_db_instance;
    }
    
    public static function run(&$time){
        $con = new Connection();
        //$con->establish_db();
        $time = $con->get_time();
        return $con->getLink();
    }
    
}
