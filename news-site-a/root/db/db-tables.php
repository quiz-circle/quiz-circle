<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$tables = array(
    'class' => array(
        'class_id' => 'int(11) not null auto_increment primary key',
        'name' => 'varchar(150) not null',
        'url_ref' => 'varchar(150) not null UNIQUE'
    ),
    'class_type' => array(
        'type_id'   =>  'int(11) not null auto_increment PRIMARY KEY',
        'class_id'    =>  'int(11) not null',
        'type'   =>  'varchar(25) not null',
        'description'   =>  'text not null',
        'parent'   =>  'int(11) DEFAULT \'0\' not null',
        'count'   =>  'int(11) DEFAULT \'0\' not null',
        'FOREIGN KEY (class_id) REFERENCES class(class_id)'
    ),
    'book_list' => array(
        'book_id'   =>  'int(11) not null auto_increment primary key',
        'book-title'   =>  'varchar(300) not null',
        'url_ref'   =>  'varchar(150) not null UNIQUE',
        'description'   =>  'longtext not null',
        'publisher'  =>  'varchar(300)  not null',
        'ISBN'  =>  'varchar(100)  not null'
    ),
    'class_relationship'=> array(
        'rel_id'   =>  'int(11) not null auto_increment primary key',
        'class_id'  => 'int(11) not null',
        'entity_id'  => 'int(11) not null',
        'FOREIGN KEY (entity_id) REFERENCES book_list(book_id), FOREIGN KEY (class_id) REFERENCES class(class_id)' 
        
    ),
    'author' => array(
        'auth_id'   =>  'int(11) not null auto_increment PRIMARY KEY',
        'name'      =>  'varchar(300) not null',
        'url_ref'   =>  'varchar(150) not null UNIQUE',
        'nick_name' =>  'varchar(200) not null'
    ),
    'book_auth_rel' => array(
        'rel_id'    => 'int(11) not null auto_increment PRIMARY KEY',
        'book_id'    => 'int(11) not null',
        'auth_id'    => 'int(11) not null',
        'FOREIGN KEY (book_id) REFERENCES book_list(book_id), FOREIGN KEY (auth_id) REFERENCES author(auth_id)' 
    ),
    'user' => array(
        'user_id' => 'int(11) not null auto_increment',
        'username' => 'varchar(40) not null UNIQUE',
        'email' => 'varchar(250) not null UNIQUE',
        'password' => 'varchar(64) not null',
        'created_date' => 'datetime not null',
        'modified_date' => 'datetime not null',
        'activision_key' => 'varchar(32) not null',
        'display_name' => 'varchar(60) not null',
        'PRIMARY KEY(user_id)'
    ),
    'edition_date' => array(
        'date_id'   =>  'int(11) not null auto_increment PRIMARY KEY',
        'book_id'    =>  'int(11) not null',
        'date'   =>  'date',
        'order'   =>  'int(4)',
        'FOREIGN KEY (book_id) REFERENCES book_list(book_id)'
    ),
    'review' => array(
        'rev_id'   =>  'int(11) not null auto_increment PRIMARY KEY',
        'book_id'    =>  'int(11) not null',
        'name'    =>  'varchar(150) not null',
        'date'   =>  'datetime not null',
        'rev_content'   =>  'longtext not null',
        'aproved'   =>  'tinyint(1)',
        'user_id'   =>  'int(11)',
        'rev_ip'   =>  'varchar(20)',
        'FOREIGN KEY (book_id) REFERENCES book_list(book_id)'
    ),
    'book_info' => array(
        'info_id'   =>  'bigint(20) not null auto_increment PRIMARY KEY',
        'book_id'    =>  'int(11) not null',
        'info_key'   =>  'varchar(30) not null',
        'info_value'   =>  'longtext not null',
        'FOREIGN KEY (book_id) REFERENCES book_list(book_id)'
    ),
    'user_info' => array(
        'info_id'   =>  'bigint(20) not null auto_increment PRIMARY KEY',
        'user_id'    =>  'int(11) not null',
        'info_key'   =>  'varchar(30) not null',
        'info_value'   =>  'longtext not null',
        'FOREIGN KEY (user_id) REFERENCES user(user_id)'
    ),
    'author_info' => array(
        'info_id'   =>  'bigint(20) not null auto_increment PRIMARY KEY',
        'auth_id'    =>  'int(11) not null',
        'info_key'   =>  'varchar(30) not null',
        'info_value'   =>  'longtext not null',
        'FOREIGN KEY (auth_id) REFERENCES author(auth_id)'
    ),
    
);