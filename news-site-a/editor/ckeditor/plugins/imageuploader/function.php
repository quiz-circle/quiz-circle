<?php

if(!defined("__DIR__")){
    define("__DIR__",  dirname(__FILE__));
}

if(isset($_GET["f"])){
    $f = filter_input(INPUT_GET, 'f', FILTER_SANITIZE_STRING);
    if($f = "loadImages") {
        loadImages();
    }
}
//echo _alert($f);

function _alert($string){
    echo "<script> alert(".$string.") </script>";
}


function loadImages() {
    require(__DIR__ . '/pluginconfig.php');
    
    if(file_exists($useruploadpath)){
        
        $filesizefinal = 0;
        $count = 0;
        
        $dir = $useruploadpath;
        $files = glob("$dir*.{jpg,jpe,jpeg,png,gif,ico}", GLOB_BRACE);
        
        usort($files, create_function('$a, $b', 'return filemtime($a) - filemtime($b);'));
        $message = array();
        for($i=count($files)-1; $i >= 0; $i--):
            $image = $files[$i];
            $image_pathinfo = pathinfo($image);
            $image_extension = $image_pathinfo['extension'];
            $image_filename = $image_pathinfo['filename'];
            $image_basename = $image_pathinfo['basename'];
            
            // image src/url
            $protocol = !empty($_SERVER['HTTPS']) ? 'https://' : 'http://';
            $site = $protocol. $_SERVER['SERVER_NAME'] .'/';
            $image_url = $site.$useruploadfolder."/".$image_basename;
            
            
            
            $size = getimagesize($image);
            $image_height = $size[0];
            $file_size_byte = filesize($image);
            $file_size_kilobyte = ($file_size_byte/1024);
            $file_size_kilobyte_rounded = round($file_size_kilobyte,1);
            $filesizetemp = $file_size_kilobyte_rounded;
            $filesizefinal = round($filesizefinal + $filesizetemp) . " KB";
            $calcsize = round($filesizefinal + $filesizetemp);
            $count = ++$count;
            
            $imageMonth = date("F", filemtime($image));
            $imageYear = date("Y", filemtime($image));
            $imageDay = date("d", filemtime($image));
            
            if(filter_date($image, $message)):
                if($file_style == "block") { ?>
                    <div class="fileDiv"
                         onclick="showEditBar('<?php echo $image_url; ?>','<?php echo $image_height; ?>','<?php echo $count; ?>','<?php echo $image_basename; ?>');"
                         ondblclick="showImage('<?php echo $image_url; ?>','<?php echo $image_height; ?>','<?php echo $image_basename; ?>');"
                         data-imgid="<?php echo $count; ?>">
                        <div class="imgDiv"><img class="fileImg lazy" data-original="<?php echo $image_url; ?>"></div>
                        <p class="fileDescription"><span class="fileMime"><?php echo $image_extension; ?></span> <?php echo $image_filename; ?><?php if($file_extens == "yes"){echo ".$image_extension";} ?></p>
                        <p class="fileTime"><?php echo date ("F d Y H:i", filemtime($image)); ?></p>
                        <p class="fileTime"><?php echo $filesizetemp; ?> KB</p>
                    </div>
                <?php } elseif($file_style == "list") { ?>
                    <div class="fullWidthFileDiv"
                         onclick="showEditBar('<?php echo $image_url; ?>','<?php echo $image_height; ?>','<?php echo $count; ?>','<?php echo $image_basename; ?>');"
                         ondblclick="showImage('<?php echo $image_url; ?>','<?php echo $image_height; ?>','<?php echo $image_basename; ?>');"
                         data-imgid="<?php echo $count; ?>">
                        <div class="fullWidthimgDiv"><img class="fullWidthfileImg lazy" data-original="<?php echo $image_url; ?>"></div>
                        <p class="fullWidthfileDescription"><?php echo $image_filename; ?><?php if($file_extens == "yes"){echo ".$image_extension";} ?></p>

                        <div class="qEditIconsDiv">
                            <img title="Delete File" src="img/cd-icon-qtrash.png" class="qEditIconsImg" onclick="window.location.href = 'imgdelete.php?img=<?php echo $image_basename; ?>'">
                        </div>

                        <p class="fullWidthfileTime fullWidthfileMime fullWidthlastChild"><?php echo $image_extension; ?></p>
                        <p class="fullWidthfileTime"><?php echo $filesizetemp; ?> KB</p>
                        <p class="fullWidthfileTime fullWidth30percent"><?php echo date ("F d Y H:i", filemtime($image)); ?></p>
                    </div>
                <?php }
            endif;
        endfor;
        
        echo (count($message)) === 0 ? "No image Found":"" ;
        
        if($count == 0){
            echo "<div class='fileDiv' style='display:none;'></div>";
            $calcsize = 0;
        }
        if($calcsize == 0){
            $filesizefinal = "0 KB";
        }
        if($calcsize >= 1024){
            $filesizefinal = round($filesizefinal/1024,1) . " MB";
        }
        
        echo "
        <script>
            $( '#finalsize' ).html('$filesizefinal');
            $( '#finalcount' ).html('$count');
        </script>
        ";
    } else {
        echo '<div id="folderError">'.$alerts9.' <b>'.$useruploadfolder.'</b> '.$alerts10;
    } 
}

function filter_date($image, &$message= ""){
    $imageMonth = date("F", filemtime($image));
    $imageYear = date("Y", filemtime($image));
    $imageDay = date("d", filemtime($image));
    
    //echo GET_DATA("year");
    if(GET_DATA("year")){
        if(GET_DATA("year") == $imageYear && GET_DATA("month") == "all" && GET_DATA("day") == "all"){
            $message[] = $image;
            return GET_DATA("year") == $imageYear;
        }else if(GET_DATA("year") == $imageYear && GET_DATA("month") == $imageMonth && GET_DATA("day") == "all"){
            $message[] = $image;
            return GET_DATA("year") == $imageYear && GET_DATA("month") == $imageMonth;
        }else if(GET_DATA("year") == $imageYear && GET_DATA("month") == $imageMonth && GET_DATA("day") == $imageDay){
            $message[] = $image;
            return GET_DATA("year") == $imageYear && GET_DATA("month") == $imageMonth && GET_DATA("day") == $imageDay;
        }
        return false;
    }
    $message[] = $image;
    return true;
}

function pathHistory() {
    require(__DIR__ . '/pluginconfig.php');
    $latestpathes = array_slice($foldershistory, -3);
    $latestpathes = array_reverse($latestpathes);
    foreach($latestpathes as $folder) {
        echo '<p class="pathHistory" onclick="useHistoryPath(\''.$folder.'\');">'.$folder.'</p>';
    }
}

function GET_DATA($key = false, $default = null){
    if(isset($_GET) && !empty($_GET)){
        if($key){
            
            
           if(isset($_GET[$key]) && !empty($_GET[$key])){
               return $_GET[$key];
           }else if (isset($_GET[$key])){
               if($_GET[$key] == 0){
                    return $_GET[$key];   
               }
           }
           return $default;
        }
        return $_GET;
    }
    return $default;
}

function SESSION_DATA($key = false, $default = null){
    if(isset($_SESSION) && !empty($_SESSION)){
        if($key){
           if(isset($_SESSION[$key]) && !empty($_SESSION[$key])){
               return $_SESSION[$key];
           }
           return $default;
        }
        return $_SESSION;
    }
    return $default;
}

function POST_DATA($key = false, $default = null){
    if(isset($_POST) && !empty($_POST)){
        if($key){
           if(isset($_POST[$key]) && !empty($_POST[$key])){
               return $_POST[$key];
           }
           return $default;
        }
        return $_POST;
    }
    return $default;
}