-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.20 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for quiz-circle
CREATE DATABASE IF NOT EXISTS `quiz-circle` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `quiz-circle`;

-- Dumping structure for table quiz-circle.admin
DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_type` enum('admin','teacher','custom') DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `permissions` text,
  `mobile_number` varchar(50) DEFAULT NULL,
  `user_type_hash` varchar(32) CHARACTER SET ascii DEFAULT NULL,
  PRIMARY KEY (`admin_id`),
  UNIQUE KEY `user_type_hash` (`user_type_hash`),
  KEY `FK_admin_user` (`user_id`),
  CONSTRAINT `FK_admin_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `FK_admin_user_2` FOREIGN KEY (`user_type_hash`) REFERENCES `user` (`user_type_hash`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table quiz-circle.admin: ~2 rows (approximately)
DELETE FROM `admin`;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` (`admin_id`, `admin_type`, `user_id`, `permissions`, `mobile_number`, `user_type_hash`) VALUES
	(1, 'admin', 1, '{"a":[1,2,3,4]}', NULL, '3cd33cd2ae8930ecabb4478aed33671a'),
	(2, 'teacher', 122, NULL, NULL, 'ba833ccae76930ecabb4478aed3cb452');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;

-- Dumping structure for table quiz-circle.class
DROP TABLE IF EXISTS `class`;
CREATE TABLE IF NOT EXISTS `class` (
  `class_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  `url_ref` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'category',
  `parent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`class_id`),
  UNIQUE KEY `url_ref` (`url_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=399 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table quiz-circle.class: ~111 rows (approximately)
DELETE FROM `class`;
/*!40000 ALTER TABLE `class` DISABLE KEYS */;
INSERT INTO `class` (`class_id`, `name`, `description`, `level`, `url_ref`, `type`, `parent`) VALUES
	(76, 'বাংলাদেশ', '', 0, 'bangladesh', 'category', 0),
	(80, 'India', '', 0, 'india-2', 'category', 0),
	(81, 'Pakistan', '', 0, 'pakistan', 'category', 0),
	(82, 'Dhaka', '', 0, 'dhaka', 'category', 76),
	(83, 'Rajshahi', '', 0, 'rajshashi', 'category', 76),
	(84, 'Kolkata', '', 0, 'kolkata', 'category', 76),
	(87, 'Home Section', '', 0, 'home-section', 'category', 0),
	(188, 'নতুন প্রজন্ম', '', 0, 'নতুন-প্রজন্ম', 'tag', 0),
	(192, 'ভাইস প্রেসিডেন্ট', '', 0, 'ভাইস-প্রেসিডেন্ট', 'tag', 0),
	(209, 'MIU', '', 0, 'miu', 'tag', 0),
	(210, 'Defodil University', '', 0, 'defodil-university', 'tag', 0),
	(211, 'University', '', 0, 'university', 'tag', 0),
	(212, 'Welcome', '', 0, 'welcome', 'tag', 0),
	(213, 'Unn', '', 0, 'unn', 'subject', 0),
	(214, 'উন্নয়ন ', '', 0, 'উন্নয়ন', 'tag', 0),
	(215, 'hhcs', '', 0, 'hhcs', 'tag', 0),
	(216, 'hhcshhcs', '', 0, 'hhcshhcs', 'tag', 0),
	(217, 'আসর', '', 0, 'আসর', 'tag', 0),
	(218, 'জাস্টিন বিবার', '', 0, 'জাস্টিন-বিবার', 'tag', 0),
	(219, 'সেলেনা', '', 0, 'সেলেনা', 'tag', 0),
	(220, 'যুদ্ধ ', '', 0, 'যুদ্ধ', 'tag', 0),
	(222, 'আন্তর্জাতিক', '', 0, 'আন্তর্জাতিক', 'tag', 0),
	(223, 'জিডিপি', '', 0, 'জিডিপি', 'tag', 0),
	(224, 'খালেদা জিয়া', '', 0, 'খালেদা-জিয়া', 'tag', 0),
	(225, 'বিএনপি', '', 0, 'বিএনপি', 'tag', 0),
	(226, 'মির্জা ফখরুল', '', 0, 'মির্জা-ফখরুল', 'tag', 0),
	(227, 'বিএনপির চেয়ারপারসন', '', 0, 'বিএনপির-চেয়ারপারসন', 'tag', 0),
	(228, 'ভিআর ', '', 0, 'ভিআর', 'tag', 0),
	(229, 'স্মার্টফোন ও ট্যাব মেলা', '', 0, 'স্মার্টফোন-ও-ট্যাব-মেলা', 'tag', 0),
	(230, 'তথ্যপ্রযুক্তি প্রতিমন্ত্রী ', '', 0, 'তথ্যপ্রযুক্তি-প্রতিমন্ত্রী', 'tag', 0),
	(231, 'ট্যাবলেট কম্পিউটার', '', 0, 'ট্যাবলেট-কম্পিউটার', 'tag', 0),
	(232, 'জুনাইদ আহমেদ পলক', '', 0, 'জুনাইদ-আহমেদ-পলক', 'tag', 0),
	(238, 'International', '', 0, 'international', 'tag', 0),
	(242, 'Bangla', '', 0, 'bangla-3', 'tutorial-category', 0),
	(243, 'Computer Science', '', 0, 'tutorial-computer-science', 'tutorial-category', 0),
	(244, 'Physical Science', '', 0, 'physical-science', 'video-category', 0),
	(245, 'Science', '', 0, 'science', 'paragraph-category', 0),
	(246, 'Urgent', '', 0, 'urgent', 'notice-category', 0),
	(247, 'Algorithm', '', 0, 'algorithm', 'quiz-category', 241),
	(249, 'Kolikata', '', 0, 'kolikata', 'category', 80),
	(254, 'Savar', '', 0, 'savar', 'category', 82),
	(256, 'df', '', 0, 'df', 'category', 251),
	(278, 'tree', '', 0, 'tree', 'question-category', 0),
	(281, 'cx', '', 0, 'cx', 'question-category', 277),
	(283, 'ddf', '', 0, 'ddf-2', 'question-category', 277),
	(284, 'ddf', '', 0, 'ddf-3', 'question-category', 279),
	(285, '', '', 0, '-6', 'question-category', 277),
	(288, 'Computer Science', '', 0, 'computer-science', 'question-category', 0),
	(289, 'ffds', '', 0, 'ffds', 'question-category', 279),
	(297, 'ggfdgh fgh', '', 0, 'ggfdgh-fgh', 'tutorial-category', 0),
	(298, 'dfg', '', 0, 'dfg', 'tutorial-category', 0),
	(299, 'dsgsdfdsfg', '', 0, 'dsgsdfdsfg', 'tutorial-category', 0),
	(300, 'ttdsf', '', 0, 'ttdsf', 'tutorial-category', 0),
	(301, 'sdf', '', 0, 'sdf-2', 'tutorial-category', 296),
	(302, 'dsdg', '', 0, 'dsdg', 'tutorial-category', 0),
	(303, 'dfgsdfg', '', 0, 'dfgsdfg', 'tutorial-category', 0),
	(304, 'Iopo', '', 0, 'iopo', 'tutorial-category', 0),
	(305, 'Opp', '', 0, 'opp', 'tutorial-category', 0),
	(306, 'Programming Language', '', 0, 'programming-language', 'tutorial-category', 0),
	(307, 'sdfg sdfg ', '', 0, 'sdfg-sdfg', 'tutorial-category', 0),
	(308, 'xxcvb', '', 0, 'xxcvb', 'tutorial-category', 302),
	(309, 'cadBB fg s', '', 0, 'cadbb-fg-s', 'tutorial-category', 297),
	(310, 'saf', '', 0, 'saf', 'tutorial-category', 302),
	(312, 'fsaaaaaaaaaaaac', '', 0, 'fsaaaaaaaaaaaac', 'tutorial-category', 0),
	(313, 'Book reference', '', 0, 'book-reference', 'tutorial-category', 0),
	(314, 'jukh', '', 0, 'jukh', 'tutorial-category', 0),
	(315, 'fd', '', 0, 'fd-2', 'tutorial-category', 0),
	(316, 'dfdsvdfg', '', 0, 'dfdsvdfg', 'tutorial-category', 0),
	(317, 'ddc', '', 0, 'ddc', 'tutorial-category', 0),
	(318, 'aab', '', 0, 'aab', 'tutorial-category', 0),
	(325, 'vxg', '', 0, 'vxg', 'tutorial-category', 0),
	(327, 'lll', '', 0, 'lll', 'tutorial-category', 0),
	(328, 'Medical Science', '', 0, 'medical-science', 'video-category', 0),
	(329, 'Pharmacist', '', 0, 'pharmacist', 'video-category', 0),
	(330, 'lllll', '', 0, 'lllll', 'tutorial-category', 0),
	(331, 'raju', '', 0, 'raju', 'video-category', 0),
	(332, 'saif', '', 0, 'saif', 'video-category', 0),
	(333, 'azom', '', 0, 'azom', 'video-category', 0),
	(336, 'fdf', '', 0, 'fdf', 'quiz-category', 322),
	(338, 'teacheer', '', 0, 'teacheer', 'video-category', 0),
	(339, 'Trrr', '', 0, 'trrr', 'video-category', 0),
	(342, 'fgyggftyfgty', '', 0, 'fgyggftyfgty', 'video-category', 0),
	(343, 'asdf', '', 0, 'asdf', 'video-category', 0),
	(344, 'sadfas', '', 0, 'sadfas', 'video-category', 0),
	(345, 'asdacas sdf ', '', 0, 'asdacas-sdf', 'video-category', 0),
	(346, 'df dasfasf ', '', 0, 'df-dasfasf', 'video-category', 0),
	(358, 'Normal', '', 0, 'normal', 'notice-category', 0),
	(361, 'iikl', '', 0, 'iikl', 'question-category', 284),
	(362, 'Computer Science', '', 0, 'computer-science-2', 'paragraph-category', 0),
	(363, 'Web Programming', '', 0, 'web-programming', 'paragraph-category', 362),
	(365, 'Computer Arithmetics', '', 0, 'computer-arithmetics', 'question-category', 288),
	(366, 'Hard', '', 0, 'hard', 'question-category', 0),
	(378, 'Computer Science', '', 0, 'computer-science-3', 'quiz-category', 0),
	(379, 'Algorithm', '', 0, 'algorithm-2', 'quiz-category', 0),
	(380, 'dd', '', 0, 'dd-4', 'quiz-category', 378),
	(381, 'rr', '', 0, 'rr', 'quiz-category', 0),
	(382, 'Tyg', '', 0, 'tyg', 'tag', 0),
	(383, 'tr', '', 0, 'tr-2', 'tag', 0),
	(384, 'fd', '', 0, 'fd-3', 'tag', 0),
	(385, 'ED', '', 0, 'ed', 'tag', 0),
	(386, 'Rd', '', 0, 'rd', 'tag', 0),
	(387, 'RSSF', '', 0, 'rssf', 'tag', 0),
	(388, 'Header Menu', '', 0, 'header-menu', 'nav-menu', 0),
	(389, 'ffd', '', 0, 'ffd-2', 'tutorial-category', 0),
	(390, 'sfseR', '', 0, 'sfser', 'tutorial-category', 242),
	(391, 'Footer Menu', '', 0, 'footer-menu', 'nav-menu', 0),
	(392, 'CSE', '', 0, 'cse', 'notice-category', 0),
	(393, 'EEE', '', 0, 'eee', 'notice-category', 0),
	(394, 'LLB', '', 0, 'llb', 'notice-category', 0),
	(395, 'JOURNALIZM', '', 0, 'journalizm', 'notice-category', 0),
	(396, 'BBA', '', 0, 'bba', 'notice-category', 0),
	(397, 'সাধারণ জ্ঞান', '', 0, 'সাধারণ-জ্ঞান', 'question-category', 0),
	(398, 'সাধারণ জ্ঞান', '', 0, 'সাধারণ-জ্ঞান-2', 'quiz-category', 0);
/*!40000 ALTER TABLE `class` ENABLE KEYS */;

-- Dumping structure for table quiz-circle.class_relation
DROP TABLE IF EXISTS `class_relation`;
CREATE TABLE IF NOT EXISTS `class_relation` (
  `rel_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `priority` int(4) NOT NULL DEFAULT '9999',
  `type` varchar(25) NOT NULL DEFAULT 'category',
  PRIMARY KEY (`rel_id`),
  KEY `FK_class_relation_class` (`class_id`),
  CONSTRAINT `FK_class_relation_class` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=805 DEFAULT CHARSET=utf8;

-- Dumping data for table quiz-circle.class_relation: ~45 rows (approximately)
DELETE FROM `class_relation`;
/*!40000 ALTER TABLE `class_relation` DISABLE KEYS */;
INSERT INTO `class_relation` (`rel_id`, `class_id`, `entity_id`, `priority`, `type`) VALUES
	(295, 288, 92, 9999, 'question-category'),
	(296, 365, 92, 9999, 'question-category'),
	(302, 288, 93, 9999, 'question-category'),
	(304, 358, 105, 9999, 'notice-category'),
	(305, 246, 106, 9999, 'notice-category'),
	(306, 246, 107, 9999, 'notice-category'),
	(307, 246, 108, 9999, 'notice-category'),
	(308, 246, 109, 9999, 'notice-category'),
	(310, 246, 110, 9999, 'notice-category'),
	(311, 288, 94, 9999, 'question-category'),
	(312, 378, 1, 9999, 'quiz-category'),
	(313, 379, 1, 9999, 'quiz-category'),
	(314, 378, 2, 9999, 'quiz-category'),
	(316, 288, 95, 9999, 'question-category'),
	(319, 288, 97, 9999, 'question-category'),
	(320, 288, 98, 9999, 'question-category'),
	(324, 365, 99, 9999, 'question-category'),
	(327, 288, 101, 9999, 'question-category'),
	(328, 288, 102, 9999, 'question-category'),
	(329, 288, 103, 9999, 'question-category'),
	(330, 288, 104, 9999, 'question-category'),
	(365, 378, 7, 9999, 'quiz-category'),
	(375, 288, 105, 9999, 'question-category'),
	(376, 288, 106, 9999, 'question-category'),
	(378, 288, 107, 9999, 'question-category'),
	(379, 288, 108, 9999, 'question-category'),
	(381, 288, 109, 9999, 'question-category'),
	(382, 288, 110, 9999, 'question-category'),
	(784, 246, 104, 9999, 'notice-category'),
	(785, 392, 104, 9999, 'notice-category'),
	(786, 209, 5, 9999, 'tag'),
	(787, 378, 5, 9999, 'quiz-category'),
	(789, 378, 3, 9999, 'quiz-category'),
	(790, 398, 22, 9999, 'quiz-category'),
	(791, 397, 113, 9999, 'question-category'),
	(792, 397, 114, 9999, 'question-category'),
	(793, 397, 115, 9999, 'question-category'),
	(794, 397, 116, 9999, 'question-category'),
	(795, 397, 117, 9999, 'question-category'),
	(796, 397, 118, 9999, 'question-category'),
	(798, 397, 120, 9999, 'question-category'),
	(800, 397, 122, 9999, 'question-category'),
	(801, 288, 119, 9999, 'question-category'),
	(802, 288, 100, 9999, 'question-category'),
	(803, 397, 121, 9999, 'question-category');
/*!40000 ALTER TABLE `class_relation` ENABLE KEYS */;

-- Dumping structure for table quiz-circle.content
DROP TABLE IF EXISTS `content`;
CREATE TABLE IF NOT EXISTS `content` (
  `content_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_title` text,
  `answer` text,
  `description` text,
  `added_date` datetime DEFAULT NULL,
  `edited_date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `content_type` varchar(50) DEFAULT 'question',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `visibility` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`content_id`),
  KEY `FK_questions_user` (`user_id`),
  CONSTRAINT `content_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table quiz-circle.content: ~7 rows (approximately)
DELETE FROM `content`;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` (`content_id`, `content_title`, `answer`, `description`, `added_date`, `edited_date`, `user_id`, `content_type`, `status`, `visibility`) VALUES
	(104, 'A Quiz competition on "fundamental of computing"', NULL, 'A Quiz competition on "fundamental of computing" Will be held on next month', '2017-11-27 07:41:19', '2017-12-09 11:33:47', 1, 'notice', 1, NULL),
	(105, 'Ds', NULL, NULL, '2017-11-27 08:03:29', '2017-11-27 08:03:29', 1, 'notice', 0, NULL),
	(106, 'ERT', NULL, NULL, '2017-11-27 08:21:10', '2017-11-27 08:21:10', 1, 'notice', 0, NULL),
	(107, 'ERT', NULL, NULL, '2017-11-27 08:21:12', '2017-11-27 08:21:12', 1, 'notice', 0, NULL),
	(108, 'ERT', NULL, NULL, '2017-11-27 08:21:13', '2017-11-27 08:21:13', 1, 'notice', 0, NULL),
	(109, 'ERT', NULL, NULL, '2017-11-27 08:21:14', '2017-11-27 08:21:14', 1, 'notice', 0, NULL),
	(110, 'Quiz Result of "Fundamental of Computing" Published ', NULL, 'Quiz Result of "Fundamental of Computing" has been published. All the student are requested to check their result on online and collect the price from the QUIZ CIRCLE Office\r\n-Quiz Circle Team', '2017-11-27 08:36:00', '2017-11-27 08:36:00', 1, 'notice', 1, NULL);
/*!40000 ALTER TABLE `content` ENABLE KEYS */;

-- Dumping structure for table quiz-circle.given_answers
DROP TABLE IF EXISTS `given_answers`;
CREATE TABLE IF NOT EXISTS `given_answers` (
  `answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `participation_id` int(11) DEFAULT NULL,
  `quest_assign_id` int(11) DEFAULT NULL,
  `given_answer` varchar(250) DEFAULT NULL,
  `right_answer` varchar(250) DEFAULT NULL,
  `answer_options` text,
  PRIMARY KEY (`answer_id`),
  KEY `FK__questions` (`quest_assign_id`),
  KEY `FK__quiz_participation` (`participation_id`),
  CONSTRAINT `FK__questions` FOREIGN KEY (`quest_assign_id`) REFERENCES `quest_assign` (`id`),
  CONSTRAINT `FK_given_answers_participation` FOREIGN KEY (`participation_id`) REFERENCES `participation` (`participation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=345 DEFAULT CHARSET=utf8;

-- Dumping data for table quiz-circle.given_answers: ~213 rows (approximately)
DELETE FROM `given_answers`;
/*!40000 ALTER TABLE `given_answers` DISABLE KEYS */;
INSERT INTO `given_answers` (`answer_id`, `participation_id`, `quest_assign_id`, `given_answer`, `right_answer`, `answer_options`) VALUES
	(49, 44, 11, '', 'The number of its distinct counting digits', '{"option_label":{"1":"(B)","2":"(C)","3":"(D)","0":"(A)"},"right_answer":"The number of its distinct counting digits","options":{"1":"The number of its distinct counting digits","2":"The range of its distinct counting number","3":"None of above","0":"The range of its distinct counting digits"}}'),
	(50, 45, 4, '', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(51, 46, 4, '', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(52, 47, 4, '', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(53, 48, 4, '', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(54, 49, 1, '', '110010101', '{"option_label":["A","B","C","D"],"right_answer":"110010101","options":["110111000100","110010101","100000101","110101110101"]}'),
	(55, 50, 4, '', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(56, 51, 1, '', '110010101', '{"option_label":["A","B","C","D"],"right_answer":"110010101","options":["110111000100","110010101","100000101","110101110101"]}'),
	(57, 52, 4, '', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(58, 53, 1, '', '110010101', '{"option_label":["A","B","C","D"],"right_answer":"110010101","options":["110111000100","110010101","100000101","110101110101"]}'),
	(59, 54, 4, 'LSI and VLSI', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(60, 55, 4, 'Integreted Circuit', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(61, 56, 1, '110111000100', '110010101', '{"option_label":["A","B","C","D"],"right_answer":"110010101","options":["110111000100","110010101","100000101","110101110101"]}'),
	(62, 57, 4, 'Transistor', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(63, 58, 1, '110101110101', '110010101', '{"option_label":["A","B","C","D"],"right_answer":"110010101","options":["110111000100","110010101","100000101","110101110101"]}'),
	(64, 58, 2, 'র‍্যান্ডম আক্সেস মেমরি', 'র‍্যান্ডম আক্সেস মেমরি', '{"option_label":["A","B","C","D"],"right_answer":"র‍্যান্ডম আক্সেস মেমরি","options":["Read Only Memory","Rapid Access Memory","র‍্যান্ডম আক্সেস মেমরি","Random Address Memory"]}'),
	(65, 58, 3, 'CPU is the fasted memory', 'RAM is parmanent', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"RAM is parmanent","options":["RAM is parmanent","HDD Memory is temporery","CPU is the fasted memory","Mouse is a Output Device"]}'),
	(66, 59, 4, 'Vacuam Tubes', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(67, 59, 5, 'Charls Babez', 'Newton', '{"option_label":["A","B","C","D"],"right_answer":"Newton","options":["Wiliam English","Steve Jobs","Charls Babez","Newton"]}'),
	(68, 60, 4, 'Vacuam Tubes', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(69, 60, 5, 'Charls Babez', 'Charls Babez', '{"option_label":["A","B","C","D"],"right_answer":"Charls Babez","options":["Wiliam English","Steve Jobs","Charls Babez","Newton"]}'),
	(70, 61, 1, '100000101', '110010101', '{"option_label":["A","B","C","D"],"right_answer":"110010101","options":["110111000100","110010101","100000101","110101110101"]}'),
	(71, 61, 2, 'র‍্যান্ডম আক্সেস মেমরি', 'র‍্যান্ডম আক্সেস মেমরি', '{"option_label":["A","B","C","D"],"right_answer":"র‍্যান্ডম আক্সেস মেমরি","options":["Read Only Memory","Rapid Access Memory","র‍্যান্ডম আক্সেস মেমরি","Random Address Memory"]}'),
	(72, 61, 3, 'CPU is the fasted memory', 'CPU is the fasted memory', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"RAM is parmanent","options":["RAM is parmanent","HDD Memory is temporery","CPU is the fasted memory","Mouse is a Output Device"]}'),
	(73, 62, 1, '100000101', '110010101', '{"option_label":["A","B","C","D"],"right_answer":"110010101","options":["110111000100","110010101","100000101","110101110101"]}'),
	(74, 62, 2, 'Random Address Memory', 'র‍্যান্ডম আক্সেস মেমরি', '{"option_label":["A","B","C","D"],"right_answer":"র‍্যান্ডম আক্সেস মেমরি","options":["Read Only Memory","Rapid Access Memory","র‍্যান্ডম আক্সেস মেমরি","Random Address Memory"]}'),
	(75, 62, 3, 'RAM is parmanent', 'CPU is the fasted memory', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"CPU is the fasted memory","options":["RAM is parmanent","HDD Memory is temporery","CPU is the fasted memory","Mouse is a Output Device"]}'),
	(76, 63, 1, '110010101', '110010101', '{"option_label":["A","B","C","D"],"right_answer":"110010101","options":["110111000100","110010101","100000101","110101110101"]}'),
	(77, 63, 2, 'Read Only Memory', 'র‍্যান্ডম আক্সেস মেমরি', '{"option_label":["A","B","C","D"],"right_answer":"র‍্যান্ডম আক্সেস মেমরি","options":["Read Only Memory","Rapid Access Memory","র‍্যান্ডম আক্সেস মেমরি","Random Address Memory"]}'),
	(78, 63, 3, 'RAM is parmanent', 'CPU is the fasted memory', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"CPU is the fasted memory","options":["RAM is parmanent","HDD Memory is temporery","CPU is the fasted memory","Mouse is a Output Device"]}'),
	(79, 64, 1, '', '110010101', '{"option_label":["A","B","C","D"],"right_answer":"110010101","options":["110111000100","110010101","100000101","110101110101"]}'),
	(80, 64, 2, '', 'র‍্যান্ডম আক্সেস মেমরি', '{"option_label":["A","B","C","D"],"right_answer":"র‍্যান্ডম আক্সেস মেমরি","options":["Read Only Memory","Rapid Access Memory","র‍্যান্ডম আক্সেস মেমরি","Random Address Memory"]}'),
	(81, 64, 3, '', 'CPU is the fasted memory', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"CPU is the fasted memory","options":["RAM is parmanent","HDD Memory is temporery","CPU is the fasted memory","Mouse is a Output Device"]}'),
	(82, 65, 4, 'Integreted Circuit', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(83, 65, 5, 'Wiliam English', 'Charls Babez', '{"option_label":["A","B","C","D"],"right_answer":"Charls Babez","options":["Wiliam English","Steve Jobs","Charls Babez","Newton"]}'),
	(84, 66, 1, '110010101', '110010101', '{"option_label":["A","B","C","D"],"right_answer":"110010101","options":["110111000100","110010101","100000101","110101110101"]}'),
	(85, 66, 2, 'র‍্যান্ডম আক্সেস মেমরি', 'র‍্যান্ডম আক্সেস মেমরি', '{"option_label":["A","B","C","D"],"right_answer":"র‍্যান্ডম আক্সেস মেমরি","options":["Read Only Memory","Rapid Access Memory","র‍্যান্ডম আক্সেস মেমরি","Random Address Memory"]}'),
	(86, 66, 3, 'CPU is the fasted memory', 'CPU is the fasted memory', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"CPU is the fasted memory","options":["RAM is parmanent","HDD Memory is temporery","CPU is the fasted memory","Mouse is a Output Device"]}'),
	(87, 67, 4, 'Integreted Circuit', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(88, 67, 5, 'Charls Babez', 'Charls Babez', '{"option_label":["A","B","C","D"],"right_answer":"Charls Babez","options":["Wiliam English","Steve Jobs","Charls Babez","Newton"]}'),
	(89, 68, 4, 'Integreted Circuit', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(90, 68, 5, 'Steve Jobs', 'Charls Babez', '{"option_label":["A","B","C","D"],"right_answer":"Charls Babez","options":["Wiliam English","Steve Jobs","Charls Babez","Newton"]}'),
	(91, 69, 4, 'Integreted Circuit', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(92, 69, 5, 'Wiliam English', 'Charls Babez', '{"option_label":["A","B","C","D"],"right_answer":"Charls Babez","options":["Wiliam English","Steve Jobs","Charls Babez","Newton"]}'),
	(93, 70, 4, 'Vacuam Tubes', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(94, 70, 5, 'Charls Babez', 'Charls Babez', '{"option_label":["A","B","C","D"],"right_answer":"Charls Babez","options":["Wiliam English","Steve Jobs","Charls Babez","Newton"]}'),
	(95, 71, 4, 'Transistor', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(96, 71, 5, 'Charls Babez', 'Charls Babez', '{"option_label":["A","B","C","D"],"right_answer":"Charls Babez","options":["Wiliam English","Steve Jobs","Charls Babez","Newton"]}'),
	(97, 72, 4, 'Vacuam Tubes', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(98, 72, 5, 'Steve Jobs', 'Charls Babez', '{"option_label":["A","B","C","D"],"right_answer":"Charls Babez","options":["Wiliam English","Steve Jobs","Charls Babez","Newton"]}'),
	(99, 73, 4, '', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(100, 73, 5, '', 'Charls Babez', '{"option_label":["A","B","C","D"],"right_answer":"Charls Babez","options":["Wiliam English","Steve Jobs","Charls Babez","Newton"]}'),
	(101, 74, 4, 'Vacuam Tubes', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(102, 74, 5, 'Charls Babez', 'Charls Babez', '{"option_label":["A","B","C","D"],"right_answer":"Charls Babez","options":["Wiliam English","Steve Jobs","Charls Babez","Newton"]}'),
	(103, 75, 1, '110010101', '110010101', '{"option_label":["A","B","C","D"],"right_answer":"110010101","options":["110111000100","110010101","100000101","110101110101"]}'),
	(104, 75, 2, 'র‍্যান্ডম আক্সেস মেমরি', 'র‍্যান্ডম আক্সেস মেমরি', '{"option_label":["A","B","C","D"],"right_answer":"র‍্যান্ডম আক্সেস মেমরি","options":["Read Only Memory","Rapid Access Memory","র‍্যান্ডম আক্সেস মেমরি","Random Address Memory"]}'),
	(105, 75, 3, 'CPU is the fasted memory', 'CPU is the fasted memory', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"CPU is the fasted memory","options":["RAM is parmanent","HDD Memory is temporery","CPU is the fasted memory","Mouse is a Output Device"]}'),
	(106, 76, 1, '110111000100', '110010101', '{"option_label":["A","B","C","D"],"right_answer":"110010101","options":["110111000100","110010101","100000101","110101110101"]}'),
	(107, 76, 2, 'র‍্যান্ডম আক্সেস মেমরি', 'র‍্যান্ডম আক্সেস মেমরি', '{"option_label":["A","B","C","D"],"right_answer":"র‍্যান্ডম আক্সেস মেমরি","options":["Read Only Memory","Rapid Access Memory","র‍্যান্ডম আক্সেস মেমরি","Random Address Memory"]}'),
	(108, 76, 3, 'HDD Memory is temporery', 'CPU is the fasted memory', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"CPU is the fasted memory","options":["RAM is parmanent","HDD Memory is temporery","CPU is the fasted memory","Mouse is a Output Device"]}'),
	(109, 77, 11, 'The range of its distinct counting number', 'The number of its distinct counting digits', '{"option_label":{"1":"(B)","2":"(C)","3":"(D)","0":"(A)"},"right_answer":"The number of its distinct counting digits","options":{"1":"The number of its distinct counting digits","2":"The range of its distinct counting number","3":"None of above","0":"The range of its distinct counting digits"}}'),
	(110, 77, 12, 'Frame', 'Interface', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"Interface","options":["Class","Programming Language","Interface","Frame"]}'),
	(111, 77, 13, 'Base is small', 'Base is small', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"Base is small","options":["It is not used as usual","Base is small","Base is big","none of above"]}'),
	(112, 77, 14, '1', '1', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"1","options":["11","00","110","1"]}'),
	(113, 63, 4, '', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(114, 63, 5, 'Charls Babez', 'Charls Babez', '{"option_label":["A","B","C","D"],"right_answer":"Charls Babez","options":["Wiliam English","Steve Jobs","Charls Babez","Newton"]}'),
	(115, 64, 11, 'The number of its distinct counting digits', 'The number of its distinct counting digits', '{"option_label":{"1":"(B)","2":"(C)","3":"(D)","0":"(A)"},"right_answer":"The number of its distinct counting digits","options":{"1":"The number of its distinct counting digits","2":"The range of its distinct counting number","3":"None of above","0":"The range of its distinct counting digits"}}'),
	(116, 64, 12, 'Interface', 'Interface', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"Interface","options":["Class","Programming Language","Interface","Frame"]}'),
	(117, 64, 13, 'Base is small', 'Base is small', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"Base is small","options":["It is not used as usual","Base is small","Base is big","none of above"]}'),
	(118, 64, 14, '1', '1', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"1","options":["11","00","110","1"]}'),
	(119, 65, 11, 'None of above', 'The number of its distinct counting digits', '{"option_label":{"1":"(B)","2":"(C)","3":"(D)","0":"(A)"},"right_answer":"The number of its distinct counting digits","options":{"1":"The number of its distinct counting digits","2":"The range of its distinct counting number","3":"None of above","0":"The range of its distinct counting digits"}}'),
	(120, 65, 12, 'Interface', 'Interface', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"Interface","options":["Class","Programming Language","Interface","Frame"]}'),
	(121, 65, 13, 'Base is small', 'Base is small', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"Base is small","options":["It is not used as usual","Base is small","Base is big","none of above"]}'),
	(122, 65, 14, '1', '1', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"1","options":["11","00","110","1"]}'),
	(123, 66, 11, 'The number of its distinct counting digits', 'The number of its distinct counting digits', '{"option_label":{"1":"(B)","2":"(C)","3":"(D)","0":"(A)"},"right_answer":"The number of its distinct counting digits","options":{"1":"The number of its distinct counting digits","2":"The range of its distinct counting number","3":"None of above","0":"The range of its distinct counting digits"}}'),
	(124, 66, 12, 'Interface', 'Interface', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"Interface","options":["Class","Programming Language","Interface","Frame"]}'),
	(125, 66, 13, 'Base is small', 'Base is small', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"Base is small","options":["It is not used as usual","Base is small","Base is big","none of above"]}'),
	(126, 66, 14, '1', '1', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"1","options":["11","00","110","1"]}'),
	(127, 67, 11, 'None of above', 'The number of its distinct counting digits', '{"option_label":{"1":"(B)","2":"(C)","3":"(D)","0":"(A)"},"right_answer":"The number of its distinct counting digits","options":{"1":"The number of its distinct counting digits","2":"The range of its distinct counting number","3":"None of above","0":"The range of its distinct counting digits"}}'),
	(128, 67, 12, 'Frame', 'Interface', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"Interface","options":["Class","Programming Language","Interface","Frame"]}'),
	(129, 67, 13, 'Base is small', 'Base is small', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"Base is small","options":["It is not used as usual","Base is small","Base is big","none of above"]}'),
	(130, 67, 14, '11', '1', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"1","options":["11","00","110","1"]}'),
	(131, 68, 44, '১০', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(132, 68, 45, 'ব্রিটেন', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(133, 68, 46, '৮০ বছর', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(134, 68, 47, 'সুপ্রীম কোর্ট।', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(135, 68, 48, 'রাষ্ট্রপতি', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(136, 68, 49, 'রূপতত্ত্ব ', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(137, 68, 50, 'ডেনমার্ক', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(138, 68, 51, 'Ctrl + B', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(139, 68, 52, 'adjective', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(140, 68, 53, 'ফ্রিডম পার্টি', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}'),
	(141, 69, 44, '৯', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(142, 69, 45, 'ব্রিটেন', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(143, 69, 46, '৯০ বছর', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(144, 69, 47, 'সুপ্রীম কোর্ট।', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(145, 69, 48, 'রাষ্ট্রপতি', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(146, 69, 49, 'কোনটিই নয়', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(147, 69, 50, 'ডেনমার্ক', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(148, 69, 51, '', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(149, 69, 52, 'adjective', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(150, 69, 53, 'আফ্রিকান ন্যাশনাল কংগ্রেস', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}'),
	(151, 70, 44, '১০', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(152, 70, 45, 'ব্রিটেন', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(153, 70, 46, '৮০ বছর', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(154, 70, 47, 'সুপ্রীম কোর্ট।', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(155, 70, 48, 'রাষ্ট্রপতি', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(156, 70, 49, 'কোনটিই নয়', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(157, 70, 50, 'ডেনমার্ক', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(158, 70, 51, 'Ctrl + B', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(159, 70, 52, 'adjective', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(160, 70, 53, 'আফ্রিকান ন্যাশনাল কংগ্রেস', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}'),
	(161, 71, 44, '৮', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(162, 71, 45, 'আমেরিকা', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(163, 71, 46, '৬০ বছর', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(164, 71, 47, 'সুপ্রীম কোর্ট।', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(165, 71, 48, 'রাষ্ট্রপতি', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(166, 71, 49, 'বাক্যতত্ত্ব', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(167, 71, 50, 'ইউ এস এ', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(168, 71, 51, 'Ctrl + B', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(169, 71, 52, '', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(170, 71, 53, 'আফ্রিকান সোস্যালিস্ট পার্টি', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}'),
	(171, 72, 44, '১০', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(172, 72, 45, 'ব্রিটেন', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(173, 72, 46, '৮০ বছর', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(174, 72, 47, 'সুপ্রীম কোর্ট।', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(175, 72, 48, 'রাষ্ট্রপতি', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(176, 72, 49, 'রূপতত্ত্ব ', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(177, 72, 50, 'ডেনমার্ক', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(178, 72, 51, 'Ctrl + B', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(179, 72, 52, 'adjective', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(180, 72, 53, 'ফ্রিডম পার্টি', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}'),
	(181, 73, 44, '১০', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(182, 73, 45, 'ব্রিটেন', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(183, 73, 46, '৮০ বছর', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(184, 73, 47, 'সুপ্রীম কোর্ট।', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(185, 73, 48, 'রাষ্ট্রপতি', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(186, 73, 49, 'রূপতত্ত্ব ', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(187, 73, 50, 'ডেনমার্ক', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(188, 73, 51, 'Ctrl + B', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(189, 73, 52, 'adjective', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(190, 73, 53, 'ফ্রিডম পার্টি', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}'),
	(191, 74, 44, '১০', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(192, 74, 45, 'ব্রিটেন', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(193, 74, 46, '৮০ বছর', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(194, 74, 47, 'সুপ্রীম কোর্ট।', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(195, 74, 48, 'রাষ্ট্রপতি', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(196, 74, 49, 'রূপতত্ত্ব ', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(197, 74, 50, 'ডেনমার্ক', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(198, 74, 51, 'Ctrl + B', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(199, 74, 52, 'adjective', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(200, 74, 53, 'ফ্রিডম পার্টি', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}'),
	(201, 75, 44, '১০', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(202, 75, 45, 'ব্রিটেন', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(203, 75, 46, '৮০ বছর', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(204, 75, 47, 'সুপ্রীম কোর্ট।', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(205, 75, 48, 'রাষ্ট্রপতি', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(206, 75, 49, 'কোনটিই নয়', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(207, 75, 50, 'চিলি', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(208, 75, 51, 'Ctrl + B', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(209, 75, 52, 'adjective', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(210, 75, 53, 'আফ্রিকান ন্যাশনাল কংগ্রেস', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}'),
	(211, 76, 11, 'The range of its distinct counting digits', 'The number of its distinct counting digits', '{"option_label":{"1":"(B)","2":"(C)","3":"(D)","0":"(A)"},"right_answer":"The number of its distinct counting digits","options":{"1":"The number of its distinct counting digits","2":"The range of its distinct counting number","3":"None of above","0":"The range of its distinct counting digits"}}'),
	(212, 76, 12, 'Interface', 'Interface', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"Interface","options":["Class","Programming Language","Interface","Frame"]}'),
	(213, 76, 13, '', 'Base is small', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"Base is small","options":["It is not used as usual","Base is small","Base is big","none of above"]}'),
	(214, 76, 14, '1', '1', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"1","options":["11","00","110","1"]}'),
	(215, 77, 44, '', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(216, 77, 45, '', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(217, 77, 46, '', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(218, 77, 47, '', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(219, 77, 48, '', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(220, 77, 49, '', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(221, 77, 50, '', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(222, 77, 51, '', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(223, 77, 52, '', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(224, 77, 53, '', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}'),
	(225, 78, 44, '', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(226, 78, 45, '', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(227, 78, 46, '', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(228, 78, 47, '', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(229, 78, 48, '', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(230, 78, 49, '', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(231, 78, 50, '', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(232, 78, 51, '', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(233, 78, 52, '', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(234, 78, 53, '', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}'),
	(235, 79, 44, '১০', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(236, 79, 45, 'ভুটান', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(237, 79, 46, '৬০ বছর', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(238, 79, 47, 'জজ কোর্ট', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(239, 79, 48, '', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(240, 79, 49, '', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(241, 79, 50, 'ডেনমার্ক', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(242, 79, 51, 'Ctrl + B', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(243, 79, 52, 'adjective', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(244, 79, 53, '', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}'),
	(245, 80, 44, '১০', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(246, 80, 45, 'ব্রিটেন', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(247, 80, 46, '৮০ বছর', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(248, 80, 47, 'সুপ্রীম কোর্ট।', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(249, 80, 48, 'রাষ্ট্রপতি', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(250, 80, 49, 'রূপতত্ত্ব ', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(251, 80, 50, 'ডেনমার্ক', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(252, 80, 51, 'Ctrl + B', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(253, 80, 52, 'adjective', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(254, 80, 53, 'আফ্রিকান ন্যাশনাল কংগ্রেস', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}'),
	(255, 81, 44, '৮', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(256, 81, 45, 'ব্রিটেন', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(257, 81, 46, '৯০ বছর', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(258, 81, 47, 'সুপ্রীম কোর্ট।', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(259, 81, 48, 'প্রধান মন্ত্রী', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(260, 81, 49, 'রূপতত্ত্ব ', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(261, 81, 50, 'ডেনমার্ক', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(262, 81, 51, 'Ctrl + B', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(263, 81, 52, 'adjective', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(264, 81, 53, 'আফ্রিকান ন্যাশনাল কংগ্রেস', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}'),
	(265, 82, 44, '৯', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(266, 82, 45, 'ব্রিটেন', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(267, 82, 46, '৯০ বছর', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(268, 82, 47, 'সুপ্রীম কোর্ট।', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(269, 82, 48, 'প্রধান মন্ত্রী', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(270, 82, 49, 'রূপতত্ত্ব ', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(271, 82, 50, 'ইউ এস এ', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(272, 82, 51, 'Ctrl + B', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(273, 82, 52, 'adjective', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(274, 82, 53, 'আফ্রিকান ন্যাশনাল কংগ্রেস', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}'),
	(275, 83, 44, '৮', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(276, 83, 45, 'ব্রিটেন', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(277, 83, 46, '৮০ বছর', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(278, 83, 47, 'সুপ্রীম কোর্ট।', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(279, 83, 48, 'রাষ্ট্রপতি', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(280, 83, 49, 'বাক্যতত্ত্ব', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(281, 83, 50, 'ডেনমার্ক', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(282, 83, 51, 'Crtl + Alt  + B', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(283, 83, 52, 'adjective', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(284, 83, 53, 'আফ্রিকান ন্যাশনাল কংগ্রেস', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}'),
	(285, 84, 44, '১০', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(286, 84, 45, 'ব্রিটেন', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(287, 84, 46, '৮০ বছর', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(288, 84, 47, 'সুপ্রীম কোর্ট।', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(289, 84, 48, 'রাষ্ট্রপতি', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(290, 84, 49, 'রূপতত্ত্ব ', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(291, 84, 50, 'ডেনমার্ক', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(292, 84, 51, 'Ctrl + B', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(293, 84, 52, 'adjective', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(294, 84, 53, 'আফ্রিকান ন্যাশনাল কংগ্রেস', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}'),
	(295, 85, 44, '১০', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(296, 85, 45, 'ব্রিটেন', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(297, 85, 46, '৮০ বছর', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(298, 85, 47, 'সুপ্রীম কোর্ট।', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(299, 85, 48, 'রাষ্ট্রপতি', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(300, 85, 49, 'রূপতত্ত্ব ', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(301, 85, 50, 'ডেনমার্ক', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(302, 85, 51, 'Ctrl + B', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(303, 85, 52, 'adjective', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(304, 85, 53, 'আফ্রিকান ন্যাশনাল কংগ্রেস', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}'),
	(305, 86, 44, '১০', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(306, 86, 45, 'ব্রিটেন', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(307, 86, 46, '৮০ বছর', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(308, 86, 47, 'সুপ্রীম কোর্ট।', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(309, 86, 48, 'রাষ্ট্রপতি', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(310, 86, 49, 'রূপতত্ত্ব ', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(311, 86, 50, 'ডেনমার্ক', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(312, 86, 51, 'Ctrl + B', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(313, 86, 52, 'adjective', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(314, 86, 53, 'আফ্রিকান ন্যাশনাল কংগ্রেস', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}'),
	(315, 87, 11, 'The number of its distinct counting digits', 'The number of its distinct counting digits', '{"option_label":{"1":"(B)","2":"(C)","3":"(D)","0":"(A)"},"right_answer":"The number of its distinct counting digits","options":{"1":"The number of its distinct counting digits","2":"The range of its distinct counting number","3":"None of above","0":"The range of its distinct counting digits"}}'),
	(316, 87, 12, 'Interface', 'Interface', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"Interface","options":["Class","Programming Language","Interface","Frame"]}'),
	(317, 87, 13, 'Base is small', 'Base is small', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"Base is small","options":["It is not used as usual","Base is small","Base is big","none of above"]}'),
	(318, 87, 14, '1', '1', '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"1","options":["11","00","110","1"]}'),
	(319, 88, 4, 'LSI and VLSI', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(320, 88, 5, 'Charls Babez', 'Charls Babez', '{"option_label":["A","B","C","D"],"right_answer":"Charls Babez","options":["Wiliam English","Steve Jobs","Charls Babez","Newton"]}'),
	(321, 89, 4, 'Vacuam Tubes', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(322, 89, 5, 'Charls Babez', 'Charls Babez', '{"option_label":["A","B","C","D"],"right_answer":"Charls Babez","options":["Wiliam English","Steve Jobs","Charls Babez","Newton"]}'),
	(323, 90, 4, 'Vacuam Tubes', 'Vacuam Tubes', '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}'),
	(324, 90, 5, 'Charls Babez', 'Charls Babez', '{"option_label":["A","B","C","D"],"right_answer":"Charls Babez","options":["Wiliam English","Steve Jobs","Charls Babez","Newton"]}'),
	(325, 91, 44, '১০', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(326, 91, 45, 'ব্রিটেন', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(327, 91, 46, '৮০ বছর', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(328, 91, 47, 'সুপ্রীম কোর্ট।', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(329, 91, 48, 'রাষ্ট্রপতি', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(330, 91, 49, 'অর্থতত্ত্ব', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(331, 91, 50, 'ডেনমার্ক', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(332, 91, 51, 'Ctrl + B', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(333, 91, 52, 'adjective', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(334, 91, 53, 'আফ্রিকান ন্যাশনাল কংগ্রেস', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}'),
	(335, 92, 44, '১০', '১০', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}'),
	(336, 92, 45, 'ব্রিটেন', 'ব্রিটেন', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}'),
	(337, 92, 46, '৮০ বছর', '৮০ বছর', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}'),
	(338, 92, 47, 'সুপ্রীম কোর্ট।', 'সুপ্রীম কোর্ট।', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}'),
	(339, 92, 48, 'রাষ্ট্রপতি', 'রাষ্ট্রপতি', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}'),
	(340, 92, 49, 'রূপতত্ত্ব ', 'রূপতত্ত্ব ', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}'),
	(341, 92, 50, 'ডেনমার্ক', 'ডেনমার্ক', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}'),
	(342, 92, 51, 'Ctrl + B', 'Ctrl + B', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}'),
	(343, 92, 52, 'adjective', 'adjective', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}'),
	(344, 92, 53, 'আফ্রিকান ন্যাশনাল কংগ্রেস', 'আফ্রিকান ন্যাশনাল কংগ্রেস', '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}');
/*!40000 ALTER TABLE `given_answers` ENABLE KEYS */;

-- Dumping structure for table quiz-circle.meta
DROP TABLE IF EXISTS `meta`;
CREATE TABLE IF NOT EXISTS `meta` (
  `meta_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `meta_key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `meta_value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table quiz-circle.meta: ~7 rows (approximately)
DELETE FROM `meta`;
/*!40000 ALTER TABLE `meta` DISABLE KEYS */;
INSERT INTO `meta` (`meta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
	(13, NULL, '__main-slide-images__', '[]'),
	(14, 5, 'abcd', NULL),
	(15, 5, 'abcd', NULL),
	(16, NULL, 'home-section-category', 'home-section'),
	(17, NULL, 'latest_news_reference', 'latest_news'),
	(18, NULL, 'headline_news_reference', '96'),
	(19, NULL, 'news_head_line_id', '');
/*!40000 ALTER TABLE `meta` ENABLE KEYS */;

-- Dumping structure for table quiz-circle.navigation
DROP TABLE IF EXISTS `navigation`;
CREATE TABLE IF NOT EXISTS `navigation` (
  `nav_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `link_attrs` text COLLATE utf8_unicode_ci NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  `uri_guid` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `class_id` int(11) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`nav_id`),
  KEY `FK_navigation_class` (`class_id`),
  CONSTRAINT `FK_navigation_class` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=398 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table quiz-circle.navigation: ~9 rows (approximately)
DELETE FROM `navigation`;
/*!40000 ALTER TABLE `navigation` DISABLE KEYS */;
INSERT INTO `navigation` (`nav_id`, `title`, `description`, `link_attrs`, `level`, `uri_guid`, `class_id`, `parent`, `position`) VALUES
	(383, 'Home', '', '{\'id\':\'n\'}', 0, '{base_url}', 388, 0, 0),
	(390, 'Subject', '', '{\'id\':\'n\'}', 0, '{base_url}d', 388, 0, 1),
	(391, 'Notice', '', '{\'id\':\'n\'}', 0, '{base_url}gd', 388, 0, 2),
	(392, 'Quiz Test', '', '{\'id\':\'n\'}', 0, '{base_url}gd', 388, 0, 3),
	(393, 'Tutorials', '', '{\'id\':\'n\'}', 0, '{base_url}gd', 388, 0, 4),
	(394, 'Mathematics', '', '{\'id\':\'n\'}', 0, '{base_url}gd', 388, 390, 4),
	(395, 'Programming Languages', '', '{\'id\':\'n\'}', 0, '{base_url}gd', 388, 390, 4),
	(396, 'Computer Science', '', '{\'id\':\'n\'}', 0, '{base_url}gd', 388, 390, 4),
	(397, 'Engineering Mathematics', '', '{\'id\':\'n\'}', 0, '{base_url}gd', 388, 394, 4);
/*!40000 ALTER TABLE `navigation` ENABLE KEYS */;

-- Dumping structure for table quiz-circle.notice
DROP TABLE IF EXISTS `notice`;
CREATE TABLE IF NOT EXISTS `notice` (
  `notice_id` int(11) NOT NULL AUTO_INCREMENT,
  `notice_title` varchar(200) DEFAULT NULL,
  `notice_description` text,
  `added_date` datetime DEFAULT NULL,
  `edited_date` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `author_id` int(11) DEFAULT NULL,
  `visibility` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`notice_id`),
  KEY `FK_quiz_user` (`author_id`),
  CONSTRAINT `notice_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table quiz-circle.notice: ~0 rows (approximately)
DELETE FROM `notice`;
/*!40000 ALTER TABLE `notice` DISABLE KEYS */;
/*!40000 ALTER TABLE `notice` ENABLE KEYS */;

-- Dumping structure for table quiz-circle.participation
DROP TABLE IF EXISTS `participation`;
CREATE TABLE IF NOT EXISTS `participation` (
  `participation_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` varchar(50) DEFAULT NULL,
  `quiz_id` int(11) DEFAULT NULL,
  `participation_in` enum('quiz','tutorial') DEFAULT 'quiz',
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`participation_id`),
  KEY `FK_quiz_participations_quiz` (`quiz_id`),
  KEY `FK_quiz_participations_student` (`student_id`),
  CONSTRAINT `FK_quiz_participations_quiz` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`quiz_id`),
  CONSTRAINT `FK_quiz_participations_student` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;

-- Dumping data for table quiz-circle.participation: ~36 rows (approximately)
DELETE FROM `participation`;
/*!40000 ALTER TABLE `participation` DISABLE KEYS */;
INSERT INTO `participation` (`participation_id`, `student_id`, `quiz_id`, `participation_in`, `date`) VALUES
	(44, '1230CSE00254', 7, 'quiz', '2017-12-12 12:17:19'),
	(45, '1230CSE00254', 5, 'quiz', '2017-12-12 12:20:29'),
	(46, '1230CSE00254', 5, 'quiz', '2017-12-12 12:21:54'),
	(47, '1230CSE00254', 5, 'quiz', '2017-12-12 12:22:58'),
	(48, '1230CSE00254', 5, 'quiz', '2017-12-12 12:24:26'),
	(49, '1230CSE00254', 3, 'quiz', '2017-12-12 12:27:19'),
	(50, '1230CSE00254', 5, 'quiz', '2017-12-12 12:28:24'),
	(51, '1230CSE00254', 3, 'quiz', '2017-12-12 12:30:03'),
	(52, '1230CSE00254', 5, 'quiz', '2017-12-12 12:30:28'),
	(53, '1230CSE00254', 3, 'quiz', '2017-12-12 12:34:45'),
	(54, '1230CSE00254', 5, 'quiz', '2017-12-12 12:45:58'),
	(55, '1230CSE00254', 5, 'quiz', '2017-12-12 12:48:43'),
	(56, '1230CSE00254', 3, 'quiz', '2017-12-12 12:50:23'),
	(57, '1230CSE00254', 5, 'quiz', '2017-12-12 12:51:57'),
	(58, '1230CSE00254', 3, 'quiz', '2017-12-12 12:54:23'),
	(59, '1230CSE00254', 5, 'quiz', '2017-12-12 12:56:40'),
	(60, '1230CSE00254', 5, 'quiz', '2017-12-12 13:00:08'),
	(61, '1230CSE00254', 3, 'quiz', '2017-12-12 13:02:53'),
	(62, '1230CSE00254', 3, 'quiz', '2017-12-12 13:07:32'),
	(63, '1230CSE22451', 5, 'quiz', '2017-12-12 18:27:01'),
	(64, '1230CSE01432', 7, 'quiz', '2017-12-13 00:38:07'),
	(65, '1744CSE00628', 7, 'quiz', '2017-12-13 01:24:40'),
	(66, '1744CSE00628', 7, 'quiz', '2017-12-13 01:25:56'),
	(67, '1732CSE00578', 7, 'quiz', '2017-12-13 01:32:54'),
	(68, '1230CSE01432', 22, 'quiz', '2017-12-13 02:01:30'),
	(69, '1744CSE00628', 22, 'quiz', '2017-12-13 02:07:49'),
	(70, '1744CSE00628', 22, 'quiz', '2017-12-13 02:11:18'),
	(71, '1732CSE00578', 22, 'quiz', '2017-12-13 02:11:59'),
	(72, '1744CSE00628', 22, 'quiz', '2017-12-13 02:12:53'),
	(73, '1744CSE00628', 22, 'quiz', '2017-12-13 02:14:07'),
	(74, '1744CSE00628', 22, 'quiz', '2017-12-13 02:15:03'),
	(75, '1230CSE01432', 22, 'quiz', '2017-12-13 02:47:56'),
	(76, '1230CSE01432', 7, 'quiz', '2017-12-13 03:16:08'),
	(77, '1230CSE01432', 22, 'quiz', '2017-12-13 03:16:38'),
	(78, '1230CSE01432', 22, 'quiz', '2017-12-13 03:16:57'),
	(79, '1230CSE01432', 22, 'quiz', '2017-12-13 03:19:01'),
	(80, '1230CSE01432', 22, 'quiz', '2017-12-13 03:20:08'),
	(81, '1640CSE00523', 22, 'quiz', '2017-12-14 14:22:48'),
	(82, '1640CSE00523', 22, 'quiz', '2017-12-14 14:24:15'),
	(83, '1640CSE00536', 22, 'quiz', '2017-12-14 19:54:45'),
	(84, '1640CSE00536', 22, 'quiz', '2017-12-14 19:58:55'),
	(85, '1640CSE00536', 22, 'quiz', '2017-12-14 20:10:00'),
	(86, '1230CSE00254', 22, 'quiz', '2017-12-15 01:31:01'),
	(87, '1230CSE00254', 7, 'quiz', '2017-12-15 01:59:17'),
	(88, '1230CSE00254', 5, 'quiz', '2017-12-15 02:00:15'),
	(89, '1230CSE00254', 5, 'quiz', '2017-12-15 02:02:50'),
	(90, '1230CSE00254', 5, 'quiz', '2017-12-15 02:14:54'),
	(91, '1744CSE00628', 22, 'quiz', '2017-12-15 02:21:07'),
	(92, '1744CSE00628', 22, 'quiz', '2017-12-15 02:23:46');
/*!40000 ALTER TABLE `participation` ENABLE KEYS */;

-- Dumping structure for table quiz-circle.questions
DROP TABLE IF EXISTS `questions`;
CREATE TABLE IF NOT EXISTS `questions` (
  `quest_id` int(11) NOT NULL AUTO_INCREMENT,
  `quest_title` text,
  `answer` text,
  `description` text,
  `added_date` datetime DEFAULT NULL,
  `edited_date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `visibility` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`quest_id`),
  KEY `FK_questions_user` (`user_id`),
  CONSTRAINT `FK_questions_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=utf8;

-- Dumping data for table quiz-circle.questions: ~31 rows (approximately)
DELETE FROM `questions`;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` (`quest_id`, `quest_title`, `answer`, `description`, `added_date`, `edited_date`, `user_id`, `status`, `visibility`) VALUES
	(92, 'What is the binary of (BC8)<sub>16</sub>', '1101 1100 0100', NULL, '2017-11-26 18:49:32', '2017-11-26 18:49:46', 1, 1, 1),
	(93, 'What is RAM', 'Random Access Memory', NULL, '2017-11-26 18:52:22', '2017-11-26 19:01:32', 1, 1, 1),
	(94, 'Whis is the output device?', 'Monitor', NULL, '2017-11-27 14:36:16', '2017-11-27 14:36:16', 1, 1, 1),
	(95, 'Which is true', '', NULL, '2017-12-03 23:01:02', '2017-12-03 23:01:02', 1, 1, 1),
	(96, 'What is the main component of first generation com', 'vacuam tubes', NULL, '2017-12-03 23:31:16', '2017-12-03 23:31:16', 1, 1, 1),
	(97, 'What is the main component of first generation com', 'vacuam tubes', NULL, '2017-12-03 23:38:25', '2017-12-03 23:38:25', 1, 1, 1),
	(98, 'Who is the inventor of Computer?', 'Charls Babez', NULL, '2017-12-03 23:38:25', '2017-12-03 23:38:25', 1, 1, 1),
	(99, 'ddsa', 'c', NULL, '2017-12-04 00:25:44', '2017-12-04 00:25:44', 1, 1, 1),
	(100, 'The brain of any computer system is', 'CPU', NULL, '2017-12-05 10:41:20', '2017-12-14 15:00:14', 1, 1, 1),
	(101, 'The radian of a number system Equals?', 'The number of its distinct counting digits', NULL, '2017-12-05 10:41:20', '2017-12-05 10:41:20', 1, 1, 1),
	(102, 'A common boundary between two systems is called', 'Interface', NULL, '2017-12-05 10:41:20', '2017-12-05 10:41:20', 1, 1, 1),
	(103, 'Binary numbers need more places for counting becau', 'Base is small', NULL, '2017-12-05 10:41:20', '2017-12-05 10:41:20', 1, 1, 1),
	(104, 'Which of the following is the 1\'s complement of 10', '1', NULL, '2017-12-05 10:41:20', '2017-12-05 10:41:20', 1, 1, 1),
	(105, 'ERT', 'd', NULL, '2017-12-05 17:00:25', '2017-12-05 17:00:25', 1, 0, 1),
	(106, 'Ert WQS', 'RE', NULL, '2017-12-05 17:00:25', '2017-12-05 17:00:25', 1, 0, 1),
	(107, 'ERT', 'd', NULL, '2017-12-05 17:02:39', '2017-12-05 17:02:39', 1, 0, 1),
	(108, 'Ert WQS', 'RE', NULL, '2017-12-05 17:02:39', '2017-12-05 17:02:39', 1, 0, 1),
	(109, 'ERT', 'd', NULL, '2017-12-05 17:03:25', '2017-12-05 17:03:25', 1, 0, 1),
	(110, 'Ert WQS', 'RE', NULL, '2017-12-05 17:03:25', '2017-12-05 17:03:25', 1, 0, 1),
	(111, 'ERT', 'd', NULL, '2017-12-05 17:03:51', '2017-12-05 17:03:51', 1, 1, 1),
	(112, 'Ert WQS', 'RE', NULL, '2017-12-05 17:03:51', '2017-12-05 17:03:51', 1, 1, 1),
	(113, 'আমার সোনার বাংলা\' রবীন্দ্রসংগীতের প্রথম কত পঙক্তি ', '১০', NULL, '2017-12-13 02:00:07', '2017-12-13 02:00:07', 1, 1, 1),
	(114, 'বিবিসি কোন দেশের সংবাদ সংস্থা?', 'ব্রিটেন', NULL, '2017-12-13 02:00:07', '2017-12-13 02:00:07', 1, 1, 1),
	(115, 'রবীন্দ্রনাথ ঠাকুর কত বছর বয়সে মারা যান?', '৮০ বছর', NULL, '2017-12-13 02:00:07', '2017-12-13 02:00:07', 1, 1, 1),
	(116, 'বাংলাদেশের সর্বোচ্চ আদালত কোনটি', 'সুপ্রীম কোর্ট।', NULL, '2017-12-13 02:00:08', '2017-12-13 02:00:08', 1, 1, 1),
	(117, 'কার উপর আদালতের কোন এখতিয়ার নেই?', 'রাষ্ট্রপতি', NULL, '2017-12-13 02:00:08', '2017-12-13 02:00:08', 1, 1, 1),
	(118, 'প্রকৃতি ও প্রত্যয় বাংলা ব্যাকরণের কোন অংশের আলোচ্য', 'রূপতত্ত্ব', NULL, '2017-12-13 02:00:08', '2017-12-13 02:00:08', 1, 1, 1),
	(119, 'বিশ্বে সর্বপ্রথম জাতীয় পতাকার প্রচলন করে কোন দেশ', 'ডেনমার্ক.', NULL, '2017-12-13 02:00:08', '2017-12-14 14:00:47', 1, 1, 1),
	(120, 'বোল্ড করার জন্য কোন কম্যান্ডটি ব্যাবহার হয়', 'Ctrl + B', NULL, '2017-12-13 02:00:08', '2017-12-13 02:00:08', 1, 1, 1),
	(121, 'The word \'friendly\' is a/an', 'adjective', NULL, '2017-12-13 02:00:08', '2017-12-14 15:00:56', 1, 1, 1),
	(122, 'নেলসন ম্যান্ডেলার রাজনৈতিক দলের নাম কি?', 'আফ্রিকান ন্যাশনাল কংগ্রেস', NULL, '2017-12-13 02:00:08', '2017-12-13 02:00:08', 1, 1, 1);
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;

-- Dumping structure for table quiz-circle.quest_assign
DROP TABLE IF EXISTS `quest_assign`;
CREATE TABLE IF NOT EXISTS `quest_assign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quest_id` int(11) NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `answer_options` text,
  `date` datetime DEFAULT NULL,
  `position` int(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_quest_assign_quiz` (`quiz_id`),
  KEY `FK_quest_assign_questions` (`quest_id`),
  CONSTRAINT `FK_quest_assign_questions` FOREIGN KEY (`quest_id`) REFERENCES `questions` (`quest_id`),
  CONSTRAINT `FK_quest_assign_quiz` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`quiz_id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- Dumping data for table quiz-circle.quest_assign: ~46 rows (approximately)
DELETE FROM `quest_assign`;
/*!40000 ALTER TABLE `quest_assign` DISABLE KEYS */;
INSERT INTO `quest_assign` (`id`, `quest_id`, `quiz_id`, `answer_options`, `date`, `position`) VALUES
	(1, 92, 3, '{"option_label":["A","B","C","D"],"right_answer":"110010101","options":["110111000100","110010101","100000101","110101110101"]}', '2017-12-03 23:01:02', 0),
	(2, 93, 3, '{"option_label":["A","B","C","D"],"right_answer":"র‍্যান্ডম আক্সেস মেমরি","options":["Read Only Memory","Rapid Access Memory","র‍্যান্ডম আক্সেস মেমরি","Random Address Memory"]}', '2017-12-03 23:01:02', 1),
	(3, 95, 3, '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"CPU is the fasted memory","options":["RAM is parmanent","HDD Memory is temporery","CPU is the fasted memory","Mouse is a Output Device"]}', '2017-12-03 23:01:02', 2),
	(4, 97, 5, '{"option_label":["A","B","C","D"],"right_answer":"Vacuam Tubes","options":["Transistor","Integreted Circuit","Vacuam Tubes","LSI and VLSI"]}', '2017-12-03 23:38:26', 0),
	(5, 98, 5, '{"option_label":["A","B","C","D"],"right_answer":"Charls Babez","options":["Wiliam English","Steve Jobs","Charls Babez","Newton"]}', '2017-12-03 23:38:26', 1),
	(6, 93, 6, '{"option_label":["A","B","C","D"],"right_answer":"cc","options":["dd","ss","cc","ss"]}', '2017-12-04 00:25:44', 0),
	(7, 94, 6, '{"option_label":["A","B","C","D"],"right_answer":"cd","options":["dc","cd","s","sd"]}', '2017-12-04 00:25:44', 1),
	(8, 97, 6, '{"option_label":["A","B","C","D"],"right_answer":"sdf","options":["sd","sdf","acsa","dcf"]}', '2017-12-04 00:25:44', 2),
	(9, 99, 6, '{"option_label":["1","2","3","4"],"right_answer":"c","options":["r","c","ssc","s"]}', '2017-12-04 00:25:44', 3),
	(11, 101, 7, '{"option_label":{"1":"(B)","2":"(C)","3":"(D)","0":"(A)"},"right_answer":"The number of its distinct counting digits","options":{"1":"The number of its distinct counting digits","2":"The range of its distinct counting number","3":"None of above","0":"The range of its distinct counting digits"}}', '2017-12-05 10:41:20', 0),
	(12, 102, 7, '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"Interface","options":["Class","Programming Language","Interface","Frame"]}', '2017-12-05 10:41:20', 1),
	(13, 103, 7, '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"Base is small","options":["It is not used as usual","Base is small","Base is big","none of above"]}', '2017-12-05 10:41:20', 2),
	(14, 104, 7, '{"option_label":["(A)","(B)","(C)","(D)"],"right_answer":"1","options":["11","00","110","1"]}', '2017-12-05 10:41:20', 3),
	(19, 92, 11, '{"option_label":["A","B","C","D"],"right_answer":"","options":["","","",""]}', '2017-12-05 16:54:27', 0),
	(20, 93, 11, '{"option_label":["A","B","C","D"],"right_answer":"","options":["","","",""]}', '2017-12-05 16:54:27', 1),
	(22, 92, 15, '{"option_label":["A","B","C","D"],"right_answer":"","options":["","","",""]}', '2017-12-05 16:59:14', 0),
	(23, 93, 15, '{"option_label":["A","B","C","D"],"right_answer":"","options":["","","",""]}', '2017-12-05 16:59:14', 1),
	(24, 92, 16, '{"option_label":["A","B","C","D"],"right_answer":"","options":["","","",""]}', '2017-12-05 17:00:25', 0),
	(25, 93, 16, '{"option_label":["A","B","C","D"],"right_answer":"RAM","options":["RAM","DD","cc","d"]}', '2017-12-05 17:00:25', 1),
	(26, 105, 16, '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"dc","options":["dc","dt","d","cd"]}', '2017-12-05 17:00:25', 2),
	(27, 106, 16, '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"FE","options":["FE","RE","d","cd"]}', '2017-12-05 17:00:25', 3),
	(28, 92, 17, '{"option_label":["A","B","C","D"],"right_answer":"","options":["","","",""]}', '2017-12-05 17:02:39', 0),
	(29, 93, 17, '{"option_label":["A","B","C","D"],"right_answer":"RAM","options":["RAM","DD","cc","d"]}', '2017-12-05 17:02:39', 1),
	(30, 107, 17, '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"dc","options":["dc","dt","d","cd"]}', '2017-12-05 17:02:39', 2),
	(31, 108, 17, '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"FE","options":["FE","RE","d","cd"]}', '2017-12-05 17:02:39', 3),
	(32, 92, 18, '{"option_label":["A","B","C","D"],"right_answer":"","options":["","","",""]}', '2017-12-05 17:03:26', 0),
	(33, 93, 18, '{"option_label":["A","B","C","D"],"right_answer":"RAM","options":["RAM","DD","cc","d"]}', '2017-12-05 17:03:26', 1),
	(34, 109, 18, '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"dc","options":["dc","dt","d","cd"]}', '2017-12-05 17:03:26', 2),
	(35, 110, 18, '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"FE","options":["FE","RE","d","cd"]}', '2017-12-05 17:03:26', 3),
	(36, 92, 19, '{"option_label":["A","B","C","D"],"right_answer":"","options":["","","",""]}', '2017-12-05 17:03:51', 0),
	(37, 93, 19, '{"option_label":["A","B","C","D"],"right_answer":"RAM","options":["RAM","DD","cc","d"]}', '2017-12-05 17:03:51', 1),
	(38, 111, 19, '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"dc","options":["dc","dt","d","cd"]}', '2017-12-05 17:03:51', 2),
	(39, 112, 19, '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"FE","options":["FE","RE","d","cd"]}', '2017-12-05 17:03:52', 3),
	(40, 92, 20, '{"option_label":["A","B","C","D"],"right_answer":"101000101010","options":["101000101010","110011010010","110000010101","1101 1100 0100"]}', '2017-12-05 18:00:00', 2),
	(42, 104, 20, '{"option_label":["A","B","C","D"],"right_answer":"1","options":["022","1","10","1010"]}', '2017-12-05 18:12:26', 0),
	(43, 95, 20, '{"option_label":["B","C","D","A"],"right_answer":"if A+B =5 and A-B=1 then A=2 and B =3","options":["if A+B =5 and A-B=1 then A=2 and B =3","if A+B =5 and A-B=1 then A=4 and B =1","if A+B =5 and A-B=1 then A=2 and B =2","if A+B =5 and A-B=1 then A=3 and B =2"]}', '2017-12-05 18:18:24', 1),
	(44, 113, 22, '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"১০","options":["৮","১০","১২","৯"]}', '2017-12-13 02:00:09', 0),
	(45, 114, 22, '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ব্রিটেন","options":["আমেরিকা","ভুটান","ব্রিটেন","বাহরাইন"]}', '2017-12-13 02:00:09', 1),
	(46, 115, 22, '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"৮০ বছর","options":["৮০ বছর","৯০ বছর","৬০ বছর","১০০ বছর"]}', '2017-12-13 02:00:09', 2),
	(47, 116, 22, '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"সুপ্রীম কোর্ট।","options":["প্রধান মন্ত্রীর কার্যালয়","জেলা দায়রা জজ","সুপ্রীম কোর্ট।","জজ কোর্ট"]}', '2017-12-13 02:00:09', 3),
	(48, 117, 22, '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রাষ্ট্রপতি","options":["স্পিকার","রাষ্ট্রপতি","প্রধান মন্ত্রী","সচিব"]}', '2017-12-13 02:00:09', 4),
	(49, 118, 22, '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"রূপতত্ত্ব ","options":["বাক্যতত্ত্ব","রূপতত্ত্ব ","অর্থতত্ত্ব","কোনটিই নয়"]}', '2017-12-13 02:00:09', 5),
	(50, 119, 22, '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"ডেনমার্ক","options":["ব্রাজিল","ডেনমার্ক","ইউ এস এ","চিলি"]}', '2017-12-13 02:00:09', 6),
	(51, 120, 22, '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"Ctrl + B","options":["Crtl + Shift  + B","Ctrl + B","Crtl + Alt  + B","Ctrl + V"]}', '2017-12-13 02:00:09', 7),
	(52, 121, 22, '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"adjective","options":["adverb","pronoun","adjective","noun "]}', '2017-12-13 02:00:09', 8),
	(53, 122, 22, '{"option_label":["(1)","(2)","(3)","(4)"],"right_answer":"আফ্রিকান ন্যাশনাল কংগ্রেস","options":["আফ্রিকান সোস্যালিস্ট পার্টি","আফ্রিকান ন্যাশনাল কংগ্রেস","ন্যাশনালিস্ট পার্টি","ফ্রিডম পার্টি"]}', '2017-12-13 02:00:09', 9);
/*!40000 ALTER TABLE `quest_assign` ENABLE KEYS */;

-- Dumping structure for table quiz-circle.quiz
DROP TABLE IF EXISTS `quiz`;
CREATE TABLE IF NOT EXISTS `quiz` (
  `quiz_id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_title` varchar(200) DEFAULT NULL,
  `quiz_description` text,
  `full_marks` int(3) DEFAULT NULL,
  `added_date` datetime DEFAULT NULL,
  `edited_date` datetime DEFAULT NULL,
  `quiz_type` varchar(50) DEFAULT 'quiz',
  `status` tinyint(1) DEFAULT '1',
  `user_id` int(11) DEFAULT NULL,
  `visibility` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`quiz_id`),
  KEY `FK_quiz_user` (`user_id`),
  CONSTRAINT `FK_quiz_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- Dumping data for table quiz-circle.quiz: ~13 rows (approximately)
DELETE FROM `quiz`;
/*!40000 ALTER TABLE `quiz` DISABLE KEYS */;
INSERT INTO `quiz` (`quiz_id`, `quiz_title`, `quiz_description`, `full_marks`, `added_date`, `edited_date`, `quiz_type`, `status`, `user_id`, `visibility`) VALUES
	(1, 'Introduction to Algorithm', NULL, 50, '2017-12-03 15:28:52', '2017-12-03 15:28:52', 'quiz', 1, 1, 1),
	(2, 'Introduction to computer', NULL, 50, '2017-12-03 15:29:39', '2017-12-03 15:29:39', 'quiz', 1, 1, 1),
	(3, 'Introduction to Computer Fundementals', '', 45, '2017-12-03 23:01:02', '2017-12-12 19:49:50', 'quiz', 1, 1, 1),
	(5, 'Numerical method', '', 25, '2017-12-03 23:38:25', '2017-12-12 12:59:38', 'quiz', 1, 1, 1),
	(6, 'abcd', NULL, 43, '2017-12-04 00:25:44', '2017-12-04 23:20:03', 'quiz', 1, 1, 1),
	(7, 'Introduction to Computer Fundemental', '', 45, '2017-12-05 10:41:19', '2017-12-05 16:42:56', 'quiz', 1, 1, 1),
	(11, 'gdfg', '', 43, '2017-12-05 16:54:27', '2017-12-05 16:54:27', 'tutorial', 0, 1, 1),
	(15, 'gdfg', '', 43, '2017-12-05 16:59:14', '2017-12-05 16:59:14', 'tutorial', 1, 1, 1),
	(16, 'gdfg', '', 43, '2017-12-05 17:00:25', '2017-12-05 17:00:25', 'quiz', 0, 1, 1),
	(17, 'gdfg', '', 43, '2017-12-05 17:02:39', '2017-12-05 17:02:39', NULL, 0, 1, 1),
	(18, 'gdfg', '', 43, '2017-12-05 17:03:25', '2017-12-05 17:03:25', NULL, 0, 1, 1),
	(19, 'gdfg', '', 43, '2017-12-05 17:03:51', '2017-12-05 17:03:51', 'tutorial', 0, 1, 1),
	(20, ' dfdsfg ', 'Gooo.... OOP', 65, '2017-12-05 18:00:00', '2017-12-05 20:10:24', 'quiz', 0, 1, 1),
	(22, 'সাধারন জ্ঞান বিষয়ক কুইজ', '', 20, '2017-12-13 02:00:07', '2017-12-13 02:00:07', 'quiz', 1, 1, 1);
/*!40000 ALTER TABLE `quiz` ENABLE KEYS */;

-- Dumping structure for table quiz-circle.quiz_participations
DROP TABLE IF EXISTS `quiz_participations`;
CREATE TABLE IF NOT EXISTS `quiz_participations` (
  `participation_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `quiz_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table quiz-circle.quiz_participations: ~0 rows (approximately)
DELETE FROM `quiz_participations`;
/*!40000 ALTER TABLE `quiz_participations` DISABLE KEYS */;
/*!40000 ALTER TABLE `quiz_participations` ENABLE KEYS */;

-- Dumping structure for table quiz-circle.student
DROP TABLE IF EXISTS `student`;
CREATE TABLE IF NOT EXISTS `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` varchar(50) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `total_score_obtained` int(8) NOT NULL DEFAULT '0',
  `total_exam_participated` int(6) NOT NULL DEFAULT '0',
  `user_type_hash` varchar(32) CHARACTER SET ascii DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_type_hash` (`user_type_hash`),
  UNIQUE KEY `student_id` (`student_id`),
  KEY `FK_student_user` (`user_id`),
  CONSTRAINT `FK_student_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `FK_student_user_2` FOREIGN KEY (`user_type_hash`) REFERENCES `user` (`user_type_hash`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- Dumping data for table quiz-circle.student: ~9 rows (approximately)
DELETE FROM `student`;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` (`id`, `student_id`, `user_id`, `date_of_birth`, `total_score_obtained`, `total_exam_participated`, `user_type_hash`) VALUES
	(1, '1330CSE00290', 2, '1992-10-21', 0, 0, '3cd33cd2ae8930ecabb4478aed33671a'),
	(6, '1230CSE22451', 115, '1989-10-11', 0, 0, '7688d1a2248cf27c6e3d506066c3f3ec'),
	(7, '1230CSE22052', 116, '1989-10-11', 0, 0, '50855f11aafbb9d46ce85c6b40768819'),
	(8, '1254LLB44882', 117, '1989-10-11', 0, 0, 'd997537e963184ea73f400934d3a1d44'),
	(9, '1230DCS21051', 118, '1989-10-11', 0, 0, '1a981e7c5a754248c981cd743ec730ed'),
	(12, '1230CSE00254', 121, '1989-10-11', 0, 0, '3cd33cd2ae8930ecabb4478aed33671b'),
	(14, '1230CSE01432', 125, NULL, 0, 0, 'b192351926111c0805a66b4fd3961146'),
	(15, '1744CSE00628', 126, NULL, 0, 0, '0db38e5f0f6cf7bd0a8a87e6ce13ff19'),
	(16, '1732CSE00578', 127, NULL, 0, 0, 'e567ca4d832a89adff7707f30af48c35'),
	(17, '1640CSE00523', 128, NULL, 0, 0, '4bf10ebbd3773dcb5d8e36c12d6b89df'),
	(18, '1640CSE00536', 129, NULL, 0, 0, 'c414edd46a07b6f773bb1d3c40125512');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;

-- Dumping structure for table quiz-circle.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_pic` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `activision_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `user_from` enum('','google','facebook') COLLATE utf8_unicode_ci DEFAULT '',
  `social_user_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `profile_image_link` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `gender` enum('male','female') COLLATE utf8_unicode_ci DEFAULT 'male',
  `user_type_hash` varchar(32) CHARACTER SET ascii DEFAULT NULL,
  `ip_address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '2',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `lang` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `user_type_hash` (`user_type_hash`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table quiz-circle.user: ~12 rows (approximately)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `username`, `firstname`, `lastname`, `email`, `profile_pic`, `password`, `salt`, `created_date`, `modified_date`, `activision_key`, `display_name`, `user_from`, `social_user_id`, `profile_image_link`, `gender`, `user_type_hash`, `ip_address`, `active`, `status`, `lang`) VALUES
	(1, 'saif', 'Saif', 'Uddins', 'saif.bigc@gmail.com', '6bd90a698addc6096adf8012246b7421.jpg', '506ff91b984e6f2e5538993eb04cacd43fa38651e9a8485eac5ea4d712898267', 'dsxp0jvPhoBEOJZSMQ8RICFL2Z6kdv7W', '2015-12-26 21:49:18', '2017-09-28 07:17:07', '', 'Saif Uddin', '', '', '', 'male', '3cd33cd2ae8930ecabb4478aed33671a', '103.196.235.130', 1, 1, 'en'),
	(2, 'Abir', 'Abir', 'Khan', 'abir@gmail.com', '6bd90a698addc6096adf8012246b7421.jpg', '506ff91b984e6f2e5538993eb04cacd43fa38651e9a8485eac5ea4d712898267', 'dsxp0jvPhoBEOJZSMQ8RICFL2Z6kdv7W', '2015-12-26 21:49:18', '2017-09-28 07:17:07', '', 'Abir Khan', '', '', '', 'male', 'ba833ccae76930ecabb4478aed3cbe45', '103.196.235.130', 1, 1, 'en'),
	(115, NULL, 'Saif', 'Uddin', NULL, NULL, '', '', '2017-10-20 09:49:52', '2017-12-12 23:35:12', NULL, 'Saif Uddin', 'facebook', '10202633168947324', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/10931348_10200311974798921_564938191017442731_n.jpg?oh', 'male', '7688d1a2248cf27c6e3d506066c3f3ec', NULL, 1, 1, 'en'),
	(116, NULL, 'Saif', 'Uddin', NULL, NULL, '', '', '2017-10-20 09:49:53', '2017-12-09 00:56:46', NULL, 'Saif Uddin', 'google', '104014323314144287933', 'https://lh5.googleusercontent.com/-Tz_kxamwpxw/AAAAAAAAAAI/AAAAAAAAAEw/6q7MmPDZafc/photo.jpg', 'male', '50855f11aafbb9d46ce85c6b40768819', NULL, 2, 1, 'en'),
	(117, NULL, 'Raju', 'Sheikh', NULL, NULL, '', '', '2017-11-07 15:53:30', '2017-11-14 16:18:39', NULL, 'Raju Sheikh', 'facebook', '829609740580882', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/23172528_828891337319389_533284815427763719_n.jpg?oh=6', 'male', 'd997537e963184ea73f400934d3a1d44', NULL, 1, 1, 'en'),
	(118, NULL, 'Monawar', 'Hossain', NULL, NULL, '', '', '2017-11-07 15:56:55', '2017-11-14 16:18:04', NULL, 'Monawar Hossain', 'google', '114455077764416894202', 'https://lh4.googleusercontent.com/-7ZGfSF_Y5k4/AAAAAAAAAAI/AAAAAAAAABs/EdburrlJSXM/photo.jpg', '', '1a981e7c5a754248c981cd743ec730ed', NULL, 2, 1, 'en'),
	(121, 'saif.u', 'Saif', 'Uddins', 'saif.b@gmail.com', '6bd90a698addc6096adf8012246b7421.jpg', '506ff91b984e6f2e5538993eb04cacd43fa38651e9a8485eac5ea4d712898267', 'dsxp0jvPhoBEOJZSMQ8RICFL2Z6kdv7W', '2015-12-26 21:49:18', '2017-09-28 07:17:07', '', 'Saif Khan', '', '', '', 'male', '3cd33cd2ae8930ecabb4478aed33671b', '103.196.235.130', 1, 1, 'en'),
	(122, 'Bashar', 'Bashar', 'Ahmed', 'bashar@gmail.com', '', '506ff91b984e6f2e5538993eb04cacd43fa38651e9a8485eac5ea4d712898267', 'dsxp0jvPhoBEOJZSMQ8RICFL2Z6kdv7W', '2017-11-12 15:46:18', '2017-11-12 15:46:18', '', 'Bashar Ahmed', '', '', '', 'male', 'ba833ccae76930ecabb4478aed3cb452', '103.196.235.130', 1, 1, 'en'),
	(125, 'abul.kashem', 'Abul', 'Kashem', 'abul.kashem@gmail.com', NULL, 'ae72210486a79d104ebd7f0db3137874a998e1060597e13eb3a916dc185d8fc8', 'JSxT9vxkxwrs38Vtf5Benp1yJkBRcHP7', '2017-12-13 00:21:36', '2017-12-13 00:21:36', NULL, 'Abul Kashem', '', '', '', 'male', 'b192351926111c0805a66b4fd3961146', '::1', 2, 1, 'en'),
	(126, 'Abdullah', 'Abdullah ', 'Ibna Kmal', 'abdullahalsagart@gmail.com', NULL, 'a7b9a98848cb084603ba62ba9dc43c742fe3a1e43f931267f3670097bcf5b269', 'Fnpsx9HasqKCAWo5cZpgd3jmH0mfnB93', '2017-12-13 01:22:17', '2017-12-13 01:22:17', NULL, 'Abdullah  Ibna Kmal', '', '', '', 'male', '0db38e5f0f6cf7bd0a8a87e6ce13ff19', '192.168.0.190', 2, 1, 'en'),
	(127, 'bekash', 'Aktaruzzaman', 'bekash', 'azbk637@hotmail.com', NULL, '5d35e8473beb33239c61329e434c0a8e36437c4a5d8782c1e9207cbc071fc042', 'NWpdpEhG7pLW6sMjQeJ5OAkUkRfGx4Gy', '2017-12-13 01:30:20', '2017-12-13 01:30:20', NULL, 'Aktaruzzaman bekash', '', '', '', 'male', 'e567ca4d832a89adff7707f30af48c35', '192.168.0.101', 2, 1, 'en'),
	(128, 'abmasadullah', 'A B M', 'Asadullah', 'abmasadullah@gmail.com', NULL, '819289d11cc39dbeb5d0cc6a71f921905c624b41cd261f438c0d0bae554a1d1a', '1yHRx8p5UACRfLtCzZeQl948BEQOfmOX', '2017-12-14 14:19:35', '2017-12-14 14:19:35', NULL, 'A B M Asadullah', '', '', '', 'male', '4bf10ebbd3773dcb5d8e36c12d6b89df', '192.168.0.112', 2, 1, 'en'),
	(129, 'Shamim.Reza', 'MD Shamim ', 'Reza', 'shamim01.fbd@gmail.com', NULL, '7f9cab947069534848fd6b2086322dfd6fe3e7d1ab6abcd60c3bceafd591a94a', 'Pyj3Z4sFVTWVH0fjBqHCyxsToSHaNWHL', '2017-12-14 19:48:16', '2017-12-14 19:48:16', NULL, 'MD Shamim  Reza', '', '', '', 'male', 'c414edd46a07b6f773bb1d3c40125512', '192.168.0.112', 2, 1, 'en');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table quiz-circle.user_permissions
DROP TABLE IF EXISTS `user_permissions`;
CREATE TABLE IF NOT EXISTS `user_permissions` (
  `type` enum('admin','teacher') NOT NULL,
  `permissions` text NOT NULL,
  KEY `FK_user_permissions_user_role` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table quiz-circle.user_permissions: ~2 rows (approximately)
DELETE FROM `user_permissions`;
/*!40000 ALTER TABLE `user_permissions` DISABLE KEYS */;
INSERT INTO `user_permissions` (`type`, `permissions`) VALUES
	('admin', '{\r\n    "building" : [1,2,3,4], \r\n    "building_image" : [1,2,3,4],\r\n    "user" : [1,2,3,4], \r\n    "all_user" : [2], \r\n    "floor_plan" : [1,2,3,4],\r\n    "settings" : [1,2,3,4],\r\n    "permission" : [1,3,4], \r\n    "plan_relation" :  [1,2,3,4],\r\n    "flat_information"  : [1,2,3,4],\r\n    "flat_image"  : [1,2,3,4],\r\n    "upload_image" :[1,2] ,\r\n    "coordinates" : [1,2,3,4] \r\n}'),
	('teacher', '{\r\n    "building" : [2,3], \r\n    "building_image" : [2],\r\n    "user" : [2,3,4], \r\n    "settings" : [1,2,3], \r\n    "floor_plan" : [1,2,3,4],\r\n    "plan_relation" :  [2],\r\n    "flat_information"  : [2,3],\r\n    "flat_image"  : [2,3]\r\n}');
/*!40000 ALTER TABLE `user_permissions` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
