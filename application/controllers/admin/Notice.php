<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author saifuddin
 */

require_once APPPATH . "models/functions/quiz-functions.php";

class Quiz extends Admin_controller {
    //put your code here
    private $content_type, $class_joined = false;


    public function __construct() {
        parent::__construct();
        $this->load->model( array("post", "category") );
        $this->load->library("list_config");
        $this->content_type = "quiz";
        $this->set_parent_category_options();
       
    }

    private function set_parent_category_options(){
        $data =& data();
        $data['class_list_config'] = array(
            'each_item_view'    =>  'admin/includes/each/class_item_input_options',
            'active_data'    => input_value('parent'),
        );
    }
    
    public function tree_parent_sign($num, $sign = "&mhash;"){
        if(is_int($num)) {
            for($i = 0; $i < $num; $i++){
                echo $sign;
            }
        }
    }

    public function view_all(){
        $data = & data();

        prepare_question_list( $questions );
        prepare_notice_list( $questions );

        $data['questions'] = $questions;
        $data["page_heading"] = "Questions";
        $data["page_title"] = "Questions List :: Quiz Circle Admin";

        admin_view('admin-questions');
    }




}
