<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Classes extends Admin_controller {
    

    


    public function get_list ( $type = "category", $action = "" , $class_id = ""){
        $this->load->model('category');
        $is_tree  = class_is_tree($type); 
        
        $this->category->update_class_from_input($action, $type, $class_id);
        
        //echo "Input Value: " . input_value("name");
        
        //Explain($this->category->add(array('name' => 'bangladsh') ));
        $this->category->type = $type;
        
        $this->data['is_tree'] = $is_tree;
        
        $this->data['list_config'] = array(
            'each_item_view'    =>  'admin/includes/each/class_item_edit',
            'plain_list'    => !$is_tree,
        );
        
        if($this->data['is_tree']){  
            $this->data['parent_list_config'] = array(
                'each_item_view'    =>  'admin/includes/each/class_item_input_options',
                'active_data'    => input_value('parent'),
            );
        }
        echo isset($this->data['test']) ? $this->data['test']:"";
        
        $this->data['showing'] = $type;
        
        $this->data['total'] = $this->category->total;
        //$this->data['list_config'] = $this->category->getList($list_config);
        
        
        $this->admin->view("admin-class-listing", data());
    }

    public function test_module(){
        admin_view('admin-relation-modele-test');
    }
    
}
