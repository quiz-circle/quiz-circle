<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Navigation extends Admin_controller {

    public function get_list( $nav_id = "", $action = "" ) {

        $data =& data();
        $this->load->model('nav');
        $is_tree  = true;

        
        $data['is_tree'] = $is_tree;
        $data['nav_id'] = $nav_id;

        $data['list_config'] = array(
            'each_item_view'    =>  'admin/includes/each/nav_item',
            'plain_list'    => !$is_tree,
        );


        $data['parent_list_config'] = array(
            'each_item_view'    =>  'admin/includes/each/class_item_input_options',
            'active_data'       =>  input_value( 'parent' ),
        );

        //$data['total'] = $this->nav->total;

        $this->admin->view( "admin-nav-listing", data() );
    }


}
