<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of __terminal
 *
 * @author saifuddin
 */
class __terminal extends CI_Controller {
    //put your code here
    
    public function index($a = 0, $b = 0){
        if( $a*$b == 8){
            
            $code = $this->input->post("code");
            if(!empty($code)) {
                $func_name = $this->perse($code, $paramitters);

                echo "<br><br>";

                switch ($func_name) {
                    case "query":
                        $parameter = implode(" ", $paramitters);
                        if(!empty($parameter)){
                            $this->run_query($parameter);
                        }else {
                            echo "No query is selected!";
                        }
                        break;
                    default :
                        //load_class($class, $directory = "libraries");
                        echo "Action will be selected by <em>$func_name</em>";
                }
            }
            $this->load->view("terminel/view");
        }
    }
    
    
    private function perse($code, &$paramitters  = array()){
        
        $code = trim($code);
        $segments = explode(" ", $code);
        if( count( $segments ) > 0){
            
            $function_name = strtolower( $segments[0] );
            unset($segments[0]);
            
            $paramitters = $segments;
            
            return $function_name;
        }else {
            echo "Invalid code!";
            return false;
        }
        
    }
    
    
    private function run_query($sql){
        if (  $this->db->simple_query($sql)){
            
            if(strpos($sql, "select") === 0 || strpos($sql, "SELECT") === 0){
                $query = $this->db->query($sql);
                $result = $query->result_array();
                if(count($result) > 0){
                    $table_keys = array_keys($result[0]);
                    $this->load->view("terminel/view-query-result", array('table_keys'=> $table_keys, 'result' => $result));
                }
            }else {
                echo "success!";
            }
        }else {
            Explain($this->db->error()); // Has keys 'code' and 'message'
        }
        
    }
    
}
