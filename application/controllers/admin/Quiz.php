<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author saifuddin
 */

require_once APPPATH . "models/functions/quiz-functions.php";

class Quiz extends Admin_controller {
    //put your code here
    private $content_type, $class_joined = false;


    public function __construct() {
        parent::__construct();
        $this->load->model( array("post", "category") );
        $this->load->library("list_config");
        $this->content_type = "quiz";
        $this->set_parent_category_options();
       
    }

    private function set_parent_category_options(){
        $data =& data();
        $data['class_list_config'] = array(
            'each_item_view'    =>  'admin/includes/each/class_item_input_options',
            'active_data'    => input_value('parent'),
        );
    }
    
    public function tree_parent_sign($num, $sign = "&mhash;"){
        if(is_int($num)) {
            for($i = 0; $i < $num; $i++){
                echo $sign;
            }
        }
    }

    public function view_results( $quiz_id = 0 ){
        $data =& data();
        $data['page_heading'] = "Student Perticipation";
        $data['page_title'] = "Student Perticipation "." :: Quiz Circle Admin";
        $data['student_result_list'] = participated_student_list($quiz_id);
        admin_view("admin-student-results");
    }
    
    private function view_content_update_page($content_id = null){
        $data =& data();
        $action = $this->uri->segment(3);

        
//        $input = $this->input->post();
//        unset($input['content-body']);
//        Explain($input );
        
        $this->load->model( "category" );

        $this->category->type = "quiz-category";
        //_alert($this->post->get_input("classes"));

        $data['action']  = $action;
        $data['cat_config'] = $cat_config = array( 'each_item_view' => 'admin/includes/each/class_items'  );
        $data['tag_config'] = $cat_config = array( 'each_item_view' => 'admin/includes/each/tag_items' );

        $data['page_heading'] = "Add Quiz";
        $data['page_title'] = "Add Quiz "." :: Quiz Circle Admin";

        $data['content_type'] = $this->content_type;
        $data['content_id'] = $content_id;

        $this->add_edit_post_item( $content_id );
        

        $this->admin->view( 'admin-add-content', data( ) );
    }
    
    private function add_edit_post_item( $content_id = null ){
        $action  = $this->uri->segment(3);
        $this->load->model("_quiz");

        if($action == "add"){
            $this->post->addItem($this->content_type);
        } else if($action == "edit"){

            $this->_quiz->related_question( $content_id );

            $this->_quiz->editData($content_id, $this->content_type );
        }

    }

    public function questions(){
        $data = & data(); 
        
        prepare_question_list( $questions );
        $data['questions'] = $questions;
        $data["page_heading"] = "Questions";
        $data["page_title"] = "Questions List :: Quiz Circle Admin";
        
        admin_view('admin-questions');
    }
    
    public function add($content_type = "quiz"){
        $data =& data( );
        $data["page_heading"] = "Add Quiz";
        $data["page_title"] = "Add Quiz :: Quiz Circle Admin";
        $this->content_type = $content_type;
        
        $this->view_content_update_page( );
    }
    
    public function edit( $content_id = null ){
        $this->view_content_update_page( $content_id );
    }    

    private function _serach(){
        if($search_string = $this->input->get("search")){
            $this->db->where("title LIKE", "%$search_string%");
        }
    }
    
    private function _filter(){
        
        $class_joined = false;
        
        if($search_string = $this->input->get("category")) {
            $this->_join_class_tables($class_joined);
            $this->db->where( "class.url_ref", "$search_string" );
        }    
        
        if($search_string = $this->input->get("tag")) {    
            
            $this->_join_class_tables($class_joined);
            $this->db->where( "class.url_ref", "$search_string" );
        }    
        
        if($search_string = $this->input->get("user_id")) {    
            $this->db->where( "user.user_id", "$search_string" );
        }    
    }

    
    private function _join_class_tables(&$class_joined){
        if(!$class_joined) {
            $this->db->join("class_relation", "class_relation.entity_id = contents.id");
            $this->db->join("class", "class_relation.class_id = class.class_id");
            $class_joined = true;
        }
    }
    
    private function _join_tables(){
        $this->db->join( "user", "user.user_id = contents.author_id" );
    } 
            
    public function get_list( $show = 'news-all', $limit = 10, $start = 0, $others = '' ) {
        
        $list = $this->db->select()->from("quiz")->get();

        Explain( $list->result("quiz_list") );
        
//        $showing = $show;
//        
//        $_show = explode("-", $showing);
//        $this->content_type = count($_show) == 1 ?  $showing : implode( "-", array_slice($_show, 0, count($_show) -1 ) );
//        
//        $showing = count($_show) == 1 ? "all" : $_show[ count($_show)-1 ];
//        
//        //exit("Content Type: ".$content_type. ", show:" .  $showing);
//        
//
//        $config['base_url'] = base_url("admin/posts/get_list/".$show."/".$limit); //different
//        $config['per_page'] = $limit; //like
//        $config['num_links'] = 5; //like
//        $config['uri_segment'] = 6; //like
//        
//        $this->list_config->set_pagination_html_config($config, $this->list_config->pagination_attrs());//like
//        
//        
//        
//        if(is_numeric($show)){
//            $this->content_type = "news";
//            $temp_limit = $limit; //like
//            $limit = $show; //like
//            $seg5 = $this->uri->segment(5); //like
//            
//            if( isset($seg5) && !empty($seg5)){ //like
//                $temp_start = $start; //like
//                $start = $temp_limit; //like
//                $others = $temp_start; //like
//            }    
//            
//            $config['uri_segment'] = 5; //like
//            $config['per_page'] = $limit; //like
//            $config['base_url'] = base_url("admin/posts/get_list/" .$limit); 
//        }
//
//        $this->list_config->_action_list = $this->list_config->_post_action_list( $showing, $this->content_type ); 
//        
//        $this->data['identifier_list'] = $this->list_config->_post_filter_links( $this->content_type ); 
//
//        $this->data['page_heading'] = "All " . ucfirst($this->content_type) . ''; 
//        $this->data['page_title'] = "All " . ucfirst($this->content_type) . ''. " :: Phulkuri Admin"; 
//        
//        $this->data['content_type'] = $this->content_type; //like
//        $this->data['showing'] = $showing; //like
//        $this->data['search_key'] = "search"; //like
//        
//        $list_func_data = array( ); //like
//        
//        
//        
//        
//        $act_select_name =  'action-type';
//        
//        $this->post->execute_selected_options( $this->list_config->_action_list, $act_select_name );
//        
//        $this->data['action_selected'] = $act_select_name; //like
//        
//        switch ($showing){
//            case "trash" : 
//                $trash_func_data = array();  
//                $list_func_data['order_by'] = " trashed_date DESC ";
//                $list_func_data['content_type'] = $this->content_type;
//                
//                $this->post->set_function_data( $list_func_data );
//                
//                $extra['data'] = $list_func_data;
//                $extra['func'] = function($db, $data){
//                    $this->_join_tables();
//                    $this->_filter();
//                    $this->_serach();
//                    if( isset( $data['contents.order_by'] )){
//                        $db->where( "contents.content_type" , $data['content_type']);
//                        $db->order_by($data['order_by']);
//                    }                    
//                };
//                        
//                $this->data['contents'] = $this->post->get_trash_data($extra, $limit, $start);
//                
//                break;
//            case "active" :
//            case "hidden" :
//                $list_func_data['visibility'] = $showing == 'active' ? 1:0;
//                $list_func_data['content_type'] = $this->content_type;
//                $this->post->set_function_data($list_func_data);
//                
//                $this->data['contents'] = $this->post->get_data(function($db, $data){
//                    $this->_join_tables();
//                    $this->_filter();
//                    $this->_serach();
//                    $db->where("contents.visibility", $data['visibility']);
//                    $db->where("contents.status", 1);
//                    $db->where( "contents.content_type" , $data['content_type']);
//                    
//                    if( isset( $data['order_by'] )){
//                        order_by($db, $data['order_by']);
//                    }
//                    
//                    $this->db->group_by("contents.id");
//                    
//                }, $limit, $start);
//                break;
//            default :
//                $list_func_data['content_type'] = $this->content_type;
//                $this->post->set_function_data($list_func_data);
//                $this->data['contents'] = $this->post->get_data(function($db, $data){
//                    $this->_join_tables();
//                    $this->_filter();
//                    $this->_serach();
//                    $db->where( "contents.content_type" , $data['content_type']);
//                    $db->where("contents.status", 1);
//                    if( isset( $data['order_by'] )){
//                        order_by($db, $data['order_by']);
//                    }                    
//                    $this->_serach();
//                }, $limit, $start);
//        }
//        
//        //echo $this->db->last_query();
//
//        $config['total_rows'] = $this->post->get_total_num_rows( ); //like
//        
//        $this->load->library( "pagination" ); //like
//        
//        $this->pagination->initialize($config); //like
//        
//        $this->data['pagination'] = $this->pagination->create_links( ); //like
//        
//        $this->admin->view( "admin-posts", $this->data( ) ); //different
    }
}
