<?php


/**
 * Description of Script
 *
 * @author Saif
 */
class Script extends Admin_controller{

    public function __construct(){
        parent::__construct(false);
        ob_start();
        $this->load->model(array( "meta", "authantication", "sec_view" ));
    }

    public function index(){
        echo 'Access forbidden!';
    }

    public function site( $class_name, $script_name ){
        $this->load_public_script($class_name, $script_name);
    }

    public function site_( $class_name, $script_name ){
        $this->load_public_script($class_name, $script_name, true);
    }

    private function load_public_script( $class_name, $script_name, $json = false ) {
        $class = load_class($class_name, "views/public/ajax/");
        if(method_exists($class, $script_name)){
            if($json) {
                $result = array( );
                $result['success'] = false;
                $result['is_logged_in'] = $this->_isLoggedIn;
                $class->$script_name( $result );
                $this->output->set_content_type('application/json')->set_output( json_encode($result) );
            } else {
                $class->$script_name( $this->_isLoggedIn );
            }
        } else {
            exit("The function <b><em>" .$script_name . "</em></b> in not found inside the <b><em>" . $class_name . "</b></em> Class!");
        }
    }

    public function ajax($class_name, $script_name){
        $this->load_script($class_name, $script_name);
    }

    public function json($class_name, $script_name){
        $this->load_script($class_name, $script_name, true);
    }

    private function load_script($class_name, $script_name, $json = false){

        $this->load->model("ajax/" .  $class_name);

        if(method_exists($this->$class_name, $script_name)){
            if($json){

                $result = array( 'is_logged_in' => 0);

                if( $this->_isLoggedIn ){
                    $result['is_logged_in'] = 1;
                    $this->$class_name->$script_name( $result );
                }else {
                    $result['success'] = false;
                    $result['message'] = "Cant't update data, Logged out or session expired!<br>please <a href='" . base_url("admin/user/login") ."'>login</a> first!";
                }

                $this->output->set_content_type('application/json')->set_output(json_encode($result));

            } else {
                if($this->_isLoggedIn){
                    $this->$class_name->$script_name();
                }else {
                    exit( "Not Logged In or Login session has expired!<br><h2><a href='" . base_url("admin/user/login") ."'>Login</a></h2>" );
                }
            }
        }
    }

    public function __destruct() {
        ob_end_flush();
    }
}
