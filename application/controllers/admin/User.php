<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author Saif
 */

class User extends Admin_controller {
    //put your code here
    
    public function __construct() {
        parent::__construct();
    }
    

    
    public function profile($user_id = false, $hash =""){
        $this->data['page_heading'] = "Profile";

        $this->load->model("form_hash");
        
        //Explain( $this->data() );
        
        if($this->authantication->getPermission("user", 2)) {
            
            $user_id = !$user_id ? $this->authantication->get_data('user_id'):$user_id;
            
            if($user_id && $u = $this->users->get_data($user_id)){
                
                $this->data['change_pass_btn'] = ($user_id == $this->authantication->get_data('user_id')) ? true:false;
                
                $this->data['userdata'] = $u;
                
                $this->load->view('admin/admin-user-profile', data());
            }else {
                $this->data['message'] = "User Not Exists!";
                $this->load->view('admin/show_message', data());
            }
        }
    }



    public function edit($user_id = false, $hash =""){
        $data = &data();
        $authantication = $this->authantication;
        
        $form_field = array();
        
        $user_id = $user_id === false ? $this->sdata['user_id']:$user_id;
        
        $data['page_heading'] = "Edit Profile";
        $this->load->model("form_hash");
        $data['user_id'] = $user_id;
        
        $action = "edit";
        $data['user_roles'] = $this->users->get_roles();
        
        if( $authantication->getPermission("user", 3) ){
            if($user_id && $u = $this->users->get_data($user_id)){
                
                $this->load->model("users");
                if($this->input->post()){


                    $this->load->helper(array('form', 'url'));

                    $this->form_validation->set_rules($this->validatation_config->user($action));
                    
//                
//                    //Explain($u);
                    
                    $form_field['firstname'] =  $this->input->post('firstname');
                    $form_field['lastname'] =  $this->input->post('lastname');
                    $form_field['email'] =  $this->input->post('email');
                    $form_field['display_name'] =  $this->input->post('display_name');
                    
                    $form_field['modified_date'] =  date("Y-m-d H:i:s");
                    
                    $form_field['username'] = $u['username'];
                    
                    if ($this->form_validation->run() != FALSE){
                        
                        //Explain($form_field);
                        
                        if($this->input->post('role')){
                            $form_field['role'] =  $this->input->post('role');
                        }
                        if($this->users->edit_data($user_id, $form_field)){
                            //Explain($form_field);
                            
                            
                            $data['status'] = "Updated!";
                        }else {
                            $data['status'] = "Not Updated!";
                        }
                        
                        if(isset($_FILES['profile-pic']['name']) && !empty($_FILES['profile-pic']['name'])   ){
                            if($_FILES['profile-pic']['error'] == 0 ){
                                
                                //Explain($_FILES);
                                $this->users->change_profile_pic("profile-pic", $user_id);
                            }
                        }
                    }
                } else {
                    $form_field['username'] = $u['username'];
                    $form_field['firstname'] = $u['firstname'];
                    $form_field['lastname'] = $u['lastname'];
                    $form_field['email'] = $u['email'];
                    $form_field['role'] = $authantication->getUserType();
                    $form_field['display_name'] = $u['display_name'];
                    

                    
                }            
            }
            
            $data['value'] = $form_field;
            $data['action'] = $action;
            
            //Explain($data);
            
            
            $this->load->view('admin/admin-register', data());
        }else {
            $this->admin->access_forbidden();
        }
    }
    
    public function change_password(){
        $this->data['page_heading'] = "Change Password";
        $this->load->model("form_hash");
        
        if($this->authantication->getPermission("user", 3)){
            $user_id = $this->authantication->get_data("user_id");
            $username = $this->authantication->get_data("username");
            if($user_id && $u = $this->users->get_data($user_id)){
                
                $action = "change_pass";
                $this->data['action'] = $action;
                $this->data['value'] = array();
                $this->load->helper(array('form', 'url'));
                $this->load->model("users");
                $this->data['user_roles'] = $this->users->get_roles();

                $this->form_validation->set_rules($this->validatation_config->user($action));

                $form_field = array();
                
                $old_pass =  $this->input->post('old_password');
                //Explain($u);
                if($this->input->post()){
                    
                    $form_field['password'] =  $this->input->post('password');
                    //$form_field['modified_date'] =  date("Y-m-d H:i:s");

                    
                    if ($this->form_validation->run() == FALSE){
                         $this->load->view('admin/admin-register', data());
                    }else{
                        $values1 = array('username'=> $username, 'password'=> $old_pass);
                        $values2 = array('username'=> $username, 'password'=>  $form_field['password']);
                        if($this->users->check( $values1 ) ){
                            
                            if( $this->users->check( $values2 ) ){
                                $this->data['status'] = "You have provided the old password, please try another one!";
                                
                            }else {
                                $this->data['status'] = "You are ok!";
                                //$username;
                                
                                if( $this->users->change_password($user_id, $form_field)){
                                    $this->data['status'] = "Your password successfully changed!";
                                }else {
                                    $this->data['status'] = "your password didn't change!";
                                }
                                
                            }
                        }else {
                            
                            $this->data['status'] = "Your password didn't match, please enter the valid password!";
                        }
                        $this->load->view('admin/admin-register', data() );
                    }
                }else {
                    $this->load->view('admin/admin-register', data() );
                }
                
            }
        }else {
            $this->admin->access_forbidden();
        }
    }
    
    public function create(){
        $this->data['page_heading'] = "Add User";
        $this->load->model("form_hash");
        //Explain($_POST);
        if($this->authantication->getPermission("user", 1)){
            
            $this->load->helper(array('form', 'url'));
            $this->load->model("users");
            $this->data['email_error'] = null;
            $action = 'create';
            $this->data['user_roles'] = $this->users->get_roles();
            
            $this->data['action'] = $action;

            $this->data['page_title'] = 'Register for an Account';
            
            $this->form_validation->set_rules($this->validatation_config->user($action));

            
            
            $register_values = array();

            
            $register_values['role'] =  $this->input->post('role');
            
            $register_values['username'] =  $this->input->post('username');
            $register_values['firstname'] =  $this->input->post('firstname');
            $register_values['lastname'] =  $this->input->post('lastname');
            $register_values['email'] =  $this->input->post('email');
            $register_values['created_date'] =  date("Y-m-d H:i:s");
            $register_values['modified_date'] =  date("Y-m-d H:i:s");
            $register_values['password'] =  $this->input->post('password');
            $register_values['display_name'] =  $this->input->post('firstname')." ".$this->input->post('lastname');
            $register_values['ip_address'] =  $_SERVER['REMOTE_ADDR'];
            
            $this->data['value'] = $register_values;
            
            if($this->form_hash->check($this->input->post('hash'), "_add_user_security_hsah_")){
                $this->session->unset_userdata('_user_just_created_');
                if ($this->form_validation->run() == FALSE){
                    if(validation_errors() == ""){
                        $this->data['page_title'] = "Error in your form submission!";
                    }
                    $this->load->view('admin/admin-register', data() );

                }else{

                    if($user_id = $this->users->register($register_values)){
                        $this->session->set_userdata("_user_just_created_", true);
                        $this->load->model("email_operation");

                        $hash = md5($register_values['username'] .  microtime() .  rand(10, 100));
                        //$this->db->update->set();

                        $this->db->update('user', array("activision_key"=> $hash) , array('user_id' => $user_id));

                        $data['activation_link'] = $activationLink = base_url("admin/activate_user/".$hash."/".$user_id);

//                        if( $this->email_operation->sendCreateUserTheme($register_values['firstname'], $activationLink, $register_values['email']) ){

                            $this->data['page_title'] = 'User Successfylly Created!';

//                        }else {
                            
//                            $this->data['page_title'] = 'User Created. But Email not sent!';
//                            $this->data['email_error'] = $this->email_operation->getError();
//                        }
                        
                        $this->load->view('admin/admin-register-success', data() );
                        
                    }else {
                        $this->data['page_title'] = 'Database Error!';
                        $this->load->view('admin/admin-register', data() );
                    }
                }
            }else {
                if($this->session->userdata("_user_just_created_")){
                    $this->data['page_title'] = 'User Created!';
                    $this->load->view('admin/admin-register-success', data() );
                }else {
                    $this->data['page_title'] = 'Create User';
                    $this->load->view('admin/admin-register', data() );
                }
            }
        }else {
            $this->data['page_title'] = "403 Access Forbiddens";
            $this->load->view('admin/admin-access-forbidden', data() );
        }
    }
    
    public function white_space($str){
        if(preg_match("/[!@#\$%\^&\*()\+=\|\[\]\?><;:\"\'\/ ]+/", $str)){
            $this->form_validation->set_message('white_space', 'Invalid charecter at {field}');
            return false;
        }
        return true;
    }
    
    public function all(){
        $this->data['page_heading'] = "All User";
        $this->load->model("form_hash");
        if($this->authantication->getPermission("all_user", 2)){
            if($u = $this->users->get_all()){
                
                $this->data['allusers'] = $u;
                $this->load->view('admin/admin-all-users', data() );
            }
        }else {
            $this->admin->access_forbidden();
        }
    }
    
    public function activate($activation_key = null, $user_id = null){
        $this->data['page_heading'] = "Activating User";
        if($activation_key && $user_id){
            $this->db->select("user_id,active")->from("user");
            $this->db->where("user_id", $user_id);
            
            $q = $this->db->get();
            //echo $this->db->last_query();
            $res  = $q->row_array();
            
            if( $res['active'] == 0) {
                $this->db->update('user', array("activision_key"=> "", "active" => 1) , array('user_id' => $user_id, "activision_key" => $activation_key));
                if($this->db->affected_rows()){
                    echo "Success!";
                }else {
                    echo "Your link has been expired!";
                }
                
            }else {
                echo "You are already active!";
            }
            $activation_key;
        }else {
            echo "This user is not exists!";
        }
    }
    
    public function login(){
        $data =& data();
        $this->data['back_to']  = "http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
        
        $this->data['ovarall_message']  = "";
        
        $this->load->model("users");
        $this->form_validation->set_rules(array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required',
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required',
            )
        ));
        
        if ($this->form_validation->run() != FALSE){
            $username =  $this->input->post('username');
            $password =  $this->input->post('password');
            if($this->authantication->login_user($username, $password, $remember = false)){
                if($this->input->get("next")){    
                    redirect(urldecode($this->input->get("next")));
                }else {
                    redirect(base_url("admin/home"));
                }
            }else {
                $this->data['ovarall_message']  = "Your username or password in not valid!";
            }
        }

        $this->admin->view("admin-login", data() );
        
    }
    
    public function logout() {
        $this->session->unset_userdata('token');
        $this->session->unset_userdata('userData');
        $this->session->sess_destroy();
        redirect (base_url("admin/user/login"));
    }
}
