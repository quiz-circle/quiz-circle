<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Home
 *
 * @author Saif
 */
class Home extends Admin_controller{
    
    public function __construct(){
        parent::__construct();
    }

    function index(){
        
        $this->data['page_heading'] = "Dashboard";
        $this->data['page_title'] = "Dashboard :: " . $this->data['site_name'];
        
        $this->admin->view( 'admin-home', data() );
    }

}
