<?php
/**
 * Created by PhpStorm.
 * User: Raju Sheikh
 * Date: 11/14/2017
 * Time: 4:43 PM
 */

require_once APPPATH . "models/functions/quiz-functions.php";
require_once APPPATH . "views/public/functions/quiz-functions.php";
require_once APPPATH . "models/functions/each_objects/related_question_each.php";

class Quiz extends  Page_controller {

    public function __construct(){
        parent::__construct();
        $this->load->model( array("category", "users") );
    }

    public function test(){
        public_view("quiz-list");
    }

    public function perform( ){
        $DT =& data();
        $token = _get("token");



        if($token && $this->session->has_userdata($token) ) {
            if(strlen($token) == 32){
                $token_data = $this->session->userdata($token);

//                Explain($token_data);

                if( isset( $token_data['user_id'] ) && isset( $token_data['quiz_id'] ) ){
                    prepare_content( $DT['quiz_data'], $token_data['quiz_id']);
                    $DT['questions'] = quiz_questions( $token_data['quiz_id'], $DT["total_question"], "related_question_each" );

                    shuffle_assoc_array($DT['questions']);

                    public_view("quiz-perform");
                }else {
                    $DT['message'] = "Invalid Token Data!";
                    public_view("includes/quiz-token-mismatch");
                }

            }else {
                $DT['message'] = "Invalid Token!";
                public_view("includes/quiz-token-mismatch");
            }
        } else {
            $DT['message'] = "Token not found!";
            public_view("includes/quiz-token-mismatch");
        }
    }

    public function ready_to_go( $quiz_id ){
        public_view("quiz-list");
    }

    public function get_list (){

        $data =& data();
        $config = array(
            'quiz_type' => 'quiz'
        );
        prepare_contents( $quiz_list, $quiz_total, "quiz" , $config);

        $data['quiz_list'] = $quiz_list;
        $data['quiz_total'] = $quiz_total;

        $data['cat_config'] = array(
            'each_item_view'    =>  'admin/includes/each/class_item_input_options',
            'active_data'    => input_value('parent'),
        );

        public_view("quiz-test");
    }

    public function notice (){
        $data = &data();
        prepare_contents($data['notices'], $data['total_notice'], "notice");
        
        $n_cat = $this->db->select()->from("class")->where("type", "notice-category")->get();
        
        $data['notice_category'] = $n_cat->result_array();
        //$data['notices'] = array();
        public_view("notice");
        
        //$this->load->view("public/notice");
    }


}