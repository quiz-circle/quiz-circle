<?php


/**
 * Description of Script
 *
 * @author Saif
 */
class Script extends Page_controller {

    public function __construct(){
        parent::__construct(false);
        ob_start();
        $this->load->model(array( "meta", "authantication", "sec_view", "users" ));
    }

    public function index(){
        echo 'Access forbidden!';
    }

    public function site( $class_name, $script_name ){
        $this->load_public_script($class_name, $script_name);
    }

    public function site_( $class_name, $script_name ){
        $this->load_public_script( $class_name, $script_name, true );
    }

    private function load_public_script( $class_name, $script_name, $json = false ) {
        $class = load_class($class_name, "views/public/ajax/");
        if(method_exists($class, $script_name)){
            $result = array( );
            $result['is_logged_in'] = $this->_isLoggedIn;

            $result['sdata'] = ($result['is_logged_in']) ? $this->_sdata : array( );

            if($json) {
                $result['success'] = false;
                $class->$script_name( $result );
                $this->output->set_content_type('application/json')->set_output( json_encode($result) );
            } else {
                $class->$script_name( $result );
            }

        } else {
            exit("The function <b><em>" .$script_name . "</em></b> in not found inside the <b><em>" . $class_name . "</b></em> Class!");
        }
    }

    public function __destruct() {
        ob_end_flush();
    }
}
