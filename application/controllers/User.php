<?php


require_once APPPATH . "views/public/functions/registration-function.php";
require_once APPPATH . "models/functions/quiz-functions.php";

class User extends Page_controller {
    //put your code here
    
    public function __construct() {
        parent::__construct();
        $this->_set_social_links();
        
        $data =& data();
        $data['messages'] = array();

        $action = uri_seg(2);

        //if( $this->input->post() && check_post_form_token("_token") ) {
        if( $this->input->post() ) {

            //echo $action;

            switch($action){
                case "login":
                    $this->_do_login();
                    break;
                case "signup":
                    $this->_do_registration();
                    break;
                    
            }
        }
    }
    
    private function _do_registration( ){
        $data =& data( );
        $data['messages'] = array();
        if(_post() ){

            if( validate_registration_information( $data['messages'] )){
                $data['ovarall_message']  = "";
                $this->load->model("users");
                $user_type_hash = _unique_hash();

                $values = array(
                    'username' => _post("username"),
                    'firstname' => _post("firstname"),
                    'lastname' => _post("lastname"),
                    'email' => _post("email"),
                    'password' => _post("password"),
                    'gender' => _post("gender"),
                    'display_name' => _post("firstname") . " " . _post("lastname"),

                    'created_date' => date_db_formated(),
                    'modified_date' => date_db_formated(),
                    'user_type_hash' => $user_type_hash,
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                );

                if( $user_id = $this->users->user_register($values)){
                    $student_values = array(
                        'student_id' => _post('student_id') ,
                        'user_type_hash' => $user_type_hash ,
                        'user_id' => $user_id ,
                    );
                    if( $this->users->add_student( $student_values ) ){
                        $this->session->set_userdata("_user_signup_success_", true);
                        redirect(base_url("user/signup/success"));
                    }
                }
            }
        }
    }

    private function _do_login(){
        $data =& data();
        $username = _post("username");
        $password = _post("password");
        $data['back_to']  = "http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
        //$this->data['ovarall_message']  = "";

        $this->load->model("users");
        $this->form_validation->set_rules(array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required',
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required',
            )
        ));

        $data['ovarall_message'] = "";

        if ($this->form_validation->run() != FALSE){
            $username = _post('username');
            $password = _post('password');


            if(  $this->authantication->login_user($username, $password, $remember = false) ){
                if($this->input->get("next") || $this->session->has_userdata( SESSION_NEXT_PAGE_AFTER_LOGIN )){
                    $redirectURL = $this->session->has_userdata( SESSION_NEXT_PAGE_AFTER_LOGIN ) ? $this->session->userdata( SESSION_NEXT_PAGE_AFTER_LOGIN ) : $this->input->get("next");
                    redirect(urldecode( $redirectURL ));
                }else {
                    $data['ovarall_message']  = "<span style='color: green'>Success!</span>";
                    redirect(base_url());
                }
            }else {
                $data['ovarall_message']  = "<span style='color: brown'>Your username or password is not valid!</span>";
            }
        }
    }
    
    public function social_login( $type = "" ){
        echo $type. "<br>";
        $this->session->set_userdata("_social_login_reference", $type);
        if(!empty($type)) {
            switch ($type){
                case "facebook" :
                    if($fb_login_url = $this->session->has_userdata("_fb_login_url")){

                        $fb_login_url = urldecode( $this->session->userdata("_fb_login_url") );
                        redirect($fb_login_url);
                    }
                    break;
                case "google" :
                    if( $google_login_url = $this->session->has_userdata("_google_login_url")){
                        //echo $type . "<br>";
                        $google_login_url = urldecode(  $this->session->userdata("_google_login_url")  );
                        echo $google_login_url;

                        redirect( $google_login_url );
                    }
                    break;
            }
        }

        redirect( base_url("user/login") );

    }





    public function quiz( $view = "list", $participation_id = 0){
        $data =& data();
        $user_id = isset($this->_sdata['user_id']) ? $this->_sdata['user_id'] : false;
        $user_type = $this->_auth->getUserType();

        if($user_id && $user_type == "student") {
            switch ($view) {
                case "list":
                    $config = array(
                        "user_id" => $user_id,
                    );

                    $data['result_list'] = participated_student_list(0, $data['total'], $config );
                    public_view("finished-quiz-list.php");
                    break;
                case "result":
                    public_view("quiz-result");
                    break;
                default:
                    redirect( base_url("user/quiz/list") );
            }
        }else {
            echo "<h2>You are not suudent! <a href='" . base_url() . "'>Back To Home</a></h2>";
        }

    }

    public function signup($success = ""){
        $data =& data();
        $data['signup_success'] = ($success == "success" && $this->session->userdata("_user_signup_success_") ) ? true:false;
        $data['page_title'] = "Sign up for an account! :: Quiz Circle";

        public_view("login");
    }

    public function login(){
        $data =& data();
        $data['page_title'] = "Login to your account! :: Quiz Circle";
        public_view("login");

    }

    public function profile(){
 data();
        $data['page_title'] = "User profile! :: Quiz Circle";
        public_view("profile");

    }

    public function forgot_password(){
        $data =& data();
        $data['page_title'] = "Login to your account!  :: Quiz Circle";
        public_view("login");
    }

    public function logout(){
        $this->authantication->logout();
        redirect(base_url());
    }
    
}
