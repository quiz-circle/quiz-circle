<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Listing_configs
 *
 * @author saifuddin
 */
class List_config {
    //put your code here
    public $_action_list;


    public function search_html($search_key, $value = ""){
        
        echo form_open( "", array("method" => "get" ) );
        echo form_input($search_key, $value, array("placeholder" => "Search..."));
        echo form_submit("", "Search", array("class" => "btn"));
        echo form_close();
    }
    
    
    
    
    public function filters_html( ){
        $SYS =& CI_Controller::get_instance();
        //Explain($SYS->load->model());
        $SYS->load->model( array("category", "users") );
        
        $users = $SYS->users->get_all("display_name, user_id as id");
        $user_list = array();
        
        if(is_array($users)) {
            foreach( $users as $user ){
                $user_list[ $user['id'] ] = $user['display_name']; 
            }
        }
        
//        /Explain($users);
        
        $config['value_only'] = false;
        
        
        function each_class($item, $data){
            return get_tree_child_sign($data['level'], "&mdash;"). $item['name'];
        }
        
        
        $config['array_index'] = "url_ref";
        $config['each_function'] = "each_class";
        
        
        $category_list = $SYS->category->getList( $config, true );
        //$category_list[""] = "(no category)";
        array_unshift($category_list,  "(no category)" );
        //Explain( $SYS->category->getList( array(), true ) );
        
        
        echo form_open( "", array("method" => "get" ) );
        
        $name = "category"; $label = "Category"; $value = $SYS->input->get($name); 
        echo form_label($label, $name);
        echo form_dropdown($name, $category_list, $value, array("id" => $name));
        
        $name = "user_id"; $label = "User"; $value = $SYS->input->get($name); 
        echo form_label($label, $name);
        echo form_dropdown($name, $user_list, $value, array("id" => $name));
        
        echo form_submit("", "Filter", array("class" => "btn"));
        echo form_close();        
    }
    
    public function form_htmls($inputs){
        
        echo form_open( "", array("method" => "get" ) );
        
        if(is_array($inputs)){
            foreach($inputs as $input_type => $attributes){
                $devider = set_unset_value($attributes, "devider");
                switch($input_type){
                    case "form_input":
                        echo form_input( $attributes );
                        break;
                    case "form_dropdown":
                        $name = set_unset_value($attributes, "name");
                        $options = set_unset_value($attributes, "options");
                        $selected = set_unset_value($attributes, "selected");
                        echo form_dropdown($name, $options, $selected, $attributes);
                        break;
                }
                echo $devider;
            }
        }
        
        echo form_submit("", "Search", array("class" => "btn"));
        echo form_close();
    }
    
    
    
    public  function _post_action_list(  $active = "all"  ){
        
        $default = array(
            "trash" => array('conf' => array( 'attrs' => array('data-selected-action'=> 'trash' ),'label' => 'Trash' ) , 'action' => "make_selected_trash" ),
            "active" => array('conf' =>  array( 'attrs' => array('data-selected-action'=> 'active' ),'label' => 'Active' ) , 'action' => "make_selected_active" ),
            "hidden" => array('conf' =>  array( 'attrs' => array('data-selected-action'=> 'hidden' ),'label' => 'Hidden' ) , 'action' => "make_selected_hidden" ),
        );
        switch ($active) {
            case "hidden":
                unset($default['hidden']);
                break;
            case "active":
                unset($default['active']); 
                break;
            case "trash":    
                return array(
                    "delete_permanently" => array('conf' => array( 'attrs' => array('data-selected-action'=> 'delete_permanently' ),'label' => 'Delete Parmanently' ) , 'action' => "delete_permanently" ),
                    "restore" => array('conf' =>  array( 'attrs' => array('data-selected-action'=> 'restore' ),'label' => 'Restore' ) , 'action' => "resote_content" ),
                );
                break;
        }
        return $default;   
    }
    
    
    public function _page_filter_links( ){
        $list = array();
        
        return array(
            "All" => array('url' => 'admin/posts/get_list/page-all/', 'count_where' => ' status = "1" and content_type = "page" ' ),
            "Active" => array('url' => 'admin/posts/get_list/page-active/','count_where' => ' status = "1" and visibility = "1" and content_type = "page" ' ),
            "Hidden" => array('url' => 'admin/posts/get_list/page-hidden/','count_where' => ' status = "1" and visibility = "0" and content_type = "page"' ),
            "Trash" => array('url' => 'admin/posts/get_list/page-trash/','count_where' => ' status = "0" and content_type = "page" ' ),
        );
    }
    
    public function _post_filter_links( $content_type ){
        $list = array();
        //$query_string = !empty($_SERVER['QUERY_STRING']) ? "?" . urldecode($_SERVER['QUERY_STRING']) : "";
        return array(
            "All" => array('url' => 'admin/posts/get_list/'.$content_type.'-all/', 'count_where' => ' status = "1" and content_type = "'.$content_type.'" ' ),
            "Active" => array('url' => 'admin/posts/get_list/'.$content_type.'-active/','count_where' => ' status = "1" and visibility = "1" and content_type = "'.$content_type.'" ' ),
            "Hidden" => array('url' => 'admin/posts/get_list/'.$content_type.'-hidden/','count_where' => ' status = "1" and visibility = "0" and content_type = "'.$content_type.'"' ),
            "Trash" => array('url' => 'admin/posts/get_list/'.$content_type.'-trash/','count_where' => ' status = "0" and content_type = "'.$content_type.'" ' ),
        );
    }

    public function set_pagination_html_config(&$config, $attributes = array()){
        $ul_attr = isset($attributes['ul']) ? $attributes['ul'] : array();
        $li_attr = isset($attributes['li']) ? $attributes['li'] : array();
        $first_tag_attr = isset($attributes['first_tag']) ? $attributes['first_tag'] : array();
        $last_tag_attr = isset($attributes['last_tag']) ? $attributes['last_tag'] : array();
        $cur_tag_attr = isset($attributes['cur_tag']) ? $attributes['cur_tag'] : array();
        $next_tag_attr = isset($attributes['next_tag']) ? $attributes['next_tag'] : array();
        $prev_tag_attr = isset($attributes['prev_tag']) ? $attributes['prev_tag'] : array();
        
        $config['full_tag_open'] = '<ul ' . _attributes_to_string($ul_attr) . '>';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li ' . _attributes_to_string($first_tag_attr) . '>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li ' . _attributes_to_string($last_tag_attr) . '>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li ' . _attributes_to_string($next_tag_attr) . '>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li ' . _attributes_to_string($prev_tag_attr) . '>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li ' . _attributes_to_string($cur_tag_attr) . '><span>';
        $config['cur_tag_close'] = '</span></li>';
        $config['num_tag_open'] = '<li ' . _attributes_to_string($li_attr) . '>';
        $config['num_tag_close'] = '</li>';
    }
    
    public function pagination_attrs(){
        $attrs = array();
        $attrs['ul'] = array( "id" => "pagination" );
        $attrs['cur_tag'] = array( "class" => " active ");
        return $attrs;
    }
    
    public function selected_option_views($attributes, $action_select_name = "selected-action" ){
        ?>

        <div <?php echo _attributes_to_string($attributes); ?> >

        <?php

        if(isset($this->_action_list)):?>
            <?php if(is_array($this->_action_list)):?>
            <!--<label for="selected-action" id="selected-action-label">Action</label>-->
            <select name="<?php  echo $action_select_name."[]";  ?>" id="selected-action" onchange="var a = this.value; $('#selected-action').each( function(){ $(this).val(a);  }); ">
                <option value="">(No Action)</option>
            <?php foreach($this->_action_list as $value => $config): ?>
                <option value="<?php echo $value?>" <?php //echo _attributes_to_string($config['conf']['attrs']) ?> ><?php echo $config['conf']['label'] ?></option>
            <?php endforeach;?>
            </select>
            <input type="submit" value="Done" class="btn" name="selected-act-btn">
            <?php endif;?>
        <?php endif;?>
        </div><?php
    }
    
}

