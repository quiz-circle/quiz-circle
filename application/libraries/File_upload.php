<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of File_upload
 *
 * @author Saif
 */
class File_upload {
    //put your code here
    private $_file, $_isMultiple, $_error = array(), $_encrypt_name = false, $_success_func= false, $_success_data;
    public  $upload_path,
            $max_size,
            $max_width,
            $max_height,
            $allowed_types;
    
    public function __construct($config) {

       foreach ($config as $property => $value){
           if(property_exists($this, $property)){
               $this->$property = $value;
           }
       }
    }
    
    public function encrypt_name(){
        $this->_encrypt_name = true;
    }
    
    public function upload_success($func){
        $this->_success_func = $func;
    }
    
    
    public function is_multiple(){
        return $this->_isMultiple;
    }
    
    public function do_upload($file){
        
        //Explain($_FILES[$file]);
        //echo $this->max_size;
        $this->_isMultiple = is_array($_FILES[$file]["name"]);

        if(is_dir($this->upload_path)){
            
            if(isset($_FILES[$file])){
                if(is_array($_FILES[$file]["name"])){
                    for($i = 0; $i < count($_FILES[$file]["name"]); $i++){
                        $name = $_FILES[$file]["name"][$i];
                        $type = $_FILES[$file]["type"][$i];
                        $tmp_name = $_FILES[$file]["tmp_name"][$i];
                        $error = $_FILES[$file]["error"][$i];
                        $size = $_FILES[$file]["size"][$i];
                        if(!$this->validSize($size)){
                            $this->_error['max_size'][] = "Maximum filesize exceds at " . $name;
                        }
                        if(!$this->validType($name, $ext)){
                            $this->_error['file_type'][] = "Filetype is not supported at " . $name;
                        }
                        $this->upload($tmp_name, $name, $error, $ext, $size);
                    }
                }else {
                    $name = $_FILES[$file]["name"];
                    $type = $_FILES[$file]["type"];
                    $tmp_name = $_FILES[$file]["tmp_name"];
                    $error = $_FILES[$file]["error"];
                    $size = $_FILES[$file]["size"];

                    if(!$this->validSize($size)){
                        $this->_error['max_size'] = "Maximum filesize exceds at " . $name;
                    }
                    if(!$this->validType($name, $ext)){
                        $this->_error['file_type'] = "Filetype is not supported at " . $name;
                    }
                    $this->upload($tmp_name, $name, $error, $ext,$size);
                }
                $this->_file = $_FILES[$file];
            }
        }else {
            $this->_error['dir'] = "Invalid upload directory!";
        }
        
        //Explain($this->_error);
    }
    
    public function upload($tmp_name, $name, $error, $ext, $filesize){
        $original_name = $name;
        $this->upload_path = rtrim($this->upload_path, "/") ."/";
        
        if($error === 0){
            
            $name = ($this->_encrypt_name) ? md5($name . microtime() . rand(5, 50)): $name;
            
            if(!count($this->_error)) {
                if(is_uploaded_file($tmp_name)){
                    
                    if(move_uploaded_file($tmp_name, $this->upload_path . $name.".".$ext)){
                        
                        $img_size = getimagesize($this->upload_path . $name.".".$ext);
                        
                        $success_param = array(
                            'size'          =>      $img_size,
                            'original_name' =>      $original_name,
                            'name'          =>      $name.".".$ext,
                            'ext'           =>      $ext,
                            'file_size'     =>      $filesize,
                        );
                        
                        if(!$this->_isMultiple){
                            $this->_success_data = $success_param;
                        }else {
                            $this->_success_data[] = $success_param;
                        }
                        
                        if($success_function = $this->_success_func){
                            $success_param = (object) $success_param;   
                            $success_function($success_param);
                        }
                    } else {
                        $this->_error['upload'] = $name . ".".$ext . "Uploading failed " ;
                    }
                }
            }
        }else {
            $this->_error['unknown'] = "Something went wrong!";
        }
    }
    
    public function data($key = false){
        
        if(!$this->_isMultiple){
            if(!$key) {
                return $this->_success_data;
            }
            return isset($this->_success_data[$key]) ? $this->_success_data[$key]:false;
        }else {
            return $this->_success_data;
        }
        
    }
    
    public function error($key = false){
        if(!count($this->_error)){
            return false;
        }else {
            if(!$key){
                return $this->_error;
            }
            return isset($this->_error[$key]) ? $this->_error[$key]:false;
        }
    }
    
    
    
    private function validSize($size){
        //echo $this->max_size ."-> ".$size."<br>";
        return $this->max_size > $size;
    }
    
    private function validType($name, &$type = null){
        $ext = explode(".", $name);
        $types = explode("|", $this->allowed_types);
        $type = strtolower(end($ext));
        return in_array($type, $types);
    }
    
}

