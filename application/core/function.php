<?php
date_default_timezone_set("Asia/Dhaka");


define("HEADLINE_REFERENCE_KEY", 'news_head_line_id');

define( 'SESSION_NEXT_PAGE_AFTER_LOGIN', '__next__after__login__' );


function  &data($key = null){
    $data =& CI_Controller::_data();
    if(isset($data[$key]))
        return $data[$key];
    else
        return $data;

}

function shuffle_assoc_array(&$array) {
    $keys = array_keys($array);

    shuffle($keys);

    foreach($keys as $key) {
        $new[$key] = $array[$key];
    }

    $array = $new;
    return true;
}

function quiz_questions( $quiz_id, &$total = 0, $each_class_name = ""){
    $DB = get_instance()->db;
    $DB->select()->from("quest_assign qa");
    $DB->join("questions qu", "qu.quest_id = qa.quest_id");
    $DB->where( "quiz_id", $quiz_id );
    $DB->order_by( "qa.position" );
    $DB->group_by( "qa.id" );

    $quiz = $DB->get();

    if($total = $quiz->num_rows()){
        if(!empty($each_class_name)){
            if( class_exists($each_class_name) ) {
                return $quiz->result($each_class_name);
            }
        }else {
            return $quiz->result_array();
        }
    }
    return array();
}

function get_category( $type = "question-category" ){
    $SYS = get_instance();
    $SYS->load->model( "category" );
    $SYS->category->type = $type;
    $config = array();
    //$data = array(
        //  'item' => $item, 
        //  'child' => $child, 
        //  'level' => $level, 
        //  'active_data' => $this->active_data, 
        //  'counter'=> $counter 
    //  );

    $config['each_function'] = function( $class, $data ) {
        return get_tree_child_sign( $data[ 'level' ] , "-") . $class['name'];
    };
    
    return $SYS->category->getList( $config, true );
}



function plural_word($word){

    if(strripos($word, "s") || strripos($word, "x")){
        return $word. "es";
    }elseif( strripos($word, "y") ){
        return rtrim($word, "y") . "ies";
    }else {
        return $word. "s";
    }
}

function class_is_tree($type = "category"){
    $exception_list =  array('category','quiz-category','question-category','tutorial-category','paragraph-category');
    return in_array($type, $exception_list);
}

function isValidJson($string){
    $json = json_decode($string);
    return ( is_object($json) && json_last_error() == JSON_ERROR_NONE ) ? true : false;
}

function date_db_formated( $date = "" ){
    if(!empty($date)) {
        return date("Y-m-d H:i:s");
    }else {
        $date = date_create($date);
        return date_format($date, "Y-m-d H:i:s");
    }
}

function admin_images($images){
    $SYS =& CI_Controller::get_instance();
    echo $SYS->admin->images($images);
}

function admin_css($images){
    $SYS =& CI_Controller::get_instance();
    echo $SYS->admin->css($images);
}

function admin_js($images){
    $images = rtrim($images, ".js") . ".js";
    $SYS =& CI_Controller::get_instance();
    echo $SYS->admin->js($images);
}

function theme_images($images){
    $SYS =& CI_Controller::get_instance();
    echo $SYS->_public->images($images);
}

function theme_css($images){
    $SYS =& CI_Controller::get_instance();
    echo $SYS->_public->css($images);
}

function theme_js($images){
    $SYS =& CI_Controller::get_instance();
    echo $SYS->_public->js($images);
}

function public_view( $path, $data = array() ){
    $data = (count($data)) ? $data : data();
    get_instance()->_public->view( $path, $data );
}

function _config( $array, $key ){
    return isset($array[$key]) ? $array[$key]:"" ;
}

function _post( $key = null ){
    return get_instance()->input->post( $key );
}

function _get( $key = null ){
    return get_instance()->input->get( $key );
}

function form_token( $key = "_flash_item" ){
    $value = md5(uniqid() . rand( 5 , 25 ));
    get_instance()->session->set_flashdata($key, $value);
    return $value;
}

function check_post_form_token( $inputName, $key = "_flash_item" ){
    $SYS = get_instance();
    $flash_data = $SYS->session->flashdata($key);
    $input_value = trim($SYS->input->post($inputName));
    return $flash_data == $input_value;
}

function check_get_form_token( $inputName, $key = "_flash_item" ){
    $SYS = get_instance();
    $flash_data = $SYS->session->flashdata($key);

    $input_value = $SYS->input->get($inputName);

    return $flash_data == $input_value;
}

function admin_view( $path, $data = array() ){
    $data = (count($data)) ? $data : data();
    get_instance()->admin->view( $path, $data );
}

function _isLoggedIn($path){
    $SYS = get_instance();
    $SYS->load->model("authantication");
    return ( $SYS->authantication->isLoggedIn() ) ? $SYS->authantication->get_data() : false;
}

function uri_seg($segment_number){
    return get_instance()->uri->segment($segment_number);
}



function model_exists($model_path){
    return file_exists(APPPATH . "models/" . $model_path . ".php");
}

function view_exists($view_path){
    return file_exists(APPPATH . "views/" . $view_path . ".php");
}


function controller_exists( $controller_path ){
    return file_exists(APPPATH . "controllers/" . $controller_path . ".php");
}

function assoc_to_indexed_array($array) {
    $act = array( );
    
    foreach($array as $value){
        $act[] = $value;
    }
    
    return $act;
} 

function tree_child_sign($num, $sign = "&nbsp;&nbsp;"){
    echo get_tree_child_sign($num, $sign);
}

function get_tree_child_sign($num, $sign = "&nbsp;&nbsp;"){
    $output = '';
    if(is_int($num)) {
        for($i = 0; $i < $num; $i++){
            $output .= $sign;
        }
    }
    return $output;
}

function set_unset_value(&$array, $key){
    $value = "";
    if( is_array($array)) {
        if(isset($array[$key])){
            $value = $array[$key];
            unset($array[$key]);
        }
    }
    return $value;
}

function get_category_list(){
    $CI =& get_instance();

    if(isset($CI->tree) && is_object($CI->tree)){
        //echo "Object Found!";
    }else {
        //echo "Object not found!";
    }
}
    
function input_value( $key ){
    $CI =& get_instance();
    
    if(isset($CI->category) && is_object($CI->category)){
        $values = $CI->category->_input_values;
        return isset($values[$key]) ? $values[$key]:"" ;
    }
}

function next_string_sequence($string, $delemiter = "-"){
    
    $string_segments = explode( $delemiter, $string);

    $last_part = end( $string_segments );
    if( is_numeric($last_part) ){
        $string = rtrim($string, $last_part);
        $last_part =(int) $last_part;
        $last_part++;
        return $string.$last_part;
    }else {
        return $string . "-2";
    }
}

function get_content_starting($string, $limit = 400){
    if(strlen($string) < $limit ) return  $string;
    $string = strip_tags($string);    
    $string = substr($string, 0, $limit);
    $last_space = strrpos($string, " ");
    
    return substr($string, 0, $last_space)."...";
}

function get_image_links($string, $get = "src"){
    return get_attrs_from_string( $string, "img", $get );
    preg_match_all('/<img[^>]+>/i',$string, $result);
    array_walk($result[0], "find_attributes", $get);
    return $result[0];
}

function get_attrs_from_string( $string, $tag_name = "div", $get = "src"){
    preg_match_all('/<' . $tag_name .'[^>]+>/i',$string, $result);
    array_walk($result[0], "find_attributes", $get);
    return $result[0];
}



function find_attributes(&$arr, $key, $get){   
    $res = array();
    if(!is_array($get)){
        $sr = preg_match_all('/('.$get.')=("[^"]*")/i', $arr, $att);
        if(!$sr){
            $sr = preg_match_all('/('.$get.')=(\'[^\']*\')/i', $arr, $att);         
        }
        $attr = ($sr) ?  $att[2][0]:"";
        $attr_value = rtrim(ltrim($attr, "\""), "\"");
        $attr_value = rtrim(ltrim($attr_value, "'"), "'");
        _alert($key);
        $res[$key] = $attr_value;
    }else if(is_array($get)) {
        foreach($get as $key){
            $sr = preg_match_all('/('.$key.')=("[^"]*")/i', $arr, $att);
            if(!$sr){
                $sr = preg_match_all('/('.$key.')=(\'[^\']*\')/i', $arr, $att);
            }
            $attr = ($sr) ?  $att[2][0]:"";
            $attr_value = rtrim(ltrim($attr, "\""), "\"");
            $attr_value = rtrim(ltrim($attr_value, "'"), "'");
            $res[$key] = $attr_value;            
        }
    }
    $arr = $res;
}


function get_dir($dir, $type = "dir", $exclution = array()){
    
    $not_dir = array(".", "..");
    
    
    if(is_array($exclution)){
        $not_dir = array_merge($not_dir, $exclution);    
    }else {
        $not_dir = array_merge($not_dir, array($exclution));
    }

    $result = array();
    if(is_dir($dir)){
        $dirs = scandir($dir);
        $result = array();
        foreach($dirs as $theme){
            if(!in_array($theme, $not_dir)){
                switch ($type) {
                    case "file":
                        if(is_file($dir .'/'. $theme)){
                            $result[] = $theme;
                        }
                        break;
                    case "dir" :
                        //echo $type , "<br>";
                        if(is_dir($dir . $theme)){
                            $result[] = $theme;
                        }
                        break;
                    default :
                        $result[] = $theme;
                        break;
                }
            }
        }
    }
    return $result;
}


function format_date($date, $format = ""){
    $format = $format == "" ? config_item("date_format") : $format;
    $date = date_create($date);
    return date_format($date, $format);
}

function _unique_hash($random_min = 0, $random_max = 50){
    return md5(microtime() . rand($random_min, $random_max) . uniqid());
}

function asset_path($uri = null, $protocol = null) {
    return base_url("assets/" . $uri, $protocol);
}

function set_message(&$result, $success_message, $failed_message = ""){
    if(isset($result['success'])){
        if($result['success']){
           $result['message'] = $success_message; 
        }else {
           $result['message'] = $failed_message;
        }
    }else {
        $result['message'] = $success_message;
    }
}

function __($text, $key) {

    $lang = isset($GLOBALS['lang']) ? $GLOBALS['lang'] : "en";

    $l = simplexml_load_file(asset_path("common/lang.xml"));

    return empty($l->$key->$lang) ? (String) $text : (String) $l->$key->$lang;
}





function input_data($valus, $key) {
    return isset($valus[$key]) ? $valus[$key] : "";
}

function _array($valus, $key, $default = "") {
    return isset($valus[$key]) ? $valus[$key] : $default;
}



function &page_data(){
    return CI_Controller::_data();
}

function set_page_data( $key, $value ){
    $data =& CI_Controller::_data();
    $data[$key] = $key;
}

function data_success_message( $message ){
    $data =& CI_Controller::_data();
    $data['_success_message'] = $message;
}

function data_warning_message( $message ){
    $data =& CI_Controller::_data();
    $data['_warning_message'] = $message;
}

function data_info_message( $message ){
    $data =& CI_Controller::_data();
    $data['_info_message'] = $message;
}

function data_danger_message( $message ){
    $data =& CI_Controller::_data();
    $data['_danger_message'] = $message;
}

function section_success_message($message){
    ?><script>section_success_message("<?php echo $message?>")</script><?php
}

function section_warning_message($message){
    ?><script>section_warning_message("<?php echo $message?>")</script><?php
}

function section_info_message($message){
    ?><script>section_info_message("<?php echo $message?>")</script><?php
}

function section_danger_message($message){
    ?><script>section_danger_message("<?php echo $message?>")</script><?php
}
