<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of App_model
 *
 * @author saif
 */


function order_by($db, $order_by, $type = "asc"){
    if(is_array($order_by)){
        
        foreach ($order_by as $column){
            if( is_array($column) ){
                if( isset( $column[0]) && isset( $column[1] ) ){
                    $db->order_by($column[0], $column[1]);
                }
            }else {
                $db->order_by($column);
            }
        }
        
    }else {
        $db->order_by($order_by, $type);
    }
}

//interface Crud{
//    
//}

//implements Crud

abstract class APP_Crud extends CI_Model  {
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    
    private $_tb_col = array();
    
    protected   $_function_data,  
                $_data,
                $_num_rows, 
                $_limit,
                $_columns = " * ",
                $_start = 0,
                $_table_name,
                $_primary_key,
                $_each_obj_name = "",
                $_total_rows, $_action_method;
    
    public      $_action_list;
    
    public function set_table($table, $column = "*"){
        $this->_tb_col['table'] = $table;
        $this->_tb_col['column'] = $column;
        $this->_table_name = $table;
        $this->_columns = $column;
        return $this;
    }
    
    public function reset_table(){
        $this->_table_name = isset( $this->_tb_col['table'] ) ? $this->_tb_col['table'] : "";
        $this->_columns = isset( $this->_tb_col['column'] ) ? $this->_tb_col['column'] : "";
    }
    
    public function set_columns( $column ){
        $this->_columns = $column;
    }
    
    public function reset_columns(){
        $this->_columns = "*";
    }
    
    public function item_count($where = false){
        $column = $this->_primary_key ?  $this->_primary_key : "*";
        if($where){
            return (int) $this->db->select( $column )->from($this->_table_name)->where($where)->get()->num_rows();
        }else {
            return (int) $this->db->select( $column )->from($this->_table_name)->get()->num_rows();
        }
    }
    
    protected function _set_extra_sql( $more_query_setting ) {
        
        if(!is_bool($more_query_setting)) {
            if(!is_numeric($more_query_setting)) {
                
                if(is_array( $more_query_setting )) {
                    $this->db->where( $more_query_setting );
                    return true;
                } else if( is_object( $more_query_setting ) || function_exists( $more_query_setting ) ){
                    $more_query_setting( $this->db, $this->_function_data );
                    return true;
                } else {
                    if(method_exists( $this, $more_query_setting )){
                        $this->$more_query_setting($this->_function_data);
                        return true;
                    }
                }
            }
        }
        
        return false;
    }
    
    
    protected function select($columns, $table, $more_query_setting = false){
        $this->db->select( $columns )->from( $table );        
        $this->_set_extra_sql($more_query_setting);
    }
    
    protected function fetch_data( $more_query_setting = false, $row = false, $limit = -1, $start = 0){
        //_alert( __LINE__ . "ok" );
        
        //echo $this->_columns;
        
        $this->select( $this->_columns, $this->_table_name, $more_query_setting);
        $list = $this->db->get();
        $this->_total_rows = $list->num_rows();
        
        $this->select($this->_columns, $this->_table_name, $more_query_setting);
        if( is_numeric($this->_limit) && $this->_limit > 0 ) {
            $this->db->limit($this->_limit, $this->_start);
        }
        //_alert($start);
        if($limit > 0 && $start >= 0)  $this->db->limit($limit, $start);
        
        //Explain($row, "dump");
        
        $list = $this->db->get( );
        $this->_num_rows = $list->num_rows( );
        $this->_columns = " * ";

        if( class_exists($this->_each_obj_name) ){
            $data = $row ? $list->row($this->_each_obj_name) : $list->result($this->_each_obj_name );
            $this->_each_obj_name = false;
            return $data;
        }
        $this->reset_table();
        return $row ? $list->row_array() : $list->result_array( );
    }
    
    /**
     * 
     * @param object $obj
     * 
     * @return void 
     */
    
    public function each_obj($obj){
        $this->_each_obj_name = $obj;
    }
    
    protected function fetch_trash_data( $data, $limit = -1, $start = 0 ) {
        
        if( !empty($this->_table_name) ) {
            
            $data['table_name'] = $this->_table_name;
            $this->set_function_data($data);
            
            return $this->fetch_data(function($db, $extra){
                
                $db->join("trash_info", "$this->_table_name.$this->_primary_key = trash_info.key");
                $db->where("trash_info.status", '0');
                $db->where("$this->_table_name.status", '0');
                $db->where("trash_info.table",  $this->_table_name);
                $db->group_by('trash_info.key');
                
                if( isset($extra['func']) ){    
                    $func = $extra['func'];
                    $data = isset($extra['data']) ? $extra['data'] : '';
                    if(!is_bool($func)) {
                       if(!is_numeric($func)) {
                           if(is_object($func) || function_exists($func)){
                               $func($db, $data);
                           }
                       }
                    }
                }
                
            }, false, $limit, $start);
            
        }
        
    }
    
    
    
    protected function add_trash_data( $id ){
        if(is_array($id)){
            $data = array();
            foreach($id as $key){
                $data[] = array(
                    'key' => $key,
                    'table' => $this->_table_name,
                    'trashed_date' => date("Y-m-d H:i:s")
                );
            }
            
            return $this->db->insert_batch("trash_info", $data);
        }else {
            $this->db->insert("trash_info", array('key' => $id, 'table' => $this->_table_name, 'trashed_date' => date("Y-m-d H:i:s")));
            return $this->db->insert_id();
        }
        return false;
    }
    
    protected function delete_trash_data($id){
        
    }

    protected function limit( $limit, $start = 0 ) {
        $this->_limit = $limit;
        $this->_start = $start;
    }
    
    public function set_function_data($data) {
        $this->_function_data = $data;
    }
    
    public function get_total_num_rows() {
        return $this->_total_rows;
    }
    
    public function get_num_rows() {
        return $this->_num_rows;
    }
    
    protected function _edit_item ($data, $where, $unique_id = null) {
        
        if(is_array($where)) {
            
            if(count($where)){
                return $this->db->update($this->_table_name, $data, $where);
            }else {
                return $this->db->update($this->_table_name, $data);
            }
        }else if( (is_numeric ($where) && $unique_id) ){
            return $this->db->update($this->_table_name, $data, array( $unique_id => $where) );
        } else {
            if($this->_set_extra_sql( $where ) ){
                return $this->db->update($this->_table_name, $data);
            }
        }
        $this->reset_table();
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }
    

    protected function _set_status( $status, $where, $unique_id = null ){
        if(is_numeric($where)) {
            return $this->_edit_item( array('status' => $status), $where, $unique_id);
        }else {
            
            return $this->_edit_item( array('status' => $status), $where);
            
        }
        
    }
    
    
    
    public function make_trash( $unique_id = null, $where = null ){
        
        //Explain($this->add_trash_data( $unique_id ));
        
        if($this->add_trash_data( $unique_id )) {
            
            if(is_array( $unique_id )){
                $this->set_function_data($unique_id);
                
                return $this->_set_status(0, function($db, $where){
                    $db->where_in($this->_table_name . ".id", $where);
                });
                
                
            } else {
                
            }
        }
        return false;
    }
    
    
    public function _delete(  $where, $unique_id = null ){
        if(is_array($where)) {
            $this->db->delete( $this->_table_name, $where);
        } else if(is_numeric ($where) ){
            $this->db->delete( $this->_table_name, array( $unique_id => $where));
        }
        return $this->db->affected_rows();
    }
    
    
    
    
    public function execute_selected_options($action_list, $action_select_name = "selected-action" ){
        //Explain($this->list_config->_action_list);
        //Explain($this->input->post());
        
        if($action_name = $this->input->post($action_select_name) ){
            //Explain( $this->input->post() );
            
            if(is_array($action_name)){
                if(count($action_name) == 1){
                    $action_name == $action_name[0];
                }else {
                    $action_name = isset($action_name[0]) ? $action_name[0]:$action_name[1];
                }
            }
            if(isset($action_list[$action_name]['action'])){
  
                $method_name = $action_list[$action_name]['action'];
                if(method_exists($this, $method_name)){
                    $this->$method_name();
                }
            }
        }
    }
    
    public function process_title_to_make_url_ref( $title, &$last_segment ){
        
        $title = trim($title);
        
        $search = array( " ", "@", "%", "&", "*", "(", ")", "!", "^", "#","_","+","'",'"',",","." );
        $url_ref = str_replace( $search, "-", $title);
        $url_ref = str_replace( array("--", "---", "----", "-----", "------" ,"-------", "--------", "----------"), "-", $url_ref);
        
        $url_ref = ltrim($url_ref, "-");
        $url_ref = rtrim($url_ref, "-");
        
        $url_ref = explode( "-", $url_ref );
        
        $url_last_segment = end($url_ref); 
        
        $last_segment = is_numeric( $url_last_segment ) ? $url_last_segment : ""; 
        
        if( is_numeric($url_last_segment) ) array_pop ( $url_ref );
        
        return  rtrim( implode("-", $url_ref) , "-" );
    }
    
}

