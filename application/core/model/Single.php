<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SingleList
 *
 * @author Saif
 */
class Single extends APP_Crud {

    //put your code here
    public  $date_added,
        $date_edited,
        $unique_column = "content_id",
        $hash,
        $id;

    public function __construct() {
        parent::__construct();
        $this->set_formated_date();
        $this->hash = _unique_hash();
        $unique_column = $this->unique_column;
        $this->id = $this->has_prop( $unique_column ) ? $this->$unique_column : 0;
    }

    protected function has_prop($prop_name){
        return property_exists($this, $prop_name);
    }

    protected function set_formated_date(){
        $this->date_added = $this->has_prop("added_date") ? format_date($this->added_date) : "";
        $this->date_edited = $this->has_prop("edited_date") ? format_date($this->edited_date) : "";
    }


    public function title( ){
        return property_exists( $this, "quest_title" ) ? $this->quest_title : "";
    }

    public function answer( ){
        return property_exists($this, "answer")? $this->answer : "";
    }

    public function id( ){
        return $this->id;
    }

    private function _get_categories($type = ""){
        $this->set_table("class");

        $option['type'] = $type;
        $option['id'] = $this->id;

        $this->set_function_data( $option );

        $data = $this->fetch_data(  function($db, $config){
            $db->join('class_relation', "class.class_id = class_relation.class_id");

            $db->where( array( 'class_relation.entity_id' => $config[ 'id' ] , 'class_relation.type' => $config[ 'type' ]  ) );
        });

        return $data;
    }

    public function categories( $type = "", $get = "class_id") {
        $data =  $this->_get_categories($type);
        $result = array();
        if( count($data) ){
            foreach ($data as $DT){
                $result[] = $DT[$get];
            }
        }
        return $result;
    }

    public function id_hash( ){
        return md5( $this->id() );
    }

    public function is_trashed( ){
        if( property_exists($this, "status" )){
            return $this->status == 0;
        }
    }

}
