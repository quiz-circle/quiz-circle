<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin_controller
 *
 * @author saifb
 */
load_core( APPPATH."core/list-settings" );

class Admin_controller extends CI_Controller {
    
        //put your code here
    private $date_format = "d,M Y h:i:d";
    protected $_isLoggedIn = false;


    public function __construct( $login_redirection = true ) {
        date_default_timezone_set("Asia/Dhaka");
        
        parent::__construct();
        
        $this->load->model(array("authantication", "validatation_config", "users", "sec_view", "admin"));
        
        $login_not_required = array("activate_user");
        
        $page_name = $this->uri->segment(2);

        $this->building_image_dir = base_url("uploads/buildings/");
        
        $back_to = "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];

        
        $this->menu();
        
        $this->data['stylesheet'] = false;
        
        $this->data['auth'] = $this->authantication;
        $this->data['user'] = $this->users;
        //$this->data['menu'] = $this->menu;
        
        $this->data['site_name'] = "Quiz Circle<small style='font-size: 12px'> (admin)</small>";
        $this->user_type = $this->authantication->getUserType();
        
        
        $this->data['sdata'] = $this->sdata = $this->authantication->get_data();
        
        $this->data['user_type'] = $this->user_type = $this->authantication->getUserType();
        
        if(!in_array($page_name, $login_not_required)){
            
            if( !$this->authantication->isLoggedIn() ) {
                if($this->uri->segment(3) !== "login"){
                    if($login_redirection) {
                        redirect(base_url("admin/user/login/?next=" . urlencode($back_to)));
                    }else {
                        $this->_isLoggedIn = false;
                    }
                }
            }else {
                $this->_isLoggedIn = true;
                $this->permissionSet = $this->data['permissions'] = $this->authantication->getPermissionSet();
                if($this->uri->segment(3) !== "login"){
                    if( $this->user_type === "student" || empty($this->user_type) || is_numeric($this->user_type) ){
                        exit("<h1>Access Forbidden!</h1> <a href='" . base_url("admin/user/login/?ref=_fuu_") . "'>Login With an administrative account!</a>");
                        //ffu = from unauthorized user
                    }
                }
            }   
        }
    }
    
    
    function format_date($date, $format = "") {
        $format = $format === "" ? $this->date_format : $format;
        $date = date_create($date);
        return date_format($date, $format);
    }
    
    protected function set_message($message){
        $this->data['message'] = $message;
    }


    public function user_type(){
        return $this->user_type;
    }
    
    private function menu( ){
        $data =& data();
        $menu  =& $data['menu'];
        
        $menu =array(
            'Dashboard'  => "admin/home",  
        );
        
        $menu["Category"] = array(
            "admin/classes/get_list/category",
            "List"      => "admin/classes/get_list/category",
            "Tag"   => "admin/classes/get_list/tag",
        );

//        $menu["Navigation"] = "admin/navigation/get_list/";

        $menu["Quiz"] = array(
            "admin/content/view/quiz",
            "Show List"      => "admin/content/view/quiz",
            "Add New"   => "admin/quiz/add",
            "Questions"   => "admin/content/view/question",
        );
        
        
//        $menu["Tutorial"] = array(
//            "admin/tutorial/get_list",
//            "Show List"      => "admin/content/view/tutorial",
//            "Add New"   => "admin/content/view/paragraph",
//            "Paragraph"      => "admin/content/view/paragraph",
//            "Video"      => "admin/content/view/video",
//        );
        
        $menu["Notice"] = "admin/content/view/notice";
        
        switch( $this->authantication->getUserType() ){
            case "admin" :
                $menu["User"] = array(
                    "admin/user/profile",
                    "Profile"      => "admin/user/profile",
                    "Create User"   => "admin/user/create",
                );
                
                break;
            case "teacher" :
                $menu['Profile'] = "admin/user/profile";
                break;
        }
    }
    
    public function access_forbidden(){
        $this->load->view('admin/admin-access-forbidden', $this->data());
    }
    
    

}
