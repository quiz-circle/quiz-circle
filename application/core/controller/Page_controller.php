<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page_default
 *
 * @author saifb
 */

load_core( APPPATH."core/list-settings" );

class Page_controller extends CI_Controller {
    //put your code here
    private $date_format = "d,M Y h:ia";
    protected $_isLoggedIn = false, $_sdata, $_auth;

    public function __construct( ) {
        $data =& data( );
        date_default_timezone_set("Asia/Dhaka" );

        parent::__construct();

        $this->set_menu_content();

        $this->load->model(array("authantication"));

        $data['_isLoggedIn'] = $this->_isLoggedIn =  $this->authantication->isLoggedIn();
        $data['sdata'] = $this->_sdata = $this->authantication->get_data();
        $data['auth'] = $this->_auth = $this->authantication;

        // echo "Alhamdulillah.. It's working...?";

    }



    protected function _set_social_links(){

        $data =& data();
        if( !data("_isLoggedIn") ) {
            //echo "Not Logged IN";
            $this->authantication->social_login(  );
        }

    }

    function format_date($date, $format = ""){
        $format = $format === "" ? $this->date_format : $format;
        $date = date_create($date);
        return date_format($date, $format);
    }
    
    
    private function set_menu_content(){
//        $this->load->model("nav");
//        $this->data['_the_menu_content_'] = $this->nav->get_data(function($nav){
//            $nav->where("status", 1);
//            $nav->order_by('position');
//        });
        return false;
    }
    
    public function error_404() {
        $this->_public->view("errors/404.php");
    }
    
}
