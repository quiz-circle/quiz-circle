<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of building_list
 *
 * @author Saif
 */

class quiz_list extends Single {
    public  $date_added,
            $date_edited;

    private $added_date_format,
            $edited_date_format;


    public function __construct() {
        parent::__construct();
        $this->set_date();
    }
    
    private function set_date() {
        if(property_exists( $this, "added_date") ){
            $this->date_added = parent::date( $this->added_date, $this->added_date_format );
        }
        
        if(property_exists( $this, "edited_date") ){
            $this->date_edited = parent::date( $this->edited_date, $this->edited_date_format );
        }
    }
    
    public function added_date( $format = "") {
        $this->added_date_format = $format;
        $this->set_date();
        return $this->date_added;
    }
    
    public function edited_date( $format = "") {
        $this->edited_date_format = $format;
        $this->set_date();
        return $this->date_edited;
    }
    
    
}