<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Content_list
 *
 * @author saifuddin
 */

class Tree extends CI_Model {
    //put your code here
    
    private $_list_array, $_array, $active_data , $child_column, $id_column, 
            $_result, $each_item_view, $ol_list, $list_attrs, $attrs_all_level, 
            $plain_list, $return_data = false, $each_function = false, $array_index = false;
            
            
    public function has_child($id){
        foreach ($this->_array as $item){
            if( $item[$this->child_column] == $id ){
                return true;
            }
        }
        return false;
    }
   
    
    
    protected  function make_list_tree($root = 0, $level = 0) {
        $attrs = $this->_get_attributes($level);
        
        $level++;
        $index = 0;
        $func = $this->_per_elem_operation;
        if ($this->_total_elem && $func) {
            
            $inside_tag = "";
            if ($this->_print_tag_in_tree && $root != 0){
                $this->_result .= $this->tag_start !== ""  ?   $this->tag_start." " . $attrs . " >":"";
            }
            
            $html_list = false;
            
            //Explain($this->_data, "export");
            foreach ( $this->_data as $key => $val ) {
                $unique_class = _unique_id();
                if (array_key_exists($this->_tree_params[0], $val)) {
                    if (count($this->_tree_params) == 2) {
                        if ($val[$this->_tree_params[1]] == $root) {
                            if($this->li_tag_inside){
                                $this->_result .= $this->set_active_li($val, $html_list, $unique_class);
                            }
                            $this->_result .= $func($val, $this->active_param, $level, $index, $this);
                            if ($this->hasChild($val[$this->_tree_params[0]])) {
                                $this->make_list_tree($val[$this->_tree_params[0]], $level);
                            }
                            if($this->li_tag_inside){
                                $this->_result .= ($html_list) ? $this->in_tag_end : "";
                            }
                        }
                    }
                }
                $index++;
            }
            if ($this->_print_tag_in_tree && $root != 0)
                $this->_result .= $this->tag_end;
        }
    }

    private function manage_each_data( $item, $child, $level, $counter ){
        
        
        
        if(array_key_exists($this->id_column, $item) && array_key_exists($this->child_column, $item)){
            if($child == $item[$this->child_column]){
                $data = array('item' => $item, 'child' => $child, 'level' => $level, 'active_data' => $this->active_data, 'counter'=> $counter );
                
                
                if($this->return_data) {
                    $array_index = $this->array_index ? $this->array_index :  $this->id_column;
                    //echo $array_index;
                    if( is_object($this->each_function) || function_exists( $this->each_function ) ){
                        
                        $func = $this->each_function;
                        unset( $data['item'] );
                        if($func != false) {
                            $this->_list_array[ $item[$array_index] ] = $func($item, $data);
                        }
                        
                    }else {
                        //Explain($this->each_function, "dump");
                        $this->_list_array[ $item[$array_index] ] = $data;
                    }    
                }else {
                    if(view_exists( $this->each_item_view ) ){
                        $this->load->view( $this->each_item_view, $data );
                    }
                }
                
                if($this->has_child( $item[$this->id_column] ) ){    
                    $this->manipulate($item[$this->id_column], $level );
                }
            }
        }
        
    }


    private function manipulate( $child = 0, $level = -1 ){
        $level++;
        $this->_result = "";
        $counter = 0;

        foreach($this->_array as $item){
            if( $this->plain_list ){
                $data = array('item' => $item, 'child' => $child, 'level' => $level, 'active_data' => $this->active_data, 'counter' => $counter );
                $this->load->view( $this->each_item_view, $data );

            }else {
                if($this->id_column != "" && $this->child_column != ""){
                    //Explain($this->_array);
                    $this->manage_each_data($item, $child, $level, $counter);
                }
            }
            $counter++;
        }
    }
    
    private function manipulate_plain_list( $child = 0, $level = -1 ){
        $level++;
        if($this->id_column != "" && $this->child_column != ""){
            $this->_result = "";

            foreach($this->_array as $item){
                
                if(array_key_exists($this->id_column, $item) && array_key_exists($this->child_column, $item)){
                    
                    //$this->_result = $item;
                    
                    if($child == $item[$this->child_column]){
                        
                        if(view_exists( $this->each_item_view ) ){
                            $data = array('item' => $item, 'child' => $child, 'level' => $level);
                            $this->load->view( $this->each_item_view, $data );
                        }
                        
                        if($this->has_child( $item[$this->id_column] ) ){    
                            $this->manipulate($item[$this->id_column], $level );
                        }
                    }
                }
            }
        }
    }
    
    public function get_data($result_array, $config = array()){

        $this->_array = $result_array;
        $this->set_config($config);
        
        //$this->active_data;
       
        
        $this->manipulate();
        if($this->return_data) {
            
            return $this->_list_array;
        }
    }
    
    public function get_plain_data($result_array, $config = array()){
        $this->_array = $result_array;
        $this->set_config($config);
        
        //Explain($result_array);
        $this->manipulate_plain_list();
    }
    
    public function view_list_items($result_array, $config = array(), $child = 0, $level = -1){
        $this->_array = $result_array;
        $this->set_config($config);        
        //Explain($result_array);
        $this->manipulate_list_tree();
    }  
    
    private function manipulate_list_tree( $child = 0, $level = -1 ) {
       
        
        $level++;
            
        if( isset($this->list_attrs[$level]) ){
            $attrs = _attributes_to_string( $this->list_attrs[$level] );
        }else {
            $attrs = ($level == 0 || $this->attrs_all_level ) ? _attributes_to_string($this->list_attrs) : "";
        }
        
        if($this->id_column != "" && $this->child_column != ""){
            $this->_result = "";
            
            echo $this->ol_list ? "<ol " . $attrs . ">":"<ul " . $attrs . ">";
            
            foreach($this->_array as $item){
                if(array_key_exists($this->id_column, $item) && array_key_exists($this->child_column, $item)){                 
                    if($child == $item[$this->child_column]){
                        echo "<li >";
                        if(view_exists( $this->each_item_view ) ){
                            $data = array('item' => $item, 'child' => $child, 'level' => $level);
                            
                            include APPPATH . "views/" . $this->each_item_view . ".php";
                            
                            //$this->load->view( $this->each_item_view, $data );
                        }
                        
                        if($this->has_child( $item[$this->id_column] ) ){    
                            $this->manipulate_list_tree($item[$this->id_column], $level );
                        }
                        echo "</li >";
                    }
                }
            }
            echo $this->ol_list ? "</ol>":"</ul>";
        }
    }
    
    private function set_config($config){
        if(is_array($config)){
            foreach($config as $property => $value){
                if(property_exists($this, $property)){
                    $this->$property = $value;
                }
            }
        }
        
    }
    
}
