<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SecView
 *
 * @author Saif
 */
class Sec_view extends CI_Model{
    private $_identifier;
    public function _print($title, $name , $body_reference,  $config = array()){
        $id = md5(uniqid() . rand(0, 50) . microtime());
        $this->_idendifier  = $id;
        $data = isset($config['data']) ? $config['data'] : array();
                
        $data['_hash'] = $id;
        
        
        $section = array();
        $section['relode_btn'] = isset($config['relode_btn']) ? $config['relode_btn']:false;
        $section['collaps_btn'] = isset($config['collaps_btn']) ? $config['collaps_btn']:false;
        $section['print_form'] = isset($config['print_form']) ? $config['print_form']:false;

        $section['name'] = $name;
        $section['hash'] = $id;
        $section['title'] = $title;
        $section['_data'] = $data;
        $section['body'] = $body_reference;
        $section['footer'] = isset($config['footer']) ? $config['footer']:"";
        
        //Explain($section);
        
        $this->admin->view("includes/section", $section);
    }
    
    public function getID(){
        return $this->_idendifier;
    }
}
