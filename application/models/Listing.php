<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Listing
 *
 * @author saifuddin
 */
class Listing extends CI_Model {
    //put your code here
    
    public function set_pagination_html_config(&$config, $attributes = array()){
        $ul_attr = isset($attributes['ul']) ? $attributes['ul'] : array();
        $li_attr = isset($attributes['li']) ? $attributes['li'] : array();
        $first_tag_attr = isset($attributes['first_tag']) ? $attributes['first_tag'] : array();
        $last_tag_attr = isset($attributes['last_tag']) ? $attributes['last_tag'] : array();
        $cur_tag_attr = isset($attributes['cur_tag']) ? $attributes['cur_tag'] : array();
        $next_tag_attr = isset($attributes['next_tag']) ? $attributes['next_tag'] : array();
        $prev_tag_attr = isset($attributes['prev_tag']) ? $attributes['prev_tag'] : array();
        
        $config['full_tag_open'] = '<ul ' . _attributes_to_string($ul_attr) . '>';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li ' . _attributes_to_string($first_tag_attr) . '>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li ' . _attributes_to_string($last_tag_attr) . '>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li ' . _attributes_to_string($next_tag_attr) . '>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li ' . _attributes_to_string($prev_tag_attr) . '>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li ' . _attributes_to_string($cur_tag_attr) . '><span>';
        $config['cur_tag_close'] = '</span></li>';
        $config['num_tag_open'] = '<li ' . _attributes_to_string($li_attr) . '>';
        $config['num_tag_close'] = '</li>';
    }
}
