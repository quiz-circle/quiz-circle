<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmailTheme
 *
 * @author Saif
 */

class Email_operation extends CI_Model{
    
    //put your code here    
    public function __construct() {
        parent::__construct();
        
        $this->mail_from = "saif.bigc@gmail.com";
        $this->mail_error = "";
    }
    
    function send($subject, $message, $email, $from = "" ){      
        $from = ($from === "") ? $this->mail_from : $from;
        
        $config = Array(
            'protocol'  => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_port' => 587,
            'smtp_crypto' => 'TLS',
            'smtp_user' => 'saif.bigc@gmail.com', // change it to yours
            'smtp_pass' => 'Hell0@123', // change it to yours
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1',
            'wordwrap'  => TRUE
        );
       
        
        $this->load->library( 'email', $config  );
        $this->email->set_newline("\r\n");
        $this->email->from($from); // change it to yours
        $this->email->to($email);// change it to yours
        $this->email->subject($subject);
        $this->email->message($message);
        
        if($this->email->send()) {
            return true;
        } else {
            $this->mail_error = $this->email->print_debugger();
            return false;
        }
    }
    
    public function getError(){
        return $this->mail_error;
    }
    
    public function sendCreateUserTheme($name, $link, $email){
        $message = "Hello ".$name;
        $message = "<br>";
        $message = "Here is your activision link. <a href='".$link."' target='_blank'>click here</a> to activate your account.";
        $subject = "Activate your building and flats account" ;
        return $this->send($subject, $message, $email);
    }
    
}
