<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Model{
    function __construct() {
        $this->load->model(array("password", "admin"));
        $this->dir = "/application/views/admin/images/profile-pictures/";
        $this->tableName = 'user';
        $this->primaryKey = 'user_id';
        $this->load->model("authantication");
        if(defined("PASSWORD_ALGORITHM_NUMBER")){
            $this->password->set_pass_algo(PASSWORD_ALGORITHM_NUMBER);
        }
        $this->loginSessionKey = "userData";
        $this->_data = $this->session->userdata( $this->loginSessionKey );
    }
    
    public function getID(){
        return isset( $this->_data['user_id'] ) ? $this->_data['user_id']  : "";
    }
    
    public function get_profile_image($user_id = 0){
        $data = $this->authantication->get_data();
        $user_id = ($user_id == 0) ? $data['user_id'] : $user_id;
        $this->db->select("profile_pic")->from("user")->where( array("user_id" => $user_id, "profile_pic != " => "" ));
        $Query = $this->db->get();
        
        if($Query->num_rows() > 0){
            $data = $Query->row_array();
            
            if(file_exists(APPPATH . "views/admin/images/profile-pictures/" . $data['profile_pic'])){
                return base_url( $this->dir . $data['profile_pic'] );
            }else {
                Explain($data);
                return base_url($this->dir . "default.png");
            }
        }else {
            return base_url($this->dir . "default.png" );
        }
        
    }
    
    public function get_display_name($user_id = 0){
        $data = $this->authantication->get_data();
        $user_id = ($user_id == 0) ? $data['user_id'] : $user_id;
        $this->db->select("display_name")->from("user")->where( array("user_id" => $user_id ));
        $Query = $this->db->get();
        
        if($Query->num_rows() > 0){
            $data = $Query->row_array();
            return !empty($data['display_name']) ? $data['display_name']:"user";
        }
        return "user";
    }
    
    public function checkSocialUser($data = array(), &$user_type_hash ="") {
        $userData = $data;
        $this->db->select( array( $this->primaryKey, "social_user_id", "user_type_hash" ) );
        $this->db->from($this->tableName);
        $this->db->where(array('user_from' => $data['user_from'], 'social_user_id'=>$data['social_user_id']));
        
        $prevQuery = $this->db->get();
        
        $prevCheck = $prevQuery->num_rows();
        
        if($prevCheck > 0){
            
            $prevResult = $prevQuery->row_array();
            $data['modified_date'] = date("Y-m-d H:i:s");
            $update = $this->db->update( $this->tableName, $data, array('social_user_id'=> $prevResult['social_user_id']));
            $user_type_hash = $prevResult['user_type_hash'];
            $userID = $prevResult['user_id'];
            
        }else{
            $user_type_hash = $this->user_type_hash();

            $data['created_date'] = date("Y-m-d H:i:s");
            $data['modified_date'] = date("Y-m-d H:i:s");
            $data['user_type_hash'] = $user_type_hash;
            $insert = $this->db->insert($this->tableName,$data);
            $userID = $this->db->insert_id();

            $this->insert_student_data("", "", $userID, $user_type_hash );
        }
        
        $userData['user_id']  =  $userID;
        return $userID?$userID:FALSE;
    }

    private function user_type_hash(){
        return md5(uniqid() . microtime() . rand(0, 10));
    }

    public function insert_student_data( $student_id, $dob, $user_id, $user_type_hash ){
        if( !empty($user_id) && strlen($user_type_hash) == 32) {
            $this->db->insert("student", array('id' => $student_id, 'date_of_birth' => $dob, 'user_id' => $user_id, 'user_type_hash' => $user_type_hash) );
        }else {
            exit("Sdudent Data not updated!");
        }
    }

    public function check(&$userData){
        $givenPass = $userData['password'];
        //echo $this->password->make("asd012563", $salt ) ." <br>";
        //echo $salt."<br>";

        $this->db->select( array($this->primaryKey, "username", "display_name", "user_id", "email", "user_type_hash", "profile_pic", "salt", "password") );
        $this->db->from($this->tableName);
        
        $this->db->where(array('username' => $userData['username']));
        
        $this->db->or_where('email', $userData['username']);
        
        $prevQuery = $this->db->get();
        $prevCheck = $prevQuery->num_rows();
        if($prevCheck > 0){
            $userData = array();
            $userData = $prevQuery->row_array();
            
            $salt = isset($userData['salt']) && !empty($userData['salt']) ? $userData['salt']:"";
            if( !$this->password->check($givenPass, $userData['password'], $salt ) ){
                $userData = array();
                return false;
            }else {
                unset($userData['password']);
                unset($userData['salt']);
                return true;
            }
            
        }else {
            $userData = array();
            return false;
        }
         
    }


    public function data( $user_id, $key = "" ){
        $user = $this->db->select()->from("user")->where( "user_id", $user_id )->get();
        if( $user->num_rows() ) {
            $user = $user->row_array();
            if( !empty($key) ){
                 return $user[$key];
            }else {
                return $user;
            }
        }
        return "";
    }

    public function has($key, $value, &$data = array() ){
        return count( $data = $this->get_all( "*", array($key => $value) ) );
    }

    public function studentHas($key, $value, &$data = array() ){
        return count( $data = $this->studentData( "*", array($key => $value) ) );
    }


    public function studentData( $columns = "*",  $where = array() ){
        $this->db->select($columns)->from("student");
        if(count($where)) $this->db->where($where);
        $student = $this->db->get();
        if( $student->num_rows() ){
            return $student->result_array();
        }
        return array();
    }

    public function isStudent( $user_id, &$student_data = array() ){
        $user_type_hash = $this->data($user_id, "user_type_hash");

        $student = $this->db->select()->from("student")->where( array("user_id" => $user_id, "user_type_hash" => $user_type_hash) )->get();
        if( $student->num_rows() ){
            $student_data = $student->row_array();
            return true;
        }
        return false;
    }

    public function get_data($user_id, $columns = "*", $col = ""){
        $this->db->select($columns)->from("user"); 
        if($col !== ""){
            //echo $col;
            $this->db->where($col, $user_id);
        }else {
            $this->db->where("user_id", $user_id);
        }
        $prevQuery = $this->db->get();
        return $prevQuery->row_array();
    }
    
    public function get_all($columns = "*", $where = false) {
        $this->db->select($columns)->from("user");
        if($where) $this->db->where($where);
        $prevQuery = $this->db->get();
        return $prevQuery->result_array();
    }
    
    public function get_roles( ){
        return array('admin' =>  "Admin", 'teacher' => 'Teacher');
        
        $this->db->select()->from("user_role");    
        $prevQuery = $this->db->get( );
  
        $userRoles = $prevQuery->result_array();
        $r = array( );
       
        foreach ($userRoles as  $role) {
            $r[$role['role_id']] = $role['type'];
        }
        return $r;
    }
    
    public function get_lang(){
        $u = $this->authantication->get_data();
        $this->db->select("lang")->from("user")->where('user_id', $u['user_id']);    
        $prevQuery = $this->db->get();
        
        $user = $prevQuery->row_array();
        return isset($user['lang']) ? $user['lang']:"";
    }
    
    public function edit_data($user_id, $values){
        
        
        
        if( $this->authantication->getPermission("user", 3) ){
            $admin_type = isset($values['role']) ? $values['role']:false;
            unset($values['role']);
            $this->db->update('user', $values , array('user_id' => $user_id)); 
            $affected_rows = $this->db->affected_rows();
            
            if( $admin_type ){
                $this->db->update("admin", array( 'admin_type' => $admin_type ), array('user_id' => $user_id) );
                $affected_rows = $this->db->affected_rows();
            }
            
            //Explain($this->db->affected_rows(), "dump");
            //return true;
            return $affected_rows;
        }   else {
            exit("<h1>You are not allowed Edit the user ".  anchor(base_url("admin"), "Back")."<h1>");
        }
        return false;
    }
    
    public function upload($file, &$is_multiple = false){
        
        
        $config = array();
        $config['max_size']     = 999999999999;
        $config['allowed_types']= 'gif|jpg|png|jpeg|svg';
        $config['upload_path'] = APPPATH. "views/admin/images/profile-pictures/";
        
        $this->load->library("file_upload", $config);
        $this->file_upload->encrypt_name();
        $this->file_upload->do_upload($file);

        if(isset($_FILES[$file])){
            Explain($this->file_upload->error());
            if(!$this->file_upload->error()){
                //echo $file;
                
                $is_multiple = $this->file_upload->is_multiple();
                return $this->file_upload->data();
            }
        }
        return array();
    }
    
   
    
    
    public function change_profile_pic($pic_file, $user_id = 0){
        $dir = APPPATH. "views/admin/images/profile-pictures/";
        echo $dir;
        //Explain($_FILES);
        
        if($user_id !== 0){
            if(isset($_FILES[$pic_file])){
                
                $this->db->select("profile_pic");
                $this->db->from("user");
                $this->db->where("user_id", $user_id);
                $q = $this->db->get();
                
                
                if($q->num_rows() > 0){
                    $d = $q->row_array();
                    $fname = $d['profile_pic'];
                    if(!empty($fname)){
                        if(file_exists($dir.$fname)){
                            unlink($dir.$fname);
                        }
                    }
        
                    }
               
                
                if($data = $this->upload($pic_file , $is_multiple)){
                    if(!$is_multiple) {
                        $values = array();
                        $values['profile_pic'] = $data['name'];

                        return $this->edit_data( $user_id, $values );
                    }
                }
            }
        }
        return false;
    }
    
    
    public function register($values){
        
        if( $this->authantication->getPermission("user", 1) ){
            if($this->_make_password( $values)){
                if(isset( $values['role'] )) {
                    
                    $role = $values['role'];
                    
                    unset($values['role']);

                    $admin_data = array(
                        'admin_type' => $role,
                        'user_type_hash' => _unique_hash()
                    );

                    $values['user_type_hash'] = $admin_data['user_type_hash']; 

//                    Explain( $values );
//                    Explain( $admin_data );

                    if(   $this->db->insert($this->tableName, $values)){
                        $admin_data['user_id'] = $this->db->insert_id();

                        if($this->db->insert( "admin", $admin_data )){
                            return $admin_data['user_id'];
                        }
                    }
                }
            }
        }else {
            exit("<h1>You are not allowed to create an user ".  anchor(base_url("admin"), "Back")."<h1>");
        }
        return false;
    }

    public function user_register($values){

        if($this->_make_password($values)){

            if($this->db->insert($this->tableName, $values)){
                return $this->db->insert_id();
            }
        }
        return false;
    }

    public function add_student( $values ){
        if($this->db->insert( "student", $values)){
            return $this->db->insert_id();
        }
        return false;
    }
    
    public function change_password($user_id, $values){
        
        if( $this->authantication->getPermission("user", 3) ){
            
            if($this->_make_password($values)){
                
                $this->db->update('user', $values , array('user_id' => $user_id));
                return $this->db->affected_rows() == "1";
                
            }
        }else {
            exit("<h1>You are not allowed to change password ".  anchor(base_url("admin"), "Back")."<h1>");
        }
        return false;
    }
    
    public function create_hash($user_id){
        $hash = substr(md5($user_id . microtime() . rand(0, 100)), 0, 10);
        $this->db->update('user', array("hash" => $hash) , array('user_id' => $user_id));
        if($this->db->affected_rows() == "1"){
            return $hash;
        }
        return false;
    }
    
    private function _make_password(&$values){
        if(isset($values['password']) && !empty($values['password']) ){
            $pass = $this->password->make($values['password'], $salt);
            $values['password'] = $pass;
            $values['salt'] = $salt;
            return true;
        }
        return false;
    }
}