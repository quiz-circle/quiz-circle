<?php

require_once APPPATH . "models/functions/each_objects/question_list.php";
require_once APPPATH . "models/functions/each_objects/test_list.php";
require_once APPPATH . "models/functions/each_objects/psl_each.php";
require_once APPPATH . "views/public/functions/quiz-functions.php";


function get_questions_and_answer( ){
    $quests = _post("questions");
    $question_ids = _post("question_ids");
    $answers = _post("answers");
    $questions_position = 1;
    $questions = array();
    if( count($quests) ){
        foreach( $quests as $key => $quest ) {
            $questions[] = array(
                'question' => $quest,
                'position' => $questions_position,
                'answer' => isset($answers[$key]) ? $answers[$key]:false,
                'quest_id' => isset($question_ids[$key]) ? $question_ids[$key]:false,
            );
            $questions_position++;
        }
    }
    return $questions;
}

function get_options_with_right_answer( ){
    $all_options = _post("opt-value");
    $right_answer = _post("opt-value-right-answer");

    $opt_label = _post("opt-label");

    $answer_options = array( );
    if( count($all_options) ){
        foreach( $all_options as $key => $options ){
            $ra_index = isset($right_answer[$key]) ? $right_answer[$key] : false;
            $answer_options[] = array(
                'option_label' => isset($opt_label[$key]) ? $opt_label[$key] : false,
                'right_answer' => isset($options[$ra_index]) ? $options[$ra_index] : false,
                'options' => $options,
            );
        }
    }
    return $answer_options;
}



function _option_question( &$config, &$where){
    $tbsn = $config['table_short_name'];
    $config['table_name'] = "questions";
    $config['column'] = "u.user_id as user_id, profile_pic, display_name, quest_id as content_id, quest_title as content_title, answer, $tbsn.description as description, added_date, edited_date, $tbsn.status as status";
    $config['content_id_col'] = "quest_id";
    
    if( _post("data_form") == "__ajax__")  {
        if($search = _post("search-text")){
            $search = trim($search);
            $search = strip_tags($search);
            if(!empty($search)){
                $where[ 'quest_title LIKE'] = "%" .$search. "%"; //different <q>.quest_title may be $table_name_short_form + quest_id changed to content_id
            }
        }
    }
    unset( $where[$tbsn . ".content_type"] );
    unset( $where[ $tbsn . '.content_title LIKE'] );
}

function _option_quiz( &$config, &$where){
    
    $tbsn = $config['table_short_name'];
    $config['table_name'] = "quiz";
    $config['column'] = "u.user_id as user_id, profile_pic, display_name, quiz_id as content_id, quiz_title as content_title, $tbsn.quiz_description as description, added_date, edited_date, $tbsn.status as status, full_marks, quiz_type";
    $config['content_id_col'] = "quiz_id";
    
    if($search = _post("search-text")){
        $search = trim($search);
        $search = strip_tags($search);
        if(!empty($search)){
            $where[ $tb_sort_name . '.content_title LIKE'] = "%" .$search. "%"; //different <q>.quest_title may be $table_name_short_form + quest_id changed to content_id
        }
    }
    
    if( _post( "data_form" ) == "__ajax__")  {
        if( $search = _post("search-text") ){
            $search = trim($search);
            $search = strip_tags($search);
            if(!empty($search)){
                $where[ 'quiz_title LIKE'] = "%" .$search. "%"; //different <q>.quest_title may be $table_name_short_form + quest_id changed to content_id
            }
        }
    }
    unset( $where[ $tbsn . ".content_type"] );
    unset( $where[ $tbsn . '.content_title LIKE'] );
}

function _option_tutorial( &$config, &$where){
    $sbsn = $config['table_short_name'];
}

function _option_video( &$config, &$where){
    $sbsn = $config['table_short_name'];
}

function _option_paragraph( &$config, &$where){
    $sbsn = $config['table_short_name'];
}

function _option_notice( &$config, &$where){
    $sbsn = $config['table_short_name'];
}


function content_category_types($type = ""){
    $list = array(
        'quiz' => 'quiz-category',
        'question' => 'question-category',
        'tutorial' => 'tutorial-category',
        'video' => 'video-category',
        'paragraph' => 'paragraph-category',
        'notice' => 'notice-category',
    );
    return isset($list[$type]) ? $list[$type] : $list;
}

function content_category_name($type = ""){
    $list = array(
        'quiz-category' => 'Subject',
        'question-category' => 'Subject',
        'tutorial-category' => 'Subject',
        'video-category' => 'Category',
        'paragraph-category' => 'Subject',
        'notice-category' => 'Department',
    );
    return isset($list[$type]) ? $list[$type] : $list;
}

function prepare_content_list( &$data, &$total = 0, $content_type = "" ) {

    //Explain($_POST);
    $individual_options  = "_option_" . str_replace("-", "_", $content_type);
    $config = array();
    $func_data = array();
    $where = array();

//    $config['column'] = "u.user_id as user_id, profile_pic, display_name, quest_id, quest_title, answer, q.description as description, added_date, edited_date, q.status as status";
    $config['column'] = "*";
    $config['table_short_name'] = $tb_sort_name = "cnt";
    $config['table_name'] = "content";
    $config['content_id_col'] = "content_id";

    $SYS = get_instance(  );
    $DB =& $SYS->db;
    $SYS->load->model( "contents" );

//    Explain( $_POST );

    if( function_exists($individual_options) ) $individual_options( $config, $where );
    $tb_sort_name = $config['table_short_name'];

    $where[ $tb_sort_name. '.status' ]  = 1;
    $config["sort_by"] = "";

    if( _post("data_form") == "__ajax__")  {
        $filter = _post("filter");
        $where[ $tb_sort_name . '.status'] = 1; //different <q>.status may be $table_name_short_form

        if( isset( $filter['status'] ) ){
            $where[ $tb_sort_name . '.status' ] = $filter['status'] == "tr" ? 0:1; //different <q>.status may be $table_name_short_form + quest_id changed to content_id
        }

        if($search = _post("search-text")){
            $search = trim($search);
            $search = strip_tags($search);
            if(!empty($search)){
                $where[ $tb_sort_name . '.content_title LIKE'] = "%" .$search. "%"; //different <q>.quest_title may be $table_name_short_form + quest_id changed to content_id
            }
        }

        if($cat_filter = _post("filter-category")){
            $where['cr.class_id'] = $cat_filter;
        }

        if($sort_by = _post("sort_by")){
            if(!empty($sort_by))
            $config["sort_by"] = $sort_by;
            //_alert( $sort_by );
        }
    }

    $config['column'] = "u.user_id as user_id, profile_pic, display_name, content_id, content_title, answer, $tb_sort_name.description as description, added_date, edited_date, $tb_sort_name.status as status";
    if($content_type) $where[ $tb_sort_name . ".content_type"] = $content_type;
    
    if(function_exists($individual_options)) $individual_options( $config, $where );
    
    $func_data['where'] = $where;
    
    $func_data['table_short_name'] = $config['table_short_name'];
    $func_data['table_name'] = $config['table_name'];
    $func_data['content_id_col'] = $config['content_id_col'];

    $func_data['table_name'] = $config['table_name'];
    $func_data['sort_by'] = $config['sort_by'];

    $SYS->contents->set_function_data( $func_data );

    $config['settings'] =  function($db, $data){
        $where = $data['where'];
        $tsn = isset($data['table_short_name']) ? $data['table_short_name'] : ( isset($data['table_name']) ? $data['table_name'] : "content" ); // tsn table short names
        $content_id_col = isset( $data['content_id_col'] ) ? $data['content_id_col'] : "content_id";

        //Explain($where);

        $db->join("user u", "u.user_id = " . $tsn . ".user_id");
        $db->join("class_relation cr", $tsn . "." .$content_id_col. " = cr.entity_id");
        $db->join("class c", "c.class_id = cr.class_id");

        $db->where( $where );

        if(isset( $data[ 'sort_by' ] ) && !empty( $data[ 'sort_by' ] ) ) {
            $db->order_by( $data[ 'sort_by' ] );
        }else {
            $db->order_by( $tsn . ".added_date DESC" );
        }

        $db->group_by( $tsn . "." .$content_id_col );

    };

    //$SYS->contents->each_obj( "question_list" ) parameter will be different for various content_type
    $SYS->contents->each_obj( "test_list" );

    $data = $SYS->contents->_get_all( $config, -1, 0 );
    $total = $SYS->contents->get_total_num_rows();

//    _alert($DB->last_query());
//    echo $DB->last_query();

    $SYS->contents->reset_columns();
}

function prepare_question_list( &$data, &$total = 0){
    $SYS = get_instance();
    $DB =& $SYS->db;
    $SYS->load->model("_quiz");
    
    $func_data = array();
    $where = array();
    
//    Explain($_POST);
    $where['q.status'] = 1;

    if( _post( "data_form" ) == "__ajax__" ){
        $filter = _post("filter");
        $where['q.status'] = 1;

        if( isset( $filter['status'] ) ){
            $where['q.status'] = $filter['status'] == "tr" ? 0:1;
        }

        if($search = _post("search-text")){
            $search = trim($search);
            $search = strip_tags($search);
            if(!empty($search)){
                $where['q.quest_title LIKE'] = "%" .$search. "%";
            }
        }

        if($cat_filter = _post("filter-category")){
            $where['cr.class_id'] = $cat_filter;
        }
    }

    $func_data['where'] = $where;
    
    $SYS->_quiz->set_function_data($func_data);

    //Explain($where);

//    $SYS->db->join("user u", "u.user_id = q.user_id");
//    $SYS->db->join("class_relation cr", "q.quest_id = cr.entity_id");
//    $SYS->db->join("class c", "c.class_id = cr.class_id");
//    $SYS->db->where( $where );

    $config = array(
        'settings' => function($db, $data){
            $where = $data['where'];

            $db->join("user u", "u.user_id = q.user_id");
            $db->join("class_relation cr", "q.quest_id = cr.entity_id");
            $db->join("class c", "c.class_id = cr.class_id");

            $db->where( $where );

            $db->group_by( "q.quest_id" );
        },
    );

    $config['column'] = "u.user_id as user_id, profile_pic, display_name, quest_id, quest_title, answer, q.description as description, added_date, edited_date, q.status as status";
    $SYS->_quiz->each_obj( "question_list" );

    $data = $SYS->_quiz->_questions( $config, -1, 0 );
    $total = $SYS->_quiz->get_total_num_rows();

    //echo $DB->last_query();
    
    $SYS->_quiz->reset_columns();
}

function participated_student_list( $quiz_id = 0, &$total = 0, $config = array() ){
    $SYS = get_instance( );
    $DB =& $SYS->db;

    $DB->select()->from( "student s" );
    $DB->join( "participation p", "s.student_id = p.student_id" );
    $DB->join( "user u", "u.user_id = s.user_id" );
    $DB->join( "quiz q", "q.quiz_id = p.quiz_id" );
    $DB->order_by( "p.date DESC" );

    if( $quiz_id ) $DB->where("p.quiz_id", $quiz_id);
    if( isset($config['user_id']) ) $DB->where( "u.user_id", $config['user_id'] );

    $DB->order_by( "p.date DESC" );
    $DB->group_by( "p.participation_id" );

    $psl_list = $DB->get();

    if( $total = $psl_list->num_rows() ){
        return $psl_list->result("psl_each");
    }
    return array();
}