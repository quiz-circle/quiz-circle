<?php
/**
 * Created by PhpStorm.
 * User: Saifuddin
 * Date: 12/13/2017
 * Time: 12:54 AM
 */

class question_result_each extends Single {
    public $isRightAnswer = false;
    public function __construct(){
        $this->unique_column = 'answer_id';
        parent::__construct( );

    }


    public function title( ){
        return $this->quest_title;
    }

    public function student_name( ){
        return $this->display_name;
    }

    public function answer_options( &$right_answer = ''){
        $data = json_decode($this->answer_options, true);
        $result = array();

        $right_answer = isset($data['right_answer']) ? $data['right_answer']:'';

        if(isset($data['option_label'])){
            if( count($data['option_label']) ){
                foreach ($data['option_label'] as $index => $label){
                    $result[$label] =  isset($data['options'][$index]) ? $data['options'][$index]: "";
                }
            }
        }
        return $result;
    }

    public function given_answer(){
        return $this->given_answer;
    }

    public function isRight(){
        $this->answer_options($right_answer);
        return md5($this->given_answer) == md5( $right_answer );
    }

}