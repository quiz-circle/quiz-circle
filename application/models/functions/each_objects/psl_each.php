<?php
/**
 * Created by PhpStorm.
 * User: Saifuddin
 * Date: 12/13/2017
 * Time: 12:54 AM
 */

class psl_each extends Single {

     public function __construct(){
        $this->unique_column = 'participation_id';
        parent::__construct( );
        $this->set_formated_date( );
     }

    protected function set_formated_date( ){
        $this->perticipation_date = $this->has_prop("date") ? format_date($this->date) : "";
    }

    public function date( ){
        return $this->perticipation_date;
    }

    public function title( ){
        return $this->quiz_title;
    }

    public function student_name( ){
        return $this->display_name;
    }

    public function full_mark( ){
        return $this->full_marks;
    }

    public function mark_obtained( ){
        return mark_obtained( $this->id );
    }
}