<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_quiz
 *
 * @author Saif
 */
class question_list extends Single {
    //put your code here
    public  $date_added,
            $date_edited,
            $hash,
            $id;
    
    public function __construct() {
        $this->unique_column = 'quest_id';

        parent::__construct();

        $this->set_formated_date();
        //$this->hash = _unique_hash();
        //$this->id = $this->quest_id;
    }
    
//    private function set_formated_date(){
//        $this->date_added = property_exists($this, "added_date")? format_date($this->added_date):"";
//        $this->date_edited = property_exists($this, "edited_date")? format_date($this->edited_date):"";
//    }
//

    public function title(){
        return property_exists( $this, "quest_title" ) ? $this->quest_title : "";
    }
    
    public function answer(){
        return property_exists($this, "answer")? $this->answer : "";
    }

    public function id(){
        return $this->id;
    }

    private function _get_categories($type = ""){
        $this->set_table("class");

        $option['type'] = $type;
        $option['id'] = $this->id;

        $this->set_function_data( $option );

        $data = $this->fetch_data(  function($db, $config){
            $db->join('class_relation', "class.class_id = class_relation.class_id");

            $db->where( array( 'class_relation.entity_id' => $config[ 'id' ] , 'class_relation.type' => $config[ 'type' ]  ) );
        });

        return $data;
    }

    public function categories( $type = "", $get = "class_id"){
        $data =  $this->_get_categories($type);
        $result = array();
        if( count($data) ){
            foreach ($data as $DT){
                $result[] = $DT[$get];
            }
        }
        return $result;
    }


    public function id_hash(){
        return md5( $this->id() );
    }

    public function is_trashed(){
        if( property_exists($this, "status" )){
            return $this->status == 0;
        }
    }
    
}
