<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Path
 *
 * @author Saif
 */
class Path {
    
    //put your code here

    function slide_images_dir($protocol = null){
        return "assets/image-files/slide-images/";
    }
    
    function assets($uri = null, $protocol = null){
        return base_url("assets/".$uri, $protocol);
    }
    
    function slide_images($uri = null, $protocol = null){
        return base_url("assets/image-files/slide-images/".$uri, $protocol);
    }
    
    function image_files($uri = null, $protocol = null){
        return base_url("assets/image-files/".$uri, $protocol);
    }
    
}
