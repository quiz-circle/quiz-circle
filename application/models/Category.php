<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Category
 *
 * @author saifuddin
 */
class Category extends APP_Crud {
    //put your code here
    
    public  $type = "category";
    public  $total = 0;
    public  $data;
    public $_input_values;
    public $message = false;
    
    public function __construct() {
        parent::__construct();
        $this->set_table('class');
        $this->load->library( array('session') );
        //$this->data();
        $this->data =& page_data();
    }
    
    public function has_class($class_name, $type, $parent_id = 0,  $class_id = false){
        $where = array('name' => $class_name, 'parent' => $parent_id, 'type' => $type);
        if($class_id) $where['class_id !='] = $class_id;
        $this->fetch_data( $where );
        return $this->get_total_num_rows();
    }
    
    
    public function update_class_from_input( $action, $type, $class_id = '' ){
        //$this->data['test'] = "Alhamdulillah It's working!";
        if( $this->input->post() ){
            $this->_input_values = $this->input->post();
            
            $name = input_value("class-name");
            $parent = input_value("parent");
            $url_ref = input_value("reference");
            $update = array('parent' => $parent);
            //Explain($parent, "export");
            if( $action == 'edit'){
                
                if(!empty($url_ref)){
                    if(!$this->has_url_reference_with_id($url_ref, $class_id)){
                        $update['url_ref'] = $this->make_url_reference( $url_ref, $class_id );
                    }
                    
                    if( !$this->has_class( $name, $type, $parent, $class_id ) ){
                        $update['name'] = $name;
                    }
                }
                
                $this->set_table('class');
                
                if($parent != $class_id) {
                        if( !$this->has_class( $name, $type,  $parent, $class_id ) ){
                            if($this->_edit_item($update, array('class_id' => $class_id))){
                                $this->session->set_flashdata('_success_message', 'Done!');
                                redirect( base_url( "admin/classes/get_list/".$type."/" ) );
                            } else {
                                $this->data['_warning_message'] = "This <em><b> " .$type. "</b></em> already update or nothing chanded!!";
                            }
                        }else {
                            data_warning_message( ucfirst($type).": <em><b>".$name . "</b></em> already exists with this parent!" );
                        }
                }else {
                    //message: Parent id and class id can't be same!
                    data_danger_message("Parent and child can't be same!!");
                }
                $this->reset_table();
                
            } else {
                if( input_value("class-name") != ""){
                    if(   $this->input->post('reference')  ){
                        $update['url_ref'] = $this->make_url_reference($url_ref);
                    }else {
                        $update['url_ref'] = $this->make_url_reference($name);
                    }
                    $update['name'] = $name; 
                    $update['type'] = $type; 
                    //echo $update['url_ref'];
                    //Explain($update);
                    if( !$this->has_class( $name, $type, $parent ) ){
                        $this->db->insert("class", $update);
                    }else {
                        data_warning_message(ucfirst($type) . ": <em><b>" . $name . "</b></em> already exists with this parent!");
                    }
                }
            }
            
        }else {
            $this->_input_values = "";
            
            if( $action  == "edit" && $class_id ){
                $this->_input_values = $this->get_item($class_id, $type, "name as class-name, url_ref as reference, class_id, parent, description");
               //Explain($this->_input_values);
            }else if( $action == "delete" ){
                if( !$this->has_entity($class_id, $type) ){
                    
                    $this->set_table('class');
                    $this->reduce_childs_parent_id_level($class_id, $type);
                    if($this->_delete($class_id, 'class_id')){
                        $this->session->set_flashdata('_success_message', 'Done!');
                        redirect(base_url( "admin/classes/get_list/".$type."/" ));
                    }
                    $this->reset_table();
                }else {
                    data_warning_message( "This ". ucfirst($type)." has some entities.!");
                }
            }
        }
    }
    
    
    public function has_entity( $class_id, $type = 'category' ){
        $this->set_table('class_relation');
        $data = $this->fetch_data( array('class_id' => $class_id) );
        if( $this->get_total_num_rows() ){
            return $data;
        }
        $this->reset_table();
        return false;
    }
    
    public function has_url_reference( $url_reference ){
        $this->set_table('class', 'url_ref');
        $this->fetch_data( array( 'url_ref' => $url_reference ) );
        $this->reset_table();
        return $this->get_total_num_rows();
    }
    
    public function has_url_reference_with_id( $url_reference, $id ){
        $this->set_table('class', 'url_ref');
        $this->fetch_data( array( 'url_ref' => $url_reference, 'class_id' => $id ) );
        $this->reset_table();
        return $this->get_total_num_rows();
    }
    
    
    public function getList( $config = array(), $get_array = false ){    
        $this->load->model( "tree" );
        $this->set_table( 'class' );

        $class_list = $this->fetch_data( array( "type" => $this->type ) );
        //echo $this->db->last_query();
        if( $this->total = $this->get_total_num_rows()){

            $tree_config = array (
                'child_column' => 'parent',
                'id_column' => 'class_id',

//                'each_item_view' => 'public/includes/class_item',
//                'list_attrs' => array( 'style' => 'padding-left:9px; margin-left:5px' ),
//                'attrs_all_level' => true
            );

            $config = array_merge( $tree_config, $config );



            if(!$get_array){
                $this->tree->get_data( $class_list, $config );
            }else {

                $config['return_data'] = true;
                return $this->tree->get_data( $class_list, $config );        
            }
        }
    }
    
    public function print_related_list( $config = array( 'plain_list' => true ), $entity_id = false, $type = "tag" ) {
        
        $this->load->model("tree");
        $this->set_table('class');
        
        
        if( !isset( $config['result_array'] ) ){
            
            $this->entity_id = $entity_id;
            $this->type = $type;
            
            $class_list = $this->fetch_data( function(){
                $this->db->join("class_relation", "class.class_id = class_relation.class_id");            
                $this->db->where( array( "class.type" => $this->type, 'entity_id' => $this->entity_id, 'class_relation.type' => $this->type ) );
            });
            
        }else {
            $class_list = $config['result_array'];
        }
        
        
        //echo $this->db->last_query();
        
        if( $this->total = $this->get_total_num_rows()){
            $this->tree->get_data( $class_list, $config );
        }
        
    }
    
    public function print_input_tag_list( $type = 'tag' ) {
        if($tag_names = $this->input->post('content-tags')){
            
            $this->_class_type = $type;
            $this->_input_content_tags = $tag_names;
           
            $result_array = $this->fetch_data(function(){
                //_alert( $this->_input_values['content-tags'] );
                
                $this->db->where('type', $this->_class_type );
                $this->db->where_in('class_id', $this->_input_content_tags );
            });
            
            //echo $this->db->last_query();
            $tag_config['each_item_view'] = "admin/includes/each/tag_items";
            $tag_config['result_array'] = $result_array;
            $tag_config['plain_list'] = true;
            
            $this->print_related_list( $tag_config);
            
        }
    }
    
    public function print_input_new_tag_list( ) {
        if( $this->input->post('content-new-tags') ){
            
            $new_tags = $this->input->post('content-new-tags');
            
            $tag_config['each_item_view'] = "admin/includes/each/tags_new_items";
            $tag_config['result_array'] = $new_tags;
            $tag_config['plain_list'] = true;
            
            $this->print_related_list( $tag_config);
        }
    }
    
    public function get_item( $class_id, $type, $column ){
        $this->set_table('class', $column);
        $data = $this->fetch_data(array('class_id' => $class_id, 'type' => $type), true);
        $this->reset_table();
        return  $data;
    }
    
    public function update_class( $content_id, $classes, $type = "category" ){
        
        $this->db->delete("class_relation", array("entity_id" => $content_id) );

        if( is_array( $classes ) ){
            $inserting_classes = array();
            foreach ($classes as $class_id ){
                $inserting_classes[] = array( 'class_id' => $class_id, 'entity_id' => $content_id, 'type' => $type );
            }
            $this->db->insert_batch( "class_relation", $inserting_classes );
        }

    }
    
    public function tree_child_sign($num, $sign = "&nbsp;&nbsp;"){
        if(is_int($num)) {
            for($i = 0; $i < $num; $i++){
                echo $sign;
            }
        }
    }
    
    public function reduce_childs_parent_id_level( $class_id, $type = 'cetegory' ){
        $this->set_table('class', 'parent');
        $_class = $this->fetch_data( array('class_id' => $class_id) ,  true );
        $parent_id = $_class['parent'];
        $child_ids = $this->fetch_data(array('parent' => $class_id, 'type' => $type));
        
        if(count($child_ids) > 0){

            foreach($child_ids as $child){
                //Explain($parent_id, 'export');
                //Explain($child);
                $this->_edit_item( array('parent' => $parent_id), array('class_id' => $child['class_id']) );
                //echo $this->db->last_query();
            }
        }

    }

    public function get_relations( $entity_id, $full_list = false ){
        $relation = $this->db->select('*')->from('class_relation')->where('entity_id', $entity_id)->get();
        if(!$full_list){
            $result = array();
            foreach ($relation->result_array() as $rels){
                $result[] = $rels['class_id'];
            }
            return $result;
        }else {
            return $relation->result_array();
        }
        return array();
    }
    
    
    public function print_data($type = ""){
        
    }
    
    public function edit_data(){
        
    }
    
    public function delete(){
        
    }
    
    public function add( $data , $parent_category = 0, &$message = ""){
        unset($data['parent']);
        unset($data['url_ref']);
        $data['parent'] = $parent_category;
        
        if($this->has_class( $data['name'], $data['type'], $parent_category) ){
            $message = "A " . str_replace(array("-","_"), " ", $data['type']) . " is exists with the name <string></em>" . $data['name'] . "</em></strong>!";
            return false;
        }else {
            if(isset($data['name'])){
                $data['url_ref'] = $this->make_url_reference($data['name']);
            }
            //Explain($data);
            $this->db->insert("class", $data);
            $message = "Data inserted!";
            return $this->db->insert_id();
        }
        
        
    }
    
    public function get_url_numbering( $contents ){
        $numbers = array();
        if(is_array($contents)){
            foreach($contents as $key){
                $key_last_segment = (int) ltrim(substr($key['url_ref'], strrpos($key['url_ref'], "-")), "-");
                //echo $key_last_segment;
                if(is_int($key_last_segment) && $key_last_segment> 0){
                    $numbers[] = $key_last_segment; 
                }
            }
            asort($numbers);
            return $numbers;
        }
        return array();
            
    }
    
    public function make_url_reference( $title, $class_id = false ) {
   
        $url_ref = strtolower($this->process_title_to_make_url_ref( $title, $last_segment ) );
        
        $this->set_table('class', "url_ref");
        $this->class_url_ref = $url_ref ;
        $this->class_id = $class_id ;
        
        $content_data = $this->fetch_data( function(){
            
            $this->db->where( ' left( url_ref , ' .strlen( $this->class_url_ref ). ') LIKE ' ,  $this->class_url_ref ."%"  );
            //$this->db->or_where( ' url_ref =', $this->class_url_ref  );
            if($this->class_id){
                $this->db->where( ' class_id !=', $this->class_id );
            }
            $this->db->order_by("url_ref DESC");
            
        });
        
        //echo ($this->db->last_query());
        //Explain($content_data);
        
        if(count($content_data)){
            $url_numbering = $this->get_url_numbering($content_data);
            if( count($url_numbering) > 0){
                $last_index = end($url_numbering);
                $last_index++;
                return $url_ref . "-" . $last_index;
            }else {
                return  next_string_sequence( $url_ref );
            }
        }else {
            return $url_ref;
        }
    }
}
