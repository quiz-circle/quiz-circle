<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//use Facebook\Facebook;
//use Facebook\Exceptions\FacebookResponseException;
//use Facebook\Exceptions\FacebookSDKException;

class Authantication extends CI_Model{
    private $_isLoggedIn = false, $_data = array();
            
    function __construct() {

        parent::__construct();
        $this->load->model("users");
        
        $this->loginSessionKey = "userData";
        $this->_data = $this->session->userdata( $this->loginSessionKey );
//
//        echo "ok...";
//        Explain( $this->_data );

        if(!empty($this->_data["user_id"]) && !empty($this->_data["user_type_hash"])){
            //echo "ok";
            $this->_isLoggedIn = true;
        }else {
            $this->social_login( );
        }
    }

    public function social_login( ){

        if (_get("code")) {
            switch ( $this->session->userdata("_social_login_reference") ) {
                case "facebook" :
                   $this->fb( );
                    if( $this->session->has_userdata( SESSION_NEXT_PAGE_AFTER_LOGIN ) )
                        redirect( $this->session->userdata( SESSION_NEXT_PAGE_AFTER_LOGIN ) );
                    redirect( "http://".$_SERVER["HTTP_HOST"].$_SERVER["REDIRECT_URL"] );
                    break;

                case "google" :
                    $this->google( );
                    if( $this->session->has_userdata( SESSION_NEXT_PAGE_AFTER_LOGIN ) )
                        redirect( $this->session->userdata( SESSION_NEXT_PAGE_AFTER_LOGIN ) );
                    redirect( "http://".$_SERVER["HTTP_HOST"].$_SERVER["REDIRECT_URL"] );
                    break;
            }

        } else {
            $status = $this->fb();
            $this->google( $status );
        }
    }
    
    public function fb() {

        include_once APPPATH . "libraries/fb/autoload.php";
        // Facebook API Configuration
        $appId = '1231404956897835';
        $appSecret = 'f2636b8177a4baa92ec576a68d186d85';
        //$redirectURL = "http://localhost/quiz-circle/user/login";
        $redirectURL = $this->session->has_userdata(SESSION_NEXT_PAGE_AFTER_LOGIN) ? $this->session->userdata(SESSION_NEXT_PAGE_AFTER_LOGIN) : base_url( );
        //$redirectURL = base_url( );


        $fbPermissions = array('email');


        //Call Facebook API
        $facebook = new Facebook\facebook(array(
            'app_id'  => $appId,
            'app_secret' => $appSecret,
            'default_graph_version' => 'v2.8',
        ));


        // Get redirect login helper
        $helper = $facebook->getRedirectLoginHelper( );

        // Try to get access token
        try {
            
            if(isset($_SESSION['facebook_access_token'])){
                $accessToken = $_SESSION['facebook_access_token'];
            } else{
                $accessToken = $helper->getAccessToken( $redirectURL );
            }
            
        } catch(FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage( );
            exit;
        } catch(FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage( );
            exit;
        }

        if(isset($accessToken)) {
            
            if(isset($_SESSION['facebook_access_token'])){
                $facebook->setDefaultAccessToken($_SESSION['facebook_access_token']);
            }else{
                // Put short-lived access token in session
                $_SESSION['facebook_access_token'] = (string) $accessToken;

                // OAuth 2.0 client handler helps to manage access tokens
                $oAuth2Client = $facebook->getOAuth2Client();

                // Exchanges a short-lived access token for a long-lived one
                $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
                $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;

                // Set default access token to be used in script
                $facebook->setDefaultAccessToken($_SESSION['facebook_access_token']);
            }
            
            
            try {
                $profileRequest = $facebook->get('/me?fields=name,first_name,last_name,email,link,gender,locale,picture');
                $userProfile = $profileRequest->getGraphNode()->asArray();
                
                //Explain($fbUserProfile);
                
                $userData['social_user_id'] = $userProfile['id'];
                $userData['firstname'] = $userProfile['first_name'];
                $userData['lastname'] = $userProfile['last_name'];
                $userData['display_name'] = $userProfile['name'];
                //$userData['email'] = $userProfile['email'];
                
                $userData['gender'] = $userProfile['gender'];
                $userData['profile_image_link'] = $userProfile['picture']['url'];
                $userData['user_from'] = "facebook";
                $userData['active'] = "1";
                
                
                
                Explain( $userData );
                $userID = $this->users->checkSocialUser($userData, $user_type_hash);

                if( !empty($userID) ){

                    $userData['user_id'] = $userID;
                    $userData['user_type_hash'] = $user_type_hash;
                    $this->_data = $data['userData'] = $userData;
                    $this->_isLoggedIn = true;
                    $this->_save_login_session($userData);
                    return "fb_logged_in";

                } else {

                    $this->_data = $data['userData'] = array();
                    $this->session->unset_userdata();
                    session_destroy();

                }
            } catch ( FacebookResponseException $e ) {
                echo 'Graph returned an error: ' . $e->getMessage();
                session_destroy();
                // Redirect user back to app login page
                header("Location: ./");
                exit;
            } catch(FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
            
        } else {
            $loginURL = $helper->getLoginUrl( $redirectURL, $fbPermissions );

            $this->session->set_userdata("_fb_login_url", urlencode($loginURL) );
        }
        return false;
    } 

    public function google($status = ""){
        // Include the google api php libraries
        include_once APPPATH."libraries/google/Google_Client.php";
        include_once APPPATH."libraries/google/contrib/Google_Oauth2Service.php";
        
        // Google Project API Credentials
        $clientId = '30078002396-pik4v921i2cldh5uuuaea1p0iglhh8av.apps.googleusercontent.com';
        $clientSecret = 'Y5nFWbpa48W3iE8hpXjlMnQ5';

        $redirectUrl = $this->session->has_userdata(SESSION_NEXT_PAGE_AFTER_LOGIN) ? $this->session->userdata(SESSION_NEXT_PAGE_AFTER_LOGIN) : base_url( );
        $redirectUrl = base_url( );

        
        // Google Client Configuration
        $gClient = new Google_Client();
        $gClient->setApplicationName('Login to Quiz Circle');
        $gClient->setClientId($clientId);
        $gClient->setClientSecret($clientSecret);
        $gClient->setRedirectUri($redirectUrl);
        $google_oauthV2 = new Google_Oauth2Service($gClient);
        
        if ( isset($_REQUEST['code']) ) {
            $gClient->authenticate();
            $this->session->set_userdata('token', $gClient->getAccessToken());

            if($this->session->has_userdata(SESSION_NEXT_PAGE_AFTER_LOGIN)) {
                $redirectUrl = $this->session->userdata( SESSION_NEXT_PAGE_AFTER_LOGIN );
            }
            redirect($redirectUrl);
        }

        $token = $this->session->userdata('token');
        if (!empty($token)) {
            $gClient->setAccessToken($token);
        }

        if ($gClient->getAccessToken( )) {
            //echo "Ok";
            $userProfile = $google_oauthV2->userinfo->get();
            
            //Explain($userProfile);
            // Preparing data for database insertion

            //$userData['oauth_provider'] = 'google';
            $userData['social_user_id'] = $userProfile['id'];
            $userData['firstname'] = $userProfile['given_name'];
            $userData['lastname'] = $userProfile['family_name'];

            //$userData['email'] = $userProfile['email'];
            $userData['gender'] = isset($userProfile['gender']) ? $userProfile['gender']:"";

            //$userData['gplus_profile_link'] = isset($userProfile['link']) ? $userProfile['link']:"";
            $userData['profile_image_link'] = isset($userProfile['picture']) ? $userProfile['picture']:"";
            $userData['user_from'] = 'google';
            $userData['display_name'] = $userProfile['given_name'] . " " . $userProfile['family_name'];


            // Insert or update user data
            $userID = $this->users->checkSocialUser( $userData , $user_type_hash);


            //Explain( $userData );
            if($status !== "fb_logged_in" ){
                if(!empty($userID)){

                    $userData['user_id'] = $userID;
                    $userData['user_type_hash'] = $user_type_hash;
                    $userData['user_from'] = 'gplus';
                    $userData['social_user_id'] = $userProfile['id'];
                    $this->_data = $data['userData'] = $userData;
                    $this->_isLoggedIn = true;
                    $this->_save_login_session( $userData );

                } else {

                    $this->_data = $data['userData'] = array();
                    $this->session->unset_userdata();
                    session_destroy();

                }
            }

        } else {
            $loginURL = $gClient->createAuthUrl();
            $this->session->set_userdata("_google_login_url", urlencode($loginURL) );
        }

    }
    
    
    public function getUserType($user_id = false, &$user_data = array()){
        
        $sess_uid = isset($this->_data['user_id']) ? $this->_data['user_id']:0;
        $user_id = $user_id === false ? $sess_uid:$user_id;
        
        $this->db->select("user_type_hash")->from("user")->where("user_id", $user_id);
        
        $q = $this->db->get();
        if($q->num_rows() > 0){
            $u = $q->row_array();
            $user_type_hash = $u['user_type_hash'];
            $user_type = '';
            $user = $this->db->select()->from("admin")->where("user_type_hash", $user_type_hash )->get();
            $user_data = array();
            if($user->num_rows()){
                $user_data = $user->row_array();
                $user_type = $user_data['admin_type'];
            }else {
                $user = $this->db->select()->from("student")->where("user_type_hash", $user_type_hash )->get();
                $user_data = $user->row_array();
                $user_type = "student";
            }
            
            if(!empty($user_data)){
                return $user_type;
            }
        }     
        return "";
    }
    
    public function isAdmin($user_id = false){
        return  $this->getUserType($user_id) == "admin" 
                || $this->getUserType($user_id) == "teacher"
                || $this->getUserType($user_id) == "custom";
    }
    
    public function isSupperAdmin($user_id = false){
        return $this->getUserType($user_id) == "admin";
    }
    
    public function getPermissionSet( ){
        //Explain($this->_data);
        if($this->getUserType() !== "student"){
            $perm = "";
            if( $this->getUserType() === "custom" ){
                $p = $this->db->select("permissions")->from("admin")->where("user_id", $this->_data['user_id'])->get();
            }else {
                $type = $this->getUserType();
                $p = $this->db->select("permissions")->from("user_permissions")->where( "type", $type )->get();
            }
            
            if($p->num_rows()){
                $per = $p->row_array();
                return json_decode($per['permissions'], true);
            }
        }
        return array();
    }
    
    public function getPermission($key, $action = false){
        $permission = $this->getPermissionSet();
        
        if($action !== false){
            $group = isset($permission[$key]) ? $permission[$key]:array();
            if(is_array($action)){
                $condition  = array();
                foreach ($action as $act){
                    $condition[] = in_array($act, $group);
                }
                return !in_array(false, $condition);
            }
            return in_array($action, $group);
        }
        return isset($permission[$key]) ? $permission[$key]:array();
    }
    
    
    public function get_data($index = false){
        if($index) return isset($this->_data[$index]) ? $this->_data[$index] : "";
        return $this->_data;
    }

    private function _save_login_session($data){
        $this->session->set_userdata($this->loginSessionKey, $data);
    }

    public function login_user($username, $password, $remember = "") {
        $loginData = array('username' => $username, 'password' => $password);
        
        if($this->users->check($loginData)){
            $this->_save_login_session($loginData);
            return true;
        }else {
            return false;
        }
    }
    
    function isLoggedIn(){
        return $this->_isLoggedIn;
    }

    function logout(){
        session_destroy();
    }
}