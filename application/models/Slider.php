<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Slider
 *
 * @author Saif
 */
class Slider extends CI_Model {
    private  $_meta_data;
    public $meta_key, $image_path;
    
    function initialize(){
        $this->meta_key = "__main-slide-images__";
        
        if(!$this->meta->exists( $this->meta_key )){
            $this->meta->save( $this->meta_key, "" );
        }
        
        $this->_meta_data = $this->meta->get( $this->meta_key );
        
        $this->image_path = $this->path->slide_images_dir();
    }
    
    function get_meta(){
        return $this->_meta_data;
    }
    
    public function delete_image($image){
        if(is_array($image)){
            $result = array();
            foreach ($image as $img){
                if(file_exists($this->image_path . $img)){
                    $result[] = unlink($this->image_path . $img);
                }
            }
            return !in_array(false, $result);
        }else {
            if(file_exists($this->image_path . $image)){
                return unlink($this->image_path . $image);
            }
        }
    }
    
    function image_meta_exists($img_name){

        $meta_array = $this->get_array();
        
        if(is_array($meta_array)){
            foreach ($meta_array as $key => $meta){
                if($meta['img_name'] == $img_name) return $key;
            }
        }
        return false;
    }
    
    function get_array(){
        return json_decode( $this->_meta_data , true );
    }
    
    function save($data){
        $data = is_array($data) ?  json_encode($data):$data;
        return $this->meta->save( $this->meta_key, $data );
    }

    public function upload_image_file($image_file){
        if($d = $this->upload($image_file, $is_multiple)){
            if(!$is_multiple){
                return $d['name'];                    
            }
        }
        return false;
    }
    
    
    public function upload( $image_file, &$is_multiple = false ){
        $config = array();
        $config['max_size']     = 99999999999999999;
        $config['allowed_types']= 'gif|jpg|png|jpeg|svg';
        $config['upload_path'] = $this->image_path;
        
        $this->load->library("file_upload", $config);
        
        $this->file_upload->encrypt_name();
        $this->file_upload->do_upload($image_file);

        if(isset($_FILES[$image_file])){
            if(!$this->file_upload->error()){
                $is_multiple = $this->file_upload->is_multiple();
                return $this->file_upload->data();
            }
        }
        return array();
    }
}
