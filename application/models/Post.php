<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Post
 *
 * @author saifuddin
 */

class Post extends APP_Crud {
    //put your code here
    private $_updating_data;
    public $_input, $title, $body, $content_type;


    public function __construct() {
        parent::__construct();
        $this->_table_name = "contents";
        $this->_primary_key = "id";
    }
    
    public function set_table($table, $column = "*"){
        $this->_table_name = $table;
        $this->_columns = $column;
    }
    
    public function reset_table(){
        $this->_table_name = "contents";
        $this->_columns = " * ";
    }
    
    public function get_data( $more_query_setting = false, $limit = -1, $start = 0) {
        $this->_table_name = "contents";
        
        $data = $this->fetch_data( $more_query_setting, false, $limit, $start );
        
        return $data;
    }
    
    public function get_post($config = array(), $columns = "*", $more = array() ){
        $config['status'] = 1;
        $config['visibility'] = 1;
        $config['content_type'] = isset($more['content_type']) ? $more['content_type'] : "news";
        if(isset($more['sort_by'])) $config['___o_r_d_e_r__b_y___'] = $more['sort_by'];
        return $this->_get_posts($config, $columns, true);
    }

    public function get_posts($config = array(), $columns = "*", $more = array()){
        if(is_array($columns)){
            $more = $columns;
            $columns = isset($more['columns']) ? $more['columns']:"*";
        }
        
        if(isset($more['sort_by'])) $config['___o_r_d_e_r__b_y___'] = $more['sort_by'];
        $limit =  (isset($more['limit'])) ? $more['limit']:-1;
        $start =  (isset($more['start'])) ? $more['start']:0;
        $config['status'] = 1;
        $config['visibility'] = 1;
        
        return $this->_get_posts( $config, $columns, false, $limit, $start );
    }

    private function _get_posts( $config = array(), $column = "*", $first_row = false, $limit = -1, $start = 0 ){
        $where = array();
        if( isset($config['url_ref']) ) $where['contents.url_ref'] = $config['url_ref'];
        if( isset($config['content_type']) ) $where['contents.content_type'] = $config['content_type'];
        if( isset($config['id']) ) $where['contents.id'] = $config['id'];
        if( isset($config['class_url_ref']) ) $where['class.url_ref'] = $config['class_url_ref'];
        if( isset($config['status']) ) $where['contents.status'] = $config['status'];
        if( isset($config['visibility']) ) $where['contents.visibility'] = $config['visibility'];
        $order_by = ( isset($config['___o_r_d_e_r__b_y___']) ) ? $config['___o_r_d_e_r__b_y___']:"";
         
        $this->set_function_data(array( 'where' => $where, 'order_by' => $order_by ));
        
        $this->set_table("contents");            
        $this->set_columns($column);            
        $data = $this->fetch_data( function($db, $config){
            $db->join("user", "user.user_id = contents.author_id");
            $db->join("class_relation", "class_relation.entity_id = contents.id");
            $db->join("class", "class_relation.class_id = class.class_id");
            
            $db->where( $config['where'] );

            $db->group_by("contents.id");
            if(isset($config['order_by'])) $db->order_by( $config['order_by'] );
            //$db->order_by( "class_relation.priority", "contents.added_date DESC" );
        }, $first_row, $limit, $start);
        $this->reset_table();
        $this->set_columns("*");            
        return $data;
    }
    
    public function get( $more_query_setting = false, $limit = -1, $start = 0) {
        $data = $this->fetch_data( $more_query_setting, false, $limit, $start );
        return $data;
    }
    
    public function get_content_by_url_ref($url_ref, $contnet_type = 'news'){
        return $this->fetch_data($where = array( 'url_ref' => $url_ref, 'content_type' => $contnet_type ), true, 0);
    }
    
    public function get_content($contnet_id, $contnet_type = 'news'){
        return $this->fetch_data($where = array( 'id' => $contnet_id, 'content_type' => $contnet_type ), true, 0);
    }
    
    public function get_trash_data( $more_query, $limit = -1, $start = 0 ) {
        return $this->fetch_trash_data($more_query, $limit, $start);
    }
    
    public function make_selected_trash( ){
        //Explain($this->input->post());
        
        if($this->input->post('content_ids')){
            $data['content_ids'] = $this->input->post('content_ids');
            
            echo $this->make_trash(  $data['content_ids']  ) ? "success" : "not success";
            
//            $this->set_function_data($data);
//            $this->_set_status(0, function($db, $data){
//                $this->where_in($this->_table_name.'.id', $data['content_id']);
//            });
        }
    }
    
    private $_others;
    
    public function editData ( $content_id , $content_type = 'news' ){

        $this->_table_name = "contents";
        $this->content_type = $content_type;
        $this->_prepare_value($content_id);
        
        if( $this->input->post() ){
            
            
            $updating_tag_ids = $this->update_new_tags();
            
            $input_tags = $this->input->post("content-tags");
            if( !empty($input_tags) && is_array($input_tags)){
                $updating_tags = array_merge( $updating_tag_ids, $input_tags );
            }else {
                $updating_tags = $updating_tag_ids;
            }
            
            $this->update_class( $updating_tags, $content_id,  "tag" );
            $this->update_class( $this->input->post("categories"), $content_id, "category" );
            
            if( $this->_edit_item( $this->_updating_data, $content_id, $this->_primary_key)){
                
                
                $this->set_news_headline_id($content_id);
                $this->set_input($content_id);
            }
        }
    }
    
    private function set_news_headline_id( $content_id ){
        if( $this->input->post("replace-headline-news") ){
            if($this->input->post("replace-headline-news") == "on"){
                $this->meta->save( HEADLINE_REFERENCE_KEY ,  $content_id );
            }else {
                $this->meta->save( HEADLINE_REFERENCE_KEY ,  "" );
            }
        }else {
            $this->meta->save( HEADLINE_REFERENCE_KEY ,  "" );
        }
    }
    
    public function addItem ( $content_type = 'news'  ){
        $this->content_type = $content_type;
        $this->_prepare_value( );
        if( $this->input->post() ){
            //Explain($this->_updating_data);
            
            if($this->db->insert($this->_table_name, $this->_updating_data )){
                $content_id = $this->db->insert_id();
                $this->update_class($this->input->post("categories"), $content_id);
                
                
                $this->set_news_headline_id($content_id);
                
                $this->update_class( $this->update_new_tags(), $content_id,  "tag" );
                redirect( base_url("admin/posts/edit/" .  $content_type . "/" . $content_id) );
            }
        }
        
    }
    
    private  function update_new_tags( $type = "tag" ){
        $this->load->model("category");
        
        $new_tags = $this->input->post("content-new-tags");
        if(is_array($new_tags)){
            $_class_ids = array();
            
            foreach($new_tags as $name){
                $this->set_table("class");
                
                $data = $this->fetch_data( array("name" => $name, "type" => $type), true );
                
                if(count($data)  == 0) {
                    $this->db->insert( 'class', array(
                            'name' => $name, 
                            'type' => $type, 
                            'url_ref' => $this->category->make_url_reference($name) 
                        )
                    );
                    $_class_ids[] = $this->db->insert_id();
                }else {
                    $_class_ids[] = $data['class_id'];
                }
            }
            return $_class_ids;
        }
        $this->reset_table();
        return array();
        
    }

    private function update_class( $classes, $content_id, $type = "category" ){
        $this->_table_name = "class_relation";
        $this->entity_id = $content_id;
        
        $this->_delete( array("entity_id" => $content_id, 'type' => $type) );
        
        $this->_columns = "class_id";
        //Explain($this->input->post("categories"));
        
        //if( $classes = $this->input->post("categories")) {
        
        if( is_array( $classes ) ){
			if( count($classes)  ) {
				
				$inserting_classes = array();
				foreach ($classes as $class_id ){
					$inserting_class = array( 'class_id' => $class_id, 'entity_id' => $content_id, 'type' => $type );
					$this->db->insert( "class_relation", $inserting_class );
				}
				//Explain( $inserting_classes );
			}
			
        }
        //}
        
        $this->_columns = "*";
        $this->_table_name = "contents";
    }
    
    private function _prepare_value( $content_id = null ){
        $action = $this->uri->segment(3);
        
        if( $this->_input = $this->input->post() ){
            $data = array();
            $this->load->model("users");
            $this->_updating_data["title"] = $this->input->post("content-title");
            $this->_updating_data["body"] = $this->input->post("content-body");
            $this->_updating_data["visibility"] = $this->input->post("visibility") === "visible" ? 1 : 0;
            $this->_updating_data["modified_date"] = $this->input->post("added_date") ? $this->input->post("added_date") : date("Y-m-d H:i:s");

            if($this->content_type == "page") {
                
                $this->_updating_data["page_template"] = $this->input->post("page-template");
            }
            
            if($action == "edit") {
                
                if($this->input->post("url_ref") !== ""){
                    $url_ref = $this->input->post("url_ref");
                    $this->_updating_data["url_ref"] = $this->make_url_reference( $url_ref );
                }
                
            }else if($action == "add"){
                
                $url_ref = $this->input->post("url_ref") ? $this->input->post("url_ref") : $this->input->post("content-title");
                
                $this->_updating_data["url_ref"] = $this->make_url_reference( $url_ref );
                $this->_updating_data["status"] = 1;
                $this->_updating_data["author_id"] = $this->users->getID();
                $this->_updating_data["publish_date"] = $this->input->post("added_date") ? $this->input->post("added_date") : date("Y-m-d H:i:s");
                $this->_updating_data["content_type"] = $this->content_type;
            }           
            
        } else {
            if ($action == "edit" && $this->uri->segment(5) ){
                
                
                
                $this->set_input( $content_id );
                
                //_alert($content_id);
            }else {
                $this->_input['categories'] = array();
            }
        }
    }
    
    private function set_input($content_id){
        $this->_table_name = "class_relation";
        $this->_columns = "class_id";
        $classes = $this->fetch_data( array( 'entity_id' => $content_id, "type" => "category" ) );
        
        $this->_table_name = "contents";
        $this->_columns = "*, title as content-title ";
        $this->_columns .= ", body as content-body ";
        $this->_columns .= ", page_template as page-template ";
        $this->_input = $this->fetch_data( array( $this->_primary_key => $content_id ), true, 0 );
        if( count($classes) ) {
            foreach($classes as $class){
                $this->_input['categories'][] = $class['class_id'];
            }
        }
        
        //Explain($this->_input);
        
        $this->_columns = "*";
        
    }
     
    public function get_input($index){
        return isset( $this->_input[$index] ) ? $this->_input[$index] : "";
    }
    
    private function make_url_reference($title) {
        $title = !empty($title) ? $title : "untitled";  
        $url_ref  = $this->process_title_to_make_url_ref($title, $last_segment);
        
        $this->set_table('contents');
        $this->_contnet_url_ref = $url_ref;
        
        $content_data = $this->fetch_data( function(){
            
            $this->db->where( ' left( url_ref , ' .strlen( $this->_contnet_url_ref ). ') LIKE ' ,  $this->_contnet_url_ref ."%"  );
            $this->db->or_where( ' url_ref =', $this->_contnet_url_ref   );
            $this->db->order_by("url_ref DESC");
            
        });
        
        
//        echo $this->db->last_query();
        
        if( count($content_data) > 0){
            
            foreach($content_data as $content) {
                $last_url_ref = $content['url_ref'];
                $trimed = substr($last_url_ref, strlen( $url_ref ) );
                if ( $trimed_length =  strlen( $trimed )){
                    
                    if( $trimed_length == 2 ) {                        
                        if( is_numeric(ltrim($trimed, "-")) || is_numeric( $end_numeric_title ) ){
                        //if( is_numeric( $end_numeric_title ) ){
                            
                            $sequence_num = (int) ltrim($trimed, "-");
                            //$sequence_num = $end_numeric_title;
                            
                            $url_ref = rtrim($url_ref, $sequence_num);
                            
                            $sequence_num ++;
                            $url_ref = $url_ref."-".$sequence_num;
                            return $url_ref;
//                            _alert( $url_ref );
//                            break;
                        } else {
                            
                        }
                    }
                }else {
                    return  next_string_sequence($last_url_ref);
                }
            }
        }
        return !empty($_last_segment) ?  $url_ref . "-" . $_last_segment : $url_ref;
    }  
    
}
