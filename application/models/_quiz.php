<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Post
 *
 * @author saifuddin
 */

require_once APPPATH . "models/functions/quiz-functions.php";
require_once APPPATH . "models/functions/each_objects/related_question_each.php";

class _quiz extends APP_Crud {
    //put your code here
    private $_updating_data, $_action, $_quest, $_ans_opts;
    public $_input, $title, $body, $quiz_type;

    public function __construct() {
        parent::__construct();
        $this->_quest = get_questions_and_answer();
        $this->_ans_opts = get_options_with_right_answer();

//        /echo json_encode( $array, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES );

        $this->load->model("users");
        $this->_table_name = "quiz";
        $this->_primary_key = "quiz_id";
    }

    /*##########################################*/
    /*--------------- DATA INSERTING -------------*/
    /*##########################################*/

    public function addItem ( ){

        //Explain( $this->_quest );

        $this->_action = _post('_ref') == "_ajax_" ? "add":"";
        $this->_prepare_value( );

        //Explain($this->_updating_data);

        if( $this->input->post() ){
            if(_post('_ref') == "_ajax_") {

                if( $this->db->insert( $this->_table_name, $this->_updating_data ) ) {
                    $quiz_id = $this->db->insert_id();
                    //echo "\nSuccess! quiz_id: " . $content_id ."\n\n";



                    //$this->set_news_headline_id($quiz_id );

                    $this->update_class($this->input->post("categories"), $quiz_id, "quiz-category" );
                    $this->update_class( $this->update_new_tags(), $quiz_id ,  "tag" );

                    $this->update_asigned_question($quiz_id);

                    if(_post('_ref') == "_ajax_") return $quiz_id;
                    //else redirect( base_url("admin/posts/edit/" .  $quiz_type . "/" . $content_id) );
                }
            }
        }
        if( _post('_ref') == "_ajax_" ) return false;
    }

    public function update_asigned_question( $quiz_id ){
        $questions = $this->_quest;

        if( count($questions) ) {

            $q_ids = array();
            $positions = array();
            $pos = 0;

            foreach( $questions as $index => $data ){
                if( !empty($data["question"] && !$data['quest_id']) ){
                    if( $this->update_question( $data, $qid ) ){
                        $this->update_class( array( _post("quest_category")), $qid, "question-category" );

                        $q_ids[] = $qid;
                        $positions[$qid] = $pos;
                        $pos++;
                        $ans_options[$qid] = $this->_ans_opts[$index];
                    }
                } else {
                    //echo "ok\n";
                    $q_ids[] = $data['quest_id'];
                    $ans_options[ $data['quest_id'] ] = $this->_ans_opts[$index];

                    $positions[ $data['quest_id'] ] = $pos;
                    $pos++;
                }
            }
            $this->assign_question_with_quiz( $q_ids, $ans_options, $quiz_id, $positions );
        }
    }

    public function assign_question_with_quiz( $question_ids, $ans_options,  $quiz_id, $positions){
//        Explain( $question_ids );
        $this->_table_name = "quest_assign";
        //Explain( $ans_options );

        $previous_ids = $this->_previous_question_relations( $quiz_id, $question_ids, $deleted_ids );
//
//        echo "QUESTIONS IDS: ";
//        Explain( $question_ids );
//        echo "QUESTIONS IDS; \n\n";
//
//
        //$previous_ids = $this->fetch_data( array( "quiz_id" => $quiz_id ) );

//        echo "PREVIOUS RELATION: ";
//        Explain($previous_ids);
//        echo "PREVIOUS RELATION; \n\n";


        $updating_ids = array_intersect($previous_ids, $question_ids);


//        echo "UPDATING RELATIONS: ";
//        Explain($updating_ids);
//        echo "UPDATING RELATIONS; \n\n";
//
//
//        echo "DELETED IDS: ";
//        Explain($deleted_ids);
//        echo "DELETED IDS; \n\n";

        //Updating data if needed
        if( count($question_ids) ){
            //Explain($question_ids);
            foreach ( $question_ids as $QID ){

                if( !in_array($QID, $updating_ids) ){

                    //echo "INSERTING DATA" . $QID;
                    $data = array(
                        'quiz_id'   => $quiz_id,
                        'quest_id'   => $QID,
                        'answer_options'    =>  json_encode($ans_options[$QID] , JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES),
                        'position' => $positions[$QID],
                        'date' => date_db_formated()
                    );
                    //Explain($data);
                    //echo "===INSERTING DATA===\n";
                    if($QID) $this->db->insert("quest_assign", $data);
                }

            }
        }

        //Insert Data if needed
        if( count($updating_ids) ){
            foreach ( $previous_ids as $QID ){
                //echo "UPDATING DATA";
                $data = array(
                    'answer_options'    =>  json_encode($ans_options[$QID] , JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES),
                    'position' => $positions[$QID]
                );
                //Explain($data);
                //echo "===UPDATING DATA===\n";
                $this->set_table("quest_assign")->_edit_item( $data, array('quest_id' => $QID, 'quiz_id' => $quiz_id) );
            }
        }

        //Delete data if needed
        if( count($deleted_ids) ){
            foreach ( $deleted_ids as $QID ){
                //echo "DELETING DATA";
                    $where = array('quest_id' => $QID, 'quiz_id' => $quiz_id) ;
                    //Explain($where);
                    $this->set_table("quest_assign")->_delete( $where );
                //echo "===DELETING DATA===\n";
            }
        }
    }


    /*##########################################*/
    /*--------------- DATA EDITING -------------*/
    /*##########################################*/
    public function make_selected_trash( ){
        //Explain($this->input->post());

        if($this->input->post('content_ids')){
            $data['content_ids'] = $this->input->post('content_ids');

            echo $this->make_trash(  $data['content_ids']  ) ? "success" : "not success";

//            $this->set_function_data($data);
//            $this->_set_status(0, function($db, $data){
//                $this->where_in($this->_table_name.'.id', $data['content_id']);
//            });
        }
    }

    public function editData ( $content_id ){
        $this->_action = _post('_ref') == "_ajax_" ? "edit":"";

        $this->_table_name = "quiz";
        $this->_prepare_value( $content_id );

        if( $this->input->post() ){
            if(_post('_ref') == "_ajax_") {

                $this->update_asigned_question($content_id);

                $updating_tag_ids = $this->update_new_tags();

                $input_tags = $this->input->post("content-tags");
                if( !empty($input_tags) && is_array($input_tags) ) {
                    $updating_tags = array_merge( $updating_tag_ids, $input_tags );
                }else {
                    $updating_tags = $updating_tag_ids;
                }

                $this->update_class( $updating_tags, $content_id,  "tag" );
                $this->update_class( $this->input->post("categories"), $content_id, "quiz-category" );


                if( $this->set_table('quiz')->_edit_item( $this->_updating_data, $content_id, $this->_primary_key)){
                    $this->set_input( $content_id );
                    if(_post('_ref') == "_ajax_") return $this->_input;
                }
            }else {
                $this->set_input($content_id);
            }
        }
        if(_post('_ref') == "_ajax_") return false;
    }

    private  function update_new_tags( $type = "tag" ){
        $this->load->model("category");

        $new_tags = $this->input->post("content-new-tags");
        if(is_array($new_tags)){
            $_class_ids = array();

            foreach($new_tags as $name){
                $this->set_table("class");

                $data = $this->fetch_data( array("name" => $name, "type" => $type), true );

                if(count($data)  == 0) {
                    $this->db->insert( 'class', array(
                            'name' => $name,
                            'type' => $type,
                            'url_ref' => $this->category->make_url_reference($name)
                        )
                    );
                    $_class_ids[] = $this->db->insert_id();
                }else {
                    $_class_ids[] = $data['class_id'];
                }
            }
            return $_class_ids;
        }
        $this->reset_table();
        return array();

    }

    public function update_question( $data, &$quest_id = 0, $type = "add" ){
        if(isset($data['question'])) {
            $QUESTION['quest_title'] = $data['question'];
            $QUESTION['answer'] = isset($data['answer']) ? $data['answer'] : "";
            $QUESTION['edited_date'] = date_db_formated();

            if($type == "add") {
                $QUESTION['status'] = 1;
                $QUESTION['added_date'] = date_db_formated();
                $QUESTION['user_id'] = $this->users->getID();
                $this->db->insert("questions", $QUESTION);
                $quest_id = $this->db->insert_id();
                return true;
            }else if($type == "edit"){
                if($quest_id !== 0) {
                    $this->db->update("questions", $QUESTION, array("quest_id" => $quest_id));
                    return $this->db->affected_rows() != 0;
                }
            }
        }
        return false;
    }

    public function update_class( $classes, $content_id, $type = "quiz-category" ) {
        $this->_table_name = "class_relation";
        $this->entity_id = $content_id;

        //Explain($this->input->post("categories"));

        $this->_delete( array( "entity_id" => $content_id, 'type' => $type) );

        $this->_columns = "class_id";
        //if( $classes = $this->input->post("categories")) {

        if (is_array($classes)) {
            if (count($classes)) {

                $inserting_classes = array();
                foreach ($classes as $class_id) {
                    $inserting_class = array('class_id' => $class_id, 'entity_id' => $content_id, 'type' => $type);
                    $this->db->insert("class_relation", $inserting_class);

                    $answer_options = "";
                }
                //Explain( $inserting_classes );
            }
        }
        //}

        $this->_columns = "*";
        $this->_table_name = "quiz";
    }


    /*##########################################*/
    /*---------------- OTHERS ------------------*/
    /*##########################################*/

    private function _previous_question_relations( $quiz_id, $question_ids, &$deleted_ids = array() ){
        $this->_table_name = "quest_assign";

        //Finding the existing question assignment
        $previous_ids = $this->fetch_data( array( "quiz_id" => $quiz_id ) );

        $ids = array();

        $this->_table_name = "quiz";

        if(count($previous_ids)) {
            foreach ($previous_ids as $previous_id){
                if( !in_array( $previous_id['quest_id'], $question_ids) ){
                    $deleted_ids[] = $previous_id['quest_id'];
                }else {
                    $ids[] = $previous_id['quest_id'];
                }
            }
            return $ids;
        }else {
            return array();
        }
        $this->_table_name = "quiz";
    }

    private function _prepare_value( $content_id = null ){
        $action = $this->uri->segment(3);

        $action = _post('_ref') == "_ajax_" ? $this->_action : $action;




        if( $this->_input = _post() ){

            $this->_updating_data["quiz_title"] = _post("content-title");
            $this->_updating_data["quiz_description"] = _post("content-description");
            $this->_updating_data["full_marks"] = _post("full_mark");
            $this->_updating_data["quiz_type"] = _post("quiz-type");
            $this->_updating_data["visibility"] = ( _post("visibility") === "visible" ) ? 1 : 0;
            $this->_updating_data["edited_date"] = date("Y-m-d H:i:s");

            if($action == "edit") {
                if( _post("url_ref") !== "" ){
//                    $url_ref = $this->input->post("url_ref");
//                    $this->_updating_data["url_ref"] = $this->make_url_reference( $url_ref );
                }
            }else if($action == "add"){
                //$url_ref = $this->input->post("url_ref") ? $this->input->post("url_ref") : $this->input->post("content-title");
                //$this->_updating_data["url_ref"] = $this->make_url_reference( $url_ref );
                $this->_updating_data["status"] = 1;
                $this->_updating_data["user_id"] = $this->users->getID( );
                $this->_updating_data["added_date"] = date("Y-m-d H:i:s");
            }
        } else {
            if ( $action == "edit" && $this->uri->segment(4) ){
                $this->set_input( $content_id );


                //_alert($content_id);
            }else {
                $this->_input['categories'] = array();
            }
        }

    }

    private function set_news_headline_id( $content_id ){
        if( $this->input->post("replace-headline-news") ){
            if($this->input->post("replace-headline-news") == "on"){
                $this->meta->save( HEADLINE_REFERENCE_KEY ,  $content_id );
            }else {
                $this->meta->save( HEADLINE_REFERENCE_KEY ,  "" );
            }
        }else {
            $this->meta->save( HEADLINE_REFERENCE_KEY ,  "" );
        }
    }

    private function set_input($content_id){
        $this->_table_name = "class_relation";

        $classes = $this->fetch_data( array( 'entity_id' => $content_id, "type" => "quiz-category" ) );

        $this->_table_name = "quiz";
        $this->_columns = "*, quiz_title as content-title ";
        $this->_columns .= ", quiz_description as content-description ";

        $this->_input = $this->fetch_data( array( $this->_primary_key => $content_id ), true, 0 );


        if( count($classes) ) {
            foreach($classes as $class){
                $this->_input['categories'][] = $class['class_id'];
            }
        }

        $this->_columns = "*";

    }

    private function make_url_reference($title) {
        $title = !empty($title) ? $title : "untitled";
        $url_ref  = $this->process_title_to_make_url_ref($title, $last_segment);

        $this->set_table('quiz');
        $this->_contnet_url_ref = $url_ref;

        $content_data = $this->fetch_data( function(){

            $this->db->where( ' left( url_ref , ' .strlen( $this->_contnet_url_ref ). ') LIKE ' ,  $this->_contnet_url_ref ."%"  );
            $this->db->or_where( ' url_ref =', $this->_contnet_url_ref   );
            $this->db->order_by("url_ref DESC");

        });


//        echo $this->db->last_query();

        if( count($content_data) > 0){

            foreach($content_data as $content) {
                $last_url_ref = $content['url_ref'];
                $trimed = substr($last_url_ref, strlen( $url_ref ) );
                if ( $trimed_length =  strlen( $trimed )){

                    if( $trimed_length == 2 ) {
                        if( is_numeric(ltrim($trimed, "-")) || is_numeric( $end_numeric_title ) ){
                            //if( is_numeric( $end_numeric_title ) ){

                            $sequence_num = (int) ltrim($trimed, "-");
                            //$sequence_num = $end_numeric_title;

                            $url_ref = rtrim($url_ref, $sequence_num);

                            $sequence_num ++;
                            $url_ref = $url_ref."-".$sequence_num;
                            return $url_ref;
//                            _alert( $url_ref );
//                            break;
                        } else {

                        }
                    }
                }else {
                    return  next_string_sequence($last_url_ref);
                }
            }
        }
        return !empty($_last_segment) ?  $url_ref . "-" . $_last_segment : $url_ref;
    }

    public function reset_table(){
        $this->_table_name = "quiz";
        $this->_columns = " * ";
    }


    /*##########################################*/
    /*-------------- DATA READING --------------*/
    /*##########################################*/

    public function _question( $config = array(), $limit = -1, $start = 0  ){

        $query_setting = _config( $config, "settings" );
        $where = _config( $config, "where" );
        $order_by = _config( $config, "order_by" );
        $this->set_table("questions");
        $data = $this->fetch_data( $query_setting, true, $limit, $start );
        $this->reset_table();
        return $data;
    }

    public function _questions( $config = array(), $limit = -1, $start = 0  ) {
        $query_setting = _config( $config, "settings" );
        $where = _config( $config, "where" );
        $column = _config( $config, "column" );
        $order_by = _config( $config, "order_by" );

        $this->set_table("questions q");
        $this->set_columns($column);

        $data = $this->fetch_data( $query_setting, false, $limit, $start );
        $this->reset_table();
        return $data;
    }

    public function get_input($index = ""){
        //echo $index;
        if($index === "") {
            return $this->_input;
        } else {
            if($this->_input){
                return array_key_exists($index, $this->_input ) ? $this->_input[$index] : "<" . $index . " not found!>";
            }
            return "";
        }
    }

    public function get_data( $more_query_setting = false, $limit = -1, $start = 0) {
        $this->_table_name = "quiz";
        $data = $this->fetch_data( $more_query_setting, false, $limit, $start );
        return $data;
    }

    public function related_question( $quiz_id ){
        $this->set_table("questions q");
        $config['where'] = array(
            'qa.quiz_id' => $quiz_id,
        );

        $this->each_obj( "related_question_each" );

        $this->set_function_data( $config );

        $data = $this->fetch_data( function($db, $config){
            $where = $config["where"];
            $db->join("quest_assign qa", "qa.quest_id = q.quest_id");
            $db->where( $where );
            $db->order_by("qa.position");
            $db->group_by("qa.id");
        } );
        return $data;
    }

    public function get_post($config = array(), $columns = "*", $more = array() ){
        $config['status'] = 1;
        $config['visibility'] = 1;
        $config['quiz_type'] = isset($more['quiz_type']) ? $more['quiz_type'] : "quiz";
        if(isset($more['sort_by'])) $config['___o_r_d_e_r__b_y___'] = $more['sort_by'];
        return $this->_get_posts($config, $columns, true);
    }

    public function get_posts($config = array(), $columns = "*", $more = array()){
        if(is_array($columns)){
            $more = $columns;
            $columns = isset($more['columns']) ? $more['columns']:"*";
        }

        if(isset($more['sort_by'])) $config['___o_r_d_e_r__b_y___'] = $more['sort_by'];
        $limit =  (isset($more['limit'])) ? $more['limit']:-1;
        $start =  (isset($more['start'])) ? $more['start']:0;
        $config['status'] = 1;
        $config['visibility'] = 1;

        return $this->_get_posts( $config, $columns, false, $limit, $start );
    }

    private function _get_posts( $config = array(), $column = "*", $first_row = false, $limit = -1, $start = 0 ){
        $where = array();
        //if( isset($config['url_ref']) ) $where['quiz.url_ref'] = $config['url_ref'];
        if( isset($config['quiz_type']) ) $where['quiz.quiz_type'] = $config['quiz_type'];
        if( isset($config['id']) ) $where['quiz.quiz_id'] = $config['id'];
        if( isset($config['class_url_ref']) ) $where['class.url_ref'] = $config['class_url_ref'];
        if( isset($config['status']) ) $where['quiz.status'] = $config['status'];
        if( isset($config['visibility']) ) $where['quiz.visibility'] = $config['visibility'];
        $order_by = ( isset($config['___o_r_d_e_r__b_y___']) ) ? $config['___o_r_d_e_r__b_y___']:"";

        $this->set_function_data(array( 'where' => $where, 'order_by' => $order_by ));

        $this->set_table("quiz");
        $this->set_columns($column);
        $data = $this->fetch_data( function( $db, $config ) {
            $db->join("user", "user.user_id = quiz.author_id");
            $db->join("class_relation", "class_relation.entity_id = quiz.quiz_id");
            $db->join("class", "class_relation.class_id = class.class_id");

            $db->where( $config['where'] );

            $db->group_by( "quiz.quiz_id" );
            if(isset($config['order_by'])) $db->order_by( $config['order_by'] );
        }, $first_row, $limit, $start);
        $this->reset_table();
        return $data;
    }

    public function get( $more_query_setting = false, $limit = -1, $start = 0) {
        $data = $this->fetch_data( $more_query_setting, false, $limit, $start );
        return $data;
    }

    public function get_content($contnet_id, $contnet_type = 'news'){
        return $this->fetch_data($where = array( 'id' => $contnet_id, 'content_type' => $contnet_type ), true, 0);
    }

    public function get_trash_data( $more_query, $limit = -1, $start = 0 ) {
        return $this->fetch_trash_data($more_query, $limit, $start);
    }


}
