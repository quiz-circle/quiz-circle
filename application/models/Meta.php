<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Meta
 *
 * @author Saif
 */

class Meta extends CI_Model {
    //put your code here
    
    
    function save($key, $value, $where = array()){
        if($this->exists($key, "", $where)){
            return $this->edit($key, $value, $where);
        }else {
            return $this->add($key, $value, $where);
        }
    }
    
    function add($key, $value, $extra = array()){
        $values = array("meta_key" => $key, "meta_value" => $value);
        $values = array_merge($values, $extra);
        
        if(!$this->exists($key, $value,  $extra)){
            if($this->db->insert("meta", $values)){
                return $this->db->insert_id();
            }
        }
        return false;
    }
    
    function get($key, $where = array()){
        $where["meta_key"] = $key;
        $this->db->select()->from("meta")->where($where);
        $Query = $this->db->get();
        //echo $this->db->last_query();
        $result =  $Query->row_array();
        return $result["meta_value"];
    }
    
    function exists($key, $value = "", $where = array()){
        $where["meta_key"] = $key;
        if($value !== ""){
            $where["meta_value"] = $value;
        }
        $this->db->select()->from("meta")->where($where);
        $Query = $this->db->get();
        return $Query->num_rows() > 0;
    }
    
    function edit($key, $value, $where = array()){
        $values = array("meta_value" => $value);
        
         $where = array_merge($where, array("meta_key" => $key));
        
        $this->db->update("meta", $values , $where);
        //echo $this->db->last_query();
        return $this->db->affected_rows() == "1";
    }
    
    function make_empty($key, $where) {
        return $this->edit($key, "", $where);
    }
    
    function delete($key, $where){
        
    }
}
