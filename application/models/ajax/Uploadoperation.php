<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UploadOperation
 *
 * @author Saif
 */
class UploadOperation extends CI_Model {
    //put your code here
    
    public function change_plan_image(&$result){
        
        $this->load->model("floor_plan");
        $plan_id = $this->input->post("plan_id");
        $action = $this->input->post("action");
        $result['success'] = false;
        $result['name'] = false;
       
        
        $result['file_name'] = $this->floor_plan->upload_image_file("change_plan_image", $plan_id, $action );
        
        if( $result["file_name"] ){
            $result['success'] = true;
        }
    }
    
    public function upload_fp(&$result){
        $fname = $this->input->post("flat_name");
        $plan_id = $this->input->post("plan_id");
        $new_flat_name = $this->input->post("new_flat_name");
        
        $values = array();
        
        $result['file_name'] = $this->floor_plan->upload_image_file( "flat-upload");
        foreach ($this->input->post("image_file_name")  as $index => $file){
            $values[$index] = array(  "file" => $file, "Name" => $fname[$index] ); 
        }
        
        if($result["file_name"]) {
            $values[md5(uniqid() . rand(0, 50) . microtime())] = array("file" => $result['file_name'], "Name" => $new_flat_name);
        }
        
        $result['success'] = $this->floor_plan->edit_plan(array("image_file_name" => json_encode($values)), $plan_id);
        
    }
    
    /*=======================================================================/*
    /*                                                                       /*
    /*                                                                       /*
    /*                      For The Building Functionality                   /*
    /*                                                                       /*
    /*                                                                       /*
    /*=======================================================================*/
    
    public function upload_building(&$result, $building_id = false, $hash = false){
        
        $data = array();
        
        $data['status'] = false;
        
        if($action = $this->input->post("action")){
            //echo $action;
            
            $building_name = $this->input->post("building_name");
            $building_location = $this->input->post("location");
            $user_hash = $this->input->post("user_hash");
            $user_id = $this->input->post("user_id");

            $this->load->model("_building");


            if($action == "add"){
                if($building_id =  $this->_building->add_instance(array(
                    'building_name'     =>  $building_name,
                    'locations'         =>  $building_location,
                    ), $hash)){
                    
                    $this->_building->make_user_relation($user_id, $user_hash, $hash);
                    $data['status'] = true;
                }

            }else if($action == "edit") {
                $building_id = $this->input->post("building_id");
                //echo "{a:'cds'}";

                if($this->_building->edit(array(
                        'building_name'     =>  $building_name,
                        'locations'         =>  $building_location,
                    ), $building_id)){
                    $data['status'] = true;
                }
                 
            }
            $this->_building->upload("fileToUpload", $building_id);
        }
        
        $data['building_id'] = $building_id;
        $data['hash'] = $hash;
        $result = $data;
    }
    
    function upload_building_image(&$result){
        //Explain($_FILES);
        //Explain($_POST);
        
        
        $this->load->model("_building");
        $data = array();
        $data['status'] = false;
        $data['name'] = ""; 
        $data["cordinate_deleted"] = false;
        $image_id = $this->input->post("image_id");
        
        if($file_name = $this->_building->upload_image_file("change-building", $image_id)){
            $data['status'] = true; 
            $data['name'] = $file_name;
            
            if($this->input->post("delete-cordinate") == "on"){
                //echo "cordinate will be deleted!";
                

                $data["cordinate_deleted"] = $this->_building->delete_cordinates(array(
                    "building_id" => $this->input->post("building_id"),
                    "image_id" => $this->input->post("image_id")
                ));
            }
        }
        
        $data['image_id'] = $image_id;
        
        
        
        $result = $data;

    }
    
    function change_price_list_back(&$result){
        //Explain($_FILES);
        //Explain($_POST);
        $this->load->model(array("price_list", "meta"));
        $dir = $this->meta->get("price-list-back-dir");
        $building_id = $this->input->post("building_id");
        
        $fname = $this->price_list->upload_background("change-price-list-back", $building_id);
        
        $result['image_url'] = base_url($dir . "/" . $fname);
        
    }
    
    
}
