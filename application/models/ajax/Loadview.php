<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoadView
 *
 * @author Saif
 */



class LoadView  extends CI_Model { 
    
    function slider_images(){
        $dt = array();
        $this->load->model( array("slider") );
        $dt['building_id'] = $this->input->post("building_id");
        
        $this->slider->initialize();
        $slider_images = $this->slider->get_array();
        
        if(is_array($slider_images)){
            if(count($slider_images)){
                $dt['slider_images'] = $slider_images;
                $dt['image_path'] = $this->path->slide_images();;
                $this->admin->view("includes/slide_show_images", $dt);
            }
        }
    }
    
    function get_classes(){
        $this->load->model("category");
        $class_type = $this->input->post("class_type");
        $content_id = $this->input->post("content_id");
        $list_id = $this->input->post("list_id");
        
        $this->category->type = $class_type;
        
        
        
        $cat_config = array( 'each_item_view' => 'admin/includes/each/class_items', 'active_data' => $this->category->get_relations($content_id)  ); 
        
        echo '<div style="text-align:left">';
        echo '<a hrerf="javascript:" class="class-box-close" onclick="$(\'.class-list-showing-in\').css(\'display\', \'none\')">x</a>';
        echo '<h4 style="margin:0;padding:0; margin-bottom: 15px ">Categories</h4>';
        echo '<input type="hidden" id="content_id' . $list_id . '" value="' . $content_id . '">';
        echo '<div class="content-category-wrap">';
            echo '<ul class="content-category">';    
                $this->category->getList($cat_config);
            echo '</ul>';
        echo '</div>';
        echo '<a href="" class="btn class_assign" id="assign_class_to_news-' . $list_id . '">Done</a>';
        echo '</div>';
        
    }
}
