<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Relation
 *
 * @author Saif
 */
class Relation extends APP_Crud {
    //put your code here
    private $_where;    
    
    public function __construct() {
        parent::__construct();
        $this->load->model("category");
        $this->set_table( _post("table") );
        
    }



    public function add_content( &$result ){
        $class_name = _post("class_name");
        $parent_id = _post("parent_id");
        $type = _post("type");
        $data = array(
            'name' => $class_name,
            'type' => $type
        );
        $result['insert_id'] = false;
        if($result['insert_id'] = $this->category->add($data, $parent_id, $message)){
            $result['success'] = true;
        }else {
            $result['success'] = false;
        }
        $result['message'] = $message;
        //Explain($_POST);
    }
    
    public function get_list(){
        $config_basic = array(
            'child_column' => 'parent', 
            'id_column' => 'class_id',
        );
        
        $this->load->model("tree");
        
        $this->get_post( $_POST );
        
        $where = _post("config");
        
        $selected = array_values( _post("selected") );
        $SELECTED = _post("selected");
        
        //Explain($where);
        
        
        $type = $where['type'];
        $is_tree = class_is_tree($type);
        
        $DT =& data();
        
        $DT['list_config'] = array(
            'each_item_view'    =>  'admin/includes/relation/content-each',
            'plain_list'    => !$is_tree,
            'active_data'    => $selected,
        );
        $DT['list_config'] = array_merge($config_basic, $DT['list_config'] );
        
        if(_post('search-string')){
            $where['name LIKE'] = "%". _post("search-string") . "%";
            $DT['list_config']['plain_list'] = true;
        }
      
        $DT['data'] = $this->fetch_data( $where );
        
        if( $DT['is_tree'] = $is_tree ){  
            $DT['options_config'] = array(
                'each_item_view'    =>  'admin/includes/each/class_item_input_options',
            );
            $DT['options_config'] = array_merge($config_basic, $DT['options_config'] );
            $DT['option_data'] = $this->fetch_data( array('type' => $type) );
        }
//        Explain($DT['option_data']);
//        Explain( $DT['options_config'] );
        admin_view("includes/relation/manage-list");
    }
    
    private function get_post(&$data){
        $post_data = _post();
        foreach($post_data as $key => $dt){
            if(isValidJson($dt)){
                $data[$key] = json_decode( $dt, true );
            }else {
                $data[$key] = $dt;
            }
        }
    }
}
