<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Quiz
 *
 * @author Saif
 */

require_once APPPATH . "models/functions/quiz-functions.php";

class _quiz_ajax extends APP_Crud {
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model("_quiz");
    }
    
    public function set_status( &$result ){
        $question_id = _post("quest_id");
        $status = _post("status");
        $message = "";
        if($status == 0){
            $undo_link = "<a href='javascript:' onclick='set_status(" . $question_id . ", 1)'>Undo</a>";
            $message = "Item Trashed! " . $undo_link;
        }else {
            $message = "Restored";
        }
        $result['success'] = $this->set_table("questions")->_set_status($status, $question_id, "quest_id");
        set_message($result, $message, "Not Success!");
    }

    
    public function delete_parmanently( &$result ){
        //Explain($_POST);
        $deleteing_id = _post("deleting-id");

        $result['success'] = $this->set_table("questions")->_delete( $deleteing_id, "quest_id" );
        set_message($result, "Deleted...!");

    }
    
    public function update_question( &$result, $action = "add"){
        $question = _post("quest-title");
        $answer = _post("quest-answer");
        
        $quest_id = ( $action== "edit" ) ? _post("question_id"):0;
        
        //echo $quest_id;
        if(_post('categories') && is_array(_post())){
            if($this->_quiz->update_question( array('question' => $question, 'answer' => $answer) , $quest_id, $action)){
                $result['success'] = true;
                $result['quest_id'] = $quest_id;
                $this->_quiz->update_class( _post('categories'), $quest_id, "question-category" );
            }
        }else {
            $result['success'] = false;
            $result['message'] = "You must select category!";
        }
        if($action == "add"){
            set_message($result, "Question Added!", "Question Adding Failed!");
        }else {
            set_message($result, "Question Edited!", "Question Editing Failed!");
        }
    }
    
    public function edit_question( &$result ){
        //Explain($_POST);
        $this->update_question($result, "edit");
    }

    public function get_a_question( &$result ){
        
        $id = _post("question_id");
        
        $config['settings'] = array("quest_id" => $id);
        
        $this->_quiz->set_columns("*");
        //$this->_quiz->each_obj("question_list");
        $result = $this->_quiz->_question( $config, 10, 0 );
        $this->_quiz->reset_columns();
    }
    

    public function get_question_list(){

        prepare_question_list($data, $total);
        
        if($total) {
            admin_view( "includes/questions/quest_list.php", array( 'questions' => $data ) ); 
        }else {
            ?>
                <tr>
                    <td colspan="20">No Data Found</td>
                </tr>
            <?php
        }
    }


    private function add_edit_post_item($content_id = null){
        
        $action  = $this->uri->segment(3);
        
        if($action == "add"){
            $this->post->addItem($this->content_type);
        } else if($action == "edit"){
            $this->post->editData($content_id, $this->content_type );
        }

    }

    public function get_related_question(  ){
        $this->load->model("_quiz");
        $quest_id = _post("quiz_id");


        $related_question = $this->_quiz->related_question( $quest_id );

        //Explain( $related_question );
        $data = array( 'questions' => $related_question );

        admin_view( "includes/content-listing/related-question-listing", $data);
    }

    public function update( &$result ){
        //Explain( $_POST );

        $action = _post("action");
        $result['success'] = false;

        if($action == "edit"){
            $content_id = _post("quiz_id");
            if($result['edited'] = $this->_quiz->editData( $content_id )){
                $result['success'] = true;
            }
        }else if($action = "add"){
            if( $result['quiz_id'] = $this->_quiz->addItem( ) ){
                $result['success'] = true;
            }
        }

    }
    
    private function add(){
        
    }
    
    private function edit(){
        
    }
}
