<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of _contents
 *
 * @author Saif
 */



require_once APPPATH . "models/functions/quiz-functions.php";

class _content extends APP_Crud {
    //put your code here
    
    public function set_status( &$result ){
        $content_id = _post("content_id");
        $content_type = _post("content_type");
        $status = _post("status");
        
        $table_name = $this->_get_content_data($content_type, $unique_column);
        
        $message = "";
        if($status == 0){
            $undo_link = "<a href='javascript:' onclick='set_status(" . $content_id . ", 1)'>Undo</a>";
            $message = "Item Trashed! " . $undo_link;
        }else {
            $message = "Restored";
        }
        $result['success'] = $this->set_table( $table_name )->_set_status($status, $content_id, $unique_column);
        set_message($result, $message, "Not Success!");
    }
    
    
    
    public function delete_parmanently( &$result ){
        //Explain($_POST);
        $deleteing_id = _post("deleting-id");
        $content_type = _post("content_type");
        
        $table_name = $this->_get_content_data($content_type, $unique_column);
        
        if($this->before_content_delete($content_type, $table_name, $deleteing_id, $message)){
            $result['success'] = $this->set_table($table_name)->_delete( $deleteing_id, $unique_column );
            set_message($result, "Deleted...!");
        }else {
            $result['success'] = false;
            set_message($result, "Deleting failed...");
        }

    }

    public function before_content_delete($content_type, $table_name, $deleteing_id, &$message){
        switch ( $content_type ){
            default :
                return $this->set_table("class_relation")->_delete( $deleteing_id, "entity_id" );
        }
    }

    private function _get_content_data( $content_type, &$unique_column = "", &$update_data = array(), $action = "add" ){
        
        $table_name = "";
        
        if($action == "add"){
            $update_data['added_date'] = date_db_formated();
            $update_data['edited_date'] = date_db_formated();
            $update_data['user_id'] = $this->users->getID();
            $update_data['status'] = 1;
            $update_data['visibility'] = 1;
        }else if( $action == "edit"){
            $update_data['edited_date'] = date_db_formated();
        }
        
        $table_name = "content";
        $unique_column = "content_id";
        
        switch ($content_type){
            case "question":
                
                $update_data['quest_title'] = _post("name");
                $update_data['answer'] = _post("answer");
                $table_name = "questions";
                $unique_column = "quest_id";
                break;
            
            case "quiz":
                
                $update_data['quiz_title'] = _post("name");
                $table_name = "quiz";
                $unique_column = "quiz_id";
                $update_data['quiz_description'] = _post("content-description");
                $update_data['full_marks'] = _post("full-marks");
                $update_data['quiz_type'] = _post("quiz-type");
                break;
            
            default :
                $update_data['content_type'] = $content_type;
                $update_data['content_title'] = _post("name");
                $update_data['description'] = _post("content-description");
                
        }
        return $table_name;
    }


    public function update( &$result, $action = "add"){
        
        $this->load->model("contents");
        $content_type = _post("content-type");
        $table_name = $this->_get_content_data( $content_type, $unique_column, $update_data, $action );
        
        $content_id = ( $action== "edit" ) ? _post("content_id") : 0;

        
        if( _post('categories') && is_array(_post()) ){
            
            if( $action == "add" ){    
                $this->db->insert($table_name, $update_data);
                $content_id = $this->db->insert_id();
                $result['success'] = $content_id != 0;
            }else if($action == "edit") {
                $this->set_table( $table_name );
                $result['success'] = $this->_edit_item( $update_data,  $content_id, $unique_column );
            }
            
            $result['content_id'] = $content_id;
            
            $this->contents->update_class( _post('categories'), $content_id, content_category_types($content_type) );
           set_message($result, "Success!", "Failed");
        }else {
            $result['success'] = false;
            $result['message'] = "You must select category!";
        }
    }

    public function edit( &$result ){
        //Explain($_POST);
        $this->update($result, "edit");
        
    }
    
    public function get( &$result ){
        
        $this->load->model("contents");
        $id = _post("content_id");
        $content_type = _post("content_type");
        
        $this->set_table( $this->_get_content_data($content_type, $unique_column) );
        $result = $this->fetch_data( array($unique_column => $id), true);
        //echo $this->db->last_query();   
    }
    
    public function question_list_for_quiz( ){
        
        $content_type = "question";
        prepare_content_list($data, $total, $content_type);        
        admin_view( "includes/questions/quietion_list_in_quiz", array( 'questions' => $data, 'content_type' => $content_type ) );
        
    }
    
    public function get_body( ){
        
        $content_type = _post("content_type");
        prepare_content_list($data, $total, $content_type);
        admin_view( "includes/content-listing/content-table-body", array( 'contents' => $data, 'content_type' => $content_type ) );
        
    }
}
