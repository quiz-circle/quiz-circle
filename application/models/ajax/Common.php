<?php
/**
 * Created by PhpStorm.
 * User: Raju Sheikh
 * Date: 11/24/2017
 * Time: 11:08 AM
 */

class Common extends APP_Crud {

    private $_table_name, $_data, $_content_id, $_content_col;

    public function __construct(){
        parent::__construct();
        $this->RESET_GLOBAL_POST();

        $this->_table_name = _post("table_name");
        $this->_data = _post("data");
        $this->_content_id = _post("content_id");
    }

    private function RESET_GLOBAL_POST( ){
        if(isset($_POST)){
            $post_data =& $_POST;
            foreach($post_data as $key => $dt){
                if(isValidJson($dt)){
                    $data[$key] = json_decode( $dt, true );
                }else {
                    $data[$key] = $dt;
                }
            }
        }
    }

    public function add( &$result ){
        if($this->db->insert($this->_table_name, $this->_data)){
            $result['success'] = true;
            $result['insert_id'] = $this->db->insert_id();
        }
        set_message($result, "Success!", "Data insertion failed!");
    }

    public function edit( &$result ){
        $result['success'] = $this->_edit_item($this->_data, $this->_content_id, $this->_content_col );
        set_message($result, "Success!", "Data updating failed!");
    }

    public function delete( &$result ){
        $result['success'] = $this->_delete($this->_content_id, $this->_content_col );
        set_message($result, "Success!", "Failed!");
    }

    public function set_status( &$result ){
        $status = _post('status');
        $result['success'] = $this->_set_status($status, $this->_content_id, $this->_content_col );
        set_message($result, "Success!", "Failed!");
    }


}