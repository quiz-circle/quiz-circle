<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Trash
 *
 * @author Saif
 */
class Trash extends CI_Model{
    //put your code here
    
    public function floor_plan(&$result){
        $this->load->model("floor_plan");
        $r_id = $this->input->post("relation_id");        
        $result['status'] = $this->floor_plan->edit_relation(array("status" => 0),  $r_id);
    }
    
    public function fp_image(&$result){
        
        $result["success"] = false;
        $result["message"] = "";
        if($relation_id = $this->input->post("relation_id")){

            $plans = $this->floor_plan->get_related_plan(false, false, $this->input->post("relation_id"));
            $image_id = $this->input->post("image_id");
            
            $image_files = json_decode($plans['image_file_name'], true);
            
            $file = $image_files[$image_id]["file"];
            
            if( ! $this->floor_plan->exists_plan_image($file) ){
                
                unset($image_files[$image_id]);
                
                $CHANGED_VALUE = json_encode($image_files);

                if($result["success"] = $this->floor_plan->edit_plan(array("image_file_name" => $CHANGED_VALUE),  $plans['plan_id'])){
                    $result["message"] = "Successfully removed mismatched data!";
                }
            }else {
                if(count($image_files) == 1){
                    $result["message"] = "At least one floor plan image must be kept, floor plan can\'t be deleted!";
                }else { 
                    if($this->floor_plan->delete_image_file($file)){
                    //if(true){

                        unset($image_files[$image_id]);
                        $CHANGED_VALUE = json_encode($image_files);
                        //Explain($CHANGED_VALUE);
                        if($result["success"] = $this->floor_plan->edit_plan(array("image_file_name" => $CHANGED_VALUE),  $plans['plan_id'])){
                            $result["message"] = "Successfully removed!";
                        }else {
                            $result["message"] = "Removed but db not updated!";
                        }
                    }else {
                        $result["message"] = "File Not Deleted!";
                    }
                }
            }
        }
    }
    
    /*=======================================================================/*
    /*                                                                       /*
    /*                                                                       /*
    /*                      For The Building Functionality                   /*
    /*                                                                       /*
    /*                                                                       /*
    /*=======================================================================*/
    
    public function building_image(&$result){
        $this->load->model("_building");
        $b_id = $this->input->post("b_id");
        $im_id = $this->input->post("im_id");
        
        $result['status'] = $this->_building->trash_image($b_id, $im_id);
        
        $result['im_id'] = $im_id;
        $result['build_im'] = $b_id;
    }
}
