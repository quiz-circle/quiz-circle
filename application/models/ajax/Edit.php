<?php


class Edit extends CI_Model{
    
    
    private function get_deleted_ids($saved_slider, $input_ids){
        $ids = array();
        $input_ids = (!$input_ids) ? array() : $input_ids;
        if(is_array($saved_slider)) {
            foreach ($saved_slider as $key => $value){
                if(count($input_ids) == 0) {
                    $ids[] = $value['img_name'];
                }else {
                    if(!in_array($key, $input_ids)){
                        $ids[] = $value['img_name'];
                    }
                }
            }
        }
        return $ids;
    }
    
    function slide_images( &$result ){
        $this->load->model("slider");
        $this->slider->initialize();
        $saved_slider = $this->slider->get_array();

        $img_names = $this->input->post("img_name");
        $title = $this->input->post("title");
        $description = $this->input->post("description");
        $img_ids = $this->input->post("img_id");
        $total = count($img_names);
        $slider = array();
        $deleting_image_list = array();
        
        if(is_array($img_names) && is_array($title) && is_array($description)){
            
            for($i = 0; $i < $total; $i ++ ){
                $slider[_unique_hash()] = array(
                    'img_name' => $img_names[$i],
                    'title' => $title[$i],
                    'description' => $description[$i],
                );
                
            }
        }
        
        $this->slider->delete_image( $this->get_deleted_ids($saved_slider, $img_ids) );
        
        //Explain($this->get_deleted_ids($saved_slider, $img_ids));
        $result["success"] = $this->slider->save($slider);
        set_message($result, "Slide show siccessfully edited!.", "Slide show editing failed!");
        
    }
    
}
