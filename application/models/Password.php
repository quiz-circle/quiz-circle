<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Password
 *
 * @author Saif
 */
class Password extends CI_Model{
    //put your code here
    private $_password_algo;

    public function __construct() {
        parent::__construct();
        if(function_exists("password_hash") && function_exists("password_verify")){
            $this->_password_algo = 1;
        }else {
            $this->_password_algo = 2;
        }
    }
    
    public function set_pass_algo($algo){
        $this->_password_algo = $algo;
    }
    
    public function get_pass_algo(){
        return $this->_password_algo;
    }
    
    public function make($password, &$salt = ""){
        switch ($this->_password_algo){
            case 1:
                return password_hash($password, PASSWORD_BCRYPT, array("cost"=> 11));
            case 2:
                return sha($password);
            case 3:
                return md5($password);
            case 4:
                $salt = $this->salt(32);
                return $this->hash_make($password, $salt);
        }
    }

    public function check($password, $submittedPassword, $salt = ""){
        switch ($this->_password_algo){
            case 1:
                return password_verify($password, $submittedPassword);
            case 2:
                return sha($password) == $submittedPassword;
            case 3:
                return md5($password) == $submittedPassword;
            case 4:
                if($salt !== ""){
                    return $submittedPassword == $this->hash_make($password, $salt);
                }
            default :
                return FALSE;
        }
    }
    
    private function hash_make($string, $salt = ""){
        return hash('sha256', $string.$salt);
    }
    
    private function salt($length){
        return str_replace(array('+','/'), array('p','s'), substr(base64_encode(mcrypt_create_iv($length)),0,$length));
    }
}
