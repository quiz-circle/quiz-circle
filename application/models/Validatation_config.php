<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Validatation_config
 *
 * @author Saif
 */
class Validatation_config {
    //put your code here
    


    public function user($action = "create") {
        $config = array(
            array(
                'field' => 'firstname',
                'label' => 'First Name',
                'rules' => 'trim|alpha_numeric_spaces|required|max_length[200]|min_length[2]',
                array('required' => '%s cant be blank!'),
            ),
            array(
                'field' => 'lastname',
                'label' => 'Last Name',
                'rules' => 'trim|required|max_length[200]|min_length[2]',
                array('required' => '%s cant be blank!'),
            ),

            
        );
        
        switch ( $action ){
            case "create" :
                $config[] = array(
                    'field' => 'username',
                    'label' => 'Username',
                    'rules' => 'trim|required|max_length[200]|min_length[2]|callback_white_space',
                    array('required' => '%s cant be blank!'),
                );
                $config[] = array(
                    'field' => 'password',
                    'label' => 'Password',
                    'rules' => 'trim|required|min_length[5]|max_length[100]',
                    array('required' => '%s field can\'t be blank!'),
                );
                
                $config[] = array(
                    'field' => 'password_repeat',
                    'label' => 'Password reapet',
                    'rules' => 'trim|required|min_length[5]|max_length[100]|matches[password]',
                    array('required' => '%s field can\'t be blank!'),
                );  
                $config[] = array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|required|valid_email|is_unique[user.email]',
                    array('is_unique' => '%s in not unique, please try another one!'),
                );
                
                return $config;
                
            case "edit" :
                
                $config[] = array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|required|valid_email',
                    array('is_unique' => '%s in not unique, please try another one!'),
                );
                return $config;
            case "change_pass" :
                $con = array();
                $con[] = array(
                    'field' => 'old_password',
                    'label' => 'Old Password',
                    'rules' => 'trim|required|min_length[5]|max_length[100]',
                    array('required' => '%s field can\'t be blank!'),
                );
                $con[] = array(
                    'field' => 'password',
                    'label' => 'Password',
                    'rules' => 'trim|required|min_length[5]|max_length[100]',
                    array('required' => '%s field can\'t be blank!'),
                );
                $con[] = array(
                    'field' => 'password_repeat',
                    'label' => 'Confirm reapet',
                    'rules' => 'trim|required|min_length[5]|max_length[100]|matches[password]',
                    array('required' => '%s field can\'t be blank!'),
                );  

                return $con;
        }
    }
}
