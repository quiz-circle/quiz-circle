<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Category
 *
 * @author saifuddin
 */
class Nav extends APP_Crud {
    //put your code here


    public function __construct() {
        parent::__construct();
        $this->set_table('navigation');
        $this->load->library( array('session') );

    }

    public function getNavClasses(){
        return $this->set_table("class")->fetch_data( array("type" => "nav-menu") );
    }

    public function has_nav( $nav_title, $parent_id = 0,  $nav_id = false ){

        $where = array('title' => $nav_title, 'parent' => $parent_id );

        if( $nav_id ) $where[ 'nav_id !=' ] = $nav_id;

        $this->fetch_data( $where );

        return $this->get_total_num_rows();
    }


    public function update_class_from_input( $action, $type, $class_id = '' ){
        //$this->data['test'] = "Alhamdulillah It's working!";
        if( $this->input->post() ){
            $this->_input_values = $this->input->post();

        }
    }


    public function getList( $class_id, $config = array(), $get_array = false ){
        $this->load->model( "tree" );
        $this->set_table( "navigation" );

        $class_list = $this->fetch_data( array( "class_id" => $class_id ) );

        //Explain( $class_list );

        //echo $this->db->last_query();

        if( $this->total = count($class_list) ){

            $tree_config = array (
                'child_column' => 'parent',
                'id_column' => 'nav_id',

//                'each_item_view' => 'public/includes/class_item',
//                'list_attrs' => array( 'style' => 'padding-left:9px; margin-left:5px' ),
//                'attrs_all_level' => true
            );

            $config = array_merge( $tree_config, $config );

            if( !$get_array ){
                $this->tree->get_data( $class_list, $config );
            }else {
                $config['return_data'] = true;
                return $this->tree->get_data( $class_list, $config );
            }

        }
    }

    public function print_related_list( $config = array( 'plain_list' => true ), $entity_id = false, $type = "tag" ) {

        $this->load->model("tree");
        $this->set_table('class');


        if( !isset( $config['result_array'] ) ){

            $this->entity_id = $entity_id;
            $this->type = $type;

            $class_list = $this->fetch_data( function(){
                $this->db->where( array( "class.type" => $this->type, 'entity_id' => $this->entity_id, 'class_relation.type' => $this->type ) );
            });

        } else {
            $class_list = $config['result_array'];
        }


        //echo $this->db->last_query();

        if( $this->total = $this->get_total_num_rows()){
            $this->tree->get_data( $class_list, $config );
        }

    }

    public function print_input_tag_list( $type = 'tag' ) {
        if($tag_names = $this->input->post('content-tags')){

            $this->_class_type = $type;
            $this->_input_content_tags = $tag_names;

            $result_array = $this->fetch_data(function(){
                //_alert( $this->_input_values['content-tags'] );

                $this->db->where('type', $this->_class_type );
                $this->db->where_in('class_id', $this->_input_content_tags );
            });

            //echo $this->db->last_query();
            $tag_config['each_item_view'] = "admin/includes/each/tag_items";
            $tag_config['result_array'] = $result_array;
            $tag_config['plain_list'] = true;

            $this->print_related_list( $tag_config);

        }
    }

    public function print_input_new_tag_list( ) {
        if( $this->input->post('content-new-tags') ){

            $new_tags = $this->input->post('content-new-tags');

            $tag_config['each_item_view'] = "admin/includes/each/tags_new_items";
            $tag_config['result_array'] = $new_tags;
            $tag_config['plain_list'] = true;

            $this->print_related_list( $tag_config);
        }
    }

    public function get_item( $class_id, $type, $column ){
        $this->set_table('class', $column);
        $data = $this->fetch_data(array('class_id' => $class_id, 'type' => $type), true);
        $this->reset_table();
        return  $data;
    }

    public function update_class( $content_id, $classes, $type = "category" ){

        $this->db->delete("class_relation", array("entity_id" => $content_id) );

        if( is_array( $classes ) ){
            $inserting_classes = array();
            foreach ($classes as $class_id ){
                $inserting_classes[] = array( 'class_id' => $class_id, 'entity_id' => $content_id, 'type' => $type );
            }
            $this->db->insert_batch( "class_relation", $inserting_classes );
        }

    }

    public function tree_child_sign($num, $sign = "&nbsp;&nbsp;"){
        if(is_int($num)) {
            for($i = 0; $i < $num; $i++){
                echo $sign;
            }
        }
    }

    public function reduce_childs_parent_id_level( $class_id, $type = 'cetegory' ){
        $this->set_table('class', 'parent');
        $_class = $this->fetch_data( array('class_id' => $class_id) ,  true );
        $parent_id = $_class['parent'];
        $child_ids = $this->fetch_data(array('parent' => $class_id, 'type' => $type));

        if(count($child_ids) > 0){

            foreach($child_ids as $child){
                //Explain($parent_id, 'export');
                //Explain($child);
                $this->_edit_item( array('parent' => $parent_id), array('class_id' => $child['class_id']) );
                //echo $this->db->last_query();
            }
        }

    }

    public function get_relations( $entity_id, $full_list = false ){
        $relation = $this->db->select('*')->from('class_relation')->where('entity_id', $entity_id)->get();
        if(!$full_list){
            $result = array();
            foreach ($relation->result_array() as $rels){
                $result[] = $rels['class_id'];
            }
            return $result;
        }else {
            return $relation->result_array();
        }
        return array();
    }


    public function print_data($type = ""){

    }

    public function edit_data(){

    }

    public function delete(){

    }

    public function add( $data , $parent_category = 0, &$message = ""){
        unset($data['parent']);
        unset($data['url_ref']);
        $data['parent'] = $parent_category;

        if($this->has_class( $data['name'], $data['type'], $parent_category) ){
            $message = "A " . str_replace(array("-","_"), " ", $data['type']) . " is exists with the name <string></em>" . $data['name'] . "</em></strong>!";
            return false;
        }else {
            if(isset($data['name'])){
                $data['url_ref'] = $this->make_url_reference($data['name']);
            }
            //Explain($data);
            $this->db->insert("class", $data);
            $message = "Data inserted!";
            return $this->db->insert_id();
        }


    }

    public function get_url_numbering( $contents ){
        $numbers = array();
        if(is_array($contents)){
            foreach($contents as $key){
                $key_last_segment = (int) ltrim(substr($key['url_ref'], strrpos($key['url_ref'], "-")), "-");
                //echo $key_last_segment;
                if(is_int($key_last_segment) && $key_last_segment> 0){
                    $numbers[] = $key_last_segment;
                }
            }
            asort($numbers);
            return $numbers;
        }
        return array();

    }

    public function make_url_reference( $title, $class_id = false ) {

        $url_ref = strtolower($this->process_title_to_make_url_ref( $title, $last_segment ) );

        $this->set_table('class', "url_ref");
        $this->class_url_ref = $url_ref ;
        $this->class_id = $class_id ;

        $content_data = $this->fetch_data( function(){

            $this->db->where( ' left( url_ref , ' .strlen( $this->class_url_ref ). ') LIKE ' ,  $this->class_url_ref ."%"  );
            //$this->db->or_where( ' url_ref =', $this->class_url_ref  );
            if($this->class_id){
                $this->db->where( ' class_id !=', $this->class_id );
            }
            $this->db->order_by("url_ref DESC");

        });

        //echo ($this->db->last_query());
        //Explain($content_data);

        if(count($content_data)){
            $url_numbering = $this->get_url_numbering($content_data);
            if( count($url_numbering) > 0){
                $last_index = end($url_numbering);
                $last_index++;
                return $url_ref . "-" . $last_index;
            }else {
                return  next_string_sequence( $url_ref );
            }
        }else {
            return $url_ref;
        }
    }
}
