<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Theme
 *
 * @author Asadullah
 */
class _public extends CI_Model{
    
    public function file_exists( $path ){
        return file_exists(APPPATH . "views/public/" . $path);
    }
    
    public function dir($path = ""){
        return APPPATH . "views/public/" . $path;
    }
    
    public function getAdminCustomizePath($config = array()){
        return isset($config['file_dir']) ? $config['file_dir']: "functions/customize-admin/";
    }
    
    public function view($referene, $data = array()){
        $this->load->view("public/". $referene, $data);
    }
    
    public function includes($referene, $data = array()){
        $this->load->view( "public/includes/". $referene, $data);
    }
    
    public function header($data = array()){
        $this->load->view("public/includes/header", $data);
    }
    
    public function footer($data = array()){
        $this->load->view("public/includes/footer", $data);
    }
    
    public function base_url($path = ""){
        return base_url("application/views/public/" . $path);
    }
    
    public function images( $path = "" ){
        return $this->base_url( "images/" . $path );
    }
    
    public function js($path = ""){
        return $this->base_url( "js/" . $path );
    }
    
    public function css($path = ""){
        return $this->base_url( "css/" . $path );
    }
    
    public function videos( $path = ""){
        return $this->base_url( "videos/" . $path );
    }
}
