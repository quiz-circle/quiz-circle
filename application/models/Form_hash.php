<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Form_hash
 *
 * @author Saif
 */
class Form_hash extends CI_Model {
    //put your code here
    
    
    public function check($match, $name= "_from_security_hash_"){
        return $this->session->flashdata($name) == $match;
    }
    
    public function get($name = "_from_security_hash_"){
        $hash = md5(uniqid().rand(2, 20).microtime());
        $this->session->set_flashdata($name, $hash);
        return $hash;
    }
    
}
