<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Post
 *
 * @author saifuddin
 */

require_once APPPATH . "models/functions/quiz-functions.php";

class Contents extends APP_Crud {
    //put your code here
    private $_updating_data, $_action, $_quest, $_ans_opts;
    public $_input, $title, $body, $quiz_type;

    
    public function get_question(){
        
    }
    
    public function get_questions(){
        
    }

    public function _get( $config = array() ){
        
        $query_setting = _config( $config, "settings" );
        $where = _config( $config, "where" );
        $order_by = _config( $config, "order_by" );        
        $this->set_table("content");
        $data = $this->fetch_data( $query_setting, true );
        $this->reset_table();
        return $data;
    }
    
    public function _get_all( $config = array(), $limit = -1, $start = 0  ) {
        $query_setting = _config( $config, "settings" );
        $where = _config( $config, "where" );
        $column = _config( $config, "column" );
        $order_by = _config( $config, "order_by" );
        $table_name = _config( $config, "table_name" );
        $table_short_name = _config( $config, "table_short_name" );
        $this->set_table($table_name  . " " . $table_short_name );
        $this->set_columns($column);
        $data = $this->fetch_data( $query_setting, false, $limit, $start );
        $this->reset_table();
        return $data;
    }


    public function update_content( $data, &$quest_id = 0, $type = "add" ){
        if(isset($data['question'])) {
            $QUESTION['content_title'] = $data['question'];
            //$QUESTION['answer'] = isset($data['answer']) ? $data['answer'] : "";
            $QUESTION['edited_date'] = date_db_formated();

            if($type == "add") {
                $QUESTION['status'] = 1;
                $QUESTION['added_date'] = date_db_formated();
                $QUESTION['user_id'] = $this->users->getID();
                $QUESTION['content_type'] = $data['content_type'];
                $this->db->insert("content", $QUESTION);
                $quest_id = $this->db->insert_id();
                return true;
            }else if($type == "edit"){
                if($quest_id !== 0) {
                    $this->db->update("content", $QUESTION, array("content_id" => $quest_id));
                    return $this->db->affected_rows() != 0;
                }
            }
        }
        return false;
    }


    public function update_class( $classes, $content_id, $type = "category" ) {
        $this->_table_name = "class_relation";
        $this->entity_id = $content_id;
        
        //Explain($this->input->post("categories"));

        $this->_delete( array( "entity_id" => $content_id, 'type' => $type) );

        $this->_columns = "class_id";
        //if( $classes = $this->input->post("categories")) {

        if (is_array($classes)) {
            if (count($classes)) {

                $inserting_classes = array();
                foreach ($classes as $class_id) {
                    $inserting_class = array('class_id' => $class_id, 'entity_id' => $content_id, 'type' => $type);
                    $this->db->insert("class_relation", $inserting_class);

                    $answer_options = "";
                }
                //Explain( $inserting_classes );
            }
        }
        //}

        $this->_columns = "*";
        $this->_table_name = "quiz";
    }
}
