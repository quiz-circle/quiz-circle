<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin
 *
 * @author Saif
 */

class Admin extends CI_Model {
    
    
    
    public function access_forbidden(){
        $this->load->view('admin/admin-access-forbidden', $this->data());
    }
    
    
    public function view($referene, $data = array()){
        $this->load->view("admin/". $referene, $data);
    }
    
    public function includes($referene, $data = array()){
        $this->load->view( "admin/includes/". $referene, $data);
    }
    
    public function header($data = array()){
        $this->load->view("admin/includes/header", $data);
    }
    
    public function footer($data = array()){
        $this->load->view("admin/includes/footer", $data);
    }
    
    public function base_url($path = ""){
        return base_url("application/views/admin/" . $path);
    }
    
    public function images( $path = "" ){
        return $this->base_url( "images/" . $path );
    }
    
    public function js($path = ""){
        return $this->base_url( "js/" . $path );
    }
    
    public function css($path = ""){
        return $this->base_url( "css/" . $path );
    }
    
    public function videos( $path = ""){
        return $this->base_url( "videos/" . $path );
    }
}
