<?php $this->admin->header(data()); ?>
<style>
   
</style>
    
<div class="core-section category-list" style="width:220px">
    <div class="section-header">
        <div class="section-title">Tutorial Categories</div>
        
    </div>
    
    <div class="section-body">
        <div class="list-header">
            <input type="text" name="" value="" id="search-string">
        </div>
        <div id="main-panel">
            
        </div>
    </div>
</div>

<div class="core-section quiz-category-list" style="width:220px">
    <div class="section-header">
        <div class="section-title">Quiz Categories</div>

    </div>

    <div class="section-body">
        <div class="list-header">
            <input type="text" name="" value="" id="search-string">
        </div>
        <div id="main-panel">

        </div>
    </div>
</div>

<div class="core-section video-category" style="width:220px">
    <div class="section-header">
        <div class="section-title">Video Categories</div>

    </div>

    <div class="section-body">
        <div class="list-header">
            <input type="text" name="" value="" id="search-string">
        </div>
        <div id="main-panel">

        </div>
    </div>
</div>


    <script>
    



$( ".category-list" ).relation_list({
    table: "class", 
    config: {"type":"tutorial-category" }, 
    "search-string":"",
    "content-name" : "Tutorial Category",
    "select-name" : "tutorial-category",
    "select-id" : "tutorial-category",
    selected : [299,305,306],
});

$( ".quiz-category-list" ).relation_list({
    table: "class", 
    config: {"type":"quiz-category" }, 
    "search-string":"",
    "content-name" : "Quiz Category",
    "select-name" : "quiz-category",
    "select-id" : "quiz-category",
    selected : [299],
});


$( ".video-category" ).relation_list({
    table: "class",
    config: {"type":"video-category" },
    "search-string":"",
    "content-name" : "Video Category",
    "select-name" : "video-category",
    "select-id" : "video-category",
});



    

</script>

<?php $this->admin->footer(data()); ?>