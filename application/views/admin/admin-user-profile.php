<?php  $this->admin->header( data() ); ?>
    <div class="left" id="profile-data" >
        <h2>User Profile</h2>
        
        <div class="data-single">
            <p class="key">Username</p>
            <p class="value"><?php echo $userdata['username'] ?></p>
        </div>
        <div class="data-single">
            <p class="key">First Name</p>
            <p class="value"><?php echo $userdata['firstname'] ?></p>
        </div>
        <div class="data-single">
            <p class="key">Last Name</p>
            <p class="value"><?php echo $userdata['lastname'] ?></p>
        </div>
        <div class="data-single">
            <p class="key">Email</p>
            <p class="value"><?php echo $userdata['email'] ?></p>
        </div>
        
        <?php echo anchor(base_url("admin/user/edit/" . $userdata['user_id']), "Edit",  array("class" => "btn")) ; ?>
        
        <?php  echo  $change_pass_btn ? anchor(base_url("admin/user/change_password/" . $userdata['user_id']),"Change Password", array("class" => "btn")):"" ; ?>
        
    </div>
<?php  $this->admin->footer( data() ); ?>