<?php  $this->admin->header( $data ); ?>



    <div class="left" id="user-form-success" >
        <h2>Users List</h2>
        <table style="border-collapse: collapse" class="content-list">
            <thead>
                <tr>
                    <td>SL No</td>
                    <td>Username</td>
                    <td>First Name</td>
                    <td>Last Name</td>
                    <td>Email</td>
                    <td>Active</td>
                    <td>Role</td>
                    <td>Ip Address</td>
                    <td colspan="3">Action</td>
                </tr>
            </thead>
            <tbody>
                <?php $c = 0; foreach ($allusers as $user): $c++?>
                <tr>
                    <td><?php echo $c;?></td>
                    <td><?php echo $user['username'];?></td>
                    <td><?php echo $user['firstname'];?></td>
                    <td><?php echo $user['lastname'];?></td>
                    <td><?php echo $user['email'];?></td>
                    <td><?php echo $user['active'] == "1" ? "Active":"Inactive";?></td>
                    <td><?php echo $user['role'] == "1" ?  "Admin":($user['role'] == "2" ? "Client":"Custom");?></td>
                    <td><?php echo $user['ip_address'];?></td>
                    <td><?php echo anchor(base_url("admin/user/edit/" . $user['user_id']),"Edit") ;?></td>
                    <td><?php echo anchor(base_url("admin/user/profile/" . $user['user_id']),"View Profile") ;?></td>
                    <td><?php echo anchor(base_url("admin/building/all/".$user['user_id']."/".$user['hash']),"Buildings") ;?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php echo anchor(base_url("admin/create_user"), "+Add a User", array("class" => "btn")) ; ?>
    </div>

<?php  $this->admin->footer( $data ); ?>