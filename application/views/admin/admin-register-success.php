<?php  $this->admin->header( data() ); ?>

<div class="left" id="user-form-success" >
    <h3>Registration successful</h3>
    <?php
        if($email_error){
            echo "<p>Email not sent!</p>";
            echo "<h1>Email Error:</h1>";
            echo $email_error;
        }
    ?>
</div>

<?php  $this->admin->footer( data() ); ?>