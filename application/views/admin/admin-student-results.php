<?php  $this->admin->header( data() );  ?>

<script>
    var content_type = "<?php echo uri_seg(4) ?>";
    var quiz_content_types = <?php echo json_encode( $quiz_content_types ) ?>;
</script>



        <form id="questions-list-form" action="" onsubmit="return false" method="post">
            <div class="content-header">
<!--                <div class="more-filter" style="display: inline-block; float:left">-->
<!---->
<!--                </div>-->
<!--                -->
<!--                <div class="search">-->
<!--                    <input type="text" id="search-text" name="search-text" placeholder="Search..">-->
<!--                </div>-->
<!--                <a href="javascript:" class="btn add_question_open" >Add --><?php //echo uri_seg(4) ?><!--</a>-->
<!--                -->
            </div>

            <?php// Explain($student_result_list); ?>
            <table class="content-list student-result-list">
                <thead >
                    <tr>
                        <td>Student Name</td>
                        <td>Quiz Title</td>
                        <td>Date</td>
                        <td>Full Mark</td>
                        <td>Mark Obtain</td>
                    </tr>
                </thead>
                <tbody>
                    <?php if(isset($student_result_list) && count($student_result_list)): ?>
                        <?php foreach ($student_result_list as $result) :?>
                            <tr>
                                <td><?php echo $result->student_name( ); ?></td>
                                <td><?php echo $result->title( ); ?></td>
                                <td><?php echo $result->date( ); ?></td>
                                <td><?php echo $result->full_mark( ); ?></td>
                                <td><?php echo $result->mark_obtained( ); ?></td>

                            </tr>
                        <?php endforeach;?>
                    <?php endif; ?>
                </tbody>
            </table>
        </form>

<script>


</script>
<?php  $this->admin->footer( data() ); ?>
