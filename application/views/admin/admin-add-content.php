<?php  $this->admin->header( data() ); ?>
<script src="//cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
<style>
    .question-list-container {
        overflow-y: scroll;
        min-height: 200px;
        max-height: 300px;
    }
    
    .left-section,
    .right-section,
    .content-body {
        float: left;
        
    }
    
    .left-section {
        width: 70%;
    }
    
    .content-body {
        width: 100%;
        float: left;
        margin-bottom: 10px;
    }
    
    .right-section {
        padding-left: 20px;
        width: 30%;
        min-height: 100px;
    }
    
    .content-title {
        width: 70%;
        float: left;
        padding-bottom: 10px;
    }
    .content-title a{
        padding: 7px 5px;
        margin-left: 10px;
        text-decoration: none;
    }
    .content-url-ref{
        width: 50%;
        float: left;
        padding-left: 10px;
    }
    
    .content-title input {
        width: 100%;
    }
    
    .placeholder {
        background-color: #eee;
        border: 2px dashed #999;
        float: left;
        display: block;
        width: 100%;
        height: 100px;
        margin-bottom: 10px;
    }
    #content-body {
        min-width: 100%;
        max-width: 100%;
        min-height: 100%;
        max-height: 100%;
        height: 300px;
        resize: none;
        border: 1px solid #CCC;
    }
    
    #page-template {
        width: 100%;
    }

    .input-box {
        width: 100%;
        float: left !important;

    }

    .content-description {
        width: 70%;
    }

    .content-description textarea {
        max-width: 100%;
        min-width: 100%;
        min-height: 150px;
        max-height: 300px;
    }

    .full-mark,
    .quiz-type {
        width: 28%;
        margin-left: 2%;
    }

    .full-mark{
        margin-top: 13px;
    }

    .input-box input[type='text'],
    .input-box input[type='email'],
    .input-box textarea {
        width: 100% !important;
        float: left;
    }

    .quiz-type .label {
        width: 100%;float: left;margin-top: 35px;
    }

    .full-width-label {
        width: 100%;
    }
    
    .question-add-section {
        
    }
    
    .question-add-section label{
        font-size: 12px;
        margin-top: 8px;
    }
    
    .question-options .inputRadio {
        min-width: 20px;
        height: 20px;
        float: left;
    }
    
    .question-options .radioButton {
        border: 1px solid #999;
    }
    
    .question-options .question-option {
        float: left;
        width: 100%;
        margin-bottom: 5px;
    }
    
    .question-options .question-option input[type='text'] { 
        width: 500px;
        padding: 4px 6px;
        border-radius: 3px;
        box-shadow:inset 0 0 11px #dffafb;
    }
    
    .question-options .question-option label {
        margin-top: 0;
    }
    
    
    .question-element {
        padding: 10px;
        border:1px solid #ccc;
        margin-bottom: 10px;
        float: left;
        width: 100%;
        box-shadow: 0 2px 2px #CCC; 
        background-color: #f2f3f5;
        border-radius: 2px;
    }
    
    .question-close-btn {
        background-color: #dffafb !important;
        padding:2px 4px !important;
        float: right;
        color : #333 !important;
    }
    .question-close-btn:hover {
        color: #581a19 !important;
    }
</style>


<?php

    //_alert($this->post->get_input("content-title"));
    //Explain( $this->input->post() );

    $action = $this->uri->segment(3);
    //_alert($action);
    $data['action'] = $action;
    $config = array();
    $rb =& $config['relode_btn'];
    $cb =& $config['collaps_btn'];
    $pf =& $config['print_form'];
    $f =& $config['footer'];
    $config['data'] = $data;
    
    $cb = true;
    //$pf = true;
    $first_element = array( "-" => "(All Subject)" );
    $categories = get_category();

?>

<script>

    function enable_events(){
        _events(".core-section", ".collaps-btn", "click");
    }
    
    task_list.collaps = {func: collapse_section};
    
    
    
    enable_events();
    
    
    
</script>

<div id="question_config" class="core-section popup" style="display: none; width: 400px">
    <div class="section-header">
        <div class="section-name">Question Information</div>
    </div>
    <div class="section-body">
        <form action="" method="post" id="set-question-config">
            <div class="">
                <div class="options-for-init">
                    <label for="num-of-question" class="full-width-label"  style="font-size: 12px">Number of Questions</label>
                    <input   style="width: 75%" type="text" id="num-of-question" name="num-of-question" value="10">
                    <label for="num-of-ans-opt" class="full-width-label" style="font-size: 12px; margin-top: 10px">Number of Options for each Questions Answer</label>
                    <input  style="width: 75%" type="text" id="num-of-ans-opt" name="num-of-ans-opt" value="4" >
                </div>

                <label for="ans-opt" class="full-width-label" style="font-size: 12px; margin-top: 10px">Options type <br><small>Answer options type e.g. A) 10 B)40 C) 60 D) 40</small> </label>
                <select name="ans-opt" id="ans-opt" style="width: 75%">
                    <option value="">Numeric (1,2,3..)</option>
                    <option value="A">Letter in uppercase (A,B,C,D)</option>
                    <option value="a">Letter in lowercase (a,b,c,d)</option>
                    <option value="i">Roman Number (i,ii,iii...)</option>
                    <option value="ক">বাংলা (ক,খ,গ...)</option>
                </select>

                <label for="category" class="full-width-label" style="font-size: 12px; margin-top: 10px">Category <br><small>Select a Category for the questions</small></label>
                <select name="category" id="question-category" required style="width: 75%">
                    <option value="" >Select</option>
                    <?php
                    
                    if(count($categories)):?>
                        <?php foreach ($categories as $value => $name):?>
                            <option value="<?php echo $value?>"><?php echo $name?></option>
                        <?php endforeach;?>
                    <?php endif; ?>
                </select>

                <!--<input type="text" id="ans-opt" name="ans-opt" value="A" placeholder="Answer options type e.g. A) 10 B)40 C) 60 D) 40" >-->
                <label for="ans-opt-wrap" class="full-width-label" style="font-size: 12px; margin-top: 10px">Options type Wrap</label>
                <input style="width: 75%" type="text" id="ans-opt-wrap" name="ans-opt" value="(*)" placeholder="Answer options type wrap e.g. (*) means (A) 10 (B) 21 (C) 35 (D) 78" >
                <div style="width: 100%; float: left; text-align: center">
                    <input   type="submit" class="btn" id="view-question-form" value="Done">
                    <a href="javascript:" class="question_config_close btn" >Close</a>
                </div>
                
            </div>
        </form>
    </div>
</div>


<div id="question_list" class="core-section popup" style="display: none; width: 600px">
    <div class="section-header">
        <div class="section-name">Add Question From List</div>
    </div>
    
    <div class="section-body">
        <form action="" method="post" id="question_list_form">
            <div class="">
                <div class="question-list-header">
                    <label for="category" class="full-width-label" style="font-size: 12px; margin-top: 10px">Subject <br><small>Select a Subject</small></label>
                    <select name="filter-category" id="question-category" style="width: 40%">
                        <option value="" >Select</option>
                        <?php

                        if(count($categories)):?>
                            <?php foreach ($categories as $value => $name):?>
                                <option value="<?php echo $value?>"><?php echo $name?></option>
                            <?php endforeach;?>
                        <?php endif; ?>
                    </select>
                    <input style="width: 30%" type="text" id="search-text" name="search-text" placeholder="Search a Question" >
                </div>
                
                <div class="question-list-container"></div>
                <div style="width: 100%; float: left; text-align: center">
                    <input   type="submit" class="btn" id="question-add-to-list" value="Add To List" disabled="">
                    <a href="javascript:" class="question_list_close btn" >Close</a>
                </div>
            </div>
        </form>
    </div>
</div>

<form method="get" action="" id="_add_quiz_and_questions">

    <input type="hidden" value="d" name="a">
    <div class="content-section">

        <div class="left-section">


            <?php $this->sec_view->_print("Quiz Details", "quiz-basic-informations", "/admin/includes/content-add-edit/quiz-basic", $config);?>
            <?php $this->sec_view->_print("Questions", "quiz-questions", "/admin/includes/content-add-edit/quiz-questions", $config);?>
            
            <?php //$this->sec_view->_print("Basic Information", "content-basic-informations", "/admin/includes/content-add-edit/basic-informations", $config);?>

        </div>


        <div class="right-section">
            <?php if($content_type === "quiz"):?>
            <?php $this->sec_view->_print("Categories", "content-categories", "/admin/includes/content-add-edit/categories", $config);?>
            
            <?php $this->sec_view->_print("Tags", "content-tags", "/admin/includes/content-add-edit/tags", $config);?>
            <?php else: ?>
            <?php $this->sec_view->_print("Page Template", "content-page-template", "/admin/includes/content-add-edit/page-template", $config);?>
            <?php endif;?>
<!--            <div class="content-others-settings">-->
                
                <?php
                    $other_setting_files = get_dir(APPPATH ."/views/admin/content-settings/" . $content_type, "file", array('content-categories','content-basic-informations','content-page-template','content-submit'));
                    //Explain($other_settings);
                    if(count($other_setting_files)){
                        foreach ($other_setting_files as $file){
          
                            $title = ucwords(trim(str_replace(array("-","_",".php"),' ', $file ) ) );
                            $this->sec_view->_print($title , strtolower( str_replace(" ", "-", $title) ), "/admin/content-settings/".$content_type . "/" . str_replace(".php", "",$file), $config);
                        }
                    }
                ?>
<!--            </div>-->
            <?php $this->sec_view->_print("Submit", "content-submit", "/admin/includes/content-add-edit/submit-options", $config);?>
        </div>
    </div>
</form>

<script>
    var action = '<?php echo uri_seg( 3 ); ?>';
    var quiz_id = '<?php echo uri_seg( 4 ) ? uri_seg( 4 ) : 0; ?>';
    var selected_category = <?php echo json_encode( $this->_quiz->get_input("categories") ) ?>;
</script>

<script src="<?php admin_js( "quiz-updating" ); ?>" type="text/javascript"></script>
<script>

    $( ".content-categories" ).relation_list({
        table: "class",
        config: { "type" : "quiz-category" },
        "search-string" : "",
        "content-name" : "Category",
        "select-name" : "categories[]",
        "select-id" : "content-category",
        "selected" : selected_category
    });

//    CKEDITOR.config.height = 380;
//    CKEDITOR.replace( 'content-description' );
    task_list.collaps = {func: collapse_section};
    
    var sortable_config = { 
        connectWith: ".content-section .right-section", 
        handle: ".section-header" ,
        axis: "x,y",
        delay: 300,
        placeholder: "placeholder",
        cancel : ".content-body"
    }
    
    
    function enable_events(){
        _events(".core-section", ".collaps-btn", "click");
        
        $(".content-section .left-section").sortable( sortable_config );
        sortable_config.connectWith = ".content-section .left-section span";
        $(".content-section .right-section").sortable( sortable_config );
    }
    
    
    enable_events();


    $('#question_config').popup( {
        transition : 'all 0.3s',
        background : true,
        color : "#666",
        opacity : 0.3,
        //type: 'tooltip',
        opentransitionend : open_question_init_form
    });

    $('#question_list').popup( {
        transition : 'all 0.3s',
        background : true,
        color : "#666",
        opacity : 0.3,
        type: 'tooltip',
        opentransitionend : show_question_list
    });


    function show_question_list( a ){
        $(a).css("transition", "none");
        $(a).draggable({handle:$(a).find(".section-header")});
        var selected_question_list = [];

        function set_selected_question(){
            selected_question_list = [];
            var counter = 0;
            $(".question-form-list tbody tr").each( function(i){
                var check = $(this).find( "input.question_id" );
                var id = $(this).find( "input.question_id" ).val( );
                var question = $(this).find( "input.question_title" ).val( );
                var answer = $(this).find( "input.question_answer" ).val( );

                if( check.prop("checked") ){
                    selected_question_list.push({ question: question, answer : answer, quest_id : id });
                    counter++;
                }
            });
        }

        function set_question_list_update_button_stauts(){
            set_selected_question( );
            if( selected_question_list.length ){
                $( "#question-add-to-list" ).attr( "disabled", false );
            }else {
                $( "#question-add-to-list" ).attr( "disabled", true );
            }
        }

        /************************************************************************************/
        /************* Add the slected question into questio list on submit *****************/
        /************************************************************************************/

        function add_to_question_list( ) {

            set_question_list_update_button_stauts();

            $("#question_list_form").off("submit");
            $("#question_list_form").on("submit", function(e){
                e.preventDefault();
                var data = {};

                //alert( num_of_quest + ", "+ num_of_ans_opt + ", " +ans_opt_type +", " +ans_opt_wrap);
                data.num_of_quest = Object.keys(selected_question_list).length;
                data.num_of_ans_opt = "";
                data.ans_opt_type = "";
                data.ans_opt_wrap = "";


                append_question_into_list( selected_question_list );
            })
        }

        /////////////////////////////////////////////////////////////////////////////////////////


        function on_check_question(){
            set_question_list_update_button_stauts();
            $(".question-form-list input.question_id").on("change", function(){
                add_to_question_list();
            });
        }

        function fetch_list(){
            _request_ajax({
                url : "_content/question_list_for_quiz",
                success : function(result){
                    $("#question_list_form").find(".question-list-container").html( result );
                    on_check_question();
                },
                formRef : $("#question_list_form"),
                data : {data_form: "__ajax__"}
            });
        }

        function enable_cat_filter(){
            $('#question_list').find( "#question-category" ).on("change", function(){
                fetch_list();
            });
        }

        function enable_cat_search(){
            $('#question_list').find( "#search-text" ).on("keyup", function(){
                fetch_list();
            });
        }

        enable_cat_filter();
        enable_cat_search();
        fetch_list();
    }

    /******************************************************************/
    function append_question_into_list( data ){


            //alert( num_of_quest + ", "+ num_of_ans_opt + ", " +ans_opt_type +", " +ans_opt_wrap);
            var html = "";
            var num_of_ans_opt = 4;
            var ans_opt_type = "A";
            var ans_opt_wrap = "";

//            html += "<input type='hidden' value='"+quest_category+"' name='quest_category' id='quest_category'>";
            for(var i=0; i < data.length; i++ ){
                html += question_form(num_of_ans_opt, ans_opt_type, ans_opt_wrap, i, data[i]);
            }
            $(".question-add-section .questions-form").prepend( html );

            enable_controls();
            re_arrange_question_list();
    }

    function open_question_init_form( a, b ){
//        $(a).css("border", "1px solid red");
//        $(b).html("Edit");

        $(a).css("transition", "none");
        $(a).draggable();


        if($(".question-add-section .question-element").length == 0){

            $(this).off("submit");
            $("#set-question-config").on("submit", function(e){
                e.preventDefault();
                var num_of_quest = $("#num-of-question").val();
                var num_of_ans_opt = $("#num-of-ans-opt").val();
                var ans_opt_type = $("#ans-opt").val();
                var quest_category = $("#question-category").val();
                var ans_opt_wrap = $("#ans-opt-wrap").val();

                //alert( num_of_quest + ", "+ num_of_ans_opt + ", " +ans_opt_type +", " +ans_opt_wrap);
                var html = "";
                html += "<input type='hidden' value='"+quest_category+"' name='quest_category' id='quest_category'>";
                for(var i=0; i < num_of_quest; i++ ){
                    html += question_form(num_of_ans_opt, ans_opt_type, ans_opt_wrap, i);
                }
                $(".question-add-section .questions-form").prepend( html );


                enable_controls();
                $(b).html("Config");
                $(a).find(".options-for-init").remove();
                $(a).popup("hide");
                $(this).off("submit");
                $(this).on("submit", set_question_config);
            });
        }

    }

    function enable_controls( ){

        re_arrange_question_list();

        $(".questions-form").sortable({
            update : function( event, ui ) {
                re_arrange_question_list();
            }
        });

        $(".question-element").each(function(){
            var $THIS = $(this);
            $THIS.find(".question-close-btn").off("click");
            $THIS.find("#hide-question-options").off("click");

            $THIS.find(".question-close-btn").on("click", function(e){
                e.preventDefault();
                if( confirm("Are You Sure?") ){
                    $THIS.remove();
                    re_arrange_question_list();
                }
            });

            //control for hiding answer option
            $THIS.find("#hide-question-options").on("click", function(){
                $THIS.find(".question-options").slideToggle(function(e){
                    if($(this).css("display") == "none"){
                        $THIS.find("#hide-question-options").html("Show Options");
                    }else {
                        $THIS.find("#hide-question-options").html("Hide Options");
                    }
                });
            });

        });
    }



//    function enable_answer_option_hiding(){
//        $(".question-element").each(function(){
//            var $THIS = $(this);
//        });
//    }





//    $("#prepare-to-question-add").on("click", function(){
//        alert("ok");
//    });


</script>


<?php  $this->admin->footer( data() ); ?>

