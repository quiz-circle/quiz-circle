<?php  $this->admin->header( $data ); ?>

    <style>
        .client-dashboard-img, .building-image, .each {
            float: left;
        }
        .client-dashboard-img {
            width: 100%;
        }
        
        .client-dashboard-img .building-image{
            width: 100;
        }
        .client-dashboard-img .building-image .each{
            width: 100%;
            height: 365px;
            overflow: hidden;
        }
        .client-dashboard-img .building-image .each img{
            width: 100%;
        }
    </style>

    <div class="left" id="user-form-success" >
        <h2>Buildings</h2>
        
        <div class="client-dashboard-img">
            
        <?php
            function client_public_link(){
                //return ;base_url("pub/view/" . $building['building_id'] ."/".$building['user_hash']. "/" .$user_hash);
                return "http://tgverk.is/demo-2/";
            }
            //Explain($buildings);
            foreach ($buildings as $building){
                $images = $build->get_images($building['building_id'], $total, array(
                    'sort_by'       =>    'priority',
                    'sort_by_type'  =>    'ASC',
                    'limit'  =>    1,
                ));
                
                ?>
                <div class="building-image">
                    <?php
                    if($total) {
                       //Explain($images);
                       foreach ($images as $im){
                           ?>
                            <div class="each">
                                <img src="<?php echo base_url("uploads/buildings/" . $im['image_file_name']) ?>">
                            </div>
                    
                            <?php echo anchor(base_url("admin/building/sect/" . $building['building_id'] . "/" . $im['image_id'] ."/".$building['user_hash'] ),"Edit " . ucwords($building['building_name']), "class='btn right' style='float:right' ") ;?>
                            <?php echo anchor(client_public_link(),"Public View", "target='_blank' class='btn'") ;?>
                            <?php
                       }
                    }
                    ?>
                </div>
                <?php
            }
            
        ?>
        </div>
    </div>
<?php  $this->admin->footer( $data ); ?>