<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Login to your account</title>
  
  <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css"> -->
<link href="<?php echo $this->admin->images("fevicon.png") ?>" rel="icon" type="image/png">

<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
<link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
<link href="<?php echo $this->admin->css("login.css"); ?>" rel="stylesheet" type="text/css" >
<!--<link rel="stylesheet" href="css/style.css"> -->
<style>
    .message {
        color: magenta;
    }
    
    .ovarall {
        font-size: 14px !important;
    }
    
</style>
</head>

<body>
  
<!-- Form Mixin-->
<!-- Input Mixin-->
<!-- Button Mixin-->
<!-- Pen Title-->
<div class="pen-title">
  <h1><?php echo isset($site_name) ? $site_name : "Buildings And Flats"?></h1>
  <span class="message ovarall"><?php echo isset($ovarall_message) ? $ovarall_message:""?></span>
</div>
<!-- Form Module-->
<div class="module form-module">
  
<div class="toggle" style="background: none">

  </div>
  <div class="form">
    <h2>Login to your account</h2>
    <!--<form action="post">-->
    <?php echo form_open($back_to) ?>
        <p class="message"><?php echo form_error("username");?></p>
        <input type="text" placeholder="Username" name="username" value="<?php echo $this->input->post('username')?>"/>
        <p  class="message"><?php echo form_error("password");?></p>
        <input type="password" placeholder="Password" name="password" value="<?php echo $this->input->post('password')?>"/>
        
      <button>Login</button>
    </form>
  </div>

</div>

    <script>

    </script>

</body>
</html>
