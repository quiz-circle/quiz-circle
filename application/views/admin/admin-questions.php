<?php  $this->admin->header( data() );  ?>
<style>
    .content-header {
        float: left;
        width: 100%;
        padding: 5px 0;
    }
    
    #basic-filter {
        font-size: 15px;
        width: auto;
        margin-right: 2px;
        height: 25px;
    }
    
    #basic-filter .radioButton {
        position: relative;
        padding: 0 5px;
        height: 25px;
        width: auto;
        background-color: #eee;
        border-radius:0;
        text-align: center;
        line-height: 25px;
        z-index: 10;
        float: left;
        left: 0;
    }
    
    /* On mouse-over, add a grey background color */
    #basic-filter:hover input ~ .radioButton {
        background-color: #ccc;
    }
    
    /* When the radio button is checked, add a blue background */
    #basic-filter input:checked ~ .radioButton {
        background-color: #2196F3;
        color: #FFF;
        padding-left: 25px;
    }

    #basic-filter.trash-btn .radioButton{
        background-color:  #f5c0bf;
    }
    
    #basic-filter.trash-btn input:checked ~ .radioButton {
        color: #581a19;
    }

    /* Style the indicator (dot/circle) */
    #basic-filter .radioButton:after {
            top: 9px;
            left: 9px;
            width: 8px;
            height: 8px;
            border-radius: 50%;
            background: white;
    }
    #basic-filter.trash-btn .radioButton:after {       
        background: #581a19;
    }
    
    .question-list {
        
    }
    .question-list td .btn {
        margin: 0;
    }
    
    .question-list td.question {
        width: 280px;
    }
    .question-list td.answer {
        width: 150px;
    }
    .question-list thead td {
        font-size: 12px;
    }
    
    .add_question_open {
        float: right;
        margin: 0;
    }
    
    .filter {
        float: left;
    }
    
    .search {
        float: left;
        margin-left: 10px;
    }
    
    .search input {
        height: 25px;
    }
    
    .questions-list-form {
        position: relative;
    }
    
    .question-category-list .__content__list__ ,
    .question-category-list-edit .__content__list__ {
        min-height: 50px;
        max-height: 180px;
        overflow-y: scroll;
        margin-top: 10px;
        border: 1px solid #ccc;
    }
    
    .question-category-list #search-string ,
    .question-category-list-edit #search-string {
        width: 100%;
        padding: 5px 4px;
        font-size: 12px;
    }
    
    .question-category-list .__content__list__ label ,
    .question-category-list-edit .__content__list__ label {
        width: 100%;
        padding: 4px 5px;
        border-bottom: 1px solid #CCC;
    }
    .question-category-list .__content__list__ label:last-child ,
    .question-category-list-edit .__content__list__ label:last-child {
        border: none;
    }
</style>

    <div class="left" id="user-form-success" >
        <?php //Explain($questions); ?>

        <div class="core-section" id="delete_question" style="float: none;width: 300px; display: none">
            <div class="section-header">
                <div class="section-title">
                    Delete Question
                </div>
            </div>
            <div class="section-body">
                <form id="questions-delete-form" action="" method="post">
                    <input type="hidden" value="" id="deleting-id" name="deleting-id">
                    Are You Sure?
                    <div style="width: 100%; text-align: center">
                        <button class="btn" type="submit">Ok</button>
                        <button class="btn delete_question_close">Cancel</button>
                    </div>
                </form>
            </div>
        </div>




        <div class="core-section" id="edit_question" style="display: none; width: 825px; float: none">
            <div class="section-header">
                <div class="section-title">
                    Edit Question
                </div>
            </div>
            <div class="section-body">
                <form id="questions-edit-form" action="" method="post">
                    <label for="quest-title">Question Title</label><br>
                    <input type="text" id="quest-title" name="quest-title" style="width: 800px">

                    <br>
                    <br>
                    <label for="quest-answer">Answer</label><br>
                    <input type="text" id="quest-answer" name="quest-answer"  style="width: 450px">
                    <br>
                    <br>
                    <div style="width: 100%; float: left; text-align: right; position: relative; display: inline-block; ">
                        <div class="core-section question-category-list-edit" style="width:260px; float:left;">
                            <div class="section-header">
                                <div class="section-title">Category</div>

                            </div>
                            <div class="section-body">
                                <div class="list-header">
                                    <input type="text" name="" value="" id="search-string" placeholder="Search a category">
                                </div>
                                <div id="main-panel" >

                                </div>
                            </div>
                        </div>
                        <div class="btns" style="position: absolute; right: 0; bottom: 10px">
                            <input type="submit" value="Add a Question" class="btn" >
                            <button class="btn edit_question_close">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


        <div class="core-section" id="add_question" style="display: none; width: 825px; float: none">
            <div class="section-header">
                <div class="section-title">
                    Add Question
                </div>
            </div>
            <div class="section-body">
                <form id="questions-add-form" action="" method="post">
                    <label for="quest-title">Question Title</label><br>
                    <input type="text" id="quest-title" name="quest-title" style="width: 800px">

                    <br>
                    <br>
                    <label for="quest-answer">Answer</label><br>
                    <input type="text" id="quest-answer" name="quest-answer"  style="width: 450px">
                    <br>
                    <br>
                    <div style="width: 100%; float: left; text-align: right; position: relative; display: inline-block; ">
                        <div class="core-section question-category-list" style="width:220px; float:left">
                            <div class="section-header">
                                <div class="section-title">Category</div>

                            </div>
                            <div class="section-body">
                                <div class="list-header">
                                    <input type="text" name="" value="" id="search-string">
                                </div>
                                <div id="main-panel">

                                </div>
                            </div>
                        </div>
                        <div class="btns" style="position: absolute; right: 0; bottom: 10px">
                            <input type="submit" value="Add a Question" class="btn" >
                            <a href="javascript:" class="btn add_question_close">Close</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>


        <form id="questions-list-form" action="" onsubmit="return false" method="post">
            <div class="content-header">
                <div class="filter">
                    <label class="inputRadio" id="basic-filter">
                        <input type="radio" name="filter[status]" value="ac" checked="">
                        <span class="radioButton">Active</span>
                    </label>
                    
                    <label class="inputRadio trash-btn" id="basic-filter">
                        <input type="radio" name="filter[status]" value="tr">
                        <span class="radioButton">Trash</span>
                    </label>
                </div>
                <div class="more-filter" style="display: inline-block; float:left">
                    <label style="display: inline-block; float: left; margin-left: 6px; font-size: 12px; margin-top: 6px; margin-right: 3px; color:#999 ">Category</label>
                    <select name="filter-category" id="filter-category" style="padding: 3px; color: #555">
                        <option value="">All Categoiries</option>
                        <?php $this->category->type = "question-category"; $this->category->getList( $class_list_config ); ?>
                    </select>
                </div>
                <div class="search">
                    <input type="text" id="search-text" name="search-text" placeholder="Search..">
                </div>
                <a href="javascript:" class="btn add_question_open" >Add Question</a>
                
            </div>
            <table class="content-list question-list">
                <thead >
                    <tr>
                        <input type="hidden" value="448" name="hello">
                        <td><input type="checkbox" id="" name="book" value="all"></td>
                        <td>Question</td>
                        <td>Answer</td>
                        <td>Posted By</td>
                        <td>Published on</td>
                        <td>Edited on</td>
                        <td colspan="2">Action</td>
                    </tr>
                </thead>
                <tbody>
                    <?php if( count($questions) ): ?>
                        <?php admin_view( "includes/questions/quest_list.php", array( 'questions' => $questions ) ); ?>
                    <?php else : ?>
                    <tr>
                        <td colspan="20">No Data Found</td>
                    </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </form>
        <?php //echo anchor(base_url("admin/create_user"), "+Add a User", array("class" => "btn")) ; ?>
    </div>
<script>

    $('#add_question').popup({
        transition: 'all 0.3s',
        background: true,
        color: "#666",
        opacity: 0.3,
        horizontal : 'center',
        vertical : 'center',
        beforeopen : function( box, btn ) {

            $( ".question-category-list" ).relation_list({
                table: "class",
                config: {"type":"question-category" },
                "search-string":"",
                "content-name" : "Category",
                "select-name" : "categories[]",
                "select-id" : "categories",
            });
        }
    });



//        autoopen    : false,
//        background  :   true,
//        color       :   "#FFF",
//        type        : 'tooltip',






//    $('#question_config').popup( {
//        transition : 'all 0.3s',
//        background : true,
//        color : "#666",
//        opacity : 0.3,
//        //type: 'tooltip',
//        opentransitionend : open_question_init_form
//    });

    counter = 0;





    function after_loaded(){
        counter++;
        console.log("\nQUESTION LIST -> Loaded " + counter + " time(s)...!\n");
        enable_category_filter();

        $(".delete_question_open").each(function () {
            $(this).off("click");
            $(this).on("click", function(e){
                var question_id = $(this).attr("data-quest-id");
                $('#questions-delete-form').find("#deleting-id").val(question_id);
            });
        });

//        $('#edit_question').popup({
//            autoopen    :   false,
//            background  :   true,
//            color       :   "#FFF",
//            type        :   'tooltip',
//            horizontal  :   'rightedge',
//            vertical    :   'topedge',
//        });

        $('#delete_question').popup({
            transition: 'all 0.3s',
            background: true,
            color: "#666",
            opacity: 0.3,
            horizontal : 'center',
            vertical : 'center',
            offsettop : "50",
            beforeopen : function( box, btn ) {
                console.log( btn );

                $('#questions-delete-form').off("submit");
                $('#questions-delete-form').on("submit", function(e){
                    e.preventDefault();
                    _request_json({
                        url : "_quiz_ajax/delete_parmanently",
                        success: function(result){
                            console.log(result);
                            success_message(result);
                            $('#delete_question').popup("hide");
                            fetch_question_list( after_loaded );
                        },
                        formRef : $(this)
                    });
                });

            }
        });
    }

    function open_edit_question(id, btn){

        var ID = $(btn).attr("id");
        var cats = $("#_cats_" + ID).val();


        //alert(cats);
        //alert(cats);

        $('#edit_question').popup( "toggle");



        $( ".question-category-list-edit" ).relation_list({
            table: "class",
            config: {"type":"question-category" },
            "search-string":"",
            "content-name" : "Category",
            "select-name" : "categories[]",
            "select-id" : "categories",
            selected : JSON.parse(cats)
        });



        var box_width = $('#edit_question').width();

//        $('#edit_question').css("margin-top", ($(btn).offset().top+20) + "px");
//        $('#edit_question').css("margin-left", ($(btn).offset().left - box_width + 30 ) + "px");

        edit_question(id);
    }

    $("#questions-add-form").on("submit", function(e){
        e.preventDefault();
        _request_json({
            url : '_quiz_ajax/update_question',
            success : function(result){
                console.log(result);
                if(result.success){
                    success_message(result);
                    fetch_question_list();

                    //$('#add_question').popup("hide");
                }
            },
            formRef : $(this)
        });
    });

    function fetch_question_list( after ){

        var quest_data = {
            data_form : "__ajax__"
        };
        _request_ajax({
            url : '_quiz_ajax/get_question_list',
            success : function( result ){

                //console.log( result );
                $("#questions-list-form").find(".content-list tbody").html( result );

                after_loaded();
                if($.type(after) == "function"){
                    after();
                }
            },
            formRef : $("#questions-list-form"),
            data : quest_data
        });
    }
    //fetch_question_list( );



    function restore_question(id){
        fetch_question_list();
    }

    function edit_question(id){
        $("#questions-edit-form").find("#quest-title").val( "" );
        $("#questions-edit-form").find("#quest-answer").val( "" );
        _request_json({
            url : "_quiz_ajax/get_a_question",
            success : function( result ){
                console.log(result);
                $("#edit_question").find("#quest-title").val( result.quest_title );
                $("#edit_question").find("#quest-answer").val( result.answer );
                $("#questions-edit-form").off("submit");
                $("#questions-edit-form").on("submit", function(e){
                    e.preventDefault();
                    _request_json({
                        url : "_quiz_ajax/edit_question",
                        success : function( result ){
                            if( result.success ) {
                                success_message( result );
                                fetch_question_list();
                                $('#edit_question').popup("hide");
                            }
                        },
                        formRef : $(this),
                        data : { question_id : id },
                    });
                });
            },
            data: {question_id : id}
        });
    }

    function permanent_delete_question(id){

    }

    function set_status(id, status){
        section_info_message("Processing...!");
        _request_json({
            url : "_quiz_ajax/set_status",
            success : function( result ){
                if( result.success ) {
                    fetch_question_list();
                }
                console.log(result);
                success_message(result);
            },
            data: { quest_id: id, status: status }
        });
        return false;
    }

    function trash_question(id){
        set_status(id, 0);
    }

    function restore_question(id){
        set_status(id, 1);
    }

    $("#search-text").on("keyup", function(){
        fetch_question_list();
    });

    $("#basic-filter input[type='radio'] ").on("click", function(e){
        //var filterValue = $(this).val();
        fetch_question_list( );
    });

    $("#filter-category").on("change", function(){

        fetch_question_list( );
    });


</script>
<?php  $this->admin->footer( data() ); ?>
