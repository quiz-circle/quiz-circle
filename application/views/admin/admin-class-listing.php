<?php $this->admin->header(data()); ?>
<style>
    .class-add-edit {
        width: 25%;
        float: left;
    }
    
    .class-add-edit input[type='text'],
    .class-add-edit #parent{
        width: 75%;
        height: 28px;
        padding: 2px 5px;
        font-size: 12px;
    }
    
    .class-list {
        width: 74%;
        float: left;
    }
    
    
    .basic-filters {
        padding: 0;
        margin: 0;

        float: left;
        border-bottom: 2px solid green;
        height: 25px;
    }
    
    .basic-filters li {
        float: left;
        display: inline-block;
        border: 1px solid #FFF;
        
    }
    
    .basic-filters li a{
        padding: 2px 8px;
        float: left;
    }
    
    .basic-filters li.active {
       height: 25px;
       z-index: 5;
       background-color: #FFF;
       border: 1px solid green;
       border-bottom: 0;
       
    }
    .basic-filters li.active a{
        padding-bottom: 6px;
    }
    
    .table-heading {
        width: 100%;
        border: 1px solid #666;
        border-bottom: none;
        padding: 8px 5px;
        display: block;
        float: left;
        font-size: 16px;
        background-color: #666;
        color: #FFF;
    }
    
    .pagination {
        text-align: center;
        width: 100%;
        float: right;
        margin: 10px 0;
    }
    
    #pagination {
        display: inline-block;
        margin: 0;
        padding: 0;
        list-style-type: none;
        float: right;
    }
    
    #pagination li {
        float: left;
        width: 25px;
        height: 25px;
        margin-left: 8px;
        text-align: center;
    }
    
    #pagination li a,
    #pagination li span {
        border: 1px solid #555;
        float: left;
        width: 100%;
        height: 100%;
        color: #333;
        line-height: 25px;
        text-decoration: none;
    }
    
    
    #pagination li.active {
        background-color: #4bbe85;
    }
    
    #pagination li.active span{
        color : #555;
    }
    
    .selected-actions {
        
    }
    
    .selected-actions select {
        width: 150px;
        font-size: 12px;
    }
    
    .selected-actions selected-action-label {
        
    }
    
    input.btn {
        font-size: 12px;
        padding: 6px 14px;
        cursor: pointer;
    }
    
    .search-box {
        width: 100%;
        float: left;
        height: 0;
    }
    .search-box form {
        height: 100%;
        display: block;
        float: right;
        margin-top: 10px;
    }
    
    .search-box form input[type='text'] {
        width: 180px;
        font-size: 15px;
        padding: 4px 8px;
    }
    
    .search-box form input.btn {
        margin: 0;
    }
    
    
    
    
    
    /*==========================*/
        .content-category-wrap {
        max-height: 300px;
        overflow-y: scroll;
        overflow-x: hidden;
        float: left;
        width: 100%;
    }
    
    .content-category {
        padding: 0;
        margin: 0;
        float: left;
        width: 100%;
        list-style: none;
    }
    
    .content-category li{
        border: 1px solid #ccc;
        background-color: #eee;
        width: 100%;
        position: relative;
        padding: 0;
        margin: 0;
        padding: 5px 6px;
        margin-bottom: 2px;
    }
    
    .content-category li input[ type='checkbox' ],
    .content-category li span{
        display: inline-block;
        padding: 0;
        margin: 0;
    }
    
    .content-category li span{
        height: 100%;
        padding: 2px 0;
    }
    
    .content-category li span label{
/*        position: absolute;
        background-color: #002166;*/
    }
    
    .content-category li input[ type='checkbox' ]{
        display: inline-block;
        height: 20px;
        width: 20px;
        padding: 0;
        margin: 0;
    }
    
    /*==================================*/
    .category-list-section {
        position: relative;
    }
    
    .category-list-section .class-list-showing {
        position: relative;
        z-index: 1000;
    }
    
    .category-list-section .class-list-showing-in {
        padding: 20px;
        padding-top: 25px; 
        width: 230px;
        background-color: #FFF;
        border-color: #911991;
        border: 1px solid;
        display: none;
        float: left;
        position: absolute;
        top: 100%;
        right: 0;
        z-index: 1000;
    }
    
    .class-box-close {
        border-radius: 50%;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 15px;
        right: 15px;
        float: left;
        border: 1px solid #999;
        cursor: pointer;
        text-align: center;
        line-height: 17px;
        font-size: 12px;
        color: #999;
        outline: none;
        text-decoration: none !important;;
    }
    
    .class-box-close:hover {
        box-shadow:inset 0 0 20px magenta;
        color:#555;
        border-color: #555;
    }
    
    .class-list-showing-in {
        text-align: center;
    }
    
    .class-list-showing-in img {
        width: 30%;
        opacity: 0.7;
    }

    .tabs {
        width: 100%;
    }

    .tabs ul {
        padding: 0;
        margin: 0px;
        list-style-type: none;
        text-align: center;
        float: left;
        width: 100%;
        margin-bottom: 15px;
    }

    .tabs ul li {
        display: inline-block;
        margin: 0 5px;
    }

    .tabs ul li a{
        padding: 6px 10px;
        border: 1px solid #CCC;
        float: left;
        font-size: 12px;
        text-decoration : none;
    }

    .tabs ul li a:hover{
        background-color: #CCC;
    }

    .tabs ul li.active a:hover,
    .tabs ul li.active a{
        background-color: #00a8e7;
        color: #FFf;
    }
</style>

<?php

    $class_type = uri_seg(4);

?>

<div class="tabs">
    <ul>
        <?php $link_ref = "quiz-category"; ?>
        <li class="<?php echo $class_type == $link_ref ? "active":""?>" >
            <a href="<?php echo base_url("admin/classes/get_list/" . $link_ref); ?>" >Quiz Category</a>
        </li>
        <?php $link_ref = "question-category"; ?>
        <li class="<?php echo $class_type == $link_ref ? "active":""?>" >
            <a href="<?php echo base_url("admin/classes/get_list/" . $link_ref); ?>" >Question Category</a>
        </li>
        <?php $link_ref = "tutorial-category"; ?>
        <li class="<?php echo $class_type == $link_ref  ? "active":""?>" >
            <a href="<?php echo base_url("admin/classes/get_list/" . $link_ref); ?>" >Tutorial Category</a>
        </li>

        <?php $link_ref = "video-category"; ?>
        <li class="<?php echo $class_type == $link_ref  ? "active":""?>" >
            <a href="<?php echo base_url("admin/classes/get_list/" . $link_ref); ?>" >Video Category</a>
        </li>
        <?php $link_ref = "paragraph-category"; ?>
        <li class="<?php echo $class_type == $link_ref ? "active":""?>" >
            <a href="<?php echo base_url("admin/classes/get_list/" . $link_ref); ?>" >Paragraph Category</a>
        </li>
        <?php $link_ref = "notice-category"; ?>
        <li class="<?php echo $class_type == $link_ref ? "active":""?>" >
            <a href="<?php echo base_url("admin/classes/get_list/" . $link_ref); ?>" >Notice Category</a>
        </li>
    </ul>

</div>
<div class="tab-sections">
    <div class="class-add-edit">
        <?php $action =  uri_seg(5) == "" ? "Add a ":"Edit";?>
        <h2 style="margin: 0; padding: 0; margin-bottom: 10px"><?php echo $action; ?> <?php echo  str_replace( array("_", "-"), " ",  $showing) ?></h2>
        <?php echo form_open("", array( 'id' => 'class-form' )); ?>
        <?php
            $form_name = "class-name";
            echo form_label(  ucfirst(str_replace( array("_", "-"), " ",  $showing )) . " Name", $form_name);
            echo "<br>";
            $inputs  = array(
                'name' => $form_name,
                'id' => $form_name,
                'value' => input_value($form_name)
            );
            echo form_input($inputs )
        ?>

        <?php if($is_tree) : ?>
        </br></br>
        <label for="parent" >Parent</label><br>
        <select name="parent" id="parent">
            <option value="0">(no parent)</option>
            <?php $this->category->getList( $parent_list_config )?>
        </select>
        <?php endif; ?>
        <br><br>
        <?php
            $form_name = "reference";
            echo form_label( " URL Reference", $form_name);
            $inputs  = array(
                'name' => $form_name,
                'id' => $form_name,
                'value' => input_value($form_name)
            );
            echo form_input($inputs);
            $submit_btn_name  = uri_seg(5) == "" ? "Add": "Edit";
        ?>
        <br><br>
        <?php echo form_submit($submit_btn_name, $submit_btn_name, array('class' => 'btn')); ?>
        <?php echo form_close(); ?>
    </div>

    <div  class="class-list" >
        <div class="search-box">
            <?php //$this->list_config->search_html($search_key, $this->input->get($search_key)); ?>
        </div>
        <form action="" method="post">

            <?php //$this->list_config->selected_option_views(array("class"=> "selected-actions"),  $action_selected )?>


            <div class="table-heading">
                Showing <?php echo str_replace( array("_", "-"), " ",  plural_word($showing)) ?>
            </div>
                <table class="content-list">
                    <thead>
                        <tr>
                            <td><input type="checkbox" name="all-checked"></td>
                            <td>Name</td>
                            <td>url referece</td>
                            <td colspan="2">Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php //if( $total > 0):?>
                            <?php $this->category->getList($list_config) ?>
                        <?php //endif;?>
                    </tbody>
                </table>

            <?php //$this->list_config->selected_option_views(array("class"=> "selected-actions"), $action_selected)?>
        </form>
    </div>

</div>

<div class="pagination">
    <?php //echo $pagination; ?>
</div>

<script>
    task_list.show_classes = {func: collaps_class_list};
    task_list.assign_class_to_news = {func: assign_class_to_news};


    function enable_events(){
        _events(".post-contents", ".select-categories", "click");
        _events(".post-contents", ".class_assign", "click");
    }

    function assign_class_to_news(id){
        var active_ids = [];
        $("#category-" + id + " li input:checked").each(function(){
            active_ids.push($(this).val());
        });
        var content_id = $("#content_id" + id).val();

        if(active_ids.length > 0){
            _request_json({
                url : 'save/class_relations',
                data : {content_id : content_id, class_id : JSON.stringify( active_ids )},
                success : function(result){
                    console.log(result);
                    collaps_class_list(id);
                }
            });
        }else {
            alert("You have to must select at least one category!");
        }
    }

    function collaps_class_list(id, c, e){

        var class_type = $("#show_classes-" + id).attr("data-list-type");
        var content_id = $("#show_classes-" + id).attr("data-content-id");

        $( ".class-list-showing-in").css("display", "none");

        $( "#"+class_type + "-" + id).css("display", "block");
        $( ".class-list-showing-in").css("text-align", "center");
        loading_gear( "#"+class_type + "-" + id );
        //$( ".class-list-showing-in").css("text-align", "left");
        //alert(e.target.id );
        _request_ajax({
            url : "loadview/get_classes",
            data : {class_type : class_type, content_id:content_id, list_id : id},
            success : function(result){
                $( "#"+class_type + "-" + id).html(result);
                enable_events();
            }
        });
    }

    enable_events();
    
    
</script>

<?php $this->admin->footer(data()); ?>