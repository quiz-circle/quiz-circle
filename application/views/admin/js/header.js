var task_list = {};
var s =0;
function resize_cordinates(nw) {
    //alert(nw);
    var im = $("#wrapper #images");
    im.each(function (i) {

        //console.log(this);

        var pw = parseInt($(this).find("#width").val());
        var ph = parseInt($(this).find("#height").val());

        $(this).width(nw)

        $(this).find(".map").css("width", "100%");
        $(this).find("img").css("width", "100%");
        var w = $(this).find("img").width();
        var h = $(this).find("img").height();

        //console.log("--------------");
        var c = $(this).find("#image-map #original-coords");
//        console.log("Pewvious Dimension:" + pw + "x" + ph + "\n");
//        console.log("-------------------------");
//        console.log("Changed Dimension:" + w + "x" + h + "\n");
//        console.log("-------------------------------------");

        var f = 1;
        if (w > h) {
            f = pw / ph;
        } else {
            f = ph / pw;
        }
        var nh = Math.floor(nw / f);
//        console.log("Changed Dimension (nh):" + w + "x" + nh + "\n");
//        console.log("-------------------------------------");
//        console.log("\n\n");

        var ofh = ph - nh;
        var ofw = pw - nw;

        var n_cord = [];

        c.each(function (j) {
            var Type = $(this)[0].type;
            var nc = "";
            var crs = $(this).val();
            var cordinates = crs.split(",");
//            console.log("Original Cordrinates: " + (i + 1) + "." + (j + 1) + " -> " + cordinates);
//
//            console.log("------------------------------------------");

            for (m = 0; m < cordinates.length; m++) {

                nc += (m != 0) ? ", " : "";

                var c = cordinates[m];

                if (i % 2 == 0) {
                    var r = (c / pw) * 100;
                    var d = (ofw * r) / 100;
                    nc += Math.abs(Math.floor(c - d));
                } else {
                    var r = (c / ph) * 100;
                    var d = (ofh * r) / 100;
                    nc += Math.abs(Math.floor(c - d));
                }
            }
            n_cord.push(nc);
        });

        var cc = $(this).find("#image-map area");

        cc.each(function (n) {
            //console.log($(this));
            //console.log("Image " + (i + 1) + "." + (n + 1) + " -> " + n_cord[n]);
            $(this).attr("coords", n_cord[n]);
        });
        
        
//      console.log("-------------");
    });
}


function changed_cordinates(){
    
}


function adjust_section(){
    var rW = $("#wrapper").width();
    //alert(rW);
    //alert(rW);
    resize_cordinates(rW); 
    hover_section();
    adjust_images();
}

function read_image(input, image, callback) {
    console.log(input);
    if (input.files && input.files[0]) {
        var reader = new FileReader();        
        reader.onload = function (e) {
            $(image).attr("src", e.target.result);
            if(typeof callback == "function"){
                callback( e );
            }
        }
        reader.readAsDataURL(input.files[0]);
    }
}
    
function hide_section_message(fade_out ){
    fade_out = ($.type(fade_out) == "boolean") ? fade_out:true;
    if(fade_out){
        show_message( "#section-message", "");
    }else {
        show_message( "#section-message", "<fade_out.true>");
    }
}
    
function show_message(container, message, color, hide){
    color = typeof color == "undefined" ? "green" : color;
    
    $(container).html( message );
    $(container).css( "color", color);
    $(container).css( "border-bottom", "2px solid " + color);
    
    if(message == "" || message.length == 0){
        //alert("ok");
        //$(container).fadeOut();
        $(container).css("display", "none");
    }else {

        if( message == "<fade_out.true>" ){
            $(container).css("display", "none");
        }else {
            $(container).append("<a id='button' href='javascript:' class='btn close' style='font-size:12px;border-color: "+color+"; padding: 2px 4px; position:absolute; right: 10px; top: 0px;' >X</a>");
            $(container).css("display", "block");
            //$(container).fadeIn();
        }
    }
    
    $(container).off("click");
    hide = $.type(hide) == "boolean" ? hide:true;
    if(hide){
        $(container).find("#button").on("click", function(){

            if( message == "<fade_out.true>" ) {
                $(container).css("display", "none");
            } else  {
                $(container).css("display", "none");
                $(container).fadeOut(function(){
                    //$(container).fadeOut();
                    //$(container).remove();
                });
            }
        });
    }
}


function section_message(message, color, hide){
    show_message( "#section-message", message, color, hide);
    
}

function section_success_message(message){
    section_message(message, "#4CAF50");
}

function section_warning_message(message, hide){
    section_message(message, "#ff9800", hide);
}

function section_info_message(message){
    section_message(message, "#2196F3");
}

function section_danger_message(message){
    section_message(message, "#f44336");
}

function display(elem, type){
    return $(elem).css("display", type);
}

function success_message(result){
    if(typeof result.message != "undefined"){
        if(typeof result.success !== "undefined"){
            if(result.success){
                section_success_message(result.message);
            }else {
                section_warning_message(result.message);
            }
        }else {
            section_warning_message(result.message);
        }
    }
}

function collapse_section(id) {
    alert("ok");
    if ($("#" + id).find(".section-body").css("display") == "block") {
        $("#" + id).find(".section-body").slideUp();
        $("#" + id).find(".section-footer").slideUp();
        $("#" + id).find(".collaps-btn img").css("margin-top", "-20px");
    } else {
        $("#" + id).find(".section-body").slideDown();
        $("#" + id).find(".section-footer").slideDown();
        $("#" + id).find(".collaps-btn img").css("margin-top", "0");
    }
}