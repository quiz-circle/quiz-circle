
jQuery.prototype.relation_list = function(config){
    var SELF = this;
    var $this = $(SELF);

    //self = jQuery.fn.init;
    var RL = { // ihc = image hover
        self : this,
        element : $this,
        DT : config,
        selected : [  ],

        push_selected_value : function ( val ){
            RL.selected.push( val );
        },

        after_loaded_list : function() {
            RL.enable_searching();
            RL.enable_adding();
            RL.push_selected_value_onchecked();
            RL.toggle_class_add_form();
        },

        toggle_class_add_form : function(){
            RL.element.find("#toggle-class-form, #toggle-class-form-searched").off("click");
            RL.element.find("#toggle-class-form, #toggle-class-form-searched").on("click", function(e){
                var search_text = $(this).attr( "data-searched-text" );
                if( search_text != "undefined" ){
                    RL.element.find("#__content__name__").val( search_text );
                    RL.element.find("#not-found-message").html("");
                }


                e.preventDefault();

                //alert( $(".___add-class-form___").attr('id') );
                if( RL.element.find(".___add-class-form___").css('display') == "none" ){
                    RL.element.find(".___add-class-form___").slideDown();
                }else {
                    RL.element.find(".___add-class-form___").slideUp();
                }
            });
        },

        set_selected_value : function (){
            //RL.selected = ($.type(RL.DT.selected) == "array") ? RL.DT.selected:[];
            console.log(RL.selected);
            RL.element.find(".__content__list__").find( "input[type='checkbox']" ).each(function(){
                if($(this).prop("checked")){
                    RL.selected.push( $(this).val() );
                }
            });
        },

        push_selected_value_onchecked : function (){
            RL.element.find( ".__content__list__" ).find( "input[type='checkbox']" ).off( "change" );
            $( ".__content__list__" ).find( "input[type='checkbox']" ).on("change", function(){
                RL.set_selected_value();
            });
        },

        // enable_adding : function (){
        //     RL.element.find("#__add__list__").off("click keypress");
        //     RL.element.find("#__add__list__").on("click keypress", function(e){
        //
        //         e.preventDefault();
        //
        //
        //         RL.DT["search-string"] = "";
        //         RL.element.find("#search-string").val("");
        //         //console.log("KEYBOARD EVENT NUMBER: "+e.which);
        //         var name = RL.element.find("#__content__name__").val();
        //         var parent = RL.element.find("#__content__parent__id__").val();
        //         if( name.length ) {
        //             _request_json({
        //                 url     :   "relation/add_content",
        //                 success :   function(r){
        //                     console.log(r);
        //                     if(r.success) {
        //                         if(r.insert_id){
        //                             RL.push_selected_value(r.insert_id);
        //                         }
        //                     }
        //                     RL._relation_list_main_panel();
        //                 },
        //                 data : {class_name : name, parent_id : parent, type: RL.DT.config.type}
        //
        //             });
        //         }else {
        //             alert("Please type a category name!");
        //         }
        //
        //     });
        // },


        enable_adding : function () {
            RL.element.find("#__add__list__,#__content__name__").off("click keypress");
            RL.element.find("#__add__list__,#__content__name__").on("click keypress", function(e){
                if( e.type == "click" ){
                    e.preventDefault();
                }

                if( ( e.type == "click" && e.target.id != '__content__name__' ) || e.which == 13 ){
                    RL.DT["search-string"] = "";
                    RL.element.find("#search-string").val("");
                    //console.log("KEYBOARD EVENT NUMBER: "+e.which);
                    var name = RL.element.find("#__content__name__").val();
                    var parent = RL.element.find("#__content__parent__id__").val();

                    _request_json({
                        url     :   "relation/add_content",
                        success :   function(r){
                            console.log(r);
                            if(r.success) {
                                if(r.insert_id){
                                    RL.push_selected_value(r.insert_id);
                                }
                            }
                            RL._relation_list_main_panel();
                        },
                        data : {class_name : name, parent_id : parent, type: RL.DT.config.type}

                    });

                }
            });
        },

        enable_searching:function (){
            RL.element.find("#search-string").off("keyup");
            RL.element.find("#search-string").on("keyup", function(e){
                //alert("ok");
                var search_test = $(this).val();
                RL.DT["search-string"] = search_test;
                RL._relation_list_main_panel();
            });
        },

        array_to_object:function (array){
            var obj = array.reduce(function(o, val) { o[val] = val; return o; }, {});
            return JSON.stringify(obj);
        },

        _relation_list_main_panel :function (){
            //RL.selected = RL.DT.selected;
            //RL.selected = ($.type(RL.DT.selected) == "array") ? RL.DT.selected:[];
            if( $.type(RL.DT.selected) == "array" ){
                $.each(RL.DT.selected, function(key){
                    RL.push_selected_value(RL.DT.selected[key]);
                })
            }

            RL.DT.selected = RL.array_to_object(RL.selected);
            console.log(RL.DT);
            //$.extend({}, RL.DT, options);

            _request_ajax({
                url : "relation/get_list",
                success : function(result){
                    RL.element.find("#main-panel").html(result);
                    RL.after_loaded_list();
                },
                data : RL.DT
            });
        },

        init : function(){
            RL._relation_list_main_panel();
            return RL;
        }
    }
    return RL.init();

}
