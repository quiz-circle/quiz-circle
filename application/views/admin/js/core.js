function _events(container, targets, event, prevent_default){
    var prevent_default = $.type(prevent_default) == "boolean" ? prevent_default:true;
    //$(".core-section").find(".btn, .reload-btn, .collaps-btn, .the-form ").each(function(){
    $(container).find(targets).each(function(){
        //$(this).off("click submit");
        $(this).off(event);
        $(this).on(event, function(e){
            //alert(e.type);
            if(prevent_default){
                e.preventDefault();
            }
            var config = {};
            
            var id =    $(this).attr("id");
            //alert(id);
            var id_seg = id.split("-");
            if(id_seg.length >= 2) {
                var id =  id_seg[1] ;
                var name = $("#" + id).find("#_core_section_name").val();

                if(typeof task_list[id_seg[0]] != "undefined" ) {
                    
                    
                    var task = task_list[id_seg[0]];
                    
                    var func = "";
                    if(typeof task[name] == "undefined") {
                        //default functionality for this event
                        func = task.func;
                        config = ( typeof task.config != "undefined") ? task.config : {};
                    }else {
                        //customize functionality for this event
                        if(typeof task[name] != "undefined") {
                            func = task[name].func;
                            config = ( typeof task[name].config != "undefined") ? task[name].config : {}
                        }
                    }
                    
                    if(typeof func == "function") {
                        config.id = id;    
                        func(id, config, e);
                    }
                }
            }
        });
    });
}





function xhr_progress(progress){
    var xhr = new window.XMLHttpRequest();
    xhr.upload.addEventListener("progress", function(e){
        if(e.lengthComputable) {
            var p = Math.round((e.loaded / e.total) * 100);            
            if(typeof progress == "function"){
                progress(p);
            }
        }
    });

    return xhr;    
} 

var s =0;
function location_hash(split_by){
    split_by = typeof split_by == "undefined" ? "=" : split_by
    var loc_hash = window.location.hash;
    loc_hash = loc_hash.replace("#", "");
    var _hash = loc_hash.split("&");

    var result = {};
    if(_hash.length == 1){
        var _h = _hash[0].split("=");
        if(_h.length >= 2){
            result[_h[0]] = _h[1];
        }
    }else {
        for(i = 0; i < _hash.length; i++) {
            var _h = _hash[i].split("=");
            if(_h.length >= 2){
                result[_h[0]] = _h[1]; 
            }
        }

    }
    return result;
}


function resize_cordinates(nw) {
    //alert(nw);
    var im = $("#wrapper #images");
    im.each(function (i) {

        //console.log(this);

        var pw = parseInt($(this).find("#width").val());
        var ph = parseInt($(this).find("#height").val());

        $(this).width(nw)

        $(this).find(".map").css("width", "100%");
        $(this).find("img").css("width", "100%");
        var w = $(this).find("img").width();
        var h = $(this).find("img").height();

        //console.log("--------------");
        var c = $(this).find("#image-map #original-coords");
//        console.log("Pewvious Dimension:" + pw + "x" + ph + "\n");
//        console.log("-------------------------");
//        console.log("Changed Dimension:" + w + "x" + h + "\n");
//        console.log("-------------------------------------");

        var f = 1;
        if (w > h) {
            f = pw / ph;
        } else {
            f = ph / pw;
        }
        var nh = Math.floor(nw / f);
//        console.log("Changed Dimension (nh):" + w + "x" + nh + "\n");
//        console.log("-------------------------------------");
//        console.log("\n\n");

        var ofh = ph - nh;
        var ofw = pw - nw;

        var n_cord = [];

        c.each(function (j) {
            var Type = $(this)[0].type;
            var nc = "";
            var crs = $(this).val();
            var cordinates = crs.split(",");
//            console.log("Original Cordrinates: " + (i + 1) + "." + (j + 1) + " -> " + cordinates);
//
//            console.log("------------------------------------------");

            for (m = 0; m < cordinates.length; m++) {

                nc += (m != 0) ? ", " : "";

                var c = cordinates[m];

                if (i % 2 == 0) {
                    var r = (c / pw) * 100;
                    var d = (ofw * r) / 100;
                    nc += Math.abs(Math.floor(c - d));
                } else {
                    var r = (c / ph) * 100;
                    var d = (ofh * r) / 100;
                    nc += Math.abs(Math.floor(c - d));
                }
            }
            n_cord.push(nc);
        });

        var cc = $(this).find("#image-map area");

        cc.each(function (n) {
            //console.log($(this));
            //console.log("Image " + (i + 1) + "." + (n + 1) + " -> " + n_cord[n]);
            $(this).attr("coords", n_cord[n]);
        });
        
        
//      console.log("-------------");
    });
}


function changed_cordinates(){
    
}


function adjust_section(){
    var rW = $("#wrapper").width();
    //alert(rW);
    //alert(rW);
    resize_cordinates(rW); 
    hover_section();
    adjust_images();
}

function change_image(id, after_print_flat_list){
    $(".image-listing ul li a").removeClass("active");
    //$(this).addClass("active");
    $("#"+id).addClass("active");


    $("#building .ims").css({
        "display": "none",
        "width": "100%",
    });
    //alert(e.target.id);

    $("#building ."+id).css({
        "display": "block",
        "width": "100%",
    });
    hover_section();
    adjust("#building ."+id+" #images");
    
    var image_id = $("." + id).find("#image_id").val();
    var building_id = $("." + id).find("#building_id").val();
    //alert(image_id);
    print_flat_list(sections, image_id, building_id, ref, after_print_flat_list);

}


function readURL(input, image) {
    
    if (input.files && input.files[0]) {

        var reader = new FileReader();
        
        if(typeof image == "function"){
            reader.onload = image
        }else {
            reader.onload = function (e) {
                $(image).html('<img src="' + e.target.result + '">');
            }
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function ajax_error_console(r){
    text = r.responseText;
    if($.type(text) == "string"){
        console.log( text.replace(/<\/?[^>]+(>|$)/g, "") );
    }else {

        console.log(r.responseText);
    }
}

function append_values(values) {
    var data = new FormData();
    for (var key in values) {
        if (values.hasOwnProperty(key)) {
            var vals = values[key];
            if( $.type(values[key]) == "object" ){
                vals = JSON.stringify( values[key] );
            }
            data.append( key, vals );
        }
    }
    return data;
}

function append_extra_values(data, values) {
    for (var key in values) {
        if (values.hasOwnProperty(key)) {
            var vals = values[key];
            if( $.type(values[key]) == "object" ){
                vals = JSON.stringify( values[key] );
            }
            data.append(key, vals);
        }
    }
    return data;
}

function _request_ajax(config) {

    var url = typeof config.url != "undefined" ? config.url : "";
    var success = typeof config.success != "undefined" ? config.success : "";
    var fr = typeof config.formRef != "undefined" ? config.formRef : "";
    var ed = typeof config.data != "undefined" ? config.data : { };
    var fd = typeof config.formData != "undefined" ? config.formData : { };
    var on_progress = $.type( config.on_progress ) == 'undefined' ? { } : config.on_progress;
    _ajax(fr, "admin/script/ajax/" + url, "ajax", success, ed, fd, on_progress);
}

function _request_json(config) {
    var url = typeof config.url != "undefined" ? config.url : "";
    var success = typeof config.success != "undefined" ? config.success : "";
    var fr = typeof config.formRef != "undefined" ? config.formRef : "";
    var ed = typeof config.data != "undefined" ? config.data : {};
    var fd = typeof config.formData != "undefined" ? config.formData : {};
    var on_progress = $.type(config.on_progress) == 'undefined' ? {} : config.on_progress;
    _ajax(fr, "admin/script/json/" + url, "json", success, ed, fd,on_progress);
}

function _ajax(form, url, dataType, callback, extraData, fd, progress) {
    var FD = {};
    
    var form = $(form);
    
    if ( form.length != 0 ) {
        var FD = new FormData(form[0]);
        FD = append_extra_values(FD, extraData);
    } else {
        FD = append_values(extraData);
    }
    
    
    var config = {
        url: base_url + url,
        data: FD, contentType: false,
        processData: false, type: "POST",
        error: ajax_error_console,
        success: callback
    }
    
    if (dataType == "json") {
        config.dataType = dataType;
    }
    
    if ($.type(progress) == "function") {
        config.xhr =  function(){
            return xhr_progress(progress);
        }
    }
    $.ajax(config);
}

function hexToRgb(hex) {
    //alert(hex);
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}
    
function rgb2hsv() {
    var rr, gg, bb,
        r = arguments[0] / 255,
        g = arguments[1] / 255,
        b = arguments[2] / 255,
        h, s,
        v = Math.max(r, g, b),
        diff = v - Math.min(r, g, b),
        diffc = function (c) {
            return (v - c) / 6 / diff + 1 / 2;
        };

    if (diff == 0) {
        h = s = 0;
    } else {
        s = diff / v;
        rr = diffc(r);
        gg = diffc(g);
        bb = diffc(b);

        if (r === v) {
            h = bb - gg;
        } else if (g === v) {
            h = (1 / 3) + rr - bb;
        } else if (b === v) {
            h = (2 / 3) + gg - rr;
        }
        if (h < 0) {
            h += 1;
        } else if (h > 1) {
            h -= 1;
        }
    }
    return {
        h: Math.round(h * 360),
        s: Math.round(s * 100),
        v: Math.round(v * 100)
    };
}


function rgbToHsl(r, g, b) {
    r /= 255, g /= 255, b /= 255;

    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, l = (max + min) / 2;

    if (max == min) {
        h = s = 0; // achromatic
    } else {
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

        switch (max) {
            case r:
                h = (g - b) / d + (g < b ? 6 : 0);
                break;
            case g:
                h = (b - r) / d + 2;
                break;
            case b:
                h = (r - g) / d + 4;
                break;
        }

        h /= 6;
    }

    return {h: Math.floor(h * 360), s: Math.floor(s * 100), l: Math.floor(l * 100)};
}


function isLight(hexCode){
    var rgb = hexToRgb(hexCode);
    
    //return true;
    return  (0.213 * rgb.r + 0.715 * rgb.g + 0.072 * rgb.b)  > (255 / 2);
        
}
    
function inverseColor(element, target){
    $(element).each(function(i){
        var jsColor = $(this).val();

        var color = "";
        console.log(isLight(jsColor)+ " " + i);

        if(isLight(jsColor)){
            color = "#000";
        }else {
            color = "#FFF";
        }
        $(target).eq(i).find("._back_color").css("color", color);

    });
}

function inverseColorPublic(element, target){
    $(element).each(function(i){
        var jsColor = $(this).val();
        //alert(jsColor);
        var color = "";
        console.log(isLight(jsColor)+ " " + i);

        if(isLight(jsColor)){
            color = "#222";
        }else {
            color = "#E2E2E2";
        }
        $(target).eq(i).css("color", color);

    });
}

function upload_file(file_input, config){
    

    if($.type(file_input) == 'object'){
        config = file_input;
        file_input = config.file_input;
    }
    
    if($.type(file_input) != "undefined" && $.type(config) == "object"){
        var data = $.type(config.data) == 'undefined' ? {} : config.data,
        fr = $.type(config.formRef) == 'undefined' ? "" : config.formRef,
        input = $.type(config.input) == 'undefined' ? {} : config.input,
        on_progress = $.type(config.on_progress) == 'undefined' ? {} : config.on_progress,
        fileRef = $.type(config.fileRef) != "undefined" ? config.fileRef : "",
        ed = $.type(config.data) != "undefined" ? config.data : {},
        upload_dir = $.type(config.upload_dir) != "undefined" ? config.upload_dir : {},
        form, x,i,f ,FD = {};
        
        if($.type(file_input) == "array"){
            data.upload_dir = JSON.stringify(upload_dir);
            data.input = JSON.stringify(file_input);
            
        }else {
            data.input = file_input;
            data.upload_dir = upload_dir;
        }
        
        form = $(fr);
                
        if (form.length != 0) {
            var FD = new FormData(form[0]);
            FD = append_extra_values(FD, data);
            FD.append("f", "dd");
            
        } else {
            var FD = new FormData();
            
            if( $.type(input[0]) != 'undefined' ){
                if( $.type(input[0].files) != 'undefined' ){
                    
                    for(x=0; x < input[0].files.length; x++ ){
                        if($.type(file_input) == 'array'){
                            for(i=0; i < file_input.length; i++){
                                FD.append( file_input[i] + "[]", input[0].files[x]);
                            }
                        }else {
                            FD.append( file_input + "[]", input[0].files[x]);
                        }
                    }
                }
            }
            FD = append_extra_values(FD, data);
        }
        

        
        var conf = {
            url: base_url + "script/json/uploadoperation/upload_files",
            data: FD, contentType: false,
            processData: false, type: "POST",
            error: ajax_error_console,
            success: function(r){
                if($.type(config.success) == "function"){
                    config.success(r)
                }
            },
            xhr: function(){
                return xhr_progress(on_progress);
            }
        }
        conf.dataType = 'json';
        $.ajax(conf);
    }
    
}