/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var ERRORS = "";


function errors(err){
    if( typeof(err) == "undefined" ){
        var Errs = ERRORS;
        if(Errs != ''){
            ERRORS = "<ul>";
                ERRORS += Errs;
            ERRORS += "</ul>";
            return ERRORS;
        }else {
            return false;
        }
    }else {
        ERRORS += "<li>" + err + "</li>";
    }
}

function re_arrange_question_list( ){
    $(".question-add-section .question-element").each( function( index ){
        var THIS = $(this);
        $(this).find("label.full-width-label strong").html("Question# " + (index + 1) );
        var input = $(this).find("input[type='text'],input[type='radio'],input[type='hidden']");
        input.each(function(indx){
            //alert( $(this).attr("name") );
            var ID = $(this).attr("id");

            if(ID == "opt-label" || ID == "opt-value"){
                //alert( ID );
                THIS.find("#" + ID).each( function(opt_index) {
                   $(this).attr("name", ID + "["+index+"]["+opt_index+"]")
                });
            }else {
                $(this).attr("name", ID +"[" + index + "]");
            }

        });
    });
}

function question_form(options, opt_type, wrap, index, related) {
    //related = $.type(related) == "object" ? related[index] : "";
    //console.log(related);

    var WRAP = wrap.split("*"), wrap_left, wrap_right;

    if(WRAP.length == 2 ){
        wrap_left = WRAP[0];
        wrap_right = WRAP[1];
    }else {
        wrap_left = "";
        wrap_right = wrap;
    }

    html = "<div class='question-element' style='text-align: left'>";
        html += "<a class='question-close-btn btn close' title='Remove this question from this quiz!'>remove</a>";
        html += "<div class='question-title'>";
            var qu_id = $.type(related) == "object" ? related.quest_id:"";
            html += "<label for='questions' class='full-width-label'><strong>Question# " + (index + 1) + "</strong> <em>("+ qu_id +")</em></label>";
            if( $.type(related) == "object" ){
                html += "<div class='' style='width: 100%; float:left; font-weight: bold'>" + related.question + "</div>";
                html += "<input type='hidden' id='questions' value='"+ related.question+" ' name='questions["+index+"]'>";
                html += "<input type='hidden' id='question_ids' value='"+ related.quest_id+"' name='question_ids["+index+"]'>";
            }else {
                html += "<input type='text' id='questions' placeholder='Type a question' style='width: 100%' name='questions["+index+"]'>";
            }


        html += "</div>";

        html += "<div class='question-answer'>";
            if( $.type(related) == "object" ) {
                html += "<div class='' style='width: 100%; float:left'>Answer</div>";
                html += "<div class='' style='width: 100%; float:left; font-weight: bold'>" + related.answer + "</div>";
                html += "<input type='hidden'  id='answers' value='" + related.answer + " ' name='answers[" + index + "]'>";
            }else {
                html += "<label for='answer' class='full-width-label'>Answer</label>";
                html += "<input type='text' id='answers' placeholder='Type the answer' style='' name='answers[]'>";
            }
        html += "</div>";

        html += "<label for='quest-title' style='margin-bottom: 5px' class='full-width-label'>Answer Options <a href='javascript:' id='hide-question-options' class='btn' style='padding: 2px' >Hide Options</a></label>";
        html += "<div class='question-options'>";


            var opt_type = options_labels(opt_type);

            for(var i=0; i < options; i++ ){
                var opt_label = i;
                opt_label = opt_type[i];
                var label = wrap_left + opt_label + wrap_right;
                var width = ( (wrap_left.length + wrap_right.length) * 10 + 15 ) +5+ "px";
                html += "<div class='question-option'>";
                    html += "<input type='hidden' id='opt-label' name='opt-label["+index+"]["+i+"]' value='" + label + "'>";
                    //html += "<div style='border:none; display : inline-block; text-align:left; padding-left:0; background:none'>" + label + "</div>";

                    html += "<label class='inputRadio mid' id='quiest-opt-option-value'>";
                        html += "<input tabindex='2' type='radio' id='opt-value-right-answer' name='opt-value-right-answer["+index+"]' value='"+i+"'>";
                        html += "<span class='radioButton'>" + label + "</span>";
                    html += "</label>";

                    html += "<input type='text' id='opt-value' name='opt-value["+index+"]["+i+"]' style='";
                    html += "font-size: 12px;display : inline-block;";
                    html += "' placeholder='Option "+(i+1)+"'><br>";
                html += "</div>";
            }

        html += "</div>";

    html += "</div>";
    html += "<br>";
    html += "<br>";
    return html;
}

function set_question_config( e  ){
    e.preventDefault();

    var data = $("#set-question-config");
    //var num_of_ans_opt = data.find("#num-of-ans-opt").val();
    var ans_opt_type = data.find("#ans-opt").val();
    var ans_opt_wrap = data.find("#ans-opt-wrap").val();


    var WRAP = ans_opt_wrap.split("*"), wrap_left, wrap_right;

    if(WRAP.length == 2 ){
        wrap_left = WRAP[0];
        wrap_right = WRAP[1];
    }else {
        wrap_left = "";
        wrap_right = ans_opt_wrap;
    }

    var opt_type = options_labels( ans_opt_type );

    //alert( ans_opt_type + ", " + ans_opt_wrap );

    $(".question-add-section .question-element").each( function(i){
        var option_length = $(this).find( ".question-option" ).length
        $(this).find( ".question-option" ).each(function(i){
            var opt_label = i;

            opt_label = opt_type[i];
            var label = wrap_left + opt_label + wrap_right;
            $(this).find("#opt-label").val( label );
            $(this).find(".inputRadio .radioButton").html( label );

            console.log( label );
        });
    });
}

function options_labels(type){

    switch(type){
        case "A":
            return ['A', 'B', 'C', 'D', 'E','F','G','H','I','J'];
        case "a":
            return ['a','b','c','d','e','f','g','h','i','j'];
        case "ক":
            return ['ক','খ','গ','ঘ','ঙ','চ','ছ','জ','ঝ','ঞ'];
        case "i":
            return ['i','ii','iii','iv','v','vi','vii','viii','ix','x'];
        case "১":
            return ['১','২','৩','৪','৫','৬','৭','৮','৯','১০'];
        default:
            return [1,2,3,4,5,6,7,8,9,10];
    }
    return false;
}
var focus_list = [];



function focus_to_error_field( focud_id, error_class ){
    $("." + error_class).css("cursor", "pointer");
    $("." + error_class).off("click");
    $("." + error_class).on("click", function(){
       $( focud_id ).css("transition", "none");
        $( focud_id ).focus();
        $( focud_id ).select();
        $( focud_id ).css("border-color", "magenta");
        setTimeout(function(){
            $( focud_id ).css("transition", "ease-out 2s");
            $( focud_id ).css("border-color", "#AAA");
        }, 4000);
        
    });
}

function validete_quiz_information( form_id ){
        $FORM = $( form_id );
        var condition = true;
        var focusing_id = "#content-title";
        if( $FORM.find(focusing_id).val() == "" ){
            var error_class = "content-title-error";
            focus_list.push({focus: focusing_id, error_reference: error_class});
            errors("<div class='" + error_class+ "'>You didn't provide a title!</div>");
            
            condition = false;
            
        }
        
        focusing_id = ".content-categories";
        var cat = $(focusing_id).find(" input[type='checkbox'] ");
        var hasCat = false;
        $.each(cat, function(index, element ){
            hasCat = hasCat || $(element).prop("checked");
        });

        if( !hasCat ){
            var error_class = "content-category-error";
            //focus_list.push({focus: focusing_id, error_reference: error_class});
            errors("<div class='"+error_class+"'>One or more category must be selected!</div>");
            condition = false;
        }
        
        focusing_id = "#full-mark";
        if( $FORM.find(focusing_id).val() == "" ){
            var error_class = "full-mark-error";
            focus_list.push({focus: focusing_id, error_reference: error_class});
            errors("<div class='"+error_class+"'>Please give a full marks!</div>");
            condition = false;
        }else {
            if( !$.isNumeric($FORM.find("#full-mark").val()) ){            
                focus_list.push({focus: focusing_id, error_reference: error_class});
                errors("<div class='"+error_class+"'>Full mark must be a number!</div>");
                condition = false;
            }
        }
        $FORM.find( "" );

    return condition;
}

function load_related_question(){
    _request_ajax({
        url : "_quiz_ajax/get_related_question",
        success : function( result ){
            $(".question-add-section .questions-form").html( result );
            enable_controls();
        },
        data: { quiz_id : quiz_id }
    });
}

if( action == "edit" ){
    load_related_question( );
}

$("#_add_quiz_and_questions").on( "submit" , function(e){
    e.preventDefault();
    ERRORS = "";
    //var data = new FormData( $(this)[0] );

    if( validete_quiz_information(this) ){
        hide_section_message(true);
        section_info_message( "Quiz Updating...!" );

        _request_json( {
            url : "_quiz_ajax/update",
            success : function(result){
                if( result.success && action == "add") {
                    window.location = base_url + "admin/quiz/edit/" + result.quiz_id;
                }else if(action == "edit") {
                    load_related_question();
                    section_success_message( "Quiz Updated!" );
                }
                console.log(result);
            },
            formRef : $(this),
            data: { _ref : "_ajax_", action : action, quiz_id: quiz_id }
        } );


    } else {
        var error_text = "<h4 style='color:#FFF; text-align:left; margin:0 0 10px 0 '>Check the following issue!</h4>";
        error_text += errors();
        section_danger_message(error_text);
        
        for(i =0; i< focus_list.length; i++){
            var f = focus_list[i];
            var focusing_id = f.focus;
            var error_class = f.error_reference;
            focus_to_error_field(focusing_id, error_class);
        }
    }
});