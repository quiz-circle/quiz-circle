<?php  $this->admin->header( $data ); ?>
                
    <div id="body">
        
        <style>
            .on-mouse-right-click {
                border: 1px solid #CCC;
                border-bottom: none;
            }
            .on-mouse-right-click a{
                width: 100%;
                float: left;
                padding: 5px;
                background-color: #F1EaE4;
                margin-top: 2px;
                font-size: 11px;
                text-decoration: none;
                color: #333;
            }
            .on-mouse-right-click a:hover{
                background-color: #0198b2;
                color: #000;
            }
        </style> 
        <button type="button" class="btn btn-editable" data-btn-id="45">Click Me</button>
        <button type="button" class="btn btn-editable">Add Me</button>
        <button type="button" class="btn btn-editable">Block Me</button>
        <button type="button" class="btn btn-editable" style="height:50px; width: 400px;">Hide Me</button>
        <button type="button" class="btn btn-editable" data-btn-id="45">Saif the boss</button>
    </div>
<script src="<?php echo $this->path->assets("common/js/theme.js"); ?>" type="text/javascript" ></script>
<script>
    

    
    $(".btn-editable").each(function(){
        $(this).on("contextmenu", function(e){
            e.preventDefault();
            show_btn_edit(this);
            return false;
        });
        $(this).on("mouseout", function(){
            hide_btn_edit(this);
        });
    });
    

    

    
    function delete_btn(){
        
    }
    
    function show_btn_editing_interfate(e){
        $("#" + e.contextID).remove();
        var BTN = e.targetBTN[0];
  
        console.log(e.targetBTN);
        console.log(BTN.offsetLeft);
        $(".btn-editing-form").remove();
        
        var EDITING_FORM  = $("<div/>")
        EDITING_FORM.addClass("btn-editing-form");
        
        EDITING_FORM.css({
            "background-color" : "#ddd",
            "width" : "250px",
            "height" : "300px",
            "float" : "left",
            "border" : "1px solid #CCC",
            "position" : "absolute",
            "left" : BTN.offsetLeft,
            "bottom" : BTN.offsetBottom,
        });
        
        EDITING_FORM.appendTo("#body");
    }
    
    function hide_btn_edit(self){
        //console.log(self);
        
    }
    
    function _hash() {
        function s4() {
          return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
    }
    
    //console.log(_hash());
    
    
</script>
<?php  $this->admin->footer( $data ); ?>