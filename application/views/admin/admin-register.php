<?php  $this->admin->header( data() ); ?>
    <?php
        function _vals($key, $value){
            return isset($value[$key]) ? $value[$key]:"";
        }
    ?>


    <div class="left" id="user-form" >
        <?php echo form_open_multipart(); ?>
            <?php if(isset($status)):?>
            <p><?php echo $status?></p>
            <?php endif;?>
            
            <?php if($action != "change_pass"):?>
            <div class="input">
                <div class="error"><?php echo form_error("username");?></div>
                <?php 
                
                $name = "username";
                $input_config = array(
                    'id' => $name,
                    'class' => $name,
                    'placeholder' => 'Type a username'
                );
                
                if($action == "edit"){
                    $input_config["disabled"] = "disabled";
                }
                
                
                echo form_label("Username", $input_config['id']);
                echo form_input($name, _vals($name, $value), $input_config);
                
                ?>
            </div>
            <div class="input">    
                <div class="error"><?php echo form_error("firstname");?></div>
                <?php 
                $name = "firstname";
                $input_config = array(
                    'id' => 'firstname',
                    'class' => 'firstname',
                    'placeholder' => 'Your First Name'
                );
                echo form_label("First Name", $input_config['id']);
                echo form_input($name, _vals($name, $value), $input_config) 
                ?>
            </div>
            
            <div class="input">
                <div class="error"><?php echo form_error("lastname");?></div>
                <?php 
                $name = "lastname";
                $input_config = array(
                    'id' => 'lastname',
                    'class' => 'lastname',
                    'placeholder' => 'Your last name',
                );
                echo form_label("Last Name", $input_config['id']);
                echo form_input($name, _vals($name, $value), $input_config) 
                ?>
            </div>
            
            <style>
                .profile-picture {
                    width: 130px;
                    float: left;
                    position: relative;
                    border: 1px solid green;
                    padding: 5px;
                    overflow: hidden;
                    z-index: 0;
                }
                
                .profile-picture .cancel-image {
                    padding: 5px;
                    font-family: "verdana", "arial", sans-serif;
                    background-color: #008080;
                    color: #FFF;
                    position: absolute;
                    right: 5px;
                    top: 5px;
                    line-height: 8px;
                    cursor: pointer;
                    display: none;
                    z-index: 0;
                }
                .profile-picture .cancel-image:hover {
                    background-color: magenta;
                }
                
                .profile-picture prof-pic-in ,
                .profile-picture img {
                    width: 100%;
                    z-index: 0;
                }
                
                .profile-picture img {                    
                    
                }
                
                .profile-picture .prof-pic-in {
                    overflow: hidden;
                }
                
                .profile-picture .cancel-image {
                }
            </style>
            <?php if(isset($user_id)):?>
            <div class="profile-picture">
                <div class="cancel-image" tabindex="0">x</div>
                <div class="prof-pic-in">
                    <input type="hidden" name="previous-pic" id="previous-pic" value="<?php echo $user->get_profile_image($user_id); ?>">
                    <img src="<?php echo $user->get_profile_image($user_id); ?>" alt="" style="width: 130px" id="image-preview">
                </div>
            </div>
            
            
            <div class="input">    
                <div class="error"><?php echo form_error("profile-pic");?></div>
                <?php
                
                    
                
                    $input_config = array(
                        'id' => 'profile-pic',
                        'class' => 'profile-pic'
                    );
                    echo form_label("Profile Picture", $input_config['id']);
                    echo form_upload("profile-pic", _vals("", $value), $input_config) 
                ?>
            </div>
            <?php endif; ?>
            <script>
                $("#profile-pic").on("change", function(){
                    read_image(  this ,"#image-preview" , function(){
                        $(".cancel-image").css("display", "block");
                        $(".cancel-image").off("click");
                        
                        $(".cancel-image").on("click", cancel_selected_image);
                    });
                });
                
                function cancel_selected_image(e){
                    e.preventDefault();                    
                    var prePic = $("#previous-pic").val();
                    $("#image-preview").attr("src", prePic);
                    $(".cancel-image").css("display", "none");
                    $("#profile-pic").val("");
                }
            </script>
            <?php if($action == "edit"):?>
            <div class="input">
                <div class="error"><?php echo form_error("lastname");?></div>
                <?php 
                //Explain($value);
                
                $name = "display_name";
                $input_config = array(
                    'id' => 'display_name',
                    'class' => 'display_name',
                    'placeholder' => 'Display Name',
                );
                echo form_label("Display Name", $input_config['id']);
                echo form_input($name, _vals($name, $value), $input_config) 
                ?>
            </div>
            <?php endif ?>
            
            
            <div class="input">    
                <div class="error"><?php echo form_error("email");?></div>
                <?php 
                $name = "email";
                $input_config = array(
                    'id' => 'email',
                    'class' => 'email',
                    'placeholder' => 'type an email address',
                    'authcomplete' => 'off',
                );
                echo form_label("Email", $input_config['id']);
                echo form_input($name, _vals($name, $value), $input_config); 
                
                $security_hash = $this->form_hash->get("_add_user_security_hsah_");
                echo form_hidden("hash", $security_hash, $input_config) 
                ?>
            </div>
            <?php endif; ?>
            
            <?php if($action == "change_pass"):?>
            <div class="input">    
                <div class="error"><?php echo form_error("old_password");?></div>
                <?php 
                    echo form_label("Old Password", "old_password");
                    echo form_password("old_password", "", 'placeholder="type your password" autocomplete="off" id="old_password"') 
                ?>
            </div>
            <?php endif;?>
            <?php if($action == "create" || $action == "change_pass"):?>
            <div class="input">    
                <div class="error"><?php echo form_error("password");?></div>
                <?php 
                    echo form_label("Password", "password");
                    echo form_password("password", _vals("password", $value), 'placeholder="type your password" autocomplete="off" id="password"') 
                ?>
            </div>
            <div class="input">    
                <div class="error"><?php echo form_error("password_repeat");?></div>
                <?php 
                    echo form_label("Confirm Password", "password_repeat");
                    echo form_password("password_repeat", "", 'placeholder="type your password" autocomplete="off" id="password_repeat"') 
                ?>
            </div>
            <?php endif;?>
            
            

            <?php
                $permission = false;
                if($action == "create"){
                    $permission = $auth->getPermission("permission", 1);
                }else if($action == "edit") {
                    $permission = $auth->getPermission("permission", 3);
                }
            
                if($permission){
                    $name = "role";
                    $input_config = array(
                        'id' => 'user_role',
                        'class' => 'user_role',
                    );
                    echo form_label( "User Role", $input_config['id'] ); 
                    echo form_dropdown($name, $user_roles, _vals($name, $value), $input_config);
                }
            ?>
            <div class="input">    
                <?php
                if($action == "create"): ?>
                <input type="submit" value="Register">
                <?php elseif($action == "edit"): ?>
                <input type="submit" value="Edit">
                <?php elseif($action == "change_pass"): ?>
                <input type="submit" value="Change Password">
                <?php endif;?>
            </div>
        </form>
    </div>
<?php $this->admin->footer( data() ); ?>