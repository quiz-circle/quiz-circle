<?php  $this->admin->header( data() );  ?>

<script>
    var content_type = "<?php echo uri_seg(4) ?>";
    var quiz_content_types = <?php echo json_encode( $quiz_content_types ) ?>;
</script>

    <?php
        $content_category_name = content_category_name(content_category_types(uri_seg(4)));
    ?>
    <div class="left" id="user-form-success" >
        <?php //Explain($questions); ?>

        <div class="core-section" id="delete_question" style="float: none;width: 300px; display: none">
            <div class="section-header">
                <div class="section-title">
                    Delete <?php echo uri_seg(4) ?>
                </div>
            </div>
            
            <div class="section-body">
                <form id="questions-delete-form" action="" method="post">
                    <input type="hidden" value="" id="deleting-id" name="deleting-id">
                    Are You Sure?
                    <div style="width: 100%; text-align: center" >
                        <button class="btn" type="submit">Ok</button>
                        <button class="btn delete_question_close">Cancel</button>
                    </div>
                </form>
            </div>
        </div>

<?php
    $data = array(
        'content_category_name' => $content_category_name
    );
    $this->admin->includes("content-listing/content-form", $data);
?>


        <form id="questions-list-form" action="" onsubmit="return false" method="post">
            <input type="hidden" value="<?php echo uri_seg(4); ?>" name="content_type" >
            <div class="content-header">
                <div class="filter">
                    <label class="inputRadio" id="basic-filter">
                        <input type="radio" name="filter[status]" value="ac" checked="">
                        <span class="radioButton">Active</span>
                    </label>
                    
                    <label class="inputRadio trash-btn" id="basic-filter">
                        <input type="radio" name="filter[status]" value="tr">
                        <span class="radioButton">Trash</span>
                    </label>
                </div>
                
                
                <div class="more-filter" style="display: inline-block; float:left">
                    <label style="display: inline-block; float: left; margin-left: 6px; font-size: 12px; margin-top: 6px; margin-right: 3px; color:#999 "><?php echo $content_category_name; ?></label>
                    <select name="filter-category" id="filter-category" style="padding: 3px; color: #555">
                        <option value="">All <?php echo uri_seg(4); ?> <?php echo $content_category_name; ?></option>
                        <?php $this->category->type = $quiz_content_types[uri_seg(4)]; $this->category->getList( $class_list_config ); ?>
                    </select>
                </div>
                
                <div class="search">
                    <input type="text" id="search-text" name="search-text" placeholder="Search..">
                </div>
                <a href="javascript:" class="btn add_question_open" >Add <?php echo uri_seg(4) ?></a>
                
            </div>
            <table class="content-list question-list">
                <thead >
                    <?php admin_view( "includes/content-listing/content-table-head", array( 'contents' => $questions, 'content_type' => uri_seg(4) ) ); ?>
                </thead>
                <tbody>
                    <?php admin_view( "includes/content-listing/content-table-body", array( 'contents' => $questions, 'content_type' => uri_seg(4) ) ); ?>
                </tbody>
            </table>
        </form>
        <?php //echo anchor(base_url("admin/create_user"), "+Add a User", array("class" => "btn")) ; ?>
    </div>
<script>
    var category_name = {
        quiz : "quiz-category",
        question : "question_category",
        notice : "notice-category",
    }


    $('#add_question').popup({
        transition: 'all 0.3s',
        background: true,
        color: "#666",
        opacity: 0.3,
        horizontal : 'center',
        vertical : 'center',
        beforeopen : function( box, btn ) {

            $( ".question-category-list" ).relation_list({
                table: "class",
                config: { "type" : quiz_content_types[content_type] },
                "search-string" : "",
                "content-name" : "Category",
                "select-name" : "categories[]",
                "select-id" : "categories",
            });
        }
    });



//        autoopen    : false,
//        background  :   true,
//        color       :   "#FFF",
//        type        : 'tooltip',






//    $('#question_config').popup( {
//        transition : 'all 0.3s',
//        background : true,
//        color : "#666",
//        opacity : 0.3,
//        //type: 'tooltip',
//        opentransitionend : open_question_init_form
//    });

    counter = 0;


    var sort_by = '';
    var sort_type = '';

    function enable_sorting(){
        $("table.content-list thead tr td").each(function(){
            $(this).off("click");
            $(this).on("click", function(){
                sort_by = $(this).attr("sort-by");
                sort_type = sort_type == "ASC" ? "DESC" : "ASC";

                if($.type(sort_by) != "undefined"){
                    sort_by = sort_by + " " + sort_type;
                    fetch_question_list();
                }
            });
        });
    }

    enable_sorting();

    function after_loaded(){
        counter++;
        console.log("\nQUESTION LIST -> Loaded " + counter + " time(s)...!\n");
//        enable_category_filter();

        $(".delete_question_open").each(function () {
            $(this).off("click");
            $(this).on("click", function(e){
                var question_id = $(this).attr("data-quest-id");
                $('#questions-delete-form').find("#deleting-id").val(question_id);
            });
        });



        $('#delete_question').popup({
            transition: 'all 0.3s',
            background: true,
            color: "#666",
            opacity: 0.3,
            horizontal : 'center',
            vertical : 'center',
            offsettop : "50",
            beforeopen : function( box, btn ) {
                console.log( btn );

                $('#questions-delete-form').off("submit");
                $('#questions-delete-form').on("submit", function(e){
                    e.preventDefault();
                    _request_json({
                        url : "_content/delete_parmanently",
                        success: function(result){
                            console.log(result);
                            success_message(result);
                            $('#delete_question').popup("hide");
                            fetch_question_list( after_loaded );
                        },
                        formRef : $(this), 
                        data:{ content_type : content_type }
                        
                    });
                });

            }
        });
    }

    function open_edit_question(id, btn){

        var ID = $(btn).attr("id");
        var cats = $("#_cats_" + ID).val();
        
        console.log(cats);
        
        //alert(cats);
        //alert(cats);

        $('#edit_question').popup( "toggle");



        $( ".question-category-list-edit" ).relation_list({
            table: "class",
            config: { "type": quiz_content_types[content_type] },
            "search-string":"",
            "content-name" : "Category",
            "select-name" : "categories[]",
            "select-id" : "categories",
            selected : JSON.parse(cats)
        });



        var box_width = $('#edit_question').width();

//        $('#edit_question').css("margin-top", ($(btn).offset().top+20) + "px");
//        $('#edit_question').css("margin-left", ($(btn).offset().left - box_width + 30 ) + "px");

        edit_question(id);
    }

    $("#questions-add-form").on("submit", function(e){
        e.preventDefault();
        _request_json({
            url : '_content/update',
            success : function(result){
                console.log(result);
                if(result.success){
                    success_message(result);
                    fetch_question_list();

                    //$('#add_question').popup("hide");
                }
            },
            formRef : $(this)
        });
    });

    function fetch_question_list( after ){

        var quest_data = {
            data_form : "__ajax__",
            sort_by : sort_by,
        };
        _request_ajax({
            url : '_content/get_body',
            success : function( result ){

                //console.log( result );
                $("#questions-list-form").find(".content-list tbody").html( result );

                after_loaded();
                if($.type(after) == "function"){
                    after();
                }
            },
            formRef : $("#questions-list-form"),
            data : quest_data
        });
    }
    //fetch_question_list( );



    function restore_question(id){
        fetch_question_list();
    }


    

    function edit_question(id){
        $("#questions-edit-form").find("#quest-title").val( "" );
        $("#questions-edit-form").find("#quest-answer").val( "" );
        _request_json({
            url : "_content/get",
            success : function( result ){
                console.log(result);
                set_editing_data(result);
                $("#questions-edit-form").off("submit");
                $("#questions-edit-form").on("submit", function(e){
                    e.preventDefault();
                    _request_json({
                        url : "_content/edit",
                        success : function( result ){
                            if( result.success ) {
                                success_message( result );
                                fetch_question_list();
                                $('#edit_question').popup("hide");
                            }
                        },
                        formRef : $(this),
                        data : { content_id : id, content_type : content_type },
                    });
                });
            },
            data: {content_id : id, content_type : content_type}
        });
    }


    function set_status(id, status){
        section_info_message("Processing...!");
        _request_json({
            url : "_content/set_status",
            success : function( result ){
                if( result.success ) {
                    fetch_question_list();
                }
                console.log(result);
                success_message(result);
            },
            data: { content_id: id, status: status, content_type: content_type }
        });
        return false;
    }

    function trash_question(id){
        set_status(id, 0);
    }

    function restore_question(id){
        set_status(id, 1);
    }

    $("#search-text").on("keyup", function(){
        fetch_question_list();
    });

    $("#basic-filter input[type='radio'] ").on("click", function(e){
        //var filterValue = $(this).val();
        fetch_question_list( );
    });

    $("#filter-category").on("change", function(){

        fetch_question_list( );
    });


</script>
<?php  $this->admin->footer( data() ); ?>
