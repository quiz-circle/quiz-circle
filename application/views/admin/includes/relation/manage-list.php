

<div class="main-list">
    <div class="__content__list__">
        <?php
            if( count($data) ){
                $this->tree->get_data( $data, $list_config );
            }else {
                ?>
        <div id="not-found-message"><strong><em><?php echo _post("search-string"); ?></em></strong> is not found!<br>You can create this item</div> <br>
                    <a href="javascript:" data-searched-text="<?php echo _post("search-string"); ?>" id="toggle-class-form">Create <strong><em><?php echo _post("search-string"); ?></em></strong></a>
                <?php
            }
        ?>
    </div>
    <?php if(count($data)):?>
    <h4 class=""><a href="javascript:" id="toggle-class-form" >Add New</a></h4>
    <?php endif;?>
    <div class="___add-class-form___" style="display: none">
        <label for="__content__name__">Name</label>
        <br>
        <input type="text" id="__content__name__">
        <br>
        <br>
        
        <?php if(isset($option_data)):  ?>
        <label for="__content__parent__id__">Parent</label>
        <br>
        <select id="__content__parent__id__">
            <option value="0">(No Parent)</option>
            <?php $this->tree->get_data( $option_data, $options_config ); ?>
        </select>
        <br>
        <br>
        <?php endif; ?>
        <a href="javascript:" class="btn" id="__add__list__">Add To List</a>

    </div>
</div>

<script>

</script>