<?php
    $data = array(
        'content_category_name' => $content_category_name,
        'action' => 'edit',
    );
?>

<div class="core-section" id="edit_question" style="display: none; width: 825px; float: none">
    <div class="section-header">
        <div class="section-title">
            Edit <?php echo uri_seg(4) ?>
        </div>
    </div>
    <div class="section-body">
        <form id="questions-edit-form" action="" method="post">
            <?php
                $this->admin->includes("content-listing/content-form-in", $data); 
            ?>
        </form>
    </div>
</div>


<div class="core-section" id="add_question" style="display: none; width: 825px; float: none">
    <div class="section-header">
        <div class="section-title">
            Add <?php echo uri_seg(4) ?>
        </div>
    </div>
    <div class="section-body">
        <form id="questions-add-form" action="" method="post">
            <?php
                $data['action'] = "add";
                $this->admin->includes("content-listing/content-form-in", $data);
            ?>
        </form>
    </div>
</div>