<?php if( count($questions) ): $index = -1; ?>

<?php //Explain( $questions );?>

    <?php foreach($questions as $question): $index++?>

        <div class='question-element' style='text-align: left'>
            <a class='question-close-btn btn close' title='Remove this question from this quiz!'>remove</a>
            <div class='question-title'>

                <label for='questions' class='full-width-label'><strong>Question# <?php echo $index+1; ?></strong> <em>(<?php echo $question->id() ?>)</em></label>

                <div class='' style='width: 100%; float:left; font-weight: bold'><?php echo $question->title() ?></div>
                <input type='hidden' id='questions' value='<?php echo $question->title() ?>' name='questions[<?php echo $index; ?>]'>
                <input type='hidden' id='question_ids' value='<?php echo $question->id() ?>' name='question_ids[<?php echo $index; ?>]'>
            </div>

            <div class='question-answer'>
                <div class='' style='width: 100%; float:left'>Answer</div>
                <div class='' style='width: 100%; float:left; font-weight: bold'><?php echo $question->answer() ?></div>
                <input type='hidden'  id='answers' value='<?php echo $question->answer() ?>' name='answers[<?php echo $index; ?>]'>
            </div>
            <label for='quest-title' style='margin-bottom: 5px' class='full-width-label'>Answer Options <a href='javascript:' id='hide-question-options' class='btn' style='padding: 2px' >Hide Options</a></label>

            <div class='question-options'>
                <?php $answer_options = $question->answer_options( $right_answer ); $i = 0;?>
                <?php foreach($answer_options as $label => $value ){  ?>
                <div class='question-option'>
                    <input type='hidden' id='opt-label' name='opt-label[<?php echo $index; ?>][<?php echo $i; ?>]' value='<?php echo $label; ?>'>
                    <label class='inputRadio mid' id='quiest-opt-option-value'>
                        <input tabindex='2' type='radio' id='opt-value-right-answer' name='opt-value-right-answer[<?php echo $index; ?>]' value='<?php echo $i; ?>' <?php echo $value == $right_answer ? "checked":"" ?> >
                        <span class='radioButton'><?php echo $label?></span>
                    </label>
                    <input type="text" id="opt-value" value="<?php echo $value; ?>" name='opt-value[<?php echo $index; ?>][<?php echo $i; ?>]' style="font-size: 12px;display : inline-block;" placeholder="Option <?php echo $i+1; ?>">
                </div>
                <?php $i++;} ?>

            </div>

        </div>
    <?php endforeach; ?>
<?php endif; ?>

<?php function s(){?>
<?php }?>
