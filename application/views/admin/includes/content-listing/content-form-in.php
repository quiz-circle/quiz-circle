<?php

    $content_cat_sec_class = ($action == "add") ? "question-category-list" : "question-category-list-edit" ;
    $content_type = uri_seg(4);
?>

<label for="quest-title"><?php echo ucfirst(uri_seg(4)) ?> Title</label><br>
<input type="text" id="quest-title" name="name" style="width: 800px">

<input type="hidden"  value="<?php echo uri_seg(4); ?>" name="content-type" >

<?php  if( $content_type == "question"): ?>
    <br><br>
    <label for="quest-answer">Answer</label><br>
    <input type="text" id="quest-answer" name="answer"  style="width: 450px">
<?php elseif($content_type == "notice"):?>
    <br><br>
    <label for="content-description">Description</label><br>
    <textarea style="min-width: 100%; max-width: 100%; min-height: 200px; max-height: 500px;" name="content-description" id="content-description"></textarea>
<?php elseif($content_type == "quiz"):?>
    <br><br>
    <label for="content-description">Full Marks</label><br>
    <input type="text" id="full-marks" name="full-marks"  style="width: 100px">
    <br>
    <span>Quiz Type</span>
    <label class="">
        <input type="radio" id="quiz" name="quiz-type" value="quiz" >
        <span class="">Quiz</span>
    </label>
    <label class="">
        <input type="radio" id="tutorial"  name="quiz-type" value="tutorial" >
        <span class="">Tutorial</span>
    </label>
<?php endif?>

<br>
<br>
<div style="width: 100%; float: left; text-align: right; position: relative; display: inline-block; ">
    <div class="core-section <?php echo $content_cat_sec_class ?>" style="width:260px; float:left;">
        <div class="section-header">
            <div class="section-title"><?php echo $content_category_name; ?></div>

        </div>
        <div class="section-body">
            <div class="list-header">
                <input type="text" name="" value="" id="search-string" placeholder="Search a category">
            </div>
            <div id="main-panel" >

            </div>
        </div>
    </div>
    <div class="btns" style="position: absolute; right: 0; bottom: 10px">
        <input type="submit" value="Done" class="btn" >
        <button class="btn <?php echo $action; ?>_question_close">Close</button>
    </div>
</div>

<script>
    
function set_editing_data( result ){        
    switch(content_type){
        case "question":
            $("#edit_question").find("#quest-title").val( result.quest_title );
            $("#edit_question").find("#quest-answer").val( result.answer );
            break;
        case "quiz":
            
            $("#edit_question").find("#quest-title").val( result.quiz_title );
            
            $("#edit_question").find("#full-marks").val( result.full_marks );
            
            if( result.quiz_type == "quiz" ){
                $("#edit_question").find("#quiz").attr("checked", true);
            }else {
                $("#edit_question").find("#tutorial").attr("checked", true);
            }
            
            

            break;
        default:
            $("#edit_question").find("#quest-title").val( result.content_title );
            $("#edit_question").find("#content-description").val( result.description );

    }
}
</script>