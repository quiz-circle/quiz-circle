<?php if( count($contents) ): 
        $content_type = (isset($content_type)) ? $content_type : "question";
        $content_category_type  = content_category_types( $content_type );
        foreach( $contents as $content ): ?>
        <tr>      
            <td>
                <input type="hidden" value='<?php echo json_encode( $content->categories( $content_category_type )); ?>'  id='_cats_<?php echo $content->hash; ?>'>
                <input id="<?php echo $content->hash; ?>" value="<?php echo $content->id; ?>" type="checkbox" name="checked_quest_id[]" >
            </td>
            <td class='question'>
                <label style="width: 100%" for="<?php echo $content->hash; ?>" title="<?php echo $content->title(); ?>">
                    <?php echo $content->title(); ?>
                </label>
                <div style="width: 100%; color: #999; font-size: 12px; display: inline-block; margin-top: 5px">
                <?php
                if( $count = count($content->categories( $content_category_type, "name" )) ){
                    echo "<strong>".( $count > 1 ? "Categories":"Category") . "</strong> : <em>" . implode(", ", $content->categories( $content_category_type, "name" )) . "</em>";
                }
                ?>
                </div>
            </td>

            <?php switch ( $content_type ) : case "quiz" : ?>
            <!-- Specific column for Quiz -->
            <td style='text-align: center'>
                <?php echo $content->full_marks; ?>
            </td>
            <td style='text-align: center'>
                <?php echo $content->quiz_type; ?>
            </td>
            
            <?php break; ?>
            <?php case "question" : ?>
            <!-- Specific column for question -->
            <td class='answer'><?php echo $content->answer(); ?></td>
            
            <?php break; ?>
            <?php case "tutorial" : ?>
            <!-- Specific column for tutorial -->
            
            <?php break; ?>
            <?php case "video" : ?>
            <!-- Specific column for video -->
            
            <?php break; ?>
            <?php case "paragraph" : ?>
            <!-- Specific column for paragraph -->
            
            <?php break; ?>
            <?php case "notice" : ?>
            <!-- Specific column for notice -->
            <td><?php echo get_content_starting($content->description, 300); ?>...</td>
            
            <?php break; ?>
            <?php default : ?>
            
            <?php endswitch; ?>
            
            <td style='font-size: 11px; color:#666;text-align: center'><?php echo $content->display_name; ?></td>
            <td style='font-size: 11px; color:#666;text-align: center'><?php echo $content->date_added; ?></td>
            <td style='font-size: 11px; color:#666;text-align: center'><?php echo $content->date_edited; ?></td>


            <?php switch ( $content->is_trashed() ):
                    case true:
            ?>
            <td style="text-align: center">
                <a href="javascript:" class="btn" onclick="restore_question(<?php echo $content->id; ?>)">
                    Restore
                </a>
            </td>
            <td style="text-align: center">
                <a href="javascript:" class="btn delete_question_open"  style="background-color: magenta"  data-quest-id="<?php echo $content->id; ?>" >
                    Delete
                </a>
            </td>
            <?php break; default :?>
            <td style="text-align: center">
                <?php
                    switch ($content_type){
                        case 'quiz' :
                            $edit_onclick = false;
                            $edit_class = "";
                            $edit_href = base_url("admin/quiz/edit/" . $content->id);
                            break;
                        default :
                            $edit_onclick = "onclick=\"open_edit_question( " . $content->id . ", this)\"";
                            $edit_href = "javascript:";
                            $edit_class = $content->hash. "_open";
                            
                            
                    }
                    
                ?>
                
                <a href="<?php echo $edit_href; ?>" class="btn <?php echo $edit_class; ?>" id="<?php echo $content->hash; ?>" <?php echo $edit_onclick ? $edit_onclick : "" ?> >
                    Edit
                </a>
            </td>
            <td style="text-align: center">
                <a href="javascript:" class="btn"  style="background-color: #e8a866"  onclick="trash_question(<?php echo $content->id; ?>)">
                    Trash
                </a>
            </td>
            <?php endswitch;?>

        </tr>
    <?php endforeach; ?>

<?php else : ?>
<tr style="text-align: center">
    <?php 
        $filter = _post("filter") ;
    ?>
    <?php if(isset($filter['status'])) ?>
    <?php if($filter['status'] == "tr" ): ?>
        <td colspan="20">Trash is empty!</td>
    <?php else: ?>
        <td colspan="20">No Data Found!</td>
    <?php endif; ?>
</tr>
<?php endif; ?>