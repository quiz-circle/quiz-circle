<?php //Explain( $_POST ); ?>
<?php 
    $list_type = "";
    if( isset($_POST['filter']['status']) ){
        $list_type = $_POST['filter']['status'];
        
    }
    
    $content_type = (isset($content_type)) ? $content_type : "question";
    
    $content_category_type  = content_category_types( $content_type );
?>

<?php foreach( $questions as $question ): ?>
    <tr>      
        <td>

            <input type="hidden" value='<?php echo json_encode( $question->categories( $content_category_type )); ?>'  id='_cats_<?php echo $question->hash; ?>'>
            <input id="<?php echo $question->hash; ?>" value="<?php echo $question->id; ?>" type="checkbox" name="checked_quest_id[]" >
        </td>
        <td class='question'>
            <label for="<?php echo $question->hash; ?>" title="<?php echo $question->title(); ?>">
                <?php echo $question->title(); ?>
            </label>
        </td>
        
        
        <td class='answer'><?php echo $question->answer(); ?></td>
        <td style='font-size: 11px; color:#666;text-align: center'><?php echo $question->display_name; ?></td>
        <td style='font-size: 11px; color:#666;text-align: center'><?php echo $question->date_added; ?></td>
        <td style='font-size: 11px; color:#666;text-align: center'><?php echo $question->date_edited; ?></td>
        
            
        <?php switch ( $question->is_trashed() ):
                case true:
        ?>
        <td style="text-align: center">
            <a href="javascript:" class="btn" onclick="restore_question(<?php echo $question->id; ?>)">
                Restore
            </a>
        </td>
        <td style="text-align: center">
            <a href="javascript:" class="btn delete_question_open"  style="background-color: magenta"  data-quest-id="<?php echo $question->id; ?>" >
                Delete
            </a>
        </td>
        <?php break; default :?>
        <td style="text-align: center">
            <a href="javascript:" class="btn <?php echo $question->hash; ?>_open" id="<?php echo $question->hash; ?>" onclick="open_edit_question(<?php echo $question->id; ?>, this)">
                Edit
            </a>
        </td>
        <td style="text-align: center">
            <a href="javascript:" class="btn"  style="background-color: #e8a866"  onclick="trash_question(<?php echo $question->id; ?>)">
                Trash
            </a>
        </td>
        <?php endswitch;?>

    </tr>
<?php endforeach; ?>