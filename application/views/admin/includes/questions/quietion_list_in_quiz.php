<table class="content-list question-form-list" style="margin-top: 10px;">
    <thead>
        <tr>
            <td><input type="checkbox" value="all" name="all-question-selected"></td>
            <td>Question</td>
            <td>Answer</td>
        </tr>
    </thead>
    <tbody>
    <?php foreach( $questions as $question ): ?>
        <tr>      
            <td style="text-align: center">
                <input class="question_id" id="<?php echo $question->hash; ?>" value="<?php echo $question->id; ?>" type="checkbox" >
                <input class="question_title" value="<?php echo $question->title(); ?>" type="hidden"  >
                <input class="question_answer" value="<?php echo $question->answer(); ?>" type="hidden"  >
            </td>
            <td class='question'>
                <label for="<?php echo $question->hash; ?>" title="<?php echo $question->title(); ?>">
                    <?php echo $question->title(); ?>
                </label>
            </td>
            <td class='answer'><?php echo $question->answer(); ?></td>

<!--        
            <td style='font-size: 11px; color:#666;text-align: center'><?php echo $question->display_name; ?></td>
            <td style='font-size: 11px; color:#666;text-align: center'><?php echo $question->date_added; ?></td>
            <td style='font-size: 11px; color:#666;text-align: center'><?php echo $question->date_edited; ?></td>
-->

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>