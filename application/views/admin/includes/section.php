<style>

    
</style>

<?php 
if(isset($hash)):  $view_path = realpath( APPPATH . "views" ); ?>
<div class="core-section <?php echo isset( $name ) ? $name : ""; ?>" id="<?php echo $hash?>" data-name="<?php echo isset( $name ) ? str_replace(array("-"," "), array("_","_") ,$name) : ""; ?>">
    <?php if($print_form):?>
    <form action="" method="post" id="the_form-<?php echo $hash?>" class="the-form" enctype="maltipart/form-data">
    <?php endif; ?>
        <input type="hidden" value="<?php echo $name; ?>" id="_core_section_name" >

        <div class="section-header">
            <div class="section-name">
                <?php echo isset( $title ) ? $title : ""; ?>
            </div>

            <?php if(isset($collaps_btn)): ?> 
                <?php if($collaps_btn === true): ?> 
                    <div class="collaps-btn" id="collaps-<?php echo $hash?>">
                        <img src="<?php echo $this->admin->images("up-down.png")?>" alt=""/>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if(isset($relode_btn)): ?> 
                <?php if($relode_btn === true): ?> 
                    <div class="reload-btn" id="reload-<?php echo $hash?>">
                        <img src="<?php echo $this->admin->images("refresh.png")?>" alt=""/>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

        </div>
        <input type="hidden" value="<?php echo $body; ?>" id="_core_body" >
        <div class="section-body">
            <?php
                if(isset($body)):
                    if(file_exists($view_path . "/" . $body. ".php")){
                            if(isset($_data)):
                                $this->load->view($body, $_data);
                            else:
                                //$this->load->view($body);
                            endif;
                    }
                endif;
            ?>
        </div>
        <?php if(isset($footer)): ?> 
            <div class="section-footer" >
                <div class="section-footer-in" >
                    <input type="hidden" value="<?php echo $footer; ?>" id="_core_footer" >
                    <?php
                        if(file_exists($view_path . "/" . $footer. ".php")){

                            if(isset($_data)):
                                $this->load->view($footer, $_data); 
                            else:
                                $this->load->view($footer);
                            endif;
                        }
                    ?>
                </div>
            </div>
        <?php endif; ?>
    <?php if($print_form):?>
    </form>
    <?php endif; ?>
</div>
<?php endif; ?>