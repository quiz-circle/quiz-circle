<?php

?>
<style>
    .basic-filters {
        padding: 0;
        margin: 0;

        float: left;
        border-bottom: 2px solid green;
        height: 25px;
    }
    
    .basic-filters li {
        float: left;
        display: inline-block;
        border: 1px solid #FFF;
        
    }
    
    .basic-filters li a{
        padding: 2px 8px;
        float: left;
    }
    
    .basic-filters li.active {
       height: 25px;
       z-index: 5;
       background-color: #FFF;
       border: 1px solid green;
       border-bottom: 0;
       
    }
    .basic-filters li.active a{
        padding-bottom: 6px;
    }
    
    .table-heading {
        width: 100%;
        border: 1px solid #666;
        border-bottom: none;
        padding: 8px 5px;
        display: block;
        float: left;
        font-size: 16px;
        background-color: #666;
        color: #FFF;
    }
    
    .pagination {
        text-align: center;
        width: 100%;
        float: right;
        margin: 10px 0;
    }
    
    #pagination {
        display: inline-block;
        margin: 0;
        padding: 0;
        list-style-type: none;
        float: right;
    }
    
    #pagination li {
        float: left;
        width: 25px;
        height: 25px;
        margin-left: 8px;
        text-align: center;
    }
    
    #pagination li a,
    #pagination li span {
        border: 1px solid #555;
        float: left;
        width: 100%;
        height: 100%;
        color: #333;
        line-height: 25px;
        text-decoration: none;
    }
    
    
    #pagination li.active {
        background-color: #4bbe85;
    }
    
    #pagination li.active span{
        color : #555;
    }
    
    .selected-actions {
        
    }
    
    .selected-actions select {
        width: 150px;
        font-size: 12px;
    }
    
    .selected-actions selected-action-label {
        
    }
    
    input.btn {
        font-size: 12px;
        padding: 6px 14px;
        cursor: pointer;
    }
    
    .search-box {
        width: 100%;
        float: left;
        height: 0;
    }
    .search-box form {
        height: 100%;
        display: block;
        float: right;
        margin-top: 10px;
    }
    
    .search-box form input[type='text'] {
        width: 180px;
        font-size: 15px;
        padding: 4px 8px;
    }
    
    .search-box form input.btn {
        margin: 0;
    }
    
    
    
    
    
    /*==========================*/
        .content-category-wrap {
        max-height: 300px;
        overflow-y: scroll;
        overflow-x: hidden;
        float: left;
        width: 100%;
    }
    
    .content-category {
        padding: 0;
        margin: 0;
        float: left;
        width: 100%;
        list-style: none;
    }
    
    .content-category li {
        border: 1px solid #ccc;
        background-color: #eee;
        width: 100%;
        position: relative;
        padding: 0;
        margin: 0;
        padding: 0 6px;
        margin-bottom: 2px;
    }
    
    .content-category li input[ type='checkbox' ],
    .content-category li span{
        display: inline-block;
        padding: 0;
        margin: 0;
        
    }
    
    .content-category li input[ type='checkbox' ]{
        float: left;
    }
    
    .content-category li span{
        padding: 2px 0;
    }
    
    .content-category li span label{
        padding: 2px 4px;
/*        
        position: absolute;
        background-color: #002166;
*/
    }
    
    .content-category li input[ type='checkbox' ]{
        display: inline-block;
        height: 20px;
        width: 20px;
        padding: 0;
        margin: 0;
    }
    
    /*==================================*/
    .category-list-section {
        position: relative;
    }
    
    .category-list-section .class-list-showing {
        position: relative;
        z-index: 1000;
    }
    
    .category-list-section .class-list-showing-in {
        padding: 20px;
        padding-top: 15px; 
        width: 230px;
        background-color: #FFF;
        border-color: #911991;
        border: 1px solid;
        display: none;
        float: left;
        position: absolute;
        top: 100%;
        right: 0;
        z-index: 1000;
    }
    
    .class-box-close {
        border-radius: 50%;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 15px;
        right: 15px;
        float: left;
        border: 1px solid #999;
        cursor: pointer;
        text-align: center;
        line-height: 17px;
        font-size: 12px;
        color: #999;
        outline: none;
        text-decoration: none !important;;
    }
    
    .class-box-close:hover {
        box-shadow:inset 0 0 20px magenta;
        color:#555;
        border-color: #555;
    }
    
    .class-list-showing-in {
        text-align: center;
    }
    
    .class-list-showing-in img {
        width: 30%;
        opacity: 0.7;
    }
    
</style>


<div  class="" >
    <?php $this->list_config->filters_html( array(
            'form_input' => array('type' => "text"),
        ) 
    ); 
    ?>
    <div class="search-box">
        <?php $this->list_config->search_html( $search_key, $this->input->get( $search_key )  ); ?>
    </div>
    <form action="" method="post">

        <?php $this->list_config->selected_option_views(array("class"=> "selected-actions"),  $action_selected )?>

        <div id="basic-filters-and-pagination">
        <?php 
        if(isset($identifier_list)):
            
            if(is_array($identifier_list)):
                    $active_link  = strlen($this->uri->segment(1)) > 0 ? $this->uri->segment(1)."/":"";
                    $active_link .= strlen($this->uri->segment(2)) > 0 ? $this->uri->segment(2)."/":"";
                    $active_link .= strlen($this->uri->segment(3)) > 0 ? $this->uri->segment(3)."/":"";
                    //_alert($content_type);
                    if($this->uri->segment(4)  == $content_type)
                        $active_link .= $this->uri->segment(4)."-all/";
                    else 
                        $active_link .= strlen($this->uri->segment(4)) > 0 ? $this->uri->segment(4)."/" : $content_type . "-all/";
                    //$showing = strlen($this->uri->segment(4)) > 0 ? $this->uri->segment(4):"all";
                    //echo $active_link;
            ?>

            <div class="pagination">
                <?php echo $pagination; ?>
                <ul class="basic-filters">
                    <?php
                    foreach($identifier_list as $title => $filter):
//                        _alert($filter['url']);
//                        _alert($active_link);
                    ?>
                    <li class="<?php echo  $filter['url'] == $active_link ? "active ":" " ?>" ><a href="<?php echo isset($filter['url']) ? base_url($filter['url']):"" ?>" 
                        <?php echo isset($filter['filter-ref']) ? 'data-filter-ref ="' .$filter['filter-ref'] .'"' : "" ?>> 
                        <?php echo $title .( isset($filter['count_where']) ? "(" . $this->post->item_count( $filter['count_where']). ")" : "" ); ?>
                        </a> 
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php endif;
        endif;
        ?>
        </div>
        <div class="table-heading">
            Showing <?php echo ucfirst($showing) ?> Pages
        </div>
        <?php if( $showing == 'trash' ):
            //Explain($contents)
            ?>
            <?php if(is_array($contents)):?>
            <table class="content-list">
                <thead>
                    <tr>
                        <td><input type="checkbox" name="all-checked"></td>
                        <td>#</td>
                        <td>Title</td>

                        <?php if(count($contents) > 0):?>
                        <?php if(isset($contents[0]['trashed_date'])):?>
                            <td>Deleted Date</td>
                        <?php endif;?>
                        <?php endif;?>

                        <td colspan="2">Actions</td>
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($contents)):$counter = 0;?>
                    <?php foreach($contents as $item):$counter++?>
                    <tr>
                        <td> <input type="checkbox" name="content_ids[]" value="<?php echo  $item['id']; ?>" > </td>
                        <td><?php echo $counter; ?></td>
                        <td><?php echo $item['title']?></td>

                        <?php if(count($contents) > 0):?>
                        <?php if(isset($item['trashed_date'])):?>
                        <td><?php echo format_date($item['trashed_date']); ?></td>
                        <?php endif;?>
                        <?php endif;?>
                        <td>Delete permanently</td>
                        <td>Restore</td>
                    </tr>
                    <?php endforeach;?>
                    <?php else:?>
                    <tr>
                        <td colspan="50">No Data Found!</td>
                    </tr>
                    <?php endif;?>
                </tbody>
            </table>
            <?php endif;?>
        <?php else:?>
        
        <table class="content-list">
            <thead>
                <tr>
                    <td><input type="checkbox" name="all-checked"> </td>
                    <td>#</td>
                    <td>Title</td>
                    <td>Body</td>
                    <td>Category</td>
                    <td>tags</td>
                    <td colspan="3">Actions</td>
                </tr>
            </thead>
            <tbody class="post-contents">
                <?php if(is_array($contents)):?>
                <?php if(count($contents)):$counter = 0;?>
                <?php foreach($contents as $item):$counter++; $_unique_id = _unique_hash(); ?>
                
                <tr>
                    <td> <input type="checkbox" name="content_ids[]" value="<?php echo  $item['id']; ?>" > </td> 
                    <td><?php echo $counter; ?></td>
                    <td><?php echo $item['title']?></td>
                    <td><?php echo get_content_starting($item['body'])?></td>
                    <td class="category-list-section">
                        <a class="btn select-categories" href="javascript:"  id="show_classes-<?php echo $_unique_id?>" data-list-type="category" data-content-id="<?php echo $item['id']?>" style="width: 60px; text-align: center">Add To</a>
                            <div class="class-list-showing-in" id="category-<?php echo $_unique_id?>" style="display: none">
                        <div class="class-list-showing">
                            <div class="class-list-showing-in" id="category-<?php echo $_unique_id?>" style="display: none; text-align: center">
                            </div>
                        </div>
                    </td>
                    <td>tags</td>
                    
                    <!-- ########--------Actions-------######## -->
                    <td>
                        <a href="<?php echo  base_url("admin/posts/edit/" . $item['content_type'] . "/" . $item['id'] ); ?>" class="btn">Edit</a>
                    </td>
                    <td><a href="" class="btn">Delete</a></td>
                    <td><a href="" class="btn">View</a></td>
                    
                </tr>
                
                <?php endforeach;?>
                <?php else:?>
                <tr>
                    <td colspan="50">No Data Found!</td>
                </tr>
                <?php endif;?>
                <?php endif;?>
            </tbody>
        </table>
        <?php endif; ?>
        <?php $this->list_config->selected_option_views(array("class"=> "selected-actions"), $action_selected)?>
    </form>
</div>


<div class="pagination">
    <?php echo $pagination; ?>
</div>

<script>
    task_list.show_classes = {func: collaps_class_list};
    task_list.assign_class_to_news = {func: assign_class_to_news};
    
    
    function enable_events(){
        _events(".post-contents", ".select-categories", "click");
        _events(".post-contents", ".class_assign", "click");
    }
    
    function assign_class_to_news(id){
        var active_ids = [];
        $("#category-" + id + " li input:checked").each(function(){
            active_ids.push($(this).val());
        });
        var content_id = $("#content_id" + id).val();

        if(active_ids.length > 0){
            _request_json({
                url : 'save/class_relations',
                data : {content_id : content_id, class_id : JSON.stringify( active_ids )},
                success : function(result){
                    console.log(result);
                    collaps_class_list(id);
                }
            });
        }else {
            alert("You have to must select at least one category!");
        }
    }
    
    function collaps_class_list(id, c, e){
        
        var class_type = $("#show_classes-" + id).attr("data-list-type");
        var content_id = $("#show_classes-" + id).attr("data-content-id");
        
        $( ".class-list-showing-in").css("display", "none");
        
        $( "#"+class_type + "-" + id).css("display", "block");
        $( ".class-list-showing-in").css("text-align", "center");
        loading_gear( "#"+class_type + "-" + id );
        //$( ".class-list-showing-in").css("text-align", "left");
        //alert(e.target.id );
        _request_ajax({
            url : "loadview/get_classes",
            data : {class_type : class_type, content_id:content_id, list_id : id},
            success : function(result){
                $( "#"+class_type + "-" + id).html(result);
                enable_events();
            }
        });
    }
    
    enable_events();
</script>