<?php
//echo APPPATH . "views/public/includes/page-templates";

$templates = get_dir(APPPATH ."views/public/includes/page-templates/", "file");

$tempt = trim($this->post->get_input( 'page-template' ));
//_alert($tempt);
?>

<select name="page-template" id="page-template" >
    <option value="">Default</option>
<?php
if( is_array( $templates ) ){
    foreach( $templates as $template ) {
        $template_file = $template;
        $file_template = ucfirst(str_replace(array(".php", "-", "_"), array("", " ", " "), $template));
        //echo $file_template . "<br>";
        $value = str_replace(".php", "", $template_file);
        //_alert($value);
        ?> 
        <option value="<?php echo $value ?>" <?php echo $tempt == $value ? "selected":""?> ><?php echo $file_template; ?></option>
        <?php
    }
}
?>
</select>