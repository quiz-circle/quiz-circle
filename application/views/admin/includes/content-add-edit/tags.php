
<style>
    .tag-list {
        width: 100%;
        float: left;
    }
    
    .tag-list .tag-item-each {
        position: relative;
        border: 1px solid #CCC;
        display: inline-block;
        padding: 5px;
        background-color: #E1E1E1;
        margin-left: 1px;
        margin-right: 1px;
    }
    
    .tag-list .related-tags {
        
    }
    
    .tag-list .tag-item-each .tag-name{
        float: left;
    }
    
    .remove-tag {
        border-radius: 50%;
        width: 15px;
        height: 15px;
        top: 2px;
        right: 2px;
        float: left;
        border: 1px solid #999;
        cursor: pointer;
        text-align: center;
        line-height: 12px;
        font-size: 11px;
        color: #999;
        outline: none;
        margin-left: 3px;
        text-decoration: none !important;;
    }
    
</style>

<div class="tag-list">
    
<?php
$this->category->type = "tag";
$tag_config['plain_list'] = true;

if( !$this->input->post() ){

    $this->category->print_related_list( $tag_config, $content_id );
}else {
    $this->category->print_input_tag_list(  );
    $this->category->print_input_new_tag_list(  );
}

//$this->category->getList( $tag_config );

?>
    
</div>

<input type="text" style="padding:4px 4px; font-size: 12px; margin-top: 5px; " id="tag-name">
<input type="submit" value="Add" class="btn" style="margin: 0; margin-left: 5px; margin-top: 5px;" id="trigger-tags" >

<script>
    
    function inArrayCaseInsensitive(needle, haystackArray){
        //Iterates over an array of items to return the index of the first item that matches the provided val ('needle') in a case-insensitive way.  Returns -1 if no match found.
        var defaultResult = -1;
        var result = defaultResult;
        $.each(haystackArray, function(index, value) { 
            if (result == defaultResult && value.toLowerCase() == needle.toLowerCase()) {
                result = index;
            }
        });
        return result;
    }

    function enable_removing(){
        $(".tag-list .tag-item-each").each(function(i){
            var self = this;
            $(this).find(".remove-tag").on("click", function(){
                $(self).remove();
            });
        });
    }
    
    enable_removing();
    
    function tag_exists_in_section(tag_name){
        var list = [];
        $(".tag-list .tag-name").each( function(){
            var name = $(this).html();
            list.push(name.trim());
        });
        //console.log( inArrayCaseInsensitive(tag_name, list) );
        return inArrayCaseInsensitive(tag_name.trim(), list) > -1;
    }
    
    $("#trigger-tags").on("click", function(e){
        e.preventDefault();
        var tag = $("#tag-name");
        var tag_name = tag.val()
        
        if(tag.length > 0) {
            var append = '<div class="tag-item-each related-tags">';
                append += '<input type="hidden" name="content-new-tags[]" value="' + tag_name + '" >';
                append += '<div class="tag-name">' + tag_name.trim() + '</div>';
                append += '<div class="remove-tag">x</div>';
            append += '</div>';
            if( !tag_exists_in_section(tag_name) ){
                $(".tag-list").append(append);
                enable_removing();
            }
        }
    });
    
</script>