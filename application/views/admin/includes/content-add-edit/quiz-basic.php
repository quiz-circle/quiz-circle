<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="input-box content-title" style="display: inline; float: none">
    <label for="content-title" class="full-width-label">Quiz Title</label>
    <input type="text" name="content-title" id="content-title" placeholder="Type quiz title" value="<?php echo $this->_quiz->get_input("content-title"); ?>">
</div>

<div class="input-box content-description" style="display: inline; float: none">
    <label for="description" class="full-width-label">Description</label>
    <textarea id="description" name="content-description" placeholder="Type the description"><?php echo $this->_quiz->get_input("content-description"); ?></textarea>
</div>

<div class="input-box full-mark" style="display: inline; float: none">
    <label for="full-mark" class="full-width-label">Full Mark</label>
    <input type="text" name="full_mark" id="full-mark"  placeholder="type the full marks" value="<?php echo $this->_quiz->get_input("full_marks"); ?>">
</div>
<div class="input-box quiz-type" style="display: inline; float: none">
    <div class="label">Quiz Type</div>
    <label ><input type="radio" name="quiz-type" value="quiz" <?php echo $this->_quiz->get_input("quiz_type") == "quiz" ? "checked":""; ?> >Quiz</label>
    <label ><input type="radio" name="quiz-type" value="tutorial" <?php echo $this->_quiz->get_input("quiz_type") == "tutorial" ? "checked":""; ?> >Tutorial</label>
</div>
