<style>
    
    .content-category-wrap {
        max-height: 200px;
        overflow-y: scroll;
        overflow-x: hidden;
    }
    
    .content-category {
        padding: 0;
        margin: 0;
        float: left;
        width: 100%;
        list-style: none;
    }
    
    .content-category li{
        border: 1px solid #ccc;
        background-color: #eee;
        width: 100%;
        position: relative;
        padding: 0;
        margin: 0;
        padding: 5px 6px;
        margin-bottom: 2px;
    }
    
    .content-category li input[ type='checkbox' ],
    .content-category li span{
        display: inline-block;
        padding: 0;
        margin: 0;
    }
    
    .content-category li span{
        padding: 0
    }
    
    .content-category li span label{
        padding: 0;
        padding-left: 5px;
        padding-top: 2px;

    }
    
    .content-category li input[ type='checkbox' ]{
        display: inline-block;
        height: 20px;
        width: 20px;
        padding: 0;
        margin: 0;
        float: left;    
    }
</style>
<?php 
    $cat_config['active_data'] = isset($active_data) ? $active_data : $this->post->get_input("categories");
    $this->category->type = 'quiz-category';
?>

<div id="main-panel" >

</div>

<!--<div class="content-category-wrap">  -->
<!--    <ul class="content-category">    -->
<!--        --><?php //$this->category->getList($cat_config); ?>
<!--    </ul>-->
<!--    -->
<!--</div>-->
<!---->
