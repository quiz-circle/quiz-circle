<?php

    $image_path = $this->path->slide_images();

    $this->slider->initialize( );
    
    $slider_images = $this->slider->get_array();
    
    //Explain($slider_images);
    
    if(is_array($slider_images)){
        if(count($slider_images)){
    ?>
    <div class="slider-images">      
        <form action="" method="post" id="edit-img-slider-form">   
            
        <?php
        $counter = 0;
        foreach ($slider_images as $slide_image_id => $slider) {
            $slider_hash = _unique_hash();
            //echo $slider['img_name'] . "<br>";
            ?>
            <div class="slider-images-each" id="<?php echo $slide_image_id?>">
                <div class="img">
                    <img src="<?php echo $image_path . $slider['img_name']; ?>" class="slide-img">
                    <input type="hidden" value="<?php echo $slider['img_name']; ?>" id="title" name="img_name[ ]">
                    <input type="hidden" value="<?php echo $slide_image_id; ?>" id="title" name="img_id[ ]">
                </div>
                <div class="controls" id="controls-<?php echo $slide_image_id?>">
                    <a href="javascript:show_controls('<?php echo $slide_image_id?>')" class="btn">Edit</a>
                    <a href="javascript:remove_slide_image('<?php echo $slide_image_id?>')" onclick="return confirm('Are you sure...?')" class="btn">Remove</a>
                </div>
                <div class="form-values" id="form-values-<?php echo $slide_image_id?>">
                    <div class="close-form btn">X</div>

                    <div class="form-input">
                        <label for="title">Slide Title</label>
                        <input type="text" value="<?php echo $slider['title']; ?>" id="title" name="title[ ]">
                    </div>
                    <div class="form-input">
                        <label for="description">Slide Description</label> 
                        <input type="text" value="<?php echo $slider['description']; ?>" id="description" name="description[ ]">
                    </div>
                </div>
            </div>
            <?php
            $counter++;
        } ?>   
            <div class="submit-btn ">
                <input type="submit" class="btn" value="Save">
            </div>
        </form>
    </div>
    <?php
        }
    }
    ?>