
<?php
$level = (int) $level;
$class_type = $this->uri->segment(4);
//for($i =0; $i < $level; $i++) { echo "&raquo; "; }
//echo  $this->category->tree_parent_sign($level) ." <a href='#'>" . $item['name'] . "</a><br>";
$un_id = _unique_hash();
//_alert($active_data);
$active_data = !is_array($active_data) ? array() : $active_data;
?>
<tr>
    <td style="width: 35px; text-align: center"> 
        <input type="checkbox" <?php echo in_array( $item['nav_id'], $active_data ) ? "checked" : ""?> value="<?php echo  $item['nav_id']; ?>" id="<?php echo $un_id; ?>" name="navs[]">
    </td>
    <td>
        <?php echo $this->nav->tree_child_sign( $level, "&mdash;" ); ?>
        <label for="<?php echo $un_id; ?>"><?php echo $item['title']; ?></label>
    </td>
    <td>
        <?php echo $item['uri_guid']; ?>
    </td>
    <td style="text-align: center; width: 60px">
        <a href="<?php echo base_url('admin/classes/get_list/edit/' . $item['nav_id']) ?>" class="btn" style="margin: 0">Edit</a>
    </td>
    <td style="text-align: center; width: 60px">
        <a href="<?php echo base_url('admin/classes/get_list/delete/' . $item['nav_id']) ?>" onclick="return confirm('Are you sure want to delete!')" class="btn" style="margin: 0">Delete</a>
    </td>
</tr>