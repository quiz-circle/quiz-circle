<?php
$level = (int) $level;

$class_name = $this->input->post("class_name") ? $this->input->post("class_name") : "related-tags";
$class_name = $this->input->post("class_name") ? $this->input->post("tag_input_name") : "tag_input_name";
?>


<div class="tag-item-each related-tags">
    <input type="hidden" name="content-tags[]" value="<?php echo $item['class_id']; ?>" >
    <div class="tag-name"><?php echo $item['name'] ?></div>
    <div class="remove-tag">x</div>
</div>
