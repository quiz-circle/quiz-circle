<!DOCTYPE HTML>
<html>
    <head>
        <?php $specific_css = $this->uri->segment(2) == "" ? "home.css":$stylesheet ? $stylesheet.".css":$this->uri->segment(2).".css"?>
        
        <title><?php echo isset($page_title) ? $page_title:"Welcome to dashboard"?></title>
        <script>var  base_url = "<?php echo base_url() ?>";</script>
        <script>var  admin_images = "<?php echo base_url("application/views/admin/images/") ?>";</script>
        <script>var  assets_images = "<?php echo base_url("assets/image-files/") ?>";</script>
        <!-- Mata Tags-->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <script type="text/javascript" > var task_list = {}; </script>
        <script src="<?php echo $this->admin->js("jscolor.js"); ?>" type="text/javascript" ></script>
        
        <script src="<?php echo $this->admin->js("core.js"); ?>" type="text/javascript" ></script>
        
        <link href="<?php echo $this->admin->images("fevicon.png") ?>" rel="icon" type="image/png">
        
        <!--Stylesheet links-->
        <link href="<?php echo  $this->admin->css("admin.css"); ?>" rel="stylesheet" type="text/css" >
        
        <!-- Stylesheet specifically for an page -->
        <link href="<?php echo  $this->admin->css("page/" . $specific_css); ?>" rel="stylesheet" type="text/css" >

        <!--Javascript Links-->
        <script src="<?php echo $this->admin->js("jQuery.js"); ?>" type="text/javascript" ></script>
        
        <link href="<?php echo  $this->admin->css("video.js.min.css"); ?>" rel="stylesheet" type="text/css" >
        <!--Jquery Library-->
        <script src="<?php echo $this->admin->js("jquery.popupoverlay.js"); ?>" type="text/javascript" ></script>
        
        <script src="<?php echo $this->admin->js("video.js.min.js"); ?>" type="text/javascript" ></script>
        
        <!--Jquery user interface script-->
        <script src="<?php echo $this->admin->js("jquery-ui.min.js"); ?>" type="text/javascript" ></script>
        
        <script src="<?php echo $this->admin->js("header.js"); ?>" type="text/javascript" ></script>

        <script src="<?php echo $this->admin->js("relation-list.js"); ?>" type="text/javascript" ></script>

    </head>
    <body>  
        
        <?php
            
            $udata = $auth->get_data( );
            
            
            
            
            function _item_is_visible($self, &$item, &$ex =""){
                if(!preg_match("/[\|]+/", $item)) return true;
                $item_seg = explode("|", $item);
                //Explain($item_seg);
                $item = isset($item_seg[0]) ? $item_seg[0]:"";
                $ex = isset($item_seg[1]) ? $item_seg[1]:"";
                
                if(count($item_seg) > 1){
                    if(!preg_match("/[\:\:]+/", $item_seg[1])) return true;
                    $item_seg = explode("::", $item_seg[1]);
                    if(count($item_seg) > 0){
                        $role_type = isset($item_seg[0]) ? $item_seg[0]:"";
                        $role_id = isset($item_seg[1]) ? $item_seg[1]:"";
                        
                        return $self->authantication->getPermission($role_type, $role_id);
                    }
                }   
            }
        
        ?>
        
        <div id="center">
            <div id="main">
                <div id="sidebar">
                    
                    <div class="profile">
                        <div class="pic-wrap">
                            <div class="profile-pic">
                                <img src="<?php echo $user->get_profile_image(); ?>">
                            </div>
                        </div>
                        <ul class="user-action">                            
                            <?php if(!empty($udata)):?>
                            <li>
                                <a href="javascript:" class="display-name"><?php echo ucwords($user->get_display_name()); ?> <span class="user-action-upper-triangle">&#x25BE</span></a> 
                                <ul class="user-actions">
                                    <li><a href="<?php echo base_url("admin/user/logout"); ?>">Logout</a></li>
                                    <li><a href="<?php echo base_url("admin/user/profile"); ?>">Profile</a></li>
                                </ul>
                            </li>
                            <?php endif; ?>
                        </ul>

                        <script>
                            
                            
                            function loading_gear(element, element_class ){
                                $(element).html("<img src='" + admin_images +"gear.gif' class='" + element_class + "' alt='Loading...' >");
                            }
                            
                            $(".user-action .user-actions").css("display", "none");
                            var t = $(".user-action .user-actions");
                            $("body").on("click", function(e){
                                //console.log(e.target.tagName);
                                if(e.target.className == "display-name" || e.target.className == "user-action-upper-triangle") {
                                    if(t.css("display") == "none"){
                                        t.css("display", "block");
                                    }else {
                                        t.css("display", "none");
                                    }
                                }else {
                                    t.css("display", "none");
                                }
                            });
                        </script>
                    </div>
                    <div class="sidebar-top">
                        <div class="logo">
                            <h1><?php echo isset($site_name) ? $site_name : "Buildings And Flats"?></h1>
                        </div>
                        <?php $udata = $auth->get_data(); ?>
                        <h2 style="width: 100%; text-align: center; margin: 8px 0; float: left">
                            <a href="<?php echo base_url()?>" style="color: #FFF" target="_blank">View Site</a>
                        </h2>
                    </div>
                    <div class="menu">
                        <ul class="left">
                            <?php $i =0; foreach ($menu as $name => $link): $i++?>
                                <?php 
                                    
                                    $is_visible = false;
                                    $act = " class='active' ";
                                    $active = "";
                                    if($this->uri->segment(1) == "admin"){
                                        
                                        
                                        if ( $this->uri->segment(2))  {
                                            
                                            if(is_string($link)) {
                                                
                                                $string = explode("/", $link);
                                                if( count($string) >= 2 ){
                                                    //$active_page = array_slice($string, 0, 4);
                                                    
                                                    //_alert($active_page);
                                                    //$active = ($active_page == $this->uri->segment(2)) ? $act:'';
                                                }
                                            }
                                        }else if($this->uri->segment(2) == "" && $i == 1){
                                            $active = $act;
                                        }
                                        
                                        
                                    }
                                ?>
                                <?php if(is_array($link)):?>
                                    <?php $init_link = isset($link[0]) ? $link[0]:"#"; ?>

                                        <li <?php echo $active?>>
                                            <a href="<?php echo base_url($init_link)?>"><?php echo $name; ?></a>
                                            <ul>
                                                <?php foreach ($link as $sub_name => $sub_link):?>
                                                    <?php if(!is_numeric($sub_name)):?>

                                                        <?php if(_item_is_visible($this, $sub_link)):?>
                                                            <li><a href="<?php echo base_url($sub_link)?>"><?php echo $sub_name; ?></a></li>    
                                                        <?php endif;?>
                                                    <?php endif;?>
                                                <?php endforeach;?>
                                            </ul>
                                        </li>
                                <?php else: ?>
                                    <li <?php echo $active?>><a href="<?php echo base_url($link)?>"><?php echo $name; ?></a></li>
                                <?php endif;?>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </div>
                <div id="header">
                    <h3 class="page-title"><?php echo isset($page_heading) ? $page_heading:"" ?></h3>
                    <?php //echo "page: ". $this->uri->segment(2);?>
                </div>
                <div id="body">
                    <div id="section-message">

                    </div>
                    
                    <?php
                        
                        
                    
                        
                        if( isset( $_success_message ) ) {
                            section_success_message( $_success_message );
                        }
                        
                        if( isset( $_warning_message ) ) {
                            section_warning_message( $_warning_message );
                        }
                        
                        if( isset( $_info_message ) ) {
                            section_info_message( $_info_message );
                        }
                        if( isset( $_danger_message ) ) {
                            section_danger_message( $_danger_message );
                        }
                        
                        if( $this->session->has_userdata( "_success_message" ) ) {
                            section_success_message($this->session->flashdata( "_success_message" ) );
                        }                        
                        if($this->session->has_userdata( "_warning_message" )){
                            section_warning_message($this->session->flashdata( "_warning_message" ) );
                        } 
                                                
                        if($this->session->has_userdata( "_info_message" )){
                            section_info_message($this->session->flashdata( "_info_message" ) );
                        } 
                                                
                        if($this->session->has_userdata( "_danger_message" )){
                            section_danger_message($this->session->flashdata( "_danger_message" ) );
                        } 
                        
                        
                    ?>