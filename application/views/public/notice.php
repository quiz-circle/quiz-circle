
<?php $this->_public->header( data() ) ?>

<style>
   #notice.col-md-2{
       width: 100px;
       height: 60px;
       background-color: skyblue;
       color: black;
       box-shadow: 6px 4px 5px silver;
       text-align: center;
       margin-top: 33px;
   }
   #notice {
       background-color: lightsalmon;
       color: black;
       box-shadow: 6px 4px 5px silver;
       text-align: center;
       margin-top: 25px;
       margin-left: 30px;
       padding: 15px;
   }
   
   #notice p{
       color: #000;
   }
   
   #notice.col-md-4{
       float: left;
       margin-top: 30px;
   }


</style>



<div id="wrapper">
    <div class="container" style="background-color: #EFEFE6" >
        <div class="row" style="text-align: center">
            
            <div class="col-lg-8 col-md-8 col-sm-8" style="margin-bottom: 20px" >
                <?php if(isset($notices) && count($notices)): ?>
                <?php foreach($notices as $notice): ?>
                
                <div id="notice" class="col-lg-12 col-md-12 col-sm-12 ">
                    <div>
                        <h3>
                            <?php echo $notice->title(); ?>
                        </h3>
                        <p>
                            <?php echo $notice->description; ?>
                        </p>
<!--                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. when are-->
                    </div>
                </div>
                
                <?php endforeach; ?>
                <?php endif; ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4"  style="margin-bottom: 20px">
                <h4>Department Name</h4>
                <ul>
                    
                    <?php 
//                    Explain($notice_category);
                    if(isset($notice_category) && count($notice_category)): ?>
                        
                        <li><a href="<?php echo base_url("quiz/notice" ); ?>"><u><strong>All Category </strong></u></a> <br></li>
                        <?php foreach($notice_category as $cat ):?>
                            <li><a href="<?php echo base_url("quiz/notice?cat_id=" . $cat['class_id'] ); ?>"><?php echo $cat['name']; ?></a></li>
                        <?php endforeach;?>
                    <?php endif;?>
                        
<!--                    <li><a href=""><u><strong>English</strong></u></a> <br></li>
                    <li><a href=""><u><strong>CSE </strong></u></a> <br></li>
                    <li><a href=""><u><strong>EEE</strong></u></a> <br></li>
                    <li><a href=""><u><strong>BBA</strong></u></a> <br></li>
                    <li><a href=""><u><strong>BPM</strong></u></a>  <br></li>
                    <li><a href=""><u><strong>LLB</strong></u></a> <br></li>
                    <li><a href=""><u><strong>MBA</strong></u></a> <br></li>
                    <li><a href=""><u><strong>MA</strong></u></a></li>-->
                </ul>
            </div>
        </div>
    </div>
</div>









<?php $this->_public->footer( data() ) ?>
