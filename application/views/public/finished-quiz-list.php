<?php $this->_public->header( data() ) ?>
<?php
/**
 * Created by PhpStorm.
 * User: Saifuddin
 * Date: 12/14/2017
 * Time: 9:17 PM
 */

?>


<div class="container">
    <div class="row">
        <div class="col-lg-12">

            <h2>Your participation in quiz</h2>

            <div class="list-header" >

            </div>
            <div class="list-body" >
                <table class="table table-responsive" id="quiz-result">
                    <thead>
                        <tr>
                            <td>#</td>
                            <td>Quiz Title</td>
                            <td>Participation Date</td>
                            <td>Full Mark</td>
                            <td>Mark Obtained</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php public_view("includes/list/finished-quiz-result.php"); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal for view details -->
<div class="modal fade" id="quizResult" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Details</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>

    $("#quiz-result .action button.btn").on("click", function(e){

        var result_id = $(this).attr( "data-participation-id" );

        //alert(result_id);

        _request_ajax({
            url : "view/student_quiz_result",
            success : function( result ){
                $("#quizResult .modal-body").html( result );
            },
            data: { participation_id : result_id }
        });
    });


//    $( '#before-you-go' ).on('shown.bs.modal', function (e) {
//        console.log(e);
//        _request_ajax({
//        url : "view/before_you_go_page",
//        success : function(result){
//        console.log( result );
//        $("#before-you-go").find(".modal-body").html( result );
//        },
//        data: { quiz_id: $("#_quiz_id_").val(), "a":45 }
//        });
//    });

</script>

<?php $this->_public->footer( data() ) ?>
