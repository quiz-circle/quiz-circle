<?php $this->_public->header( data() ) ?>

<style>
    #quiz-perform {
        margin-top: 40px;
    }

    .result-view ,
    .starting-delay-time {
        width: 100%;
        height:100%;
        position: fixed;
        background-color: #FFFFFFBB;
        left: 0;
        top: 0;
        z-index: 50;
        display: block;
        text-align: center;
        vertical-align: middle;
    }

    .result-view .deay-time-in,
    .starting-delay-time .deay-time-in{
        font-size: 12px;
        display: inline-block;
        margin-top: 20%;
    }

    .result-view .deay-time-in .round,
    .starting-delay-time .deay-time-in .round {
        width: 200px;
        height: 200px;
        border-radius: 50%;
        display: inline-block;
        float: none;
        border: 1px solid #0b0a0a;
        background-color: #55830c55;
        line-height: 200px;
    }

    .result-view .deay-time-in .round p,
    .starting-delay-time .deay-time-in .round p {
        color:#0b0a0a;
        font-size: 100px;
    }

    #button-container {
        margin-bottom: 20px
    }

    .quiz-title {
        display: inline-block;
    }

    .quiz-timer {
        color:#00b300;
        font-size: 30px;
        float: right;
        display: inline-block;
    }

    .result-view {
        display: none;
    }

</style>

<div class="starting-delay-time">
    <div class="deay-time-in">
        <h4>Your quiz is starting in</h4>
        <div class="round">
            <p class="time"></p>
        </div>
    </div>
</div>

<div class="result-view">
    <div class="deay-time-in">
        <h4></h4>
    </div>
</div>

<div class="container" id="quiz-perform">
    <form method="post" action="" id="quiz-performance" >
        <div class="row">
                <div class="quiz-perform-header col-lg-12">

                    <h2 class="quiz-title"><?php echo $quiz_data->title(); ?></h2>

                    <div class="quiz-timer"></div>

                </div>

                <?php
        //          Explain($questions);
                    public_view("includes/list/question-relations.php");
                ?>
        </div>
        <div class="row" id="button-container">
            <div class="col-lg-12">
                <input type="submit" id="final-submit" disabled value="Finish" class="btn btn-primary">
                <a href="javascript:" id="next-button" class="btn btn-primary">Next</a>
            </div>
        </div>
    </form>


</div>



<script>
    var token = '<?php echo _get("token") ?>';
    var second = 0;
    var delay_time = 3000;
    var timer = 0;

    function start_delay_timer(){
        if( delay_time > -1000 ){
            $(".starting-delay-time").find(".time").html( ( delay_time/1000 ) + "s" );
            timer = setTimeout(start_delay_timer, 1000);
        }

        if( delay_time == 0){
            start_quizing(  );
            clearTimeout( timer );
            $(".starting-delay-time").remove();
            delay_time = -1000;
        }

        delay_time = delay_time - 1000;
    }


    $current_index = 0;
    $total_questions = $(".quiz-element").length;
    function single_question_view(){

        //alert($total_questions);
        $("#final-submit").css("display", "none");
        $("#next-button").css("display", "inline-block");

        $("#next-button").on("click", function(e){
            e.preventDefault();
            $current_index++;
            show_question($current_index);

            if(  $current_index == ( $total_questions - 1 ) ){

                $("#final-submit").css("display", "inline-block");
                $("#next-button").css("display", "none");
            }

        });

        $(".quiz-element").eq($current_index);
    }

    function show_question(index){
        $(".quiz-element").css("display", "none");
        $(".quiz-element").eq(index).css("display", "block");
    }

    single_question_view();

    show_question(0);


    function start_quizing(){

        get_quiz_data_enable_submitting();
    }

    var total_seconds = 0;

    function get_quiz_data_enable_submitting(){
        _request_json({
            url : 'data/quiz_from_token',
            success : function ( result ) {

                if(result.is_logged_in && result.success) {
                    $( "#button-container #final-submit" ).prop( "disabled", false );
                    var total_quest = result.total_question;
                    total_seconds = 60  * total_quest;
                    var _be5ac5efd5b7a8c8b7ea897_ = total_seconds,_8abcd8e8d5b0ae9c4a4bc47_ = 1*5*(951/(80+23+848))*100*((1+9*40+70*6)/(261+520))*1*2*1;
                    setTimeout( function(){ auto_submit_participation( result.quiz_data.id, result.student_id ); }, _be5ac5efd5b7a8c8b7ea897_*_8abcd8e8d5b0ae9c4a4bc47_ );

                    function start_countdown(){
                        if(total_seconds >= 0){
                            var Hour = Math.floor(total_seconds / 3600  );
                            var Minute  = Math.floor( total_seconds /  60  );
                            var Second  = total_seconds % 60;
                            Hour = Hour < 10 ? "0" + Hour : Hour;
                            Minute = Minute < 10 ? "0" + Minute : Minute;
                            Second = Second < 10 ? "0" + Second : Second;
                            $(".quiz-timer").html(Hour + ":" + Minute + ":" + Second );
                            total_seconds--;
                            setTimeout(start_countdown, _8abcd8e8d5b0ae9c4a4bc47_);
                        }
                    }

                    enable_submitting( result.quiz_data.id, result.student_id );
                    start_countdown( );
                }
            },
            data : {token: token }
        });
    }


    var auto_submission = true;

    function auto_submit_participation( quiz_id, student_id ) {
        if(auto_submission) {
            submit_form({
                submission_type : "auto",
                quiz_id : quiz_id,
                student_id : student_id,
                token : token
            })
        }
    }

    function enable_submitting( quiz_id, student_id ){
        $("#quiz-performance").off("submit");
        $("#quiz-performance").on("submit", function (e) {
            e.preventDefault();
            total_seconds = 0;
            submit_form({
                submission_type : "manual",
                quiz_id : quiz_id,
                student_id : student_id,
                token : token
            })
        });
    }

    function submit_form( data ) {


        var config = {
            url : "save/quiz_performance",
            success : function ( result ) {

                $("#quiz-perform").html("" );
                var new_row = $("<div/>");
                new_row.addClass( "row" );
                $("#quiz-perform").append( new_row );

                $("#quiz-perform .row").css( "height", "500px" );
                $("#quiz-perform .row").html( "" );

                //console.log(result);
                if( result.is_logged_in && result.success ) {
                    auto_submission = false;
                    $(".result-view h4").html( "Checking your performance...!"  );
                    _request_ajax({
                        url : "view/finished_quiz_result",
                        success : function( result ){

                            //console.log(result);
                            $("#quiz-perform .row").html( result );
                            $(".result-view").css("display", "none");
                        },
                        data : {token : result.token }
                    });

                }
            },
            formRef : $("#quiz-performance")
        }
        if( $.type( data ) == "object" ) config.data = data;

        $(".result-view h4").html( "Submitting your performance...!"  );
        $(".result-view").css( "display", "block" );

        $("html, body").scrollTop(0);
        _request_json(config);
    }



    start_delay_timer();


</script>
<?php $this->_public->footer( data() ) ?>