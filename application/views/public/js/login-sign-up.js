$(window).on("load", function () {
    $(".form .field-wrap input, .form .field-wrap textarea").each(function () {
        var label = $(this).prev('label');
        if($(this).val() === ""){
            label.removeClass('active highlight');
        }else {
            label.addClass('active highlight');
        }
    });
});


$('.form').find('input, textarea').on('keyup blur focus drop', function (e) {
    var $this = $(this), label = $this.prev('label');

    if (e.type === 'keyup' || e.type === 'drop') {
        if ($this.val() === '') {
            label.removeClass('active highlight');
            if(e.type === "drop") {
                label.addClass('active highlight');
            }
        } else  {
            label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
        if ($this.val() === '') {
            label.removeClass('active highlight');
        } else {
            label.removeClass('highlight');
        }
    } else if (e.type === 'focus' ) {
        
    }
    
});

$('.tab a').on('click', function (e) {

    e.preventDefault();

    $(this).parent().addClass('active');
    $(this).parent().siblings().removeClass('active');

    target = $(this).attr('href');

    $('.tab-content > div').not(target).hide();

    $(target).fadeIn(600);

});