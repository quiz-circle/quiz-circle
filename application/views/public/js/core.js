function title(T){
    document.title = T;
}

function monitor(html){
    var M = $("<div/>");
    $(".-global-monitor-").remove();
    M.addClass("-global-monitor-");
    M.css({
        position :"fixed",
        width: "100%",
        "background-color" : "rgba(200, 200, 200, 0.3)",
        "text-align" : "center",
        "padding" :"15px",
        "top" :"0",
        "left" :"0",
        "z-index" :"50000000",
    });
    M.html(html)
    M.appendTo("body");
}

function _events(selector, target, event){
    $(selector).find(target).each(function(){
        $(this).off(event);
        $(this).on(event, function(e){
            e.preventDefault();
            var config = {};
            var id =    $(this).attr("id");
            var id_seg = id.split("-");
            if(id_seg.length >= 2) {
                var id =  id_seg[1] ;
                var name = $("#" + id).find("#_core_section_name").val();
                if(typeof task_list[id_seg[0]] != "undefined" ) {
                    var task = task_list[id_seg[0]];
                    var func = "";
                    if(typeof task[name] == "undefined") {
                        //default functionality for this event
                        func = task.func;
                        config = ( typeof task.config != "undefined") ? task.config : {};
                    }else {
                        //customize functionality for this event
                        if(typeof task[name] != "undefined") {
                            func = task[name].func;
                            config = ( typeof task[name].config != "undefined") ? task[name].config : {}
                        }
                    }

                    if(typeof func == "function") {
                        config.id = id;
                        func(id, config, e);
                    }

                }
            }
        });
    });
}

var s =0;

function location_hash(split_by){
    split_by = typeof split_by == "undefined" ? "=" : split_by
    var loc_hash = window.location.hash;
    loc_hash = loc_hash.replace("#", "");
    var _hash = loc_hash.split("&");

    var result = {};
    if(_hash.length == 1){
        var _h = _hash[0].split("=");
        if(_h.length >= 2){
            result[_h[0]] = _h[1];
        }
    }else {
        for(i = 0; i < _hash.length; i++) {
            var _h = _hash[i].split("=");
            if(_h.length >= 2){
                result[_h[0]] = _h[1];
            }
        }

    }
    return result;
}


function resize_cordinates(nw) {
    var im = $(".links-img");

    nw = $.type(nw) == "undefined" ? im.width():nw;

    console.log(nw);

    im.each(function (i) {

        var pw = parseInt($(this).find("#width").val());
        var ph = parseInt($(this).find("#height").val());

        $(this).width(nw);


        $(this).find("#image_canvas_control").css("width", "100%");
        $(this).find("#visible-image").css("width", "100%");
        $(this).find("#hidden-image").css("width", "100%");

        var w = $(this).find("#visible-image").width();
        var h = $(this).find("#visible-image").height();

        var c = $(this).find("#image_canvas_control_map #original_coords");
        //console.log(c.length);
        var f = 1;
        if (w > h) {
            f = pw / ph;
        } else {
            f = ph / pw;
        }

        var nh = Math.floor(nw / f);
        var ofh = ph - nh;
        var ofw = pw - nw;

        var n_cord = [];

        c.each(function (j) {
            var Type = $(this)[0].type;
            var nc = "";
            var crs = $(this).val();
            var cordinates = crs.split(",");
            for (m = 0; m < cordinates.length; m++) {

                nc += (m != 0) ? ", " : "";

                var c = parseInt( cordinates[m] );
                console.log(pw);

                if (i % 2 == 0) {
                    var r = (c / pw) * 100;
                    var d = (ofw * r) / 100;
                    nc += Math.abs(Math.floor(c - d));
                } else {
                    var r = (c / ph) * 100;
                    var d = (ofh * r) / 100;
                    nc += Math.abs(Math.floor(c - d));
                }
            }
            n_cord.push(nc);
        });

        //console.log(n_cord);

        var cc = $(this).find("#image_canvas_control_map area");
        //var ccValue = $(this).find("#image_canvas_control_map #changed_coords");

        cc.each(function (n) {
            $(this).attr("coords", n_cord[n]);
        });
    });
}


function changed_cordinates(){

}


function adjust_section(){
    var rW = $(".buildings").width();
    resize_cordinates( rW );
}

function change_image(id, after_print_flat_list){
    $(".image-listing ul li a").removeClass("active");
    //$(this).addClass("active");
    $("#"+id).addClass("active");


    $("#building .ims").css({
        "display": "none",
        "width": "100%",
    });
    //alert(e.target.id);

    $("#building ."+id).css({
        "display": "block",
        "width": "100%",
    });
    hover_section();
    adjust("#building ."+id+" #images");

    var image_id = $("." + id).find("#image_id").val();
    var building_id = $("." + id).find("#building_id").val();
    //alert(image_id);
    print_flat_list(sections, image_id, building_id, ref, after_print_flat_list);

}


function readURL(input, image) {

    if (input.files && input.files[0]) {

        var reader = new FileReader();

        if(typeof image == "function"){
            reader.onload = image
        }else {
            reader.onload = function (e) {
                $(image).html('<img src="' + e.target.result + '">');
            }
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function ajax_error_console(r){
    console.log(r.responseText);
}

function append_values(values) {
    var data = new FormData();
    for (var key in values) {
        if (values.hasOwnProperty(key)) {
            data.append(key, values[key]);
        }
    }
    return data;
}

function append_extra_values(data, values) {

    for (var key in values) {
        if (values.hasOwnProperty(key)) {
            data.append(key, values[key]);
        }
    }
    return data;
}

function _request_ajax(config) {

    var url = typeof config.url != "undefined" ? config.url : "";
    var success = typeof config.success != "undefined" ? config.success : "";
    var fr = typeof config.formRef != "undefined" ? config.formRef : "";
    var ed = typeof config.data != "undefined" ? config.data : {};
    _ajax(fr, "script/site/" + url, "ajax", success, ed );
}

function _request_json(config) {
    var url = typeof config.url != "undefined" ? config.url : "";
    var success = typeof config.success != "undefined" ? config.success : "";
    var fr = typeof config.formRef != "undefined" ? config.formRef : "";
    var ed = typeof config.data != "undefined" ? config.data : {};
    _ajax(fr, "script/site_/" + url, "json", success, ed);
}

function _ajax(form, url, dataType, callback, extraData) {
    var FD = {};
    var form = $(form);

    if (form.length != 0) {
        var FD = new FormData(form[0]);
        FD = append_extra_values(FD, extraData);
    } else {
        FD = append_values(extraData);
    }
    var config = {
        url: base_url + url,
        data: FD, contentType: false,
        processData: false, type: "POST",
        error: ajax_error_console,
        success: callback
    }
    if (dataType == "json") {
        config.dataType = dataType;
    }
//
//    console.log(extraData);
//    console.log(config);
    $.ajax(config);
}

function hexToRgb(hex) {
    //alert(hex);
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function rgb2hsv() {
    var rr, gg, bb,
        r = arguments[0] / 255,
        g = arguments[1] / 255,
        b = arguments[2] / 255,
        h, s,
        v = Math.max(r, g, b),
        diff = v - Math.min(r, g, b),
        diffc = function (c) {
            return (v - c) / 6 / diff + 1 / 2;
        };

    if (diff == 0) {
        h = s = 0;
    } else {
        s = diff / v;
        rr = diffc(r);
        gg = diffc(g);
        bb = diffc(b);

        if (r === v) {
            h = bb - gg;
        } else if (g === v) {
            h = (1 / 3) + rr - bb;
        } else if (b === v) {
            h = (2 / 3) + gg - rr;
        }
        if (h < 0) {
            h += 1;
        } else if (h > 1) {
            h -= 1;
        }
    }
    return {
        h: Math.round(h * 360),
        s: Math.round(s * 100),
        v: Math.round(v * 100)
    };
}


function rgbToHsl(r, g, b) {
    r /= 255, g /= 255, b /= 255;

    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, l = (max + min) / 2;

    if (max == min) {
        h = s = 0; // achromatic
    } else {
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

        switch (max) {
            case r:
                h = (g - b) / d + (g < b ? 6 : 0);
                break;
            case g:
                h = (b - r) / d + 2;
                break;
            case b:
                h = (r - g) / d + 4;
                break;
        }

        h /= 6;
    }

    return {h: Math.floor(h * 360), s: Math.floor(s * 100), l: Math.floor(l * 100)};
}


function isLight(hexCode){
    var rgb = hexToRgb(hexCode);

    //return true;
    return  (0.213 * rgb.r + 0.715 * rgb.g + 0.072 * rgb.b)  > (255 / 2);

}

function inverseColor(element, target){
    $(element).each(function(i){
        var jsColor = $(this).val();

        var color = "";
        console.log(isLight(jsColor)+ " " + i);

        if(isLight(jsColor)){
            color = "#000";
        }else {
            color = "#FFF";
        }
        $(target).eq(i).find("._back_color").css("color", color);

    });
}

function inverseColorPublic(element, target){
    $(element).each(function(i){
        var jsColor = $(this).val();
        //alert(jsColor);
        var color = "";
        console.log(isLight(jsColor)+ " " + i);

        if(isLight(jsColor)){
            color = "#222";
        }else {
            color = "#E2E2E2";
        }
        $(target).eq(i).css("color", color);

    });
}



function focus_flat_location(image, coords, canvasId, config) {
    reset_canvas( canvasId );
    var w = $(".links-img").find("#visible-image").width();
    var h = $(".links-img").find("#visible-image").height();

    var Chtml = "<canvas id='" + canvasId + "' height='" + (h)  + "' width='" + (w) + "'  style=' position: absolute; left: 0; top: 0; z-index: 1' />"

    if (typeof image != "undefined") {
        image.append(Chtml);

        draw_poligon(coords, "#" + canvasId, config);
    }
}

function get_object_property(object, property, default_value){
    if(typeof object == "object"){
        return typeof object[property] == "undefined" ? default_value:object[property];
    }
}


function draw_poligon( coords, location, config ) {
    var poly = coords.split(",");
    var c = $(location);
    config = typeof config ==  "object" ? config : {};

    if(c.length != 0){
        var ctx = c[0].getContext("2d");
        ctx.fillStyle = get_object_property(config, "background-color", "#1761ac");
        ctx.globalAlpha = get_object_property(config, "opacity", 0.3);


        ctx.beginPath();
        ctx.moveTo(poly[0], poly[1]);
        for (i = 2; i < poly.length - 1; i = i + 2) {
            ctx.lineTo(poly[i], poly[i + 1]);
        }
        if(typeof config["border-color"] != "undefined"){
            ctx.strokeStyle = config["border-color"];
            //alert(ctx.strokeStyle);
            ctx.stroke();
        }
        ctx.fill();
    }
}




function reset_all_location( ) {
    $(".links-img canvas").remove( );
}

function reset_hover( ) {
    $(".links-img #canvasHover").remove( );
}

function reset_all_canvas( ) {
    $("canvas").remove( );
}

function reset_canvas( canvasID ) {
    $("canvas#" + canvasID ).remove( );
}





function hover_section(){
    $.fn.maphilight.defaults = {
        fill: true,
        //fillColor: '0a60ab',
        fillColor: '1761ac',
        fillOpacity: "0.0",
        stroke: true,
        strokeColor: '660000',
        strokeOpacity: 0,
        strokeWidth: 0,
        fade: true,
        alwaysOn: false,
        neverOn: false,
        groupBy: false,
        wrapClass: true,
        shadow: false,
        shadowX: 5,
        shadowY: 5,
        shadowRadius: 6,
        shadowColor: '00FF00',
        shadowOpacity: 0.8,
        shadowPosition: 'outside',
        shadowFrom: false
    }
    $('.map').maphilight();
}

"use strict";

jQuery.prototype.display = function(visible){
    visible = $.type(visible) != "undefined" ? visible:"block";
    $(this).css("display", visible);
    return $(this);
}


if (!window.slider) { window.slider = (
    function () {

        var slider = {

            wrapper: "",
            image_container: "",
            image_each: "",
            controls: "",
            type: "",
            changeDelay: 4000,
            total_slides: 0,
            current_pos: 0,
            timeOutId: 0,
            change: 1000,
            direction: "left",
            backAction: false,
            heightRatio: 0.3,

            self: this,

            start_slide_show: function (selector, config) {
                slider.image_container = config.image_container;
                slider.wrapper = selector;
                slider.image_each = config.image_each;
                slider.controls = config.controls;
                slider.type = config.type;
                slider.delay = config.delay;
                slider.changeDelay = config.changeDelay;
                slider.heightRatio = config.heightRatio;

                slider.total_slides = slider.image_each.length;
                slider.image_each.css("display", "none");
                slider.image_each.eq(0).css("display", "block");
                slider.wrapper.css({
                    overflow: "hidden",
                });
                slider.enableSettings();
                slider.start_auto();
                slider.disableEvents();
                slider.enableEvents();

                slider.adjustHeightRatio();


            },
            adjustHeightRatio: function (W) {
                $(document).ready(function () {
                    var Width = slider.wrapper.width();
                    //monitor(Width);
                    slider.wrapper.height( Width * slider.heightRatio);
                    $(window).on("load resize", function () {
                        var Width = slider.wrapper.width();
                        //monitor(Width);
                        slider.wrapper.height( Width * slider.heightRatio);
                    });
                })

            },
            enableEvents: function () {
                this.controls.find(".next").on("click", function () {
                    slider.direction = "right";
                    slider.next_slide(function (self) {
                            slider.start_auto();
                        }, slider.beforeChange
                    );
                });

                this.controls.find(".previous").on("click", function () {
                    slider.direction = "left";
                    slider.previous_slide(function (self) {
                        slider.start_auto();
                    });
                });
            },
            disableEvents: function () {
                this.controls.find(".next").off("click");
                this.controls.find(".previous").off("click");
            },
            enableSettings: function () {
                switch (slider.type) {
                    case "move":
                        slider.image_each.css("transition", (slider.changeDelay / 1000) + "s");

                        slider.image_each.eq(0).css({
                            left: "0",
                            display: "inline",
                        });
                        slider.image_each.eq(1).css({
                            left: "100%",
                            display: "inline",
                        });
                        slider.image_each.eq(this.total_slides - 1).css({
                            left: "-100%",
                            display: "inline",
                        });
                        break;
                    case "fade":
                        break;
                }
            },
            afterChange: function () {
                switch (slider.type) {
                    case "move":
                        break;
                    case "fade":
                        break;
                }
            },
            beforeChange: function () {
                switch (slider.type) {
                    case "move":
                        break;
                    case "fade":
                        break;
                }
            },

            stop_slide: function () {
                clearTimeout(slider.timeOutId);
            },

            restart_slide: function () {
                slider.start_auto();
            },

            start_auto: function () {
                if (slider.total_slides > 1) {
                    clearTimeout(slider.timeOutId);
                    slider.timeOutId = setTimeout(function () {
                        slider.direction = "right";
                        slider.next_slide(
                            slider.afterChange,
                            slider.beforeChange
                        );
                        slider.start_auto();
                    }, slider.delay + slider.changeDelay);
                }
            },

            next_slide: function (after_next, before_next) {
                if (slider.total_slides > 1) {
                    slider.backAction = false;
                    slider.current_pos++;
                    if (slider.current_pos > slider.total_slides - 1) {
                        slider.current_pos = 0;
                    }
                    if (typeof before_next == "function") {
                        before_next(slider);
                    }
                    slider.change_slide(slider.current_pos);
                    if (typeof after_next == "function") {
                        after_next(slider);
                    }
                }
            },

            change_slide: function (pos) {

                switch (slider.type) {
                    case "fade" :
                        slider.change_fade(pos);
                        break;
                    case "move" :
                        slider.disableEvents();
                        setTimeout(function () {
                            slider.enableEvents()
                        }, slider.changeDelay);
                        slider.moove(pos);
                        break;
                }

            },

            change_fade: function (pos) {
                var fadeOutPos = 0;
                if (slider.backAction) {
                    fadeOutPos = (pos == slider.total_slides - 1) ? 0 : pos + 1;
                } else {
                    fadeOutPos = (pos == 0) ? slider.total_slides - 1 : pos - 1;
                }


                slider.image_each.eq(fadeOutPos).fadeOut(slider.changeDelay);
                slider.image_each.eq(pos).fadeIn(slider.changeDelay - (slider.changeDelay * 0.3));

                setTimeout(function () {

                }, slider.changeDelay);
            },

            moove: function (pos) {
                var fadeOutPos = 0;
                var nextSlideNumber = 0;
                var beforeSlideNumber = 0;

                nextSlideNumber = (pos == slider.total_slides - 1) ? 0 : pos + 1;
                beforeSlideNumber = (pos == 0) ? slider.total_slides - 1 : pos - 1;


                slider.image_each.css("display", "none");

                switch (slider.direction) {
                    case "right" :
                        slider.image_each.eq(beforeSlideNumber).css({
                            display: "inline",
                            left: "-100%",
                        });
                        slider.image_each.eq(pos).css({
                            display: "inline",
                            left: "0",
                        });
                        setTimeout(function () {
                            slider.image_each.eq(beforeSlideNumber).css({
                                left: "-100%",
                                display: "inline",
                            });
                            slider.image_each.eq(nextSlideNumber).css({
                                display: "inline",
                                left: "100%",
                            });
                        }, slider.changeDelay);
                        break;

                    case "left" :
                        slider.image_each.eq(beforeSlideNumber).css({
                            display: "inline",
                            left: "-100%",
                        });
                        slider.image_each.eq(pos).css({
                            display: "inline",
                            left: "0",
                        });
                        slider.image_each.eq(nextSlideNumber).css({
                            display: "inline",
                            left: "100%",
                        });
                        setTimeout(function () {
                            slider.image_each.eq(beforeSlideNumber).css({
                                display: "inline",
                            });
                            slider.image_each.eq(nextSlideNumber).css({
                                display: "inline",
                                left: "100%",
                            });
                        }, slider.changeDelay);
                        break;
                }
            },

            previous_slide: function (after_prev, before_prev) {
                if (slider.total_slides > 1) {
                    slider.backAction = true;
                    slider.current_pos--;
                    if (slider.current_pos < 0) {
                        slider.current_pos = slider.total_slides - 1;
                    }
                    if (typeof before_prev == "function") {
                        before_prev(slider);
                    }
                    slider.change_slide(slider.current_pos);
                    if (typeof after_prev == "function") {
                        after_prev(slider);
                    }
                }
            }
        }

        return slider.start_slide_show;
    })();
}