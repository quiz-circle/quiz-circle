
<?php
/**
 * Created by PhpStorm.
 * User: Saifuddin
 * Date: 12/12/2017
 * Time: 6:36 PM
 */

function validate_registration_information( &$messages = array() ){
    get_instance()->load->model('users');
    $USER = get_instance()->users;
    $success = true;



    $username = _post('username');
    $firstname = _post('firstname');
    $lastname = _post('lastname');
    $email = _post('email');
    $password = _post('password');
    $password_again = _post('password_again');
    $student_id = _post('student_id');
    $gender = _post('gender');

    //validation for first name
    $key = "firstname";
    $error_msg = "You need to provide your first name!";
    if(empty($firstname)){
        set_form_error($key, $messages, $error_msg);
        $success = false;
    }

    //validation for first name
    $key = "gender";
    $error_msg = "Select your gender!";
    if(empty($gender)){
        set_form_error($key, $messages, $error_msg);
        $success = false;
    }

    //validation for last name
    $key = "lastname";
    $error_msg = "You need to provide your last name!";
    if(empty($lastname)){
        set_form_error($key, $messages, $error_msg);
        $success = false;
    }

    //validation for email
    $key = "email";
    $error_msg = "Provide your student id";
    if(empty($email)){
        set_form_error($key, $messages, $error_msg);
        $success = false;
    }else {
        $error_msg = "This email is invalid!";
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            set_form_error($key, $messages, $error_msg);
            $success = false;
        } else {
            $error_msg = "This email is exists!";
            if ($USER->has($key, $email)) {
                set_form_error($key, $messages, $error_msg);
                $success = false;
            }
        }
    }

    //validation for username
    $key = "username";
    $error_msg = "Username cant't be blank";
    if(empty($username)){
        set_form_error($key, $messages, $error_msg);
        $success = false;
    }else {
        $error_msg = "Username length must be in greater than two charecters!";
        if( strlen( $username ) <= 1 ) {
            set_form_error($key, $messages, $error_msg);
            $success = false;
        }else {
            $error_msg = "Username length must be in less than hundred charecters!";
            if(strlen( $username ) > 100){
                set_form_error($key, $messages, $error_msg);
                $success = false;
            }else {
                $error_msg = "This username is exists!";
                if($USER->has( $key, $username)){
                    set_form_error($key, $messages, $error_msg);
                    $success = false;
                }
            }
        }
    }

    // validation for password
    $key = "password";
    $error_msg = "You have to provide a password!";
    if(empty($password)){
        set_form_error($key, $messages, $error_msg);
        $success = false;
    }else {
        $error_msg = "Your password length must be in 8 charecter!";
        if( strlen($password ) <= 7 ) {
            set_form_error($key, $messages, $error_msg);
            $success = false;
        }else {
            $key = "password_again";
            $error_msg = "Password doesn't match!";
            if( $password !== $password_again ) {
                set_form_error($key, $messages, $error_msg);
                $success = false;
            }
        }
    }

    //Validation for student id
    $key = "student_id";
    $error_msg = "Provide your student id";
    if(empty($student_id)){
        set_form_error($key, $messages, $error_msg);
        $success = false;
    }else {
        $error_msg = "Invalid student id!";
        if( strlen($student_id) !== 12 ){
            set_form_error($key, $messages, $error_msg);
            $success = false;
        }else {
            $error_msg = "This student id is exists!";
            if($USER->studentHas( $key, $student_id)){
                set_form_error($key, $messages, $error_msg);
                $success = false;
            }
        }
    }

    return $success;

//    if( !$success ) {
//        Explain($messages);
//    }else {
//        echo "Your form is ready to submit...!";
//    }

}

function set_form_error( $key, &$messages, $error = "" ){
    $messages[$key] = $error;
}