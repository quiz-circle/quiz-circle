<?php
/**
 * Created by PhpStorm.
 * User: Saifuddin
 * Date: 12/8/2017
 * Time: 9:57 PM
 */

function show_form_error($name, $class_name = "form-error"){
    $error = form_error( $name );
    echo !empty($error) ? '<div class="' . $class_name . '">' . $error . '</div>' : "";
}

function show_err_message( $messages, $key,  $class_name = "form-error"){

    $error = isset( $messages[$key] ) ? $messages[$key] : "";
    echo !empty($error) ? '<div class="' . $class_name . '">' . $error . '</div>' : "";
}

function login_form( $config = array() ) {
    get_instance()->_public->includes( "login-form", $config);
}

function social_login_links( $config = array() ) {
    get_instance()->_public->includes( "social-login-links", $config);
}

function login_reg_styles(){
    require_once APPPATH . "views/public/includes/login-registration-form-styles.php";
}