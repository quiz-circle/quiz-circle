<?php

require_once APPPATH . "models/functions/each_objects/question_list.php";
require_once APPPATH . "models/functions/each_objects/test_list.php";
require_once APPPATH . "models/functions/each_objects/related_question_each.php";
require_once APPPATH . "models/functions/each_objects/question_result_each.php";



function mark_obtained( $participation_id, &$percentage = 0 ) {
    $THIS =& get_instance();

    $THIS->db->select("q.full_marks as full_marks")->from('participation p')->join("quiz q", "p.quiz_id = q.quiz_id")->where("p.participation_id", $participation_id);
    $quiz = $THIS->db->get()->row_array();

    $full_mark = $quiz['full_marks'];


    $ans = $THIS->db->select()->from( "given_answers" )->where( array("participation_id" => $participation_id) )->get();


    $right_answers = 0;
    $marks = 0;
    if( $total_answers = $ans->num_rows() ){
        $each_mark = $full_mark / $total_answers;
        foreach ($ans->result_array() as $result  ){
            if( $result['given_answer'] == $result['right_answer']){
                $right_answers += 1;
            }
        }
        $percentage = floor( ($right_answers / $total_answers ) * 100 );
        $marks = $each_mark * $right_answers;
    }

    return number_format( $marks, 2, '.', '');
}


function check_quiz_token( &$result = array(), &$message = "" ){
    $SYS = get_instance();
    $SYS->load->model("authantication");
    $isLoggedIn =  $SYS->authantication->isLoggedIn();
    $sdata = $SYS->authantication->get_data();

    if($isLoggedIn){


        if( _post("token") && $SYS->session->has_userdata( _post("token") ) ) {
            $token = _post("token");

            if(strlen($token) == 32){
                $token_data = $SYS->session->userdata($token);

                if( isset( $token_data['user_id'] ) && isset( $token_data['quiz_id'] ) && $token_data['student_id'] ){
                    if( $sdata['user_id'] == $token_data['user_id'] ) {

                        prepare_content( $result['quiz_data'], $token_data['quiz_id']);
                        $result['questions'] = quiz_questions( $token_data['quiz_id'], $result["total_question"] );
                        $result['student_id'] = $token_data['student_id'];
                        return true;
                    } else {
                        $message = "This token is not yours!";
                    }
                } else {
                    $message = "Invalid Token Data!";
                }
            } else {
                $message = "Invalid Token!";
            }
        } else {
            $message = "Token not found!";
        }


    } else {
        $message = "Please login first!";
    }
    return false;
}


function _config_question( &$config, &$where){
    $tbsn = $config['table_short_name'];
    $config['table_name'] = "questions";
    $config['column'] = "u.user_id as user_id, profile_pic, display_name, quest_id as content_id, quest_title as content_title, answer, $tbsn.description as description, added_date, edited_date, $tbsn.status as status";
    $config['content_id_col'] = "quest_id";

    if( _post("data_form") == "__ajax__")  {
        if($search = _post("search-text")){
            $search = trim($search);
            $search = strip_tags($search);
            if(!empty($search)){
                $where[ 'quest_title LIKE'] = "%" .$search. "%"; //different <q>.quest_title may be $table_name_short_form + quest_id changed to content_id
            }
        }
    }
    unset( $where[$tbsn . ".content_type"] );
    unset( $where[ $tbsn . '.content_title LIKE'] );
}

function _config_quiz( &$config, &$where){

    $tbsn = $config['table_short_name'];
    $config['table_name'] = "quiz";
    $config['column'] = "u.user_id as user_id, profile_pic, display_name, quiz_id as content_id, quiz_title as content_title, $tbsn.quiz_description as description, added_date, edited_date, $tbsn.status as status, full_marks, quiz_type";
    $config['content_id_col'] = "quiz_id";

    $where[$tbsn . ".quiz_type" ] = _config($config,"quiz_type") ? _config($config,"quiz_type") : "quiz";
    $where[$tbsn . ".visibility" ] = 1;

    if($search = _post("search-text")){
        $search = trim($search);
        $search = strip_tags($search);
        if(!empty($search)){
            $where[ $tbsn . '.content_title LIKE'] = "%" .$search. "%"; //different <q>.quest_title may be $table_name_short_form + quest_id changed to content_id
        }
    }

    if( _post( "data_form" ) == "__ajax__")  {
        if( $search = _post("search-text") ){
            $search = trim($search);
            $search = strip_tags($search);
            if(!empty($search)){
                $where[ 'quiz_title LIKE'] = "%" .$search. "%"; //different <q>.quest_title may be $table_name_short_form + quest_id changed to content_id
            }
        }
    }
    $row = _config($config, "row");

    if( $row ) {
        $where[ $tbsn . ".quiz_id"] = _config( $config, "content_id" );
    }

    unset( $where[ $tbsn . ".content_id"] );
    unset( $where[ $tbsn . ".content_type"] );
    unset( $where[ $tbsn . '.content_title LIKE'] );
}

function _config_tutorial( &$config, &$where){
    $sbsn = $config['table_short_name'];
}

function _config_video( &$config, &$where){
    $sbsn = $config['table_short_name'];
}

function _config_paragraph( &$config, &$where){
    $sbsn = $config['table_short_name'];
}

function _config_notice( &$config, &$where){
    $sbsn = $config['table_short_name'];
    if(_get("cat_id")){
        $where["c.class_id"] = _get("cat_id");
    }
}


function category_types( $type = "" ){
    $list = array(
        'quiz' => 'quiz-category',
        'question' => 'question-category',
        'tutorial' => 'tutorial-category',
        'video' => 'video-category',
        'paragraph' => 'paragraph-category',
        'notice' => 'notice-category',
    );
    return isset($list[$type]) ? $list[$type] : $list;
}

function category_name($type = ""){
    $list = array(
        'quiz-category' => 'Topic',
        'question-category' => 'Subject',
        'tutorial-category' => 'Subject',
        'video-category' => 'Category',
        'paragraph-category' => 'Subject',
        'notice-category' => 'Topic',
    );
    return isset($list[$type]) ? $list[$type] : $list;
}

function prepare_contents( &$data, &$total = 0, $content_type = "", $config = array( ) ) {

    //Explain($_POST);
    $individual_options  = "_config_" . str_replace("-", "_", $content_type);
    $func_data = array();
    $where = array();

//    $config['column'] = "u.user_id as user_id, profile_pic, display_name, quest_id, quest_title, answer, q.description as description, added_date, edited_date, q.status as status";
    $config['column'] = "*";
    $config['table_short_name'] = $tb_sort_name = "cnt";
    $config['table_name'] = "content";
    $config['content_id_col'] = "content_id";

    $SYS = get_instance(  );
    $DB =& $SYS->db;
    $SYS->load->model( "contents" );

//    Explain( $_POST );

    if( function_exists($individual_options) ) $individual_options( $config, $where );
    $tb_sort_name = $config['table_short_name'];

    $where[ $tb_sort_name. '.status' ]  = 1;

    if( _post("data_form") == "__ajax__" )  {
        $filter = _post("filter");

        if($search = _post("search-text")){
            $search = trim($search);
            $search = strip_tags($search);
            if(!empty($search)){
                $where[ $tb_sort_name . '.content_title LIKE'] = "%" .$search. "%"; //different <q>.quest_title may be $table_name_short_form + quest_id changed to content_id
            }
        }

        if ( $cat_filter = _post("filter-category" ) ) {
            $where['cr.class_id'] = $cat_filter;
        }
    }

    $config['column'] = "u.user_id as user_id, profile_pic, display_name, content_id, content_title, answer, $tb_sort_name.description as description, added_date, edited_date, $tb_sort_name.status as status";
    if($content_type) $where[ $tb_sort_name . ".content_type"] = $content_type;

    $row = _config($config, "row");
    if($row) {
        $where[$tb_sort_name . ".content_id"] = _config($config, "content_id");
    }

    if( function_exists($individual_options) ) $individual_options( $config, $where );


    $func_data['where'] = $where;

    $func_data['table_short_name'] = $config['table_short_name'];
    $func_data['table_name'] = $config['table_name'];
    $func_data['content_id_col'] = $config['content_id_col'];

    $func_data['table_name'] = $config['table_name'];

    $SYS->contents->set_function_data( $func_data );

    $config['settings'] =  function($db, $data){
        $where = $data['where'];
        $tsn = isset($data['table_short_name']) ? $data['table_short_name'] : ( isset($data['table_name']) ? $data['table_name'] : "content" ); // tsn table short names
        $content_id_col = isset( $data['content_id_col'] ) ? $data['content_id_col'] : "content_id";

        //Explain($where);

        $db->join("user u", "u.user_id = " . $tsn . ".user_id");
        $db->join("class_relation cr", $tsn . "." .$content_id_col. " = cr.entity_id");
        $db->join("class c", "c.class_id = cr.class_id");

        $db->where( $where );
        $db->group_by( $tsn . "." .$content_id_col );

    };

    //$SYS->contents->each_obj( "question_list" ) parameter will be different for various content_type
    $SYS->contents->each_obj( "test_list" );


    $data = $SYS->contents->_get_all( $config, -1, 0 );
    $total = $SYS->contents->get_total_num_rows();

    //echo $DB->last_query();

    $SYS->contents->reset_columns(  );
}

function prepare_content( &$data, $content_id,  $config = array() ){
    $content_type = _config($config, "content_type") ? _config($config, "content_type") : "quiz";
    $config['content_id'] = $content_id;
    $config['row'] = true;
    prepare_contents( $data, $total, $content_type, $config );
    if(isset( $data[0] )) $data = $data[0];
}


function prepare_finished_quiz_result( $participation_id, &$total = 0 , $each_result  = ""){
    $SYS =& get_instance();
    $SYS->db->select()->from( "given_answers a" );
    $SYS->db->join( "quest_assign qa", "a.quest_assign_id = qa.id" );
    $SYS->db->join( "questions qu", "qa.quest_id = qu.quest_id" );

    $SYS->db->where("participation_id", $participation_id );
    $SYS->db->group_by( "a.answer_id" );

    $res = $SYS->db->get();

    if( $total = $res->num_rows() ) {
        return (!empty($each_result)) ? $res->result($each_result) : $res->result_array();
    }
    return array();
}