<?php echo isset($login_page_title) ? $login_page_title : ""; ?>
<?php if( isset($next) && $next ){ $this->session->set_userdata( SESSION_NEXT_PAGE_AFTER_LOGIN, $next ); } ?>
<?php echo form_open( base_url("user/login"), '', array("_token" => form_token())); ?>

<div class="field-wrap">
    <?php

    $inputName = "username";
    $inputLabel = 'Username or email address<span class="req">*</span>';

    $_extra = array( "id" =>  $inputName, "autocomplete" => "off" );
    echo form_label( $inputLabel, $inputName );
    echo form_input( $inputName, _post($inputName),  $_extra);
    show_form_error( $inputName );
    ?>

</div>

<div class="field-wrap">
    <?php

    $inputName = "password";
    $inputLabel = 'Password<span class="req">*</span>';

    $_extra = array( "id" =>  $inputName,  "required" => "required", "autocomplete" => "off" );
    echo form_label( $inputLabel, $inputName );
    echo form_password( $inputName, _post($inputName),  $_extra);
    show_form_error( $inputName );
    ?>
</div>

<p class="forgot"><a href="user/forgot_password">Forgot Password?</a></p>

<button class="button button-block">Log In</button>

</form>

<?php if( isset($script) && $script ): ?>
<script>
    $(".form .field-wrap input, .form .field-wrap textarea").each(function () {
        var label = $(this).prev('label');
        if($(this).val() === ""){
            label.removeClass('active highlight');
        }else {
            label.addClass('active highlight');
        }
    });


    $('.form').find('input, textarea').on('keyup blur focus drop', function (e) {
        var $this = $(this), label = $this.prev('label');

        if (e.type === 'keyup' || e.type === 'drop') {
            if ($this.val() === '') {
                label.removeClass('active highlight');
                if(e.type === "drop") {
                    label.addClass('active highlight');
                }
            } else  {
                label.addClass('active highlight');
            }
        } else if (e.type === 'blur') {
            if ($this.val() === '') {
                label.removeClass('active highlight');
            } else {
                label.removeClass('highlight');
            }
        } else if (e.type === 'focus' ) {

        }

    });

    $('.tab a').on('click', function (e) {

        e.preventDefault();

        $(this).parent().addClass('active');
        $(this).parent().siblings().removeClass('active');

        target = $(this).attr('href');

        $('.tab-content > div').not(target).hide();

        $(target).fadeIn(600);

    });

</script>
<?php endif; ?>