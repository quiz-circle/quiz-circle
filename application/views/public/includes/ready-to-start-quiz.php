<style>

    .quiz-preperation p {
        color: #333;
    }

    .quiz-preperation p.quiz-on,
    .quiz-preperation p.quiz-title {
        margin: 5px 0;
    }

    .quiz-preperation p.quiz-head {
        color: #2b3a4c;
        font-size: 28px;
    }

    .quiz-preperation p.quiz-on {

    }

    .quiz-preperation p.quiz-title {
        font-size: 20px;
        color: #0e76e6;
        margin-bottom: 20px;
    }

</style>
<div class="quiz-preperation" style="text-align: center; color:#333">
    <?php

    prepare_content($quiz, $quiz_id);

    if( $this->users->isStudent( $sdata['user_id'], $student ) ):

//        Explain($student);

        //        echo $quiz_id;
        //        Explain($quiz);
        $token = $quiz->hash;
        quiz_questions( $quiz_id, $total_questions );

        $token_data = array(
            'student_id' => $student['student_id'],
            'user_id'   => $sdata['user_id'],
            'quiz_id'   => $quiz_id
        );

        $this->session->set_userdata($token, $token_data);
        ?>

        <p class="quiz-head">
            <strong>Quiz Title:</strong>
            <?php echo $quiz->title() ?>
        </p>

        <p class="quiz-full-marks">
            <strong>Full Marks:</strong>
            <?php echo $quiz->full_marks ?>
        </p>

        <p class="quiz">
            Total <?php echo (string) $total_questions; ?> Questions
        </p>


        <?php if(!empty($quiz->description)): ?>
            <p class="quiz-description">
                <strong>Quiz Description:</strong>
                <?php echo $quiz->description ?>
            </p>
        <?php endif; ?>


        <h2>Are you ready to do quiz</h2>
        <p class="quiz-on">on</p>
        <p  class="quiz-title"><?php echo $quiz->title(); ?></p>

        <?php if($total_questions > 0 && !empty($token)): ?>
            <a href="<?php echo base_url("quiz/perform?token=" . $token) ?>" class="btn btn-primary">Start Quizing</a>
        <?php endif; ?>
    <?php else:  ?>
        <h2>Please Login as Student or Register as Student</h2>
        <p><a href="<?php echo base_url("user/logout"); ?>">Log Out</a> and go on</p>
    <?php endif;  ?>
</div>
