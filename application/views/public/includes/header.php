<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo isset($page_title) ? $page_title: "Quiz Circle :: Learn Knowledge and evaluate yourself"?></title>
        <!-- font awesome -->
        <base href="<?php echo base_url(); ?>">

<!--        <link href="--><?php //theme_css("owl.carousel.css"); ?><!--" rel="stylesheet">-->
<!--        <link href="--><?php //theme_css("animate.css"); ?><!--" rel="stylesheet">-->
<!--        <link href="--><?php //theme_css("venobox.css"); ?><!--" rel="stylesheet">-->
<!--        <link href="--><?php //theme_css("slicknav.css"); ?><!--" rel="stylesheet">-->

        <link href="<?php theme_css("bootstrap.min.css"); ?>" rel="stylesheet">
        <link href="<?php theme_images("fevicon.png"); ?>" rel="icon" type="image/png">
        <link href="<?php theme_css("font-awesome.css"); ?>" rel="stylesheet">
        <link href="<?php theme_css("responsive.css"); ?>" rel="stylesheet">
        <link href="<?php theme_css("style.css"); ?>" rel="stylesheet">
        
        <link href="<?php theme_css("login-sign-up.css"); ?>" rel="stylesheet">

<!--        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">-->
<!--        <link rel="stylesheet" href="css1/css/style.css">-->


<!--        <script src="--><?php //theme_js("owl.carousel.min.js"); ?><!--"></script>-->
<!--        <script src="--><?php //theme_js("jquery.easing.1.3.js"); ?><!--"></script>-->
<!--        <script src="--><?php //theme_js("venobox.min.js"); ?><!--"></script>-->
<!--        <script src="--><?php //theme_js("isotope.min.js"); ?><!--"></script>-->
<!--        <script src="--><?php //theme_js("jquery.counterup.min.js"); ?><!--"></script>-->
<!--        <script src="--><?php //theme_js("jquery.slicknav.js"); ?><!--"></script>-->
<!--        <script src="--><?php //theme_js("main.js"); ?><!--"></script>-->

        <script src="<?php theme_js( "jquery.min.js" ); ?>"></script>
        <script src="<?php theme_js("bootstrap.min.js"); ?>"></script>
        <script src="<?php theme_js("core.js"); ?>"></script>

        <!--[if lt IE 9]>
            <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script> var base_url = '<?php echo  base_url(); ?>'; </script>
    </head>

    <body>


        <!------------------------MENU-AREA START--------------------->
        <nav class="navbar navbar-default nav-menu">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-hover="dropdown" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand  logo" href="">
                        <img src="<?php theme_images("logo.png") ?>" alt="Logo" >
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="display: inline-grid !important; overflow: hidden">

                    <div class="main-top-menu">


                        <ul class="nav navbar-nav navbar-right" style="display: block; margin-top: 10px">
                            <li><a href=""> <span class="glyphicon glyphicon-search"></span></a></li>


                            <?php

                            if($_isLoggedIn): ?>
                                <li class="dropdown" style="background-color: #f9fafb; ">
                                    <?php
                                        //Explain($sdata);
                                        
                                        if(isset($sdata['profile_image_link']) && !empty($sdata['profile_image_link'])) {
                                            $profile_link = $sdata['profile_image_link'];
                                        }else if(isset($sdata['profile_pic']) && !empty($sdata['profile_pic'])) {
                                            $profile_link = $sdata['profile_pic'];
                                        }else {
                                            $profile_link = "";
                                        }
                                        
                                    ?>
                                    <a class="dropdown-toggle" data-toggle="dropdown" style="padding: 10px" href="#"><?php echo $sdata['display_name']?> <span style="width:30px; height: 30px; display: inline-block; border:1px solid #ccc;"><img src="<?php echo $profile_link ?>" style="width: 100%"></span>
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu" style="margin-top: 0px">
                                        <li><a href="<?php echo base_url("user/profile"); ?>">Profile</a></li>
                                        <li><a href="<?php echo base_url("user/quiz/list"); ?>">Participated Quiz list</a></li>
                                        <li><a href="<?php echo base_url("user/logout"); ?>">Log Out</a></li>
                                    </ul>
                                </li>
                            <?php else: ?>
                                <li><a  href="user/login">Log In</a></li>
                                <li><a href="user/signup" style="padding-right: 0">Sign Up</a></li>
                            <?php endif; ?>
                        </ul>

                    </div>
                    <div class="main-bottom-menu">



                        <ul  style="display: inline-block" class="nav navbar-nav navbar-right" data-hover="dropdown" data-animations="bounce bounceInUp flipInX bounceInUp">
                            <li><a href=""> <span class="glyphicon glyphicon-home" title="Home"></span></a></li>

                            <li class="dropdown dropdown-full-width">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="">SUBJECT
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li class="full-width-item"><a href="">Mathematics</a>
                                        <ul>
                                            <li><a href="">Engineering Mathematics</a> </li>
                                            <li><a href="">Statistics and Probability</a> </li>
                                            <li><a href="">Discrete Mathematics</a> </li>
                                            <li><a href="">Differential Calculus</a> </li>
                                            <li><a href="">Integral Calculus</a> </li>

                                        </ul>
                                    </li>
                                    <li  class="full-width-item"><a href="">Programming Language</a>
                                        <ul>
                                            <li><a href="">C++</a> </li>
                                            <li><a href="">Python</a> </li>
                                            <li><a href="">PHP</a> </li>
                                            <li><a href="">Java</a> </li>
                                            <li><a href="">C</a> </li>
                                        </ul>
                                    </li>
                                    <li  class="full-width-item"><a href="">Computer Science</a>
                                        <ul class="">
                                            <li><a href="">Algorithm</a> </li>
                                            <li><a href="">Data Structure</a> </li>
                                            <li><a href="">Compiler Design</a> </li>
                                            <li><a href="">Computer Graphics</a> </li>
                                            <li><a href="">Software Engineering</a> </li>
                                        </ul>
                                    </li>
                                    <li class="full-width-item"><a href="">Generel Education</a>
                                        <ul class="">
                                            <li><a href="">Economics</a> </li>
                                            <li><a href="">Accounting</a> </li>
                                            <li><a href="">Basic Electronics</a> </li>
                                            <li><a href="">Essential computing</a> </li>
                                            <li><a href="">Basic Concept of Islam</a> </li>
                                        </ul>
                                    </li>
                                    <li class="full-width-item"><a href="">Others</a>
                                        <ul class="">
                                            <li><a href="">History of bangladesh</a> </li>
                                            <li><a href="">Grammer</a> </li>
                                            <li><a href="">World History</a> </li>
                                            <li><a href="">Generel Knowledge</a> </li>
                                            <li><a href="">Bangladesh</a> </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a  href="quiz/notice">NOTICE</a>
                            </li>

                            <li>
                                <a href="quiz/get_list">QUIZ TEST</a>
                            </li>

                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">TUTORIALS
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="">Science</a></li>
                                    <li><a href="">Business Studies</a></li>
                                    <li><a href="">Mathematics</a></li>
                                    <li><a href="">General Education</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        <!------------------------MENU-AREA END--------------------->