<style>
    .sussestion a,
    .forgot a {
        color:#ccc;
    }

    .sussestion a:hover,
    .forgot a:hover {
        color: #1ab188;
    }

    .form, .social-login {
        background: rgba(19, 35, 47, 0.9);
        padding: 40px;
        max-width: 600px;
        margin: 40px auto;
        border-radius: 4px;
        box-shadow: 0 4px 10px 4px rgba(19, 35, 47, 0.3);
    }

    .overall-message {
        margin: 10px auto;
        width: 600px;
        text-align: center;
    }

    .social-login{
        background: #D5D5D5;
        text-align: center;
        margin-top: 15px;
        padding: 0;
        padding: 15px;
    }
    .social-login h1{
        color: #145558;
        margin: 0;
        margin-bottom: 20px;
    }

    .social-login .btn{
        margin: 10px 50px;
    }

    .tab-group {
        list-style: none;
        padding: 0;
        margin: 0 0 40px 0;
    }
    .tab-group:after {
        content: "";
        display: table;
        clear: both;
    }
    .tab-group li a {
        display: block;
        text-decoration: none;
        padding: 8px;
        background: rgba(160, 179, 176, 0.25);
        color: #a0b3b0;
        font-size: 18px;
        float: left;
        width: 50%;
        text-align: center;
        cursor: pointer;
        -webkit-transition: .5s ease;
        transition: .5s ease;
    }
    .tab-group li a:hover {
        background: #179b77;
        color: #ffffff;
    }
    .tab-group .active a {
        background: #1ab188;
        color: #ffffff;
    }

    .tab-content > div:last-child {
        display: none;
    }

    h1 {
        text-align: center;
        color: #ffffff;
        font-weight: 300;
        margin: 0 0 40px;
    }

    label {
        position: absolute;
        -webkit-transform: translateY(6px);
        transform: translateY(6px);
        left: 13px;
        color: rgba(255, 255, 255, 0.5);
        -webkit-transition: all 0.25s ease;
        transition: all 0.25s ease;
        -webkit-backface-visibility: hidden;
        pointer-events: none;
        font-size: 15px;
        top: -2px;
        font-weight: normal
    }

    label .req {
        margin: 2px;
        color: #1ab188;
    }

    label.active {
        -webkit-transform: translateY(25px);
        transform: translateY(25px);
        left: 2px;
        font-size: 14px;
        top: 6px;
    }

    label.active .req {
        opacity: 0;
    }

    label.highlight {
        color: #ffffff;
    }

    input, textarea, select {
        font-size: 15px;
        display: block;
        width: 100%;
        height: 35px;
        padding: 8px 10px;
        background: none;
        background-image: none;
        border: 1px solid #145558;
        color: #ffffff;
        border-radius: 0;
        -webkit-transition: border-color .25s ease, box-shadow .25s ease;
        transition: border-color .25s ease, box-shadow .25s ease;
        font-weight: normal;
    }


    input:focus, textarea:focus {
        outline: 0;
        border-color: #7ca298;
    }

    textarea {
        border: 2px solid #a0b3b0;
        resize: vertical;
    }

    .field-wrap {
        position: relative;
        margin-bottom: 40px;
    }


    .dropdown {

    }

    .dropdown label {
        margin-top: -25px;
        margin-left: 0;
    }

    .dropdown select {

        //color:#0b0a0a !important;
    }

    .dropdown select option {
        background-color: #0b0a0a;
    }

    .top-row:after {
        content: "";
        display: table;
        clear: both;
    }

    .top-row > div {
        float: left;
        width: 48%;
        margin-right: 4%;
    }
    .top-row > div:last-child {
        margin: 0;
    }

    .button {
        border: 0;
        outline: none;
        border-radius: 0;
        padding: 15px 0;
        font-size: 2rem;
        font-weight: 600;
        text-transform: uppercase;
        letter-spacing: .1em;
        background: #1ab188;
        color: #ffffff;
        -webkit-transition: all 0.5s ease;
        transition: all 0.5s ease;
        -webkit-appearance: none;
    }
    .button:hover, .button:focus {
        background: #179b77;
    }

    .button-block {
        display: block;
        width: 100%;
    }

    .forgot, .sussestion {
        margin-top: -20px;
        text-align: right;
        color: #D5D5D5;
    }

    .form-error {
        width: 100%;
        height: 22px;
        float: right;
        position: absolute;
        text-align: right;
        right: 0;
        bottom: 100%;
        margin: 0;
        color: chocolate;
    }



</style>