<div class="social-login">
    <p>OR</p>
    <div id="forgot-password"  class="tab-content-in">
        <h1 style="font-size: 28px; ">Login with</h1>
        <a href="<?php echo base_url("user/social_login/facebook");?>" class="btn btn-primary">Facebook</a>
        <a href="<?php echo base_url("user/social_login/google");?>" class="btn btn-danger">Google</a>
    </div>
</div>