
<!------------------------FOOTER-AREA START--------------------->
<div class="footer-area">
    <div class="container">
        <div class="section-title center">
            <h2 style="color: orange" >Keep me Get in Touch</h2>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6" style="margin-top: 15px">
                <div class="single-footer-widget">
                    <div class="footer-logo">
                        <h3 style="background-color: orange">Quiz Circle</h3>
                    </div>
                    <p>Our system is being launched because a need for a destination that is beneficiar for both institutes and students. Student can give exam and view their result. It is also an attempt to remove the existing flaws in the manual system of conducting exams.</p>
                </div>
                <!-- .single-footer-widget -->
            </div>

            <div class="col-md-6 col-sm-6">
                <div id="contact" class="text-center">
<!--                    <h3>Leave me a message</h3>-->
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="row">
                            <div class="col-md-6 col-sm-4">
                                <div class="form-group">
                                    <input type="text" id="name" class="form-control" placeholder="Name" required="required">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-4">
                                <div class="form-group">
                                    <input type="email" id="email" class="form-control" placeholder="Email" required="required">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea name="message" id="message" class="form-control" rows="4" placeholder="Message" required></textarea>
                            <p class="help-block text-danger"></p>
                        </div>
                        <div id="success"></div>
                        <button type="submit" class="btn btn-default">Send Message</button>
                    </form>

                </div>

            </div>

        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6" style="margin-top: 50px">
                <div class="single-footer-widget">
                    <h4 class="footer-widget-title">Importaint link</h4>
                    <ul>
                        <li><a href="http://www.wikipedia.com" target="newtab">Wikipedia</a></li>
                        <li><a href="http://www.tutorialspoint.com" target="newtab">Tutorials Point</a></li>
                        <li><a href="http://www.slideshare.com" target="newtab">Slide Share</a></li>
                        <li><a href="http://www.khanacademy.com" target="newtab">Khan Academy</a></li>

                    </ul>
                </div>
                <!-- .single-footer-widget -->
            </div>
            <div class="col-md-9 col-sm-4" style="margin-top: 50px">
                <div class="col-md-4"> <i class="fa fa-map-marker fa-2x"></i>
                    <p>Gulshan Campus: <br>
                        Plot#CEN-16, Road # 106<br>
                        Gulshan-2, Dhaka-1212<br>
                        Bangladesh
                    </p>
                    <p>Ashulia Permanent Campus: <br>
                        Ashulia Model Town,
                        Khagan, Ashulia
                        Dhaka
                    </p>
                </div>
                <div class="col-md-4 col-sm-4"> <i class="fa fa-envelope-o fa-2x"></i>
                    <p>www.manarat.ac.bd</p>
                    <p> info@manarat.ac.bd</p>
                    <p>admission@manarat.ac.bd</p>
                </div>
                <div class="col-md-4 col-sm-4"> <i class="fa fa-phone fa-2x"></i>
                    <p> 02-55060025, 02-9862251 </p>
                    <p>Fax: 02-55059924, 01780364414  </p>
                    <p>Ashulia: 01819245895, 01780364415 </p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="footer-social-icon" style="text-align: right">
            <a href="https://www.facebook.com/quiz.circle04" target="newtab"><i class="fa fa-facebook"></i></a>
            <a href="#" target="newtab"><i class="fa fa-linkedin"></i></a>
            <a href="#" target="newtab"><i class="fa fa-twitter"></i></a>
            <a href="https://www.youtube.com/channel/UCM3utOB-62iSU01NTqFEa2g?view_as=subscriber" target="newtab"><i class="fa fa-youtube"></i></a>
            <a href="https://plus.google.com/u/0/102785994042454756285" target="newtab"><i class="fa fa-google-plus"></i></a>
        </div>

    </div>

</div>

<div class="footer-copyright-area" style="background-color: #FBEEE6 ">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="copyright">
                    <p style="color: black"><strong>Copyright </strong><span>&copy;</span><strong> 2017, All Right Reserved</strong></p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="design-by">
                    <p style="color: black"><strong>Designed by </strong><span><strong>Team of Quiz Circle</strong></span></p>
                </div>
            </div>
        </div>
    </div>
</div>
<!------------------------FOOTER-AREA END--------------------->



<script  src="<?php theme_js("login-sign-up.js") ?>"></script>

</body>

</html>