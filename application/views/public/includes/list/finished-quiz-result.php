
<?php if( isset( $result_list ) && count( $result_list ) ) : $counter = 0; ?>
    <?php foreach( $result_list as $result ): $counter++; ?>
        <tr>
            <td> <?php echo $counter; ?></td>
            <td>
                <div class="quiz-title">
                    <?php echo $result->title( ); ?>
                </div>
                <div class="action">
                    <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#quizResult" style="padding: 1px 4px;" data-participation-id="<?php echo $result->id; ?>">
                        View details
                    </button>
                    <!--<a href="javascript:" date-target="#quizResult" data-toggle="modal" class="btn btn-info btn-sm" style="padding: 1px 4px;">View Details</a>-->
                </div>
            </td>
            <td> <?php echo $result->date( ); ?></td>
            <td> <?php echo $result->full_mark( ); ?></td>
            <td> <?php echo $result->mark_obtained( ); ?></td>
        </tr>
    <?php endforeach; ?>
<?php endif; ?>