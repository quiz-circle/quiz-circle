
<style>
    .result-details {
        float: left;
        border:2px solid #ccc;
        margin-bottom: 5px;
        margin-top: 5px;
        width: 100%;
        padding: 10px;
        border-radius: 5px;
        display: inline-block;
    }

    .right_answer_details {

        border-color: #3bb175;;
    }

    .result-details .options {
        float: left;
        width: 100%;
        margin-bottom: 20px;
    }

    .right_answer_details .options {

    }

    .right_answer_details .options .option {
        padding: 5px;
    }

    .result-details  .options .right-answer {
        color: #3bb175;
        padding: 5px;
    }
    .result-details  .options .wrong-answer {
        color: #990000;
        text-decoration: line-through;
    }

</style>

<?php if( isset( $answers ) && count( $answers ) ) : $counter = 0; ?>
    <?php foreach( $answers as $answer ): $counter++; ?>
        <?php $right_ans_class = ($answer->isRight() ) ? 'right_answer_details' : ''; ?>

        <h3><?php echo $counter; ?>. <?php echo $answer->title( ); ?></h3>
        <div class="result-details <?php echo $right_ans_class; ?>">
            <?php
                $options = $answer->answer_options( $right_answer );
                echo "<div class='options'><div style='width: 100%; float: left;font-weight: bold; font-style: italic'>Options</div> ";
                    $count = 1;
                    foreach ($options as $opt){
                        $right_class = ($opt == $right_answer) ? 'right-answer':'';

                        $wrong_class = ($opt == $answer->given_answer && !$answer->isRight() ) ? 'wrong-answer':'';

                        echo "<div class='option $right_class $wrong_class'>" .$count .". ". $opt . "</div>";
                        $count++;
                    }
                echo "</div>";

                if(empty($answer->given_answer)){
                    echo "<span style='color: #990000; '>No answer is given for this question!</span>";
                }else {
                    echo ($answer->isRight() ) ? "<span style='color: #3bb175'>Bravo! You have given the right answer!</span>" : "Sorry you have given a wrong answer!<br>";
                    if(!($answer->isRight() )){
                        echo "Right answer is <EM><STRONG style='color: green'>" .$right_answer. "</STRONG></EM>. you given <EM><STRONG style='color: #990000'>" . $answer->given_answer . "</STRONG></EM>";
                    }
                }
            ?>




        </div>

    <?php endforeach; ?>
<?php endif; ?>