<?php
/**
 * Created by PhpStorm.
 * User: Saifuddin
 * Date: 12/10/2017
 * Time: 12:07 AM
 */

//Explain($questions);
?>

<style>
    .quiz-element {
        margin-bottom: 30px;
        float: left;
    }

    .quiz-element .quiz-title {
        font-size: 18px;
        margin-bottom: 10px;
        float: left;
        width: 100%;
        font-weight: bold;
    }

    .answer-options .answer-option {
        width: 100%;
        float: left;
        font-weight: normal;
    }

</style>







<?php if(isset($questions) && count($questions) ):$count=0; ?>
    <?php foreach($questions as $question):$count++; ?>
        <div class="col-lg-12 quiz-element">

            <div class="quiz-title">
                <?php echo $count.". ". $question->title(); ?>
            </div>
            <div class="answer-options">
                <?php $counter = 0; foreach($question->answer_options() as $label => $option): $counter++; ?>
                    <div class="answer-option">
                        <label>
                            <input type="radio" value="<?php echo $option?>" id="<?php echo $counter."_".$question->id_hash()?>" name="given_answer[<?php echo $question->id ?>]"  >
                        </label>
                        <label for="<?php echo $counter."_".$question->id_hash()?>"><?php echo $option?></label>
                    </div>
                <?php endforeach; ?>
            </div>

        </div>
    <?php endforeach; ?>
<?php endif; ?>
