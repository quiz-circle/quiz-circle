<div class="col-lg-12" style="text-align: center">

    <?php
    /**
     * Created by PhpStorm.
     * User: Saifuddin
     * Date: 12/15/2017
     * Time: 1:50 AM
     */

    echo "<br><br><h5>Your exam has been finished!</h5>";
    echo "Your have got <em><strong>" . $mark_obtained . "</strong></em> out of <em><strong>" . $full_mark . "</strong></em>. marks";
    //
    ?>
    <br>
    <br>
    <button type="button" id="view-details" class="btn  btn-info" data-toggle="modal" data-target="#quizResult" data-participation-id="<?php echo $participation_id; ?>">
        View details
    </button>
    <a href="<?php echo base_url("quiz/get_list"); ?>" class="btn btn-primary">Back</a>
</div>

<!-- Modal for view details -->
<div class="modal fade" id="quizResult" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Details</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>

$("#view-details").on("click", function(e){

    var result_id = $(this).attr( "data-participation-id" );

    //alert(result_id);

    _request_ajax({
        url : "view/student_quiz_result",
        success : function( result ){
            $("#quizResult .modal-body").html( result );
        },
        data: { participation_id : result_id }
    });
});
</script>