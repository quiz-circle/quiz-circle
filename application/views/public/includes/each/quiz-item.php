<?php if( isset($quiz_list) && count($quiz_list) ): ?>
    <?php foreach( $quiz_list as $quiz): ?>

        <tr>
            <td><?php echo $quiz->title(); ?></td>

            <td class="text-muted" style="text-align: center; font-size: 12px"><?php echo $quiz->date_edited; ?></td>

            <td class="text-muted" style="text-align: center; font-size: 12px"><?php echo implode(",",  $quiz->categories("quiz-category", "name") ); ?></td>
            <td class="text-muted" style="text-align: center; font-size: 12px"><?php echo $quiz->display_name; ?></td>

            <td class="center quiz-actions" style="text-align: center">
                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#before-you-go" data-quiz-id="<?php echo $quiz->id; ?>">
                    Take Exam
                </button>
            </td>
        </tr>

    <?php endforeach; ?>
<?php endif; ?>