<?php $this->_public->header( data() ) ?>

<!--    <style>-->
<!---->
<!--        #wrapper {-->
<!--            width: 100%;-->
<!--            border-top: 5px solid #f9bf3b;-->
<!--        }-->
<!--        #quiz-search{-->
<!---->
<!--            width: 60%;-->
<!--            height: 70px;-->
<!--            margin-top: 50px;-->
<!--            margin-bottom: 50px;-->
<!--            background-color: white;-->
<!--            -webkit-background-size: cover;-->
<!--            background-size: cover;-->
<!--        }-->
<!---->
<!--        #search .brand {-->
<!--            width: 7%;-->
<!--            height: 70px;-->
<!--            padding-left: 10px;-->
<!--            padding-right: 10px;-->
<!--            background-color: #f9bf3b;-->
<!--        }-->
<!--        -->
<!--        .btn-group {-->
<!--            color: red;-->
<!--        }-->
<!--        .table table-hover{-->
<!--            margin-top: 50px;-->
<!--        }-->
<!---->
<!---->
<!--    </style>-->


<div class="container">
    <div class="row">


        <div id="content" class="col-lg-12">
            <h2 class="title">Quiz List</h2>
            <div class="box">


                <div class="box-content">
                    <form class="form-horizontal" method="post">
                        <fieldset>


                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="category">Select Subject</label>
                                    <select id="category" name="filter-category" class="form-control">
                                        <option value="">All Subject</option>
                                        <?php
                                        $this->category->type = "quiz-category";
                                        $this->category->getList( $cat_config );
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="search">Search</label>
                                    <input type="text" class="form-control" id="search" name="search-text" placeholder="Search...">
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>

            </div><!--/span-->


            <?php
            //Explain($quiz_list);
            ?>

            <div class="box span12">

                <div class="box-content">
                    <table id="quiz-list" class="table table-striped">
                        <thead>
                        <tr>
                            <th style="text-align: center">Quiz Title</th>
                            <th style="text-align: center">Published Date</th>
                            <th style="text-align: center">Category</th>
                            <th style="text-align: center">Teacher</th>
                            <th style="text-align: center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php public_view("includes/each/quiz-item") ?>
                        </tbody>
                    </table>
                </div>

            </div><!--/.fluid-container-->
        </div>




        <!--     Modal-->
        <div class="modal fade bd-example-modal-lg" id="before-you-go" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog  modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <input type="hidden" value="" name="_quiz_id_" id="_quiz_id_">
                        <h5 class="modal-title" id="exampleModalLabel" style="display: inline-block">Before Starting</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    </div><!--/row-->

</div><!--/.fluid-container-->

<!-- end: Content -->


<script>


    $("#search").on( "keyup", function(){
        load_quiz_content();
    });

    $("#category").on( "change", function(){
        load_quiz_content();
    });


    function load_quiz_content(){
        _request_ajax({
            url:"view/content_list",
            success: function( result ){

                $( "#quiz-list tbody" ).html( result );

            },
            formRef : $(".form-horizontal"),
            data: { "data_form" : "__ajax__" }
        });
    }

    $("td.quiz-actions button.btn").on("click", function(e){
        var quiz_id = $(this).attr("data-quiz-id");
        $("#_quiz_id_").val(quiz_id);
    });

    $( '#before-you-go' ).on('shown.bs.modal', function (e) {
        console.log(e);
        _request_ajax({
            url : "view/before_you_go_page",
            success : function(result){
                console.log( result );
                $("#before-you-go").find(".modal-body").html( result );
            },
            data: { quiz_id: $("#_quiz_id_").val(), "a":45 }
        });
    });

</script>
<?php $this->_public->footer( data() ) ?>


