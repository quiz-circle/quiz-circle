<?php $this->_public->header( data() ); ?>

<?php
    if($_isLoggedIn){
        redirect(base_url("user/profile"));
    }
?>




<?php
    require_once APPPATH . "views/public/functions/login-functions.php";
    login_reg_styles();
    $signup_success = isset($signup_success) ? $signup_success : false;

//    Explain($messages);

    echo "<div class='overall-message'>";
        echo isset($ovarall_message) ? $ovarall_message:"";
    echo "</div>";
?>

<?php if(  !$signup_success ): ?>
    <div class="form">
        <?php if (uri_seg(2) === "login" || uri_seg(2) === "signup"): ?>
            <ul class="tab-group">
                <li class=" <?php echo uri_seg(2) == "login" ? "active" : ""; ?>" data-action="login"><a href="<?php echo base_url("user/login");?>">Log In</a></li>
                <li class=" <?php echo uri_seg(2) == "signup" ? "active" : ""; ?>" data-action="signup"><a href="<?php echo base_url("user/signup");?>">Sign Up</a></li>
            </ul>

            <div class="tab-content">

                <div id="signup" class="tab-content-in">
                    <h1>Sign Up for Free</h1>
                    <?php echo form_open('', '', array("_token" => form_token())); ?>

                        <div class="top-row">
                            <div class="field-wrap">

                                <?php

                                    $inputName = "firstname";
                                    $inputLabel = 'First Name<span class="req">*</span>';

                                    $_extra = array( "id" =>  $inputName,  "required" => "required", "autocomplete" => "off" );
                                    echo form_label( $inputLabel, $inputName );
                                    echo form_input( $inputName, _post($inputName),  $_extra);
                                    show_err_message($messages, $inputName );
                                ?>
                            </div>

                            <div class="field-wrap">
                                <?php
                                    $inputName = "lastname";
                                    $inputLabel = 'Last Name<span class="req">*</span>';
                                    echo form_label( $inputLabel, $inputName );
                                    echo form_input( $inputName, _post($inputName),  $_extra);
                                    show_err_message($messages, $inputName );
                                ?>
                            </div>
                        </div>

                        <div class="field-wrap">
                            <?php

                                $inputName = "email";
                                $inputLabel = 'Email<span class="req">*</span>';

                                $extra = array(
                                    'type'  => 'email',
                                    'name'  => $inputName,
                                    'id'    => $inputName,
                                    'required' => 'required',
                                    'autocomplete' => 'off',
                                );

                                echo form_label( $inputLabel, $inputName );
                                echo form_input( $inputName, _post($inputName),  $extra );
                                show_err_message( $messages, $inputName );
                            ?>
                        </div>
                        <div class="field-wrap">
                            <?php

                                $inputName = "username";
                                $inputLabel = 'Username<span class="req">*</span>';

                                $data = array(
                                    'type'  => 'text',
                                    'name'  => $inputName,
                                    'id'    => $inputName,
                                    'required' => 'required',
                                    'autocomplete' => 'off',
                                );

                                echo form_label( $inputLabel, $inputName );
                                echo form_input( $inputName, _post($inputName),  $data);
                                show_err_message( $messages, $inputName );
                            ?>
                        </div>

                        <div class="field-wrap dropdown">
                            <?php
                                $inputName = "gender";
                                $data = array(
                                    'name'  => $inputName,
                                    'id'    => $inputName,
                                    'required' => 'required',
                                );

                                $gender = array( "" => "(Gender)", "male" => "Male", "female" => "Female");

                                echo form_dropdown($inputName, $gender, _post($inputName), $data);
                                show_err_message( $messages, $inputName );

                            ?>
                        </div>

                        <div class="field-wrap">
                            <?php
                                $inputName = "password";
                                $inputLabel = 'Password<span class="req">*</span>';
                                echo form_label( $inputLabel, $inputName );
                                echo form_password( $inputName, _post($inputName),  $_extra);
                                show_err_message( $messages, $inputName );
                            ?>
                        </div>

                        <div class="field-wrap">
                            <?php
                                $inputName = "password_again";
                                $inputLabel = 'Retype Your Password<span class="req">*</span>';
                                echo form_label( $inputLabel, $inputName );
                                echo form_password( $inputName, _post($inputName),  $_extra);
                                show_err_message( $messages,  $inputName );
                            ?>
                        </div>

                        <div class="field-wrap">
                            <?php
                                $inputName = "student_id";
                                $inputLabel = 'Student ID<span class="req">*</span>';
                                echo form_label( $inputLabel, $inputName );
                                echo form_input( $inputName, _post($inputName),  $_extra);
                                show_err_message( $messages, $inputName );
                            ?>
                        </div>

                        <p class="sussestion tab-group">Already Member? Then <a href="user/login">Login</a></p>

                        <button type="submit" class="button button-block"/>Get Started</button>

                    </form>

                </div>

                <div id="login"  class="tab-content-in">

                    <?php
                        $data = array();
                        $data['login_page_title'] = "<h1>Welcome Back!</h1>";
                        $data[ 'next' ] = urlencode( _get( "next" ) );

                        login_form( $data );
                    ?>

                </div>


            </div><!-- tab-content -->
        <?php else: ?>
            <div id="forgot-password"  class="tab-content-in">
                <h1 style="font-size: 28px; ">Type your email address and submit<br><small style="color: #b3b3b3; font-size: 15px !important">a password reset link will be sent to your email account!</small></h1>

                <?php echo form_open('', '', array("_token" => form_token())); ?>

                    <div class="field-wrap">
                        <?php
                            $inputName = "email";
                            $inputLabel = 'Email Address<span class="req">*</span>';

                            $data = array(
                                'type'  => 'email',
                                'name'  => $inputName,
                                'id'    => $inputName,
                                'value' => _post($inputName),
                                'required' => 'required',
                                'autocomplete' => 'off',
                            );


                            echo form_label( $inputLabel, $inputName );
                            echo form_input( $data);
                            show_form_error( $inputName );
                        ?>
                    </div>


                    <p class="sussestion"><a href="user/login">Go To Login Page</a></p>
                    <button class="button button-block"/>Submit</button>

                </form>

            </div>
        <?php endif; ?>

    </div> <!-- /form -->
<?php else : ?>
    <div class="container" style="text-align: center">
        <div class="row">
            <div class="col-lg-12">
                <h2>Registration complete!</h2>
                <p>You can <a href="<?php echo base_url( "user/login?_nu_" ); ?>" class="btn btn-primary">login</a>  now</p>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php social_login_links(); ?>

<script>
    var user_action = '<?php echo uri_seg(2) ?>';
    $(".tab-group li").removeClass("active");

    $(".tab-group").find("li[data-action='"  + user_action + "']").addClass("active");

    $(".tab-content .tab-content-in").css("display", "none");
    $(".tab-content #" + user_action).css("display", "block");

</script>






<?php $this->_public->footer( data() ); ?>
