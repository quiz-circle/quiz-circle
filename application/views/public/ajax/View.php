<?php
/**
 * Created by PhpStorm.
 * User: Saifuddin
 * Date: 12/8/2017
 * Time: 6:28 PM
 */

require_once APPPATH . "views/public/functions/login-functions.php";
require_once APPPATH . "views/public/functions/quiz-functions.php";

class View extends APP_Crud {

    public function __construct(){
        parent::__construct();

    }

    public function before_you_go_page( $data ){

        $is_logged_in = $data['is_logged_in'];

        if ( $is_logged_in ) {
            $data[ 'quiz_id' ] = _post("quiz_id");
            public_view("includes/ready-to-start-quiz", $data);
        } else {
            $data['login_page_title'] = "<h1>You Need To Login To Continue!</h1>";
            $data['next'] = base_url("quiz/get_list");
            $data['script'] = true;
            login_reg_styles();
            echo "<div class=\"form\">";
                login_form( $data );
            echo "</div>";
            social_login_links();
        }
    }

    public function content_list( ){
        $data =& data();
        $config = array(
            'quiz_type' => 'quiz'
        );
        prepare_contents( $quiz_list, $quiz_total, "quiz" , $config);

        $data['quiz_list'] = $quiz_list;
        $data['quiz_total'] = $quiz_total;

        public_view("includes/each/quiz-item.php");
    }

    public function student_quiz_result(){
        $data =& data();
        $id = _post("participation_id");
        //echo $id;
        $data['answers'] = prepare_finished_quiz_result($id, $total, "question_result_each");

        //Explain( $results  );
        public_view("includes/list/finished-quiz-answers");

    }


    public function finished_quiz_result( $data ) {

        $result_token = _post("token");

        $data = &data();
        if($this->session->has_userdata($result_token)) {
            $result_data = $this->session->userdata( $result_token );
            $participation_id = $result_data['participation_id'];

            prepare_content( $quiz, $result_data[ 'quiz_id' ] );

            $data['quiz_id'] = $result_data[ 'quiz_id' ];
            $data['full_mark'] = $quiz->full_marks;
            $data['participation_id'] = $participation_id;
            $data['mark_obtained'] = mark_obtained( $participation_id );

            public_view( "includes/just-finished" );
        }
    }

}
