<?php
/**
 * Created by PhpStorm.
 * User: Saifuddin
 * Date: 12/11/2017
 * Time: 11:04 PM
 */

require_once APPPATH . "views/public/functions/login-functions.php";
require_once APPPATH . "views/public/functions/quiz-functions.php";




class Data extends  APP_Crud {


    public function quiz_from_token( &$result ){
        $stata = $result['sdata'];
        unset($result['sdata']);
        $result["success"] = check_quiz_token($result, $result["message"]);
        unset($result["questions"]);
    }


}