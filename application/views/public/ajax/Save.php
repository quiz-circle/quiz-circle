<?php
/**
 * Created by PhpStorm.
 * User: Saifuddin
 * Date: 12/12/2017
 * Time: 1:54 AM
 */

require_once APPPATH . "views/public/functions/login-functions.php";
require_once APPPATH . "views/public/functions/quiz-functions.php";

class Save extends  APP_Crud {
    public function quiz_performance( &$result ){
        $sdata = $result['sdata'];
        unset($result['sdata']);

        $question_participated = _post( "given_answer" ) ? _post( "given_answer" ) : array();
        $quiz_id = _post("quiz_id");
        $student_id = _post("student_id");


//        Explain( $_POST );

        if(  check_quiz_token()  ){

            if($this->db->insert( "participation", array( 'student_id' => $student_id, 'quiz_id' => $quiz_id, 'date' => date_db_formated() ) )){

                if( $participation_id = $this->db->insert_id() ){



                    $quiz_questions = quiz_questions( $quiz_id, $total, "related_question_each" );

//                    Explain($quiz_questions);
//                    Explain($question_participated);


                    if( $total ) {

                        foreach($quiz_questions as $question){


                            $given_answer = array_key_exists( $question->id, $question_participated ) ? $question_participated[$question->id]:"";

                            $answer_options_json = $question->answer_options;
                            $answer_options = $question->answer_options( $right_answer );
                            $given_answer_data = array(
                                'participation_id' => $participation_id,
                                'quest_assign_id' => $question->id,
                                'given_answer' => $given_answer,
                                'right_answer' => $right_answer,
                                'answer_options' => $answer_options_json,
                            );


                            $this->db->insert( "given_answers", $given_answer_data );
                        }
                        $result["success"] = true;

                        $this->session->unset_userdata( _post("token") );

                        $result[ "token" ] = $token = _unique_hash( );

                        $token_data = array(
                            'student_id' => $student_id,
                            'participation_id'   => $participation_id,
                            'quiz_id' => $quiz_id,
                            'user_id' => $sdata['user_id'],
                        );

                        $this->session->set_userdata($token, $token_data);

                    }
                }
            }
        } else {
            $result["message"] = "Your token has expired or not logged in!";
        }

    }
}
