<?php if(count($result) ):?>
    <table border="1" style="border-collapse: collapse">
        <thead>
            <tr>
                <?php foreach($table_keys as $key):?>
                <td><?php echo $key ?></td>
                <?php endforeach;?>
            </tr>
        </thead>
        <tbody>
            <?php foreach($result as $data):?>
            <tr>
                <?php foreach($table_keys as $key):?>
                <td><?php echo $data[$key]; ?></td>
                <?php endforeach;?>
            </tr>
            <?php endforeach;?>

        </tbody>
    </table>
<?php endif;?>